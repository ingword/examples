unit CustomTypes;

interface

uses Windows, SysUtils, Graphics;

type
  // ���������� ����������
  PFloatPoint = ^TFloatPoint;
  TFloatPoint = record
    X, Y: Extended;
  end;
  
  // ������ �����
  TPointsArray = array of TFloatPoint;

  // ��������� ��� �������������� ���������
  ICoordConvert = interface(IInterface)
  ['{FA6D6063-9D24-4172-B577-1EAECFCA09EB}']
    procedure LogToScreen(lX, lY: Extended; var sX, sY: Integer); overload;
    procedure ScreenToLog(sX, sY: Integer; var lX, lY: Extended); overload;
    function LogToScreen(Value: Extended): Integer; overload;
    function ScreenToLog(Value: Integer): Extended; overload;
  end;

  // ���������� �����
  TLogicalCanvas = class(TObject)
  private
    FCanvas: TCanvas;
    FConvertIntf: ICoordConvert;
  public
    constructor Create(Canvas: TCanvas; ConvertIntf: ICoordConvert);
    // ������ ���������
    procedure DrawVertex(X, Y: Extended);
    procedure DrawLine(X1, Y1, X2, Y2, LineWidth: Extended);
    procedure DrawRect(X1, Y1, X2, Y2, LineWidth: Extended);
    procedure DrawRoundRect(X1, Y1, X2, Y2, LineWidth: Extended);
    procedure DrawText(X1, Y1, X2, Y2, TextHeight: Extended; Text: String);
    procedure DrawPolygon(Pts: TPointsArray; LineWidth: Extended);
  end;

implementation

uses Types;

{ TLogicalCanvas }

constructor TLogicalCanvas.Create(Canvas: TCanvas;
  ConvertIntf: ICoordConvert);
begin
  inherited Create;
  FCanvas := Canvas;
  FConvertIntf := ConvertIntf;
end;

procedure TLogicalCanvas.DrawLine(X1, Y1, X2, Y2, LineWidth: Extended);
var
  sX1, sY1, sX2, sY2: Integer;
begin
  // ������� � �������� ����������
  FConvertIntf.LogToScreen(X1, Y1, sX1, sY1);
  FConvertIntf.LogToScreen(X2, Y2, sX2, sY2);
  // ������ �����
  FCanvas.Pen.Width := FConvertIntf.LogToScreen(LineWidth);
  // ������
  FCanvas.MoveTo(sX1, sY1);
  FCanvas.LineTo(sX2, sY2);
end;

procedure TLogicalCanvas.DrawPolygon(Pts: TPointsArray;
  LineWidth: Extended);
var
  i: Integer;
  sPts: array of TPoint;
begin
  // ������� � �������� ����������
  SetLength(sPts, Length(Pts));
  for i := 0 to Length(Pts) - 1 do
    FConvertIntf.LogToScreen(Pts[i].X, Pts[i].Y, sPts[i].X, sPts[i].Y);
  // ������ �����, ��� �����
  FCanvas.Pen.Width := FConvertIntf.LogToScreen(LineWidth);
  FCanvas.Brush.Style := bsSolid;
  // ������
  FCanvas.Polygon(sPts);
end;

procedure TLogicalCanvas.DrawRect(X1, Y1, X2, Y2, LineWidth: Extended);
var
  sX1, sY1, sX2, sY2: Integer;
begin
  // ������� � �������� ����������
  FConvertIntf.LogToScreen(X1, Y1, sX1, sY1);
  FConvertIntf.LogToScreen(X2, Y2, sX2, sY2);
  // ������ �����, ��� �����
  FCanvas.Pen.Width := FConvertIntf.LogToScreen(LineWidth);
  FCanvas.Brush.Style := bsSolid;
  // ������
  FCanvas.Rectangle(sX1, sY1, sX2, sY2);
end;

procedure TLogicalCanvas.DrawRoundRect(X1, Y1, X2, Y2,
  LineWidth: Extended);
var
  sX1, sY1, sX2, sY2: Integer;
begin
  // ������� � �������� ����������
  FConvertIntf.LogToScreen(X1, Y1, sX1, sY1);
  FConvertIntf.LogToScreen(X2, Y2, sX2, sY2);
  // ������ �����, ��� �����
  FCanvas.Pen.Width := FConvertIntf.LogToScreen(LineWidth);
  FCanvas.Brush.Style := bsSolid;
  // ������
  FCanvas.RoundRect(sX1, sY1, sX2, sY2, (sX2 - sX1) div 6, (sY2 - sY1) div 2);
end;

procedure TLogicalCanvas.DrawText(X1, Y1, X2, Y2, TextHeight: Extended;
  Text: String);
var
  sX1, sY1, sX2, sY2, H: Integer;
  ARect: TRect;
begin
  // ������� � �������� ����������
  FConvertIntf.LogToScreen(X1, Y1, sX1, sY1);
  FConvertIntf.LogToScreen(X2, Y2, sX2, sY2);
  // ������ ������, ��� �����
  FCanvas.Brush.Style := bsClear;
  FCanvas.Font.Height := FConvertIntf.LogToScreen(TextHeight);
  // ������� ����� � �������������� �� ����� ����
  ARect := Rect(sX1, sY1, sX2, sY2);
  H := Windows.DrawText(FCanvas.Handle, PChar(Text), -1, ARect,
    DT_CENTER or DT_WORDBREAK or DT_CALCRECT);
  OffsetRect(ARect, 0, (sY2 - sY1 - H) div 2);
  ARect.Left := sX1;
  ARect.Right := sX2;
  Windows.DrawText(FCanvas.Handle, PChar(Text), -1, ARect,
    DT_CENTER or DT_WORDBREAK);
end;

procedure TLogicalCanvas.DrawVertex(X, Y: Extended);
var
  sX, sY: Integer;
begin
  // ������� � �������� ����������
  FConvertIntf.LogToScreen(X, Y, sX, sY);
  // ������ �����
  FCanvas.Pen.Width := 1;
  // ������
  FCanvas.Rectangle(sX - 3, sY - 3, sX + 3, sY + 3);
end;

end.
