inherited FormCustomersOrdersItemsGrids: TFormCustomersOrdersItemsGrids
  Caption = 'FormCustomersOrdersItemsGrids'
  ClientHeight = 530
  ClientWidth = 597
  ExplicitWidth = 605
  ExplicitHeight = 564
  PixelsPerInch = 96
  TextHeight = 13
  inline FraCustomersTable1: TFraCustomersTable
    Left = 0
    Top = 0
    Width = 313
    Height = 530
    Align = alLeft
    TabOrder = 0
    ExplicitWidth = 313
    ExplicitHeight = 530
    inherited DBGridCustomers: TDBGrid
      Width = 313
      Height = 530
    end
  end
  object Panel1: TPanel
    Left = 313
    Top = 0
    Width = 284
    Height = 530
    Align = alClient
    TabOrder = 1
    ExplicitLeft = 448
    ExplicitTop = 200
    ExplicitWidth = 185
    ExplicitHeight = 41
    inline FraOrdersGrid1: TFraOrdersGrid
      Left = 1
      Top = 1
      Width = 282
      Height = 195
      Align = alTop
      TabOrder = 0
      ExplicitTop = 112
      inherited DBGridOrders: TDBGrid
        Width = 282
      end
      inherited TableOrders: TTable
        IndexName = 'CustNo'
        MasterFields = 'CustNo'
        MasterSource = FraCustomersTable1.DataSourceCustomers
      end
    end
    inline FraItemsGrid1: TFraItemsGrid
      Left = 1
      Top = 196
      Width = 282
      Height = 333
      Align = alClient
      TabOrder = 1
      ExplicitLeft = 80
      ExplicitTop = 248
      ExplicitWidth = 9
      inherited DBGridItems: TDBGrid
        Width = 282
        Height = 333
      end
      inherited TableItems: TTable
        IndexFieldNames = 'OrderNo'
        MasterFields = 'OrderNo'
        MasterSource = FraOrdersGrid1.DataSourceOrders
      end
    end
  end
end
