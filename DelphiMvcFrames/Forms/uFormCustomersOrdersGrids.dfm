inherited FormCustomersOrdersGrids: TFormCustomersOrdersGrids
  Caption = 'FormCustomersOrdersGrids '
  ClientHeight = 444
  ClientWidth = 481
  ExplicitWidth = 489
  ExplicitHeight = 478
  PixelsPerInch = 96
  TextHeight = 13
  inline FraCustomersTable1: TFraCustomersTable
    Left = 0
    Top = 0
    Width = 481
    Height = 217
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 481
    ExplicitHeight = 217
    inherited DBGridCustomers: TDBGrid
      Width = 481
      Height = 217
    end
  end
  inline FraOrdersGrid1: TFraOrdersGrid
    Left = 0
    Top = 217
    Width = 481
    Height = 227
    Align = alClient
    TabOrder = 1
    ExplicitTop = 217
    ExplicitWidth = 481
    ExplicitHeight = 96
    inherited DBGridOrders: TDBGrid
      Width = 481
      Height = 227
    end
    inherited TableOrders: TTable
      IndexName = 'CustNo'
      MasterFields = 'CustNo'
      MasterSource = FraCustomersTable1.DataSourceCustomers
    end
  end
end
