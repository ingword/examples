unit uFormCustomersOrdersGrids;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uFormMdiChildBase, uFraOrdersBase, uFraOrdersGrid, uFraBase,
  uFraBaseDB, uFraCustomersBase, uFraCustomersGrid, uFra1Button;

type
  TFormCustomersOrdersGrids = class(TFormMdiChildBase)
    FraCustomersTable1: TFraCustomersTable;
    FraOrdersGrid1: TFraOrdersGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
