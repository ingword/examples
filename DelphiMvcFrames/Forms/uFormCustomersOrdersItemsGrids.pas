unit uFormCustomersOrdersItemsGrids;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uFormMdiChildBase, uFraBase, uFraBaseDB, uFraCustomersBase,
  uFraCustomersGrid, uFraOrdersBase, uFraOrdersGrid, uFraItemsBase,
  uFraItemsGrid, ExtCtrls;

type
  TFormCustomersOrdersItemsGrids = class(TFormMdiChildBase)
    FraCustomersTable1: TFraCustomersTable;
    Panel1: TPanel;
    FraOrdersGrid1: TFraOrdersGrid;
    FraItemsGrid1: TFraItemsGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
