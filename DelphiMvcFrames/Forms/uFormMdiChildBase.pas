unit uFormMdiChildBase;

interface

uses
  uFormBase,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms;

type
  TFormMdiChildBase = class(TFormBase)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

  TFormMdiChildClass = class of TFormMdiChildBase;

implementation

{$R *.dfm}

procedure TFormMdiChildBase.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action:= caFree;
end;

end.
