unit uFraBaseDB;

interface

uses
  uFraBase,
  //uDataModule,
  DB,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  // class for all common DB operations with any real TDataSet descendant
  TFraBaseDB = class(TFraBase)
  private
    FDataSet: TDataSet;
    FAutoFindDataSet: boolean;
    procedure SetDataSet(const Value: TDataSet);
    procedure SetAutoFindDataSet(const Value: boolean);
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property DataSet:TDataSet read FDataSet write SetDataSet;

    procedure Init; override;

  published
    property AutoFindDataSet: boolean read FAutoFindDataSet write SetAutoFindDataSet default True;
  end;

// ***  delphi creates for frames such rows as below - just remove them ***
//var
//  FraBaseDB: TFraBaseDB;

implementation

{$R *.dfm}

{ TFraBaseDB }

constructor TFraBaseDB.Create(AOwner: TComponent);
begin
  inherited;
  AutoFindDataSet:=True; 
end;

procedure TFraBaseDB.Init;
var
  I: Integer;
begin
  inherited;
  // for readability DataSets can have differnt names, but all frames will have 1 main dataset
  if AutoFindDataSet then
    for I := 0 to ComponentCount - 1 do
      if components[i] is TDataSet then
      begin
        DataSet:= TDataSet(Components[i]);
        Break;
      end;

  if Assigned(DataSet)then
    DataSet.Open;
end;

procedure TFraBaseDB.SetAutoFindDataSet(const Value: boolean);
begin
  FAutoFindDataSet := Value;
end;

procedure TFraBaseDB.SetDataSet(const Value: TDataSet);
begin
  FDataSet := Value;
end;

end.
