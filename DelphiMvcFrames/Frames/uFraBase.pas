unit uFraBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TFraBase = class(TFrame)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AfterConstruction; override;
    // any initialization needed, but also you can use AfterConstruction (I just like shorter names ;)
    procedure Init; virtual;
  end;

implementation

{$R *.dfm}

{ TFraBase }

procedure TFraBase.AfterConstruction;
begin
  inherited;
  Init;
end;

procedure TFraBase.Init;
begin
  //
end;

end.
