program DelphiMvcFramesDemo;

uses
  Forms,
  Main in 'Main.pas' {MainForm},
  about in 'about.pas' {AboutBox},
  uDataModule in 'DB\uDataModule.pas' {DM: TDataModule},
  uFraBase in 'Frames\uFraBase.pas' {FraBase: TFrame},
  uFraBaseDB in 'Frames\uFraBaseDB.pas' {FraBaseDB: TFrame},
  uFraCustomersBase in 'Frames\Customers\uFraCustomersBase.pas' {FraCustomersBase: TFrame},
  uFraCustomersGrid in 'Frames\Customers\uFraCustomersGrid.pas' {FraCustomersTable: TFrame},
  uFraOrdersBase in 'Frames\Orders\uFraOrdersBase.pas' {FraOrdersBase: TFrame},
  uFraOrdersGrid in 'Frames\Orders\uFraOrdersGrid.pas' {FraOrdersGrid: TFrame},
  uFormBase in 'Forms\uFormBase.pas' {FormBase},
  uFormMdiChildBase in 'Forms\uFormMdiChildBase.pas' {FormMdiChildBase},
  uFormCustomersOrdersGrids in 'Forms\uFormCustomersOrdersGrids.pas' {FormCustomersOrdersGrids},
  uFraItemsBase in 'Frames\Items\uFraItemsBase.pas' {FraItemsBase: TFrame},
  uFraItemsGrid in 'Frames\Items\uFraItemsGrid.pas' {FraItemsGrid: TFrame},
  uFormCustomersOrdersItemsGrids in 'Forms\uFormCustomersOrdersItemsGrids.pas' {FormCustomersOrdersItemsGrids},
  uFra1Button in 'Frames\NonDB\uFra1Button.pas' {FraBase1: TFrame};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TDM, DM);
  Application.Run;
end.
