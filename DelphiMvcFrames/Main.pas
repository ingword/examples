unit MAIN;

interface

uses
  uFormMdiChildBase,
  Windows, SysUtils, Classes, Graphics,
  ImgList, Controls, StdActns, ActnList,
  Menus, ComCtrls, ToolWin, Forms;

type
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    FileNewItem: TMenuItem;
    Window1: TMenuItem;
    Help1: TMenuItem;
    N1: TMenuItem;
    FileExitItem: TMenuItem;
    WindowCascadeItem: TMenuItem;
    WindowTileItem: TMenuItem;
    WindowArrangeItem: TMenuItem;
    HelpAboutItem: TMenuItem;
    WindowMinimizeItem: TMenuItem;
    StatusBar: TStatusBar;
    ActionList1: TActionList;
    FileExit1: TAction;
    WindowCascade1: TWindowCascade;
    WindowTileHorizontal1: TWindowTileHorizontal;
    WindowArrangeAll1: TWindowArrange;
    WindowMinimizeAll1: TWindowMinimizeAll;
    HelpAbout1: TAction;
    WindowTileVertical1: TWindowTileVertical;
    WindowTileItem2: TMenuItem;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ImageList1: TImageList;
    ActOpenCustomersOrders: TAction;
    ActionOpenFormCustomersOrdersItemsGrids: TAction;
    ActOpenCustomersOrders1: TMenuItem;
    procedure HelpAbout1Execute(Sender: TObject);
    procedure FileExit1Execute(Sender: TObject);
    procedure ActOpenCustomersOrdersExecute(Sender: TObject);
    procedure ActionOpenFormCustomersOrdersItemsGridsExecute(Sender: TObject);
  private
    { Private declarations }
    procedure CreateChild(FormMdiChildClass:TFormMdiChildClass);
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses about, uFormCustomersOrdersGrids, uFormCustomersOrdersItemsGrids;

procedure TMainForm.ActionOpenFormCustomersOrdersItemsGridsExecute(
  Sender: TObject);
begin
  CreateChild(TFormCustomersOrdersItemsGrids);
end;

procedure TMainForm.ActOpenCustomersOrdersExecute(Sender: TObject);
begin
  CreateChild(TFormCustomersOrdersGrids);
end;

procedure TMainForm.CreateChild(FormMdiChildClass: TFormMdiChildClass);
begin
  FormMdiChildClass.Create(Application);
end;

procedure TMainForm.HelpAbout1Execute(Sender: TObject);
begin
  AboutBox.ShowModal;
end;

procedure TMainForm.FileExit1Execute(Sender: TObject);
begin
  Close;
end;

end.
