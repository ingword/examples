unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, GLWin32Viewer, GLCrossPlatform,
  GLBaseClasses, GLScene, GLObjects, GLCoordinates, GLCadencer, Vcl.StdCtrls,
  Vcl.ExtCtrls, GLRenderContextInfo;

type
  TForm6 = class(TForm)
    glscnMain: TGLScene;
    glViewMain: TGLSceneViewer;
    glcdcMain: TGLCadencer;
    glcmrMain: TGLCamera;
    glsrcMain: TGLLightSource;
    glcbFirst: TGLCube;
    glLineFirst: TGLLines;
    pnlBottom: TPanel;
    btnStart: TButton;
    glDirect1: TGLDirectOpenGL;
    procedure glcbFirstProgress(Sender: TObject; const deltaTime,
      newTime: Double);
    procedure btnStartClick(Sender: TObject);
    procedure glDirect1Render(Sender: TObject; var rci: TRenderContextInfo);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation
uses
 GLCanvas
 , glScreen
 ;

{$R *.dfm}

procedure TForm6.btnStartClick(Sender: TObject);
var
 MyGLSprite: TGLSprite;
begin
 MyGLSprite := TGLSprite(glcbFirst.AddNewChild(TGLSprite));
 MyGLSprite.Position.X := 3;
 MyGLSprite.Material.Texture.Image.LoadFromFile('test.png');
 MyGLSprite.Material.Texture.Disabled := False;
 MyGLSprite.Scale.SetVector(1.4, 1.4, 1.4);
 //MyGLSprite.Material.Texture.ImageClassName;
end;

procedure TForm6.glcbFirstProgress(Sender: TObject; const deltaTime,
  newTime: Double);
begin
{ glcbFirst.TurnAngle := glcbFirst.TurnAngle + deltaTime * 100;
 glcbFirst.CubeHeight := glcbFirst.CubeHeight + deltaTime * 5;

 if glcbFirst.CubeHeight > 10 then
  glcbFirst.CubeHeight := 1;}
end;

procedure TForm6.glDirect1Render(Sender: TObject; var rci: TRenderContextInfo);
var
 MyCanvas : TGLCanvas;
begin
 MyCanvas := TGLCanvas.Create(256, 256);
 MyCanvas.PenColor := clRed;
 MyCanvas.PenWidth := 3;
 MyCanvas.MoveTo(0,0);
 MyCanvas.Line(10, 10, 40, 40);

 MyCanvas.PenColor := clBlack;
 MyCanvas.PenWidth := 2;
 MyCanvas.MoveTo(40, 40);
 MyCanvas.LineTo(60, 60);

 MyCanvas.PenColor := clGreen;
 MyCanvas.PenWidth := 3;
 MyCanvas.Ellipse(100, 100, 50, 30);

 MyCanvas.PenColor := clGreen;
 MyCanvas.PenWidth := 3;
 MyCanvas.Ellipse(100, 100, 50, 30);

 FreeAndNil(myCanvas);
end;

end.
