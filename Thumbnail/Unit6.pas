unit Unit6;

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes, System.Win.ComObj,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ShlObj, ActiveX, Vcl.StdCtrls, Vcl.ExtCtrls,  Vcl.ComCtrls,
  System.ImageList, Vcl.ImgList;

type
  TForm6 = class(TForm)
    Image1: TImage;
    btnDo: TButton;
    lbl2: TLabel;
    ilMain: TImageList;
    lvMain: TListView;
    procedure btnDoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation

{$R *.dfm}

procedure TForm6.btnDoClick(Sender: TObject);
const
  IEIFLAG_ASPECT = 4;
  IEIFLAG_OFFLINE = 8;
  IEIFLAG_SCREEN = $20;
  IEIFLAG_ORIGSIZE = $40;

var
  FolderISF, DesktopISF: IShellFolder;
  IExtractImg: IExtractImage;
  Attrib, Eaten: DWORD;
  pItemIDL: PItemIDList;
  MemAlloc: IMalloc;
  CharBuf: array [0 .. 2047] of WideChar;
  Bmp: TBitmap;
  hBmp: HBITMAP;
  Size: TSize;
  Priority, Flags: Cardinal;
  GLResult: HResult;

begin
  if not((Win32Platform = VER_PLATFORM_WIN32_NT) and (Win32MajorVersion >= 5))
  then
  begin
    ShowMessage('This is NOT avalible in systems older than Win 2000');
    Exit;
  end;

  if (SHGetMalloc(MemAlloc) <> NOERROR) or (MemAlloc = nil) then
    Exit;;
  // we need IMAlloc interface to free allocated memory of Shell
  if NOERROR = SHGetDesktopFolder(DesktopISF) then
  begin
    OleCheck(DesktopISF.ParseDisplayName(0, nil, 'E:\temp\12', Eaten,
      // place your Folder name here
      pItemIDL, Attrib));
    DesktopISF.BindToObject(pItemIDL, nil, IShellFolder, FolderISF);
    // you must free all of the allocated shell memory of pItemIDL
    MemAlloc.Free(pItemIDL);
    OleCheck(FolderISF.ParseDisplayName(0, nil, 'Panorama 4.JPG', Eaten,
      // place your file name here
      pItemIDL, Attrib));
    FolderISF.GetUIObjectOf(0, 1, pItemIDL, IExtractImage, nil, IExtractImg);
    MemAlloc.Free(pItemIDL);
    // set the size of the thumbnail with the Size1
    Size.cx := 100; // thumbnail bitmap width
    Size.cy := 100; // thumnbail bitmap height, before crop

    Flags := // IEIFLAG_SCREEN // does not crop off bitmap
      IEIFLAG_ORIGSIZE // crops extra area off. may not work in win 2000
      or IEIFLAG_OFFLINE; // no online search for unknown video codex

    Priority := 0; // not sure is this has any effect

    GLResult := IExtractImg.GetLocation(CharBuf, sizeof(CharBuf), Priority,
      Size, 32 { ColorDepth } , Flags);
    if (GLResult = NOERROR) or (GLResult = E_PENDING) then
    begin
      Bmp := TBitmap.Create;
      try
        OleCheck(IExtractImg.Extract(hBmp));
        Bmp.Handle := hBmp;
        Image1.Picture.Assign(Bmp);
      finally
        Bmp.Free;
      end;
    end;
    lbl2.Caption := CharBuf;
  end;

end;

end.
