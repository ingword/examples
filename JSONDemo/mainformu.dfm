object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Creating and Reading JSON Objects'
  ClientHeight = 511
  ClientWidth = 670
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    670
    511)
  PixelsPerInch = 96
  TextHeight = 13
  object WriteJSONObjects: TButton
    Left = 8
    Top = 24
    Width = 174
    Height = 25
    Caption = 'Create Simple JSON Object'
    TabOrder = 0
    OnClick = WriteJSONObjectsClick
  end
  object Memo1: TMemo
    Left = 188
    Top = 26
    Width = 468
    Height = 180
    Anchors = [akLeft, akTop, akRight]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object WriteJSONNumbers: TButton
    Left = 8
    Top = 55
    Width = 174
    Height = 26
    Caption = 'Create JSON Numbers'
    TabOrder = 2
    OnClick = WriteJSONNumbersClick
  end
  object WriteJSONTrueFalseNull: TButton
    Left = 8
    Top = 87
    Width = 174
    Height = 25
    Caption = 'Create JSON true, false, and null'
    TabOrder = 3
    OnClick = WriteJSONTrueFalseNullClick
  end
  object WriteJSONArrays: TButton
    Left = 8
    Top = 118
    Width = 174
    Height = 25
    Caption = 'Create JSON Arrays'
    TabOrder = 4
    OnClick = WriteJSONArraysClick
  end
  object Memo2: TMemo
    Left = 188
    Top = 223
    Width = 468
    Height = 273
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 5
  end
  object ReadJSONObject: TButton
    Left = 8
    Top = 223
    Width = 174
    Height = 25
    Caption = 'Read a JSON Object'
    TabOrder = 6
    OnClick = ReadJSONObjectClick
  end
  object ReadJSONNumbers: TButton
    Left = 8
    Top = 254
    Width = 174
    Height = 25
    Caption = 'Read JSON Numbers'
    TabOrder = 7
    OnClick = ReadJSONNumbersClick
  end
  object ReadJSONTrueFalseNull: TButton
    Left = 8
    Top = 285
    Width = 174
    Height = 25
    Caption = 'Read JSON true, false, and null'
    TabOrder = 8
    OnClick = ReadJSONTrueFalseNullClick
  end
  object ReadJSONArrays: TButton
    Left = 8
    Top = 316
    Width = 174
    Height = 25
    Caption = 'Read JSON Arrays'
    TabOrder = 9
    OnClick = ReadJSONArraysClick
  end
  object ReadGenericJSON: TButton
    Left = 8
    Top = 347
    Width = 174
    Height = 25
    Caption = 'Read Generic JSON'
    TabOrder = 10
    OnClick = ReadGenericJSONClick
  end
  object Button1: TButton
    Left = 8
    Top = 378
    Width = 174
    Height = 25
    Caption = 'Read Generic JSON'
    TabOrder = 11
    OnClick = ReadGenericJSONClick
  end
  object Button2: TButton
    Left = 8
    Top = 409
    Width = 174
    Height = 25
    Caption = 'Read Generic JSON'
    TabOrder = 12
    OnClick = ReadGenericJSONClick
  end
  object DefaultMarshalToJSON: TButton
    Left = 8
    Top = 149
    Width = 174
    Height = 25
    Caption = 'Marshal TDataObject to JSON'
    TabOrder = 13
    OnClick = DefaultMarshalToJSONClick
  end
  object MarshalWithConverter: TButton
    Left = 8
    Top = 180
    Width = 174
    Height = 25
    Caption = 'Marshal With Converter'
    TabOrder = 14
    OnClick = MarshalWithConverterClick
  end
  object DefaultUnmarshalToObject: TButton
    Left = 8
    Top = 440
    Width = 174
    Height = 25
    Caption = 'Unmarshal JSON to TDataObject'
    TabOrder = 15
    OnClick = DefaultUnmarshalToObjectClick
  end
  object UnmarshalWithReverter: TButton
    Left = 8
    Top = 471
    Width = 174
    Height = 25
    Caption = 'Unmarshal With Reverter'
    TabOrder = 16
    OnClick = UnmarshalWithReverterClick
  end
end
