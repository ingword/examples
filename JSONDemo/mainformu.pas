unit mainformu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, StdCtrls, strUtils;

type
  TForm1 = class(TForm)
    WriteJSONObjects: TButton;
    Memo1: TMemo;
    WriteJSONNumbers: TButton;
    WriteJSONTrueFalseNull: TButton;
    WriteJSONArrays: TButton;
    Memo2: TMemo;
    ReadJSONObject: TButton;
    ReadJSONNumbers: TButton;
    ReadJSONTrueFalseNull: TButton;
    ReadJSONArrays: TButton;
    ReadGenericJSON: TButton;
    Button1: TButton;
    Button2: TButton;
    DefaultMarshalToJSON: TButton;
    MarshalWithConverter: TButton;
    DefaultUnmarshalToObject: TButton;
    UnmarshalWithReverter: TButton;
    procedure WriteJSONObjectsClick(Sender: TObject);
    procedure WriteJSONNumbersClick(Sender: TObject);
    procedure WriteJSONTrueFalseNullClick(Sender: TObject);
    procedure WriteJSONArraysClick(Sender: TObject);
    procedure ReadJSONObjectClick(Sender: TObject);
    procedure ReadJSONNumbersClick(Sender: TObject);
    procedure ReadJSONTrueFalseNullClick(Sender: TObject);
    procedure ReadJSONArraysClick(Sender: TObject);
    procedure ReadGenericJSONClick(Sender: TObject);
    procedure DefaultMarshalToJSONClick(Sender: TObject);
    procedure MarshalWithConverterClick(Sender: TObject);
    procedure DefaultUnmarshalToObjectClick(Sender: TObject);
    procedure UnmarshalWithReverterClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TDataObject = class
  private
    FOne: String;
    FTwo: Integer;
    FTimeCreated: TDateTime;
  protected
    FData: String;
  public
    property One: String read FOne write FOne;
    property Two: Integer read FTwo write FTwo;
    constructor Create;
  end;

var
  Form1: TForm1;

implementation

uses //DBXJSON,         // For the base JSON classes
     DBXJSONReflect,  // For the marshaling and unmarshaling classes
     RTTI,
     JSON;            // For the new runtime type information

{$R *.dfm}

{ TDataObject }


constructor TDataObject.Create;
begin
  FData := 'Delphi';
  FOne := 'One';
  FTwo := 2;
  FTimeCreated := now();
end;

procedure TForm1.WriteJSONObjectsClick(Sender: TObject);
var
  jo: TJSONObject;
begin
  Memo1.Clear;
  try
    jo := TJSONObject.Create;
    jo.AddPair(TJSONPair.Create('Delphi',
      TJSONString.Create('Supports JSON')));
    Memo1.Lines.Add(jo.ToString);
  finally
    jo.Free;
  end;
end;

procedure TForm1.WriteJSONNumbersClick(Sender: TObject);
 var
  jo: TJSONObject;
  jp: TJSONPair;
begin
  Memo1.Clear;
  try
    jo := TJSONObject.Create;
    jp := TJSONPair.Create('Integer', TJSONNumber.Create(10));
    jo.AddPair(jp);
    jo.AddPair(TJSONPair.Create('Float', TJSONNumber.Create('1001.23')));
    jo.AddPair(TJSONPair.Create('Exponential',TJSONNumber.Create(5.1e3)));
    Memo1.Lines.Add(jo.ToString);
  finally
    jo.Free;
  end;
end;

procedure TForm1.WriteJSONTrueFalseNullClick(Sender: TObject);
 var
  jo: TJSONObject;
begin
  Memo1.Clear;
  try
    jo := TJSONObject.Create(TJSONPair.Create('True', TJSONTrue.Create));
    jo.AddPair(TJSONPair.Create('False', TJSONFalse.Create));
    jo.AddPair(TJSONPair.Create('Null',TJSONNull.Create));
    Memo1.Lines.Add(jo.ToString);
  finally
    jo.Free;
  end;
end;

procedure TForm1.WriteJSONArraysClick(Sender: TObject);
var
  ja: TJSONArray;
  jo: TJSONObject;
begin
  Memo1.Clear;
  ja := TJSONArray.Create;
  try
    ja.Add(TJSONObject.Create(TJSONPair.Create('Delphi','Supports JSON')));
    ja.Add('Hello World');
    ja.Add(TJSONArray.Create(TJSONNumber.Create(1),TJSONNumber.Create(2)));
    (ja.Get(2) as TJSONArray).Add(3);
    ja.Add(true);
    ja.AddElement(TJSONFalse.Create);
    ja.AddElement(TJSONNull.Create);
    ja.AddElement(TJSONNumber.Create('-5.243e5'));
  except
    ja.Free;
    raise;
  end;
  jo := TJSONObject.Create(TJSONPair.Create('result', ja));
  jo.AddPair('One More','Last Pair');
  try
     Memo1.Lines.Add(jo.ToString);
  finally
    jo.Free;
  end;
end;

procedure TForm1.ReadJSONObjectClick(Sender: TObject);
var
  jo: TJSONObject;
  jp: TJSONPair;
begin
  Memo2.Clear;
  WriteJSONObjectsClick(nil);
  jo := TJSONObject.ParseJSONValue(Memo1.Lines.Text) as TJSONObject;
  try
  jp := jo.Get(0);
  Memo2.Lines.Add('ToString: ' + jp.ToString);
  Memo2.Lines.Add('JsonString: ' + jp.JsonString.Value);
  Memo2.Lines.Add('JsonValue: ' + jp.JsonValue.Value);
  finally
     jo.Free;
  end;
end;

procedure TForm1.ReadJSONNumbersClick(Sender: TObject);
var
  jo: TJSONObject;
  jp: TJSONPair;
  jn: TJSONNumber;
begin
  Memo2.Clear;
  WriteJSONNumbersClick(nil);
  jo := TJSONObject.ParseJSONValue(Memo1.Lines.Text) as TJSONObject;
  try
  jp := jo.Get(0);
  Memo2.Lines.Add('ToString: ' + jp.ToString);
  Memo2.Lines.Add('JsonString: ' + jp.JsonString.Value);
  Memo2.Lines.Add('JsonValue: ' + jp.JsonValue.Value);
  if jo.Get('Float') <> nil then
  begin
    jp := jo.Get('Float');
    Memo2.Lines.Add('ToString: ' + jp.ToString);
    Memo2.Lines.Add('JsonString: ' + jp.JsonString.Value);
    jn := jp.JsonValue as TJSONNumber;
    Memo2.Lines.Add('JsonValue: ' + FloatToStr(jn.AsDouble));
  end;
  finally
     jo.Free;
  end;
end;

procedure TForm1.ReadJSONTrueFalseNullClick(Sender: TObject);
var
  i: Integer;
  jo: TJSONObject;
  jp: TJSONPair;
begin
  Memo2.Clear;
  WriteJSONTrueFalseNullClick(nil);
  jo := TJSONObject.ParseJSONValue(Memo1.Lines.Text) as TJSONObject;
  try
    for i := 0 to jo.Size - 1 do
    begin
      jp := jo.Get(i);
      Memo2.Lines.Add('ToString: ' + jp.ToString);
      Memo2.Lines.Add('JsonString: ' + jp.JsonString.Value);
      if (jp.JSONValue is TJSONTrue) or (jp.JSONValue is TJSONFalse) or (jp.JSONValue is TJSONNull) then
        Memo2.Lines.Add('JsonValue: ' + jp.JsonValue.ToString)
      else
        Memo2.Lines.Add('JsonValue: ' + jp.JsonValue.Value)
    end;
  finally
     jo.Free;
  end;
end;

procedure TForm1.UnmarshalWithReverterClick(Sender: TObject);
var
  jo: TJSONValue;
  UnMarshal: TJSONUnMarshal;
  DataObject: TDataObject;
begin
  Memo2.Clear;
  jo := TJSONObject.ParseJSONValue(Memo1.Lines.Text) as TJSONObject;
  try
    UnMarshal := TJSONUnMarshal.Create;
    try
      Unmarshal.RegisterReverter(TDataObject, 'FTimeCreated',
        procedure(Data: TObject; Field: string; Arg: string)
        var
          RTTIContext: TRTTIContext;
          RTTIField: TRTTIField;
          TimeCreated : TDateTime;
        begin
          TimeCreated := StrToDateTime(arg);
          RTTIField := RTTIContext.GetType(Data.ClassType).GetField(Field);
          RTTIField.SetValue(Data, TimeCreated);
        end);
      DataObject := TDataObject(UnMarshal.Unmarshal(jo));
      try
        Memo2.Lines.Add('One: ' + DataObject.One);
        Memo2.Lines.Add('Two: ' + IntToStr(DataObject.Two));
        Memo2.Lines.Add('FData: ' + DataObject.FData);
        Memo2.Lines.Add('FTimeCreated: ' +
          FormatDateTime('c', DataObject.FTimeCreated));
        Memo2.Lines.Add('FTimeCreated as Extended ' +
          FloatToStr(DataObject.FTimeCreated));
       finally
         DataObject.Free;
       end;
    finally
      UnMarshal.Free;
    end;
  finally
    jo.Free;
  end;
end;

procedure TForm1.ReadJSONArraysClick(Sender: TObject);
var
  i, j: Integer;
  jo: TJSONObject;
  jp: TJSONPair;
  ja: TJSONArray;
begin
  WriteJSONArraysClick(nil);
  jo := TJSONObject.ParseJSONValue(Memo1.Lines.Text) as TJSONObject;
  try
    for i := 0 to jo.Size - 1 do
    begin
      jp := jo.Get(i);
      if jp.JsonValue is TJSONArray then
      begin
        Memo2.Lines.Add('JSONArray start');
        ja := jp.JsonValue as TJSONArray;
        for j := 0 to ja.Size - 1 do
          Memo2.Lines.Add(ja.Get(i).ClassName + ': ' + ja.Get(j).ToString);
        Memo2.Lines.Add('JSONArray end');
      end
      else
        Memo2.Lines.Add(jp.ClassName + ': ' + jp.ToString);
    end;
  finally
     jo.Free;
  end;
end;

procedure TForm1.DefaultMarshalToJSONClick(Sender: TObject);
var
  jo: TJSONValue;
  Marshal: TJSONMarshal;
  DataObject: TDataObject;
begin
  DataObject := TDataObject.Create;
  try
    Marshal := TJSONMarshal.Create(TJSONConverter.Create);
    try
      jo := Marshal.Marshal(DataObject);
      try
        Memo1.Lines.Text := jo.ToString;
      finally
        jo.Free;
      end;
    finally
      Marshal.Free;
    end;
  finally
    DataObject.Free;
  end;
end;

procedure TForm1.MarshalWithConverterClick(Sender: TObject);
var
  jo: TJSONValue;
  Marshal: TJSONMarshal;
  DataObject: TDataObject;
begin
  DataObject := TDataObject.Create;
  try
    Marshal := TJSONMarshal.Create(TJSONConverter.Create);
    try
      Marshal.RegisterConverter(TDataObject, 'FTimeCreated',
      function (Data: TObject; Field: string): string
      begin
        Result := FormatDateTime('c', TDataObject(Data).FTimeCreated);
      end);
      jo := Marshal.Marshal(DataObject);
      try
        Memo1.Lines.Text := jo.ToString;
      finally
        jo.Free;
      end;
    finally
      Marshal.Free;
    end;
  finally
    DataObject.Free;
  end;
end;

procedure TForm1.DefaultUnmarshalToObjectClick(Sender: TObject);
var
  jo: TJSONValue;
  UnMarshal: TJSONUnMarshal;
  DataObject: TDataObject;
begin
  Memo2.Clear;
  jo := TJSONObject.ParseJSONValue(Memo1.Lines.Text) as TJSONObject;
  try
    UnMarshal := TJSONUnMarshal.Create;
    try
      DataObject := TDataObject(UnMarshal.Unmarshal(jo));
      try
        Memo2.Lines.Add('One: ' + DataObject.One);
        Memo2.Lines.Add('Two: ' + IntToStr(DataObject.Two));
        Memo2.Lines.Add('FData: ' + DataObject.FData);
        Memo2.Lines.Add('FTimeCreate: ' +
          FormatDateTime('c', TDateTime(DataObject.FTimeCreated)));
       finally
         DataObject.Free;
       end;
    finally
      UnMarshal.Free;
    end;
  finally
    jo.Free;
  end;
end;

procedure TForm1.ReadGenericJSONClick(Sender: TObject);
var
  jo: TJSONObject;
  Indent: string;
const
  IndentSize = 2;

  procedure ParseJSONPairs(jo: TJSONObject); forward;

  procedure Indentation(Increase: Boolean);
  begin
   if Increase then
     Indent := Indent +  '  '
   else
     Indent := copy(Indent, 1, Length(Indent) - IndentSize);
  end;

  procedure WriteJSONValue(ancestor: TJSONAncestor);
  var
    jp: TJSONPair;
    jv: TJSONValue;
  begin
    Memo2.Lines.Add(Indent + ancestor.ClassName);
    if ancestor is TJSONPair then
    begin
      jp := TJSONPair(ancestor);
      Memo2.Lines.Add(Indent + 'ToString: ' + jp.ToString);
      Memo2.Lines.Add(Indent + 'JsonString: ' + jp.JsonString.Value);
      if (jp.JSONValue is TJSONTrue) or (jp.JSONValue is TJSONFalse) or (jp.JSONValue is TJSONNull) then
        Memo2.Lines.Add(Indent + 'JsonValue: ' + jp.JsonValue.ToString)
      else
        Memo2.Lines.Add(Indent + 'JsonValue: ' + jp.JsonValue.Value)
    end

    else
    begin
      jv := TJSONValue(ancestor);
      Memo2.Lines.Add(Indent + 'ToStrint: ' + jv.ToString);
      Memo2.Lines.Add(Indent + 'Value: ' + jv.Value);
    end;
  end;

  procedure ParseJSONArray(ja: TJSONArray);
  var
    i: Integer;
  begin
    for i := 0 to ja.Size -1 do
    begin
      if ja.Get(i) is TJSONArray then
      begin
        Memo2.Lines.Add(Indent + 'JSONArray');
        Indentation(True);
        ParseJSONArray(ja.Get(i) as TJSONArray);
        Indentation(False);
      end
      else if ja.Get(i) is TJSONObject then
      begin
        Memo2.Lines.Add(Indent + 'JSONObject');
        Indentation(True);
        ParseJSONPairs(ja.Get(i) as TJSONObject);
        Indentation(False);
      end
      else
        WriteJSONValue(ja.Get(i));
    end;
  end;

  procedure ParseJSONPairs(jo: TJSONObject);
  var
    i: Integer;
    jv: TJSONValue;
  begin
    for i := 0 to jo.Size - 1 do
    begin
      jv := jo.Get(i).JsonValue;
      if jv is TJSONOBject then
      begin
        Memo2.Lines.Add(Indent + 'TJSONObject');
        Indentation(True);
        ParseJSONPairs(jv as TJSONObject);
        Indentation(False);
      end
      else if jv is TJSONArray then
      begin
        Memo2.Lines.Add(Indent + 'TJSONArray');
        Indentation(True);
        ParseJSONArray(jv as TJSONArray);
        Indentation(False);
      end
      else
        WriteJSONValue(jo.Get(i));
    end;
  end;

begin
  if Memo1.Lines.Text = '' then
  begin
    ShowMessage('Display a JSON object in memo and try again');
    exit;
  end;
  Indent := '';
  Memo2.Clear;
  jo := TJSONObject.ParseJSONValue(Memo1.Lines.Text) as TJSONObject;
  try
  ParseJSONPairs(jo);
  finally
    jo.Free;
  end;
end;

end.
