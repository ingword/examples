unit JSONCDS;

interface

uses classes, sysutils, DBXJSON, db, typinfo, DBClient,
  IdBaseComponent, IdCoder, IdCoder3to4, IdCoderMIME, dialogs, json, IdGlobal;

///<summary>
///  This function is designed to accept any dataset and
///  create a JSON object from it. the JSON object
///  consists of two arrays of arrays
///  The first array, named meta, contains one array
///  for each field in the dataset, and will contain at least
///  one element, the data type. If the datatype is either
///  ftString, ftBDC, ftDecimal, a second element will
///  contain the size. If the type is a ftBDC, ftDecimal,
///  etc, it will contain a third field indicating precision.
///  This routine does not support all TField types (yet)
///  </summary>
function DataSetToJSON(DataSet: TDataSet): TJSONObject;

///<summary>
///  This function is designed to accept any JSON object
///  created using the DataSetToJSON function, and return
///  a TClientDataSet whose meta data is based on the data
///  in the meta array, and whose contents is based on the
///  data in the data array.
///  The returned ClientDataSet is owned by AOwner (which can be nil)
///  and the JSON object is not destroyed. It is the reponsibility of the
///  caller to free the JSON object.
///  If the JSON object was created by calling a Delphi REST Web method, that
///  JSON object appears as the first element of a JSONArray stored as the
///  JSONValue of the result JSONPair.
///  Call GetJSONObjectFromDelphiJSONREST to extract the JSON object
///  from the result JSONPair.
///  This routine does not support all TField types (yet)
///  </summary>
function JSONToClientDataSet(JSON: TJSONObject; AOwner: TComponent): TClientDataSet;

implementation

function DataSetToJSON(DataSet: TDataSet): TJSONObject;
var
  meta, data: TJSONArray;
  i: Integer;

  function CreateMetaArray(Meta: TJSONArray; DataSet: TDataSet): TJSONArray;
  var
    i: Integer;
    innermeta: TJSONArray;
    jo: TJSONObject;
    metarow: TJSONArray;
 begin
    Result := Meta;
    //For each field in the dataset
   for i := 0 to DataSet.FieldDefs.Count - 1 do
   begin
     //Create an array for that column's meta data
//     showMessage(DataSet.Fields[i].FieldName);
     metarow := TJSONArray.Create;
     metarow.Add(DataSet.Fields[i].FieldName);
     metarow.Add(GetEnumName(TypeInfo(TFieldType),Ord(DataSet.Fields[i].DataType)));
     //Some field types will need additional information
     case DataSet.FieldDefs[i].DataType of
       ftString, ftWideString:
         metarow.Add(DataSet.FieldDefs[i].Size);
       ftbcd:
         begin
           metarow.Add(DataSet.FieldDefs[i].Size);
           metarow.Add(DataSet.FieldDefs[i].Precision);
         end;
       ftDataSet:
         begin
           metaRow.Add(CreateMetaArray(TJSONArray.Create, TDataSetField(DataSet.Fields[i]).NestedDataSet));
         end;
     end;
     //Add the field meta data to the meta JSONArray
    // ShowMessage(metarow.tostring);
     meta.Add(metarow);
   end;
  end;

  function CreateDataArray(data: TJSONArray; DataSet: TDataSet): TJSONArray;
  var
    i: Integer;
    datarow: TJSONArray;
    js: TJSONString;
  begin
    Result := data;
    //now add the data rows
    DataSet.First;
    //For each record
    while not DataSet.EOF do
    begin
      //for each column
      datarow := TJSONArray.Create;
      for i := 0 to DataSet.Fields.Count - 1 do
      if DataSet.FieldDefs[i].DataType = ftDataSet then
        datarow.Add(CreateDataArray(TJSONArray.Create, TDataSetField(DataSet.Fields[i]).NestedDataSet))
      else
      if (DataSet.FieldDefs[i].DataType = ftGraphic) or (DataSet.FieldDefs[i].DataType = ftBlob) then
      begin
      begin
        js := TJSONString.Create(TIdEncoderMime.EncodeBytes(TIdBytes(DataSet.Fields[i].AsBytes)));
        ShowMessage(js.ToString);
        //Need some sort of encoding. The following line does not work
        ShowMessage(datarow.ToString);
        datarow.Add((TIdEncoderMime.EncodeBytes(TIdBytes(DataSet.Fields[i].AsBytes))));
        ShowMessage(datarow.ToString);
      end;
      end
      else
        datarow.Add(DataSet.Fields[i].AsString); //add the data
      //Insert this record into the data JSONArray
      data.Add(datarow);
      DataSet.Next;
    end;
  end;

begin
  //Create the JSON object
  Result := TJSONObject.Create;
  //Create the meta data and data arrays
  meta := TJSONArray.Create;
  CreateMetaArray(meta, Dataset);
  data := TJSONArray.Create;
  CreateDataArray(data, DataSet);
  //wrap it up. Stuff the two JSONArrays into the TJSONObject
  Result.AddPair('meta', meta);
  Result.AddPair('data', data);
end;

function JSONToClientDataSet(JSON: TJSONObject; AOwner: TComponent): TClientDataSet;
var
  meta, data: TJSONArray;
  i, j: Integer;
//  metarow: TJSONArray;
//  datarow: TJSONArray;

  procedure DefineMetaData(meta: TJSONArray; FieldDefs: TFieldDefs);
  var
    i: Integer;
    cds: TClientDataSet;
    s: string;
    metarow: TJSONArray;
  begin
    //Get the data
    for i := 0 to meta.Size - 1 do
    begin
      //For each field
      metarow := meta.Get(i) as TJSONArray;
      //configure the CDS FieldDefs
      if metarow.Get(1).Value = 'ftBCD' then
      begin
        with FieldDefs.AddFieldDef do
        begin
          s := metarow.Get(0).Value;
          Name := metarow.Get(0).Value;
          DataType := TFieldType(GetEnumValue(TypeInfo(TFieldType), metarow.Get(1).Value));
          Size := StrToInt(metarow.Get(2).Value);
          Precision := StrToInt(metarow.Get(3).Value);
        end;
      end
      else if (metarow.get(1).value = 'ftString') or (metarow.get(1).value = 'ftWideString') then
        FieldDefs.Add(metarow.Get(0).Value, TFieldType(GetEnumValue(TypeInfo(TFieldType), metarow.Get(1).Value)), StrToInt(metarow.Get(2).value))
      else if metarow.Get(1).Value = 'ftDataSet' then
      begin
        with FieldDefs.AddFieldDef do
        begin
          Name := metarow.Get(0).Value;
          DataType := ftDataSet;
          DefineMetaData(metarow.Get(2) as TJSONArray, ChildDefs);
        end;
      end

      else
        FieldDefs.Add(metarow.Get(0).Value, TFieldType(GetEnumValue(TypeInfo(TFieldType), metarow.Get(1).Value)));
    end;
  end;

  procedure InsertData(Data: TJSONArray; DataSet: TDataSet);
  var
    i, j: Integer;
    datarow: TJSONArray;
    s: string;
  begin

    for i := 0 to data.Size - 1 do
    begin
      DataSet.Append;
      datarow := data.Get(i) as TJSONArray;
      for j := 0 to datarow.Size - 1 do
      begin
        if (DataSet.Fields[j] is TDateField) or (DataSet.fields[j] is TDateTimeField) then
        begin
          if datarow.Get(j).Value <> '' then
            DataSet.Fields[j].AsDateTime := StrToDateTime(datarow.Get(j).Value)
        end
        else
        if DataSet.Fields[j] is TIntegerField then
        begin
          if datarow.Get(j).Value <> '' then
            DataSet.Fields[j].AsInteger := StrToint(datarow.Get(j).Value)
        end
        else
        if  DataSet.Fields[j] is TFloatField then
        begin
          if datarow.Get(j).Value <> '' then
            DataSet.Fields[j].AsFloat := StrToFloat(datarow.Get(j).Value)
        end
        else
        if (DataSet.Fields[j] is TBlobField) or (DataSet.fields[j] is TGraphicField) then
        begin
           //This isn't working, since the encoding is not working.
          DataSet.Fields[j].AsBytes := TBytes(TIdDecoderMIME.DecodeBytes(datarow.Get(j).Value))
        end
        else
        if DataSet.Fields[j] is TDataSetField then
        begin
          InsertData(datarow.Get(j) as TJSONArray, TDataSetField(DataSet.Fields[j]).NestedDataSet)
        end
        else
        begin
          if datarow.Get(j).Value <> '' then
            DataSet.Fields[j].Value := datarow.Get(j).Value;  //Fields[j].Value is a variant. Delphi should take care of the data translation
        end;
      end;
      DataSet.Post;
    end;
  end;

begin
  Result := TClientDataSet.Create(AOwner);
  //Get the meta data
  meta := json.Get('meta').JSONValue as TJSONArray;
  DefineMetaData(meta, Result.FieldDefs);

  //Create the ClientDataSet
  Result.CreateDataSet;
  Result.LogChanges := False;
  //And populate it
  data := json.Get('data').JSONValue as TJSONArray;
  InsertData(data, Result);

  //Return the cds to its first record
  Result.First;
end;

end.

