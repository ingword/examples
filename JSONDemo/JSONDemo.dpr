program JSONDemo;

uses
  Forms,
  mainformu in 'mainformu.pas' {Form1},
  JSONCDS in 'JSONCDS.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
