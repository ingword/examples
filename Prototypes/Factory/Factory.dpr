program Factory;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  u_Factory in 'u_Factory.pas';

function SomeCondition(const aSomeDataToDecide: TSomeDataToDecide) : Boolean;
begin
 Result:= True;
end;

function SomeOtherCondition(const aSomeDataToDecide: TSomeDataToDecide) : Boolean;
begin
 Result:= True;
end;

var
 l_BaseClassFactoryFactory : RBaseClassFactoryFactory;
 l_BaseClass : TBaseClass;
 l_SomeDataToDecide : TSomeDataToDecide;
 SomeParams : integer;
begin
 SomeParams := 1;
 try
  l_SomeDataToDecide := TSomeDataToDecide.Create(SomeParams);
   if SomeCondition(l_SomeDataToDecide) then
    l_BaseClassFactoryFactory := TFactoryFactory1
   else
   if SomeOtherCondition(l_SomeDataToDecide) then
    l_BaseClassFactoryFactory := TFactoryFactory2
   else
   begin
    Assert(false);
    l_BaseClassFactoryFactory := nil;
   end;

   l_BaseClass := l_BaseClassFactoryFactory.Make(l_SomeDataToDecide).Make(l_BaseClassFactoryFactory.Used(l_SomeDataToDecide), l_SomeDataToDecide);
   try
    Writeln(l_BaseClass.ClassName);
    Readln;
   finally
    FreeAndNil(l_BaseClass);
   end;//try..finally

 except
   on E: Exception do
     Writeln(E.ClassName, ': ', E.Message);
 end;
end.
