unit u_Factory;

interface

type
 TSomeDataToDecide = record
  fI : integer;
  constructor Create(aI : Integer);
 end;//TSomeDataToDecide

 TUsedBaseClass = class abstract (TObject)
  private
   FSomeDataToDecide : TSomeDataToDecide;
  public
   constructor Create(const aSomeDataToDecide: TSomeDataToDecide);
 end;//TUsedBaseClass

 RUsedBaseClass = class of TUsedBaseClass;

 TBaseClass = class abstract (TObject)
  private
   f_Used : TUsedBaseClass;
  public
   constructor Create(aUsed: RUsedBaseClass; const aSomeDataToDecide: TSomeDataToDecide);
 end;//TBaseClass

 TUsedImpl1 = class(TBaseClass)
 end;//TUsedImpl1

 TUsedImpl2 = class(TBaseClass)
 end;//TUsedImpl2

 TImpl1 = class(TBaseClass)
 end;//TImpl1

 TImpl2 = class(TBaseClass)
 end;//TImpl2

 TBaseClassFactory = class abstract (TObject)
  public
   class function Make(aUsed: RUsedBaseClass; const aSomeDataToDecide: TSomeDataToDecide): TBaseClass; virtual; abstract;
 end;//TBaseClassFactory

 RBaseClassFactory = class of TBaseClassFactory;

 TFactory1 = class(TBaseClassFactory)
  public
   class function Make(aUsed: RUsedBaseClass; const aSomeDataToDecide: TSomeDataToDecide): TBaseClass; override;
 end;//TFactory1

 TFactory2 = class(TBaseClassFactory)
  public
   class function Make(aUsed: RUsedBaseClass; const aSomeDataToDecide: TSomeDataToDecide): TBaseClass; override;
 end;//TFactory2

 TBaseClassFactoryFactory = class abstract (TObject)
  public
   class function Make(const aSomeDataToDecide: TSomeDataToDecide): RBaseClassFactory; virtual; abstract;
   class function Used(const aSomeDataToDecide: TSomeDataToDecide): RUsedBaseClass; virtual; abstract;
 end;//TBaseClassFactoryFactory

 RBaseClassFactoryFactory = class of TBaseClassFactoryFactory;

 TFactoryFactory1 = class (TBaseClassFactoryFactory)
  public
   class function Make(const aSomeDataToDecide: TSomeDataToDecide): RBaseClassFactory; override;
   class function Used(const aSomeDataToDecide: TSomeDataToDecide): RUsedBaseClass; override;
 end;//T1FactoryFactory1

 TFactoryFactory2 = class (TBaseClassFactoryFactory)
  public
   class function Make(const aSomeDataToDecide: TSomeDataToDecide): RBaseClassFactory; override;
   class function Used(const aSomeDataToDecide: TSomeDataToDecide): RUsedBaseClass; override;
 end;//T1FactoryFactory2

implementation

function SomeCondition(const aSomeDataToDecide: TSomeDataToDecide) : Boolean;
begin
 Result:= True;
end;

function SomeOtherCondition(const aSomeDataToDecide: TSomeDataToDecide) : Boolean;
begin
 Result:= True;
end;

constructor TBaseClass.Create(aUsed: RUsedBaseClass; const aSomeDataToDecide: TSomeDataToDecide);
begin
 if not Assigned(f_Used) then
  f_Used := aUsed.Create(aSomeDataToDecide);
end;

class function TFactory1.Make(aUsed: RUsedBaseClass; const aSomeDataToDecide: TSomeDataToDecide): TBaseClass;
begin
 if SomeCondition(aSomeDataToDecide) then
  Result := TImpl1.Create(aUsed, aSomeDataToDecide)
 else
 if SomeOtherCondition(aSomeDataToDecide) then
  Result := TImpl2.Create(aUsed, aSomeDataToDecide)
 else
 begin
  Assert(false);
  Result := nil;
 end;
end;

class function TFactory2.Make(aUsed: RUsedBaseClass; const aSomeDataToDecide: TSomeDataToDecide): TBaseClass;
begin
 if SomeCondition(aSomeDataToDecide) then
  Result := TImpl2.Create(aUsed, aSomeDataToDecide)
 else
 if SomeOtherCondition(aSomeDataToDecide) then
  Result := TImpl1.Create(aUsed, aSomeDataToDecide)
 else
 begin
  Assert(false);
  Result := nil;
 end;
end;

class function TFactoryFactory1.Make(const aSomeDataToDecide: TSomeDataToDecide): RBaseClassFactory;
begin
 if SomeCondition(aSomeDataToDecide) then
  Result := TFactory1
 else
 if SomeOtherCondition(aSomeDataToDecide) then
  Result := TFactory2
 else
 begin
  Assert(false);
  Result := nil;
 end;
end;

class function TFactoryFactory1.Used(const aSomeDataToDecide: TSomeDataToDecide): RUsedBaseClass;
begin
 Writeln(self.ClassName + ' Used');
end;

class function TFactoryFactory2.Make(const aSomeDataToDecide: TSomeDataToDecide): RBaseClassFactory;
begin
 if SomeCondition(aSomeDataToDecide) then
  Result := TFactory2
 else
 if SomeOtherCondition(aSomeDataToDecide) then
  Result := TFactory1
 else
 begin
  Assert(false);
  Result := nil;
 end;
end;

{ TSomeDataToDecide }

constructor TSomeDataToDecide.Create(aI: Integer);
begin
 fI := aI;
end;

{ TUsedBaseClass }

constructor TUsedBaseClass.Create(const aSomeDataToDecide: TSomeDataToDecide);
begin
 fSomeDataToDecide := aSomeDataToDecide;
end;

class function TFactoryFactory2.Used(const aSomeDataToDecide: TSomeDataToDecide): RUsedBaseClass;
begin
 Writeln(self.ClassName + ' Used');
end;

end.
