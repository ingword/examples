float4x4 VPM    : register(c0);
float3 LightPos : register(c4);

struct appdata {
       float4 Position : POSITION;
       float2 UV       : TEXCOORD0;
       float3 Normal   : NORMAL;
       float3 Tangent  : TANGENT;
       float3 Binormal : BINORMAL;
};

struct vertexOutput {
       float4 Position : POSITION;
       float2 TexCoord : TEXCOORD0;
       float3 LightVec : TEXCOORD1;
       float3 ViewVec  : TEXCOORD2;
};

vertexOutput std_VS(appdata Input) {
        vertexOutput Output = (vertexOutput)0;
        float3 CamPos = (0.0, 0.0, 1.0);

        Input.Tangent.y = Input.Tangent.y;
        Input.Binormal.y = -Inp ut.Binormal.y;
     //   float3 Light = float3(1.0, 1.0, 0.1);
        float3x3 RM;
        RM[0] = Input.Tangent;
        RM[1] = Input.Binormal;
        RM[2] = Input.Normal;

        Output.Position = mul(VPM, Input.Position);
        Output.TexCoord = Input.UV;

        Output.LightVec = mul(RM, normalize(mul(VPM, LightPos) - Output.Position));
        Output.ViewVec = mul(RM, normalize(mul(VPM, CamPos) - Output.Position));

 return Output;
}                                   


sampler2D DiffuseMap    : register(s0);

float4 std_PS(vertexOutput Input) : COLOR {  

        float4 Output;   
        float sizeh;
        sizeh = 1.0/512;
        float2 coordw = Input.TexCoord;
        coordw.y -= sizeh;
        float w = tex2D(DiffuseMap, coordw).r;

        float2 coords = Input.TexCoord;
        coords.y += sizeh;
        float s = tex2D(DiffuseMap, coords).r;

        float2 coorda = Input.TexCoord;
        coorda.x -= sizeh;
        float a = tex2D(DiffuseMap, coorda).r;

        float2 coordd = Input.TexCoord;
        coordd.x += sizeh;
        float d = tex2D(DiffuseMap, coordd).r;
              
        float t = tex2D(DiffuseMap, Input.TexCoord).r;

        
        float ws = w-s;
        Output = float4(((a-t)*16 - (d-t)*16 + 1.0) / 2.0, ((w-t)*16 - (s-t)*16 + 1.0) / 2.0, 1.0, 0);
          
        return Output;
}


technique main
{
    pass Pass0 
 {
        VertexShader = compile vs_2_0 std_VS();
        PixelShader = compile ps_2_0 std_PS();
 }
}
