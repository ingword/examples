unit PaintOnButton;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls;

type
  TForm3 = class(TForm)
    btnCircle: TButton;
    procedure btnCirclePaint(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.fmx}

procedure TForm3.btnCirclePaint(Sender: TObject; Canvas: TCanvas;
  const ARect: TRectF);
var
 l_StartRectPoint, l_FinishRectPoint : TPointF;
begin
 l_StartRectPoint := TPointF.Create(20 - 10, 20 - 10);
 l_FinishRectPoint := TPointF.Create(20 + 10, 20 + 10);

 Canvas.Stroke.Kind := TBrushKind.bkSolid;

 Canvas.DrawEllipse(TRectF.Create(l_StartRectPoint, l_FinishRectPoint), 1);
 Canvas.FillEllipse(TRectF.Create(l_StartRectPoint, l_FinishRectPoint), 0.5);
end;

end.
