// Copyright (C) Dmitry Shuklin 2008-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Console
{
	/// <summary>
	/// Summary description for Application.
	/// </summary>
	public class Application : Cerebrum.Integrator.Application
	{
		protected static Application m_Instance;

		public static Cerebrum.Samples.Console.Application Instance
		{
			get
			{
				return m_Instance;
			}
			set
			{
				m_Instance = value;
			}
		}

		public Application()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string ApplicationDirectory
		{
			get
			{
				return System.IO.Path.GetDirectoryName(this.GetType().Assembly.Location);
			}
		}

		public override Cerebrum.Integrator.DomainContext CreateMasterContext(string domainName, bool creating)
		{
			return new Cerebrum.Integrator.DomainContext(this);
		}

		protected override Cerebrum.Integrator.IContextFactory CreateContextFactory()
		{
			string primaryDirectory = this.ApplicationDirectory;
			string databaseFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.bin");
			string activityFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.log");
			string templateFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.xdb");

			return new Cerebrum.Management.SimpleContextFactory(this, databaseFileName, activityFileName, 4, true, templateFileName, true);
		}

		public override void ProcessException(Exception ex, string source)
		{
			System.Console.WriteLine(source);
			System.Console.WriteLine(ex.ToString());
		}

	}
}
