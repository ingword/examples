// Copyright (C) Dmitry Shuklin 2008-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Console
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			Application app = new Application();
			Application.Instance = app;
			app.Initialize();

			try
			{
				using(Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(app.MasterContext, "MyTable"))
				{
					if(view.Count < 1)
					{
						Cerebrum.Data.ComponentItemView item = view.AddNew();
						using(Cerebrum.IConnector connector = item.GetComposite())
						{
							Cerebrum.Samples.ConsoleObjects.MyClass1 myobj = connector.Component as Cerebrum.Samples.ConsoleObjects.MyClass1;
							if(myobj!=null)
							{
								myobj.Name = "Hello world!";
							}
						}
					}
					foreach(Cerebrum.Data.ComponentItemView item in view)
					{
						//System.Console.WriteLine(Convert.ToString(item["Name"]));
						using(Cerebrum.IConnector connector = item.GetComposite())
						{
							Cerebrum.Samples.ConsoleObjects.MyClass1 myobj = connector.Component as Cerebrum.Samples.ConsoleObjects.MyClass1;
							if(myobj!=null)
							{
								System.Console.WriteLine(myobj.Name);
							}
							// ����� �� �������� ������������� ��=12345 � ��������� �� ������-��������
							Cerebrum.IConnector connector2 = ((Cerebrum.IContainer)connector).AttachConnector(new Cerebrum.ObjectHandle(12345));
							try
							{
								// ���� �� �������, ���� ��������� ����� �������� ����
								if(connector2==null)
								{
									connector2 = ((Cerebrum.IContainer)connector).CreateConnector(new Cerebrum.ObjectHandle(12345), Cerebrum.Specialized.Concepts.ScalarNodeTypeId);
								}
								// �������� ��������� �� �������� ��������� 12345
								Cerebrum.Samples.ConsoleObjects.Test test = connector2.Component as Cerebrum.Samples.ConsoleObjects.Test;
								// ���� �����, ������� ����� ��������
								if(test==null)
								{
									test = new Cerebrum.Samples.ConsoleObjects.Test(); 
									connector2.Component = test;
									test.HitCount = 0;
								}
								//����������� �������
								test.HitCount++;

								//�������� ������� ��������
								System.Console.WriteLine(((Cerebrum.Samples.ConsoleObjects.Test)connector2.Component).HitCount);
							}
							finally
							{
								//��������� ��������, �������� ��������� ���� � ���������
								if(connector2!=null)
								{
									connector2.Dispose();
								}
							}
						}
					}
				}
			}
			finally
			{
				app.Shutdown();
				app.Dispose();
			}
		}
	}
}
