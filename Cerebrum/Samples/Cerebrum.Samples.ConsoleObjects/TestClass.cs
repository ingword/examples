// Copyright (C) Dmitry Shuklin 2008-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.ConsoleObjects
{
	/// <summary>
	/// Summary description for TestClass.
	/// </summary>
	[Serializable()]
	public class Test
	{
		public int HitCount;
	}
}
