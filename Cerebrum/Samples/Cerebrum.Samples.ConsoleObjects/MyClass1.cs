// Copyright (C) Dmitry Shuklin 2008-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.ConsoleObjects
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class MyClass1 : Cerebrum.Integrator.GenericComponent
	{
		public MyClass1()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string Name
		{
			get
			{
				using(Cerebrum.IContainer container = this.GetChildComponents())
				{
					string msg = Convert.ToString(Cerebrum.Integrator.Utilites.ObtainComponent(container, Cerebrum.Specialized.Concepts.NameAttribute));
					msg += ".";
					this.Name = msg;
					return msg;
				}
			}
			set
			{
				using(Cerebrum.IContainer container = this.GetChildComponents())
				{
					Cerebrum.Integrator.Utilites.UpdateComponent(container, Cerebrum.Specialized.Concepts.NameAttribute, value);
				}
			}
		}

	}
}
