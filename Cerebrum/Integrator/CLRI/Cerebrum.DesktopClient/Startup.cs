// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.DesktopClient
{
	/// <summary>
	/// Summary description for Startup.
	/// </summary>
	public sealed class Startup
	{
		private Startup()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		[STAThread]
		static void Main() 
		{
			System.Threading.Mutex myMutex = null;
			try
			{
				System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

				string cultureName = null;
				try
				{
					cultureName = ((string)(configurationAppSettings.GetValue("DesktopClient.Culture", typeof(string))));
				}
				catch
				{
				}
				if(cultureName!=null && cultureName!=string.Empty)
				{
					try
					{
						System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cultureName);
					}
					catch(System.Exception ex)
					{
						System.Windows.Forms.MessageBox.Show(ex.ToString(), "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
					}
				}

				bool licenseEnable = true;
				try
				{
					licenseEnable = ((bool)(configurationAppSettings.GetValue("LicenseAgreement.Enabled", typeof(bool))));
				}
				catch
				{
				}
				if(licenseEnable)
				{
					LicenseForm dlg = new LicenseForm();
					if(dlg.ShowDialog()!=System.Windows.Forms.DialogResult.OK)
					{
						return;
					}
				}

				bool isSingleton = false;
				try
				{
					isSingleton = ((bool)(configurationAppSettings.GetValue("SingletonProcess", typeof(bool))));
				}
				catch
				{
				}
#if !DEBUG
				if(isSingleton)
				{
					// Mutex constructor sets ownsMutex to false if mutex already exists,
					//    or to true if a new mutex was just created
					bool ownsMutex;
					myMutex = new System.Threading.Mutex(false, System.Windows.Forms.Application.ProductName, out ownsMutex);
					if (!ownsMutex)
					{
						System.Resources.ResourceManager resources = new System.Resources.ResourceManager("Cerebrum.DesktopEngine.Resources", typeof(Cerebrum.DesktopEngine.BackgroundListener).Assembly);
						System.Windows.Forms.MessageBox.Show(((string)(resources.GetObject("ApplicationAlreadyRunning.Message"))), System.Windows.Forms.Application.ProductName);
						return;
						/*MessageBox.Show
								(
								Application.ProductName + " is already running."
								);
							return; // do not allow this program to run twice
							*/
					}
					GC.KeepAlive(myMutex);

					/*
					System.Diagnostics.Process i_am = System.Diagnostics.Process.GetCurrentProcess();
					System.Diagnostics.Process[] ps = System.Diagnostics.Process.GetProcessesByName(i_am.ProcessName);
					foreach(System.Diagnostics.Process pr in ps)
					{
						if(pr.Id != i_am.Id)
						{
							System.Resources.ResourceManager resources = new System.Resources.ResourceManager("Cerebrum.DesktopEngine.Resources", typeof(Cerebrum.DesktopEngine.BackgroundListener).Assembly);
							System.Windows.Forms.MessageBox.Show(((string)(resources.GetObject("ApplicationAlreadyRunning.Message"))));
							return;
						}
					}
					*/
				}
#endif

				bool quickNotifyIconEnable = true;
				try
				{
					quickNotifyIconEnable = ((bool)(configurationAppSettings.GetValue("QuickNotifyIcon.Enabled", typeof(bool))));
				}
				catch
				{
				}

				Cerebrum.DesktopEngine.Application.Run(quickNotifyIconEnable, typeof(Cerebrum.DesktopEngine.Application), typeof(Cerebrum.Integrator.DomainContext), typeof(Cerebrum.DesktopClient.PrimaryWindowForm));
			}
			catch(System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(ex.ToString() + "\r\n\r\nThis application will close.", "Critical Error: " + ex.Source, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
			finally
			{
				// be sure and release Mutex before program exits
				if (myMutex != null) myMutex.Close();
			}
		}

	}
}
