// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.DesktopClient
{
	/// <summary>
	/// Summary description for License.
	/// </summary>
	public class LicenseForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnAccept;
		private System.Windows.Forms.Button btnHomePage;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public LicenseForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(LicenseForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnHomePage = new System.Windows.Forms.Button();
			this.btnAccept = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.AccessibleDescription = resources.GetString("panel1.AccessibleDescription");
			this.panel1.AccessibleName = resources.GetString("panel1.AccessibleName");
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel1.Anchor")));
			this.panel1.AutoScroll = ((bool)(resources.GetObject("panel1.AutoScroll")));
			this.panel1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMargin")));
			this.panel1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMinSize")));
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.Controls.Add(this.btnHomePage);
			this.panel1.Controls.Add(this.btnAccept);
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel1.Dock")));
			this.panel1.Enabled = ((bool)(resources.GetObject("panel1.Enabled")));
			this.panel1.Font = ((System.Drawing.Font)(resources.GetObject("panel1.Font")));
			this.panel1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel1.ImeMode")));
			this.panel1.Location = ((System.Drawing.Point)(resources.GetObject("panel1.Location")));
			this.panel1.Name = "panel1";
			this.panel1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel1.RightToLeft")));
			this.panel1.Size = ((System.Drawing.Size)(resources.GetObject("panel1.Size")));
			this.panel1.TabIndex = ((int)(resources.GetObject("panel1.TabIndex")));
			this.panel1.Text = resources.GetString("panel1.Text");
			this.panel1.Visible = ((bool)(resources.GetObject("panel1.Visible")));
			// 
			// btnHomePage
			// 
			this.btnHomePage.AccessibleDescription = resources.GetString("btnHomePage.AccessibleDescription");
			this.btnHomePage.AccessibleName = resources.GetString("btnHomePage.AccessibleName");
			this.btnHomePage.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnHomePage.Anchor")));
			this.btnHomePage.BackColor = System.Drawing.Color.Turquoise;
			this.btnHomePage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHomePage.BackgroundImage")));
			this.btnHomePage.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnHomePage.Dock")));
			this.btnHomePage.Enabled = ((bool)(resources.GetObject("btnHomePage.Enabled")));
			this.btnHomePage.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnHomePage.FlatStyle")));
			this.btnHomePage.Font = ((System.Drawing.Font)(resources.GetObject("btnHomePage.Font")));
			this.btnHomePage.Image = ((System.Drawing.Image)(resources.GetObject("btnHomePage.Image")));
			this.btnHomePage.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnHomePage.ImageAlign")));
			this.btnHomePage.ImageIndex = ((int)(resources.GetObject("btnHomePage.ImageIndex")));
			this.btnHomePage.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnHomePage.ImeMode")));
			this.btnHomePage.Location = ((System.Drawing.Point)(resources.GetObject("btnHomePage.Location")));
			this.btnHomePage.Name = "btnHomePage";
			this.btnHomePage.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnHomePage.RightToLeft")));
			this.btnHomePage.Size = ((System.Drawing.Size)(resources.GetObject("btnHomePage.Size")));
			this.btnHomePage.TabIndex = ((int)(resources.GetObject("btnHomePage.TabIndex")));
			this.btnHomePage.Text = resources.GetString("btnHomePage.Text");
			this.btnHomePage.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnHomePage.TextAlign")));
			this.btnHomePage.Visible = ((bool)(resources.GetObject("btnHomePage.Visible")));
			this.btnHomePage.Click += new System.EventHandler(this.btnHomePage_Click);
			// 
			// btnAccept
			// 
			this.btnAccept.AccessibleDescription = resources.GetString("btnAccept.AccessibleDescription");
			this.btnAccept.AccessibleName = resources.GetString("btnAccept.AccessibleName");
			this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAccept.Anchor")));
			this.btnAccept.BackColor = System.Drawing.Color.Turquoise;
			this.btnAccept.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.BackgroundImage")));
			this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnAccept.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAccept.Dock")));
			this.btnAccept.Enabled = ((bool)(resources.GetObject("btnAccept.Enabled")));
			this.btnAccept.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAccept.FlatStyle")));
			this.btnAccept.Font = ((System.Drawing.Font)(resources.GetObject("btnAccept.Font")));
			this.btnAccept.Image = ((System.Drawing.Image)(resources.GetObject("btnAccept.Image")));
			this.btnAccept.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAccept.ImageAlign")));
			this.btnAccept.ImageIndex = ((int)(resources.GetObject("btnAccept.ImageIndex")));
			this.btnAccept.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAccept.ImeMode")));
			this.btnAccept.Location = ((System.Drawing.Point)(resources.GetObject("btnAccept.Location")));
			this.btnAccept.Name = "btnAccept";
			this.btnAccept.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAccept.RightToLeft")));
			this.btnAccept.Size = ((System.Drawing.Size)(resources.GetObject("btnAccept.Size")));
			this.btnAccept.TabIndex = ((int)(resources.GetObject("btnAccept.TabIndex")));
			this.btnAccept.Text = resources.GetString("btnAccept.Text");
			this.btnAccept.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAccept.TextAlign")));
			this.btnAccept.Visible = ((bool)(resources.GetObject("btnAccept.Visible")));
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			// 
			// textBox1
			// 
			this.textBox1.AccessibleDescription = resources.GetString("textBox1.AccessibleDescription");
			this.textBox1.AccessibleName = resources.GetString("textBox1.AccessibleName");
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("textBox1.Anchor")));
			this.textBox1.AutoSize = ((bool)(resources.GetObject("textBox1.AutoSize")));
			this.textBox1.BackColor = System.Drawing.Color.White;
			this.textBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("textBox1.BackgroundImage")));
			this.textBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("textBox1.Dock")));
			this.textBox1.Enabled = ((bool)(resources.GetObject("textBox1.Enabled")));
			this.textBox1.Font = ((System.Drawing.Font)(resources.GetObject("textBox1.Font")));
			this.textBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("textBox1.ImeMode")));
			this.textBox1.Location = ((System.Drawing.Point)(resources.GetObject("textBox1.Location")));
			this.textBox1.MaxLength = ((int)(resources.GetObject("textBox1.MaxLength")));
			this.textBox1.Multiline = ((bool)(resources.GetObject("textBox1.Multiline")));
			this.textBox1.Name = "textBox1";
			this.textBox1.PasswordChar = ((char)(resources.GetObject("textBox1.PasswordChar")));
			this.textBox1.ReadOnly = true;
			this.textBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("textBox1.RightToLeft")));
			this.textBox1.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("textBox1.ScrollBars")));
			this.textBox1.Size = ((System.Drawing.Size)(resources.GetObject("textBox1.Size")));
			this.textBox1.TabIndex = ((int)(resources.GetObject("textBox1.TabIndex")));
			this.textBox1.Text = resources.GetString("textBox1.Text");
			this.textBox1.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("textBox1.TextAlign")));
			this.textBox1.Visible = ((bool)(resources.GetObject("textBox1.Visible")));
			this.textBox1.WordWrap = ((bool)(resources.GetObject("textBox1.WordWrap")));
			// 
			// License
			// 
			this.AcceptButton = this.btnAccept;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.CancelButton = this.btnCancel;
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.panel1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "License";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Load += new System.EventHandler(this.LicenseForm_Load);
			this.Activated += new System.EventHandler(this.LicenseForm_Activated);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnHomePage_Click(object sender, System.EventArgs e)
		{
			System.Diagnostics.Process.Start("http://www.shuklin.com/go/en/Cerebrum.aspx?source=cerebrum-desktop-client");
		}

		private void LicenseForm_Load(object sender, System.EventArgs e)
		{
			this.MinimumSize = this.Size;
		}

		private void LicenseForm_Activated(object sender, System.EventArgs e)
		{
			this.btnCancel.Focus();
		}

	}
}
