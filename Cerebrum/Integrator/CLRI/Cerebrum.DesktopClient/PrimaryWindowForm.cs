// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.DesktopClient
{
	/// <summary>
	/// Summary description for PrimaryWindowForm.
	/// </summary>
	public class PrimaryWindowForm : Cerebrum.DesktopEngine.PrimaryWindowForm
	{
		private System.Windows.Forms.MenuItem mnuCredits;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PrimaryWindowForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(PrimaryWindowForm));
			this.mnuCredits = new System.Windows.Forms.MenuItem();
			// 
			// menuItemHelp
			// 
			this.m_HelpMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.mnuCredits});
			// 
			// mnuCredits
			// 
			this.mnuCredits.Enabled = ((bool)(resources.GetObject("mnuCredits.Enabled")));
			this.mnuCredits.Index = 0;
			this.mnuCredits.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("mnuCredits.Shortcut")));
			this.mnuCredits.ShowShortcut = ((bool)(resources.GetObject("mnuCredits.ShowShortcut")));
			this.mnuCredits.Text = resources.GetString("mnuCredits.Text");
			this.mnuCredits.Visible = ((bool)(resources.GetObject("mnuCredits.Visible")));
			this.mnuCredits.Click += new System.EventHandler(this.mnuCredits_Click);
			// 
			// PrimaryWindowForm
			// 

		}
		#endregion

		public override void BuildUi()
		{
			base.BuildUi();

			System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

			bool creditsScreenEnabled = true;
			try
			{
				creditsScreenEnabled = ((bool)(configurationAppSettings.GetValue("CreditsScreen.Enabled", typeof(bool))));
			}
			catch
			{
			}
			if(!creditsScreenEnabled)
			{
				mnuCredits.Visible = false;
				mnuCredits.Enabled = false;
			}

		}
		private void mnuCredits_Click(object sender, System.EventArgs e)
		{
			CreditsForm dlg = new CreditsForm();
			dlg.ShowDialog(this);
		}
	}
}
