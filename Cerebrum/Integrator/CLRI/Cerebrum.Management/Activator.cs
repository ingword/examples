// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Management
{
	/// <summary>
	/// Summary description for Activator.
	/// </summary>
	public class Activator : Cerebrum.IActivator
	{
		public Activator()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		/*#region IObjectActivator Members

		public Cerebrum.ObjectHandle CreateTypeRecord(Cerebrum.IConnector domain, Type type)
		{
			// TODO:  Add Activator.CreateTypeRecord implementation
			return new Cerebrum.ObjectHandle ();
		}

		#endregion*/

		#region IActivator Members

		void Cerebrum.IActivator.CreateInstance(Cerebrum.IConnector domain, Cerebrum.ObjectHandle typeID, object [] args, ref object instance, ref Cerebrum.ObjectHandle classType)
		{
			// TODO:  Add Activator.CreateInstance implementation
			if(((System.IComparable)typeID).CompareTo(Cerebrum.ObjectHandle.Null) > 0)
			{
				if(typeID==Cerebrum.Specialized.Concepts.WardenNodeTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = null;
				}
				else if(typeID==Cerebrum.Specialized.Concepts.ScalarNodeTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Scalar;
					instance = null;
				}
				else if(typeID==Cerebrum.Specialized.Concepts.AttributeDescriptorTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = new Cerebrum.Reflection.AttributeDescriptor();
				}
				else if(typeID==Cerebrum.Specialized.Concepts.TypeDescriptorTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = new Cerebrum.Reflection.TypeDescriptor();
				}
				else if(typeID==Cerebrum.Specialized.Concepts.TableDescriptorTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = new Cerebrum.Reflection.TableDescriptor();
				}
				else if(typeID==Cerebrum.Specialized.Concepts.MessageDescriptorTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = new Cerebrum.Reflection.MessageDescriptor();
				}
				else if(typeID==Cerebrum.Specialized.Concepts.SystemInt16TypeId ||
					typeID==Cerebrum.Specialized.Concepts.SystemInt32TypeId ||
					typeID==Cerebrum.Specialized.Concepts.SystemInt64TypeId ||
					typeID==Cerebrum.Specialized.Concepts.SystemStringTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Scalar;
					instance = null;
				}
				else
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = null;
				}
			}
			else
			{
				classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
				instance = null;
			}
		}

		#endregion
	}
}
