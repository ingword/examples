// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Management
{
    /// <summary>
    /// Summary description for Tools.
    /// </summary>
    public sealed class Utilites
    {
        private Utilites()
        {
            //
            // TODO: Add constructor logic here
            //
        }

		/// <summary>
		/// ������� Coalesce ��������� �� ���� ��������� ����������.
		/// ������������ ������ �������� �� ������ null.
		/// </summary>
		public static object Coalesce(params object []values)
		{
			object v = null;
			for(int i=0; i < values.Length; i++)
			{
				v = values[i];
				if(v!=null)
				{
					break;
				}
			}
			return v;
		}

		private static void EngageHelper(Cerebrum.Data.SimpleView view, Cerebrum.IContainer container, Cerebrum.ObjectHandle handle)
		{
			using(Cerebrum.IComposite connector = container.AttachConnector(handle))
			{
				view.EngageConnector(handle, connector);
			}
		}

        public static void InitTypeDescriptor(Cerebrum.Runtime.NativeSector sector, Cerebrum.ObjectHandle nativeHandleTypeId, string QualifiedTypeName, Cerebrum.ObjectHandle KernelObjectKindId)
        {
            using (Cerebrum.IConnector connector = Cerebrum.Reflection.TypeDescriptor.Create(sector, nativeHandleTypeId))
            {
                Cerebrum.Reflection.TypeDescriptor tds = (Cerebrum.Reflection.TypeDescriptor)connector.Component;
                tds.QualifiedTypeName = QualifiedTypeName;
                tds.KernelObjectKindId = KernelObjectKindId;
            }
        }

        public static void InitAttributeDescriptor(Cerebrum.Runtime.NativeSector sector, Cerebrum.ObjectHandle nativeHandleAttributeId, string Name, string DisplayName, Cerebrum.ObjectHandle nativeHandleTypeId)
        {
            using (Cerebrum.IConnector connector = Cerebrum.Reflection.AttributeDescriptor.CreateInstance(sector, nativeHandleAttributeId))
            {
                Cerebrum.Reflection.AttributeDescriptor ads = (Cerebrum.Reflection.AttributeDescriptor)connector.Component;
                if (Name != null)
                {
                    ads.Name = Name;
                }
                if (DisplayName != null)
                {
                    ads.DisplayName = DisplayName;
                }
                ads.AttributeTypeIdHandle = nativeHandleTypeId;
            }
        }

        public static void PrepAttributeDescriptor(Cerebrum.Runtime.NativeSector sector, Cerebrum.ObjectHandle nativeHandleAttributeId, string Name, string DisplayName)
        {
            using (Cerebrum.IConnector connector = sector.AttachConnector(nativeHandleAttributeId))
            {
                Cerebrum.Reflection.AttributeDescriptor ads = (Cerebrum.Reflection.AttributeDescriptor)connector.Component;
                ads.Name = Name;
                ads.DisplayName = DisplayName;
            }
        }

        /*public static AttributeDescriptor Create(Cerebrum.Runtime.NativeSector sector, Cerebrum.ObjectHandle attrHandle)
        {
            using(Cerebrum.IConnector connector = sector.CreateConnector(attrHandle, Cerebrum.Specialized.Concepts.AttributeDescriptorTypeId))
            {
                return (AttributeDescriptor)connector.Component;
            }
        }*/

        public static void InitTableDescriptor(Cerebrum.Runtime.NativeSector sector, Cerebrum.ObjectHandle nativeHandleAttributeId, string Name, string DisplayName, Cerebrum.ObjectHandle nativeHandleComponentTypeId, Cerebrum.ObjectHandle[] attributes)
        {
            using (Cerebrum.IConnector connector = Cerebrum.Reflection.TableDescriptor.CreateTable(sector, nativeHandleAttributeId, attributes))
            {
                Cerebrum.Reflection.TableDescriptor tbl = (Cerebrum.Reflection.TableDescriptor)connector.Component;
                tbl.Name = Name;
                tbl.DisplayName = DisplayName;
                tbl.ComponentTypeIdHandle = nativeHandleComponentTypeId;
            }
        }

        public static void InitDataBase(Cerebrum.IWorkspace domain)
        {

            using (Cerebrum.Runtime.NativeSector sector = domain.GetSector() as Cerebrum.Runtime.NativeSector)
            {

                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.ObjectHandleTypeId, "Cerebrum.ObjectHandle, Cerebrum.Typedef", Cerebrum.Runtime.KernelObjectKindId.Scalar);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.SystemStringTypeId, "System.String", Cerebrum.Runtime.KernelObjectKindId.Scalar);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.SystemInt16TypeId, "System.Int16", Cerebrum.Runtime.KernelObjectKindId.Scalar);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.SystemInt32TypeId, "System.Int32", Cerebrum.Runtime.KernelObjectKindId.Scalar);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.SystemInt64TypeId, "System.Int64", Cerebrum.Runtime.KernelObjectKindId.Scalar);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.WardenNodeTypeId, "System.Object", Cerebrum.Runtime.KernelObjectKindId.Warden);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.ScalarNodeTypeId, "System.Object", Cerebrum.Runtime.KernelObjectKindId.Scalar);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.SystemBooleanTypeId, "System.Boolean", Cerebrum.Runtime.KernelObjectKindId.Scalar);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.SystemDateTimeTypeId, "System.DateTime", Cerebrum.Runtime.KernelObjectKindId.Scalar);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.TypeDescriptorTypeId, "Cerebrum.Reflection.TypeDescriptor, Cerebrum.Integrator", Cerebrum.Runtime.KernelObjectKindId.Warden);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.AttributeDescriptorTypeId, "Cerebrum.Reflection.AttributeDescriptor, Cerebrum.Integrator", Cerebrum.Runtime.KernelObjectKindId.Warden);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.TableDescriptorTypeId, "Cerebrum.Reflection.TableDescriptor, Cerebrum.Integrator", Cerebrum.Runtime.KernelObjectKindId.Warden);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.MessageDescriptorTypeId, "Cerebrum.Reflection.MessageDescriptor, Cerebrum.Integrator", Cerebrum.Runtime.KernelObjectKindId.Warden);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.BindingListTypeId, "System.ComponentModel.IBindingList, System", Cerebrum.Runtime.KernelObjectKindId.Warden);
                //InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.KernelObjectClassTypeId, "Cerebrum.Runtime.KernelObjectKindId, Cerebrum.Runtime", Cerebrum.Runtime.KernelObjectKindId.Scalar);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.FieldDescriptorTypeId, "Cerebrum.Reflection.FieldDescriptor, Cerebrum.Integrator", Cerebrum.Runtime.KernelObjectKindId.Warden);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.GenericComponentTypeId, "Cerebrum.Integrator.GenericComponent, Cerebrum.Integrator", Cerebrum.Runtime.KernelObjectKindId.Warden);
                InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.DomainContextTypeId, "Cerebrum.Integrator.DomainContext, Cerebrum.Integrator", Cerebrum.ObjectHandle.Null);
				InitTypeDescriptor(sector, Cerebrum.Specialized.Concepts.SystemByteArrayTypeId, "System.Byte[]", Cerebrum.Runtime.KernelObjectKindId.Stream);

                /*Cerebrum.Reflection.AttributeDescriptor atrObjectHandle;
                Cerebrum.Reflection.AttributeDescriptor atrName;
                Cerebrum.Reflection.AttributeDescriptor atrDisplayName;
                Cerebrum.Reflection.AttributeDescriptor atrDescription;
                Cerebrum.Reflection.AttributeDescriptor atrTypeId;
                Cerebrum.Reflection.AttributeDescriptor atrDefTypeId;
                Cerebrum.Reflection.AttributeDescriptor atrQualifiedTypeName;
                Cerebrum.Reflection.AttributeDescriptor atrKernelObjectClass;
                Cerebrum.Reflection.AttributeDescriptor atrSelectedAttributes;
                Cerebrum.Reflection.AttributeDescriptor atrSelectedComponents;
                Cerebrum.Reflection.AttributeDescriptor atrPrecedentTableId;
                Cerebrum.Reflection.AttributeDescriptor atrDependentFieldId;
                Cerebrum.Reflection.AttributeDescriptor atrDisplayFieldId;
*/
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.PlasmaHandleAttribute, null, null, Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.NameAttribute, null, null, Cerebrum.Specialized.Concepts.SystemStringTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.DisplayNameAttribute, null, null, Cerebrum.Specialized.Concepts.SystemStringTypeId);

                PrepAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.PlasmaHandleAttribute, Cerebrum.Specialized.KnownNames.ObjectHandle, "Object Handle");
                PrepAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.NameAttribute, "Name", "Name");
                PrepAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.DisplayNameAttribute, "DisplayName", "DisplayName");

                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.DescriptionAttribute, "Description", "Description", Cerebrum.Specialized.Concepts.SystemStringTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.GroundHandleAttribute, "GroundHandle", "Ground handle", Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.KindIdHandleAttribute, "KindIdHandle", "KindId handle", Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.TypeIdHandleAttribute, "TypeIdHandle", "TypeId handle", Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute, "DefaultTypeId", "Default TypeId handle", Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.QualifiedTypeNameAttribute, "QualifiedTypeName", "Qualified Type Name", Cerebrum.Specialized.Concepts.SystemStringTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.KernelObjectKindIdAttribute, "KernelObjectClass", "Kernel Object KindId", Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.SelectedAttributesAttribute, "SelectedAttributes", "Selected Attributes", Cerebrum.Specialized.Concepts.BindingListTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.SelectedComponentsAttribute, "SelectedComponents", "Selected Components", Cerebrum.Specialized.Concepts.BindingListTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.PrecedentTableIdAttribute, "PrecedentTableId", "Precedent Table Id", Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.DependentFieldIdAttribute, "DependentFieldId", "Dependent Field Id", Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.DisplayFieldIdAttribute, "DisplayFieldId", "Display Field Id", Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.DerivedFlagAttribute, "Derived", "Derived", Cerebrum.Specialized.Concepts.SystemBooleanTypeId);
                InitAttributeDescriptor(sector, Cerebrum.Specialized.Concepts.ReadOnlyAttribute, "ReadOnly", "ReadOnly", Cerebrum.Specialized.Concepts.SystemBooleanTypeId);

                Cerebrum.IConnector cntAttributes = Cerebrum.Reflection.TableDescriptor.CreateTable(sector, Cerebrum.Specialized.Concepts.AttributesTableId,
                    new Cerebrum.ObjectHandle[] {
															 Cerebrum.Specialized.Concepts.PlasmaHandleAttribute, 
															 Cerebrum.Specialized.Concepts.TypeIdHandleAttribute, 
															 Cerebrum.Specialized.Concepts.NameAttribute, 
															 Cerebrum.Specialized.Concepts.DisplayNameAttribute, 
															 Cerebrum.Specialized.Concepts.DescriptionAttribute, 
															 Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute});
                Cerebrum.Reflection.TableDescriptor tblAttributes = cntAttributes.Component as Cerebrum.Reflection.TableDescriptor;
                tblAttributes.Name = "Attributes";
                tblAttributes.DisplayName = "Attributes";
                tblAttributes.ComponentTypeIdHandle = Cerebrum.Specialized.Concepts.AttributeDescriptorTypeId;

                Cerebrum.Data.TableView vwv;
                vwv = tblAttributes.GetTableView();
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.PlasmaHandleAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.TypeIdHandleAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.NameAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.DisplayNameAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.DescriptionAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.QualifiedTypeNameAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.KernelObjectKindIdAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.SelectedAttributesAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.SelectedComponentsAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.PrecedentTableIdAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.DependentFieldIdAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.DisplayFieldIdAttribute);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.DerivedFlagAttribute);
                vwv.Dispose();

                Cerebrum.IConnector cntTypes = Cerebrum.Reflection.TableDescriptor.CreateTable(sector, Cerebrum.Specialized.Concepts.TypesTableId,
                    new Cerebrum.ObjectHandle[] {
															Cerebrum.Specialized.Concepts.PlasmaHandleAttribute, 
															Cerebrum.Specialized.Concepts.TypeIdHandleAttribute, 
															Cerebrum.Specialized.Concepts.NameAttribute, 
															Cerebrum.Specialized.Concepts.DisplayNameAttribute, 
															Cerebrum.Specialized.Concepts.DescriptionAttribute, 
															Cerebrum.Specialized.Concepts.QualifiedTypeNameAttribute, 
															Cerebrum.Specialized.Concepts.KernelObjectKindIdAttribute});
                Cerebrum.Reflection.TableDescriptor tblTypes = cntTypes.Component as Cerebrum.Reflection.TableDescriptor;
                tblTypes.Name = "Types";
                tblTypes.DisplayName = "Types";
                tblTypes.ComponentTypeIdHandle = Cerebrum.Specialized.Concepts.TypeDescriptorTypeId;

                vwv = tblTypes.GetTableView();
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.ObjectHandleTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.ScalarNodeTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.WardenNodeTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.SystemStringTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.SystemInt16TypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.SystemInt32TypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.SystemInt64TypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.SystemBooleanTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.SystemDateTimeTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.TypeDescriptorTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.AttributeDescriptorTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.TableDescriptorTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.MessageDescriptorTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.BindingListTypeId);
                //EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.KernelObjectClassTypeId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.FieldDescriptorTypeId);
				EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.GenericComponentTypeId);
				EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.DomainContextTypeId);
				EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.SystemByteArrayTypeId);
				vwv.Dispose();

                InitTableDescriptor(sector, Cerebrum.Specialized.Concepts.MessagesTableId, "Messages", "Messages", Cerebrum.Specialized.Concepts.MessageDescriptorTypeId,
                    new Cerebrum.ObjectHandle[] {
															Cerebrum.Specialized.Concepts.PlasmaHandleAttribute, 
															Cerebrum.Specialized.Concepts.TypeIdHandleAttribute, 
															Cerebrum.Specialized.Concepts.NameAttribute, 
															Cerebrum.Specialized.Concepts.DisplayNameAttribute, 
															Cerebrum.Specialized.Concepts.DescriptionAttribute});

                InitTableDescriptor(sector, Cerebrum.Specialized.Concepts.FieldsTableId, "Fields", "Fields", Cerebrum.Specialized.Concepts.FieldDescriptorTypeId,
                    new Cerebrum.ObjectHandle[] {
															Cerebrum.Specialized.Concepts.PlasmaHandleAttribute, 
															Cerebrum.Specialized.Concepts.TypeIdHandleAttribute, 
															Cerebrum.Specialized.Concepts.DependentFieldIdAttribute, 
															Cerebrum.Specialized.Concepts.PrecedentTableIdAttribute, 
															Cerebrum.Specialized.Concepts.DerivedFlagAttribute});

                InitTableDescriptor(sector, Cerebrum.Specialized.Concepts.TablesTableId, "Tables", "Tables", Cerebrum.Specialized.Concepts.TableDescriptorTypeId,
                    new Cerebrum.ObjectHandle[] {
															Cerebrum.Specialized.Concepts.PlasmaHandleAttribute, 
															Cerebrum.Specialized.Concepts.TypeIdHandleAttribute, 
															Cerebrum.Specialized.Concepts.NameAttribute, 
															Cerebrum.Specialized.Concepts.DisplayNameAttribute, 
															Cerebrum.Specialized.Concepts.DescriptionAttribute, 
															Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute, 
															Cerebrum.Specialized.Concepts.DisplayFieldIdAttribute, 
															Cerebrum.Specialized.Concepts.SelectedAttributesAttribute, 
															Cerebrum.Specialized.Concepts.SelectedComponentsAttribute});

                Cerebrum.IConnector cntTables = sector.AttachConnector(Cerebrum.Specialized.Concepts.TablesTableId);
                Cerebrum.Reflection.TableDescriptor tblTables = cntTables.Component as Cerebrum.Reflection.TableDescriptor;

                vwv = tblTables.GetTableView();
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.TypesTableId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.TablesTableId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.FieldsTableId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.AttributesTableId);
                EngageHelper(vwv, sector, Cerebrum.Specialized.Concepts.MessagesTableId);
                vwv.Dispose();

                // inherited table support
                sector.Newobj(Cerebrum.Specialized.Concepts.SingleObjectHandleVectorId, Cerebrum.Runtime.KernelObjectKindId.Warden);
                using (Cerebrum.IContainer c1 = sector.AttachConnector(Cerebrum.Specialized.Concepts.SingleObjectHandleVectorId) as Cerebrum.IContainer)
                {
                    //vector.SetMap(Cerebrum.Specialized.Concepts.PlasmaHandleAttribute, Cerebrum.Runtime.MapAccess.Scalar, new Cerebrum.ObjectHandle(1));
                    using (Cerebrum.IComposite c2 = sector.AttachConnector(Cerebrum.Specialized.Concepts.PlasmaHandleAttribute))
                    {
                        c1.EngageConnector(Cerebrum.Specialized.Concepts.PlasmaHandleAttribute, c2);
                    }
                }

                cntTypes.Dispose();
                cntTables.Dispose();
                cntAttributes.Dispose();
            }
			domain.Savepoints.Flush();
        }

        public static void ExportDatabase(Cerebrum.IWorkspace workspace, System.Xml.XmlWriter xw, bool exportGrounds)
        {
            xw.WriteStartDocument(false);
            xw.WriteDocType("vnpidb", null, null/*"vnpiodl.dtd"*/, null);
            using (Cerebrum.Runtime.NativeSector sector = workspace.GetSector() as Cerebrum.Runtime.NativeSector)
            {
                xw.WriteStartElement("SystemDatabase");

                xw.WriteStartElement("Globals");
                xw.WriteAttributeString("SectorCurrentSequence", sector.CurrSequence.ToInt64().ToString());
                xw.WriteEndElement();

                using (Cerebrum.IConnector connector = sector.AttachConnector(Cerebrum.Specialized.Concepts.TablesTableId))
                {
                    Cerebrum.Reflection.TableDescriptor tblTables = connector.Component as Cerebrum.Reflection.TableDescriptor;
                    Cerebrum.Data.TableView vwv = tblTables.GetTableView();
                    Cerebrum.Data.TableSet tst = new Cerebrum.Data.TableSet(vwv);
                    System.ComponentModel.PropertyDescriptorCollection prpsTables = tst.GetItemProperties(null);

                    //Cerebrum.Integrator.Xml.SystemDatabase sysdb = new Cerebrum.Integrator.Xml.SystemDatabase();
                    //sysdb.EnforceConstraints = false;

                    foreach (System.ComponentModel.PropertyDescriptor prpTbl in prpsTables)
                    {
                        Cerebrum.ObjectHandle defaultTypeHandle = Cerebrum.ObjectHandle.Null;
                        if (prpTbl is Cerebrum.Data.CerebrumTableDescriptor)
                        {
                            defaultTypeHandle = (prpTbl as Cerebrum.Data.CerebrumTableDescriptor).TableView.TableDescriptor.ComponentTypeIdHandle;
                        }
                        System.ComponentModel.IBindingList list = (System.ComponentModel.IBindingList)prpTbl.GetValue(tst[0]);
                        System.ComponentModel.PropertyDescriptorCollection itemPrps = (list as System.ComponentModel.ITypedList).GetItemProperties(null);
                        foreach (object item in list)
                        {
							bool plasmaWrited = false;
							bool groundWrited = false;
							bool typeIdWrited = false;

                            xw.WriteStartElement(prpTbl.Name);

							if(exportGrounds)
							{
								Cerebrum.Data.ComponentItemView itemView = item as Cerebrum.Data.ComponentItemView;
								if(itemView != null)
								{
									using(Cerebrum.IComposite itemComp = itemView.GetComposite())
									{
										Cerebrum.ObjectHandle plasmaHandle = itemComp.PlasmaHandle;
										Cerebrum.ObjectHandle typeIdHandle = itemComp.TypeIdHandle;

										Cerebrum.ObjectHandle groundHandle = itemComp.GroundHandle;
										if(groundHandle == Cerebrum.ObjectHandle.Null && itemComp is Cerebrum.Runtime.ICompositeEx)
										{
											(itemComp as Cerebrum.Runtime.ICompositeEx).FastenGround();
											groundHandle = itemComp.GroundHandle;
										}

										bool isSectorGround;
										using(Cerebrum.IComposite itemSecComp = sector.AttachConnector(plasmaHandle))
										{
											isSectorGround = itemSecComp != null && (groundHandle == itemSecComp.GroundHandle);
										}

										if(isSectorGround)
										{
											xw.WriteAttributeString(Cerebrum.Specialized.KnownNames.ObjectHandle, plasmaHandle.ToString());
										}
										else
										{
											xw.WriteAttributeString(Cerebrum.Specialized.KnownNames.PlasmaHandle, plasmaHandle.ToString());
										}

										if(groundHandle != Cerebrum.ObjectHandle.Null)
										{
											xw.WriteAttributeString(Cerebrum.Specialized.KnownNames.GroundHandle, groundHandle.ToString());
											groundWrited = true;
										}
										if (typeIdHandle != defaultTypeHandle) 
										{
											xw.WriteAttributeString(Cerebrum.Specialized.KnownNames.TypeIdHandle, typeIdHandle.ToString());
										}

										plasmaWrited = true;
										typeIdWrited = true;
									}
								}
							}

							foreach (System.ComponentModel.PropertyDescriptor itemPrp in itemPrps)
                            {
								Cerebrum.Data.AttributePropertyDescriptor apd = itemPrp as Cerebrum.Data.AttributePropertyDescriptor;
								Cerebrum.ObjectHandle kc = Cerebrum.Runtime.KernelObjectKindId.Scalar;
                                if (apd!=null)
                                {
                                    if (apd.Derived)
                                    {
                                        continue;
                                    }
									using(Cerebrum.IConnector tdc = sector.AttachConnector(apd.AttributeTypeIdHandle))
									{
										if(tdc!=null)
										{
											kc = ((Cerebrum.Reflection.TypeDescriptor)tdc.Component).KernelObjectKindId;
										}
									}
                                }

								if(kc==Cerebrum.Runtime.KernelObjectKindId.Stream)
								{
									continue;
								}
								else if (itemPrp.PropertyType == typeof(Cerebrum.ObjectHandle))
								{
									if (itemPrp.Name == Cerebrum.Specialized.KnownNames.ObjectHandle && plasmaWrited)
									{
										continue;
									}
									if (itemPrp.Name == Cerebrum.Specialized.KnownNames.GroundHandle && groundWrited)
									{
										continue;
									}
									if (itemPrp.Name == Cerebrum.Specialized.KnownNames.TypeIdHandle && typeIdWrited)
									{
										continue;
									}
									Cerebrum.ObjectHandle h = Cerebrum.ObjectHandle.Null;
									object o = itemPrp.GetValue(item);
									if (o != null && o != System.DBNull.Value)
									{
										h = (Cerebrum.ObjectHandle)o;
										if (itemPrp.Name == Cerebrum.Specialized.KnownNames.TypeIdHandle)
										{
											if (h == defaultTypeHandle) continue;
										}

										xw.WriteAttributeString(itemPrp.Name, h.ToString());
									}
								}
								else if (itemPrp.PropertyType == typeof(System.ComponentModel.IBindingList))
								{
									continue;
								}
								else if (itemPrp.PropertyType == typeof(System.Object))
								{
									continue;
								}
								else if (itemPrp.PropertyType == typeof(System.DateTime))
								{
									object o = itemPrp.GetValue(item);
									if (o != null && o != System.DBNull.Value)
									{
										xw.WriteAttributeString(itemPrp.Name, Convert.ToDateTime(o).ToString("s"));
									}
								}
								else
								{
									object o = itemPrp.GetValue(item);
									if (o != null && o != System.DBNull.Value)
									{
										xw.WriteAttributeString(itemPrp.Name, Convert.ToString(o));
									}
								}
                            }
                            foreach (System.ComponentModel.PropertyDescriptor itemPrp in itemPrps)
                            {
								Cerebrum.Data.AttributePropertyDescriptor apd = itemPrp as Cerebrum.Data.AttributePropertyDescriptor;
								Cerebrum.ObjectHandle kc = Cerebrum.Runtime.KernelObjectKindId.Scalar;
								if (apd!=null)
								{
									if (apd.Derived)
									{
										continue;
									}
									using(Cerebrum.IConnector tdc = sector.AttachConnector(apd.AttributeTypeIdHandle))
									{
										if(tdc!=null)
										{
											kc = ((Cerebrum.Reflection.TypeDescriptor)tdc.Component).KernelObjectKindId;
										}
									}
								}
								
								if(kc==Cerebrum.Runtime.KernelObjectKindId.Stream)
								{
									object o = itemPrp.GetValue(item);
									if (o is byte[])
									{
										xw.WriteStartElement(itemPrp.Name);
										xw.WriteString(Convert.ToBase64String(o as byte[]));
										xw.WriteEndElement();
									}
								}
								else if (itemPrp.PropertyType == typeof(System.ComponentModel.IBindingList))
                                {
                                    if (prpTbl.Name == "Tables" && itemPrp.Name == "SelectedComponents") continue;
                                    System.ComponentModel.IBindingList attrList = (System.ComponentModel.IBindingList)itemPrp.GetValue(item);
                                    System.ComponentModel.PropertyDescriptor attrNH = (attrList as System.ComponentModel.ITypedList).GetItemProperties(null)[Cerebrum.Specialized.KnownNames.ObjectHandle];
									foreach (object itemNH in attrList)
									{
										bool plasmaWrited2 = false;

										xw.WriteStartElement(itemPrp.Name);

										if(exportGrounds)
										{
											Cerebrum.Data.ComponentItemView itemView2 = itemNH as Cerebrum.Data.ComponentItemView;
											if(itemView2 != null)
											{
												using(Cerebrum.IComposite itemComp2 = itemView2.GetComposite())
												{
													xw.WriteAttributeString(Cerebrum.Specialized.KnownNames.ObjectHandle, itemComp2.PlasmaHandle.ToString());
													Cerebrum.ObjectHandle ground2 = itemComp2.GroundHandle;
													if(ground2 == Cerebrum.ObjectHandle.Null && itemComp2 is Cerebrum.Runtime.ICompositeEx)
													{
														(itemComp2 as Cerebrum.Runtime.ICompositeEx).FastenGround();
														ground2 = itemComp2.GroundHandle;
													}
													if(ground2 != Cerebrum.ObjectHandle.Null)
													{
														xw.WriteAttributeString(Cerebrum.Specialized.KnownNames.GroundHandle, ground2.ToString());
													}
												}
												plasmaWrited2 = true;
											}
										}

										if(!plasmaWrited2)
										{
											Cerebrum.ObjectHandle h;
											h = (Cerebrum.ObjectHandle)attrNH.GetValue(itemNH);

											xw.WriteAttributeString(attrNH.Name, ((Cerebrum.ObjectHandle)h).ToString());
										}
										
										xw.WriteEndElement();
									}
                                    if (attrList is System.IDisposable) ((System.IDisposable)attrList).Dispose();
                                }
                                else if (itemPrp.PropertyType == typeof(System.Object))
                                {
                                    object o = itemPrp.GetValue(item);
                                    if (o != null && o != System.DBNull.Value)
                                    {
                                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                        {
                                            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                                            formatter.Serialize(ms, o);
                                            xw.WriteStartElement(itemPrp.Name);
                                            xw.WriteString(Convert.ToBase64String(ms.ToArray()));
                                            xw.WriteEndElement();
                                        }
                                    }
                                }
                            }
                            xw.WriteEndElement();
                        }
                        if (list is System.IDisposable) ((System.IDisposable)list).Dispose();
                    }
                    vwv.Dispose();
                    tst.Dispose();

                }
                xw.WriteEndElement();
            }
            xw.WriteEndDocument();
            xw.Flush();
        }

		public static void ExportWorkspace(Cerebrum.IWorkspace workspace, System.Xml.XmlWriter xw)
		{
			xw.WriteStartDocument(false);
			xw.WriteDocType("vnpidb", null, null/*"vnpiodl.dtd"*/, null);
			using (Cerebrum.Runtime.NativeSector sector = workspace.GetSector() as Cerebrum.Runtime.NativeSector)
			{
				xw.WriteStartElement("workspace");

				xw.WriteAttributeString("sequence", sector.CurrSequence.ToInt64().ToString());
				xw.WriteAttributeString("SectorGround", sector.GroundHandle.ToInt64().ToString());

				Cerebrum.Collections.Specialized.HandleObjectDictionary dictionary = new Cerebrum.Collections.Specialized.HandleObjectDictionary();

				ExportComposite(sector, xw, dictionary);

				xw.WriteEndElement();
			}
			xw.WriteEndDocument();
			xw.Flush();
		}

		private static void ExportComposite(Cerebrum.IComposite composite, System.Xml.XmlWriter xw, Cerebrum.Collections.Specialized.HandleObjectDictionary dictionary)
		{
			Cerebrum.ObjectHandle groundHandle = composite.GroundHandle;
			if(dictionary[groundHandle]!=null)
			{
				return;
			}
			dictionary[groundHandle] = true;

			Cerebrum.ObjectHandle kindIdHandle = composite.KindIdHandle;
			Cerebrum.ObjectHandle typeIdHandle = Cerebrum.ObjectHandle.Null;
			if(
				kindIdHandle==Cerebrum.Runtime.KernelObjectKindId.Scalar ||
				kindIdHandle==Cerebrum.Runtime.KernelObjectKindId.Stream ||
				kindIdHandle==Cerebrum.Runtime.KernelObjectKindId.Warden )
			{
				typeIdHandle = composite.TypeIdHandle;
			}

			xw.WriteStartElement("composite");
			xw.WriteAttributeString("GroundHandle", groundHandle.ToInt64().ToString());
			xw.WriteAttributeString("KindIdHandle", kindIdHandle.ToInt64().ToString());
			
			bool componentWrited = false;

			if(typeIdHandle!=Cerebrum.ObjectHandle.Null)
			{
				if(!componentWrited)
				{
					componentWrited = true;
					xw.WriteStartElement("component");
				}
				xw.WriteAttributeString("TypeIdPlasma", typeIdHandle.ToInt64().ToString());
			}

			if(kindIdHandle==Cerebrum.Runtime.KernelObjectKindId.Scalar)
			{
				object component = composite.Component;
				if(component != null && !Convert.IsDBNull(component))
				{
					System.Type type = component.GetType();
					System.ComponentModel.TypeConverter tc = System.ComponentModel.TypeDescriptor.GetConverter(component);
					if(tc!=null && tc.CanConvertTo(typeof(System.String)) && (type.IsValueType || type == typeof(System.String)) && !type.IsArray )
					{
						if(!componentWrited)
						{
							componentWrited = true;
							xw.WriteStartElement("component");
						}
						xw.WriteAttributeString("typeName", component.GetType().AssemblyQualifiedName);
						xw.WriteAttributeString("value", tc.ConvertToInvariantString(component));
					}
					else
					{
						if(!componentWrited)
						{
							componentWrited = true;
							xw.WriteStartElement("component");
						}
						using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
						{
							if(component is Cerebrum.IPersistent)
							{
								xw.WriteAttributeString("typeName", component.GetType().AssemblyQualifiedName);
								(component as Cerebrum.IPersistent).Serialize(Cerebrum.SerializeDirection.Save, composite, ms);
							}
							else
							{
								System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
								formatter.Serialize(ms, component);
							}
							xw.WriteString(Convert.ToBase64String(ms.ToArray()));
						}
					}
				}
			}
			else
			{
				object component = composite.Component;
				if(component != null && component is Cerebrum.IPersistent)
				{
					if(!componentWrited)
					{
						componentWrited = true;
						xw.WriteStartElement("component");
					}
					using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
					{
						(component as Cerebrum.IPersistent).Serialize(Cerebrum.SerializeDirection.Save, composite, ms);
						xw.WriteString(Convert.ToBase64String(ms.ToArray()));
					}
				}
			}
			if(componentWrited)
			{
				componentWrited = false;
				xw.WriteEndElement();
			}

			if(kindIdHandle==Cerebrum.Runtime.KernelObjectKindId.Stream)
			{
				xw.WriteStartElement("stream");
				using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
				{
					System.IO.Stream cs = (System.IO.Stream)composite;
					byte[] buffer = new byte[cs.Length];
					cs.Read(buffer, 0, buffer.Length);
					ms.Write(buffer, 0, buffer.Length);
					xw.WriteString(Convert.ToBase64String(ms.ToArray()));
				}
				xw.WriteEndElement();
			}

			System.Collections.ArrayList list = null;
			if(composite is Cerebrum.IContainer && composite is System.Collections.IEnumerable)
			{
				list = new System.Collections.ArrayList();
				System.Collections.IEnumerator enumerator = ((System.Collections.IEnumerable)composite).GetEnumerator();
				try
				{
					while(enumerator.MoveNext())
					{
						System.Collections.DictionaryEntry de = (System.Collections.DictionaryEntry)enumerator.Current;
						Cerebrum.ObjectHandle drivenPlasma = (Cerebrum.ObjectHandle)de.Key;
						Cerebrum.ObjectHandle drivenGround = (Cerebrum.ObjectHandle)de.Value;
						xw.WriteStartElement("composite");
						xw.WriteAttributeString("GroundHandle", drivenGround.ToInt64().ToString());
						xw.WriteAttributeString("PlasmaHandle", drivenPlasma.ToInt64().ToString());
						xw.WriteEndElement();
						if(dictionary[drivenGround] == null)
						{
							list.Add(drivenPlasma);
						}
					}
				}
				finally
				{
					if(enumerator is System.IDisposable)
					{
						((System.IDisposable)enumerator).Dispose();
					}
					enumerator = null;
				}
			}
			xw.WriteEndElement();

			if(list!=null)
			{
				foreach(Cerebrum.ObjectHandle drivenPlasma in list)
				{
					using(Cerebrum.IComposite drivenComposite = (composite as Cerebrum.IContainer).AttachConnector(drivenPlasma))
					{
						ExportComposite(drivenComposite, xw, dictionary);
					}
				}
			}
		}

		private static void ImportWorkspace(Cerebrum.IWorkspace workspace, System.Xml.XmlNode root)
		{
			Cerebrum.Collections.Specialized.HandleObjectDictionary plasmaIndex = null;
			Cerebrum.Collections.Specialized.HandleObjectDictionary groundIndex = new Cerebrum.Collections.Specialized.HandleObjectDictionary();
			Cerebrum.Collections.Specialized.HandleObjectDictionary importIndex = new Cerebrum.Collections.Specialized.HandleObjectDictionary();
			Cerebrum.Collections.Specialized.HandleObjectDictionary sectorIndex = new Cerebrum.Collections.Specialized.HandleObjectDictionary();

			foreach (System.Xml.XmlNode node in root.ChildNodes)
			{
				System.Xml.XmlAttribute attrGroundHandle = node.Attributes["GroundHandle"];
				Cerebrum.ObjectHandle groundHandle = Cerebrum.ObjectHandle.Parse(attrGroundHandle.Value);
				if(importIndex[groundHandle]!=null)
				{
					throw new System.Exception("GroundHandle = '" + groundHandle.ToString() + "' is duplicated");
				}
				importIndex[groundHandle] = node;
			}

			System.Xml.XmlAttribute attrSectorGround = root.Attributes["SectorGround"];
			Cerebrum.ObjectHandle importSectorGround = Cerebrum.ObjectHandle.Parse(attrSectorGround.Value);
			System.Xml.XmlNode sectorNode = (System.Xml.XmlNode)importIndex[importSectorGround];

			foreach (System.Xml.XmlNode node in sectorNode.ChildNodes)
			{
				System.Xml.XmlAttribute attrGroundHandle = node.Attributes["GroundHandle"];
				System.Xml.XmlAttribute attrPlasmaHandle = node.Attributes["PlasmaHandle"];
				Cerebrum.ObjectHandle drivenPlasma = Cerebrum.ObjectHandle.Parse(attrPlasmaHandle.Value);
				Cerebrum.ObjectHandle drivenGround = Cerebrum.ObjectHandle.Parse(attrGroundHandle.Value);
				if(sectorIndex[drivenPlasma]!=null)
				{
					throw new System.Exception("PlasmaHandle = '" + drivenPlasma.ToString() + "' is duplicated");
				}
				sectorIndex[drivenPlasma] = drivenGround;
			}

			using (Cerebrum.Runtime.NativeSector sector = workspace.GetSector() as Cerebrum.Runtime.NativeSector)
			{
				if (plasmaIndex == null)
				{
					sector.CurrSequence = new Cerebrum.ObjectHandle(Convert.ToInt64(root.Attributes["sequence"].Value));
				}
				groundIndex[importSectorGround] = sector.GroundHandle;
			}
			
			ImportComposite(workspace, importSectorGround, importIndex, plasmaIndex, groundIndex, sectorIndex);
		}

		private static void ImportComposite(Cerebrum.IWorkspace workspace, Cerebrum.ObjectHandle groundImportHandle, Cerebrum.Collections.Specialized.HandleObjectDictionary importIndex, Cerebrum.Collections.Specialized.HandleObjectDictionary plasmaIndex, Cerebrum.Collections.Specialized.HandleObjectDictionary groundIndex, Cerebrum.Collections.Specialized.HandleObjectDictionary sectorIndex)
		{
			object objptr = groundIndex[groundImportHandle];
			if(!(objptr is Cerebrum.ObjectHandle))
			{
				throw new System.Exception("GroundHandle = '" + groundImportHandle.ToString() + "' is not instantiated");
			}

			Cerebrum.ObjectHandle domainGroundHandle = (Cerebrum.ObjectHandle)objptr;
			System.Xml.XmlNode nodeComposite = (System.Xml.XmlNode)importIndex[groundImportHandle];
			using(System.IDisposable connector = ((Cerebrum.Runtime.NativeDomain)workspace).AttachComposite(domainGroundHandle, Cerebrum.ObjectHandle.Null, Cerebrum.ObjectHandle.Null))
			{
				if(connector is Cerebrum.IContainer)
				{
					foreach (System.Xml.XmlNode node in nodeComposite.SelectNodes("composite"))
					{
						System.Xml.XmlAttribute attrGroundHandle = node.Attributes["GroundHandle"];
						System.Xml.XmlAttribute attrPlasmaHandle = node.Attributes["PlasmaHandle"];
						Cerebrum.ObjectHandle drivenImportGround = Cerebrum.ObjectHandle.Parse(attrGroundHandle.Value);
						Cerebrum.ObjectHandle drivenDomainPlasma = ReMapPlasmaHandle(workspace, plasmaIndex, attrPlasmaHandle.Value);

						CreateComposite(workspace, connector as Cerebrum.IContainer, drivenDomainPlasma, drivenImportGround, importIndex, plasmaIndex, groundIndex, sectorIndex);
					}
				}
			}
		}

		private static void CreateComposite(Cerebrum.IWorkspace workspace, Cerebrum.IContainer container, Cerebrum.ObjectHandle drivenDomainPlasma, Cerebrum.ObjectHandle drivenImportGround, Cerebrum.Collections.Specialized.HandleObjectDictionary importIndex, Cerebrum.Collections.Specialized.HandleObjectDictionary plasmaIndex, Cerebrum.Collections.Specialized.HandleObjectDictionary groundIndex, Cerebrum.Collections.Specialized.HandleObjectDictionary sectorIndex)
		{
			object objptr = groundIndex[drivenImportGround];
			if(objptr==null)
			{
				System.Xml.XmlNode nodeDrivenComposite = (System.Xml.XmlNode)importIndex[drivenImportGround]; 
				System.Xml.XmlAttribute attrKindIdHandle = nodeDrivenComposite.Attributes["KindIdHandle"];
				System.Xml.XmlNode nodeDrivenCommponent = nodeDrivenComposite.SelectSingleNode("component");
				Cerebrum.ObjectHandle kindIdHandle = Cerebrum.ObjectHandle.Parse(attrKindIdHandle.Value);
				Cerebrum.IComposite drivenComposite = null;
				try
				{
					drivenComposite = container.AttachConnector(drivenDomainPlasma);
					if(drivenComposite==null)
					{
						System.Xml.XmlAttribute attrTypeIdHandle = nodeDrivenCommponent==null?null:nodeDrivenCommponent.Attributes["TypeIdPlasma"];
						if(attrTypeIdHandle!=null)
						{
							Cerebrum.ObjectHandle typeIdImportPlasma = Cerebrum.ObjectHandle.Parse(attrTypeIdHandle.Value);
							Cerebrum.ObjectHandle typeIdDomainPlasma = ReMapPlasmaHandle(workspace, plasmaIndex, attrTypeIdHandle.Value);
							Cerebrum.ObjectHandle typeIdImportGround = (Cerebrum.ObjectHandle)sectorIndex[typeIdImportPlasma];
							CreateComposite(workspace, workspace, typeIdDomainPlasma, typeIdImportGround, importIndex, plasmaIndex, groundIndex, sectorIndex);
							drivenComposite = container.CreateConnector(drivenDomainPlasma, typeIdDomainPlasma);
						}
						else
						{
							drivenComposite = (container as Cerebrum.Runtime.IContainerEx).CreateConnector(drivenDomainPlasma, kindIdHandle, Cerebrum.ObjectHandle.Null, null);
							//drivenComposite = container.AttachConnector(drivenDomainPlasma);
						}
					}
#warning COMMIT DRIVEN
					Cerebrum.ObjectHandle ohtmp = drivenComposite.GroundHandle;
					if(ohtmp==Cerebrum.ObjectHandle.Null)
					{
						if(drivenComposite is Cerebrum.Runtime.ICompositeEx)
						{
							(drivenComposite as Cerebrum.Runtime.ICompositeEx).FastenGround();
						}
						else
						{
							workspace.Savepoints.Flush();
						}
						ohtmp = drivenComposite.GroundHandle;
					}
					groundIndex[drivenImportGround] = ohtmp;

					if(kindIdHandle==Cerebrum.Runtime.KernelObjectKindId.Stream)
					{
						System.Xml.XmlNode nodeCompositeStream = nodeDrivenComposite.SelectSingleNode("stream");
						if(nodeCompositeStream!=null)
						{
							byte [] buffer = Convert.FromBase64String(nodeCompositeStream.InnerText);
							(drivenComposite as System.IO.Stream).Write(buffer, 0, buffer.Length);
						}
					}

					if(nodeDrivenCommponent!=null)
					{
						System.Xml.XmlAttribute attrTypeName = nodeDrivenCommponent.Attributes["typeName"];
						if(attrTypeName!=null)
						{
							System.Type type = System.Type.GetType(attrTypeName.Value, true);
							if(type == typeof(Cerebrum.ObjectHandle))
							{
								drivenComposite.Component = ReMapPlasmaHandle(workspace, plasmaIndex, nodeDrivenCommponent.Attributes["value"].Value);
							}
							else
							{
								System.Xml.XmlAttribute attrValue = nodeDrivenCommponent.Attributes["value"];
								if(attrValue!=null)
								{
									System.ComponentModel.TypeConverter tc = System.ComponentModel.TypeDescriptor.GetConverter(type);
									drivenComposite.Component = tc.ConvertFromInvariantString(attrValue.Value);
								}
								else
								{
									drivenComposite.Component = System.Activator.CreateInstance(type);
									if(drivenComposite.Component != null && (drivenComposite.Component is Cerebrum.IPersistent))
									{
										string val = nodeDrivenCommponent.InnerText;
										if (!(val == null || val == string.Empty ))
										{
											using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(val)))
											{
												(drivenComposite.Component as Cerebrum.IPersistent).Serialize(Cerebrum.SerializeDirection.Load, drivenComposite, ms);
											}
										}
									}
								}
							}
						}
						else
						{
							if(drivenComposite.Component == null || !(drivenComposite.Component is Cerebrum.IPersistent))
							{
								string val = nodeDrivenCommponent.InnerText;
								if (!(val == null || val == string.Empty ))
								{
									using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(val)))
									{
										System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
										object o = formatter.Deserialize(ms);
										if (!(o == null || Convert.IsDBNull(o)))
										{
											drivenComposite.Component = o;
										}
									}
								}
							}
							else
							{
								string val = nodeDrivenCommponent.InnerText;
								if (!(val == null || val == string.Empty ))
								{
									using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(val)))
									{
										(drivenComposite.Component as Cerebrum.IPersistent).Serialize(Cerebrum.SerializeDirection.Load, drivenComposite, ms);
									}
								}
							}
						}
					}

				}
				finally
				{
					if(drivenComposite!=null)
					{
						drivenComposite.Dispose();
					}
				}
				ImportComposite(workspace, drivenImportGround, importIndex, plasmaIndex, groundIndex, sectorIndex);
			}
			else
			{
				Cerebrum.ObjectHandle drivenDomainGround = (Cerebrum.ObjectHandle)objptr;
				using(System.IDisposable drivenConnector = ((Cerebrum.Runtime.NativeDomain)workspace).AttachComposite(drivenDomainGround, Cerebrum.ObjectHandle.Null, Cerebrum.ObjectHandle.Null))
				{
					if(drivenConnector is Cerebrum.IConnector)
					{
						Cerebrum.Integrator.Utilites.EngageConnector(container, drivenDomainPlasma, drivenConnector as Cerebrum.IConnector, true);
					}
				}
			}
		}

		public static void ImportDatabase(Cerebrum.IWorkspace workspace, System.Xml.XmlReader xr)
        {
            ImportDatabase(workspace, xr, null);
        }
		public static void ImportDatabase(Cerebrum.IWorkspace workspace, System.Xml.XmlReader xr, System.Collections.IDictionary plasmaMap)
		{
			System.Collections.IDictionary groundMap = new System.Collections.Hashtable();
			System.Collections.ArrayList lateBinding = new System.Collections.ArrayList();
			System.Xml.XmlDocument xd = new System.Xml.XmlDocument();
			xd.Load(xr);
			System.Xml.XmlNode root = xd.SelectSingleNode("SystemDatabase");
			if(root!=null)
			{
				if (plasmaMap == null)
				{
					System.Xml.XmlNode nodeGlobals = root.SelectSingleNode("Globals");
					if(nodeGlobals!=null)
					{
						using (Cerebrum.Runtime.NativeSector sector = workspace.GetSector() as Cerebrum.Runtime.NativeSector)
						{
							sector.CurrSequence = new Cerebrum.ObjectHandle(Convert.ToInt64(nodeGlobals.Attributes["SectorCurrentSequence"].Value));
						}
					}
				}

				using (Cerebrum.IConnector connector = workspace.AttachConnector(Cerebrum.Specialized.Concepts.TablesTableId))
				{

					Cerebrum.Reflection.TableDescriptor tblTables = connector.Component as Cerebrum.Reflection.TableDescriptor;
					Cerebrum.Data.TableView vwvTables = tblTables.GetTableView();
					Cerebrum.Data.TableSet tst = new Cerebrum.Data.TableSet(vwvTables);
					System.ComponentModel.PropertyDescriptorCollection prps = tst.GetItemProperties(null);

					Cerebrum.Data.TableView vwvTypes = (Cerebrum.Data.TableView)prps["Types"].GetValue(tst[0]);
					Cerebrum.Data.TableView vwvAttributes = (Cerebrum.Data.TableView)prps["Attributes"].GetValue(tst[0]);

					ImportTable(workspace, groundMap, plasmaMap, "Types", vwvTypes, root, lateBinding);
					ImportTable(workspace, groundMap, plasmaMap, "Attributes", vwvAttributes, root, lateBinding);

					Cerebrum.Data.TableView vwvFields = (Cerebrum.Data.TableView)prps["Fields"].GetValue(tst[0]);

					ImportTable(workspace, groundMap, plasmaMap, "Fields", vwvFields, root, lateBinding);
					ImportTable(workspace, groundMap, plasmaMap, "Tables", vwvTables, root, lateBinding);

					vwvFields.Dispose();
					vwvTables.Dispose();

					tblTables.ResetDefaultView();

					vwvTables = tblTables.GetTableView();
					tst.Dispose();
					tst = new Cerebrum.Data.TableSet(vwvTables);
					prps = tst.GetItemProperties(null);

					//System.ComponentModel.PropertyDescriptor prpt = prps["UiServices"];

					foreach (System.ComponentModel.PropertyDescriptor prp in prps)
					{
						if (prp.Name == "Types" || prp.Name == "Attributes" || prp.Name == "Fields" || prp.Name == "Tables") continue;

						System.ComponentModel.IBindingList list = (System.ComponentModel.IBindingList)prp.GetValue(tst[0]);
						if (list is Cerebrum.Data.TableView)
						{
							ImportTable(workspace, groundMap, plasmaMap, prp.Name, list as Cerebrum.Data.TableView, root, lateBinding);
						}
						//if(list is System.IDisposable) ((System.IDisposable)list).Dispose();
					}
					foreach (VectorLateBinding entry in lateBinding)
					{
						System.ComponentModel.PropertyDescriptor prpTbl = prps[entry.TableName];
						Cerebrum.Data.TableView list = prpTbl.GetValue(tst[0]) as Cerebrum.Data.TableView;
						if (list != null)
						{
							System.ComponentModel.PropertyDescriptorCollection itemPrps = list.GetItemProperties(null);
							System.ComponentModel.PropertyDescriptor prpFld = itemPrps[entry.FieldName];
							using (Cerebrum.Data.VectorView attrList = (Cerebrum.Data.VectorView)prpFld.GetValue(list.CreateItemViewByObjectHandle(entry.ComponentPlasma)))
							{
								Cerebrum.ObjectHandle hGR = Cerebrum.ObjectHandle.Null;
								if(entry.AttributeGround!=null)
								{
									hGR = ReMapGroundHandle(workspace, groundMap, entry.AttributeGround, null);
								}
								if(hGR==Cerebrum.ObjectHandle.Null)
								{
									System.ComponentModel.PropertyDescriptor attrNH = attrList.GetItemProperties(null)[Cerebrum.Specialized.KnownNames.ObjectHandle];
									Cerebrum.Data.ComponentItemView itemNH = attrList.AddNew();
									attrNH.SetValue(itemNH, entry.AttributePlasma);
								}
								else
								{
									using(Cerebrum.IComposite cmpstAttr = (Cerebrum.IComposite)(workspace as Cerebrum.Runtime.NativeDomain).AttachComposite(hGR, entry.AttributePlasma, Cerebrum.ObjectHandle.Null))
									{
										if (attrList.ContainsObject(entry.AttributePlasma))
										{
											attrList.FindItemViewByObjectHandle(entry.AttributePlasma).SetConnector(entry.AttributePlasma, cmpstAttr);
										}
										else
										{
											attrList.AddNew().SetConnector(entry.AttributePlasma, cmpstAttr);
										}
									}
								}
							}
						}
					}
					tst.Dispose();
					vwvTables.Dispose();
					vwvTypes.Dispose();
					vwvAttributes.Dispose();
				}
			}
			root = xd.SelectSingleNode("workspace");
			if(root!=null)
			{
				ImportWorkspace(workspace, root);
				//FixrelWorkspace(workspace);
			}
			workspace.Savepoints.Flush();
		}
        public static void ImportTable(Cerebrum.IWorkspace workspace, System.Collections.IDictionary groundMap, System.Collections.IDictionary plasmaMap, string name, Cerebrum.Data.TableView tblView, System.Xml.XmlNode root, System.Collections.ArrayList lateBinding)
        {
			System.Collections.ArrayList itemList = new System.Collections.ArrayList();
            System.Xml.XmlNodeList nodes = root.SelectNodes(name);
            System.ComponentModel.PropertyDescriptorCollection itemPrps = tblView.GetItemProperties(null);

            foreach (System.Xml.XmlNode node in nodes)
            {
                Cerebrum.Data.ComponentItemView item = null;
				System.Xml.XmlAttribute xObject = node.Attributes[Cerebrum.Specialized.KnownNames.ObjectHandle];
				System.Xml.XmlAttribute xPlasma = node.Attributes[Cerebrum.Specialized.KnownNames.PlasmaHandle];
				System.Xml.XmlAttribute xGround = node.Attributes[Cerebrum.Specialized.KnownNames.GroundHandle];
				bool isSectorPlasma;

				Cerebrum.ObjectHandle h = Cerebrum.ObjectHandle.Null;
				if(xObject!=null)
				{
					h = ReMapPlasmaHandle(workspace, plasmaMap, xObject.Value);
					isSectorPlasma = true;
				}
				else 
				{
					h = ReMapPlasmaHandle(workspace, plasmaMap, xPlasma.Value);
					isSectorPlasma = false;
				}

				Cerebrum.ObjectHandle g = Cerebrum.ObjectHandle.Null;

				if (tblView.ContainsObject(h))
                {
                    item = tblView.CreateItemViewByObjectHandle(h);

					if(xGround != null)
					{
						g = ReMapGroundHandle(workspace, groundMap, xGround.Value, item);
					}
                }
                else
                {
					if(xGround != null)
					{
						g = ReMapGroundHandle(workspace, groundMap, xGround.Value, null);
					}

					Cerebrum.IComposite composite = null;

					bool needAttachToSector = true;
					if(g != Cerebrum.ObjectHandle.Null)
					{
						composite = (Cerebrum.IComposite)(workspace as Cerebrum.Runtime.NativeDomain).AttachComposite(g, h, Cerebrum.ObjectHandle.Null);
					}
					else
					{
						if(isSectorPlasma)
						{
							composite = workspace.AttachConnector(h);
							needAttachToSector = false;
						}
					}

					if (composite == null)
                    {
                        Cerebrum.ObjectHandle TypeIdHandle = Cerebrum.ObjectHandle.Null;

                        string id = null;
                        System.Xml.XmlNode n = node.Attributes[Cerebrum.Specialized.KnownNames.TypeIdHandle];
                        if (n != null)
                        {
                            id = n.Value;
                        }
                        if (id != null && id.Length > 0)
                        {
                            TypeIdHandle = ReMapPlasmaHandle(workspace, plasmaMap, id);
                        }

						if(isSectorPlasma)
						{
							if (TypeIdHandle == Cerebrum.ObjectHandle.Null)
							{
								item = tblView.AddNew(h);
							}
							else
							{
								item = tblView.AddNew(h, TypeIdHandle);
							}
						}
						else
						{
							if (TypeIdHandle == Cerebrum.ObjectHandle.Null)
							{
								item =  tblView[tblView.CreateConnector(h, tblView.TableDescriptor.ComponentTypeIdHandle)];
							}
							else
							{
								item =  tblView[tblView.CreateConnector(h, TypeIdHandle)];
							}
						}
                    }
                    else
                    {
                        item = tblView[tblView.EngageConnector(h, composite)];
						if(isSectorPlasma && needAttachToSector)
						{
							using(Cerebrum.IComposite comp22 = workspace.AttachConnector(h))
							{
								if(comp22==null)
								{
									workspace.EngageConnector(h, composite);
								}
								//TODO check ground collision
								/*else
								{
								}*/
							}
						}
						composite.Dispose();
                    }

					if(xGround != null && g == Cerebrum.ObjectHandle.Null)
					{
						g = ReMapGroundHandle(workspace, groundMap, xGround.Value, item);
					}
				}

                foreach (System.ComponentModel.PropertyDescriptor itemPrp in itemPrps)
                {
					Cerebrum.Data.AttributePropertyDescriptor apd = itemPrp as Cerebrum.Data.AttributePropertyDescriptor;
					Cerebrum.ObjectHandle kc = Cerebrum.Runtime.KernelObjectKindId.Scalar;
					if (apd!=null)
					{
						using(Cerebrum.IConnector tdc = workspace.AttachConnector(apd.AttributeTypeIdHandle))
						{
							if(tdc!=null)
							{
								kc = ((Cerebrum.Reflection.TypeDescriptor)tdc.Component).KernelObjectKindId;
							}
						}
					}
					
					if (kc == Cerebrum.Runtime.KernelObjectKindId.Stream)
					{
						System.Xml.XmlNode itemNode = node.SelectSingleNode(itemPrp.Name);
						if (itemNode != null)
						{
							string val = itemNode.InnerText;
							if (!(val == null || val == string.Empty ))
							{
								itemPrp.SetValue(item, Convert.FromBase64String(val));
							}
						}
					}
					else if (itemPrp.PropertyType == typeof(System.ComponentModel.IBindingList))
                    {
                        if (name == "Tables" && itemPrp.Name == "SelectedComponents") continue;
                        System.Xml.XmlNodeList listItems = node.SelectNodes(itemPrp.Name);
                        if (listItems != null)
                        {
                            //import table attributes
                            using (Cerebrum.Data.VectorView attrList = (Cerebrum.Data.VectorView)itemPrp.GetValue(item))
                            {
                                System.ComponentModel.PropertyDescriptor attrNH = attrList.GetItemProperties(null)[Cerebrum.Specialized.KnownNames.ObjectHandle];
                                foreach (System.Xml.XmlNode n in listItems)
                                {
                                    Cerebrum.ObjectHandle hNH = ReMapPlasmaHandle(workspace, plasmaMap, n.Attributes[Cerebrum.Specialized.KnownNames.ObjectHandle].Value);
									Cerebrum.ObjectHandle hGR = Cerebrum.ObjectHandle.Null;
									System.Xml.XmlAttribute xGrnd = n.Attributes[Cerebrum.Specialized.KnownNames.GroundHandle];
									if(xGrnd!=null)
									{
										hGR = ReMapGroundHandle(workspace, groundMap, xGrnd.Value, null);
										if(hGR != Cerebrum.ObjectHandle.Null)
										{
											using(Cerebrum.IComposite cmpstAttr = (Cerebrum.IComposite)(workspace as Cerebrum.Runtime.NativeDomain).AttachComposite(hGR, hNH, Cerebrum.ObjectHandle.Null))
											{
												if (attrList.ContainsObject(hNH))
												{
													attrList.FindItemViewByObjectHandle(hNH).SetConnector(hNH, cmpstAttr);
												}
												else
												{
													attrList.AddNew().SetConnector(hNH, cmpstAttr);
												}
											}
										}
										else
										{
											VectorLateBinding entry = new VectorLateBinding();
											entry.TableName = name;
											entry.FieldName = itemPrp.Name;
											entry.ComponentPlasma = item.ObjectHandle;
											entry.AttributePlasma = hNH;
											entry.AttributeGround = xGrnd.Value;
											lateBinding.Add(entry);
										}
									}
									else
									{
										if (!attrList.ContainsObject(hNH))
										{
											using (Cerebrum.IConnector iconnector = workspace.AttachConnector(hNH))
											{
												if (iconnector != null)
												{
													//attrNH.SetValue(attrList.AddNew(), hNH);
													attrList.EngageConnector(hNH, iconnector);
												}
												else
												{
													VectorLateBinding entry = new VectorLateBinding();
													entry.TableName = name;
													entry.FieldName = itemPrp.Name;
													entry.ComponentPlasma = item.ObjectHandle;
													entry.AttributePlasma = hNH;
													entry.AttributeGround = null;
													lateBinding.Add(entry);
												}
											}
										}
									}
                                }
                            }
                        }
                    }
                    else if (itemPrp.PropertyType == typeof(object))
                    {
                        System.Xml.XmlNode itemNode = node.SelectSingleNode(itemPrp.Name);
                        if (itemNode != null)
                        {
                            string val = itemNode.InnerText;
                            if (!(val == null || val == string.Empty ))
                            {
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(val)))
                                {
                                    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                                    object o = formatter.Deserialize(ms);
                                    if (!(o == null || Convert.IsDBNull(o)))
                                    {
                                        itemPrp.SetValue(item, o);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        System.Xml.XmlNode itemNode = node.Attributes[itemPrp.Name];
                        if (itemNode != null)
                        {
                            string val = itemNode.Value;
                            if (itemPrp.PropertyType == typeof(Cerebrum.ObjectHandle))
                            {
                                if (val == null || val == string.Empty )
                                {
                                    itemPrp.SetValue(item, Cerebrum.ObjectHandle.Null);
                                }
                                else
                                {
                                    itemPrp.SetValue(item, ReMapPlasmaHandle(workspace, plasmaMap, val));
                                }
                            }
                            else if (itemPrp.PropertyType == typeof(System.DateTime))
                            {
                                if (!(val == null || val == string.Empty ))
                                {
									System.DateTime d = System.DateTime.ParseExact(val, new string[] {"s", "R", "G"}, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                                    itemPrp.SetValue(item, d);
                                }
                            }
                            else
                            {
                                if (!(val == null || val == string.Empty ))
                                {
                                    itemPrp.SetValue(item, val);
                                }
                            }
                        }
                    }
                }
				itemList.Add(item);
            }
			if(name=="Tables")
			{
				foreach(Cerebrum.Data.ComponentItemView item in itemList)
				{
					using(Cerebrum.IConnector connector = item.GetComposite())
					{
						if(connector.Component is Cerebrum.Reflection.TableDescriptor)
						{
							((Cerebrum.Reflection.TableDescriptor)connector.Component).ResetDefaultView();
						}
					}
				}
			}
        }
		public static Cerebrum.ObjectHandle ReMapGroundHandle(Cerebrum.IWorkspace workspace, System.Collections.IDictionary groundMap, string shandle, Cerebrum.Data.ComponentItemView item)
		{
            Cerebrum.ObjectHandle handle = Cerebrum.ObjectHandle.Null;
			if (shandle[0] == '?')
			{
				string path = shandle.Substring(1);
				int dot = path.IndexOf('.');
				int eqv = path.IndexOf('=', dot + 1);
				int q01 = path.IndexOf('\'', eqv + 1);
				int q02 = path.IndexOf('\'', q01 + 1);
				string table = path.Substring(0, dot);
				string field = path.Substring(dot + 1, eqv - dot - 1);
				string value = path.Substring(q01 + 1, q02 - q01 - 1);

				using (Cerebrum.IConnector connector = Cerebrum.Integrator.DomainContext.FromConnector(workspace).GetTable(table))
				{
					using (Cerebrum.Data.TableView view = (connector.Component as Cerebrum.Reflection.TableDescriptor).GetTableView())
					{
						System.ComponentModel.PropertyDescriptor descriptor = view.GetItemProperties(null)[field];
						Cerebrum.ObjectHandle plasma = FindHandle(view, descriptor, value);
						using(Cerebrum.IComposite con2 = view.CreateItemViewByObjectHandle(plasma).GetComposite())
						{
							Cerebrum.ObjectHandle newHandle = con2.GroundHandle;
							if(newHandle == Cerebrum.ObjectHandle.Null && con2 is Cerebrum.Runtime.ICompositeEx)
							{
								(con2 as Cerebrum.Runtime.ICompositeEx).FastenGround();
								newHandle = con2.GroundHandle;
							}
							return newHandle;
						}
					}
				}

			}
			else
			{
				handle = Cerebrum.ObjectHandle.Parse(shandle);
			}
			if (groundMap.Contains(handle))
			{
				return (Cerebrum.ObjectHandle)groundMap[handle];
			}
			else
			{
				if(item!=null)
				{
					using(Cerebrum.IComposite composite = item.GetComposite())
					{
						Cerebrum.ObjectHandle newHandle = composite.GroundHandle;
						if(newHandle==Cerebrum.ObjectHandle.Null && composite is Cerebrum.Runtime.ICompositeEx)
						{
							(composite as Cerebrum.Runtime.ICompositeEx).FastenGround();
							newHandle = composite.GroundHandle;
						}
						groundMap.Add(handle, newHandle);
						return newHandle;
					}
				}
			}
			return Cerebrum.ObjectHandle.Null;
		}
        public static Cerebrum.ObjectHandle ReMapPlasmaHandle(Cerebrum.IWorkspace workspace, System.Collections.IDictionary plasmaMap, string shandle)
        {
            Cerebrum.ObjectHandle handle;
            if (shandle[0] == '#')
            {
                handle = Cerebrum.ObjectHandle.Parse(shandle.Substring(1));
                return handle;
            }
            else if (shandle[0] == '?')
            {
                string path = shandle.Substring(1);
                int dot = path.IndexOf('.');
                int eqv = path.IndexOf('=', dot + 1);
                int q01 = path.IndexOf('\'', eqv + 1);
                int q02 = path.IndexOf('\'', q01 + 1);
                string table = path.Substring(0, dot);
                string field = path.Substring(dot + 1, eqv - dot - 1);
                string value = path.Substring(q01 + 1, q02 - q01 - 1);

                using (Cerebrum.IConnector connector = Cerebrum.Integrator.DomainContext.FromConnector(workspace).GetTable(table))
                {
                    using (Cerebrum.Data.TableView view = (connector.Component as Cerebrum.Reflection.TableDescriptor).GetTableView())
                    {
                        System.ComponentModel.PropertyDescriptor descriptor = view.GetItemProperties(null)[field];
                        handle = FindHandle(view, descriptor, value);
                        return handle;
                    }
                }

            }
            else
            {
                handle = Cerebrum.ObjectHandle.Parse(shandle);
            }
            if (plasmaMap == null) return handle;
            if (((System.IComparable)handle).CompareTo(Cerebrum.Specialized.Concepts.FirstUserHandle) < 0)
            {
                return handle;
            }
            else
            {
                if (plasmaMap.Contains(handle))
                {
                    return (Cerebrum.ObjectHandle)plasmaMap[handle];
                }
                else
                {
                    Cerebrum.ObjectHandle newHandle = workspace.NextSequence();
                    plasmaMap.Add(handle, newHandle);
                    return newHandle;
                }
            }
        }

        public static Cerebrum.Data.TableView GetView(Cerebrum.Integrator.DomainContext domainContext, string tableName)
        {
            using (Cerebrum.IConnector connector = domainContext.GetTable(tableName))
            {
				if(connector==null)
				{
					return null;
				}
				else
				{
					Cerebrum.Reflection.TableDescriptor tbd = connector.Component as Cerebrum.Reflection.TableDescriptor;
					return tbd == null ? null : tbd.GetTableView();
				}
            }
            /*using(Cerebrum.Data.TableView vwvTables = domainContext.TablesView)
            {
                using(Cerebrum.Data.TableSet tst = new Cerebrum.Data.TableSet(vwvTables))
                {
                    System.ComponentModel.PropertyDescriptorCollection prps = tst.GetItemProperties(null);
                    System.ComponentModel.PropertyDescriptor prp = prps[tableName];
			
                    return prp==null?null:((Cerebrum.Data.TableView)prp.GetValue(tst[0]));
                }
            }*/
        }
		/*private static void FixrelWorkspace(Cerebrum.IWorkspace workspace)
		{
			using (Cerebrum.IConnector conTables = workspace.AttachConnector(Cerebrum.Specialized.Concepts.TablesTableId))
			{

				Cerebrum.Reflection.TableDescriptor tblTables = conTables.Component as Cerebrum.Reflection.TableDescriptor;
				using(Cerebrum.Data.TableView vwvTables = tblTables.GetTableView())
				{
					for (int i = 0; i < vwvTables.Count; i++)
					{
						using (Cerebrum.IConnector conTable = vwvTables[i].GetComposite())
						{
							Cerebrum.Reflection.TableDescriptor table = conTable.Component as Cerebrum.Reflection.TableDescriptor;
							using (Cerebrum.Runtime.NativeWarden columns = table.GetSelectedAttributesVector())
							{
								System.Collections.ArrayList list = new System.Collections.ArrayList();
								System.Collections.IEnumerator columnsEnu = columns.GetEnumerator();
								while(columnsEnu.MoveNext())
								{
									System.Collections.DictionaryEntry de = (System.Collections.DictionaryEntry)columnsEnu.Current;
									Cerebrum.ObjectHandle h = (Cerebrum.ObjectHandle)de.Key;
									/--*using(Cerebrum.IComposite connector = columns.AttachConnector(h))
									{
										if(connector.Component is Cerebrum.Reflection.FieldDescriptor)
										{
											list.Add((connector.Component as Cerebrum.Reflection.FieldDescriptor).AttributePlasmaHandle);
										}
									}*--/
								}
								foreach(Cerebrum.ObjectHandle h in list)
								{
									columns.RemoveConnector(h);
								}
							}
						}
					}
				}
			}
		}*/


        private static System.Collections.Hashtable m_ResolverHashTable = null;

        /// <summary>
        /// ���������� ObjectHandle �������� �� ��������.
        /// </summary>
        /// <param name="name">�������� ��������</param>
        /// <returns>ObjectHandle ��������</returns>
		public static Cerebrum.ObjectHandle ResolveHandle(Cerebrum.Integrator.DomainContext context, string TableName, string FieldName, object value)
		{
			object key = Utilites.GetResolverKey(TableName, FieldName);
			System.Collections.Hashtable resolver = context.Properties[key] as System.Collections.Hashtable;

			if (resolver == null)
			{
				resolver = new System.Collections.Hashtable();
				RefreshResolver(resolver, context, TableName, FieldName);
				context.Properties[key] = resolver;
			}

			object h = resolver[value];
			if (h == null)
			{
				return Cerebrum.ObjectHandle.Null;
			}
			else
			{
				return (Cerebrum.ObjectHandle)h;
			}
		}
        private static object GetResolverKey(string TableName, string FieldName)
        {
            if (m_ResolverHashTable == null)
            {
                m_ResolverHashTable = new System.Collections.Hashtable();
            }

            string keyName = TableName + "." + FieldName;
            object key;
            if (m_ResolverHashTable.ContainsKey(keyName))
            {
                key = m_ResolverHashTable[keyName];
            }
            else
            {
                key = new object();
                m_ResolverHashTable[keyName] = key;
            }
			return key;
        }
		private static void RefreshResolver(System.Collections.Hashtable resolver, Cerebrum.Integrator.DomainContext context, string TableName, string FieldName)
		{
			// ���������� ������ ��������� ���� Cerebrum.
			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(context, TableName))
			{
				System.ComponentModel.PropertyDescriptor pd = view.GetItemProperties(null)[FieldName];
				// �������� ������ ��������� � ���� ��������.
				for (int i = 0; i < view.Count; i++)
				{
					object v = pd.GetValue(view[i]);
					resolver[v] = view[i].ObjectHandle;
				}
			}
		}

		public static void RefreshHandle(Cerebrum.Integrator.DomainContext context, string TableName, string FieldName, Cerebrum.ObjectHandle handle)
		{
			object key = Utilites.GetResolverKey(TableName, FieldName);
			System.Collections.Hashtable resolver = context.Properties[key] as System.Collections.Hashtable;

			if (resolver == null)
			{
				resolver = new System.Collections.Hashtable();
				RefreshResolver(resolver, context, TableName, FieldName);
				context.Properties[key] = resolver;
			}
			else
			{
				using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(context, TableName))
				{
					if(view.ContainsObject(handle))
					{
						System.ComponentModel.PropertyDescriptor pd = view.GetItemProperties(null)[FieldName];
						Cerebrum.Data.ComponentItemView item = view.CreateItemViewByObjectHandle(handle);
						object v = pd.GetValue(item);
						resolver[v] = handle;
					}
				}
			}
		}

		public static void RefreshHandles(Cerebrum.Integrator.DomainContext context, string TableName, string FieldName)
		{
			object key = Utilites.GetResolverKey(TableName, FieldName);
			System.Collections.Hashtable resolver = new System.Collections.Hashtable();
			RefreshResolver(resolver, context, TableName, FieldName);
			context.Properties[key] = resolver;
		}

        public static Cerebrum.ObjectHandle FindHandle(Cerebrum.Data.TableView view, System.ComponentModel.PropertyDescriptor prp, string value)
        {
            int i = view.Find(prp, value);
            if (i >= 0)
            {
                return view[i].ObjectHandle;
            }
            else
            {
                return Cerebrum.ObjectHandle.Null;
            }
        }

		private static Cerebrum.Security.CerebrumPrincipal LoadPrincipal(Cerebrum.ObjectHandle prh, Cerebrum.Management.Security.Reflection.PrincipalDescriptor prd, bool isAuthenticated)
		{
			Cerebrum.Security.CerebrumIdentity cid = new Cerebrum.Security.CerebrumIdentity(prh, prd.Name, prd.DisplayName, prd.Description, "Cerebrum.Security", isAuthenticated);
			System.Collections.Hashtable hshPriveleges = new System.Collections.Hashtable();
			System.Collections.ArrayList arrPriveleges = new System.Collections.ArrayList();
			System.Collections.ArrayList arrWorkgroups = new System.Collections.ArrayList();
			using(Cerebrum.IContainer workgroups = prd.GetSelectedWorkgroupsVector())
			{
				if(workgroups!=null)
				{
					foreach(System.Collections.DictionaryEntry de in (System.Collections.IEnumerable)workgroups)
					{
						Cerebrum.ObjectHandle wh = (Cerebrum.ObjectHandle)de.Key;
						using(Cerebrum.IComposite workgroup = workgroups.AttachConnector(wh))
						{
							Cerebrum.Management.Security.Reflection.WorkgroupDescriptor wgd = workgroup.Component as Cerebrum.Management.Security.Reflection.WorkgroupDescriptor;
							arrWorkgroups.Add(new Cerebrum.Specialized.StringAttributeEntry(wh, wgd.Name));

							using(Cerebrum.IContainer priveleges = wgd.GetSelectedPrivilegesVector())
							{
								if(priveleges!=null)
								{
									foreach(System.Collections.DictionaryEntry ve in (System.Collections.IEnumerable)priveleges)
									{
										Cerebrum.ObjectHandle vh = (Cerebrum.ObjectHandle)ve.Key;
										using(Cerebrum.IComposite privelege = priveleges.AttachConnector(vh))
										{
											Cerebrum.Management.Security.Reflection.PrivilegeDescriptor vgd = privelege.Component as Cerebrum.Management.Security.Reflection.PrivilegeDescriptor;
											if(!hshPriveleges.ContainsKey(vh))
											{
												hshPriveleges.Add(vh, vh);
												arrPriveleges.Add(new Cerebrum.Specialized.StringAttributeEntry(vh, vgd.Name));
											}
										}
									}
								}
							}
						}
					}
				}
			}
			using(Cerebrum.IContainer priveleges = prd.GetSelectedPrivilegesVector())
			{
				if(priveleges!=null)
				{
					foreach(System.Collections.DictionaryEntry ve in (System.Collections.IEnumerable)priveleges)
					{
						Cerebrum.ObjectHandle vh = (Cerebrum.ObjectHandle)ve.Key;
						using(Cerebrum.IComposite privelege = priveleges.AttachConnector(vh))
						{
							Cerebrum.Management.Security.Reflection.PrivilegeDescriptor vgd = privelege.Component as Cerebrum.Management.Security.Reflection.PrivilegeDescriptor;
							if(!hshPriveleges.ContainsKey(vh))
							{
								hshPriveleges.Add(vh, vh);
								arrPriveleges.Add(new Cerebrum.Specialized.StringAttributeEntry(vh, vgd.Name));
							}
						}
					}
				}
			}
			return new Cerebrum.Security.CerebrumPrincipal(cid, (Cerebrum.Specialized.StringAttributeEntry[])arrWorkgroups.ToArray(typeof(Cerebrum.Specialized.StringAttributeEntry)), (Cerebrum.Specialized.StringAttributeEntry[])arrPriveleges.ToArray(typeof(Cerebrum.Specialized.StringAttributeEntry)));
		}
		public static Cerebrum.Security.CerebrumPrincipal Authenticate(Cerebrum.Integrator.DomainContext context, string name, string password)
		{
			using(Cerebrum.Data.TableView principals = GetView(context, "Principals"))
			{
				if(principals==null) 
				{
					return null;
				}
				Cerebrum.ObjectHandle prh = FindHandle(principals, principals.GetItemProperties(null)["Name"], name);
				if(prh==Cerebrum.ObjectHandle.Null)
				{
					return null;
				}
				Cerebrum.Data.ComponentItemView item = principals.CreateItemViewByObjectHandle(prh);
				using(Cerebrum.IComposite principal = item.GetComposite())
				{
					if(principal==null) 
					{
						return null;
					}
					Cerebrum.Management.Security.Reflection.PrincipalDescriptor prd = principal.Component as Cerebrum.Management.Security.Reflection.PrincipalDescriptor;
					if(prd==null)
					{
						return null;
					}
					if(prd.Password != password)
					{
						return null;
					}

					return LoadPrincipal(prh, prd, true);
				}
			}
		}
		public static Cerebrum.Security.CerebrumPrincipal LoadPrincipal(Cerebrum.Integrator.DomainContext context, Cerebrum.ObjectHandle principalHandle, bool isAuthenticated)
		{
			if(principalHandle==Cerebrum.ObjectHandle.Null)
			{
				return null;
			}
			using(Cerebrum.Data.TableView principals = GetView(context, "Principals"))
			{
				if(principals==null) 
				{
					return null;
				}

				Cerebrum.Data.ComponentItemView item = principals.CreateItemViewByObjectHandle(principalHandle);
				using(Cerebrum.IComposite principal = item.GetComposite())
				{
					if(principal==null) 
					{
						return null;
					}
					Cerebrum.Management.Security.Reflection.PrincipalDescriptor prd = principal.Component as Cerebrum.Management.Security.Reflection.PrincipalDescriptor;
					if(prd==null)
					{
						return null;
					}

					return LoadPrincipal(principalHandle, prd, isAuthenticated);
				}
			}
		}

        private class VectorLateBinding
        {
            public string TableName;
            public string FieldName;
            public Cerebrum.ObjectHandle ComponentPlasma;
            public Cerebrum.ObjectHandle AttributePlasma;
            public string AttributeGround;
        }
    }
}

