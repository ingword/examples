// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Management.Security.Reflection
{
	/// <summary>
	/// Summary description for PrincipalDescriptor.
	/// </summary>
	public class PrincipalDescriptor : Cerebrum.Reflection.MemberDescriptor
	{
		public PrincipalDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string Password
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Management.Specialized.Concepts.PasswordAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Management.Specialized.Concepts.PasswordAttribute(this.DomainContext), value);
			}
		}

		public Cerebrum.Runtime.NativeWarden GetSelectedWorkgroupsVector()
		{
			return this.GetAttributeContainer(Cerebrum.Management.Specialized.Concepts.SelectedWorkgroupsAttribute(this.DomainContext)) as Cerebrum.Runtime.NativeWarden;
		}

		public Cerebrum.Runtime.NativeWarden GetSelectedPrivilegesVector()
		{
			return this.GetAttributeContainer(Cerebrum.Management.Specialized.Concepts.SelectedPrivilegesAttribute(this.DomainContext)) as Cerebrum.Runtime.NativeWarden;
		}

		public string EmailAddress
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Management.Specialized.Concepts.EmailAddressAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Management.Specialized.Concepts.EmailAddressAttribute(this.DomainContext), value);
			}
		}

		public string HomePageURL
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Management.Specialized.Concepts.HomePageURLAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Management.Specialized.Concepts.HomePageURLAttribute(this.DomainContext), value);
			}
		}

		public bool Disabled
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Management.Specialized.Concepts.DisabledAttribute(this.DomainContext));
				if(o==null || Convert.IsDBNull(o))
				{
					return false;
				}
				else
				{
					return Convert.ToBoolean(o);
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.Management.Specialized.Concepts.DisabledAttribute(this.DomainContext), value);
			}
		}

		public bool Internal
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Management.Specialized.Concepts.InternalAttribute(this.DomainContext));
				if(o==null || Convert.IsDBNull(o))
				{
					return false;
				}
				else
				{
					return Convert.ToBoolean(o);
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.Management.Specialized.Concepts.InternalAttribute(this.DomainContext), value);
			}
		}
		
		public System.DateTime CreatedDateTime
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Management.Specialized.Concepts.CreatedDateTimeAttribute(this.DomainContext));
				if(o==null || Convert.IsDBNull(o))
				{
					return System.DateTime.MinValue;
				}
				else
				{
					return Convert.ToDateTime(o);
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.Management.Specialized.Concepts.CreatedDateTimeAttribute(this.DomainContext), value);
			}
		}

		public System.DateTime UpdatedDateTime
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Management.Specialized.Concepts.UpdatedDateTimeAttribute(this.DomainContext));
				if(o==null || Convert.IsDBNull(o))
				{
					return System.DateTime.MinValue;
				}
				else
				{
					return Convert.ToDateTime(o);
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.Management.Specialized.Concepts.UpdatedDateTimeAttribute(this.DomainContext), value);
			}
		}
	}
}
