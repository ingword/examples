// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Management.Security.Reflection
{
	/// <summary>
	/// Summary description for WorkgroupDescriptor.
	/// </summary>
	public class WorkgroupDescriptor : Cerebrum.Reflection.MemberDescriptor
	{
		public WorkgroupDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public Cerebrum.Runtime.NativeWarden GetSelectedPrivilegesVector()
		{
			return this.GetAttributeContainer(Cerebrum.Management.Specialized.Concepts.SelectedPrivilegesAttribute(this.DomainContext)) as Cerebrum.Runtime.NativeWarden;
		}
	}
}
