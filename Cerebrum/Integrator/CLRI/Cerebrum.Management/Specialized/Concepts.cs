// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Management.Specialized
{
	/// <summary>
	/// Summary description for Concepts.
	/// </summary>
	/// <summary>
	/// Summary description for Reflection.
	/// </summary>
	public sealed class Concepts
	{
		private Concepts()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static Cerebrum.ObjectHandle PasswordAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "Password");
		}

		public static Cerebrum.ObjectHandle EmailAddressAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "EmailAddress");
		}

		public static Cerebrum.ObjectHandle HomePageURLAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "HomePageURL");
		}

		public static Cerebrum.ObjectHandle DisabledAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "Disabled");
		}

		public static Cerebrum.ObjectHandle InternalAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "Internal");
		}

		public static Cerebrum.ObjectHandle CreatedDateTimeAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "CreatedDateTime");
		}

		public static Cerebrum.ObjectHandle UpdatedDateTimeAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "UpdatedDateTime");
		}

		public static Cerebrum.ObjectHandle SelectedPrivilegesAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "SelectedPrivileges");
		}

		public static Cerebrum.ObjectHandle SelectedWorkgroupsAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "SelectedWorkgroups");
		}
	}
}
