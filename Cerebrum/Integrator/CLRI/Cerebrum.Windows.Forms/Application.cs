// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms
{


	/// <summary>
	/// ����� Application ������������ ����� ��������� ����� ����������. ����� �����
	/// ������������ ������� ����� ����� ����������. 
	/// </summary>
	public abstract class Application : Cerebrum.Integrator.Application
	{
		protected internal static readonly object PrimaryWindowChangedEvent = new object();
//		protected internal static readonly object ExecutingEvent = new object();

		/// <summary>
		/// ����������� ������ �� ��������� ������� ����������.
		/// ������ ���������� ������ ������������ � ����� ����������.
		/// </summary>
		protected static Cerebrum.Windows.Forms.Application m_Instance;
		/// <summary>
		/// �������� ������������ ��������� ����������
		/// </summary>
		public static Application Instance
		{
			get
			{
				return m_Instance;
			}
			set
			{
				m_Instance = value;
			}
		}


		/// <summary>
		/// ����������� ������� ����������. 
		/// </summary>
		public Application()
		{
		}

		public string ApplicationDirectory
		{
			get
			{
				return System.IO.Path.GetDirectoryName(this.GetType().Assembly.Location);
			}
		}

		/// <summary>
		/// ����� ������� ������ ���� ���. ������� �
		/// �������������� ��������� ��������.
		/// </summary>
		public override void Initialize()
		{
//-			m_AccessId = UserAccessModes.UnknownAccess;
			base.Initialize();
		}

		protected System.Windows.Forms.Form m_PrimaryWindow = null;

			/// <summary>
			/// �������� ������������ � ��������������� ������� �������� ���� ����������.
			/// ������������ ����������� � ���������� m_PrimaryWindow_Clearing � ��� 
			/// ������� Clearing ���������� IReleasable.
			/// </summary>
			public System.Windows.Forms.Form PrimaryWindow
			{
				get
				{
					return m_PrimaryWindow;
				}
				set
				{
					SetPrimaryWindow(value);
				}
			}

		protected virtual void SetPrimaryWindow(System.Windows.Forms.Form primaryWindow)
		{
			if(m_PrimaryWindow!=primaryWindow)
			{
				if(m_PrimaryWindow!=null)
				{
					m_PrimaryWindow_Disposing(m_PrimaryWindow, System.EventArgs.Empty);
				}
				m_PrimaryWindow = primaryWindow;
				if(m_PrimaryWindow!=null)
				{
					((Cerebrum.IDisposable)m_PrimaryWindow).Disposing += new System.EventHandler(m_PrimaryWindow_Disposing);
					((Cerebrum.IComponent)m_PrimaryWindow).SetConnector(Cerebrum.SerializeDirection.Init, new Cerebrum.Runtime.SurrogateComposite(this.MasterContext.Workspace));
					//m_PrimaryWindow.Application = this;
				}
				this.OnPrimaryWindowChanged(this, System.EventArgs.Empty);
			}
		}
	
			/// <summary>
			/// ��� �������� �������� ���� ��� ��������� ������� Disposing ���������� IReleasable
			/// �� �������� ������������ ���������� ������� ���������� �� ����� ���� �
			/// ������� ���������� m_PrimaryWindow, �������� ��� ����� ������� ����� ����
			/// ��� ��������� � �������� PrimaryWindow
			/// </summary>
			private void m_PrimaryWindow_Disposing(object sender, System.EventArgs e)
			{
				((Cerebrum.IDisposable)m_PrimaryWindow).Disposing -= new System.EventHandler(m_PrimaryWindow_Disposing);
				//m_PrimaryWindow.Application = null;
				m_PrimaryWindow = null;
				this.OnPrimaryWindowChanged(this, System.EventArgs.Empty);
			}

		public event System.EventHandler PrimaryWindowChanged
		{
			add	{	this.GetEventHandlerList().AddHandler(PrimaryWindowChangedEvent, value);	}
			remove	{	this.GetEventHandlerList().RemoveHandler(PrimaryWindowChangedEvent, value);	}
		}

		protected virtual void OnPrimaryWindowChanged(object sender, System.EventArgs e)
		{
			System.EventHandler handler = (System.EventHandler)this.GetEventHandlerList()[Cerebrum.Windows.Forms.Application.PrimaryWindowChangedEvent];
			if (handler != null)
				handler(this, e);
		}


		/*
		/// <summary>
		/// ���������� �������� ������ �� ��������� ��������� � ������
		/// ����� ���������.
		/// </summary>
		protected Cerebrum.Integrator.DomainContext m_ActiveContext = null;
		
		public Cerebrum.Integrator.DomainContext ActiveContext
		{
			get
			{
				return GetActiveContext();
			}
			set
			{
				this.SetActiveContext(value);
			}
		}
		
		protected virtual Cerebrum.Integrator.DomainContext GetActiveContext()
		{
			return m_ActiveContext;
		}

		protected virtual void SetActiveContext(Cerebrum.Integrator.DomainContext context)
		{
			if(m_ActiveContext!=context)
			{
				if(m_ActiveContext!=null)
				{
					m_ActiveContext.Disposing -= new EventHandler(m_ActiveContext_Disposing);
				}
				m_ActiveContext = context;
				if(m_ActiveContext!=null)
				{
					m_ActiveContext.Disposing += new EventHandler(m_ActiveContext_Disposing);
				}
			}
		}

		private void m_ActiveContext_Disposing(object sender, EventArgs e)
		{
			if(sender != null && sender is Cerebrum.IDisposable)
			{
				(sender as Cerebrum.IDisposable).Disposing -= new EventHandler(m_ActiveContext_Disposing);
			}
			if(sender == m_ActiveContext)
			{
				m_ActiveContext = null;
			}
		}*/
//
//		/// <summary>
//		/// ������� ���������� ������ �� ���������
//		/// �������� � ������� ������������ ���������
//		/// ���������.
//		/// </summary>
//		public SystemDatabase SystemDatabase
//		{
//			get
//			{
//				if(m_ActiveContext!=null)
//				{
//					return m_ActiveContext.SystemDatabase;
//				}
//				else
//					return null;
//			}
//		}
//
//		/// <summary>
//		/// ��������� ��������� �������� ���������
//		/// </summary>
//		/// <returns></returns>
//		public bool CommitContext()
//		{
//			if(!SaveMasterDatabase()) return false;
////-			return SaveActiveActivity();
//			return true;
//		}
//
//		/// <summary>
//		/// �������� ��������� �������� ���������
//		/// </summary>
//		/// <returns></returns>
//		public bool RejectContext()
//		{
////-			m_ActiveContext.SystemDatabase.RejectChanges();
////			this.AddActivityRowResource(SystemActivityEvents.DatabaseRejected, "Application.SaveActiveDatabase", "SystemActivityChangesRejected.Message", null);
////			return true;
//			return false;
//		}

//		/// <summary>
//		/// ��������� ������� ������� ���������� �������
//		/// </summary>
//		/// <returns>true</returns>
//		protected bool SaveActiveActivity()
//		{
//			if(m_ActiveContext!=null && m_ActiveContext.SystemActivity.HasChanges())
//				if(m_ContextFactory!=null) m_ContextFactory.SaveActivity(m_ActiveContext);
//			return true;
//		}

		/// <summary>
		/// ����� LogIn ������� ����������� ������������ � �������. 
		/// � ������ �������� ����������� ���������� �������� true
		/// </summary>
		public virtual bool LogIn()
		{
			return true;
			//if(!(AccessId==UserAccessModes.NormalAccess || AccessId==UserAccessModes.SuperAccess))
			//{
			//	try
			//	{
			//		string userId = "";
			//		string userPassword = "";
			//		string loginService = "";
			//		UserAccessModes accessId = UserAccessModes.UnknownAccess;
			//		System.Windows.Forms.DialogResult dr = System.Windows.Forms.DialogResult.None;
			//
			//
			//		System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();
			//
			//		if((System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Shift)==0)
			//			try
			//			{
			//				userId = ((string)(configurationAppSettings.GetValue("User.LoginId", typeof(string))));
			//				userPassword = ((string)(configurationAppSettings.GetValue("User.Password", typeof(string))));
			//				loginService = ((string)(configurationAppSettings.GetValue("User.LoginService", typeof(string))));
			//				accessId = UserAccessModes.NormalAccess;
			//				if( (userId.Length) > 0 && (loginService.Length) > 0 )
			//				{
			//					dr = System.Windows.Forms.DialogResult.OK;
			//				}
			//			}
			//			catch
			//			{
			//			}
			//
			//		if(dr!=System.Windows.Forms.DialogResult.OK)
			//		{
			//			LoginDialog dlg = new LoginDialog();
			//
			//			System.Data.DataView dv = new System.Data.DataView(m_MasterContext.SystemDatabase.Components);
			//			dv.RowFilter = "CategoryId='" + SystemCategories.ContextFactorys + "'";
			//			//dv.Sort = "Caption";
			//
			//			//, "", System.Data.DataViewRowState.OriginalRows);
			//
			//			Cerebrum.Integrator.Forms.DropDownStringItem di;
			//			foreach(System.Data.DataRowView rv in dv)
			//			{
			//				Cerebrum.Integrator.SystemDatabase.ComponentsRow rw = (Cerebrum.Integrator.SystemDatabase.ComponentsRow)(rv.Row);
			//				di = new Cerebrum.Integrator.Forms.DropDownStringItem();
			//				di.Value = rw.ComponentId;
			//				di.Caption = (string)Tools.nvl(m_MasterContext.GetSystemVariable(rw.ComponentId, "caption"), rw.ComponentId);
			//				dlg.cmbLoginService.Items.Add(di);
			//			}
			//			di = new Cerebrum.Integrator.Forms.DropDownStringItem();
			//			di.Caption = this.FormattedMessageInternal("ApplicationLogInLocally.Messsage", null);
			//			di.Value = "Cerebrum.Integrator.LocalContextFactory";
			//			dlg.cmbLoginService.Items.Add(di);
			//			dlg.cmbLoginService.SelectedIndex = 0;
			//
			//			bool oldEnabled = m_Enabled;
			//			Enabled = false;
			//
			//			try
			//			{
			//				Microsoft.Win32.RegistryKey key = this.DefaultUserRegistryKey;
			//				if(key!=null)
			//				{
			//					dlg.txtUserName.Text = (string)key.GetValue("Login.LastUserName");
			//
			//					string cmbv = (string)key.GetValue("Login.LastUserService");
			//					foreach(Cerebrum.Integrator.Forms.DropDownStringItem diT in dlg.cmbLoginService.Items)
			//					{
			//						if(diT.Value.ToString() == cmbv)
			//						{
			//							dlg.cmbLoginService.SelectedItem = diT;
			//						}
			//					}
			//				}
			//			}
			//			catch
			//			{
			//			}
			//
			//			dr = dlg.ShowDialog();
			//			Enabled = oldEnabled;
			//
			//			if(dr==System.Windows.Forms.DialogResult.OK)
			//			{
			//				userId = dlg.UserName;
			//				accessId = dlg.UserAccess;
			//				userPassword = dlg.UserPassword;
			//				loginService = dlg.LoginService.ToString();
			//			}
			//		}
			//
			//		if(dr==System.Windows.Forms.DialogResult.OK)
			//		{
			//			if(loginService=="Cerebrum.Integrator.LocalContextFactory")
			//			{
			//				m_ContextFactory = (Cerebrum.Integrator.IContextFactory)m_MasterContext.AcquireComponent("Cerebrum.Integrator.LocalContextFactory");
			//				m_ActiveContext = m_MasterContext;
			//			}
			//			else
			//			{
			//				m_ContextFactory = (Cerebrum.Integrator.IContextFactory)m_MasterContext.AcquireComponent(loginService);
			//				m_ActiveContext = new Cerebrum.Integrator.DomainContext(this, new Cerebrum.Integrator.RuntimeDatabase(),new Cerebrum.Integrator.SystemActivity());
			//				m_ContextFactory.LoadContext(m_ActiveContext);
			//			}
			//
			//
			//			string groupId = GetUserAccount(userId, userPassword, accessId).GroupId;
			//			if(groupId==null || groupId.Length == 0) throw new System.Exception();
			//
			//			this.m_UserId = userId;
			//			this.m_GroupId = groupId;
			//			this.m_AccessId = accessId;
			//
			//			if(!MsgCheckRight(SystemAccessRights.Login)) return !LogOut();
			//			this.AddActivityRowResource(SystemActivityEvents.LoggedIn, "Application.LogIn", "UserLoggedIn.Message", null);
			//
			//			try
			//			{
			//				Microsoft.Win32.RegistryKey key = this.DefaultUserRegistryKey;
			//				if(key!=null && m_AccessId==UserAccessModes.NormalAccess)
			//				{
			//					key.SetValue("Login.LastUserName", userId);
			//					key.SetValue("Login.LastUserService", loginService);
			//				}
			//			}
			//			catch
			//			{
			//			}
			//
			//		}
			//		else if(dr==System.Windows.Forms.DialogResult.Abort)
			//		{
			//			this.Exit();
			//			return false;
			//		}
			//		else
			//		{
			//			return false;
			//		}
			//	}
			//	catch
			//	{
			//		this.MessageBox("LoginError", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			//		return !LogOut();
			//	}
			//}
			//else
			//{
			//	if(!MsgCheckRight(SystemAccessRights.Login)) return !LogOut();
			//};
			//return true;
		}


		/// <summary>
		/// ����� �������� ���������� ������ ������������ � ��������.
		/// ���������� true � ������ ��������� ����������. false - � ������ ������
		/// ������������� ������. �������� ��������� ������� ������� ������ - 
		/// ������� � ������ ������������� ������, � �������� ���������� ���������� ������.
		/// </summary>
		public virtual void LogOut()
		{
			
			//IReleasable tmp = null;
			try
			{

				//bool cancel = false;
				
				// already in Exit
				//this.Relax(this, ce);
				//if(ce.Cancel) return false;

//				if(this.ActiveContext!=null && m_ActiveContext!=m_MasterContext)
//				{
//
////-				m_ActiveContext.Relax(this, ce);
//					if(ce.Cancel) return false;
//				}

				/*tmp = m_PrimaryWindow as IReleasable;
				if(tmp!=null)
				{
					cancel = tmp.Release(cancel);
				}
				if(cancel) return false;*/

//				if(m_UserId!=null || m_GroupId!=null)
//					this.AddActivityRowResource(SystemActivityEvents.LoggedOut, "Application.LogOut", "UserLoggedOut.Message", null);
//
//				SaveActiveActivity();

				/*if(m_ActiveContext!=null && m_ActiveContext!=m_MasterContext)
				{
					m_ActiveContext.Shutdown();
					m_ActiveContext = null;
				}*/

				IPrimaryWindow frm1 = m_PrimaryWindow as IPrimaryWindow;
				if(frm1!=null)
				{
					frm1.Close();
				}
				else
				{
					Cerebrum.Windows.Forms.DesktopComponent frm2 = m_PrimaryWindow as Cerebrum.Windows.Forms.DesktopComponent;
					if(frm2!=null)
					{
						frm2.Close();
					}
					else
					{
						System.Windows.Forms.Form frm3 = m_PrimaryWindow;
						if(frm3!=null)
						{
							frm3.Close();
						}
					}
				}


//-				m_UserId = null;
//-				m_GroupId = null;
//-				m_AccessId = UserAccessModes.UnknownAccess;

				//return true;
			}
			catch(System.Exception ex)
			{
				this.ProcessException(ex, "Application.LogOut");
				//return false;
			}
		}

//		/// <summary>
//		/// ����� �������� ��������� ������ ������������.
//		/// </summary>
//		public void ChangePassword()
//		{
//			try
//			{
//				if(MsgCheckRight(SystemAccessRights.ChangePassword))
//				{
//					Cerebrum.Integrator.ChangePasswordDialog dlg;
//					dlg = new Cerebrum.Integrator.ChangePasswordDialog(this);
//					
//					bool oldEnabled = m_Enabled;
//					Enabled = false;
//					dlg.ShowDialog();
//					Enabled = oldEnabled;
//
//					if(!dlg.Canceled)
//					{
//						Cerebrum.Integrator.SystemDatabase.LoginsRow rw 
//							= GetUserAccount(UserId, dlg.txtOldPassword.Text, AccessId);
//						if(rw==null)
//						{
//							this.MessageBox("LoginError", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
//							return;
//						};
//
//						string password = dlg.txtNewPassword.Text;
//						if(password.Length>0)
//						{
//							password = System.String.Format("{0:X}", password.GetHashCode());
//						}
//						else
//						{
//							password = null;
//						}
//				
//						if(AccessId == UserAccessModes.SuperAccess)
//						{
//							if(password==null)
//							{
//								m_ActiveContext.DelSystemVariable("system", BUILT_IN_ADMINISTRATOR_PASSWORD_VARIABLE, null);
//							}
//							else
//							{
//								m_ActiveContext.SetSystemVariable("system", BUILT_IN_ADMINISTRATOR_PASSWORD_VARIABLE, password);
//							}
//						}
//						if(AccessId == UserAccessModes.NormalAccess)
//						{
//							rw.Password = password;
//						}
//						this.AddActivityRowResource(SystemActivityEvents.PasswordChanged, "Application.ChangePassword", "UserPasswordChanged.Message", null);
//						this.MsgSaveActiveDatabase(System.Windows.Forms.MessageBoxButtons.YesNo, "SaveChangedPassword");
//						this.SaveActiveActivity();
//					}
//				}
//			}
//			catch(System.Exception ex)
//			{
//				this.AddActivityRow(ex, "Application.ChangePassword");
//			}
//		}
//		/// <summary>
//		/// �������� ���� ������������ RightId. � ������
//		/// ��������� ����, ������ ��������� � ������� false
//		/// </summary>
//		public bool MsgCheckRight(string rightId)
//		{
//			if(CheckRight(rightId))
//			{
//				return true;
//			}
//			else
//			{
//				this.AddActivityRow(SystemActivityEvents.AccessDenied, "Application.MsgCheckRight", this.FormattedMessageInternal("AccessDeniedActivity.Message", new object[] {rightId}));
//				this.MessageBox("AccessDeniedError", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
//				return false;
//			}
//		}
//		/// <summary>
//		/// �������� ���� ������������ RightId. � ������
//		/// ��������� ����, ������� false
//		/// </summary>
//		public bool CheckRight(string rightId)
//		{
//			try
//			{
//				if(m_AccessId == UserAccessModes.SuperAccess)
//				{
//					return true;
//				}
//				if(m_AccessId == UserAccessModes.NormalAccess)
//				{
//					if(m_UserId.Length > 0 && m_GroupId.Length > 0)
//					{
//						Cerebrum.Integrator.SystemDatabase.GroupRightsRow rw;
//						rw = m_ActiveContext.SystemDatabase.GroupRights.FindByGroupIdRightId(m_GroupId, rightId);
//						if(rw!=null) return true;
//						rw = m_ActiveContext.SystemDatabase.GroupRights.FindByGroupIdRightId(m_GroupId, SystemAccessRights.Unlimited);
//						if(rw!=null) return true;
//					}
//				}
//			}
//			catch(System.Exception ex)
//			{
//				this.AddActivityRow(ex, "Application.CheckRight");
//			}
//			return false;
//		}
//		/// <summary>
//		/// ������� ���������� ������ �� ������������ �������� ��������� � ������� ������������.
//		/// ��� ��������� �������� ���������� ����������� ����� � ������ ������������.
//		/// � ������ ����� � ������� ������������������� ������������ ��������� ������
//		/// �� ������������� �� ������������ �������� ���������.
//		/// </summary>
//		public Cerebrum.Integrator.SystemDatabase.LoginsRow 
//			GetUserAccount(string userId, string password, UserAccessModes accessId)
//		{
//			try
//			{
//				if(password.Length>0)
//				{
//					password = System.String.Format("{0:X}", password.GetHashCode());
//				}
//				else
//				{
//					password = null;
//				}
//
//				Cerebrum.Integrator.SystemDatabase.LoginsRow rw;
//				if(accessId==UserAccessModes.NormalAccess)
//				{
//					
//					rw = m_ActiveContext.SystemDatabase.Logins.FindByLoginId(userId);
//
//					if(rw==null) throw new System.Exception();
//
//					if(password!=null)
//					{
//						if(rw.IsPasswordNull() || rw.Password!=password) throw new System.Exception();
//					}
//					else
//					{
//						if((!rw.IsPasswordNull()) && rw.Password.Length >0) throw new System.Exception();
//					}
//					if(rw.Disabled) throw new System.Exception();
//
//					return rw;
//				}
//				if(accessId==UserAccessModes.SuperAccess)
//				{
//					string v = (string)m_ActiveContext.GetSystemVariable("system", BUILT_IN_ADMINISTRATOR_PASSWORD_VARIABLE);
//					if(v!=null && v.Length==0) v = null;
//					if(password!=v) throw new System.Exception();;
//					rw = m_ActiveContext.SystemDatabase.Logins.NewLoginsRow();
//					rw.LoginId = "Administrator";
//					rw.GroupId = "system";
//					rw.Password = password;
//					return rw;
//				}
//			}
//			catch
//			{
//			};
//			return null;
//		}

		protected abstract System.Windows.Forms.Form CreatePrimaryWindow();

		/// <summary>
		/// ���������� ������� MDI ���� ����������
		/// ������������ ����������� ������������, ���� ������������ �� ���������������.
		/// � ������ ��������� �������� ���� ������������ ��� ��������,
		/// ������������� � ����������� ��� � �������� ���������.
		/// </summary>
		public bool Show()
		{
			try
			{
				if(this.LogIn() /*-&& this.MsgCheckRight(SystemAccessRights.Login)*/)
				{
					if(m_PrimaryWindow==null)
					{
						// ���������� ����������� � ��������� ������������ � ������� PrimaryWindow ���������. �� �������� RegisterInstance
						PrimaryWindow = CreatePrimaryWindow();
						// already in set m_PrimaryWindow.Application = this;
						//-m_PrimaryWindow.ComponentId = "Cerebrum.Integrator.PrimaryWindow";
						//m_PrimaryWindow.BuildUi();
					};
					m_PrimaryWindow.Show();
					m_PrimaryWindow.Activate();
					return true;
				}
				return false;
			}
			catch(System.Exception ex)
			{
				this.ProcessException(ex, "Application.Show");
				return false;
			}
		}

		/// <summary>
		/// �������� ������� MDI ���� ����������
		/// </summary>
		public void Hide()
		{
			try
			{
				if(m_PrimaryWindow!=null)
				{
					m_PrimaryWindow.Hide();
				};
			}
			catch(System.Exception ex)
			{
				this.ProcessException(ex, "Application.Hide");
			}
		}


		/// <summary>
		/// �������� � ������������� �������� ��������� ��������
		/// ���� MDI
		/// </summary>
		public bool Visible
		{
			get
			{
				if(m_PrimaryWindow!=null)
				{
					return m_PrimaryWindow.Visible;
				};
				return false;
			}
			set
			{
				if(m_PrimaryWindow!=null)
				{
					if(value)
					{
						//-if(MsgCheckRight(SystemAccessRights.Login))
						{
							m_PrimaryWindow.Visible = true;
							m_PrimaryWindow.Activate();
						}
						//-else LogOut();
					}
					else
						m_PrimaryWindow.Visible = false;
				}
				else if(value) Show();
			}
		}
//
//		/// <summary>
//		/// ���������� ������ ��� �������� ������������
//		/// ��������������������� � �������
//		/// </summary>
//		protected string m_PrincipalId;
//		/// <summary>
//		/// �������� ���������� ��� �������� ������������
//		/// ��������������������� � �������
//		/// </summary>
//		public string PrincipalId
//		{
//			get
//			{
//				return m_PrincipalId;
//			}
//		}
//		/// <summary>
//		/// ���������� ������ ����� ������� ��������
//		/// ������������
//		/// </summary>
//		protected UserAccessModes m_AccessId;
//		/// <summary>
//		/// �������� ���������� ����� ������� ��������
//		/// ������������
//		/// </summary>
//		public UserAccessModes AccessId
//		{
//			get
//			{
//				return m_AccessId;
//			}
//		}

//		/// <summary>
//		/// ���������� ������ ������ �������� ������������.
//		/// ���� � ������� ����������������� ������������������,
//		/// ���������� �� ������������ ������� ������ ���������������
//		/// ������ "system"
//		/// </summary>
//		protected string m_GroupId;
//		/// <summary>
//		/// �������� ���������� ������ �������� ������������.
//		/// </summary>
//		public string GroupId
//		{
//			get
//			{
//				if(AccessId==UserAccessModes.SuperAccess)
//				{
//					return "system";
//				}
//				else
//				{
//					return m_GroupId;
//				};
//			}
//		}


//		/// <summary>
//		/// ��������� ������ � ��� ��������� ����������
//		/// </summary>
//		/// <param name="activityId">������������� ���� ����������</param>
//		/// <param name="source">�������� ���������� (���� ����� ��� ������ ������������ ������)</param>
//		/// <param name="description">�������� �������</param>
//		/// <returns>������ ����������� �������</returns>
//		public Cerebrum.Integrator.SystemActivity.ActivityRow AddActivityRow(string activityId, string source, string description)
//		{
//			if(m_ActiveContext!=null && m_ActiveContext.SystemActivity!=null)
//			{
//				return AddActivityRow(activityId, source, description, m_ActiveContext.SystemActivity);
//			}
//			else if(m_MasterContext!=null && m_MasterContext.SystemActivity!=null)
//			{
//				return AddActivityRow(activityId, source, description, m_MasterContext.SystemActivity);
//			}
//			else
//			{
//				this.MessageBox(description, FormattedMessageInternal("AddActivityRowError.Caption", new object[] {source, activityId}), System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
//			}
//			return null;
//		}
		public override void ProcessException(System.Exception ex, string source)
		{
			try
			{
				Cerebrum.Runtime.NativeException cex = ex as Cerebrum.Runtime.NativeException;
				if(cex != null && cex.ResultHandle == Cerebrum.Runtime.ErrorResults.FileVersion)
				{
					//TODO write to log
					return;
				}
			}
			catch
			{
			}
			try
			{
				string smessage = ex==null?string.Empty:ex.ToString();
				string scaption = source;

				System.Windows.Forms.DialogResult dr = MessageBox(smessage, scaption, Cerebrum.Windows.Forms.MessageBoxStyle.BriefOK, System.Windows.Forms.MessageBoxIcon.Error);
				if(dr == System.Windows.Forms.DialogResult.Abort)
				{
					System.Environment.Exit(65535);
				}
			}
			catch/*(System.Exception e)*/
			{
				System.Windows.Forms.MessageBox.Show(ex.ToString(), "Unexpected error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
		}
		internal void ProcessException(string rcaption, System.Exception ex)
		{
			try
			{
				System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Application));
				string scaption = ((string)(resources.GetObject(rcaption + ".Caption")));

				ProcessException(ex, scaption);
			}
			catch/*(System.Exception e)*/
			{
				System.Windows.Forms.MessageBox.Show(ex.ToString(), "Unexpected error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
		}

//		internal Cerebrum.Integrator.SystemActivity.ActivityRow AddActivityRowResource(string activityId, string source, string resource, params object [] args)
//		{
//			return AddActivityRow(activityId, source, this.FormattedMessageInternal(resource, args));
//		}
//		internal Cerebrum.Integrator.SystemActivity.ActivityRow AddActivityRowResource(Cerebrum.Integrator.SystemActivity systemActivity, string activityId, string source, string resource, params object [] args)
//		{
//			return AddActivityRow(activityId, source, this.FormattedMessageInternal(resource, args), systemActivity);
//		}
//
//		/// <summary>
//		/// ��������� ������ � ��� ��������� ����������
//		/// </summary>
//		/// <param name="activityId">������������� ���� ����������</param>
//		/// <param name="source">�������� ���������� (���� ����� ��� ������ ������������ ������)</param>
//		/// <param name="description">�������� �������</param>
//		/// <param name="systemActivity">DataSet � �������� �������</param>
//		/// <returns>������ ����������� �������</returns>
//		internal Cerebrum.Integrator.SystemActivity.ActivityRow AddActivityRow(string activityId, string source, string description, Cerebrum.Integrator.SystemActivity systemActivity)
//		{
//			try
//			{
//				bool b = false;
//				Cerebrum.Integrator.SystemActivity.ActivityRow row = systemActivity.Activity.NewActivityRow();
//				row.ActivityId = activityId;
//				row.UserId = m_UserId;
//				row.GroupId = m_GroupId;
//				row.AccessId = (int)m_AccessId;
//				row.Source = source;
//				row.Description = description;
//				for(;;)
//				{
//					try
//					{
//						row.Timestamp = System.DateTime.Now;
//						systemActivity.Activity.AddActivityRow(row);
//						return row;
//					}
//					catch(System.Exception ex)
//					{
//						if(b) throw(ex);
//						b=true;
//					}
//				}
//			}
//			catch(System.Exception ex)
//			{
//				this.MessageBox(description + System.Environment.NewLine + ex.ToString(), FormattedMessageInternal("AddActivityRowError.Caption", new object[] {source, activityId}), System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
//			}
//			return null;
//		}


		public System.Windows.Forms.DialogResult MessageBox(string message, string caption)
		{
			return this.MessageBox(message, caption, Cerebrum.Windows.Forms.MessageBoxStyle.BriefOK, System.Windows.Forms.MessageBoxIcon.Information, System.Windows.Forms.MessageBoxDefaultButton.Button1);
		}

		public System.Windows.Forms.DialogResult MessageBox(string message, string caption, Cerebrum.Windows.Forms.MessageBoxStyle style, System.Windows.Forms.MessageBoxIcon icon)
		{
			return this.MessageBox(message, caption, style, icon, System.Windows.Forms.MessageBoxDefaultButton.Button1);
		}

		/// <summary>
		/// ���������� ����������� �����������
		/// </summary>
		/// <param name="text">���������</param>
		/// <param name="caption">���������</param>
		/// <param name="buttons">������</param>
		/// <param name="icon">������</param>
		/// <returns></returns>
		public System.Windows.Forms.DialogResult MessageBox(string message, string caption, Cerebrum.Windows.Forms.MessageBoxStyle style, System.Windows.Forms.MessageBoxIcon icon, System.Windows.Forms.MessageBoxDefaultButton defaultButton)
		{
			System.Windows.Forms.DialogResult dr;
			bool oldEnabled = this.Enabled;
			this.Enabled = false;
			try
			{
				if( ((int)style) < ((int)Cerebrum.Windows.Forms.MessageBoxStyle.ShortAbortRetryIgnore))
				{
					ErrorReport dlg = new ErrorReport();
					dlg.Message = message;
					dlg.Text = caption;
					if(style==Cerebrum.Windows.Forms.MessageBoxStyle.BriefOK)
					{
						dlg.btnCancel.Text = "OK";
					}
					if(icon==System.Windows.Forms.MessageBoxIcon.Information)
					{
						dlg.BackColor = System.Drawing.Color.Aquamarine;
						dlg.pnlButtons.BackColor = dlg.BackColor;
						dlg.btnCancel.BackColor = System.Drawing.Color.Turquoise;
					}
					if(m_PrimaryWindow!=null)
					{
						dr = dlg.ShowDialog(m_PrimaryWindow);
					}
					else
					{
						dr = dlg.ShowDialog();
					}
				}
				else
				{
					System.Windows.Forms.MessageBoxButtons buttons;
					switch(style)
					{
						case Cerebrum.Windows.Forms.MessageBoxStyle.ShortAbortRetryIgnore:
							buttons = System.Windows.Forms.MessageBoxButtons.AbortRetryIgnore;
							break;
						case Cerebrum.Windows.Forms.MessageBoxStyle.ShortOK:
							buttons = System.Windows.Forms.MessageBoxButtons.OK;
							break;
						case Cerebrum.Windows.Forms.MessageBoxStyle.ShortOKCancel:
							buttons = System.Windows.Forms.MessageBoxButtons.OKCancel;
							break;
						case Cerebrum.Windows.Forms.MessageBoxStyle.ShortRetryCancel:
							buttons = System.Windows.Forms.MessageBoxButtons.RetryCancel;
							break;
						case Cerebrum.Windows.Forms.MessageBoxStyle.ShortYesNo:
							buttons = System.Windows.Forms.MessageBoxButtons.YesNo;
							break;
						case Cerebrum.Windows.Forms.MessageBoxStyle.ShortYesNoCancel:
							buttons = System.Windows.Forms.MessageBoxButtons.YesNoCancel;
							break;
						default:
							buttons = System.Windows.Forms.MessageBoxButtons.OK;
							break;
					}
					if(m_PrimaryWindow!=null)
					{
						dr = System.Windows.Forms.MessageBox.Show(m_PrimaryWindow, message, caption, buttons, icon, defaultButton);
					}
					else
					{
						dr = System.Windows.Forms.MessageBox.Show(message, caption, buttons, icon, defaultButton);
					}
				}
			}
			finally
			{
				this.Enabled = oldEnabled;
			}
			return dr;
		}

		private Microsoft.Win32.RegistryKey m_DefaultUserRegistryKey = null;
		public Microsoft.Win32.RegistryKey DefaultUserRegistryKey
		{
			get
			{
				if(m_DefaultUserRegistryKey==null)
				{
					try
					{
						string ver = System.Windows.Forms.Application.ProductVersion;
						int i = ver.IndexOf('.', 0);
						if(i > 0)
						{
							i = ver.IndexOf('.', i + 1);
						}
						if(i > 0)
						{
							ver = ver.Substring(0, i);
						}
						Microsoft.Win32.RegistryKey key2 = null;
						Microsoft.Win32.RegistryKey key1 = null;
						Microsoft.Win32.RegistryKey key0 = Microsoft.Win32.Registry.CurrentUser;
						bool key1w;
						bool key2w;

						key1 = key0.OpenSubKey("Software");
						key1w = false;
						key2 = key1.OpenSubKey(System.Windows.Forms.Application.CompanyName);
						key2w = false;


						if(key2==null)
						{
							if(!key1w)
							{
								key1w = true;
								key1.Close();
								key1 = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software", true);
							}
							key2 = key1.CreateSubKey(System.Windows.Forms.Application.CompanyName);
							key2w = true;
						}
						key0 = key1;
						key1 = key2;
						key2 = null;
						key1w = key2w;

						key2 = key1.OpenSubKey(System.Windows.Forms.Application.ProductName);
						key2w = false;


						if(key2==null)
						{
							if(!key1w)
							{
								key1w = true;
								key1.Close();
								key1 = key0.OpenSubKey(System.Windows.Forms.Application.CompanyName, true);
							}
							key2 = key1.CreateSubKey(System.Windows.Forms.Application.ProductName);
							key2w = true;
						}
						key0.Close();
						key0 = key1;
						key1 = key2;
						key2 = null;
						key1w = key2w;
						
						key2 = key1.OpenSubKey(ver, true);
						if(key2==null)
						{
							if(!key1w)
							{
								key1w = true;
								key1.Close();
								key1 = key0.OpenSubKey(System.Windows.Forms.Application.ProductName, true);
							}
							key2 = key1.CreateSubKey(ver);
						}
						key0.Close();
						key1.Close();

						m_DefaultUserRegistryKey = key2;
					}
					catch(System.Exception ex)
					{
						this.ProcessException(ex, "Application.DefaultUserRegistryKey");
					}
					finally
					{
						if(m_DefaultUserRegistryKey == null)
						{
							m_DefaultUserRegistryKey = System.Windows.Forms.Application.UserAppDataRegistry;
						}
					}
				}
				return m_DefaultUserRegistryKey;
			}
		}
		
		/*internal System.Windows.Forms.DialogResult MessageBox(string resource, System.Windows.Forms.MessageBoxButtons buttons, System.Windows.Forms.MessageBoxIcon icon)
		{
			return MessageBox(resource, buttons, icon, null);
		}
		internal System.Windows.Forms.DialogResult MessageBox(string resource, System.Windows.Forms.MessageBoxButtons buttons, System.Windows.Forms.MessageBoxIcon icon, string caption)
		{
			try
			{
				System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Application));
				string smessage = ((string)(resources.GetObject(resource + ".Message")));
				string scaption = caption==null?((string)(resources.GetObject(resource + ".Caption"))):((string)(resources.GetObject(caption + ".Caption")));

				return MessageBox(smessage, scaption, buttons, icon);
			}
			catch(System.Exception ex)
			{
				return System.Windows.Forms.MessageBox.Show(ex.ToString(), "Unexpected error", buttons, icon);
			}
		}*/

		/*internal string FormattedMessageInternal(string resource, params object [] args)
		{
			try
			{
				System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Application));
				string sformat = ((string)(resources.GetObject(resource)));
				if(args!=null)
				{
					return System.String.Format(sformat, args);
				}
				else
				{
					return sformat;
				}
			}
			catch(System.Exception ex)
			{
				return ex.ToString();
			}
		}*/
	}
}


/*
		public System.Reflection.Assembly PluggedModule(string instanceId)
		{
			return m_ActiveContext.PluggedModule(instanceId);
		}

		public void RegisterInstance(Cerebrum.Integrator.IComponent component)
		{
			m_ActiveContext.RegisterInstance(component);
		}
		
		public void RegisterFactory(string instanceId, Cerebrum.Integrator.IComponentFactory factory)
		{
			m_ActiveContext.RegisterFactory(instanceId,factory);
		}

		public object AcquireComponent(string instanceId)
		{
			return m_ActiveContext.AcquireComponent(instanceId);
		}


		public object GetSystemVariable(string instanceId, string variableId)
		{
			return m_ActiveContext.GetSystemVariable(instanceId,variableId);
		}

		public void SetSystemVariable(string instanceId, string variableId, object variableValue)
		{
			m_ActiveContext.SetSystemVariable(instanceId, variableId, variableValue);
		}

		public void DelSystemVariable(string instanceId, string variableId)
		{
			m_ActiveContext.DelSystemVariable(instanceId, variableId);
		}
        
*/

//		public object CreateInstance(string instanceId)
//		{
//			Cerebrum.Integrator.SystemDatabase.InstancesRow row = m_SystemDatabase.Instances.FindByInstanceId(ComponentId);
//			if(row!=null)
//			{
//				if(!row.Singleton)
//				{
//					System.Reflection.Assembly m = PluggedModule(row.AssemblyId);
//					return m.CreateInstance(row.ClassName);
//				}
//			}
//			return null;
//		}
		//		public void Close()
		//		{
		//			if(m_PrimaryWindow!=null)
		//			{
		//				m_PrimaryWindow.Close();
		//			};
		//		}
