// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// Summary description for IntegratorBandPanel.
	/// </summary>
	public class DesktopBandPlace : Cerebrum.Windows.Forms.TabPage, Cerebrum.Windows.Forms.IUiControl, Cerebrum.IReleasable
	{
		public DesktopBandPlace()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		protected Cerebrum.ObjectHandle m_CommandId;

		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		public Cerebrum.ObjectHandle CommandId
		{
			get
			{
				return m_CommandId;
			}
			set
			{
				m_CommandId = value;
			}
		}

		public event System.ComponentModel.CancelEventHandler InvokeCommand;

		public void AttachCommand(Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor command)
		{
		}


		public void RaiseInvokeCommand(System.ComponentModel.CancelEventArgs e)
		{
			if(this.InvokeCommand!=null)
			{
				this.InvokeCommand(this, e);
			}
		}

		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		public bool Checked
		{
			get
			{
				return false;
			}
			set
			{
				/*if (value)
				{
					throw new ArgumentOutOfRangeException("Checked", value, "Can't set Checked property to 'True'");
				}*/
			}
		}

		protected int m_MergeOrder;
		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		public int MergeOrder
		{
			get
			{
				return this.m_MergeOrder;
			}
			set
			{
				this.m_MergeOrder = value;
			}
		}
		#region IReleasable Members

		public bool Release(bool cancel)
		{
			System.ComponentModel.CancelEventArgs e = new System.ComponentModel.CancelEventArgs(cancel);
			System.ComponentModel.CancelEventHandler handler = (System.ComponentModel.CancelEventHandler)Events[Cerebrum.Specialized.EventKeys.ReleasingEvent];
			if (handler != null)
				handler(this, e);
			return e.Cancel;
		}
		/// <summary>
		/// ���������� ��� ������������� ������� ���������, �����������
		/// ������ �����������. �� ������ ������� ������ ����� ���������
		/// ������ ������������ �������� ��� ������������ ��������. ������
		/// ������� ������� ����������. ������� �������� ������������.
		/// </summary>
		/// <param name="sender">�������� �������</param>
		/// <param name="e">��������� �������</param>
		protected virtual void OnDisposing(System.EventArgs e)
		{
			System.EventHandler handler = (System.EventHandler)Events[Cerebrum.Specialized.EventKeys.DisposingEvent];
			if (handler != null)
				handler(this, e);
		}

		public event System.ComponentModel.CancelEventHandler Releasing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
		}

		/// <summary>
		/// ���������� ������� Clearing ���������� IReleasable
		/// </summary>
		event System.EventHandler Cerebrum.IDisposable.Disposing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
		}

		#endregion
	}
}
