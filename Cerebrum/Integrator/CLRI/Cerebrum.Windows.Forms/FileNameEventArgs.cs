// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// Summary description for DomainAddedEventArgs.
	/// </summary>
	public class FileNameEventArgs
	{
		public FileNameEventArgs(string fileName)
		{
			m_FileName = fileName;
		}

		private string m_FileName;

		public string FileName
		{
			get
			{
				return m_FileName;
			}
		}
	}

	//public delegate void DomainContextEventHandler(object sender, DomainContextEventArgs e);
}
