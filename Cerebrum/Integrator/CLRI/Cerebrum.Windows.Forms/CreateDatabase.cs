// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// Summary description for CreateDatabase.
	/// </summary>
	public class CreateDatabase
	{
		public CreateDatabase(CreateDatabaseOptions dbOptions)
		{
			m_CreateOptions = dbOptions;
			if(m_CreateOptions==CreateDatabaseOptions.None)
			{
				m_Dialog = null;
			}
			else
			{
				m_Dialog = new CreateDatabaseForm(this);
			}

			// 
			// saveFileDialog1
			// 
		}
		public bool Versioning;
		public int ClusterFrameSize;
		public string ClusterFileName;
		public string DomainName;
		internal CreateDatabaseOptions m_CreateOptions;
		private CreateDatabaseForm m_Dialog;

		public System.Windows.Forms.DialogResult ShowDialog(System.Windows.Forms.IWin32Window owner)
		{
			System.Windows.Forms.DialogResult dr;
			if(m_Dialog!=null)
			{
				dr = m_Dialog.ShowDialog(owner);
			}
			else
			{
				dr = System.Windows.Forms.DialogResult.OK;
			}
			if(dr == System.Windows.Forms.DialogResult.OK)
			{
				System.Windows.Forms.SaveFileDialog saveFileDialog1;
				using(saveFileDialog1 = new System.Windows.Forms.SaveFileDialog())
				{
					saveFileDialog1.DefaultExt = "vnn";
					saveFileDialog1.Filter = "Cerebrum Database files (*.vnn)|*.vnn|All files|*.*";
					saveFileDialog1.Title = "Create Cerebrum Database";
					dr = saveFileDialog1.ShowDialog(owner);
					if(dr==System.Windows.Forms.DialogResult.OK)
					{
						this.ClusterFileName = saveFileDialog1.FileName;
					}
				}
			}
			return dr;
		}
	}
}
