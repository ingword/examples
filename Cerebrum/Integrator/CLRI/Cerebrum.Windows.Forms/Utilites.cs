// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.
using System;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// ��������� ����� �� ������������ ���������
	/// ������������ ����� ���������� ������� 
	/// �������, ������� ��������� ��������� ����
	/// </summary>
	public class Utilites
	{
		private Utilites()
		{
			//
			// No constructor logic for static class
			//
		}

		public static bool SetControlVisibleProperty(object control, bool Value)
		{
			Cerebrum.Windows.Forms.IVisibleProperty uictl = control as Cerebrum.Windows.Forms.IVisibleProperty;
			if(uictl != null)
			{
				uictl.Visible = Value;
				return true;
			}
			System.Windows.Forms.Control wfctl = control as System.Windows.Forms.Control;
			if(wfctl != null)
			{
				wfctl.Visible = Value;
				return true;
			}
			return false;
		}

		public static bool SetControlEnabledProperty(object control, bool Value)
		{
			Cerebrum.Integrator.IEnabledProperty uictl = control as Cerebrum.Integrator.IEnabledProperty;
			if(uictl != null)
			{
				uictl.Enabled = Value;
				return true;
			}
			System.Windows.Forms.Control wfctl = control as System.Windows.Forms.Control;
			if(wfctl != null)
			{
				wfctl.Enabled = Value;
				return true;
			}
			return false;
		}

		public static bool SetControlCheckedProperty(object control, bool Value)
		{
			Cerebrum.Windows.Forms.ICheckedProperty uictl = control as Cerebrum.Windows.Forms.ICheckedProperty;
			if(uictl != null)
			{
				uictl.Checked = Value;
				return true;
			}
			return false;
		}

		//	/// <summary>
		//	/// ��� � ������������� �������� ������������ ����� �������
		//	/// ������������ � �������. ����� ���� ��� ������ �������:
		//	/// - ��������������, ������������ �� ����������������� � �������.
		//	/// - �����������, � ������� ����� �������������, �������� 
		//	///   ��� ������� ��������� � ��������� ������. 
		//	/// - ����������, ������� ����� ���������������� �������.
		//	/// </summary>
		//	public enum UserAccessModes
		//	{
		//		/// <summary>
		//		/// �������������� ����� �������. ������������ ��
		//		/// ����������������� � �������. ���������� ��������
		//		/// � �������.
		//		/// </summary>
		//		UnknownAccess = 0,
		//		/// <summary>
		//		/// ����� ������������. � ������� ����� ������������� �
		//		/// ��������� ������. �������� �������� ������. ����������
		//		/// ������������ ����������� ��������� ������ ��� ����������
		//		/// ������. �������� ������� ��������� � ������������ ���������.
		//		/// 
		//		/// � ������� �� ����������� ������ ������, ������ �������������������
		//		/// �������� � ������� Variables, InstznceId="system", 
		//		/// �������� BUILT_IN_ADMINISTRATOR_PASSWORD_VARIABLE
		//		/// � ������ ���������� ���� ���������� ������ ������������ ������ ������.
		//		/// ��� ���������� ������� � ������� ������� �� ����� ������������ 
		//		/// ���������� system,BUILT_IN_ADMINISTRATOR_PASSWORD_VARIABLE
		//		/// </summary>
		//		SuperAccess = 1,
		//		/// <summary>
		//		/// ���������� ����� �������. ����� ������������ �� ����������,
		//		/// ������������ � ���� ������ ������� ������������. �������
		//		/// ����� ������ ���������� � ��������������.
		//		/// </summary>
		//		NormalAccess = 2
		//	}

		//	/// <summary>
		//	/// ������������ � ����������� ������������� ����� �������
		//	/// ������������� � ���������� ������.
		//	/// </summary>
		//	public sealed class SystemAccessRights
		//	{
		//		/// <summary>
		//		/// �������� ����������� ��� �������������� ��������
		//		/// ���������� ������.
		//		/// </summary>
		//		private SystemAccessRights(){}
		//
		//		/// <summary>
		//		/// ������������ �����. ������������ ���������� 
		//		/// ���� ������ �������� ������ ������ � �������.
		//		/// </summary>
		//		public const string Unlimited = "super";
		//		/// <summary>
		//		/// ����� �� ���� � �������.
		//		/// </summary>
		//		public const string Login = "login";
		//		/// <summary>
		//		/// ����� �������� ����������� ������.
		//		/// </summary>
		//		public const string ChangePassword = "chgpwd";
		//
		//		public const string EmergencyConfigurator = "emcfg";
		//	}
		//
		//	/// <summary>
		//	/// ������������ � �����������, ������������� 
		//	/// �������������� ������� ���������� �������.
		//	/// </summary>
		//	public sealed class SystemActivityEvents
		//	{
		//		/// <summary>
		//		/// �������� ����������� ��� �������������� ��������
		//		/// ���������� ������.
		//		/// </summary>
		//		private SystemActivityEvents(){}
		//
		//		/// <summary>
		//		/// ������������ ������� ����� � �������
		//		/// </summary>
		//		public const string LoggedIn = "login";
		//		/// <summary>
		//		/// ������������ ����� �� �������
		//		/// </summary>
		//		public const string LoggedOut = "logout";
		//		/// <summary>
		//		/// ������������ ������� ������
		//		/// </summary>
		//		public const string PasswordChanged = "chgpwd";
		//		/// <summary>
		//		/// � ������� ��������
		//		/// </summary>
		//		public const string AccessDenied = "denacc";
		//		/// <summary>
		//		/// ��������� ��������� �� ���������
		//		/// </summary>
		//		public const string DatabaseAccepted = "accsdb";
		//		/// <summary>
		//		/// ��������� ��������� �� ��������
		//		/// </summary>
		//		public const string DatabaseRejected = "rejsdb";
		//		/// <summary>
		//		/// ������� ���������� ������� ���� �������.
		//		/// </summary>
		//		public const string ActivityCleared = "clract";
		//		/// <summary>
		//		/// ������ �� ����������� ������.
		//		/// </summary>
		//		public const string ExceptionError = "error";
		//		/// <summary>
		//		/// ������ �� ����������� ������.
		//		/// </summary>
		//		public const string TraceMessage = "message";
		//		/// <summary>
		//		/// ������ �������� ������ �� �������� �������
		//		/// </summary>
		//		public const string AbsentAssemblyError = "misasmerr";
		//		/// <summary>
		//		/// ������ �������� ���������� � �������� ������
		//		/// </summary>
		//		public const string AbsentComponentError = "miscomerr";
		//		/// <summary>
		//		/// ������ ������ ������ � �������� ����������
		//		/// </summary>
		//		public const string AbsentMethodError = "missuberr";
		//		/// <summary>
		//		/// ���������� ������������ ��� ������� AssemblyId
		//		/// </summary>
		//		public const string ConfigAssemblyError = "cfgasmerr";
		//		/// <summary>
		//		/// ���������� ������������ ��� ������� ComponentId
		//		/// </summary>
		//		public const string ConfigComponentError = "cfgcomerr";
		//		/// <summary>
		//		/// ���������� ������������ ��� ������� MethodId
		//		/// </summary>
		//		public const string ConfigMethodError = "cfgmeterr";
		//	}
		//
		//	/// <summary>
		//	/// ������������ � ����������� ������������� ���������
		//	/// �����������.
		//	/// </summary>
		//	public class SystemCategories
		//	{
		//		/// <summary>
		//		/// �������� ����������� ��� �������������� ��������
		//		/// ���������� ������.
		//		/// </summary>
		//		private SystemCategories(){}
		//
		//		/// <summary>
		//		/// ��������� ������������� ������ ���������
		//		/// ������ ����������� ��������� IContextFactory.
		//		/// ���������� ������ ��������� ������������ ���
		//		/// �������� � ���������� ��������� ���� ������
		//		/// ������������. � ������� Variables �����������
		//		/// ���������������� ������������ ��������� ������
		//		/// ������������. ComponentId = ������ ����������,
		//		/// VariableId = "caption".
		//		/// </summary>
		//		public const string ContextFactorys = "context";
		//	}

		/// <summary>
		/// ��������� IRelaxable ����������
		/// ��������� �������� �����������. ������������ ���
		/// ����������� ����������� �������� ���������� � ���
		/// ���������� ��� ��������.
		/// </summary>
		/*public interface IRelaxable : Cerebrum.IDisposable 
		{
			/// <summary>
			/// ����� ���������� Relax ���������� ����������� �������� ���
			/// ����������� ����������� �������� ����������� ����������. � ������
			/// ������������� �������� ��������� ������ ���������� �������� e.Cancel = true.
			/// � ������ ���� ���������� ��������� ��������� �������� � ��� �������� �������,
			/// ��������� ������ ��������� ��� ��������. ��� ��� � ������� ������ ������ Clean
			/// ��������� �������� ������� ����������� ��� ���������� ����� ���� ��� ���������.
			/// </summary>
			void Relax(object sender, System.ComponentModel.CancelEventArgs e);
		
			/// <summary>
			/// ����� ���������� Clean ���������� ����������� �������� ���
			/// �������������� ������������ ���� �������� ����������� ����������.
			/// ���������� ��������� ������ ��������� ���� ������. �� ����� �����
			/// ������ �� ������������� ������� ���� ����������� ����������� 
			/// ��� ����������� ���������������� ������� ����������. ��� ������
			/// ��������� ����������� ������ ���� ������������ � ������ Relax
			/// </summary>
			void Dispose(object sender, System.EventArgs e);
		
			/// <summary>
			/// ������� ���������� Relaxing ������������ ����������� ���
			/// ���������� ���� ���������������� ����������� � ��� ��� ��������������
			/// �������� �������� ����������. � ������ ���� �������� �������� ����������
			/// ������������ ��������� ������������ �� ��� ������� ���������� ����������
			/// �� ����� �������� �������� ���������� �������� e.Cancel = true.
			/// </summary>
			event System.ComponentModel.CancelEventHandler Relaxing;
		
	//		/// <summary>
	//		/// ������� ���������� Cleaning ������������ ����������� ���
	//		/// ���������� ���� ���������������� ����������� � ��� ��� 
	//		/// ��������� ��� �������� � ���������� ������� ��� ������������� ��������
	//		/// � ��������������� �����������. ��� ����������� ���������� ������
	//		/// ���������� ������������ ������ ��������� � ����� � ���� ��� ������.
	//		/// </summary>
	//		event System.EventHandler Cleaning;

	}*/
	}
}
