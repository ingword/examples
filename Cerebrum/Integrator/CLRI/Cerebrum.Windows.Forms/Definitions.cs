using System;

namespace Cerebrum.Windows.Forms
{

	public interface IVisibleProperty
	{
		bool Visible {get;set;}
	}
	public interface ICheckedProperty
	{
		bool Checked {get;set;}
	}

	public interface IUiControl : IReleasable, Cerebrum.Integrator.IEnabledProperty, ICheckedProperty, IVisibleProperty
	{
		void AttachCommand(Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor command);
		Cerebrum.ObjectHandle CommandId {get;set;} 
		event System.ComponentModel.CancelEventHandler InvokeCommand;
		int MergeOrder {get;}
	}
	public interface IUiControlEvents : Cerebrum.Integrator.IEnabledChangedEvent
	{
		event System.EventHandler VisibleChanged;
		event System.EventHandler CheckedChanged;
	}

	public interface IUiCommandsCollection: System.Collections.IEnumerable
	{
		Cerebrum.Windows.Forms.IUiCommand this[int index] {get;}
		Cerebrum.Windows.Forms.IUiCommand this[string name] {get;}
		int Count {get;}
	}

	public interface IPrimaryWindow
	{
		//Cerebrum.Windows.Forms.IUiCommand GetCommand(string name);
		IUiCommandsCollection Commands {get;}
		Cerebrum.Windows.Forms.IUiCommand [] GetCommands(string messageName);
		object ActiveComponent{get;set;}
		void Close();
		event System.EventHandler Ready;
	}

	public interface IUiContainer
	{
		Cerebrum.Windows.Forms.IUiControlsCollection Controls {get;}
	}

	public interface IUiCommand : IUiContainer, IUiControl, IUiControlEvents
	{
		bool ActivateCommand(bool cancel);
	}

	public interface IUiControlsCollection : System.Collections.ICollection 
	{
		int Add(IUiControl control);
		void Remove(IUiControl control);

		IUiControl this[int index] {get; set;}
	}

	public interface IUiControlFactory
	{
		IUiControl CreateControl(object descriptor);
	}

	public class ConnectorEventArgs : System.ComponentModel.CancelEventArgs
	{
		protected Cerebrum.IConnector m_Connector;

		public ConnectorEventArgs(bool cancel, Cerebrum.IConnector connector) : base(cancel)
		{
			m_Connector = connector;
		}

		public Cerebrum.IConnector Connector
		{
			get
			{
				return m_Connector;
			}
		}
	}

	public delegate void ConnectorEventHandler(object sender, ConnectorEventArgs e);

	/*
	public interface IChainable: IRelaxable
	{
		/// <summary>
		/// ���� ��� �������, ������ ��� ������ ��������� ���������������� ����������.
		/// ������ ���������� �� ����������� ���������� ������� ���������� ������������.
		/// � ���� ������ � �������� ���������� ���������� ������ UiCommand ���������� �
		/// �� ��������� �������� CommandId ����������� �� ����������� ��������.
		/// </summary>
		/// <param name="sender">������ �������� �������. �������� UiCommand</param>
		/// <param name="e">��������� �������</param>
		void Chain(object sender, System.ComponentModel.CancelEventArgs e);
		event System.ComponentModel.CancelEventHandler Chaining;
	}
	*/

//	/// <summary>
//	/// ���������, ������� ������� ������������ ��� 
//	/// ���������� ���������� ��� ����������� ���������
//	/// �������. 
//	/// </summary>
//	public interface IComponent: IRelaxable
//	{
//		/// <summary>
//		/// �������� ��� ��������� � ��������� ������ ����������
//		/// </summary>
//		string ComponentId {get; set;}
//		/// <summary>
//		/// �������� ��� ��������� � ��������� ��������� ����������
//		/// </summary>
//		Cerebrum.Integrator.DomainContext DomainContext {get; set;}
//		/// <summary>
//		/// �������� ������������ ����������� ����������� �������� �����������
//		/// � �������� � ���� �����. true - 1 ���������, false - ����� �����������.
//		/// </summary>
//		bool Singleton {get; set;}
//		/// <summary>
//		/// �������� ��� ����������� ��������� �� ������ ��������� � �������� �����������.
//		/// ����������� ��� �������������� ���������� ���������� ������ � ���� �� ����������.
//		/// </summary>
//		bool IsCleaning {get;}
//		/// <summary>
//		/// ���� ��� �������, ������ ��� ������ ��������� ���������������� ����������.
//		/// ������ ���������� �� ����������� ���������� ������� ���������� ������������.
//		/// � ���� ������ � �������� ���������� ���������� ������ UiCommand ���������� �
//		/// �� ��������� �������� CommandId ����������� �� ����������� ��������.
//		/// </summary>
//		/// <param name="sender">������ �������� �������. �������� UiCommand</param>
//		/// <param name="e">��������� �������</param>
//		void Execute(object sender, System.ComponentModel.CancelEventArgs e);
//
//		event System.ComponentModel.CancelEventHandler Executing;
//	}
//
//	public enum SerializeOperation
//	{
//		None,
//		Init,
//		Load,
//		Save,
//		Take,
//		Drop,
//		Kill
//	}
//
//	public interface ISerializable: IComponent
//	{
//		string InstanceId{get;set;}
//		void Serialize(object context, Cerebrum.Integrator.SerializeOperation operation);
//	}
//
//	public class CommandEventArgs: System.ComponentModel.CancelEventArgs 
//	{
//		protected UiCommand m_Command;
//		public UiCommand Command
//		{
//			get
//			{
//				return m_Command;
//			}
//			set
//			{
//				m_Command = value;
//			}
//		}
//	}
//
//	/// <summary>
//	/// �������� ������� IComponent.Execute
//	/// ���������� � ������ �������� ��������.
//	/// </summary>
//	public class CreateCommandEventArgs: CommandEventArgs 
//	{
//		public CreateCommandEventArgs(UiCommand command)
//		{
//			this.Cancel = false;
//			this.m_Command = command;
//		}
//	}
//	
//	/// <summary>
//	/// �������� ������� IComponent.Execute
//	/// ���������� � ������ ����������� �������� � ����������.
//	/// </summary>
//	public class AttachCommandEventArgs: CommandEventArgs 
//	{
//		public AttachCommandEventArgs(UiCommand command)
//		{
//			this.Cancel = false;
//			this.m_Command = command;
//		}
//	}
//	/// <summary>
//	/// �������� ������� IComponent.Execute
//	/// ���������� � ������ ���������� �������� �� ����������.
//	/// </summary>
//	public class DetachCommandEventArgs: CommandEventArgs 
//	{
//		public DetachCommandEventArgs(UiCommand command)
//		{
//			this.Cancel = false;
//			this.m_Command = command;
//		}
//	}
//	/// <summary>
//	/// �������� ������� IComponent.Execute
//	/// ���������� � ������ ������������� ��������� ��������.
//	/// </summary>
//	public class InvokeCommandEventArgs: CommandEventArgs 
//	{
//		protected object m_Result;
//		
//		public InvokeCommandEventArgs(UiCommand command) :this(command, false)
//		{
//		}
//		public InvokeCommandEventArgs(UiCommand command, bool cancel)
//		{
//			this.Cancel = cancel;
//			this.m_Command = command;
//			this.m_Result = null;
//		}
//
//		public object Result
//		{
//			get
//			{
//				return m_Result;
//			}
//			set
//			{
//				m_Result = value;
//			}
//		}
//	}
//
//	/// <summary>
//	/// ���������, ������� ������ ����������� ������� �����������
//	/// ������� ����������� ����������� ��� �������� �����������,
//	/// ��������� ������������� ������� ��������. ���������� �����������
//	/// ����� ������� �� ��������� � �������� � �������� Assemblies, Instances
//	/// </summary>
//	public interface IComponentFactory
//	{
//		/// <summary>
//		/// ����� ��������� ��������� ����������
//		/// </summary>
//		object CreateInstance();
//	}
//
//	public interface IWorkspace
//	{
//		Cerebrum.Integrator.SystemDatabase SystemDatabase {get;}
//		object AcquireInstance(string instanceId, string componentId);
//	}


//	public class DatabaseLocationEventArgs : System.EventArgs
//	{
//		protected string m_PrimaryDirectory;
//		protected string m_DatabaseFileName;
//		protected string m_ActivityFileName;
//
//		public DatabaseLocationEventArgs()
//		{
//			m_PrimaryDirectory = string.Empty;
//			m_DatabaseFileName = string.Empty;
//			m_ActivityFileName = string.Empty;
//		}
//
//		public DatabaseLocationEventArgs(string primaryDirectory, string databaseFileName, string activityFileName)
//		{
//			m_PrimaryDirectory = primaryDirectory;
//			m_DatabaseFileName = databaseFileName;
//			m_ActivityFileName = activityFileName;
//		}
//		
//		public string PrimaryDirectory
//		{
//			get
//			{
//				return this.m_PrimaryDirectory;
//			}
//			set
//			{
//				this.m_PrimaryDirectory = value;
//			}
//		}
//		public string DatabaseFileName
//		{
//			get
//			{
//				return this.m_DatabaseFileName;
//			}
//			set
//			{
//				this.m_DatabaseFileName = value;
//			}
//		}
//		public string ActivityFileName
//		{
//			get
//			{
//				return this.m_ActivityFileName;
//			}
//			set
//			{
//				this.m_ActivityFileName = value;
//			}
//		}
//	}

}
