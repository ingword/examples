// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// Summary description for MessageBoxButtons.
	/// </summary>
	public enum MessageBoxStyle
	{
		BriefDismiss = 1,
		BriefOK = 2,
		/*BriefOKCancel = 3,
		BriefIgnoreAbort = 4,
		BriefRetryAbortIgnore = 5,
		BriefRetryCancel = 6,
		BriefYesNo = 7,
		BriefYesNoCancel = 8,*/
		ShortAbortRetryIgnore = 33,
		ShortOK = 34,
		ShortOKCancel = 35,
		ShortRetryCancel = 36,
		ShortYesNo = 37,
		ShortYesNoCancel = 38
	}
}
