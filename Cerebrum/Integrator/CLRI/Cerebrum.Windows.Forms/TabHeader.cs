// TabHeader : implementation file
// Copyright (C) Dmitry Shuklin 2004-2009. All rights reserved.

// The TabHeader Windows.Forms.Control is based on Boris Bord (http://www.codeproject.com/KB/miscctrl/TabHeader.aspx) project

//#define DRAG_AND_DROP_SUPPORT

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Cerebrum.Windows.Forms
{
#if DRAG_AND_DROP_SUPPORT
	#region DRAG AND DROP
	public class DragControlsEventArgs: EventArgs
	{		
		public DragControlsEventArgs(System.Windows.Forms.Control DragTarget, System.Windows.Forms.Control[] DragSource) 
		{
			this.m_DragTarget=DragTarget;
			this.m_DragSource=DragSource;
		}

		protected DragDropEffects m_Effect = DragDropEffects.None;
		public DragDropEffects Effect
		{
			get
			{
				return m_Effect;
			}
			set
			{
				m_Effect = value;
			}
		}

		public DragControlsEventArgs() 
		{
			this.m_DragTarget=null;
			this.m_DragSource=null;
		}

		protected System.Windows.Forms.Control m_DragTarget;
		public System.Windows.Forms.Control DragTarget
		{
			get 
			{
				return m_DragTarget;
			}
			set
			{	
				m_DragTarget = value;									
			}
		}
	

		protected System.Windows.Forms.Control[] m_DragSource;			
		public System.Windows.Forms.Control[] DragSource
		{
			get 
			{
				return m_DragSource;
			}
			set
			{	
				m_DragSource = value;									
			}
		}

	}
	
	public delegate void DragControlsEventHandler(object sender, DragControlsEventArgs me);	
	#endregion
#endif

	public interface ITabElement: IVisibleProperty
	{
		bool Fellowship {get;set;}
	}

	public interface ITabButton : ITabElement, ICheckedProperty
	{
		event System.EventHandler CheckedChanged;
	}

	public class TabPage : System.Windows.Forms.Panel, ITabElement
	{
		protected ITabButton m_HeaderButton;
		[System.ComponentModel.DefaultValue(null)]
		public ITabButton HeaderButton
		{
			get
			{
				return m_HeaderButton;
			}
			set
			{
				if(m_HeaderButton!=value)
				{
					if(m_HeaderButton!=null)
					{
						m_HeaderButton.CheckedChanged -= new System.EventHandler(HeaderButton_CheckedChanged);
					}
					m_HeaderButton = value;
					if(m_HeaderButton!=null)
					{
						m_HeaderButton.CheckedChanged += new System.EventHandler(HeaderButton_CheckedChanged);
					}
				}
			}
		}

		protected System.Windows.Forms.MenuItem m_MenuItem;
		[System.ComponentModel.DefaultValue(null)]
		public System.Windows.Forms.MenuItem MenuItem
		{
			get
			{
				return m_MenuItem;
			}
			set
			{
				m_MenuItem = value;
			}
		}

		private void HeaderButton_CheckedChanged(object sender, System.EventArgs e)
		{
			OnHeaderButtonCheckedChanged(sender, e);
		}
		protected virtual void OnHeaderButtonCheckedChanged(object sender, System.EventArgs e)
		{
			this.Visible = m_HeaderButton.Checked;
		}
		
		protected override void OnVisibleChanged(System.EventArgs e)
		{
			try
			{ // something wrong in MenuItem
				if ( m_MenuItem != null)
				{
					m_MenuItem.Visible = this.Visible;
				}
			}
			catch
			{
			}
			base.OnVisibleChanged(e);
		}

		protected override void OnEnabledChanged(System.EventArgs e)
		{
			try
			{
			if ( m_MenuItem != null)
			{
				m_MenuItem.Enabled = this.Enabled;
			}
			}
			catch
			{
			}
			base.OnEnabledChanged(e);
		}

		#region ITabElement Members

		private bool m_MyVisible = true;
		[System.ComponentModel.DefaultValue(true)]
		public new bool Visible 
		{
			set 
			{
				if( value != m_MyVisible ) 
				{
					m_MyVisible = value;
					TabHeader header = null;
					if( m_TabFellowship && (header = (this.Parent as TabHeader)) != null )
					{
						header.RefreshLayout();
					}
					else
					{
						base.Visible = m_MyVisible;
					}
				}
			}
			get 
			{
				return m_MyVisible;
			}
		}

		protected bool m_TabFellowship = false;
		[System.ComponentModel.DefaultValue(false)]
		public bool Fellowship
		{
			get
			{
				return m_TabFellowship;
			}
			set
			{
				m_TabFellowship = value;
			}
		}

		#endregion
	}


	public class TabButton : ImageButton, ITabButton
	{	
		[Browsable(true), EditorBrowsable(EditorBrowsableState.Always),	]
		public new bool AllowDrop 
		{
			get 
			{
				return base.AllowDrop;
			}
			set 
			{				
				base.AllowDrop = value;
			}
		}
		
		/*public bool BaseVisible 
		{
			get 
			{
				return base.Visible;
			}
			set 
			{
				base.Visible = value;
			}
		}*/



		#region ITabElement Members

		private bool m_MyVisible = true;
		[System.ComponentModel.DefaultValue(true)]
		public new bool Visible 
		{
			set 
			{
				if( value != m_MyVisible ) 
				{
					m_MyVisible = value;
					TabHeader header = null;
					if( m_TabFellowship && (header = (this.Parent as TabHeader)) != null )
					{
						header.RefreshLayout();
					}
					else
					{
						base.Visible = m_MyVisible;
					}
				}
			}
			get 
			{
				return m_MyVisible;
			}
		}

		protected bool m_TabFellowship = true;
		[System.ComponentModel.DefaultValue(true)]
		public bool Fellowship
		{
			get
			{
				return m_TabFellowship;
			}
			set
			{
				m_TabFellowship = value;
			}
		}

		#endregion
	}

	public enum LayoutDirection
	{
		Horizontal,
		Vertical
	}

	public enum SelectionBehavior
	{
		SingleSelect,
		MultiSelect,
		ManualSelect
	}
	///
	///
	///
	/// <summary>
	/// Summary description for TabHeader.
	/// </summary>
	///
	///
	public class TabHeader : TabPage 
	{

		// private fields
		private ImageButton m_ScrollLeftButton;
		private ImageButton m_ScrollRightButton;		
		private ITabButton m_SelectedButton;
		private ITabButton m_LastShiftKeyTab;
		private int m_LeftFirst;		
		private bool m_Scrolling; 
		private LayoutDirection m_Direction; 
		private SelectionBehavior m_Behavior;

		// proprties
		[System.ComponentModel.DefaultValue(SelectionBehavior.SingleSelect)]
		public SelectionBehavior Behavior 
		{
			get 
			{
				return m_Behavior;
			}
			set 
			{
				m_Behavior = value;
				RefreshLayout();
			}
		}

		[System.ComponentModel.DefaultValue(LayoutDirection.Horizontal)]
		public LayoutDirection Direction 
		{
			get 
			{
				return m_Direction;
			}
			set 
			{
				m_Direction = value;
				RefreshLayout();
			}
		}

		public event System.EventHandler SelectedTabChanged;
		protected void OnSelectedTabChanged(System.EventArgs e)
		{
			if(SelectedTabChanged!=null)
			{
				SelectedTabChanged(this, e);
			}
		}

		[System.ComponentModel.DefaultValue(null)]
		public ITabButton SelectedButton 
		{
			get 
			{
				return m_SelectedButton;
			}
			set 
			{
				if( m_SelectedButton == value )
					return;
 
				ITabButton oldTab = m_SelectedButton;

				m_SelectedButton = value;

				if( m_Behavior == SelectionBehavior.SingleSelect && oldTab != null )
					oldTab.Checked = false;

				if( (m_SelectedButton != null) && (m_Behavior != SelectionBehavior.ManualSelect) && (!m_SelectedButton.Checked) )
					m_SelectedButton.Checked = true;

				RefreshLayout();
				
				if(oldTab != m_SelectedButton)
					OnSelectedTabChanged(System.EventArgs.Empty);
			}
		}

		[System.ComponentModel.DefaultValue(null)]
		public ImageButton ScrollLeftButton
		{
			get
			{
				return m_ScrollLeftButton;
			}
			set 
			{
				if(m_ScrollLeftButton != value)
				{
					if(m_ScrollLeftButton!=null)
					{
						m_ScrollLeftButton.Click -= new EventHandler(ScrollLeftButton_Click);
					}
					m_ScrollLeftButton = value;
					if(m_ScrollLeftButton!=null)
					{
						m_ScrollLeftButton.Visible = false;
						m_ScrollLeftButton.Click += new EventHandler(ScrollLeftButton_Click);
					}
				}
			}
		}

		[System.ComponentModel.DefaultValue(null)]
		public ImageButton ScrollRightButton
		{
			get
			{
				return m_ScrollRightButton;
			}
			set 
			{
				if(m_ScrollRightButton!=value)
				{					
					if(m_ScrollRightButton!=null)
					{
						m_ScrollRightButton.Click -= new EventHandler(ScrollRightButton_Click);
					}
					m_ScrollRightButton = value;
					if(m_ScrollRightButton!=null)
					{
						m_ScrollRightButton.Visible = false;
						m_ScrollRightButton.Click += new EventHandler(ScrollRightButton_Click);
					}
				}
			}
		}

		
#if DRAG_AND_DROP_SUPPORT
		#region DRAG AND DROP
		public event DragControlsEventHandler DragDropButtons;
		public event DragControlsEventHandler DragOverButtons;
		
		protected virtual void OnDragDropButtons(DragControlsEventArgs de)
		{
			if(DragDropButtons!=null)
			{
				DragDropButtons(this, de);
			}
		}
		
		protected virtual void OnDragOverButtons(DragControlsEventArgs de)
		{
			if(DragOverButtons!=null)
			{
				DragOverButtons(this, de);
			}
		}

		private void Control_DragStarting(object sender, DragEventArgs e)
		{
			ArrayList ArrayTabButton =new ArrayList();// 

			ITabButton currentButton = null;						
			if( typeof(ITabButton).IsAssignableFrom(sender.GetType()) ) 	currentButton = (ITabButton)sender;				
			
			if(currentButton != null) 
			{						 
				if (currentButton.Checked)
				{			
					foreach( Control c in Controls ) 
					{
						if( typeof(ITabButton).IsAssignableFrom(c.GetType()) ) 
						{							
							ITabButton tabButton = (ITabButton)c; 
							if( tabButton.Visible && tabButton.Checked &&tabButton.AllowDrag) 
							{
								ArrayTabButton.Add(tabButton);																		
							}
						}
					} // foreach					
				}
				else
				{
					ArrayTabButton.Add(currentButton);	
				}	
			}						
				
			e.Data.SetData(ArrayTabButton.ToArray(typeof(System.Windows.Forms.Control)));			
		}

		
		private void Control_DragDrop(object sender, DragEventArgs de) 
		{			
			DragControlsEventArgs me =new DragControlsEventArgs();

			me.DragTarget = (System.Windows.Forms.Control) sender;
			me.DragSource=(System.Windows.Forms.Control[])de.Data.GetData(de.Data.GetFormats(false)[0]);										
			
			OnDragDropButtons(me);
			de.Effect = me.Effect;
		}
			
		private void Control_DragOver(object sender, DragEventArgs de)
		{
			DragControlsEventArgs me =new DragControlsEventArgs();

			me.DragTarget = (System.Windows.Forms.Control) sender;
			me.DragSource=(System.Windows.Forms.Control[])de.Data.GetData(de.Data.GetFormats(false)[0]);										
			
			OnDragOverButtons(me);	
			de.Effect = me.Effect;
		}
		#endregion
#endif
		///
		///
		///
		public TabHeader() 
		{
			m_LeftFirst = 0;
			m_Scrolling = false;
			m_Behavior = SelectionBehavior.SingleSelect;;
			m_Direction = LayoutDirection.Horizontal; 
			m_SelectedButton = null;
			m_LastShiftKeyTab = null;

			this.ControlAdded += new ControlEventHandler(TabHeader_OnControlAdded);
			this.ControlRemoved += new ControlEventHandler(TabHeader_OnControlRemoved);

#if DRAG_AND_DROP_SUPPORT
			this.DragOver+=new DragEventHandler(Control_DragOver);
			this.DragDrop+=new DragEventHandler(Control_DragDrop);
#endif
		}
           

		private void TabHeader_OnControlAdded(object sender,
			ControlEventArgs e) 
		{
			if( typeof(ITabButton).IsAssignableFrom(e.Control.GetType()) ) 
			{
				ITabButton tabButton = (ITabButton)e.Control;
				e.Control.Click += new EventHandler(TabButton_OnClick); 
				tabButton.CheckedChanged += new EventHandler(TabButton_OnCheckedChanged); 

#if DRAG_AND_DROP_SUPPORT
				e.Control.DragOver    +=new DragEventHandler(Control_DragOver);
				e.Control.DragDrop    +=new DragEventHandler(Control_DragDrop);								
				e.Control.DragStarting+=new DragEventHandler(Control_DragStarting);
#endif
			}
		}


		private void TabHeader_OnControlRemoved(object sender,
			ControlEventArgs e) 
		{
			if( typeof(ITabButton).IsAssignableFrom(e.Control.GetType()) ) 
			{
				ITabButton tabButton = (ITabButton)e.Control;
				e.Control.Click -= new EventHandler(TabButton_OnClick); 
				tabButton.CheckedChanged -= new EventHandler(TabButton_OnCheckedChanged); 

				if(this.m_SelectedButton == tabButton)
					this.m_SelectedButton=null;// unselect
				if(this.m_LastShiftKeyTab == tabButton)
					this.m_LastShiftKeyTab=null;// unselect

#if DRAG_AND_DROP_SUPPORT
				e.Control.DragOver    -=new DragEventHandler(Control_DragOver);
				e.Control.DragDrop    -=new DragEventHandler(Control_DragDrop);								
				e.Control.DragStarting-=new DragEventHandler(Control_DragStarting);			
#endif
			}
		}


		private void TabButton_OnClick(object sender, 
			System.EventArgs e) 
		{

			if(sender==null || !typeof(ITabButton).IsAssignableFrom(sender.GetType()) ) 
			{
				return;
			}

			ITabButton currentButton = (ITabButton)sender;

			if( m_Behavior == SelectionBehavior.SingleSelect || m_Behavior == SelectionBehavior.ManualSelect ) 
			{
				m_LastShiftKeyTab = null;
				this.SelectedButton = currentButton;
			}
			else if( m_Behavior == SelectionBehavior.MultiSelect ) 
			{
				// Multiselect
   
				System.Windows.Forms.Keys keys = ModifierKeys;
				bool bCtrlPressed = (keys&Keys.Control) == Keys.Control;
				bool bShiftPressed = (keys&Keys.Shift) == Keys.Shift;
               
				if( bCtrlPressed ) 
				{
					m_LastShiftKeyTab = null;

					if( currentButton != null ) 
						currentButton.Checked = !currentButton.Checked;

					if( m_SelectedButton == currentButton ) 
					{
						if( !currentButton.Checked ) 
						{
							m_SelectedButton = null;

							// m_SelectedButton = first checked
							foreach( Control c in Controls ) 
							{
								if( typeof(ITabButton).IsAssignableFrom(c.GetType()) ) 
								{
									ITabButton tabButton = (ITabButton)c; 
									if( tabButton.Visible && tabButton.Checked ) 
									{
										m_SelectedButton = tabButton;
										break;
									}
								}
							} // foreach
						}
					}
					else
					{
						m_SelectedButton = currentButton;
					}
				}
				else if( bShiftPressed && currentButton != null ) 
				{
					// TODO: ShiftKeyUp -- m_SelectedButton = currentButton
					m_LastShiftKeyTab = currentButton;

					int first = -1, last = -1,  m = -1;

					// has no selection ? m_SelectedButton = first item from container
					if( m_SelectedButton == null ) 
					{
						foreach( Control c in Controls ) 
						{
							if( typeof(ITabButton).IsAssignableFrom(c.GetType()) ) 
							{
								ITabButton tabButton = (ITabButton)c; 
								if( tabButton.Visible ) 
								{
									m_SelectedButton = tabButton;
									break;
								}
							}
						}
					}

					Control currentButtonCtl = currentButton as Control;
					Control selectedButtonCtl = m_SelectedButton as Control;
					// get btn range to be checked
					if( m_Direction == LayoutDirection.Vertical ) 
					{
						first = currentButtonCtl.Top+currentButtonCtl.Height/2;
						last = selectedButtonCtl.Top+selectedButtonCtl.Height/2;
					}
					else 
					{
						first = currentButtonCtl.Left+currentButtonCtl.Width/2;
						last = selectedButtonCtl.Left+selectedButtonCtl.Width/2;
					}

					if( first > last ) 
					{
						m = last;
						last = first;
						first = m;
					}

					// check btns
					foreach( Control c in Controls ) 
					{
						if( typeof(ITabButton).IsAssignableFrom(c.GetType()) ) 
						{
							ITabButton tabButton = (ITabButton)c; 
							if( tabButton.Visible ) 
							{
                           
								if( m_Direction == LayoutDirection.Vertical ) 
									m = c.Top+c.Height/2;
								else
									m = c.Left+c.Width/2;

								if( first <= m && last >= m ) 
									tabButton.Checked = true;
								else
									tabButton.Checked = false;
							}
						}
					} // foreach
				}
				else 
				{
					foreach( Control c in Controls ) 
					{
						if( typeof(ITabButton).IsAssignableFrom(c.GetType()) ) 
						{
							ITabButton tabButton = (ITabButton)c; 
							tabButton.Checked = (currentButton == tabButton);
						}
					}

					m_SelectedButton = currentButton;
				}

				RefreshLayout();
				OnSelectedTabChanged(System.EventArgs.Empty);
			}            
		}


		private void TabButton_OnCheckedChanged(object sender, 
			System.EventArgs e)
		{
			if(sender!=null && typeof(ITabButton).IsAssignableFrom(sender.GetType()))
			{
				if(m_Behavior == SelectionBehavior.SingleSelect && (sender as ITabButton).Checked)
				{
					this.SelectedButton = (ITabButton)sender;
				}
			}
		}


		private void ScrollLeftButton_Click(object sender, 
			System.EventArgs e) 
		{
			--m_LeftFirst;
			RefreshLayout();
		}


		private void ScrollRightButton_Click(object sender, 
			System.EventArgs e) 
		{
			++m_LeftFirst;
			RefreshLayout();
		}


		/*public void AddTab(int index, ITabButton tab, Object o) 
		{
			Controls.Add(tab);
			RefreshLayout();     
		}*/

		//
		//
		//

		public void RefreshLayout() 
		{
			int u, i;
			bool bs = !(m_ScrollRightButton == null || m_ScrollLeftButton == null);

			Size size = this.Size;
			Size isize;
			Size scrollersSize;

			if( bs ) 
				isize = m_ScrollRightButton.Size;
			else 
				isize = new Size(0, 0);

			if( m_Direction == LayoutDirection.Vertical ) 
			{
				scrollersSize = new Size(isize.Width, isize.Height*2+5);
			}
			else 
			{
				scrollersSize = new Size(isize.Width*2+5, isize.Height*2);
			}

			// calculate the sum width of all tabs... need scroling????
			int totalWidth = 0;
			int tabCnt = 0;

			ArrayList alWidths = new ArrayList();

			foreach( Control control in Controls ) 
			{
				ITabElement tabElement = (control as ITabElement);
				if( tabElement!=null ) 
				{
					if( tabElement.Visible && tabElement.Fellowship ) 
					{
						if( m_Direction == LayoutDirection.Vertical ) 
						{
							totalWidth += control.Size.Height;
							alWidths.Add(control.Size.Height);
						}
						else 
						{
							totalWidth += control.Size.Width;
							alWidths.Add(control.Size.Width);
						}
						tabCnt++;
					}
				}
			}
			totalWidth += (tabCnt - 1) * m_Interval;

			#region Vertical
			if( m_Direction == LayoutDirection.Vertical ) 
			{
				if( totalWidth >= size.Height ) 
				{
					m_Scrolling = bs & true;
					size.Height -= scrollersSize.Height;

					if( m_LeftFirst < 0 ) 
						m_LeftFirst = 0;

					// i = number of visible tabs from right side
					for( u = 0, i = 0; i < alWidths.Count; i++ ) 
					{
						u += (int)alWidths[alWidths.Count-i-1];
						if(i>0) u+= m_Interval;
						if( u > size.Height ) 
							break;
					}

					if( m_LeftFirst > (tabCnt-i) )
						m_LeftFirst = tabCnt-i;

				}
				else 
				{
					m_LeftFirst = 0;
					m_Scrolling = false;
				}
			}
			else 
			{
				if( totalWidth >= size.Width ) 
				{
					m_Scrolling = true&bs;
					size.Width -= scrollersSize.Width;

					if( m_LeftFirst < 0 ) 
						m_LeftFirst = 0;

					// i = number of visible tabs from right side
					for( u = 0, i = 0; i < alWidths.Count; i++ ) 
					{
						u += (int)alWidths[alWidths.Count-i-1];
						if(i>0) u+= m_Interval;
						if( u > size.Width ) 
							break;
					}

					if( m_LeftFirst > (tabCnt-i) )
						m_LeftFirst = tabCnt-i;

				}
				else 
				{
					m_LeftFirst = 0;
					m_Scrolling = false;
				}
			}
			#endregion

			//
			u = i = 0;
			foreach( Control control in Controls ) 
			{
				ITabElement tabElement = (control as ITabElement);
				if( tabElement!=null ) 
				{
					if( tabElement.Visible && tabElement.Fellowship ) 
					{
						int ci = i++;

						if( m_LeftFirst > ci ) 
						{
							control.Visible = false;
							continue;
						}

						if( m_Direction == LayoutDirection.Vertical ) 
						{
							Size curImgSize = control.Size;
							control.SetBounds(0, u, curImgSize.Width, curImgSize.Height);
							u += curImgSize.Height;

							control.Visible = (m_LeftFirst == ci) || !(u > size.Height)|!bs;
						}
						else 
						{
							Size curImgSize = control.Size;
							control.SetBounds(u, 0, curImgSize.Width, curImgSize.Height);
							u += curImgSize.Width;

							control.Visible = (m_LeftFirst == ci) || !(u > size.Width)|!bs;
						}
						u += m_Interval;
					}
					else 
					{
						control.Visible = false;
					}
				}
			}

			#region Scrolling
			//
			if( m_Scrolling ) 
			{            
				if( m_Direction == LayoutDirection.Vertical ) 
				{
					m_ScrollLeftButton.SetBounds(5, 
						ClientRectangle.Bottom-scrollersSize.Height, 
						isize.Width, isize.Height);

					m_ScrollRightButton.SetBounds(5, 
						ClientRectangle.Bottom-scrollersSize.Height+isize.Width,
						isize.Width, isize.Height);
				}
				else 
				{
					m_ScrollLeftButton.SetBounds(ClientRectangle.Right-scrollersSize.Width, 
						(size.Height/2)-(isize.Height/2), 
						isize.Width, isize.Height);

					m_ScrollRightButton.SetBounds(ClientRectangle.Right-scrollersSize.Width+isize.Width, 
						(size.Height/2)-(isize.Height/2), 
						isize.Width, isize.Height);
				}

				m_ScrollLeftButton.Visible = true;
				m_ScrollRightButton.Visible = true;
			}
			else 
			{
				if( m_ScrollLeftButton != null )
					m_ScrollLeftButton.Visible = false;
				if( m_ScrollRightButton != null )
					m_ScrollRightButton.Visible = false;
			}
			#endregion
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		/*protected override void OnPaint(System.Windows.Forms.PaintEventArgs e) 
		{
			//RefreshLayout();
			base.OnPaint(e);
		}*/

		protected override void OnResize(System.EventArgs e) 
		{
			base.OnResize(e);
			RefreshLayout();
		}

		protected override void OnHeaderButtonCheckedChanged(object sender, 
			System.EventArgs e)
		{
			base.OnHeaderButtonCheckedChanged(sender, e);
			if(this.m_Behavior == SelectionBehavior.SingleSelect)
			{
				if(this.Visible)
				{
					if(this.SelectedButton==null)
					{
						foreach( Control c in Controls ) 
						{
							if( typeof(ITabButton).IsAssignableFrom(c.GetType()) ) 
							{
								ITabButton tabButton = (ITabButton)c; 
								if( tabButton.Visible ) 
								{
									this.SelectedButton = tabButton;
									break;
								}
							}
						}
					}
					else
					{
						if(!this.SelectedButton.Checked)
						{
							this.SelectedButton.Checked = true;
							RefreshLayout();
							OnSelectedTabChanged(System.EventArgs.Empty);
						}
					}
				}
				else
				{
					if(this.SelectedButton!=null)
					{
						this.SelectedButton.Checked = false;
						RefreshLayout();
						OnSelectedTabChanged(System.EventArgs.Empty);
					}
				}
			}
		}


		protected int m_Interval = 0;		
		[System.ComponentModel.DefaultValue(0)]
		public int Interval
		{
			get
			{
				return m_Interval;
			}
			set
			{
				m_Interval = value;
			}
		}
	}        

	/// <summary>
}
