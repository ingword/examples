// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// Summary description for ControlComponent.
	/// </summary>
	public class ControlComponent : System.Windows.Forms.UserControl, Cerebrum.IDisposable, Cerebrum.IComponent
	{
		protected Cerebrum.IConnector m_Connector;
		
		#region IComponent Members


		void Cerebrum.IComponent.SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			SetConnector(direction, connector);
		}
		protected virtual void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			m_Connector = connector;
			//connector.Disposing += new EventHandler(connector_Disposing);
		}
		#endregion


		public ControlComponent()
		{
			m_Destroying = false;
			m_Disposed = false;
		}

		
		/// <summary>
		/// ���������� ������� ��������� �������� true ���� ������ ��������
		/// ��������� � �������� �����������. ��������������� �� ������
		/// ������ Clear ���������� IReleasable ��� ������ Close ������� ������
		/// </summary>
		protected bool m_Destroying;
		protected bool m_Disposed;

		
		public Cerebrum.Integrator.DomainContext DomainContext
		{
			get
			{
				Cerebrum.IConnector connector = m_Connector.Workspace as Cerebrum.IConnector;
				return (Cerebrum.Integrator.DomainContext)connector.Component;
			}
		}

		/// <summary>
		/// ���������� ��� ������������� ������� ���������, �����������
		/// ������ �����������. �� ������ ������� ������ ����� ���������
		/// ������ ������������ �������� ��� ������������ ��������. ������
		/// ������� ������� ����������. ������� �������� ������������.
		/// </summary>
		/// <param name="sender">�������� �������</param>
		/// <param name="e">��������� �������</param>
		protected virtual void OnDisposing(object sender, System.EventArgs e)
		{
			System.EventHandler handler = (System.EventHandler)Events[Cerebrum.Specialized.EventKeys.DisposingEvent];
			if (handler != null)
				handler(this, e);
		}

		protected override void Dispose(bool disposing)
		{
			m_Destroying = true;
			if(!m_Disposed && disposing)
			{
				m_Disposed = true;
				this.OnDisposing(this, System.EventArgs.Empty);
			}
			base.Dispose (disposing);
		}

		/// <summary>
		/// ���������� ������ Release ���������� IReleasable
		/// </summary>
		/// <param name="sender">�������� �������</param>
		/// <param name="e">��������� ������� e.Cancel ��������������� � true 
		/// ��� ������ �������� ����� � ������ �����</param>
		public virtual bool Release(bool cancel)
		{
			System.ComponentModel.CancelEventArgs e = new CancelEventArgs(cancel);
			this.OnReleasing(this, e, m_Destroying);
			return e.Cancel;
		}

		/// <summary>
		/// ����������� ����� ��������� ����������� ������ ����������
		/// ��������� ������ ������ Relax. ����� ��������� ������ ���������
		/// �������� ��������� destroying. � ������ ���� ���� �������� = false
		/// ����� ��������� ����� ����� �������� ������ ���������� � ��������
		/// ������������ � ����������� ���������� �������� ��� ������ ��������.
		/// � ������ ������ ���� ���������� e.Cancel = true
		/// � ������ ���� �������� destroying = true ��������� �� �����
		/// ����������� �� ���������� ���������������� ������ �����������, �
		/// ������ ���������� ����� ������ ��������� ����� ������������
		/// ������ �������� ������.
		/// </summary>
		/// <param name="sender">�������� �������</param>
		/// <param name="e">��������� ������� e.Cancel ��������������� � true 
		/// ��� ������ �������� ����� � ������ �����</param>
		/// <param name="destroying">��������������� � true ���� ����� ������������ � 
		/// �������� ����������� �������� ����������</param>
		protected virtual void OnReleasing(object sender, System.ComponentModel.CancelEventArgs e, bool destroying)
		{
			if(!destroying)
			{
				System.ComponentModel.CancelEventHandler handler = (System.ComponentModel.CancelEventHandler)Events[Cerebrum.Specialized.EventKeys.ReleasingEvent];
				if (handler != null)
					handler(this, e);
			}
		}

		/// <summary>
		/// ���������� ������� Clearing ���������� IReleasable
		/// </summary>
		event System.EventHandler Cerebrum.IDisposable.Disposing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
		}


		/// <summary>
		/// ���������� ������� Relaxing ���������� IReleasable
		/// </summary>
		public event System.ComponentModel.CancelEventHandler Releasing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
		}

		/// <summary>
		/// �������� ��� ����������� ��������� �� ������ ��������� � �������� �����������.
		/// ���������� �������� ���������� m_Destroying
		/// </summary>
		public bool IsCleaning
		{
			get
			{
				return m_Destroying;
			}
		}


		/*		protected override void WndProc(ref System.Windows.Forms.Message m)
				{
					try
					{
						base.WndProc(ref m);
					}
					catch(System.Exception ex)
					{
						m_SystemContext.Application.AddActivityRow(Cerebrum.Integrator.SystemActivityEvents.ExceptionError, "VisualComponent.WndProc", ex.ToString());
					}
				}*/
	}
}
