// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.Reflection
{
	/// <summary>
	/// Summary description for UiCommand.
	/// </summary>
	public class UiCommandDescriptor : Cerebrum.Integrator.GenericComponent
	{
		public UiCommandDescriptor()
		{
		}


		public string Name
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Specialized.Concepts.NameAttribute));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.NameAttribute, value);
			}
		}

		public string NormalLeaveImageStreamName
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.NormalLeaveImageStreamNameAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.NormalLeaveImageStreamNameAttribute(this.DomainContext), value);
			}
		}

		public string NormalEnterImageStreamName
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.NormalEnterImageStreamNameAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.NormalEnterImageStreamNameAttribute(this.DomainContext), value);
			}
		}

		public object NormalLeaveImageBinaryData
		{
			get
			{
				return GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.NormalLeaveImageBinaryDataAttribute(this.DomainContext));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.NormalLeaveImageBinaryDataAttribute(this.DomainContext), value);
			}
		}

		public object NormalEnterImageBinaryData
		{
			get
			{
				return GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.NormalEnterImageBinaryDataAttribute(this.DomainContext));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.NormalEnterImageBinaryDataAttribute(this.DomainContext), value);
			}
		}

		public System.Drawing.Image GetNormalLeaveImage()
		{
			object o = this.NormalLeaveImageBinaryData;
			if(o is System.Drawing.Image) return (System.Drawing.Image)o;
			if(o is System.Array)
			{
				using(System.IO.MemoryStream fs = new System.IO.MemoryStream((byte[])o))
				{
					using(System.Drawing.Image org = System.Drawing.Bitmap.FromStream(fs))
					{
						return new System.Drawing.Bitmap(org);
					}
				}
			}
			string name = this.NormalLeaveImageStreamName;
			if(name==null || name.Length<1) return null;
			string path = System.IO.Path.Combine((this.DomainContext.Application as Cerebrum.Windows.Forms.Application).ApplicationDirectory, name);
			if(!System.IO.File.Exists(path)) return null;
			using(System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read))
			{
				using(System.Drawing.Image org = System.Drawing.Bitmap.FromStream(fs))
				{
					return new System.Drawing.Bitmap(org);
				}
			}
		}

		public System.Drawing.Image GetNormalEnterImage()
		{
			object o = this.NormalEnterImageBinaryData;
			if(o is System.Drawing.Image) return (System.Drawing.Image)o;
			if(o is System.Array)
			{
				using(System.IO.MemoryStream fs = new System.IO.MemoryStream((byte[])o))
				{
					using(System.Drawing.Image org = System.Drawing.Bitmap.FromStream(fs))
					{
						return new System.Drawing.Bitmap(org);
					}
				}
			}
			string name = this.NormalEnterImageStreamName;
			if(name==null || name.Length<1) return null;
			string path = System.IO.Path.Combine((this.DomainContext.Application as Cerebrum.Windows.Forms.Application).ApplicationDirectory, name);
			if(!System.IO.File.Exists(path)) return null;
			using(System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read))
			{
				using(System.Drawing.Image org = System.Drawing.Bitmap.FromStream(fs))
				{
					return new System.Drawing.Bitmap(org);
				}
			}
		}

		public Cerebrum.ObjectHandle MessageId
		{
			get
			{
				return (Cerebrum.ObjectHandle)GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.MessageIdAttribute(this.DomainContext));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.MessageIdAttribute(this.DomainContext), value);
			}
		}

		public Cerebrum.Runtime.NativeWarden GetAttachedControlsVector()
		{
			return this.GetAttributeContainer(Cerebrum.Windows.Forms.Specialized.Concepts.AttachedControlsAttribute(this.DomainContext)) as Cerebrum.Runtime.NativeWarden;
		}

		public int Ordinal
		{
			get
			{
				return Convert.ToInt32(GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.OrdinalAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.OrdinalAttribute(this.DomainContext), value);
			}
		}


		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						warden.Newobj(Cerebrum.Windows.Forms.Specialized.Concepts.MessageIdAttribute(this.DomainContext), Cerebrum.Runtime.KernelObjectKindId.Scalar);
						warden.Newobj(Cerebrum.Windows.Forms.Specialized.Concepts.AttachedControlsAttribute(this.DomainContext), Cerebrum.Runtime.KernelObjectKindId.Warden);
						//warden.Newobj(Cerebrum.Windows.Forms.Specialized.Concepts.OrdinalAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
					}
					break;
				}
			}
		}
	}
}
