// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.Reflection
{
	/// <summary>
	/// Summary description for UiHotKeyDescriptor.
	/// </summary>
	public class UiHotKeyDescriptor : Cerebrum.Integrator.GenericComponent
	{
		public UiHotKeyDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string KeyCode
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.KeyCodeAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.KeyCodeAttribute(this.DomainContext), value);
			}
		}
	}
}
