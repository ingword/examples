// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.Reflection
{
	/// <summary>
	/// Summary description for UiMenuTree.
	/// </summary>
	public class UiMenuItemDescriptor : Cerebrum.Integrator.GenericComponent
	{
		public UiMenuItemDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public Cerebrum.ObjectHandle ParentId
		{
			get
			{
				return (Cerebrum.ObjectHandle)GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.ParentIdAttribute(this.DomainContext));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.ParentIdAttribute(this.DomainContext), value);
			}
		}

		public string DisplayName
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Specialized.Concepts.DisplayNameAttribute));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.DisplayNameAttribute, value);
			}
		}


		public int MergeOrder
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.MergeOrderAttribute(this.DomainContext));
				if(o==null || Convert.IsDBNull(o))
				{
					return int.MaxValue;
				}
				else
				{
					return Convert.ToInt32(o);
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.MergeOrderAttribute(this.DomainContext), value);
			}
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						warden.Newobj(Cerebrum.Windows.Forms.Specialized.Concepts.ParentIdAttribute(this.DomainContext), Cerebrum.Runtime.KernelObjectKindId.Scalar);
						//warden.Newobj(Cerebrum.Specialized.Concepts.DisplayNameAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
						//warden.Newobj(Cerebrum.Windows.Forms.Specialized.Concepts.MergeOrderAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
					}
					break;
				}
			}
		}
	}
}
