// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.Reflection
{
	/// <summary>
	/// Summary description for UiBandPanelDescriptor.
	/// </summary>
	public class UiBandPlaceDescriptor : UiBandItemDescriptor
	{
		public UiBandPlaceDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public int Width
		{
			get
			{
				return Convert.ToInt32(GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.WidthAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.WidthAttribute(this.DomainContext), value);
			}
		}

	}
}
