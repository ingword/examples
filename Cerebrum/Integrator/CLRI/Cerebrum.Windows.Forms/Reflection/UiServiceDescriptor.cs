// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.Reflection
{
	/// <summary>
	/// Summary description for UiService.
	/// </summary>
	public class UiServiceDescriptor : Cerebrum.Integrator.GenericComponent
	{
		public UiServiceDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public string Name
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Specialized.Concepts.NameAttribute));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.NameAttribute, value);
			}
		}
		
		public int Ordinal
		{
			get
			{
				return Convert.ToInt32(GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.OrdinalAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.OrdinalAttribute(this.DomainContext), value);
			}
		}

		public Cerebrum.Runtime.NativeWarden GetAttachedCommandsVector()
		{
			return this.GetAttributeContainer(Cerebrum.Windows.Forms.Specialized.Concepts.AttachedCommandsAttribute(this.DomainContext)) as Cerebrum.Runtime.NativeWarden;
		}

		public virtual bool CommandActivated(Cerebrum.IConnector message, object sender, object argument)
		{
			return false;
		}
		public virtual void ComponentActivated(object component)
		{
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						//warden.Newobj(Cerebrum.Windows.Forms.Specialized.Concepts.OrdinalAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
						warden.Newobj(Cerebrum.Windows.Forms.Specialized.Concepts.AttachedCommandsAttribute(this.DomainContext), Cerebrum.Runtime.KernelObjectKindId.Warden);
					}
					break;
				}
			}
		}

	}
}
