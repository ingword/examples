// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.Reflection
{
	/// <summary>
	/// Summary description for UiBandItemDescriptor.
	/// </summary>
	public class UiBandItemDescriptor : Cerebrum.Integrator.GenericComponent
	{
		public UiBandItemDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string DisplayName
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Specialized.Concepts.DisplayNameAttribute));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.DisplayNameAttribute, value);
			}
		}

		public int MergeOrder
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.MergeOrderAttribute(this.DomainContext));
				if(o==null || Convert.IsDBNull(o))
				{
					return int.MaxValue;
				}
				else
				{
					return Convert.ToInt32(o);
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.MergeOrderAttribute(this.DomainContext), value);
			}
		}
	}
}


