// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// Summary description for CreateDatabase.
	/// </summary>
	internal class CreateDatabaseForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnAccept;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.NumericUpDown numClusterFrameSize;
		private System.Windows.Forms.CheckBox chkVersioning;
		private System.Windows.Forms.TextBox txtDomainName;
		private System.Windows.Forms.Label lblCaption;
		private System.Windows.Forms.Label lblClusterSize;
		private System.Windows.Forms.Label lblDomainName;
		private System.Windows.Forms.Panel pnlCaption;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Panel pnlClusterSize;
		private System.Windows.Forms.Panel pnlVersioning;
		private System.Windows.Forms.Panel pnlDomainName;

		private CreateDatabase m_Controller;
		public CreateDatabaseForm(CreateDatabase controller)
		{
			m_Controller = controller;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			m_Controller.ClusterFrameSize = (int)numClusterFrameSize.Value;
			m_Controller.ClusterFileName = string.Empty;
			m_Controller.Versioning = chkVersioning.Checked;
			m_Controller.DomainName = txtDomainName.Text;

			if((m_Controller.m_CreateOptions & CreateDatabaseOptions.ClusterSize) != 0)
			{
				pnlClusterSize.Visible = true;
			}

			if((m_Controller.m_CreateOptions & CreateDatabaseOptions.Versioning) != 0)
			{
				pnlVersioning.Visible = true;
			}

			if((m_Controller.m_CreateOptions & CreateDatabaseOptions.DomainName) != 0)
			{
				pnlDomainName.Visible = true;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(CreateDatabaseForm));
			this.numClusterFrameSize = new System.Windows.Forms.NumericUpDown();
			this.btnAccept = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblCaption = new System.Windows.Forms.Label();
			this.lblClusterSize = new System.Windows.Forms.Label();
			this.chkVersioning = new System.Windows.Forms.CheckBox();
			this.pnlClusterSize = new System.Windows.Forms.Panel();
			this.pnlVersioning = new System.Windows.Forms.Panel();
			this.pnlDomainName = new System.Windows.Forms.Panel();
			this.txtDomainName = new System.Windows.Forms.TextBox();
			this.lblDomainName = new System.Windows.Forms.Label();
			this.pnlCaption = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.numClusterFrameSize)).BeginInit();
			this.pnlClusterSize.SuspendLayout();
			this.pnlVersioning.SuspendLayout();
			this.pnlDomainName.SuspendLayout();
			this.pnlCaption.SuspendLayout();
			this.SuspendLayout();
			// 
			// numClusterFrameSize
			// 
			this.numClusterFrameSize.AccessibleDescription = resources.GetString("numClusterFrameSize.AccessibleDescription");
			this.numClusterFrameSize.AccessibleName = resources.GetString("numClusterFrameSize.AccessibleName");
			this.numClusterFrameSize.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("numClusterFrameSize.Anchor")));
			this.numClusterFrameSize.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("numClusterFrameSize.Dock")));
			this.numClusterFrameSize.Enabled = ((bool)(resources.GetObject("numClusterFrameSize.Enabled")));
			this.numClusterFrameSize.Font = ((System.Drawing.Font)(resources.GetObject("numClusterFrameSize.Font")));
			this.numClusterFrameSize.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("numClusterFrameSize.ImeMode")));
			this.numClusterFrameSize.Location = ((System.Drawing.Point)(resources.GetObject("numClusterFrameSize.Location")));
			this.numClusterFrameSize.Maximum = new System.Decimal(new int[] {
																				128,
																				0,
																				0,
																				0});
			this.numClusterFrameSize.Minimum = new System.Decimal(new int[] {
																				4,
																				0,
																				0,
																				0});
			this.numClusterFrameSize.Name = "numClusterFrameSize";
			this.numClusterFrameSize.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("numClusterFrameSize.RightToLeft")));
			this.numClusterFrameSize.Size = ((System.Drawing.Size)(resources.GetObject("numClusterFrameSize.Size")));
			this.numClusterFrameSize.TabIndex = ((int)(resources.GetObject("numClusterFrameSize.TabIndex")));
			this.numClusterFrameSize.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("numClusterFrameSize.TextAlign")));
			this.numClusterFrameSize.ThousandsSeparator = ((bool)(resources.GetObject("numClusterFrameSize.ThousandsSeparator")));
			this.numClusterFrameSize.UpDownAlign = ((System.Windows.Forms.LeftRightAlignment)(resources.GetObject("numClusterFrameSize.UpDownAlign")));
			this.numClusterFrameSize.Value = new System.Decimal(new int[] {
																			  4,
																			  0,
																			  0,
																			  0});
			this.numClusterFrameSize.Visible = ((bool)(resources.GetObject("numClusterFrameSize.Visible")));
			// 
			// btnAccept
			// 
			this.btnAccept.AccessibleDescription = resources.GetString("btnAccept.AccessibleDescription");
			this.btnAccept.AccessibleName = resources.GetString("btnAccept.AccessibleName");
			this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnAccept.Anchor")));
			this.btnAccept.BackColor = System.Drawing.Color.Turquoise;
			this.btnAccept.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.BackgroundImage")));
			this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnAccept.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnAccept.Dock")));
			this.btnAccept.Enabled = ((bool)(resources.GetObject("btnAccept.Enabled")));
			this.btnAccept.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnAccept.FlatStyle")));
			this.btnAccept.Font = ((System.Drawing.Font)(resources.GetObject("btnAccept.Font")));
			this.btnAccept.Image = ((System.Drawing.Image)(resources.GetObject("btnAccept.Image")));
			this.btnAccept.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAccept.ImageAlign")));
			this.btnAccept.ImageIndex = ((int)(resources.GetObject("btnAccept.ImageIndex")));
			this.btnAccept.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnAccept.ImeMode")));
			this.btnAccept.Location = ((System.Drawing.Point)(resources.GetObject("btnAccept.Location")));
			this.btnAccept.Name = "btnAccept";
			this.btnAccept.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnAccept.RightToLeft")));
			this.btnAccept.Size = ((System.Drawing.Size)(resources.GetObject("btnAccept.Size")));
			this.btnAccept.TabIndex = ((int)(resources.GetObject("btnAccept.TabIndex")));
			this.btnAccept.Text = resources.GetString("btnAccept.Text");
			this.btnAccept.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnAccept.TextAlign")));
			this.btnAccept.Visible = ((bool)(resources.GetObject("btnAccept.Visible")));
			this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			// 
			// lblCaption
			// 
			this.lblCaption.AccessibleDescription = resources.GetString("lblCaption.AccessibleDescription");
			this.lblCaption.AccessibleName = resources.GetString("lblCaption.AccessibleName");
			this.lblCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblCaption.Anchor")));
			this.lblCaption.AutoSize = ((bool)(resources.GetObject("lblCaption.AutoSize")));
			this.lblCaption.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblCaption.Dock")));
			this.lblCaption.Enabled = ((bool)(resources.GetObject("lblCaption.Enabled")));
			this.lblCaption.Font = ((System.Drawing.Font)(resources.GetObject("lblCaption.Font")));
			this.lblCaption.Image = ((System.Drawing.Image)(resources.GetObject("lblCaption.Image")));
			this.lblCaption.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCaption.ImageAlign")));
			this.lblCaption.ImageIndex = ((int)(resources.GetObject("lblCaption.ImageIndex")));
			this.lblCaption.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblCaption.ImeMode")));
			this.lblCaption.Location = ((System.Drawing.Point)(resources.GetObject("lblCaption.Location")));
			this.lblCaption.Name = "lblCaption";
			this.lblCaption.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblCaption.RightToLeft")));
			this.lblCaption.Size = ((System.Drawing.Size)(resources.GetObject("lblCaption.Size")));
			this.lblCaption.TabIndex = ((int)(resources.GetObject("lblCaption.TabIndex")));
			this.lblCaption.Text = resources.GetString("lblCaption.Text");
			this.lblCaption.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCaption.TextAlign")));
			this.lblCaption.Visible = ((bool)(resources.GetObject("lblCaption.Visible")));
			// 
			// lblClusterSize
			// 
			this.lblClusterSize.AccessibleDescription = resources.GetString("lblClusterSize.AccessibleDescription");
			this.lblClusterSize.AccessibleName = resources.GetString("lblClusterSize.AccessibleName");
			this.lblClusterSize.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblClusterSize.Anchor")));
			this.lblClusterSize.AutoSize = ((bool)(resources.GetObject("lblClusterSize.AutoSize")));
			this.lblClusterSize.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblClusterSize.Dock")));
			this.lblClusterSize.Enabled = ((bool)(resources.GetObject("lblClusterSize.Enabled")));
			this.lblClusterSize.Font = ((System.Drawing.Font)(resources.GetObject("lblClusterSize.Font")));
			this.lblClusterSize.Image = ((System.Drawing.Image)(resources.GetObject("lblClusterSize.Image")));
			this.lblClusterSize.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblClusterSize.ImageAlign")));
			this.lblClusterSize.ImageIndex = ((int)(resources.GetObject("lblClusterSize.ImageIndex")));
			this.lblClusterSize.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblClusterSize.ImeMode")));
			this.lblClusterSize.Location = ((System.Drawing.Point)(resources.GetObject("lblClusterSize.Location")));
			this.lblClusterSize.Name = "lblClusterSize";
			this.lblClusterSize.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblClusterSize.RightToLeft")));
			this.lblClusterSize.Size = ((System.Drawing.Size)(resources.GetObject("lblClusterSize.Size")));
			this.lblClusterSize.TabIndex = ((int)(resources.GetObject("lblClusterSize.TabIndex")));
			this.lblClusterSize.Text = resources.GetString("lblClusterSize.Text");
			this.lblClusterSize.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblClusterSize.TextAlign")));
			this.lblClusterSize.Visible = ((bool)(resources.GetObject("lblClusterSize.Visible")));
			// 
			// chkVersioning
			// 
			this.chkVersioning.AccessibleDescription = resources.GetString("chkVersioning.AccessibleDescription");
			this.chkVersioning.AccessibleName = resources.GetString("chkVersioning.AccessibleName");
			this.chkVersioning.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("chkVersioning.Anchor")));
			this.chkVersioning.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("chkVersioning.Appearance")));
			this.chkVersioning.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkVersioning.BackgroundImage")));
			this.chkVersioning.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("chkVersioning.CheckAlign")));
			this.chkVersioning.Checked = true;
			this.chkVersioning.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkVersioning.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("chkVersioning.Dock")));
			this.chkVersioning.Enabled = ((bool)(resources.GetObject("chkVersioning.Enabled")));
			this.chkVersioning.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("chkVersioning.FlatStyle")));
			this.chkVersioning.Font = ((System.Drawing.Font)(resources.GetObject("chkVersioning.Font")));
			this.chkVersioning.Image = ((System.Drawing.Image)(resources.GetObject("chkVersioning.Image")));
			this.chkVersioning.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("chkVersioning.ImageAlign")));
			this.chkVersioning.ImageIndex = ((int)(resources.GetObject("chkVersioning.ImageIndex")));
			this.chkVersioning.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("chkVersioning.ImeMode")));
			this.chkVersioning.Location = ((System.Drawing.Point)(resources.GetObject("chkVersioning.Location")));
			this.chkVersioning.Name = "chkVersioning";
			this.chkVersioning.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("chkVersioning.RightToLeft")));
			this.chkVersioning.Size = ((System.Drawing.Size)(resources.GetObject("chkVersioning.Size")));
			this.chkVersioning.TabIndex = ((int)(resources.GetObject("chkVersioning.TabIndex")));
			this.chkVersioning.Text = resources.GetString("chkVersioning.Text");
			this.chkVersioning.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("chkVersioning.TextAlign")));
			this.chkVersioning.Visible = ((bool)(resources.GetObject("chkVersioning.Visible")));
			// 
			// pnlClusterSize
			// 
			this.pnlClusterSize.AccessibleDescription = resources.GetString("pnlClusterSize.AccessibleDescription");
			this.pnlClusterSize.AccessibleName = resources.GetString("pnlClusterSize.AccessibleName");
			this.pnlClusterSize.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pnlClusterSize.Anchor")));
			this.pnlClusterSize.AutoScroll = ((bool)(resources.GetObject("pnlClusterSize.AutoScroll")));
			this.pnlClusterSize.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("pnlClusterSize.AutoScrollMargin")));
			this.pnlClusterSize.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("pnlClusterSize.AutoScrollMinSize")));
			this.pnlClusterSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlClusterSize.BackgroundImage")));
			this.pnlClusterSize.Controls.Add(this.lblClusterSize);
			this.pnlClusterSize.Controls.Add(this.numClusterFrameSize);
			this.pnlClusterSize.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pnlClusterSize.Dock")));
			this.pnlClusterSize.Enabled = ((bool)(resources.GetObject("pnlClusterSize.Enabled")));
			this.pnlClusterSize.Font = ((System.Drawing.Font)(resources.GetObject("pnlClusterSize.Font")));
			this.pnlClusterSize.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pnlClusterSize.ImeMode")));
			this.pnlClusterSize.Location = ((System.Drawing.Point)(resources.GetObject("pnlClusterSize.Location")));
			this.pnlClusterSize.Name = "pnlClusterSize";
			this.pnlClusterSize.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pnlClusterSize.RightToLeft")));
			this.pnlClusterSize.Size = ((System.Drawing.Size)(resources.GetObject("pnlClusterSize.Size")));
			this.pnlClusterSize.TabIndex = ((int)(resources.GetObject("pnlClusterSize.TabIndex")));
			this.pnlClusterSize.Text = resources.GetString("pnlClusterSize.Text");
			this.pnlClusterSize.Visible = ((bool)(resources.GetObject("pnlClusterSize.Visible")));
			// 
			// pnlVersioning
			// 
			this.pnlVersioning.AccessibleDescription = resources.GetString("pnlVersioning.AccessibleDescription");
			this.pnlVersioning.AccessibleName = resources.GetString("pnlVersioning.AccessibleName");
			this.pnlVersioning.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pnlVersioning.Anchor")));
			this.pnlVersioning.AutoScroll = ((bool)(resources.GetObject("pnlVersioning.AutoScroll")));
			this.pnlVersioning.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("pnlVersioning.AutoScrollMargin")));
			this.pnlVersioning.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("pnlVersioning.AutoScrollMinSize")));
			this.pnlVersioning.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlVersioning.BackgroundImage")));
			this.pnlVersioning.Controls.Add(this.chkVersioning);
			this.pnlVersioning.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pnlVersioning.Dock")));
			this.pnlVersioning.Enabled = ((bool)(resources.GetObject("pnlVersioning.Enabled")));
			this.pnlVersioning.Font = ((System.Drawing.Font)(resources.GetObject("pnlVersioning.Font")));
			this.pnlVersioning.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pnlVersioning.ImeMode")));
			this.pnlVersioning.Location = ((System.Drawing.Point)(resources.GetObject("pnlVersioning.Location")));
			this.pnlVersioning.Name = "pnlVersioning";
			this.pnlVersioning.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pnlVersioning.RightToLeft")));
			this.pnlVersioning.Size = ((System.Drawing.Size)(resources.GetObject("pnlVersioning.Size")));
			this.pnlVersioning.TabIndex = ((int)(resources.GetObject("pnlVersioning.TabIndex")));
			this.pnlVersioning.Text = resources.GetString("pnlVersioning.Text");
			this.pnlVersioning.Visible = ((bool)(resources.GetObject("pnlVersioning.Visible")));
			// 
			// pnlDomainName
			// 
			this.pnlDomainName.AccessibleDescription = resources.GetString("pnlDomainName.AccessibleDescription");
			this.pnlDomainName.AccessibleName = resources.GetString("pnlDomainName.AccessibleName");
			this.pnlDomainName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pnlDomainName.Anchor")));
			this.pnlDomainName.AutoScroll = ((bool)(resources.GetObject("pnlDomainName.AutoScroll")));
			this.pnlDomainName.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("pnlDomainName.AutoScrollMargin")));
			this.pnlDomainName.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("pnlDomainName.AutoScrollMinSize")));
			this.pnlDomainName.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlDomainName.BackgroundImage")));
			this.pnlDomainName.Controls.Add(this.txtDomainName);
			this.pnlDomainName.Controls.Add(this.lblDomainName);
			this.pnlDomainName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pnlDomainName.Dock")));
			this.pnlDomainName.Enabled = ((bool)(resources.GetObject("pnlDomainName.Enabled")));
			this.pnlDomainName.Font = ((System.Drawing.Font)(resources.GetObject("pnlDomainName.Font")));
			this.pnlDomainName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pnlDomainName.ImeMode")));
			this.pnlDomainName.Location = ((System.Drawing.Point)(resources.GetObject("pnlDomainName.Location")));
			this.pnlDomainName.Name = "pnlDomainName";
			this.pnlDomainName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pnlDomainName.RightToLeft")));
			this.pnlDomainName.Size = ((System.Drawing.Size)(resources.GetObject("pnlDomainName.Size")));
			this.pnlDomainName.TabIndex = ((int)(resources.GetObject("pnlDomainName.TabIndex")));
			this.pnlDomainName.Text = resources.GetString("pnlDomainName.Text");
			this.pnlDomainName.Visible = ((bool)(resources.GetObject("pnlDomainName.Visible")));
			// 
			// txtDomainName
			// 
			this.txtDomainName.AccessibleDescription = resources.GetString("txtDomainName.AccessibleDescription");
			this.txtDomainName.AccessibleName = resources.GetString("txtDomainName.AccessibleName");
			this.txtDomainName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtDomainName.Anchor")));
			this.txtDomainName.AutoSize = ((bool)(resources.GetObject("txtDomainName.AutoSize")));
			this.txtDomainName.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtDomainName.BackgroundImage")));
			this.txtDomainName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtDomainName.Dock")));
			this.txtDomainName.Enabled = ((bool)(resources.GetObject("txtDomainName.Enabled")));
			this.txtDomainName.Font = ((System.Drawing.Font)(resources.GetObject("txtDomainName.Font")));
			this.txtDomainName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtDomainName.ImeMode")));
			this.txtDomainName.Location = ((System.Drawing.Point)(resources.GetObject("txtDomainName.Location")));
			this.txtDomainName.MaxLength = ((int)(resources.GetObject("txtDomainName.MaxLength")));
			this.txtDomainName.Multiline = ((bool)(resources.GetObject("txtDomainName.Multiline")));
			this.txtDomainName.Name = "txtDomainName";
			this.txtDomainName.PasswordChar = ((char)(resources.GetObject("txtDomainName.PasswordChar")));
			this.txtDomainName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtDomainName.RightToLeft")));
			this.txtDomainName.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtDomainName.ScrollBars")));
			this.txtDomainName.Size = ((System.Drawing.Size)(resources.GetObject("txtDomainName.Size")));
			this.txtDomainName.TabIndex = ((int)(resources.GetObject("txtDomainName.TabIndex")));
			this.txtDomainName.Text = resources.GetString("txtDomainName.Text");
			this.txtDomainName.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtDomainName.TextAlign")));
			this.txtDomainName.Visible = ((bool)(resources.GetObject("txtDomainName.Visible")));
			this.txtDomainName.WordWrap = ((bool)(resources.GetObject("txtDomainName.WordWrap")));
			// 
			// lblDomainName
			// 
			this.lblDomainName.AccessibleDescription = resources.GetString("lblDomainName.AccessibleDescription");
			this.lblDomainName.AccessibleName = resources.GetString("lblDomainName.AccessibleName");
			this.lblDomainName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblDomainName.Anchor")));
			this.lblDomainName.AutoSize = ((bool)(resources.GetObject("lblDomainName.AutoSize")));
			this.lblDomainName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblDomainName.Dock")));
			this.lblDomainName.Enabled = ((bool)(resources.GetObject("lblDomainName.Enabled")));
			this.lblDomainName.Font = ((System.Drawing.Font)(resources.GetObject("lblDomainName.Font")));
			this.lblDomainName.Image = ((System.Drawing.Image)(resources.GetObject("lblDomainName.Image")));
			this.lblDomainName.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDomainName.ImageAlign")));
			this.lblDomainName.ImageIndex = ((int)(resources.GetObject("lblDomainName.ImageIndex")));
			this.lblDomainName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblDomainName.ImeMode")));
			this.lblDomainName.Location = ((System.Drawing.Point)(resources.GetObject("lblDomainName.Location")));
			this.lblDomainName.Name = "lblDomainName";
			this.lblDomainName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblDomainName.RightToLeft")));
			this.lblDomainName.Size = ((System.Drawing.Size)(resources.GetObject("lblDomainName.Size")));
			this.lblDomainName.TabIndex = ((int)(resources.GetObject("lblDomainName.TabIndex")));
			this.lblDomainName.Text = resources.GetString("lblDomainName.Text");
			this.lblDomainName.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblDomainName.TextAlign")));
			this.lblDomainName.Visible = ((bool)(resources.GetObject("lblDomainName.Visible")));
			// 
			// pnlCaption
			// 
			this.pnlCaption.AccessibleDescription = resources.GetString("pnlCaption.AccessibleDescription");
			this.pnlCaption.AccessibleName = resources.GetString("pnlCaption.AccessibleName");
			this.pnlCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pnlCaption.Anchor")));
			this.pnlCaption.AutoScroll = ((bool)(resources.GetObject("pnlCaption.AutoScroll")));
			this.pnlCaption.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("pnlCaption.AutoScrollMargin")));
			this.pnlCaption.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("pnlCaption.AutoScrollMinSize")));
			this.pnlCaption.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlCaption.BackgroundImage")));
			this.pnlCaption.Controls.Add(this.lblCaption);
			this.pnlCaption.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pnlCaption.Dock")));
			this.pnlCaption.Enabled = ((bool)(resources.GetObject("pnlCaption.Enabled")));
			this.pnlCaption.Font = ((System.Drawing.Font)(resources.GetObject("pnlCaption.Font")));
			this.pnlCaption.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pnlCaption.ImeMode")));
			this.pnlCaption.Location = ((System.Drawing.Point)(resources.GetObject("pnlCaption.Location")));
			this.pnlCaption.Name = "pnlCaption";
			this.pnlCaption.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pnlCaption.RightToLeft")));
			this.pnlCaption.Size = ((System.Drawing.Size)(resources.GetObject("pnlCaption.Size")));
			this.pnlCaption.TabIndex = ((int)(resources.GetObject("pnlCaption.TabIndex")));
			this.pnlCaption.Text = resources.GetString("pnlCaption.Text");
			this.pnlCaption.Visible = ((bool)(resources.GetObject("pnlCaption.Visible")));
			// 
			// CreateDatabaseForm
			// 
			this.AcceptButton = this.btnAccept;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.CancelButton = this.btnCancel;
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.pnlDomainName);
			this.Controls.Add(this.pnlVersioning);
			this.Controls.Add(this.pnlClusterSize);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnAccept);
			this.Controls.Add(this.pnlCaption);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "CreateDatabaseForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Load += new System.EventHandler(this.CreateDatabase_Load);
			((System.ComponentModel.ISupportInitialize)(this.numClusterFrameSize)).EndInit();
			this.pnlClusterSize.ResumeLayout(false);
			this.pnlVersioning.ResumeLayout(false);
			this.pnlDomainName.ResumeLayout(false);
			this.pnlCaption.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		private void btnAccept_Click(object sender, System.EventArgs e)
		{
			if((m_Controller.m_CreateOptions & CreateDatabaseOptions.ClusterSize) != 0)
			{
				m_Controller.ClusterFrameSize = (int)numClusterFrameSize.Value;
			}
			if((m_Controller.m_CreateOptions & CreateDatabaseOptions.Versioning) != 0)
			{
				m_Controller.Versioning = chkVersioning.Checked;
			}
			if((m_Controller.m_CreateOptions & CreateDatabaseOptions.DomainName) != 0)
			{
				m_Controller.DomainName = txtDomainName.Text;
			}
		}

		private void CreateDatabase_Load(object sender, System.EventArgs e)
		{
			numClusterFrameSize.Value = m_Controller.ClusterFrameSize;
			chkVersioning.Checked = m_Controller.Versioning;
			txtDomainName.Text = m_Controller.DomainName;
		}
	}
	[Flags]
	public enum CreateDatabaseOptions
	{
		None = 0,
		ClusterSize = 1,
		Versioning = 2,
		DomainName = 4,
		All = 7
	}
}
