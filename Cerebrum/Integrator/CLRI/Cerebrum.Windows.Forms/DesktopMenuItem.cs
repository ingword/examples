// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using Cerebrum.Integrator;
using System.Drawing;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// DesktopMenuItem ��������������� ����� ����������� � �����������
	/// ���������������� System.Windows.Forms.MenuItem �������� CommandId � MenuId
	/// CommandId ������������ ��� ����������� �������, ������� ���� ������������
	/// �� ������� ������� �������� ����;
	/// MenuId ������������ ��� ����������� ������� ������� �������� ���� � ������ ����.
	/// </summary>
	public class DesktopMenuItem : System.Windows.Forms.MenuItem, IUiControl, IReleasable
	{
		private const int TextPositionOffset = 16;

		public DesktopMenuItem():base()
		{
			//
			// TODO: Add constructor logic here
			//
			this.Click += new EventHandler(DesktopMenuItem_Click);
		}
		protected Cerebrum.ObjectHandle m_MenuItemId;

		protected System.Drawing.Image m_NormalEnterImage;
		[System.ComponentModel.DefaultValue(null)]
		public System.Drawing.Image NormalEnterImage
		{
			get
			{
				return m_NormalEnterImage;
			}
			set
			{
				m_NormalEnterImage = value;
			}
		}

		protected System.Drawing.Image m_NormalLeaveImage;
		[System.ComponentModel.DefaultValue(null)]
		public System.Drawing.Image NormalLeaveImage
		{
			get
			{
				return m_NormalLeaveImage;
			}
			set
			{
				m_NormalLeaveImage = value;
			}
		}


		protected override void OnMeasureItem(System.Windows.Forms.MeasureItemEventArgs e)
		{
			SizeF textSize;
			textSize = e.Graphics.MeasureString(this.Text, ((DesktopMainMenu)this.GetMainMenu()).Font);
			int imageWidth = 0;
			int imageHeight = 0;
			if(m_NormalEnterImage!=null)
			{
				imageWidth = m_NormalEnterImage.Width;
				imageHeight = m_NormalEnterImage.Height;
			}
			if(m_NormalLeaveImage!=null)
			{
				if(imageWidth < m_NormalLeaveImage.Width)
				{
					imageWidth = m_NormalLeaveImage.Width;
				}
				if(imageHeight < m_NormalLeaveImage.Height)
				{
					imageHeight = m_NormalLeaveImage.Height;
				}
			}
			if(imageWidth < TextPositionOffset)
			{
				imageWidth = TextPositionOffset;
			}

			e.ItemWidth = (int)textSize.Width + imageWidth;

			if (imageHeight > textSize.Height)
			{
				e.ItemHeight = 4 + imageHeight;
			}
			else
			{
				e.ItemHeight = 4 + (int)textSize.Height;
			}
		}

		protected override void OnDrawItem(System.Windows.Forms.DrawItemEventArgs e)
		{
			bool selected;
			selected = (e.State & System.Windows.Forms.DrawItemState.Selected)!=0;

			Color textcolor;
			if(this.Enabled)
			{
				textcolor = SystemColors.MenuText;
			}
			else
			{
				textcolor = SystemColors.GrayText;
			}
			StringFormat textformat = new StringFormat();
			textformat.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.Show;
			int nIconW = 0;

			System.Drawing.Image image;
			if(selected)
			{
				image = this.m_NormalEnterImage;
				if(image == null)
				{
					image = this.m_NormalLeaveImage;
				}
			}
			else
			{
				image = this.m_NormalLeaveImage;
				if(image == null)
				{
					image = this.m_NormalEnterImage;
				}
			}

			if (image != null)
			{
				nIconW = image.Width;
			}

			if(nIconW < TextPositionOffset)
			{
				nIconW = TextPositionOffset;
			}

			RectangleF R = new RectangleF(e.Bounds.Left+ nIconW, e.Bounds.Top, e.Bounds.Width, e.Bounds.Height);
			e.Graphics.FillRectangle(SystemBrushes.Menu, e.Bounds.Left, e.Bounds.Top, e.Bounds.Width, e.Bounds.Height);
			if (selected)
			{
				e.DrawBackground();
				textcolor = SystemColors.HighlightText;
			}

			else
			{
				e.Graphics.FillRectangle(SystemBrushes.Menu, R);
			}

			int dy = 0;
			if (image != null)
			{
				dy = (e.Bounds.Height - image.Height) / 2;
				if(this.Enabled)
				{
					e.Graphics.DrawImageUnscaled(image, e.Bounds.Left, e.Bounds.Top + dy);
				}
				else
				{
					System.Windows.Forms.ControlPaint.DrawImageDisabled(e.Graphics, image, e.Bounds.Left, e.Bounds.Top + dy, SystemColors.Control);
				}
			}


			System.Drawing.Font font = ((DesktopMainMenu)this.GetMainMenu()).Font;
			SizeF textSize;
			textSize = e.Graphics.MeasureString(this.Text, font, e.Bounds.Width, textformat);

			if(image!=null)
			{
				R.Y += dy + (image.Height - textSize.Height) / 2;
			}
			else
			{
				R.Y += (e.Bounds.Height - textSize.Height) / 2;
			}

			e.Graphics.DrawString(this.Text, font, new SolidBrush(textcolor), R, textformat);
		}

		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		public Cerebrum.ObjectHandle MenuItemId
		{
			get
			{
				return this.m_MenuItemId;
			}
			set
			{
				this.m_MenuItemId = value;
			}
		}
		protected Cerebrum.ObjectHandle m_CommandId;

		#region IUiCommandItem Members

		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		public Cerebrum.ObjectHandle CommandId
		{
			get
			{
				return m_CommandId;
			}
			set
			{
				m_CommandId = value;
			}
		}

		bool IEnabledProperty.Enabled
		{
			get
			{
				// TODO:  Add DesktopMenuItem.Enabled getter implementation
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		public event System.ComponentModel.CancelEventHandler InvokeCommand;

		public void AttachCommand(Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor command)
		{
			this.NormalLeaveImage = command.GetNormalLeaveImage();
			this.NormalEnterImage = command.GetNormalEnterImage();
		}

		#endregion

		private void DesktopMenuItem_Click(object sender, EventArgs e)
		{
			if(InvokeCommand!=null)
			{
				InvokeCommand(sender, new System.ComponentModel.CancelEventArgs(false));
			}
		}
		#region IReleasable Members

		public bool Release(bool cancel)
		{
			System.ComponentModel.CancelEventArgs e = new System.ComponentModel.CancelEventArgs(cancel);
			System.ComponentModel.CancelEventHandler handler = (System.ComponentModel.CancelEventHandler)Events[Cerebrum.Specialized.EventKeys.ReleasingEvent];
			if (handler != null)
				handler(this, e);
			return e.Cancel;
		}

		/*public void Dispose(object sender, EventArgs e)
		{
			if(this.Parent!=null) this.Parent.MenuItems.Remove(this);
			OnDisposing(e);
			this.Dispose();
		}*/
		protected virtual void OnDisposing(System.EventArgs e)
		{
			System.EventHandler handler = (System.EventHandler)Events[Cerebrum.Specialized.EventKeys.DisposingEvent];
			if (handler != null)
				handler(this, e);
		}

		public event System.ComponentModel.CancelEventHandler Releasing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
		}

		public event System.EventHandler Disposing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
		}

		#endregion
	}
}
