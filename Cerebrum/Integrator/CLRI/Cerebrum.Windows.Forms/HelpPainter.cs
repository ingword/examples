// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

//#define MY_AUTOSIZE

using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Cerebrum.Windows.Forms
{
	public interface IPaintHelper
	{
		void Paint(object sender, System.Windows.Forms.PaintEventArgs pe);		
	}
	[DefaultProperty("BorderColor")]	
	public class PaintHelper: System.ComponentModel.Component, IPaintHelper
	{
		protected System.Drawing.Color m_DisabledBackColor = System.Drawing.SystemColors.Control; //System.Drawing.Color.Gray;
		protected System.Drawing.Color m_EnterBackColor = System.Drawing.Color.Empty;
		protected System.Drawing.Color m_LeaveBackColor = System.Drawing.Color.Empty;
		protected System.Drawing.Color m_CheckBackColor = System.Drawing.Color.Empty;
		protected System.Drawing.Color m_PressBackColor = System.Drawing.Color.Empty;
		protected System.Drawing.Color m_BorderColor = System.Drawing.Color.Empty;
		//protected System.Drawing.Color m_BackColor;
		protected System.Byte m_Alpha;

		#region default source
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PaintHelper(System.ComponentModel.IContainer container)
		{
			container.Add(this);
			InitializeComponent();			
		}

		public PaintHelper()
		{
			InitializeComponent();
			//m_BackColor=m_LeaveBackColor;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		#endregion

		#region property
		[Category("ExtenderBackColors")]			
		public System.Drawing.Color DisabledBackColor
		{
			get
			{
				return m_DisabledBackColor;
			}
			set
			{
				m_DisabledBackColor=value;
			}
		}

		[Category("ExtenderBackColors")]			
		public System.Drawing.Color LeaveBackColor
		{
			get
			{
				return m_LeaveBackColor;
			}
			set
			{
				m_LeaveBackColor=value;
			}
		}

		[Category("ExtenderBackColors")]	
		public System.Drawing.Color CheckBackColor
		{
			get
			{
				return m_CheckBackColor;
			}
			set
			{
				m_CheckBackColor=value;
			}
		}

		[Category("ExtenderBackColors")]	
		public System.Drawing.Color EnterBackColor
		{
			get
			{
				return m_EnterBackColor;
			}
			set
			{
				m_EnterBackColor=value;
			}
		}

		[Category("ExtenderBackColors")]	
		public System.Drawing.Color PressBackColor
		{
			get
			{
				return m_PressBackColor;
			}
			set
			{
				m_PressBackColor=value;
			}
		}
		[Category("MyColors")]
		public System.Drawing.Color BorderColor
		{
			get
			{
				return m_BorderColor;
			}
			set
			{
				m_BorderColor=value;
			}
		}
		[Category("MyColors")]			
		public System.Byte Alpha
		{
			get
			{
				return m_Alpha;
			}
			set
			{
				m_Alpha=value;
			}
		}
//		[Browsable(false)]	
//		public System.Drawing.Color BackColor
//		{
//			get
//			{
//				return m_BackColor;
//			}
//			set
//			{
//				m_BackColor=value;
//			}
//		}
		#endregion

		virtual public void Paint(object sender, System.Windows.Forms.PaintEventArgs pe)
		{
		}
	}


	public class PaintHelperRect : PaintHelper
	{

		#region default

		private System.ComponentModel.Container components = null;

		public PaintHelperRect()
		{
			InitializeComponent();
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		public PaintHelperRect(System.ComponentModel.IContainer container) : this()
		{
			container.Add(this);
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion

		override public void Paint(object sender, System.Windows.Forms.PaintEventArgs pe)
		{	
					
			Cerebrum.Windows.Forms.ImageButton tabButton = sender as Cerebrum.Windows.Forms.ImageButton;
			if (tabButton==null) return;

			string text=tabButton.Text;
			System.Drawing.Color BackColor;
			System.Drawing.Color textColor = tabButton.Enabled?tabButton.ForeColor:System.Drawing.Color.DimGray;					                    		
			System.Drawing.Image image = tabButton.Image;			
			System.Drawing.SizeF sizeText = pe.Graphics.MeasureString(text, tabButton.Font);
			System.Drawing.SizeF sizeInterval = pe.Graphics.MeasureString("A", tabButton.Font);
			
			
			#region highlight bar
			
			if (tabButton.Entered) 
			{				
				BackColor=System.Drawing.Color.FromArgb(Alpha, EnterBackColor);
				if (tabButton.Pressed) 	BackColor=System.Drawing.Color.FromArgb(Alpha,PressBackColor);					
			}
			else
			{
				if (tabButton.Checked) 	
					BackColor=System.Drawing.Color.FromArgb(Alpha,CheckBackColor);
				else 
					BackColor=(LeaveBackColor);
			}
			bool disableImage = false;
			if (!tabButton.Enabled) 
			{
				BackColor=System.Drawing.Color.FromArgb(Alpha, DisabledBackColor);
				if((tabButton.Checked && tabButton.CheckedDisabledImage==null) || (!tabButton.Checked && tabButton.NormalDisabledImage==null))
				{
					disableImage = true;
				}
			}
						
			pe.Graphics.FillRectangle(new System.Drawing.SolidBrush(BackColor), 0, 0, tabButton.Size.Width, tabButton.Size.Height);

			if (tabButton.Entered||tabButton.Checked)
			{
				System.Drawing.Pen Pen;
				Pen = new System.Drawing.Pen(new System.Drawing.SolidBrush(BorderColor), 0);			 			
				Pen.StartCap = System.Drawing.Drawing2D.LineCap.Square;
				Pen.EndCap	 = System.Drawing.Drawing2D.LineCap.Square;
				System.Drawing.Point[] Points = new System.Drawing.Point[]
						{
							new System.Drawing.Point(0, 0),
							new System.Drawing.Point(tabButton.Size.Width- 1, 0),
							new System.Drawing.Point(tabButton.Size.Width- 1, tabButton.Size.Height- 1),
							new System.Drawing.Point(0, tabButton.Size.Height- 1),
							new System.Drawing.Point(0, 0)
						};
				pe.Graphics.DrawLines(Pen, Points);
			}
			
			#endregion			
			
			#region Paint Text and Image	
			
			if (tabButton.OwnerDraw || true)
			{
				/*if (image != null)						
					{
				#region 

	
						int dy=(tabButton.Size.Height- image.Height)/ 2;
						if(text.Length >0)
						{						
							System.Drawing.Point dPoint= new System.Drawing.Point((int)sizeInterval.Width,dy);

							pe.Graphics.DrawImageUnscaled(image,dPoint );							
				
							pe.Graphics.DrawString(text, tabButton.Font, new System.Drawing.SolidBrush(textColor), 
								new System.Drawing.RectangleF( 
								dPoint.X+image.Width+sizeInterval.Width,(tabButton.Size.Height- sizeText.Height)/ 2, image.Width+3*sizeInterval.Width+sizeText.Width, tabButton.Size.Height-(tabButton.Size.Height- sizeText.Height)/ 2));
						}
						else
						{	
							pe.Graphics.DrawImageUnscaled(image, (tabButton.Size.Width- image.Width)/ 2,dy);											
						}
				#endregion
					}	*/
				if (image != null)						
				{
					#region 	
					int dy=(tabButton.Size.Height- image.Height)/ 2;
					if(text.Length >0)
					{		
						#region Paint Icon+Text
						System.Drawing.Point dPoint= new System.Drawing.Point((int)sizeInterval.Width,dy);

						if(disableImage)
						{
							System.Windows.Forms.ControlPaint.DrawImageDisabled(pe.Graphics,image,dPoint.X,dPoint.Y,BackColor);
						}
						else
						{
							pe.Graphics.DrawImageUnscaled(image,dPoint );
						}
				
						pe.Graphics.DrawString(text, tabButton.Font, new System.Drawing.SolidBrush(textColor), 
							new System.Drawing.RectangleF( 
							dPoint.X+image.Width+sizeInterval.Width,(tabButton.Size.Height- sizeText.Height)/ 2, image.Width+3*sizeInterval.Width+sizeText.Width, tabButton.Size.Height-(tabButton.Size.Height- sizeText.Height)/ 2));
						#endregion
					}
					else
					{
						#region Paint Icon							
						int x=(int)(tabButton.Size.Width- image.Width)/ 2;
						int y=dy;	
						if (tabButton.Enabled)
						{
							#region Enable=true
							if (tabButton.Entered)
							{
								System.Windows.Forms.ControlPaint.DrawImageDisabled(pe.Graphics,image,x + 1,y + 1,BackColor);
								x-=1;y-=1;
							}								
							if(tabButton.Pressed)
							{
								x+=1;y+=1;
							}
							pe.Graphics.DrawImage(image, x,y);	
							#endregion			
						}
						else
						{
							if(disableImage)
							{
								System.Windows.Forms.ControlPaint.DrawImageDisabled(pe.Graphics,image,x,y,BackColor);
							}
							else
							{
								pe.Graphics.DrawImage(image, x, y);	
							}
						}
						#endregion
					}
					#endregion
				}
				else
				{	
					#region 
					if(text.Length >0)
					{	
						System.Drawing.Font font=new Font(tabButton.Font,System.Drawing.FontStyle.Bold);
											
						System.Drawing.Point pp= new System.Drawing.Point((int)(int)(tabButton.Width-sizeText.Width)/2,(int)(tabButton.Height-sizeText.Height)/2);
						pe.Graphics.DrawString(text, tabButton.Font, new System.Drawing.SolidBrush(textColor), 
							new System.Drawing.RectangleF( 
							pp.X,pp.Y,pp.X+sizeText.Width , pp.Y+sizeText.Height));											
					}
					else
					{						
					}
					#endregion
				}
			}
			else	
				if(text.Length >0)
			{	
				System.Drawing.Font font=new Font(tabButton.Font,System.Drawing.FontStyle.Bold);
											
				System.Drawing.Point pp= new System.Drawing.Point((int)(int)(tabButton.Width-sizeText.Width)/2,(int)(tabButton.Height-sizeText.Height)/2);
				pe.Graphics.DrawString(text, font, new System.Drawing.SolidBrush(textColor), 
					new System.Drawing.RectangleF( 
					pp.X,pp.Y,pp.X+sizeText.Width , pp.Y+sizeText.Height));											
			}
			
			#endregion			
		}
	}
	


	
	/*
	[DefaultProperty("BorderColor")]	
	public class PaintHelperRect : System.ComponentModel.Component, IPaintHelper
	{

		private System.Drawing.Color m_EnterBackColor;
		private System.Drawing.Color m_LeaveBackColor;
		private System.Drawing.Color m_CheckBackColor;
		private System.Drawing.Color m_PressBackColor;
		private System.Drawing.Color m_BorderColor;
		private System.Drawing.Color m_BackColor;
		private System.Byte m_Alpha;

	#region default 
		private System.ComponentModel.Container components = null;

		public PaintHelperRect()
		{
			InitializeComponent();
		}

	#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
	#endregion

		public PaintHelperRect(System.ComponentModel.IContainer container) : this()
		{
			container.Add(this);
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

	#endregion

	#region property
		[Category("ExtenderBackColors")]	
		[Description("This is leave BackColor")]
		public System.Drawing.Color LeaveBackColor
		{
			get
			{
				return m_LeaveBackColor;
			}
			set
			{
				m_LeaveBackColor=value;
			}
		}

		[Category("ExtenderBackColors")]	
		public System.Drawing.Color CheckBackColor
		{
			get
			{
				return m_CheckBackColor;
			}
			set
			{
				m_CheckBackColor=value;
			}
		}

		[Category("ExtenderBackColors")]	
		public System.Drawing.Color EnterBackColor
		{
			get
			{
				return m_EnterBackColor;
			}
			set
			{
				m_EnterBackColor=value;
			}
		}

		[Category("ExtenderBackColors")]	
		public System.Drawing.Color PressBackColor
		{
			get
			{
				return m_PressBackColor;
			}
			set
			{
				m_PressBackColor=value;
			}
		}
		[Category("MyColors")]
		public System.Drawing.Color BorderColor
		{
			get
			{
				return m_BorderColor;
			}
			set
			{
				m_BorderColor=value;
			}
		}

		[Category("MyColors")]	
		[Description("This is Alpha for BackColor")]
		public System.Byte Alpha
		{
			get
			{
				return m_Alpha;
			}
			set
			{
				m_Alpha=value;
			}
		}
	#endregion

		public void Paint(object sender, System.Windows.Forms.PaintEventArgs pe)
		{	
					
			Cerebrum.Windows.Forms.ImageButton tabButton = sender as Cerebrum.Windows.Forms.ImageButton;
			if (tabButton==null) return;

			string text=tabButton.Text;		
			System.Drawing.Color textColor = tabButton.Enabled?tabButton.ForeColor:System.Drawing.Color.Gray;		//???????				                    		
			System.Drawing.Image image = tabButton.Image;			
			System.Drawing.SizeF sizeText = pe.Graphics.MeasureString(text, tabButton.Font);
			System.Drawing.SizeF sizeInterval = pe.Graphics.MeasureString("A", tabButton.Font);

			
			
	#region highlight bar
			
			if (tabButton.Entered) 
			{				
				m_BackColor=System.Drawing.Color.FromArgb(Alpha, m_EnterBackColor);//m_EnterBackColor;
				if (tabButton.Pressed) 	m_BackColor=System.Drawing.Color.FromArgb(Alpha,m_PressBackColor);					
			}
			else
			{
				if (tabButton.Checked) 	
					m_BackColor=System.Drawing.Color.FromArgb(Alpha,m_CheckBackColor);
				else 
					m_BackColor=(m_LeaveBackColor);
			}
			//if (!tabButton.Enabled) m_BackColor=System.Drawing.Color.FromArgb(Alpha,System.Drawing.Color.Gray);						
						
			pe.Graphics.FillRectangle(new System.Drawing.SolidBrush(m_BackColor), 0, 0, tabButton.Size.Width, tabButton.Size.Height);

			if (tabButton.Entered||tabButton.Checked)
			{
				System.Drawing.Pen Pen;
				Pen = new System.Drawing.Pen(new System.Drawing.SolidBrush(BorderColor), 0);			 			
				Pen.StartCap = System.Drawing.Drawing2D.LineCap.Square;
				Pen.EndCap	 = System.Drawing.Drawing2D.LineCap.Square;
				System.Drawing.Point[] Points = new System.Drawing.Point[]
							{
								new System.Drawing.Point(0, 0),
								new System.Drawing.Point(tabButton.Size.Width- 1, 0),
								new System.Drawing.Point(tabButton.Size.Width- 1, tabButton.Size.Height- 1),
								new System.Drawing.Point(0, tabButton.Size.Height- 1),
								new System.Drawing.Point(0, 0)
							};
				pe.Graphics.DrawLines(Pen, Points);
			}
			
	#endregion
			
			
	#region Paint Text and Image	
			if (tabButton.OwnerDraw)
			{
				if (image != null)						
				{
	#region 	
					int dy=(tabButton.Size.Height- image.Height)/ 2;
					if(text.Length >0)
					{		
	#region Paint Icon+Text
						System.Drawing.Point dPoint= new System.Drawing.Point((int)sizeInterval.Width,dy);

						pe.Graphics.DrawImageUnscaled(image,dPoint );							
				
						pe.Graphics.DrawString(text, tabButton.Font, new System.Drawing.SolidBrush(textColor), 
							new System.Drawing.RectangleF( 
							dPoint.X+image.Width+sizeInterval.Width,(tabButton.Size.Height- sizeText.Height)/ 2, image.Width+3*sizeInterval.Width+sizeText.Width, tabButton.Size.Height-(tabButton.Size.Height- sizeText.Height)/ 2));
	#endregion
					}
					else
					{
	#region Paint Icon							
						int x=(int)(tabButton.Size.Width- image.Width)/ 2;
						int y=dy;	
						if (tabButton.Enabled)
						{
	#region Enable=true
							if (tabButton.Entered)
							{
								System.Windows.Forms.ControlPaint.DrawImageDisabled(pe.Graphics,image,x + 1,y + 1,m_BackColor);//m_BackColor);
								x-=1;y-=1;
							}								
							if(tabButton.Pressed)
							{
								x+=1;y+=1;
							}
							pe.Graphics.DrawImage(image, x,y);	
	#endregion			
						}
						else 		
							System.Windows.Forms.ControlPaint.DrawImageDisabled(pe.Graphics,image,x,y,m_BackColor);
	#endregion
					}
	#endregion
				}	
				else 				
				{	
	#region Paint Text
					if(text.Length >0)
					{	
						tabButton.Height=(int)sizeText.Height+6;	
						System.Drawing.Point pp= new System.Drawing.Point((int)sizeInterval.Width,0);
						pe.Graphics.DrawString(text, tabButton.Font, new System.Drawing.SolidBrush(textColor), 
							new System.Drawing.RectangleF( 
							pp.X,3, 2*sizeInterval.Width+sizeText.Width, sizeText.Height));		
									
					}
					else
					{						
					}
	#endregion
				}
			}
	#endregion			
		}

		/-*
		public void Paint(object sender, System.Windows.Forms.PaintEventArgs pe)
		{						
			Cerebrum.Windows.Forms.ImageButton tabButton = sender as Cerebrum.Windows.Forms.ImageButton;
			if(tabButton == null) return;

			string text=tabButton.Text;		
			System.Drawing.Color textColor = tabButton.Enabled?tabButton.ForeColor:System.Drawing.Color.Gray;		//???????				                    		
			System.Drawing.Image image = tabButton.Image;			
			System.Drawing.SizeF sizeText = pe.Graphics.MeasureString(text, tabButton.Font);
			System.Drawing.SizeF sizeInterval = pe.Graphics.MeasureString("A", tabButton.Font);

	#region highlight bar
			
			if (tabButton.Entered) 
			{				
				m_BackColor=System.Drawing.Color.FromArgb(Alpha, m_EnterBackColor);//m_EnterBackColor;
				if (tabButton.Pressed) 	m_BackColor=System.Drawing.Color.FromArgb(Alpha,m_PressBackColor);					
			}
			else
			{
				if (tabButton.Checked) 	
					m_BackColor=System.Drawing.Color.FromArgb(Alpha,m_CheckBackColor);
				else 
					m_BackColor=(m_LeaveBackColor);
			}
			if (!tabButton.Enabled) m_BackColor=System.Drawing.Color.FromArgb(Alpha,System.Drawing.Color.Gray);						
						
			pe.Graphics.FillRectangle(new System.Drawing.SolidBrush(m_BackColor), 0, 0, tabButton.Size.Width, tabButton.Size.Height);

			if (tabButton.Entered||tabButton.Checked)
			{
				System.Drawing.Pen Pen;
				Pen = new System.Drawing.Pen(new System.Drawing.SolidBrush(BorderColor), 0);			 			
				Pen.StartCap = System.Drawing.Drawing2D.LineCap.Square;
				Pen.EndCap	 = System.Drawing.Drawing2D.LineCap.Square;
				System.Drawing.Point[] Points = new System.Drawing.Point[]
							{
								new System.Drawing.Point(0, 0),
								new System.Drawing.Point(tabButton.Size.Width- 1, 0),
								new System.Drawing.Point(tabButton.Size.Width- 1, tabButton.Size.Height- 1),
								new System.Drawing.Point(0, tabButton.Size.Height- 1),
								new System.Drawing.Point(0, 0)
							};
				pe.Graphics.DrawLines(Pen, Points);
			}
			
	#endregion
			
	#region Paint Text and Image	
			if (tabButton.OwnerDraw)
			{
				if (image != null)						
				{
	#region 


					int dy=(tabButton.Size.Height- image.Height)/ 2;
					if(text.Length >0)
					{						
						System.Drawing.Point dPoint= new System.Drawing.Point((int)sizeInterval.Width,dy);

						pe.Graphics.DrawImageUnscaled(image,dPoint );							
				
						pe.Graphics.DrawString(text, tabButton.Font, new System.Drawing.SolidBrush(textColor), 
							new System.Drawing.RectangleF( 
							dPoint.X+image.Width+sizeInterval.Width,(tabButton.Size.Height- sizeText.Height)/ 2, image.Width+3*sizeInterval.Width+sizeText.Width, tabButton.Size.Height-(tabButton.Size.Height- sizeText.Height)/ 2));
					}
					else
					{	
						pe.Graphics.DrawImageUnscaled(image, (tabButton.Size.Width- image.Width)/ 2,dy);											
					}
	#endregion
				}	
				else 				
				{	
	#region 
					if(text.Length >0)
					{	
						tabButton.Height=(int)sizeText.Height+6;	
						System.Drawing.Point pp= new System.Drawing.Point((int)sizeInterval.Width,0);
						pe.Graphics.DrawString(text, tabButton.Font, new System.Drawing.SolidBrush(textColor), 
							new System.Drawing.RectangleF( 
							pp.X,3, 2*sizeInterval.Width+sizeText.Width, sizeText.Height));		
									
					}
					else
					{						
					}
	#endregion
				}
			}
	#endregion			
		}
		*-/
	}

*/
}
