// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// IntegratorBarButton ��������������� ����� ����������� � �����������
	/// ���������������� System.Windows.Forms.ToolBarButton �������� CommandId
	/// CommandId ������������ ��� ����������� �������, ������� ���� ������������
	/// �� ������� ������ ������ ������ ������������;
	/// </summary>
	public class DesktopBandButton : Cerebrum.Windows.Forms.TabButton, Cerebrum.Windows.Forms.IUiControl, Cerebrum.IReleasable
	{
		public DesktopBandButton():base()
		{
			//
			// TODO: Add constructor logic here
			//
			this.Click += new EventHandler(IntegratorBandButton_Click);
		}
		protected Cerebrum.ObjectHandle m_CommandId;

		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		public Cerebrum.ObjectHandle CommandId
		{
			get
			{
				return m_CommandId;
			}
			set
			{
				m_CommandId = value;
			}
		}

		public event System.ComponentModel.CancelEventHandler InvokeCommand;

		public void AttachCommand(Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor command)
		{
			this.NormalLeaveImage = command.GetNormalLeaveImage();
			this.NormalEnterImage = command.GetNormalEnterImage();
		}


		private void IntegratorBandButton_Click(object sender, EventArgs e)
		{
			if(this.InvokeCommand!=null)
			{
				this.InvokeCommand(sender, new System.ComponentModel.CancelEventArgs(false));
			}
		}
		#region IReleasable Members

		public bool Release(bool cancel)
		{
			System.ComponentModel.CancelEventArgs e = new System.ComponentModel.CancelEventArgs(cancel);
			System.ComponentModel.CancelEventHandler handler = (System.ComponentModel.CancelEventHandler)Events[Cerebrum.Specialized.EventKeys.ReleasingEvent];
			if (handler != null)
				handler(this, e);
			return e.Cancel;
		}

		/*public void Dispose(object sender, EventArgs e)
		{
			if(this.Parent!=null) this.Parent.Controls.Remove(this);
			OnDisposing(e);
			this.Dispose();
		}*/
		/// <summary>
		/// ���������� ��� ������������� ������� ���������, �����������
		/// ������ �����������. �� ������ ������� ������ ����� ���������
		/// ������ ������������ �������� ��� ������������ ��������. ������
		/// ������� ������� ����������. ������� �������� ������������.
		/// </summary>
		/// <param name="sender">�������� �������</param>
		/// <param name="e">��������� �������</param>
		protected virtual void OnDisposing(System.EventArgs e)
		{
			System.EventHandler handler = (System.EventHandler)Events[Cerebrum.Specialized.EventKeys.DisposingEvent];
			if (handler != null)
				handler(this, e);
		}

		public event System.ComponentModel.CancelEventHandler Releasing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
		}

		/// <summary>
		/// ���������� ������� Clearing ���������� IReleasable
		/// </summary>
		event System.EventHandler Cerebrum.IDisposable.Disposing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
		}

		#endregion

	
		protected int m_MergeOrder;
		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		public int MergeOrder
		{
			get
			{
				return this.m_MergeOrder;
			}
			set
			{
				this.m_MergeOrder = value;
			}
		}
	}

}