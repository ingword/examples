// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// DropDownStringItem ��������������� ����� ����������� ������������� �
	/// ������� ��������� ������ � ������� � Combobox ������ ����� ��������, ������������ ��� �����.
	/// </summary>
	public class DropDownStringItem
	{
		public DropDownStringItem()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public DropDownStringItem(string caption, object value)
		{
			//
			// TODO: Add constructor logic here
			//
			Caption = caption;
			Value = value;
		}
		public string Caption;
		public object Value;

		public override string ToString()
		{
			return Caption;
		}

		public override bool Equals(object obj)
		{
			DropDownStringItem item = obj as DropDownStringItem;
			if(item!=null) return this.Value.Equals(item.Value);
			return this.Value.Equals (obj);
		}

		public override int GetHashCode()
		{
			return this.Value.GetHashCode ();
		}


		public static DropDownStringItem FindItem(System.Collections.IList list, object value)
		{
			foreach(object obj in list)
			{
				DropDownStringItem item = obj as DropDownStringItem;
				if(item!=null && item.Value == value) return item;
			}
			return null;
		}
	}
}
