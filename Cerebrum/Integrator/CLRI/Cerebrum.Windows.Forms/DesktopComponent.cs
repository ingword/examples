// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// ������� ����� ��� ���� ���������� �����������. 
	/// ��������� ������� ��������� ����� � ���������
	/// Cerebrum.Integrator.IComponent
	/// ��� ������ ����, ������� ������ ���� ���������� ����������
	/// ��� ���������, ������ ������������� �� ������� ������ ��� �� ������
	/// ������������ ������ �����. 
	/// </summary>
	public class DesktopComponent : System.Windows.Forms.Form, Cerebrum.IDisposable, Cerebrum.IComponent, Cerebrum.IReleasable
	{
		protected Cerebrum.IConnector m_Connector;
		
		#region IComponent Members


		void Cerebrum.IComponent.SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			SetConnector(direction, connector);
		}
		protected virtual void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			m_Connector = connector;
			//connector.Disposing += new EventHandler(connector_Disposing);
			if(m_Connector==null)
			{
				this.MdiParent = null;
			}
			else
			{
				Cerebrum.Windows.Forms.Application app = this.DomainContext.Application as Cerebrum.Windows.Forms.Application;
				System.Windows.Forms.Form primaryWindow = app==null?null:app.PrimaryWindow;
				if(primaryWindow!=null)
				{
					if(this.m_Disposition==DesktopComponentDisposition.Child)
					{
						this.MdiParent = primaryWindow;
					}
					if(this.m_Disposition==DesktopComponentDisposition.Owned)
					{
						primaryWindow.AddOwnedForm(this);
					}
				}
			}
		}
		#endregion


		/// <summary>
		/// ����������� ������
		/// </summary>
		public DesktopComponent(DesktopComponentDisposition disposition):base()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			m_Destroying = false;
			m_Disposed = false;
			this.m_Disposition = disposition;
		}
	
		public DesktopComponent()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.m_Disposition = DesktopComponentDisposition.Alone;
			m_Destroying = false;
			m_Disposed = false;
		}

		/// <summary>
		/// ���������� ������� ��������� �������� true ���� ������ ��������
		/// ��������� � �������� �����������. ��������������� �� ������
		/// ������ Clear ���������� IReleasable ��� ������ Close ������� ������
		/// </summary>
		protected bool m_Destroying;
		protected bool m_Disposed;

		
		public Cerebrum.Integrator.DomainContext DomainContext
		{
			get
			{
				return Cerebrum.Integrator.DomainContext.FromConnector(this.m_Connector);
			}
		}

		/*
		/// <summary>
		/// ���������� ������ �� ������ Application �� ���������.
		/// </summary>
		public virtual Cerebrum.Windows.Forms.Application Application
		{
			get
			{
				if(m_DomainContext!=null)
				{
					return this.m_DomainContext.Application;
				}
				else
				{
					return null;
				}
			}
		}
		*/

		/*
		/// <summary>
		/// ���������� ������ ��� ������ �� �������� ��� ������� ������ ���������
		/// </summary>
		protected string m_ComponentId;
		/// <summary>
		/// �������� ���������� ��� ������ �� �������� ��� ������� ������ ���������
		/// </summary>
		public virtual string ComponentId
		{
			get
			{
				return m_ComponentId;
			}
			set
			{
				m_ComponentId = value;
			}
		}
		
		/// <summary>
		/// ���������� ������ ����, ����������� ������� �����������
		/// ������� ���������� ����� ���� ������������ �������� � 
		/// ������� ���������
		/// </summary>
		protected bool m_Singleton;
		/// <summary>
		/// �������� ���������� ����, ����������� ������� �����������
		/// ������� ���������� ����� ���� ������������ �������� � 
		/// ������� ���������
		/// </summary>
		public virtual bool Singleton
		{
			get
			{
				return m_Singleton;
			}
			set
			{
				m_Singleton = value;
			}
		}
		*/

		/// <summary>
		/// ����� ����������� ����������� ����� �������� ������ Windows.Forms,
		/// ���������� ��������������� ����� ��������� �����.
		/// �������� ����������� ����� �������� ������ � ����� OnClearing
		/// </summary>
		/// <param name="e">��������� �������</param>
		protected override void OnClosed(System.EventArgs e)
		{
			m_Destroying = true;
			if(!m_Disposed)
			{
				m_Disposed = true;
				this.OnDisposing(this, e);
			}
			base.OnClosed(e);
		}

		/// <summary>
		/// ����� ����������� ����������� ����� �������� ������ Windows.Forms,
		/// ���������� ��� ����������� ����������� ��������� �����.
		/// �������� ����������� ����� �������� ������ � ����� OnRelaxing.
		/// ������ OnRelaxing ���������� ������� �������� ���������� m_Destroying
		/// </summary>
		/// <param name="e">��������� ������� e.Cancel ��������������� � true 
		/// ��� ������ �������� ����� � ������ �����</param>
		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			base.OnClosing(e);
			this.OnReleasing(this, e, m_Destroying);
		}
		/// <summary>
		/// ����������� ����� ��������� ����������� ������ ����������
		/// ��������� ������ ������ Relax. ����� ��������� ������ ���������
		/// �������� ��������� destroying. � ������ ���� ���� �������� = false
		/// ����� ��������� ����� ����� �������� ������ ���������� � ��������
		/// ������������ � ����������� ���������� �������� ��� ������ ��������.
		/// � ������ ������ ���� ���������� e.Cancel = true
		/// � ������ ���� �������� destroying = true ��������� �� �����
		/// ����������� �� ���������� ���������������� ������ �����������, �
		/// ������ ���������� ����� ������ ��������� ����� ������������
		/// ������ �������� ������.
		/// </summary>
		/// <param name="sender">�������� �������</param>
		/// <param name="e">��������� ������� e.Cancel ��������������� � true 
		/// ��� ������ �������� ����� � ������ �����</param>
		/// <param name="destroying">��������������� � true ���� ����� ������������ � 
		/// �������� ����������� �������� ����������</param>
		protected virtual void OnReleasing(object sender, System.ComponentModel.CancelEventArgs e, bool destroying)
		{
			if(!destroying)
			{
				System.ComponentModel.CancelEventHandler handler = (System.ComponentModel.CancelEventHandler)Events[Cerebrum.Specialized.EventKeys.ReleasingEvent];
				if (handler != null)
					handler(this, e);
			}
		}
		/// <summary>
		/// ���������� ��� ������������� ������� ���������, �����������
		/// ������ �����������. �� ������ ������� ������ ����� ���������
		/// ������ ������������ �������� ��� ������������ ��������. ������
		/// ������� ������� ����������. ������� �������� ������������.
		/// </summary>
		/// <param name="sender">�������� �������</param>
		/// <param name="e">��������� �������</param>
		protected virtual void OnDisposing(object sender, System.EventArgs e)
		{
			System.EventHandler handler = (System.EventHandler)Events[Cerebrum.Specialized.EventKeys.DisposingEvent];
			if (handler != null)
				handler(this, e);
		}

		/// <summary>
		/// ������������� ���������� m_Destroying � �������� true
		/// � �������� ����������� ����� Close �������� ������ ���
		/// �������� �����
		/// </summary>
		public virtual new void Close()
		{
			m_Destroying = true;
			base.Close();
		}

		protected override void Dispose(bool disposing)
		{
			m_Destroying = true;
			if(!m_Disposed && disposing)
			{
				m_Disposed = true;
				this.OnDisposing(this, System.EventArgs.Empty);
			}
			base.Dispose (disposing);
		}

		/// <summary>
		/// ���������� ������ Relax ���������� IReleasable
		/// </summary>
		/// <param name="sender">�������� �������</param>
		/// <param name="e">��������� ������� e.Cancel ��������������� � true 
		/// ��� ������ �������� ����� � ������ �����</param>
		public virtual bool Release(bool cancel)
		{
			System.ComponentModel.CancelEventArgs e = new CancelEventArgs(cancel);
			this.OnClosing(e);
			return e.Cancel;
		}


		/// <summary>
		/// ���������� ������� Clearing ���������� IReleasable
		/// </summary>
		event System.EventHandler Cerebrum.IDisposable.Disposing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.DisposingEvent, value);	}
		}


		/// <summary>
		/// ���������� ������� Relaxing ���������� IReleasable
		/// </summary>
		public event System.ComponentModel.CancelEventHandler Releasing
		{
			add	{	Events.AddHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
			remove	{	Events.RemoveHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
		}

		/// <summary>
		/// �������� ��� ����������� ��������� �� ������ ��������� � �������� �����������.
		/// ���������� �������� ���������� m_Destroying
		/// </summary>
		public bool IsCleaning
		{
			get
			{
				return m_Destroying;
			}
		}

		
		protected DesktopComponentDisposition m_Disposition;
		public DesktopComponentDisposition Disposition
		{
			get
			{
				return m_Disposition;
			}
			set
			{
				if(m_Disposition != value)
				{
					Cerebrum.Windows.Forms.Application app = this.DomainContext.Application as Cerebrum.Windows.Forms.Application;
					System.Windows.Forms.Form primaryWindow = app==null?null:app.PrimaryWindow;

					if(primaryWindow!=null)
					{
						if(this.MdiParent!=null && (value!=DesktopComponentDisposition.Child || this.MdiParent!=primaryWindow))
						{
							this.MdiParent = null;
						}
						if(this.Owner!=null && (value!=DesktopComponentDisposition.Owned || this.Owner!=primaryWindow))
						{
							if(this.Owner!=null)this.Owner.RemoveOwnedForm(this);
						}
					}
					else
					{
						this.MdiParent = null;
						if(this.Owner!=null)this.Owner.RemoveOwnedForm(this);
					}

					m_Disposition = value;

					if(primaryWindow!=null)
					{
						Cerebrum.Windows.Forms.IPrimaryWindow pw = primaryWindow as Cerebrum.Windows.Forms.IPrimaryWindow;
						switch(this.m_Disposition)
						{
							case DesktopComponentDisposition.Child:
								if(this.MdiParent!=primaryWindow)
								{
									this.MdiParent = primaryWindow;
								}
								break;
							case DesktopComponentDisposition.Owned:
								if(this.Owner!=primaryWindow)
								{
									primaryWindow.AddOwnedForm(this);
									if(pw!=null)
									{
										pw.ActiveComponent = this;
									}
								}
								break;
							case DesktopComponentDisposition.Alone:
								if(pw!=null)
								{
									pw.ActiveComponent = this;
								}
								break;
						}
					}
				}
			}
		}

		public virtual new void Show()
		{
			System.Windows.Forms.DialogResult dr;
			Cerebrum.Windows.Forms.Application app = this.m_Connector==null?null:this.DomainContext.Application as Cerebrum.Windows.Forms.Application;
			System.Windows.Forms.Form primaryWindow = app==null?null:app.PrimaryWindow;
			switch(this.m_Disposition)
			{
				case DesktopComponentDisposition.Child:
					if(this.WindowState == System.Windows.Forms.FormWindowState.Minimized)
					{
						if(primaryWindow!=null && primaryWindow.ActiveMdiChild!=null)
						{
							this.WindowState = primaryWindow.ActiveMdiChild.WindowState;
						}
					}
					base.Show();
					base.Focus();
					return;
				case DesktopComponentDisposition.Modal:
					bool oldEnabled = app==null?true:app.Enabled;
					if(app!=null)
					{
						app.Enabled = false;
					}
					dr = base.ShowDialog(primaryWindow);
					if(app!=null)
					{
						app.Enabled = oldEnabled;
					}
					return;
				default:
					base.Show();
					base.Focus();
					return;
			}
		}
		public new System.Windows.Forms.DialogResult ShowDialog()
		{
			Cerebrum.Windows.Forms.Application app = this.DomainContext.Application as Cerebrum.Windows.Forms.Application;
			System.Windows.Forms.Form primaryWindow = app==null?null:app.PrimaryWindow;
			System.Windows.Forms.DialogResult dr;
			bool oldEnabled = app==null?true:app.Enabled;
			if(app!=null)
			{
				app.Enabled = false;
			}
			if(primaryWindow!=null)
			{
				dr = base.ShowDialog(primaryWindow);
			}
			else
			{
				dr = base.ShowDialog();
			}
			if(app!=null)
			{
				app.Enabled = oldEnabled;
			}
			return dr;
		}

		public new System.Windows.Forms.DialogResult ShowDialog(System.Windows.Forms.IWin32Window owner)
		{
			Cerebrum.Windows.Forms.Application app = this.DomainContext.Application as Cerebrum.Windows.Forms.Application;
			System.Windows.Forms.DialogResult dr;
			bool oldEnabled = app==null?true:app.Enabled;
			if(app!=null)
			{
				app.Enabled = false;
			}
			dr = base.ShowDialog(owner);
			if(app!=null)
			{
				app.Enabled = oldEnabled;
			}

			return dr;
		}

		//
//		/// <summary>
//		/// ���������� ������ Execute ���������� IComponent
//		/// </summary>
//		/// <param name="sender">�������� �������</param>
//		/// <param name="e">��������� �������</param>
//		public virtual void Execute(object sender, System.ComponentModel.CancelEventArgs e)
//		{
//			System.ComponentModel.CancelEventHandler handler = (System.ComponentModel.CancelEventHandler)Events[Cerebrum.Windows.Forms.Application.ExecutingEvent];
//			if (handler != null)
//				handler(this, e);
//		}
//
//		public event System.ComponentModel.CancelEventHandler Executing
//		{
//			add	{	Events.AddHandler(Cerebrum.Windows.Forms.Application.ExecutingEvent, value);	}
//			remove	{	Events.RemoveHandler(Cerebrum.Windows.Forms.Application.ExecutingEvent, value);	}
//		}


/*		protected override void WndProc(ref System.Windows.Forms.Message m)
		{
			try
			{
				base.WndProc(ref m);
			}
			catch(System.Exception ex)
			{
				m_DomainContext.Application.AddActivityRow(Cerebrum.Integrator.SystemActivityEvents.ExceptionError, "VisualComponent.WndProc", ex.ToString());
			}
		}*/

	}
	
	public enum DesktopComponentDisposition
	{
		Alone,
		Child, 
		Owned, 
		Modal
	}

}
