// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

namespace Cerebrum.Windows.Forms
{
	/// <summary>
	/// Summary description for DesktopMainMenu.
	/// </summary>
	public class DesktopMainMenu : System.Windows.Forms.MainMenu
	{

		public DesktopMainMenu(System.ComponentModel.IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public DesktopMainMenu()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			/*if( disposing )
			{
			}*/
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion

		private System.Drawing.Font m_Font;
		public System.Drawing.Font Font
		{
			get
			{
				return this.m_Font;
			}
			set
			{
				this.m_Font = value;
			}
		}
	}
}
