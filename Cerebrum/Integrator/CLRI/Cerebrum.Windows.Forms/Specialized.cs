// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.Specialized
{
	/// <summary>
	/// Summary description for Reflection.
	/// </summary>
	public sealed class Concepts
	{
		private Concepts()
		{
			//
			// No constructor logic for static class
			//
		}

		public static Cerebrum.ObjectHandle ParentIdAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, "ParentId");
		}
		public static Cerebrum.ObjectHandle MergeOrderAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, "MergeOrder");
		}
		public static Cerebrum.ObjectHandle MessageIdAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, Cerebrum.Specialized.KnownNames.MessageId);
		}
		public static Cerebrum.ObjectHandle AttachedControlsAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, "AttachedControls");
		}
		public static Cerebrum.ObjectHandle OrdinalAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, Cerebrum.Specialized.KnownNames.Ordinal);
		}
		public static Cerebrum.ObjectHandle AttachedCommandsAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, "AttachedCommands");
		}
		public static Cerebrum.ObjectHandle KeyCodeAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, "KeyCode");
		}
		public static Cerebrum.ObjectHandle WidthAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, "Width");
		}
		public static Cerebrum.ObjectHandle HeightAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, "Height");
		}
		public static Cerebrum.ObjectHandle NormalLeaveImageStreamNameAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, Cerebrum.Windows.Forms.Specialized.KnownNames.NormalLeaveImageStreamName);
		}
		public static Cerebrum.ObjectHandle NormalEnterImageStreamNameAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, Cerebrum.Windows.Forms.Specialized.KnownNames.NormalEnterImageStreamName);
		}
		public static Cerebrum.ObjectHandle NormalLeaveImageBinaryDataAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, Cerebrum.Windows.Forms.Specialized.KnownNames.NormalLeaveImageBinaryData);
		}
		public static Cerebrum.ObjectHandle NormalEnterImageBinaryDataAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", Cerebrum.Specialized.KnownNames.Name, Cerebrum.Windows.Forms.Specialized.KnownNames.NormalEnterImageBinaryData);
		}

		public static Cerebrum.ObjectHandle AbstractCommandId = new Cerebrum.ObjectHandle(-500);
		// �� ������� ��� ����� Help ������������ ��������� � ����� ���� HelpMenu
		public static Cerebrum.ObjectHandle ApplicationHelpCommandId = new Cerebrum.ObjectHandle(-501);
		public static Cerebrum.ObjectHandle ApplicationHideCommandId = new Cerebrum.ObjectHandle(-502);
		public static Cerebrum.ObjectHandle ApplicationExitCommandId = new Cerebrum.ObjectHandle(-503);
		// �� ������� ��� ����� HelpAbout ������������ ��������� � ����� ���� HelpMenu
		public static Cerebrum.ObjectHandle ApplicationHelpAboutCommandId = new Cerebrum.ObjectHandle(-505);

		//public static Cerebrum.ObjectHandle ApplicationLogOutCommandId = new Cerebrum.ObjectHandle(-511);
		//public static Cerebrum.ObjectHandle ApplicationChangePasswordCommandId = new Cerebrum.ObjectHandle(-512);

		// �� ���� �������� ������
		public static Cerebrum.ObjectHandle SystemMenuItemId = new Cerebrum.ObjectHandle(-7);
		public static Cerebrum.ObjectHandle SecurityMenuItemId = new Cerebrum.ObjectHandle(-8);
		public static Cerebrum.ObjectHandle WindowMenuItemId = new Cerebrum.ObjectHandle(-9);
		public static Cerebrum.ObjectHandle HelpMenuItemId = new Cerebrum.ObjectHandle(-10);

		
	}
	public sealed class KnownNames
	{
		private KnownNames()
		{
		}

		public static readonly string NormalLeaveImageStreamName = "NormalLeaveImageStreamName";
		public static readonly string NormalEnterImageStreamName = "NormalEnterImageStreamName";
		public static readonly string NormalLeaveImageBinaryData = "NormalLeaveImageBinaryData";
		public static readonly string NormalEnterImageBinaryData = "NormalEnterImageBinaryData";
	}
}
