// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator
{
	/// <summary>
	/// Summary description for Utilites.
	/// </summary>
	public class Utilites
	{
		private Utilites()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static object ObtainComponent(Cerebrum.IContainer container, Cerebrum.ObjectHandle handle)
		{
			using(Cerebrum.IConnector connector = container.AttachConnector(handle))
			{
				if(connector!=null)
				{
					return connector.Component;
				}
				else
				{
					return null;
				}
			}
		}
		public static void UpdateComponent(Cerebrum.IContainer container, Cerebrum.ObjectHandle handle, object value)
		{
			Cerebrum.IConnector connector = null;
			try
			{
				connector = container.AttachConnector(handle);
				if(connector==null)
				{
					using(Cerebrum.IConnector catr = ((Cerebrum.IConnector)container).Workspace.AttachConnector(handle))
					{
						connector = container.CreateConnector(handle, (catr.Component as Cerebrum.Reflection.IAttributeDescriptor).AttributeTypeIdHandle);
					}
				}
						
				connector.Component = value;
			}
			finally
			{
				if(connector!=null)
				{
					connector.Dispose();
				}
			}
		}

		public static void EngageConnector(Cerebrum.IContainer container, Cerebrum.ObjectHandle plasma)
		{
			Cerebrum.Runtime.NativeWarden warden = container as Cerebrum.Runtime.NativeWarden;
			if(warden==null || !warden.HasMap(plasma))
			{
				using(Cerebrum.IConnector connector = ((Cerebrum.IConnector)container).Workspace.AttachConnector(plasma))
				{
					container.EngageConnector(plasma, connector);
				}
			}
		}

		public static void EngageConnector(Cerebrum.IContainer container, Cerebrum.ObjectHandle plasma, Cerebrum.IConnector connector, bool throwOnError)
		{
			bool shouldDispose = false;
			Cerebrum.Runtime.NativeWarden warden = container as Cerebrum.Runtime.NativeWarden;
			if(warden==null && container is Cerebrum.Runtime.NativeDomain)
			{
				shouldDispose = true;
				warden = (Cerebrum.Runtime.NativeWarden)(container as Cerebrum.Runtime.NativeDomain).GetSector();
			}
			try
			{
				if(warden==null || !warden.HasMap(plasma))
				{
					Cerebrum.ObjectHandle ground = Cerebrum.ObjectHandle.Null;
					if(warden!=null)
					{
						ground = warden.GetMap(plasma);
					}
					if(Cerebrum.ObjectHandle.Null.Equals(ground))
					{
						container.EngageConnector(plasma, connector);
					}
					else
					{
						if(throwOnError)
						{
							if( (!(connector is Cerebrum.IComposite)) || (ground!=(connector as Cerebrum.IComposite).GroundHandle))
							{
								throw new System.Exception("Another instance already engaged");
							}
						}
					}
				}
			}
			finally
			{
				if(shouldDispose && warden!=null)
				{
					warden.Dispose();
				}
			}
		}

		public static object CreateInstance(Cerebrum.IWorkspace workspace, Cerebrum.ObjectHandle typeId, object [] args)
		{
			object instance = null;
			if(((System.IComparable)typeId).CompareTo(Cerebrum.ObjectHandle.Null) > 0)
			{
				using(Cerebrum.IConnector ctype = workspace.AttachConnector(typeId))
				{
					Cerebrum.Reflection.TypeDescriptor tds = (Cerebrum.Reflection.TypeDescriptor)ctype.Component;
					System.Type componentType = tds.ComponentType;
					if(componentType.IsAbstract)
					{
						instance = null;
					}
					else
					{
						if(args==null)
						{
							instance = System.Activator.CreateInstance(componentType);
						}
						else
						{
							instance = System.Activator.CreateInstance(componentType, args);
						}
					}
				}
			}
			return instance;
		}
	}
}
