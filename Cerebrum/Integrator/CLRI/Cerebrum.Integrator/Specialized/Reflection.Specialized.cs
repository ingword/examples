// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Specialized
{
	public sealed class KnownNames
	{
		private KnownNames()
		{
		}

		public static readonly string ObjectHandle = "ObjectHandle";
		public static readonly string PlasmaHandle = "PlasmaHandle";
		public static readonly string TypeIdHandle = "TypeIdHandle";
		public static readonly string KindIdHandle = "KindIdHandle";
		public static readonly string GroundHandle = "GroundHandle";
		public static readonly string Description = "Description";
		public static readonly string Name = "Name";
		public static readonly string DisplayName = "DisplayName";
		public static readonly string Ordinal = "Ordinal";
		public static readonly string MessageId = "MessageId";
	}

	/// <summary>
	/// Summary description for Reflection.
	/// </summary>
	public sealed class Concepts
	{
		private Concepts()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static readonly Cerebrum.ObjectHandle FirstUserHandle = new Cerebrum.ObjectHandle(65536);

		public static readonly Cerebrum.ObjectHandle GroundHandleAttribute = new Cerebrum.ObjectHandle(252);
		public static readonly Cerebrum.ObjectHandle KindIdHandleAttribute = new Cerebrum.ObjectHandle(253);
		public static readonly Cerebrum.ObjectHandle TypeIdHandleAttribute = new Cerebrum.ObjectHandle(254);
		public static readonly Cerebrum.ObjectHandle PlasmaHandleAttribute = new Cerebrum.ObjectHandle(255);
		public static readonly Cerebrum.ObjectHandle NameAttribute = new Cerebrum.ObjectHandle(1000);
		public static readonly Cerebrum.ObjectHandle DescriptionAttribute = new Cerebrum.ObjectHandle(1001);
		public static readonly Cerebrum.ObjectHandle SelectedAttributesAttribute = new Cerebrum.ObjectHandle(1002);
		public static readonly Cerebrum.ObjectHandle SelectedComponentsAttribute = new Cerebrum.ObjectHandle(1003);
		public static readonly Cerebrum.ObjectHandle AttributeDescriptorTypeId = new Cerebrum.ObjectHandle(1004);
		public static readonly Cerebrum.ObjectHandle WardenNodeTypeId = new Cerebrum.ObjectHandle(1005);
		public static readonly Cerebrum.ObjectHandle QualifiedTypeNameAttribute = new Cerebrum.ObjectHandle(1006);
		public static readonly Cerebrum.ObjectHandle DefaultTypeIdAttribute = new Cerebrum.ObjectHandle(1007);
		public static readonly Cerebrum.ObjectHandle TypeDescriptorTypeId = new Cerebrum.ObjectHandle(1008);
		public static readonly Cerebrum.ObjectHandle ObjectHandleTypeId = new Cerebrum.ObjectHandle(1009);
		public static readonly Cerebrum.ObjectHandle SystemStringTypeId = new Cerebrum.ObjectHandle(1010);
		public static readonly Cerebrum.ObjectHandle TableDescriptorTypeId = new Cerebrum.ObjectHandle(1011);

		public static readonly Cerebrum.ObjectHandle AttributesTableId = new Cerebrum.ObjectHandle(1012);
		public static readonly Cerebrum.ObjectHandle TypesTableId = new Cerebrum.ObjectHandle(1013);
		public static readonly Cerebrum.ObjectHandle TablesTableId = new Cerebrum.ObjectHandle(1014);
		public static readonly Cerebrum.ObjectHandle DisplayNameAttribute = new Cerebrum.ObjectHandle(1015);
		public static readonly Cerebrum.ObjectHandle BindingListTypeId = new Cerebrum.ObjectHandle(1016);
		
		public static readonly Cerebrum.ObjectHandle SingleObjectHandleVectorId = new Cerebrum.ObjectHandle(1017);
		
		//public static readonly Cerebrum.ObjectHandle KernelObjectClassTypeId = new Cerebrum.ObjectHandle(1018);
		public static readonly Cerebrum.ObjectHandle KernelObjectKindIdAttribute = new Cerebrum.ObjectHandle(1019);
		
		public static readonly Cerebrum.ObjectHandle MessageDescriptorTypeId = new Cerebrum.ObjectHandle(1020);
		public static readonly Cerebrum.ObjectHandle MessagesTableId = new Cerebrum.ObjectHandle(1021);
		
		public static readonly Cerebrum.ObjectHandle SystemInt16TypeId = new Cerebrum.ObjectHandle(1022);
		public static readonly Cerebrum.ObjectHandle SystemInt32TypeId = new Cerebrum.ObjectHandle(1023);
		public static readonly Cerebrum.ObjectHandle SystemInt64TypeId = new Cerebrum.ObjectHandle(1024);
		
		public static readonly Cerebrum.ObjectHandle DisplayFieldIdAttribute = new Cerebrum.ObjectHandle(1025);
		public static readonly Cerebrum.ObjectHandle DependentFieldIdAttribute = new Cerebrum.ObjectHandle(1026);
		public static readonly Cerebrum.ObjectHandle PrecedentTableIdAttribute = new Cerebrum.ObjectHandle(1027);
		public static readonly Cerebrum.ObjectHandle FieldDescriptorTypeId = new Cerebrum.ObjectHandle(1028);
		public static readonly Cerebrum.ObjectHandle SystemBooleanTypeId = new Cerebrum.ObjectHandle(1029);
		public static readonly Cerebrum.ObjectHandle SystemDateTimeTypeId = new Cerebrum.ObjectHandle(1030);
		public static readonly Cerebrum.ObjectHandle DerivedFlagAttribute = new Cerebrum.ObjectHandle(1031);

		public static readonly Cerebrum.ObjectHandle ScalarNodeTypeId = new Cerebrum.ObjectHandle(1032);
		
		public static readonly Cerebrum.ObjectHandle FieldsTableId = new Cerebrum.ObjectHandle(1033);
		
		public static readonly Cerebrum.ObjectHandle GenericComponentTypeId = new Cerebrum.ObjectHandle(1034);
		public static readonly Cerebrum.ObjectHandle DomainContextTypeId = new Cerebrum.ObjectHandle(1035);
		
		public static readonly Cerebrum.ObjectHandle SystemByteArrayTypeId = new Cerebrum.ObjectHandle(1036);
		
		public static readonly Cerebrum.ObjectHandle ReadOnlyAttribute = new Cerebrum.ObjectHandle(1037);
	}
}
