// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Collections.Specialized
{
	/// <summary>
	/// Summary description for HandleMap.
	/// </summary>
	[Serializable()]
	public class HandleObjectDictionary : System.Collections.IEnumerable, System.Collections.IDictionary
	{
		private object [] m_Array;
		private int m_Count;

		public HandleObjectDictionary()
		{
			this.m_Count = 0;
			this.m_Array = new object [1024];
		}

		/// <summary>
		/// ������������� ��� ���������� ������ �� ��������� ObjectHandle.
		/// </summary>
		public object this[Cerebrum.ObjectHandle ObjectHandle]
		{
			set
			{
				if (value != null)
				{
					int v = ObjectHandle.ToInt32();
					int section1 = v & 0x07FF;
					int section2 = (v >> 11) & 0x07FF;
					int section3 = (v >> 22) & 0x03FF;

					object[] s3;
					object[] s2;

					s3 = (object[])m_Array[section3];
					if (s3 == null)
					{
						s3 = new object[2048];
						m_Array[section3] = s3;
					}

					s2 = (object[])s3[section2];
					if (s2 == null)
					{
						s2 = new object[2048];
						s3[section2] = s2;
					}

					if(s2[section1]==null)
					{
						this.m_Count ++;
					}
					s2[section1] = value;
				}
				else
				{
					this.Remove(ObjectHandle);
				}
			}
			get
			{
				int v = ObjectHandle.ToInt32();
				int section1 = v & 0x07FF;
				int section2 = (v >> 11) & 0x07FF;
				int section3 = (v >> 22) & 0x03FF;

				object[] s3;
				object[] s2;

				if ((s3 = (object[])m_Array[section3]) == null)
				{
					return null;
				}
				else if ((s2 = (object[])s3[section2]) == null)
				{
					return null;
				}
				return s2[section1];
			}
		}

		/// <summary>
		/// �������� ������������ �������.
		/// </summary>
		/// <param name="ObjectHandle">����� ��� ������ �������.</param>
		public void Remove(Cerebrum.ObjectHandle ObjectHandle)
		{
			int v = ObjectHandle.ToInt32();
			int section1 = v & 0x07FF;
			int section2 = (v >> 11) & 0x07FF;
			int section3 = (v >> 22) & 0x03FF;

			object[] s3;
			object[] s2;

			if ((s3 = (object[])m_Array[section3]) == null)
			{
				return;
			}
			else if ((s2 = (object[])s3[section2]) == null)
			{
				return;
			}

			if (s2[section1] != null)
			{
				this.m_Count --;
				s2[section1] = null;
				if (this.CalcChildrenCount(s2) < 1)
				{
					s3[section2] = null;
					if (this.CalcChildrenCount(s3) < 1)
					{
						m_Array[section3] = null;
					}
				}
			}
		}

		private int CalcChildrenCount(object[] array)
		{
			int nCount = 0;

			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] != null)
				{
					nCount++;
				}
			}
			return nCount;
		}

        private class Enumerator : System.Collections.IDictionaryEnumerator
        {
            private HandleObjectDictionary m_Dictionary;
            private long m_CurrentIndex;
            private object m_Value;

            public Enumerator(HandleObjectDictionary dictionary)
            { 
                m_Dictionary = dictionary;
                this.Reset();
            }
            

			#region IEnumerator Members

            public object Current
            {
                get { return this.Entry; }
            }

            public bool MoveNext()
            {
                m_CurrentIndex++;
                while (m_CurrentIndex < 0x100000000)
                {
                    int section3 = (int)((m_CurrentIndex >> 22) & 0x03FF);
                    int section2 = (int)((m_CurrentIndex >> 11) & 0x07FF);
                    int section1 = (int)(m_CurrentIndex & 0x07FF);

                    object[] s3;
                    object[] s2;

                    if ((s3 = (object[])m_Dictionary.m_Array[section3]) == null)
                    {
                        m_CurrentIndex += ((long)1) << 22;
                        continue;
                    }
                    else if ((s2 = (object[])s3[section2]) == null)
                    {
                        m_CurrentIndex += ((long)1) << 11;
                        continue;
                    }
                    else if (s2[section1] == null)
                    {
                        m_CurrentIndex += ((long)1);
                        continue;
                    }
                    else
                    {
                        m_Value = s2[section1];
                        return true;
                    }
                }
                return false;
            }

            public void Reset()
            {
                m_CurrentIndex = -1;
                m_Value = null;
            }

            #endregion

            #region IDictionaryEnumerator Members

            public System.Collections.DictionaryEntry Entry
            {
                get { return new System.Collections.DictionaryEntry(new Cerebrum.ObjectHandle((int)m_CurrentIndex), this.m_Value); }
            }

            public object Key
            {
                get { return new Cerebrum.ObjectHandle((int)m_CurrentIndex); }
            }

            public object Value
            {
                get { return this.m_Value; }
            }

            #endregion
        }


		#region IEnumerable Members

        public System.Collections.IEnumerator GetEnumerator()
        {
            return new HandleObjectDictionary.Enumerator(this);
        }

        #endregion

		#region IDictionary Members

		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		public bool ContainsKey(Cerebrum.ObjectHandle key)
		{
			return this[key]!=null;
		}

		public void Add(Cerebrum.ObjectHandle key, object value)
		{
			object v = this[key];
			if(v!=null)
			{
				throw new System.Exception(string.Format("Collection already contains ObjectHandle='{0}' with value='{1}', adding value='{2}'", Convert.ToString(key), Convert.ToString(v), Convert.ToString(value)));
			}
			this[key] = value;
		}

		System.Collections.IDictionaryEnumerator System.Collections.IDictionary.GetEnumerator()
		{
			return new HandleObjectDictionary.Enumerator(this);
		}

		object System.Collections.IDictionary.this[object key]
		{
			get
			{
				return this[(Cerebrum.ObjectHandle)key];
			}
			set
			{
				this[(Cerebrum.ObjectHandle)key] = value;
			}
		}

		void System.Collections.IDictionary.Remove(object key)
		{
			this.Remove((Cerebrum.ObjectHandle)key);
		}

		bool System.Collections.IDictionary.Contains(object key)
		{
			return this[(Cerebrum.ObjectHandle)key]!=null;
		}

		public void Clear()
		{
			this.m_Array = new object [1024];
			this.m_Count = 0;
		}

		public System.Collections.ICollection Values
		{
			get
			{
				throw new System.NotImplementedException();
			}
		}

		void System.Collections.IDictionary.Add(object key, object value)
		{
			Add((Cerebrum.ObjectHandle)key, value);
		}

		public System.Collections.ICollection Keys
		{
			get
			{
				throw new System.NotImplementedException();
			}
		}

		public bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		#endregion

		#region ICollection Members

		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		public int Count
		{
			get
			{
				return this.m_Count;
			}
		}

		public void CopyTo(Array array, int index)
		{
			throw new System.NotImplementedException();
		}

		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		#endregion
	}
}

/*
namespace Cerebrum.Collections.Specialized
{
	/// <summary>
	/// Summary description for HandleMap.
	/// </summary>
	[Serializable()]
	public class HandleObjectDictionary
	{
		private HandleObjectNode[] m_Array;

		public HandleObjectDictionary()
		{
			m_Array = new HandleObjectNode[1024];
		}

		/// <summary>
		/// ������������� ��� ���������� ������ �� ��������� ObjectHandle.
		/// </summary>
		public object this[Cerebrum.ObjectHandle ObjectHandle]
		{
			set
			{
				int v = ObjectHandle.ToInt32();
				int section3 = (v >> 22) & 0x03FF;
				int section2 = (v >> 11) & 0x07FF;
				int section1 = v & 0x07FF;

				HandleObjectNode s3;
				HandleObjectNode s2;
				HandleObjectNode s1;

				s3 = m_Array[section3];
				if (s3 == null)
				{
					s3 = new HandleObjectNode();
					m_Array[section3] = s3;
				}

				s2 = s3[section2];
				if (s2 == null)
				{
					s2 = new HandleObjectNode();
					s3[section2] = s2;
				}

				s1 = s2[section1];
				if (s1 == null)
				{
					s1 = new HandleObjectNode();
					s2[section1] = s1;
				}
				s1.Object = value;
			}
			get
			{
				int v = ObjectHandle.ToInt32();
				int section3 = (v >> 22) & 0x03FF;
				int section2 = (v >> 11) & 0x07FF;
				int section1 = v & 0x07FF;

				HandleObjectNode s3;
				HandleObjectNode s2;
				HandleObjectNode s1;

				if ((s3 = m_Array[section3]) == null)
				{
					return null;
				}
				else if ((s2 = s3[section2]) == null)
				{
					return null;
				}
				else if ((s1 = s2[section1]) == null)
				{
					return null;
				}
				else
				{
					return s1.Object;
				}
			}
		}

		/// <summary>
		/// �������� ������������ �������.
		/// </summary>
		/// <param name="ObjectHandle">����� ��� ������ �������.</param>
		public void Remove(Cerebrum.ObjectHandle ObjectHandle)
		{
			int v = ObjectHandle.ToInt32();
			int section3 = (v >> 22) & 0x03FF;
			int section2 = (v >> 11) & 0x07FF;
			int section1 = v & 0x07FF;

			if (m_Array[section3][section2][section1] != null)
			{
				m_Array[section3][section2][section1] = null;
			}
			if ((m_Array[section3][section2] != null) && (m_Array[section3][section2].ChildrenNeuronCount == 0))
			{
				m_Array[section3][section2] = null;
				if ((m_Array[section3] != null) && (m_Array[section3].ChildrenNeuronCount == 0))
				{
					m_Array[section3] = null;
				}
			}
		}

		[Serializable()]
		private class HandleObjectNode
		{
			private HandleObjectNode[] m_Children;
			private object m_Object;

			public HandleObjectNode()
			{
				m_Children = new HandleObjectNode[2048];
				m_Object = null;
			}

			public int ChildrenNeuronCount
			{
				get
				{
					int nCount = 0;

					for (int i = 0; i < 2048; i++)
					{
						if (m_Children[i] != null)
						{
							nCount++;
						}
					}
					return nCount;
				}
			}

			public HandleObjectNode this[int section]
			{
				set
				{
					m_Children[section] = value;
				}
				get
				{
					return m_Children[section];
				}
			}

			public object Object
			{
				set
				{
					m_Object = value;
				}
				get
				{
					return m_Object;
				}
			}
		}
	}
}
*/