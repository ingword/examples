// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Collections
{
	public class CollectionItemChangeEventArgs: System.EventArgs 
	{
		protected int m_Index;
		protected object m_Value;

		public int Index
		{
			get
			{
				return m_Index;
			}
		}

		public object Value
		{
			get
			{
				return m_Value;
			}
		}

		public CollectionItemChangeEventArgs(int index, object value)
		{
			this.m_Index = index;
			this.m_Value = value;
		}
	}

	public delegate void CollectionItemChangeEventHandler(object sender, CollectionItemChangeEventArgs e);

	public interface ICollectionEvents
	{
		//event System.EventHandler Clearing;
		//event System.EventHandler Cleared;
		event CollectionItemChangeEventHandler Inserting;
		event CollectionItemChangeEventHandler Inserted;
		event CollectionItemChangeEventHandler Removing;
		event CollectionItemChangeEventHandler Removed;
	}

	public abstract class AbstractCollection : Cerebrum.DisposableObject, System.Collections.ICollection 
	{
		protected object[] m_Items;
		protected int m_Count;

		//Constructor
		public AbstractCollection()
		{
			m_Count = 0;
			m_Items = null;
		}

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				m_Items = null;
			}
			base.Dispose(disposing);
		}

		object System.Collections.ICollection.SyncRoot
		{
			get{return this.GetSyncRoot(); }
		} 

		bool System.Collections.ICollection.IsSynchronized
		{
			get {return this.GetIsSynchronized(); }
		}	
	
		public virtual int Count 
		{ 
			get
			{
				return this.GetItemsCount(); 
			}
		}

		protected abstract object GetSyncRoot();
		protected abstract int GetItemsCount();
		protected abstract bool GetIsSynchronized();
		public abstract void CopyTo(Array dest, int index);
		public abstract System.Collections.IEnumerator GetEnumerator();

		/*public override bool Equals(object other)
		{
			MYControlCollection collection1;
			int num1;
			collection1 = (other as MYControlCollection);
			if (collection1 == null)
			{
				return false; 
			}
			if (((this.m_Items == null) != (collection1.controls == null)) || ((this.m_Items != null) && (this.m_Items.Length != collection1.controls.Length)))
			{
				return false; 
			}
			if (this.m_Items == null)
			{
				return true; 
			}
			for (num1 = 0; (num1 < this.m_Items.Length); num1 = (num1 + 1))
			{
				if (this.m_Items[num1] != collection1.controls[num1])
				{
					return false; 
				}
 
			}
			return true; 
		}

		public override int GetHashCode()
		{
			return base.GetHashCode(); 
		} */







		protected class ArraySubsetEnumerator : System.Collections.IEnumerator
		{
			private object[] array;
			private int current; 
			private int total;

			public ArraySubsetEnumerator(object[] array, int count)
			{
				this.array = array;
				this.total = count;
				this.current = -1; 
			}
			public void Reset()
			{
				this.current = -1;
			}
			public bool MoveNext()
			{
				if (this.current < (this.total - 1))
				{
					this.current = (this.current + 1);
					return true; 
				}
				return false; 
			}
			public object Current 
			{ 
				get 
				{
					if (this.current == -1)
					{
						return null; 
					}
					return this.array[this.current]; 
				}
				
			}

		}


		protected void EnsureCapacity(int min) 
		{
			if (this.m_Items==null || this.m_Items.Length < min) 
			{
				object [] temp = new Object[min];
				if (this.m_Items!=null && this.m_Count > 0)
					Array.Copy(this.m_Items, 0, temp, 0, this.m_Count);
				this.m_Items = temp;
			}
		}

		protected int SearchItem(object value)
		{
			int i;
			for (i = 0; i < this.m_Count; i++)
			{
				if (System.Object.ReferenceEquals(value, this.m_Items[i]) || value.Equals(this.m_Items[i]))
				{
					return i; 
				}
 
			}
			return -1; 
		}

		protected abstract object ObtainItem(int index);
		protected abstract void UpdateItem(int index, object value);

		protected void RemoveItem(int index) 
		{
			Array.Copy(this.m_Items, (index + 1), this.m_Items, index, ((this.m_Count - index) - 1));
			this.m_Count = (this.m_Count - 1);
			this.m_Items[this.m_Count] = null;
		}

		protected void InsertItem(int index, object value) 
		{
			if (index < 0 || index > this.m_Count)
				throw new ArgumentOutOfRangeException("index", "Argument out of range exception"/*Environment.GetResourceString("ArgumentOutOfRange_ArrayListInsert")*/);
			if (this.m_Items==null || this.m_Count == this.m_Items.Length)
				this.EnsureCapacity((this.m_Count + 1) * 2);
			if (index < this.m_Count)
				Array.Copy(this.m_Items, index, this.m_Items, index + 1, this.m_Count - index);
			this.UpdateItem(index, value);
			this.m_Count = this.m_Count + 1;
			//this._version = this._version + 1;
		}

		
		protected void MoveItem(int fromIndex, int toIndex)
		{
			object item;
			int num1;
			int num2;
			int num3;
			num1 = (toIndex - fromIndex);
			switch (num1)
			{
				case 0:
				{
					return;
				}
				case -1:
				case 1:
				{
					item = this.m_Items[fromIndex];
					this.m_Items[fromIndex] = this.m_Items[toIndex];
					break;
				}
				default:
				{
					item = this.m_Items[fromIndex];
					num2 = 0;
					num3 = 0;
					if (num1 > 0)
					{
						num2 = (fromIndex + 1);
						num3 = fromIndex;
 
					}
					else
					{
						num2 = toIndex;
						num3 = (toIndex + 1);
						num1 = -num1;
					}
					Array.Copy(this.m_Items, num2, this.m_Items, num3, num1); 
					break;
				}
			}
			this.m_Items[toIndex] = item;
		}
	}

	/// <summary>
	/// Summary description for AbstractCollection.
	/// </summary>
	public abstract class AbstractListBase : AbstractCollection, System.Collections.IList
	{
		
	
		protected void ClearItems()
		{
			int c;
			while ((c = this.GetItemsCount()) != 0)
			{
				this.OnRemove(c - 1, this.ObtainItem(c - 1)); 
			}
 
		}


		int System.Collections.IList.Add(object value)
		{
			if(this.Validate(value, true))
			{
				int i = this.GetItemsCount();
				this.OnInsert(i, value);
				return i;
			}
			return -1;
		}


		bool System.Collections.IList.Contains(object value)
		{
			if(this.Validate(value, false))
			{
				return this.OnSearch(value)>=0; 
			}
			return false; 
		}
		

		int System.Collections.IList.IndexOf(object value)
		{
			return this.OnSearch(value);
		}

		void System.Collections.IList.Insert(int index, object value)
		{
			if(this.Validate(value, true))
			{
				this.OnInsert(index, value);
			}
		} 


		void System.Collections.IList.Remove(object value)
		{
			int i = this.OnSearch(value);
			if(i>=0)
			{
				this.OnRemove(i, value);
			}
		} 
		
		void System.Collections.IList.RemoveAt(int index)
		{
			this.OnRemove(index, this.ObtainItem(index));
		}

		bool System.Collections.IList.IsFixedSize
		{
			get{return this.GetIsFixedSize(); }
		}
		
		void System.Collections.IList.Clear() 
		{
			this.OnClear();
		}

		object System.Collections.IList.this[int index]
		{
			get
			{
				return this.ObtainItem(index);
			}
			set
			{ 
				if(this.Validate(value, true))
				{
					this.OnUpdate(index, value);
				}
			}
		}

		bool System.Collections.IList.IsReadOnly 
		{
			get { return this.GetIsReadOnly();	 } 
		}



		protected abstract bool GetIsReadOnly();

		protected abstract bool GetIsFixedSize();

		protected abstract void OnClear();
		
		protected abstract void OnInsert(int index, object value);

		protected abstract void OnRemove(int index, object value);

		protected abstract void OnUpdate(int index, object value);

		protected abstract bool Validate(object value, bool throwException);

		protected abstract int OnSearch(object value);

	}

	public abstract class ConcreteListBase : AbstractListBase
	{

		protected override object ObtainItem(int index)
		{
			return this.m_Items[index];
		}

		protected override void UpdateItem(int index, object value)
		{
			this.m_Items[index] = value;
		}

		protected override int GetItemsCount()
		{
			return this.m_Count;
		}

		protected override object GetSyncRoot()
		{
			return this;
		}

		public override void CopyTo(Array dest, int index)
		{
			if (this.m_Count > 0)
			{
				Array.Copy(this.m_Items, 0, dest, index, this.m_Count);
			}
		}
		
		public override System.Collections.IEnumerator GetEnumerator()
		{
			return new ArraySubsetEnumerator(this.m_Items, this.m_Count);
		}

		protected override bool GetIsReadOnly()
		{
			return false;
		}

		protected override bool GetIsFixedSize()
		{
			return false;
		}

		protected override bool GetIsSynchronized()
		{
			return false;
		}

		protected override void OnInsert(int index, object value)
		{
			InsertItem(index, value);
		}

		protected override void OnUpdate(int index, object value)
		{
			UpdateItem(index, value);
		}

		protected override void OnClear()
		{
			this.ClearItems();
		}

		protected override void OnRemove(int index, object value)
		{
			this.RemoveItem(index);
		}

		protected override int OnSearch(object value)
		{
			return this.SearchItem(value);
		}

	}
}