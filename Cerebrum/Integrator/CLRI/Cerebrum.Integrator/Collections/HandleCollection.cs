// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Text;

namespace Cerebrum.Collections.Specialized
{
    public class HandleCollection : Cerebrum.Collections.ConcreteListBase, Cerebrum.IPersistent
    {
        public HandleCollection()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region IPersistent Members

        public void Serialize(Cerebrum.SerializeDirection direction, IConnector connector, System.IO.Stream stream)
        {
            int count;
            switch (direction)
            {
                case Cerebrum.SerializeDirection.Load:
                    count = Cerebrum.ObjectHandle.ReadFrom(stream).ToInt32();
                    this.EnsureCapacity(count);
                    for (int i = 0; i < count; i++)
                    {
                        this.m_Items[i] = Cerebrum.ObjectHandle.ReadFrom(stream);
                    }
                    for (int i = count; i < this.m_Items.Length; i++)
                    {
                        this.m_Items[i] = null;
                    }
                    this.m_Count = count;
                    break;
                case Cerebrum.SerializeDirection.Save:
                    count = this.m_Count;
                    Cerebrum.ObjectHandle.WriteTo(stream, new Cerebrum.ObjectHandle(count));
                    for (int i = 0; i < count; i++)
                    {
                        Cerebrum.ObjectHandle.WriteTo(stream, (Cerebrum.ObjectHandle)this.m_Items[i]);
                    }
                    break;
                default:
                    throw new System.Exception("Unknown Cerebrum.SerializeDirection = " + direction.ToString());
            }
        }

        #endregion

        public int Add(Cerebrum.ObjectHandle handle)
        {
            return ((System.Collections.IList)this).Add(handle);
        }

        public void Remove(Cerebrum.ObjectHandle handle)
        {
            this.OnRemove(this.OnSearch(handle), handle);
        }

        protected override bool Validate(object value, bool throwException)
        {
            bool valid = value is Cerebrum.ObjectHandle;
            if (throwException && !valid)
            {
                throw new System.InvalidCastException("SyanpseCollection can store only Synapse objects");
            }
            return valid;
        }

        public Cerebrum.ObjectHandle this[int index]
        {
            get
            {
                return (Cerebrum.ObjectHandle)((System.Collections.IList)this)[index];
            }
            set
            {
                ((System.Collections.IList)this)[index] = value;
            }
        }

        public ObjectHandle[] ToArray()
        {
            Cerebrum.ObjectHandle[] arr = new ObjectHandle[this.Count];
            this.CopyTo(arr, 0);
            return arr;
        }
    }
}
