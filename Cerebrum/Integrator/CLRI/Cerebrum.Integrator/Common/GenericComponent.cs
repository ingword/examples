// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator
{
	/// <summary>
	/// Summary description for CerebrumComponent.
	/// </summary>
	public class GenericComponent : Cerebrum.DisposableObject, Cerebrum.IComponent, System.ComponentModel.IComponent
	{
		protected internal Cerebrum.IConnector m_Connector;

		public GenericComponent()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/*public Cerebrum.ObjectHandle PlasmaHandle
		{
			get
			{
				return m_Connector.PlasmaHandle;
			}
		}*/

		public object GetAttributeContainer(Cerebrum.ObjectHandle handle)
		{
			using(Cerebrum.IContainer c0 = GetChildComponents())
			{
				return c0.AttachConnector(handle);
			}
		}


		public object GetAttributeComponent(Cerebrum.ObjectHandle handle)
		{
			using(Cerebrum.IContainer c0 = GetChildComponents())
			{
				return Cerebrum.Integrator.Utilites.ObtainComponent(c0, handle);
			}
		}
		public void SetAttributeComponent(Cerebrum.ObjectHandle handle, object value)
		{
			using(Cerebrum.IContainer c0 = GetChildComponents())
			{
				Cerebrum.Integrator.Utilites.UpdateComponent(c0, handle, value);
			}
		}

		protected Cerebrum.IContainer GetChildComponents()
		{
			return (m_Connector as Cerebrum.Runtime.IConnectorEx).GetContainer() as Cerebrum.IContainer;
		}

		public Cerebrum.IConnector GetConnector()
		{
#warning " using SurrogateConnector "
			return Cerebrum.Runtime.InstanceConnector.FromConnector(m_Connector);
		}
		
		public Cerebrum.Integrator.DomainContext DomainContext
		{
			get
			{
				return Cerebrum.Integrator.DomainContext.FromConnector(this.m_Connector);
			}
		}

		#region IComponent Members


		void Cerebrum.IComponent.SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			SetConnector(direction, connector);
		}
		protected virtual void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			m_Connector = connector;
			//connector.Disposing += new EventHandler(connector_Disposing);
		}
		#endregion

		#region IComponent Members

		private static readonly object EventDisposed = new object();

		public event System.EventHandler Disposed
		{
			add
			{
				this.GetEventHandlerList().AddHandler(GenericComponent.EventDisposed, value);
			}
			remove
			{
				if (this.m_Events != null)
				{
					this.m_Events.RemoveHandler(GenericComponent.EventDisposed, value);
				}
			}
		}

		protected System.ComponentModel.ISite m_Site;
		public System.ComponentModel.ISite Site
		{
			get
			{
				return m_Site;
			}
			set
			{
				m_Site = value;
			}
		}

		#endregion

		private void connector_Disposing(object sender, EventArgs e)
		{
			if(sender==m_Connector)
			{
				m_Connector.Disposing -= new EventHandler(connector_Disposing);
				m_Connector = null;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				lock (this)
				{
					if ((this.m_Site != null) && (this.m_Site.Container != null))
					{
						this.m_Site.Container.Remove(this);
						this.m_Site = null;
					}
					if (this.m_Events != null)
					{
						EventHandler handler1 = (EventHandler) this.m_Events[GenericComponent.EventDisposed];
						if (handler1 != null)
						{
							handler1(this, EventArgs.Empty);
						}
					}
				}
			}
			if(m_Connector!=null)
			{
				m_Connector.Disposing -= new EventHandler(connector_Disposing);
				m_Connector = null;
			}
			base.Dispose (disposing);
		}

	}
}
