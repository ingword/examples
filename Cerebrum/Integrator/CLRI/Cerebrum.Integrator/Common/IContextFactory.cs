// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator
{
	/// <summary>
	/// ��������� ����������� �������� ���������. ������
	/// ��������� ���������� ��������� ���������� � ��������
	/// ������ �� ���������. ������ ���� ���������� �����������
	/// �������������� ���������� � �������� ���� ������ ������������.
	/// �������� ������ ������������ ������������ ������������ ����������
	/// ������� ���������.
	/// </summary>
	public interface IContextFactory
	{
		string DatabaseFileName {get;}

		Cerebrum.IWorkspace InitContext(string pathName, string domainName);
		/// <summary>
		/// ����� LoadContext ���������� �������� ���������� �� ���������
		/// � ��������
		/// </summary>
		/// <param name="c">��������, � ������� ���������� ������������</param>
		Cerebrum.IWorkspace LoadContext(string pathName, bool readOnly);
		/// <summary>
		/// ����� SaveDatabase ���������� ���������� ������������ �� ���������
		/// � ��������.
		/// </summary>
		/// <param name="c">��������, �� �������� ����������� ����������</param>
		void ExportDatabase(Cerebrum.Integrator.DomainContext c);
		void ImportDatabase(Cerebrum.Integrator.DomainContext c, bool isNew);
		/// <summary>
		/// ����� SaveActivity ���������� ���������� ������� ���������� �� ���������
		/// � ��������.
		/// </summary>
		/// <param name="c">��������, �� �������� ����������� ����������</param>
		void SaveActivity(Cerebrum.Integrator.DomainContext c);
	}
}
