// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator
{
	/// <summary>
	/// Summary description for DomainAddedEventArgs.
	/// </summary>
	public class DomainContextEventArgs
	{
		public DomainContextEventArgs(DomainContext domainContext)
		{
			m_DomainContext = domainContext;
		}

		private DomainContext m_DomainContext;

		public DomainContext DomainContext
		{
			get
			{
				return m_DomainContext;
			}
		}
	}

	public delegate void DomainContextEventHandler(object sender, DomainContextEventArgs e);
}
