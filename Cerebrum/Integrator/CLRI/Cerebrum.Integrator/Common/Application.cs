using System;

namespace Cerebrum.Integrator
{

	/// <summary>
	/// ����� Application ������������ ����� ��������� ����� ����������. ����� �����
	/// ������������ ������� ����� ����� ����������. 
	/// </summary>
	public abstract class Application : Cerebrum.DisposableObject, IReleasable, IEnabledProperty, IEnabledChangedEvent
	{
		protected internal static readonly object EnabledChangedEvent = new object();
		protected internal static readonly object DomainContextOpenedEvent = new object();
		protected internal static readonly object DomainContextClosedEvent = new object();

		protected Cerebrum.Runtime.Environment m_Environment;

		protected string m_MasterContextDomainName;
		public string MasterContextDomainName
		{
			get
			{
				return this.m_MasterContextDomainName;
			}
			set
			{
				this.m_MasterContextDomainName = value;
			}
		}

		private bool m_IsMasterDomainReadOnly;
		protected bool IsMasterDomainReadOnly
		{
			get
			{
				return this.m_IsMasterDomainReadOnly;
			}
			set
			{
				this.m_IsMasterDomainReadOnly = value;
			}
		}

		/// <summary>
		/// ����������� ������� ����������. 
		/// </summary>
		public Application()
		{
			m_Enabled = true;
			m_OpenedDomains = null;
			m_Environment = null;
			this.m_MasterContextDomainName = "Cerebrum.Integrator.Application";
			this.m_IsMasterDomainReadOnly = false;
		}

		/// <summary>
		/// ����� ������� ������ ���� ���. ������� �
		/// �������������� ��������� ��������.
		/// </summary>
		public virtual void Initialize()
		{
			if(m_Environment==null)
			{
				m_Environment = Cerebrum.Runtime.Environment.ConstructInstance();
				m_Environment.RuntimeException += new System.Threading.ThreadExceptionEventHandler(Environment_RuntimeException);
				m_Environment.Collecting += new EventHandler(Environment_Collecting);
				m_Environment.Disposing	+= new EventHandler(Environment_Disposing);
			}

			if(m_MasterContext==null)
			{
		
				//m_MasterContext = this.CreateMasterContext();


				Cerebrum.Integrator.IContextFactory l_ContextFactory;

				/// ��� ��� DomainContext � Application ��������� � ����� � ��� �� ��������, �� ��
				/// ���� ���������. ������, ������������ �������� ��������, ��� ���-�� �������
				/// �������� m_SystemDirectory � ���������. 
				//DatabaseLocationEventArgs e = new DatabaseLocationEventArgs();
				//this.OnInitMasterDatabaseLocation(e);
				//string directory = e.PrimaryDirectory;

				l_ContextFactory = this.CreateContextFactory();
				//lcs.DomainContext = m_MasterContext;

				string databaseFileName = l_ContextFactory.DatabaseFileName;

				Cerebrum.IWorkspace domain;
				bool isNew; 
				if(System.IO.File.Exists(databaseFileName))
				{
					domain = l_ContextFactory.LoadContext(databaseFileName, this.m_IsMasterDomainReadOnly);
					isNew = false;
				}
				else
				{
					domain = l_ContextFactory.InitContext(databaseFileName, this.m_MasterContextDomainName);
					isNew = true;
				}
				m_MasterContext = domain.Component as Cerebrum.Integrator.DomainContext;
				m_MasterContext.Disposing += new EventHandler(m_MasterContext_Disposing);

				l_ContextFactory.ImportDatabase(m_MasterContext, isNew);

				m_MasterContext.Initialize();

				OnEnabledChanged(new System.EventArgs());
			}
			if(m_OpenedDomains==null)
			{
				m_OpenedDomains = new System.Collections.ArrayList();
			}
		}

		public virtual void Shutdown()
		{
			if(m_OpenedDomains!=null)
			{
				foreach(Cerebrum.Integrator.DomainContext dc in (System.Collections.IEnumerable)m_OpenedDomains.Clone())
				{
					dc.Shutdown();
				}
				m_OpenedDomains = null;
			}

			if(m_MasterContext!=null)
			{
				m_MasterContext.Shutdown();
				m_MasterContext = null;
			}

			DetachEnvironment();
		}

		/// <summary>
		/// ���������� �������� ������ �� ��������� ���������� ���������.
		/// ��������� �������� �������� � ���� ��������� ������������,
		/// ����������� �� ����� VNN � �������� ������ ����������. �����
		/// �������� �������� ��������� ���������� ����� ������������� 
		/// �������� ����������. ������� ���������� ������ ���� ���������
		/// � ��������� ��������� ��� ���������� ������������� 
		/// ��������� SystemCategories.ContextFactory
		/// </summary>
		protected Cerebrum.Integrator.DomainContext m_MasterContext = null;
		/// <summary>
		/// �������� ������������ ������ �� ��������� ��������� � ������
		/// ����� ���������.
		/// </summary>
		public Cerebrum.Integrator.DomainContext MasterContext
		{
			get
			{
				return m_MasterContext;
			}
		}


		private void m_MasterContext_Disposing(object sender, EventArgs e)
		{
			if(m_MasterContext==sender)
			{
				m_MasterContext.Disposing -= new EventHandler(m_MasterContext_Disposing);
				m_MasterContext = null;
			}
		}


		protected abstract IContextFactory CreateContextFactory();
		public abstract DomainContext CreateMasterContext(string domainName, bool creating);


		public abstract void ProcessException(System.Exception ex, string source);


		/// <summary>
		/// ���������� ������ Release ���������� IReleasable
		/// </summary>
		public virtual bool Release(bool cancel)
		{
			System.ComponentModel.CancelEventArgs e = new System.ComponentModel.CancelEventArgs(cancel);
			System.ComponentModel.CancelEventHandler handler = (System.ComponentModel.CancelEventHandler)this.GetEventHandlerList()[Cerebrum.Specialized.EventKeys.ReleasingEvent];
			if (handler != null)
				handler(this, e);
			return e.Cancel;
		}


		/// <summary>
		/// ���������� ������� Releasing ���������� IReleasable
		/// </summary>
		public event System.ComponentModel.CancelEventHandler Releasing
		{
			add	{	this.GetEventHandlerList().AddHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
			remove	{	this.GetEventHandlerList().RemoveHandler(Cerebrum.Specialized.EventKeys.ReleasingEvent, value);	}
		}

		/// <summary>
		/// ���������� ��� ��������� �������� <see cref="Application.EnabledChanged">Application.EnabledChanged</see>
		/// </summary>
		public event System.EventHandler EnabledChanged
		{
			add	{	this.GetEventHandlerList().AddHandler(EnabledChangedEvent, value);	}
			remove	{	this.GetEventHandlerList().RemoveHandler(EnabledChangedEvent, value);	}
		}

		/// <summary>
		/// �������� ��������, ������������ ��� � ������ ������ ����������� ����
		/// ������������� (�������� ������������ ��������� ����)
		/// </summary>
		protected bool m_Enabled;
		/// <summary>
		/// ���������� ��������, ������������ ��� � ������ ������ ����������� ����
		/// ������������� (�������� ������������ ��������� ����)
		/// </summary>
		public bool Enabled
		{
			get 
			{
				return m_Enabled;
			}
			set
			{
				if(m_Enabled!=value)
				{
					m_Enabled = value;
					OnEnabledChanged(new System.EventArgs());
				}
			}
		}
		/// <summary>
		/// ��������� ����������� �� ��������� ��������� Enabled
		/// </summary>
		/// <param name="e">��������� �������</param>
		protected virtual void OnEnabledChanged(System.EventArgs e)
		{
			System.EventHandler handler = (System.EventHandler)this.GetEventHandlerList()[Cerebrum.Integrator.Application.EnabledChangedEvent];
			if (handler != null)
				handler(this, e);
		}

		protected System.Collections.ArrayList m_OpenedDomains;

		public System.Collections.IList OpenedDomains
		{
			get
			{
				return m_OpenedDomains;
			}
		}

		public void AddDomain(Cerebrum.Integrator.DomainContext c)
		{
			c.Disposing += new EventHandler(Context_Disposing);
			m_OpenedDomains.Add(c);
			OnDomainOpened(new DomainContextEventArgs(c));
		}

		protected virtual void OnDomainOpened(DomainContextEventArgs e)
		{
			DomainContextEventHandler handler = (DomainContextEventHandler)this.GetEventHandlerList()[Cerebrum.Integrator.Application.DomainContextOpenedEvent];
			if (handler != null)
				handler(this, e);
		}

		protected virtual void OnDomainClosed(DomainContextEventArgs e)
		{
			DomainContextEventHandler handler = (DomainContextEventHandler)this.GetEventHandlerList()[Cerebrum.Integrator.Application.DomainContextClosedEvent];
			if (handler != null)
				handler(this, e);
		}

		public event DomainContextEventHandler DomainOpened
		{
			add	{	this.GetEventHandlerList().AddHandler(DomainContextOpenedEvent, value);	}
			remove	{	this.GetEventHandlerList().RemoveHandler(DomainContextOpenedEvent, value);	}
		}

		public event DomainContextEventHandler DomainClosed
		{
			add	{	this.GetEventHandlerList().AddHandler(DomainContextClosedEvent, value);	}
			remove	{	this.GetEventHandlerList().RemoveHandler(DomainContextClosedEvent, value);	}
		}

		private void Context_Disposing(object sender, EventArgs e)
		{
			Cerebrum.Integrator.DomainContext c = (Cerebrum.Integrator.DomainContext)sender;
			c.Disposing -= new EventHandler(Context_Disposing);
			if(m_OpenedDomains!=null)
			{
				m_OpenedDomains.Remove(c);
			}
			OnDomainClosed(new DomainContextEventArgs(c));
		}

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				this.Shutdown();
			}
			base.Dispose(disposing);
		}

		private void Environment_RuntimeException(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			ProcessException(e.Exception, "Runtime Exception");
		}

		private void Environment_Collecting(object sender, EventArgs e)
		{
			try
			{
				System.GC.Collect();
				System.GC.WaitForPendingFinalizers();
			}
			catch
			{
			}
		}

		private void DetachEnvironment()
		{
			Cerebrum.Runtime.Environment env = this.m_Environment;
			if(env!=null)
			{
				this.m_Environment = null;
				env.RuntimeException -= new System.Threading.ThreadExceptionEventHandler(Environment_RuntimeException);
				env.Collecting -= new EventHandler(Environment_Collecting);
				env.Disposing -= new EventHandler(Environment_Disposing);
			}
		}
		private void Environment_Disposing(object sender, EventArgs e)
		{
			if(this.m_Environment == sender)
			{
				DetachEnvironment();
			}
		}
	}
}

