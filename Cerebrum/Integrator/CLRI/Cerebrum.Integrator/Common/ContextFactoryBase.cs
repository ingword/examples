// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator
{
	/// <summary>
	/// ������ ��������� ���������. ������������� ��������
	/// ��� �������� � ���������� ������ �� ������������ ��
	/// XML ����� ���������� ����� ��������� �������� �������.
	/// 
	/// ����� ��������� ���� �������� � ������������ ���
	/// ���� �������� � ������� Variables � ComponentId = ������ �������
	/// ������ � ������� Instances � VariableId="location"
	/// </summary>
	public abstract class ContextFactoryBase: Cerebrum.Integrator.GenericComponent, Cerebrum.Integrator.IContextFactory, Cerebrum.Runtime.IDomainFactory
	{
		protected bool m_Versions;
		protected int m_ClusterFrameSize;

		protected Cerebrum.Integrator.Application m_Application;
		/// <summary>
		/// �������� ��� ����� VNN ���� ������������ ��������� � ������� ������������
		/// </summary>
		protected string m_DatabaseFileName;
		/// <summary>
		/// �������� ��� ����� XML ������� ���������� ��������� � ������� ������������
		/// </summary>
		protected string m_ActivityFileName;
		/// <summary>
		/// ����������� ������
		/// </summary>
		public ContextFactoryBase():base()
		{
			m_ClusterFrameSize = 4;
		}

		/// <summary>
		/// ����������� ������
		/// </summary>
		/// <param name="databaseFileName">������ ���� � XML ����� � �� ������������</param>
		/// <param name="activityFileName">������ ���� � XML ����� � �� ������� ����������</param>
		public ContextFactoryBase(Cerebrum.Integrator.Application application, string databaseFileName, string activityFileName, int clusterFrameSize, bool versions):base()
		{
			m_ClusterFrameSize = clusterFrameSize;
			m_Application = application;
			m_DatabaseFileName = databaseFileName;
			m_ActivityFileName = activityFileName;
			m_Versions = versions;
		}

		public int ClusterFrameSize
		{
			get
			{
				return m_ClusterFrameSize;
			}
			set
			{
				m_ClusterFrameSize = value;
			}
		}
		/// <summary>
		/// ���������� ��� ����� XML ���� ������������ ��������� � ������� ������������
		/// ���� ������������� ����� ������� Variables � ComponentId = ������ �������
		/// ������ � ������� Instances � VariableId="database"
		/// </summary>
		public string DatabaseFileName
		{
			get
			{
				string fileName;
				if(m_DatabaseFileName!=null)
				{
					fileName = m_DatabaseFileName;
				}
				else
				{
					throw new System.NotSupportedException();
					//fileName = (string)m_DomainContext.GetSystemVariable(this.m_ComponentId, "database");
				}
				//				if(fileName!=null)
				//				{
				//					fileName = System.IO.Path.Combine(m_DomainContext.SystemDirectory, fileName);
				//				}
				return fileName;
			}
		}
		/// <summary>
		/// ���������� ��� ����� XML ���� ������� ���������� ��������� � ������� ������������
		/// ���� ������������� ����� ������� Variables � ComponentId = ������ �������
		/// ������ � ������� Instances � VariableId="activity"
		/// </summary>
		public string ActivityFileName
		{
			get
			{
				string fileName;
				if(m_ActivityFileName!=null)
				{
					fileName = m_ActivityFileName;
				}
				else
				{
					throw new System.NotSupportedException();
					//fileName = (string)m_DomainContext.GetSystemVariable(this.m_ComponentId, "activity");
				}
				//				if(fileName!=null)
				//				{
				//					fileName = System.IO.Path.Combine(m_DomainContext.SystemDirectory, fileName);
				//				}
				return fileName;
			}
		}

		/// <summary>
		/// ��������� �� ��������� �� �����
		/// </summary>
		/// <param name="c">������ �� ������ DataSet � �� ������������</param>
		public virtual Cerebrum.IWorkspace LoadContext(string databaseFileName, bool readOnly)
		{
			Cerebrum.Integrator.DomainContext c;
			Cerebrum.Runtime.Environment env = Cerebrum.Runtime.Environment.Current;
			Cerebrum.IConnector css = env.CurrentStreamVendor;
			Cerebrum.Runtime.NativeWarden cssw = css as Cerebrum.Runtime.NativeWarden;

			Cerebrum.ObjectHandle file = cssw.Remote("", databaseFileName, readOnly);
			Cerebrum.IConnector vcm = env.ReviveGroundStream(css, file, readOnly);
			//Cerebrum.Runtime.NativeWarden vcmw = vcm.GetContainer() as Cerebrum.Runtime.NativeWarden;
			Cerebrum.ObjectHandle h = new Cerebrum.ObjectHandle(1);//vcmw.Create(8, 0);
			Cerebrum.IWorkspace domain = env.ReviveDomain(vcm, h, this);

			c = domain.Component as Cerebrum.Integrator.DomainContext;
			c.m_GroundHandle = file;
			c.m_FileName = databaseFileName;

			return domain;
		}
		public virtual Cerebrum.IWorkspace InitContext(string databaseFileName, string domainName)
		{
			Cerebrum.Integrator.DomainContext c;
			Cerebrum.Runtime.Environment env = Cerebrum.Runtime.Environment.Current;
			Cerebrum.IConnector css = env.CurrentStreamVendor;
			
			Cerebrum.Runtime.NativeWarden cssw = css as Cerebrum.Runtime.NativeWarden;

			Cerebrum.ObjectHandle file = cssw.Remote("", databaseFileName, false);
			Cerebrum.Runtime.NativeWarden vcm = env.CreateGroundStream(css, file, m_ClusterFrameSize, 16) as Cerebrum.Runtime.NativeWarden;
			Cerebrum.ObjectHandle h = vcm.Create(8, 0);
			Cerebrum.IWorkspace domain = env.CreateDomain(domainName, vcm, h, this, m_Versions);

			c = domain.Component as Cerebrum.Integrator.DomainContext;
			c.m_GroundHandle = file;
			c.m_FileName = databaseFileName;

			return domain;
		}

		public virtual Cerebrum.IWorkspace InitContextDebug(string domainName)
		{
			Cerebrum.Integrator.DomainContext c;
			Cerebrum.Runtime.Environment env = Cerebrum.Runtime.Environment.Current;
			Cerebrum.IConnector css = env.CreateLiquidMemory(-1, -1);
			Cerebrum.ObjectHandle file;

			Cerebrum.Runtime.NativeWarden cssw = css as Cerebrum.Runtime.NativeWarden;

			file = cssw.Create(4, 0);

			Cerebrum.Runtime.NativeWarden vcm = env.CreateGroundStream(css, file, m_ClusterFrameSize, 16) as Cerebrum.Runtime.NativeWarden;

			Cerebrum.ObjectHandle h = vcm.Create(8, 0);
			Cerebrum.IWorkspace domain = env.CreateDomain(domainName, vcm, h, this, false);

			c = domain.Component as Cerebrum.Integrator.DomainContext;
			c.m_GroundHandle = file;
			c.m_FileName = string.Empty;

			return domain;
		}

		//public bool OpenContext(Cerebrum.Integrator.DomainContext c)
		//{

			//			System.IO.FileStream fs = null;

			//			c.SystemDatabase.AcceptChanges();
			//
			//			try
			//			{
			//				fs = new System.IO.FileStream(ActivityFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
			//				c.SystemActivity.ReadXml(fs);
			//				c.SystemActivity.AcceptChanges();
			//			}
			//			catch(System.Exception ex)
			//			{
			//				this.m_DomainContext.Application.AddActivityRow(ex, m_ComponentId + ".LoadActivity");
			//				m_DomainContext.Application.MessageBox(ex, "SystemActivityReadingError");
			//			}
			//			finally
			//			{
			//				if(fs!=null) fs.Close();
			//			}
			//
			//			fs = null;


			//			try
			//			{
			//				fs = new System.IO.FileStream(databaseFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
			//				c.SystemDatabase.ReadXml(fs);
			//				c.SystemDatabase.AcceptChanges();
			//			}
			//			catch(System.Exception ex)
			//			{
			//				this.m_DomainContext.Application.AddActivityRow(ex, m_ComponentId + ".LoadDatabase");
			//				m_DomainContext.Application.MessageBox(ex, "SystemDatabaseReadingError");
			//			}
			//			finally
			//			{
			//				if(fs!=null) fs.Close();
			//			}
			//
			//			fs = null;

			//			try
			//			{
			//				string s0 = System.IO.Path.GetDirectoryName(databaseFileName);
			//				string s1 = System.IO.Path.GetFileNameWithoutExtension(databaseFileName);
			//				string s2 = System.IO.Path.GetExtension(databaseFileName);
			//				string s3 = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
			//				string s = System.IO.Path.Combine(s0, s1 + "." + s3 + s2);
			//
			//				if(System.IO.File.Exists(s))
			//				{
			//					RuntimeDatabase db = new RuntimeDatabase();
			//					fs = new System.IO.FileStream(s, System.IO.FileMode.Open, System.IO.FileAccess.Read);
			//					db.ReadXml(fs);
			//					db.AcceptChanges();
			//					c.FallbackDatabases.Add(db);
			//				}
			//			}
			//			catch(System.Exception ex)
			//			{
			//				this.m_DomainContext.Application.AddActivityRow(ex, m_ComponentId + ".LoadDatabase");
			//				m_DomainContext.Application.MessageBox(ex, "SystemDatabaseReadingError");
			//			}
			//			finally
			//			{
			//				if(fs!=null) fs.Close();
			//			}

		//}

		/// <summary>
		/// ��������� �� ������������ � �����
		/// </summary>
		/// <param name="c">������ �� ������ DomainContext � �� ������������</param>
		public abstract void ExportDatabase(Cerebrum.Integrator.DomainContext c);

		/// <summary>
		/// ��������� �� ������� ���������� � �����
		/// </summary>
		/// <param name="c">������ �� ������ DomainContext � �� ����������</param>
		public abstract void SaveActivity(Cerebrum.Integrator.DomainContext c);

		/// <summary>
		/// ��������� � ������������ �������� ������������ �� ����� �������� ������.
		/// 
		/// � �������� ������� ������������, ��� ������������� ������� �������� � 
		/// ��������� ���������� incoming ��� �������� ������ ���������.
		/// 
		/// ��������������� ���������� ������� �������� ��������� ������ 
		/// ������������ � ���� ������� � ���������� ���� ������ � �� ����� 
		/// ��������� �������� �������, � ������� ��� ������ ������ ��������������� 
		/// � ������� ������.
		/// </summary>
		/// <param name="c">��������, � ������� ���������� ������������</param>
		public abstract void ImportDatabase(Cerebrum.Integrator.DomainContext c, bool isNew);

		#region IDomainFactory Members

		public abstract object CreateContext(string domainName, bool creating);

		#endregion
	}
}
