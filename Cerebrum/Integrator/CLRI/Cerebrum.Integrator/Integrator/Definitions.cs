// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator
{
	public interface IEnabledProperty
	{
		bool Enabled {get;set;}
	}
	public interface IEnabledChangedEvent
	{
		event System.EventHandler EnabledChanged;
	}

	/// <summary>
	/// ��������� ������ ������� ��� DomainContext
	/// </summary>
	public interface IContextProperties
	{
		bool ContainsProperty(object key);
		Cerebrum.Integrator.DomainContext DomainContext
		{
			get; 
		}
		object this[object key]
		{
			set;
			get;
		}
	}
}
