// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator
{
	/// <summary>
	/// ��������� ������ DomainContext �������� � ����
	/// ������ �� ������� ��������� �� �������� ���������
	/// �������. � ��� ������� ������ ������ �����������
	/// �������, ������ ����������� �����������,
	/// ������ ������ ��������, ������ ���������� ������,
	/// ���� ������ ������������, ������� ���� MDI ����������
	/// </summary>
	public class DomainContext : Cerebrum.Integrator.GenericComponent, Cerebrum.IContainer  //-:IReleasable
	{
		public static DomainContext FromConnector(Cerebrum.IConnector connector)
		{
			if(connector!=null)
			{
				return (Cerebrum.Integrator.DomainContext)connector.Workspace.Component;
			}
			else
			{
				return null;
			}
		}

		protected Cerebrum.Runtime.Environment m_Environment;
		//protected Cerebrum.IConnector m_DomainConnector;

		private void m_Environment_Disposing(object sender, EventArgs e)
		{
			if(m_Environment==sender)
			{
				this.Dispose();
			}
		}
		//
		//		private void m_Domain_Disposing(object sender, EventArgs e)
		//		{
		//			if(m_DomainConnector==sender)
		//			{
		//				this.Dispose();
		//			}
		//		}

		public virtual void Initialize()
		{
		}

#warning " must be obtained from VCM ? "
		internal Cerebrum.ObjectHandle m_GroundHandle;
		internal string m_FileName;

		public string FileName
		{
			get
			{
				return this.m_FileName;
			}
		}

		public void Shutdown()
		{
			this.Dispose();
		}

		private bool m_Disposed = false;

		public override void Dispose()
		{
			if(!this.m_Disposed)
			{
				this.m_Disposed = true;
				base.Dispose();
			}
		}

		protected virtual void OnShutdown()
		{
			Cerebrum.IConnector domain = this.m_Connector;
			if(domain!=null)
			{
				this.m_Connector = null;

				Cerebrum.Runtime.Environment env = Cerebrum.Runtime.Environment.Current;
			
				try
				{
					System.GC.Collect();
					System.GC.WaitForPendingFinalizers();
				}
				catch(System.Threading.ThreadAbortException)
				{
					throw;
				}
				catch {}

				if(env!=null)
				{
					if(!env.IsUnlimitedMemory) env.Collect();
				}

                Cerebrum.ISavepoints savepoints = ((Cerebrum.IWorkspace)domain).Savepoints;
                if (savepoints!=null)
                {
                    savepoints.Flush();
                }

				try
				{
					System.GC.Collect();
					System.GC.WaitForPendingFinalizers();
				}
				catch(System.Threading.ThreadAbortException)
				{
					throw;
				}
				catch {}

				if(env!=null)
				{
					if(!env.IsUnlimitedMemory) env.Collect();
				}

				try
				{
					System.GC.Collect();
					System.GC.WaitForPendingFinalizers();
				}
				catch(System.Threading.ThreadAbortException)
				{
					throw;
				}
				catch {}

				Cerebrum.IConnector ground;

				ground = ((Cerebrum.Runtime.NativeDomain)domain).Ground;

				domain.Dispose();
				ground.Dispose();
#warning "old stream vendor"
				if(env!=null)
				{
					Cerebrum.IConnector css = env.CurrentStreamVendor;
					Cerebrum.Runtime.NativeWarden w = css as Cerebrum.Runtime.NativeWarden;

					w.Forget(m_GroundHandle);
				}
				m_GroundHandle = Cerebrum.ObjectHandle.Null;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				OnShutdown();

				//				if(m_DomainConnector!=null)
				//				{
				//					m_DomainConnector.Disposing -= new EventHandler(m_Domain_Disposing);
				//					m_DomainConnector = null;
				//				}
				if(m_Environment!=null)
				{
					m_Environment.Disposing -= new EventHandler(m_Environment_Disposing);
					m_Environment = null;
				}
			}
			base.Dispose(disposing);
		}

		public Cerebrum.Runtime.Environment Environment
		{
			get
			{
				return m_Environment;
			}
		}

		public Cerebrum.IWorkspace Workspace
		{
			get
			{
				return this.m_Connector.Workspace;
			}
			//			set
			//			{
			//
			//				if(m_DomainConnector!=value)
			//				{
			//					if(m_DomainConnector!=null)
			//					{
			//						m_DomainConnector.Disposing -= new EventHandler(m_Domain_Disposing);
			//					}
			//					m_DomainConnector = value;
			//					if(m_DomainConnector!=null)
			//					{
			//						m_DomainConnector.Disposing += new EventHandler(m_Domain_Disposing);
			//					}
			//				}
			//			}
		}

		protected bool m_HasChanges;
		public bool HasChanges
		{
			get
			{
				return m_HasChanges;
			}
			set
			{
				m_HasChanges = value;
			}
		}

		public string DomainName
		{
			get
			{
				return ((Cerebrum.IWorkspace)this.m_Connector).Name;
			}
		}


		/// <summary>
		/// ������ �� ������� ������ ����������
		/// </summary>
		protected /*Cerebrum.Integrator.Application*/object m_Application;

		//		/// <summary>
		//		/// ������ ����������� ������� 
		//		/// </summary>
		//		protected System.Collections.Hashtable m_PluggedModules;
		//
		//		/// <summary>
		//		/// ������ ����������� �����������, ����� ����� �� ������ ������� ����� ���������
		//		/// </summary>
		//		protected System.Collections.Hashtable m_StaticInstances;
		//
		//		/// <summary>
		//		/// ������ ������ �������� 
		//		/// </summary>
		//		protected System.Collections.Hashtable m_Factories;
		//
		//		/// <summary>
		//		/// ������ ���������� ������, �������� �������� ����������
		//		/// ������, ����� ����� = ������� ����� ���������.
		//		/// </summary>
		//		protected System.Collections.Hashtable m_SessionVariables;
		//
		//		/// <summary>
		//		/// ���� ������ ������������ ,  �������� ����������,
		//		/// ����������� �������� ���������
		//		/// </summary>
		//		protected Cerebrum.Integrator.SystemDatabase m_SystemDatabase;
		//
		//		protected Cerebrum.Integrator.SystemActivity m_SystemActivity;
		//		public Cerebrum.Integrator.SystemActivity SystemActivity
		//		{
		//			get
		//			{
		//				return m_SystemActivity;
		//			}
		//			set 
		//			{
		//				m_SystemActivity = value;
		//			}
		//		}
		//
		//		/// <summary>
		//		/// ���������� �������� ������ �� ������� MDI ����
		//		/// ����������. �� ����� ������ ���������� �������
		//		/// ���� MDI ����� ������������ ����������� � ����������� ������.
		//		/// </summary>
		//		protected Cerebrum.Integrator.PrimaryWindowForm m_PrimaryWindow;
		//
		//		/// <summary>
		//		/// �������� ������������ �� ������������
		//		/// </summary>
		//		public Cerebrum.Integrator.SystemDatabase SystemDatabase
		//		{
		//			get
		//			{
		//				return m_SystemDatabase;
		//			}
		//		}
		/// <summary>
		/// �������� ������������ ������� ������ ����������
		/// </summary>
		public /*Cerebrum.Integrator.Application*/object Application
		{
			get
			{
				return this.m_Application;
			}
		}
		//
		//
		//		/// <summary>
		//		/// ����������� ���������. �������������� ���������� ����������.
		//		/// </summary>
		//		/// <param name="Application">������� ������ ����������</param>
		//		/// <param name="SystemDatabase">�� ������������</param>
		public DomainContext(object application/*-Cerebrum.IConnector domain, Cerebrum.Integrator.Application application, Cerebrum.Integrator.SystemDatabase systemDatabase, Cerebrum.Integrator.SystemActivity systemActivity*/)
		{
			m_Properties = new Cerebrum.Integrator.ContextProperties(this);
			//!--m_SystemDirectory = null;
			m_Environment = Cerebrum.Runtime.Environment.Current;
			m_Environment.Disposing += new EventHandler(m_Environment_Disposing);
			m_HasChanges = false;

			//			m_DomainConnector = null;

			//
			m_Application = application;
			//			m_SystemDatabase = systemDatabase;
			//			m_SystemActivity = systemActivity;
			//
			//			m_Application.Relaxing += new System.ComponentModel.CancelEventHandler(this.Relax);
			//			m_Application.Cleaning += new System.EventHandler(this.Clean);
			//
			//			m_PluggedModules = new System.Collections.Hashtable();
			//			m_StaticInstances = new System.Collections.Hashtable();
			//			m_Factories = new System.Collections.Hashtable();
			//			m_SessionVariables = new System.Collections.Hashtable();
			//
			//			m_FallbackDatabases = new SystemDatabaseCollection();
		}

		private Cerebrum.Integrator.IContextProperties m_Properties;

		public Cerebrum.Integrator.IContextProperties Properties
		{
			get
			{
				return m_Properties;
			}
		}

		/// <summary>
		/// ������ ���� � ����� � ������� ��������� Cerebrum.Integrator.dll
		/// ���������������� ��� ������ ���������
		/// </summary>
		//!--protected string m_SystemDirectory;

		/// <summary>
		/// ���������� ���� � ����� � ������� ��������� Cerebrum.Integrator.dll
		/// </summary>
		/*!--public string SystemDirectory
		{
			get
			{
				if(m_SystemDirectory==null)
				{
					m_SystemDirectory = System.IO.Path.GetDirectoryName(this.GetType().Assembly.Location);
				}
				return m_SystemDirectory;
			}
		}
		*/

		public Cerebrum.Data.TableView TablesView
		{
			get
			{
				return GetTableView(Cerebrum.Specialized.Concepts.TablesTableId);
			}
		}

		public Cerebrum.Data.TableView GetTableView(Cerebrum.ObjectHandle handle)
		{
			using(Cerebrum.IConnector connector = ((Cerebrum.IContainer)this.m_Connector).AttachConnector(handle))
			{
				Cerebrum.Reflection.TableDescriptor tblTables = connector.Component as Cerebrum.Reflection.TableDescriptor;
				return tblTables.GetTableView();
			}
		}

		public Cerebrum.IConnector GetTable(string name)
		{
#warning "//using"
			using(Cerebrum.Data.TableView tbls = GetTableView(Cerebrum.Specialized.Concepts.TablesTableId))
			{
				//Cerebrum.Data.TableView tbls = GetTableView(Cerebrum.Specialized.Concepts.TablesTableId);
			
				System.ComponentModel.PropertyDescriptor prp = tbls.GetItemProperties(null)["Name"];
				int i = tbls.Find(prp, name);
				if(i<0)
				{
					return null;
				}
				Cerebrum.Data.ComponentItemView item = tbls[i];
				return item.GetComposite();
			}
			
		}

		#region IContainer Members

		//TODO should be depricated
		public object ObtainComponent(Cerebrum.ObjectHandle plasma)
		{
			Cerebrum.IConnector domain = this.m_Connector;
			
			Cerebrum.Runtime.INativeDomain nd = domain as Cerebrum.Runtime.INativeDomain;

			using(Cerebrum.IContainer sector = nd.GetSector() as Cerebrum.IContainer)
			{
				return Cerebrum.Integrator.Utilites.ObtainComponent(sector, plasma);
				/*using(Cerebrum.IContainer container = sector.GetContainer() as Cerebrum.IContainer)
						{
							return container.ObtainComponent(plasma);
						}*/
			}
		}

		public void UpdateComponent(Cerebrum.ObjectHandle plasma, object component)
		{
			Cerebrum.Runtime.INativeDomain nd = this.m_Connector as Cerebrum.Runtime.INativeDomain;

			using(Cerebrum.IContainer sector = nd.GetSector() as Cerebrum.IContainer)
			{
				Cerebrum.Integrator.Utilites.UpdateComponent(sector, plasma, component);
			}
		}

		public Cerebrum.IComposite AttachConnector(Cerebrum.ObjectHandle plasma)
		{
			return ((Cerebrum.IContainer)this.m_Connector).AttachConnector(plasma);
		}

		public Cerebrum.IComposite CreateConnector(Cerebrum.ObjectHandle plasma, Cerebrum.ObjectHandle typeId)
		{
			return ((Cerebrum.IContainer)this.m_Connector).CreateConnector(plasma, typeId);
		}

		public void RemoveConnector(Cerebrum.ObjectHandle plasma)
		{
			((Cerebrum.IContainer)this.m_Connector).RemoveConnector(plasma);
		}

		public void EngageConnector(Cerebrum.ObjectHandle plasma, Cerebrum.IConnector connector)
		{
			((Cerebrum.IContainer)this.m_Connector).EngageConnector(plasma, connector);
		}

		#endregion

		public Cerebrum.ObjectHandle NextSequence()
		{
			return ((Cerebrum.IWorkspace)this.m_Connector).NextSequence();
		}

		public Cerebrum.Runtime.NativeSector GetSector()
		{
			return ((Cerebrum.Runtime.INativeDomain)this.m_Connector).GetSector() as Cerebrum.Runtime.NativeSector;
		}

		public int BeginTransaction(bool safepoint)
		{
			return ((Cerebrum.IWorkspace)this.m_Connector).Savepoints.Create(safepoint);
		}

		public void CommitTransaction()
		{
			((Cerebrum.IWorkspace)this.m_Connector).Savepoints.Accept();
		}

		public void RollbackTransaction()
		{
			((Cerebrum.IWorkspace)this.m_Connector).Savepoints.Reject();
		}

		public void RollbackTransaction(System.Boolean all)
		{
			Cerebrum.IWorkspace workspace = (Cerebrum.IWorkspace)this.m_Connector;
			if(all)
			{
				while(workspace.Savepoints.Count > 1)
				{
					workspace.Savepoints.Reject();
				}
			}
			else
			{
				workspace.Savepoints.Reject();
			}
		}


		/*public Cerebrum.Runtime.INativeDomainEx GetDomain()
		{
			return this.m_Connector.GetDomain() as Cerebrum.Runtime.INativeDomainEx;
		}*/

		//		/// <summary>
		//		/// ���������� ������ �� �� ������. � ������ ����������
		//		/// ������ �������� �� ���������.
		//		/// </summary>
		//		/// <param name="assemblyId">��� ������ ������</param>
		//		public System.Reflection.Assembly PluggedModule(string assemblyId)
		//		{
		//			if(m_PluggedModules.ContainsKey(assemblyId))
		//			{
		//				return (System.Reflection.Assembly)m_PluggedModules[assemblyId];
		//			}
		//			Cerebrum.Integrator.SystemDatabase.AssembliesRow row = m_SystemDatabase.Assemblies.FindByAssemblyId(assemblyId);
		//			if(row!=null)
		//			{
		//				try
		//				{
		//					System.Reflection.Assembly m = System.Reflection.Assembly.LoadFrom( System.IO.Path.Combine(this.SystemDirectory, row.AssemblyLocation) );
		//					if(m!=null)
		//					{
		//						m_PluggedModules.Add(assemblyId, m);
		//						return m;
		//					}
		//					else
		//					{
		//						m_Application.AddActivityRowResource(m_SystemActivity, SystemActivityEvents.AbsentAssemblyError, "DomainContext.PluggedModule(AssemblyId='" + assemblyId + "')", "FileSystemErrorLoadingAssebly.Message", null);
		//						return null;
		//					}
		//				}
		//				catch(System.Exception ex)
		//				{
		//					m_Application.AddActivityRow(SystemActivityEvents.AbsentAssemblyError, "DomainContext.PluggedModule(AssemblyId='" + assemblyId + "')", ex.Message, m_SystemActivity);
		//					return null;
		//				}
		//			}
		//			else
		//			{
		//				m_Application.AddActivityRowResource(m_SystemActivity, SystemActivityEvents.ConfigAssemblyError, "DomainContext.PluggedModule(AssemblyId='" + assemblyId + "')", "ContextMissingAssemblyIdConfiguration.Message", null);
		//			}
		//			return null;
		//		}
		//
		//		/// <summary>
		//		/// ���������� true � ������ ������� ������.
		//		/// </summary>
		//		/// <param name="assemblyId">��� ������ ������</param>
		//		public bool ExistsPluggedModule(string assemblyId)
		//		{
		//			if(m_PluggedModules.ContainsKey(assemblyId))
		//			{
		//				return true;
		//			}
		//			Cerebrum.Integrator.SystemDatabase.AssembliesRow row = m_SystemDatabase.Assemblies.FindByAssemblyId(assemblyId);
		//			if(row!=null)
		//			{
		//				try
		//				{
		//					if(System.IO.File.Exists( System.IO.Path.Combine(this.SystemDirectory, row.AssemblyLocation) ))
		//						return true;
		//				}
		//				catch
		//				{
		//					return false;
		//				}
		//			}
		//			return false;
		//		}
		//
		//		/// <summary>
		//		/// ������������ ������ ��� ����������� ���������� ���������.
		//		/// �������� Singleton ������� ���������� ���������� �����������
		//		/// �����������. �������� �������� ���������� �� ������� ���������.
		//		/// </summary>
		//		/// <param name="component">�������������� ���������</param>
		//		public void RegisterInstance(Cerebrum.Integrator.IComponent component)
		//		{
		//			if(component.Singleton)
		//			{
		//				m_StaticInstances.Add(component.ComponentId, component);
		//			}
		//
		//			component.DomainContext = this;
		//			component.Cleaning += new System.EventHandler(IComponent_Cleaning);
		//			this.Relaxing += new System.ComponentModel.CancelEventHandler(component.Relax);
		//			this.Cleaning += new System.EventHandler(component.Clean);
		//		}
		//		
		//		/// <summary>
		//		/// ������������ � ��������� ������� �����������
		//		/// </summary>
		//		/// <param name="componentId">��� ������ ����������� ��� ������� �������������� �������</param>
		//		/// <param name="factory">������ �������</param>
		//		public void RegisterFactory(string componentId, Cerebrum.Integrator.IComponentFactory factory)
		//		{
		//			m_Factories.Add(componentId, factory);
		//		}
		//
		//		/// <summary>
		//		/// ���������� ��������� �� ��� ������.
		//		/// � ������ ���� ������ ����� ����� ��������������
		//		/// ����������� �����������, ��� � ������ ����������
		//		/// ���������� �������� ������� ��������� ���������
		//		/// ����� ��� ������.
		//		/// </summary>
		//		/// <param name="componentId">��� ������ ����������</param>
		//		/// <returns>�������� �������������� ������</returns>
		//		public object AcquireComponent(string componentId)
		//		{
		//			if(m_StaticInstances.ContainsKey(componentId))
		//			{
		//				return m_StaticInstances[componentId];
		//			}
		//
		//			if(m_Factories.ContainsKey(componentId))
		//			{
		//				object o = ((Cerebrum.Integrator.IComponentFactory)m_Factories[componentId]).CreateInstance();
		//				if(o!=null)
		//				{
		//					if(typeof(Cerebrum.Integrator.IComponent).IsAssignableFrom(o.GetType()))
		//					{
		//						Cerebrum.Integrator.IComponent ic = (Cerebrum.Integrator.IComponent)o;
		//						ic.ComponentId = componentId;
		//						RegisterInstance(ic);
		//					}
		//				}
		//				return o;
		//			}
		//
		//			Cerebrum.Integrator.SystemDatabase.ComponentsRow row = m_SystemDatabase.Components.FindByComponentId(componentId);
		//			if(row!=null)
		//			{
		//				System.Reflection.Assembly m = PluggedModule(row.AssemblyId);
		//				if(m!=null)
		//				{
		//					object []arguments = null;
		//					if(!row.IsArgumentIdNull())
		//					{
		//						arguments = (this.GetSystemInstance(row.ArgumentId, null) as object []);
		//					}
		//
		//					object o = m.CreateInstance(row.ClassName, false, System.Reflection.BindingFlags.Default, null, arguments, System.Threading.Thread.CurrentThread.CurrentCulture, null);
		//					if(o!=null)
		//					{
		//						if(typeof(Cerebrum.Integrator.IComponent).IsAssignableFrom(o.GetType()))
		//						{
		//							Cerebrum.Integrator.IComponent ic = (Cerebrum.Integrator.IComponent)o;
		//							ic.ComponentId = componentId;
		//							ic.Singleton = row.Singleton;
		//							RegisterInstance(ic);
		//						}
		//					}
		//					else
		//					{
		//						m_Application.AddActivityRowResource(m_SystemActivity, SystemActivityEvents.AbsentComponentError, "DomainContext.AcquireComponent(ComponentId='" + componentId + "')", "ContextInstanceCreationError.Message", null);
		//					}
		//					return o;
		//				}
		//			}
		////			else
		////			{
		////				//������ ������ �������� ���������� �� ���������� �������, ��� ���
		////				//���������� �������������� �������� � ������������ �� ������ ������� �� �������� ���������.
		////				//m_Application.AddActivityRow(SystemActivityEvents.ConfigInstanceError, "DomainContext.AcquireComponent(ComponentId='" + componentId + "')", "���������� ������������ ��� ������� ComponentId", m_SystemActivity);
		////			}
		//			return null;
		//		}
		//
		//		/// <summary>
		//		/// ���������� ���� ���������� �� ��� ������.
		//		/// </summary>
		//		/// <param name="componentId">��� ������ ����������</param>
		//		/// <returns>�������� �������������� ������</returns>
		//		public System.Type TypeOfComponent(string componentId)
		//		{
		//			if(m_StaticInstances.ContainsKey(componentId))
		//			{
		//				return m_StaticInstances[componentId].GetType();
		//			}
		//
		//			/*if(m_Factories.ContainsKey(componentId))
		//			{
		//				object o = ((Cerebrum.Integrator.IComponentFactory)m_Factories[componentId]).CreateInstance();
		//				if(o!=null)
		//				{
		//					if(typeof(Cerebrum.Integrator.IComponent).IsAssignableFrom(o.GetType()))
		//					{
		//						Cerebrum.Integrator.IComponent ic = (Cerebrum.Integrator.IComponent)o;
		//						ic.ComponentId = componentId;
		//						RegisterInstance(ic);
		//					}
		//				}
		//				return o;
		//			}*/
		//
		//			Cerebrum.Integrator.SystemDatabase.ComponentsRow row = m_SystemDatabase.Components.FindByComponentId(componentId);
		//			if(row!=null)
		//			{
		//				System.Reflection.Assembly m = PluggedModule(row.AssemblyId);
		//				if(m!=null)
		//				{
		//					return m.GetType(row.ClassName, false, false);
		//				}
		//			}
		//			return null;
		//		}
		//
		//		/// <summary>
		///// ��������� ������� ������ � �������� ������� ��� ������� ����������
		///// </summary>
		///// <param name="componentId">ALIAS NAME ����������</param>
		///// <returns>true ���� ������ ����������, ����� false</returns>
		//		public bool ExistsComponent(string componentId)
		//		{
		//			if(m_StaticInstances.ContainsKey(componentId))
		//			{
		//				return true;
		//			}
		//
		//			if(m_Factories.ContainsKey(componentId))
		//			{
		//				return true;
		//			}
		//
		//			Cerebrum.Integrator.SystemDatabase.ComponentsRow row = m_SystemDatabase.Components.FindByComponentId(componentId);
		//			if(row!=null)
		//			{
		//				return ExistsPluggedModule(row.AssemblyId);
		//			}
		//			return false;
		//		}
		//
		//		public string[] CategoryComponents(string categoryId)
		//		{
		//			System.Data.DataView dv = new System.Data.DataView(this.m_SystemDatabase.Components);
		//			dv.RowFilter = "CategoryId='" + categoryId + "'";
		//
		//			System.Collections.ArrayList components = new System.Collections.ArrayList();
		//
		//			foreach(System.Data.DataRowView rv in dv)
		//			{
		//				Cerebrum.Integrator.SystemDatabase.ComponentsRow rw = (Cerebrum.Integrator.SystemDatabase.ComponentsRow)(rv.Row);
		//				components.Add(rw.ComponentId);
		//			}
		//			return (string[])components.ToArray(typeof(string));
		//		}
		//
		//
		//		/// <summary>
		//		/// ������� �������, ���������� �� ���������� ������ ����������.
		//		/// �������� ������� ���������� �� ��������� ������� ���������
		//		/// </summary>
		//		private void IComponent_Cleaning(object sender, System.EventArgs e)
		//		{
		//			if(typeof(Cerebrum.Integrator.IComponent).IsAssignableFrom(sender.GetType()))
		//			{
		//				Cerebrum.Integrator.IComponent ic = (Cerebrum.Integrator.IComponent)sender;
		//				ic.Cleaning -= new System.EventHandler(IComponent_Cleaning);
		//				this.Relaxing -= new System.ComponentModel.CancelEventHandler(ic.Relax);
		//				this.Cleaning -= new System.EventHandler(ic.Clean);
		//				if(ic.Singleton) 
		//					m_StaticInstances.Remove(ic.ComponentId);
		//			}
		//		}
		//		/// <summary>
		//		/// �������� ����� �� ��� ALIAS NAME
		//		/// </summary>
		//		/// <param name="methodId">ALIAS NAME ������</param>
		//		/// <param name="args">��������� ������</param>
		//		/// <returns>��������, ������������ �������</returns>
		//		public object InvokeMethod(string methodId, object[] args)
		//		{
		//			Cerebrum.Integrator.SystemDatabase.MethodsRow row = m_SystemDatabase.Methods.FindByMethodId(methodId);
		//			if(row!=null)
		//			{
		//				object o = AcquireComponent(row.ComponentId);
		//				if(o!=null)
		//				{
		//					try
		//					{
		//						return o.GetType().InvokeMember(row.MethodName, System.Reflection.BindingFlags.InvokeMethod, null, o, args);
		//					}
		//					catch(System.Exception ex)
		//					{
		//						m_Application.AddActivityRow(SystemActivityEvents.AbsentMethodError, "DomainContext.InvokeMethod(MethodId='" + methodId + "')", ex.Message, m_SystemActivity);
		//					}
		//				}
		//				else
		//				{
		//					m_Application.AddActivityRowResource(m_SystemActivity, SystemActivityEvents.ConfigComponentError, "DomainContext.InvokeMethod(ComponentId='" + row.ComponentId + "')", "ContextMissingComponentIdConfiguration.Message", null);
		//				}
		//			}
		//			else
		//			{
		//				m_Application.AddActivityRowResource(m_SystemActivity, SystemActivityEvents.ConfigMethodError, "DomainContext.InvokeMethod(MethodId='" + methodId + "')", "ContextMissingMethodIdConfiguration.Message", null);
		//			}
		//			return null;
		//		}
		//
		//		/// <summary>
		//		/// ���������� �� �� ������������ (������� Variables) ��������
		//		/// ����������. � �� ������������ �������� ���������� ��������
		//		/// ��� �������� �����, � ���� base64byte.
		//		/// </summary>
		//		/// <param name="instanceId">�����(��) ���������� ������� �������� �������� ����������</param>
		//		/// <param name="variableId">�����(��) ������������ ����������</param>
		//		/// <returns>�������� ����������</returns>
		//		public object GetSystemVariable(string instanceId, string variableId)
		//		{
		//			return GetSystemVariable(instanceId, variableId, null);
		//		}
		//		public object GetSystemVariable(string instanceId, string variableId, object context)
		//		{
		//			for(int i=this.m_FallbackDatabases.Count-1; i>=0; i--)
		//			{
		//				SystemDatabase database = this.m_FallbackDatabases[i];
		//				Cerebrum.Integrator.SystemDatabase.InstancesRow rwsys = database.Instances.FindByInstanceId(instanceId);
		//				if(rwsys!=null)
		//				{
		//					object o = GetSystemVariable(database, instanceId, variableId, context);
		//					if(o!=null) return o;
		//				}
		//			}
		//			return GetSystemVariable(m_SystemDatabase, instanceId, variableId, context);
		//		}
		//
		//		protected object GetSystemVariable(SystemDatabase database, string instanceId, string variableId, object context)
		//		{
		//			Cerebrum.Integrator.SystemDatabase.VariablesRow rwvar = database.Variables.FindByInstanceIdVariableId(instanceId, variableId);
		//			if(rwvar!=null && !rwvar.IsResourceIdNull()) 
		//			{
		//				return this.GetSystemInstance(rwvar.ResourceId, context);
		//			}
		//			return null;
		//		}
		//
		//
		//		/// <summary>
		//		/// ���������� �� �� ������������ (������� Resources) ��������
		//		/// ��������. � �� ������������ �������� �������� ��������
		//		/// ��� �������� �����, � ���� base64byte.
		//		/// </summary>
		//		/// <param name="instanceId">�����(��) ������������ ����������</param>
		//		/// <returns>�������� ����������</returns>
		//		public object GetSystemInstance(string instanceId)
		//		{
		//			return GetSystemInstance(instanceId, null);
		//		}
		//		public object GetSystemInstance(string instanceId, object context)
		//		{
		//			for(int i=this.m_FallbackDatabases.Count-1; i>=0; i--)
		//			{
		//				SystemDatabase database = this.m_FallbackDatabases[i];
		//				object o = GetSystemInstance(database, instanceId, false, context);
		//				if(o!=null) return o;
		//			}
		//			return GetSystemInstance(m_SystemDatabase, instanceId, true, context);
		//		}
		//
		//		public object GetSystemInstance(SystemDatabase database, string instanceId, bool system, object context)
		//		{
		//			Cerebrum.Integrator.SystemDatabase.InstancesRow rwsys = database.Instances.FindByInstanceId(instanceId);
		//			if(rwsys!=null || system)
		//			{
		//				Cerebrum.Integrator.RuntimeDatabase.InstancesRow rwrun = rwsys as Cerebrum.Integrator.RuntimeDatabase.InstancesRow;
		//				if(rwrun!=null && !rwrun.IsRuntimeObjectNull())
		//				{
		//					object o = rwrun.RuntimeObject;
		//					ISerializable s = o as ISerializable;
		//					if(s!=null)
		//					{
		//						s.Serialize(context, SerializeOperation.Take);
		//					}
		//					return o;
		//				}
		//				else if(rwsys!=null && !rwsys.IsComponentIdNull())
		//				{
		//					object o = this.AcquireComponent(rwsys.ComponentId);
		//					ISerializable s = o as ISerializable;
		//					if(s!=null)
		//					{
		//						s.InstanceId = rwsys.InstanceId;
		//						s.Cleaning += new EventHandler(this.InstanceCleaning);
		//						s.Serialize(context, SerializeOperation.Load);
		//					}
		//					if(rwrun!=null) rwrun.RuntimeObject = o;
		//					return o;
		//				}
		//				else
		//				{
		//					Cerebrum.Integrator.SystemDatabase.ResourcesRow rw = database.Resources.FindByResourceId(instanceId);
		//					if(rw!=null && !rw.IsEncodedValueNull()) 
		//					{
		//						System.IO.MemoryStream ms = new System.IO.MemoryStream(rw.EncodedValue);
		//
		//						System.Runtime.Serialization.IFormatter formatter;
		//						formatter = (System.Runtime.Serialization.IFormatter) new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
		//
		//						object o = formatter.Deserialize(ms);
		//						ms.Close();
		//						return o;
		//					}
		//				}
		//			}
		//
		//			return null;
		//		}
		//
		//		
		//		private void InstanceCleaning(object sender, System.EventArgs e)
		//		{
		//			ISerializable s = sender as ISerializable;
		//			string instanceId = s.InstanceId;
		//
		//			if(s!=null)
		//			{
		//				Cerebrum.Integrator.RuntimeDatabase.InstancesRow rwrun;
		//				s.Cleaning -= new EventHandler(InstanceCleaning);
		//				for(int i=this.m_FallbackDatabases.Count-1; i>=0; i--)
		//				{
		//					rwrun = this.m_FallbackDatabases[i].Instances.FindByInstanceId(instanceId) as Cerebrum.Integrator.RuntimeDatabase.InstancesRow;
		//					if(rwrun!=null && !rwrun.IsRuntimeObjectNull())
		//					{
		//						if(s==rwrun.RuntimeObject)
		//						{
		//							rwrun.SetRuntimeObjectNull();
		//							return;
		//						}
		//					}
		//				}
		//				rwrun = m_SystemDatabase.Instances.FindByInstanceId(instanceId) as Cerebrum.Integrator.RuntimeDatabase.InstancesRow;
		//				if(rwrun!=null && !rwrun.IsRuntimeObjectNull())
		//				{
		//					if(s==rwrun.RuntimeObject)
		//					{
		//						rwrun.SetRuntimeObjectNull();
		//						return;
		//					}
		//				}
		//			}
		//		}
		//
		//		/// <summary>
		//		/// ������������� � �� ������������ (������� Variables) ��������
		//		/// ����������. � �� ������������ �������� ���������� ��������
		//		/// ��� �������� �����, � ���� base64byte.
		//		/// </summary>
		//		/// <param name="instanceId">�����(��) ���������� ������� ������������� �������� ����������</param>
		//		/// <param name="variableId">�����(��) ��������������� ����������</param>
		//		public void SetSystemVariable(string instanceId, string variableId, object value)
		//		{
		//			SetSystemVariable(instanceId, variableId, value, null);
		//		}
		//		public void SetSystemVariable(string instanceId, string variableId, object value, object context)
		//		{
		//			for(int i=this.m_FallbackDatabases.Count-1; i>=0; i--)
		//			{
		//				SystemDatabase database = this.m_FallbackDatabases[i];
		//				if(database.Instances.FindByInstanceId(instanceId)!=null)
		//				{
		//					SetSystemVariable(database, instanceId, variableId, value, false, context);
		//					return;
		//				}
		//			}
		//			SetSystemVariable(m_SystemDatabase, instanceId, variableId, value, true, context);
		//		}
		//		protected void SetSystemVariable(SystemDatabase database, string instanceId, string variableId, object value, bool system, object context)
		//		{
		//
		//			if(value==null)
		//			{
		//				DelSystemVariable(database, instanceId, variableId, context);
		//			}
		//			else
		//			{
		//				Cerebrum.Integrator.SystemDatabase.VariablesRow rw = database.Variables.FindByInstanceIdVariableId(instanceId, variableId);
		//				if(rw==null) 
		//				{
		//					rw = database.Variables.NewVariablesRow();
		//					rw.InstanceId = instanceId;
		//					rw.VariableId = variableId;
		//					database.Variables.AddVariablesRow(rw);
		//				}
		//			
		//				bool newResId = false;
		//				string strResId = "";
		//
		//				Cerebrum.Integrator.ISerializable s = value as Cerebrum.Integrator.ISerializable;
		//				if(s!=null)
		//				{
		//					newResId = true;
		//					strResId = s.InstanceId;
		//
		//					Cerebrum.Integrator.SystemDatabase.InstancesRow rwins = null;
		//					for(int i=this.m_FallbackDatabases.Count-1; i>=0; i--)
		//					{
		//						SystemDatabase dbtemp = this.m_FallbackDatabases[i];
		//						rwins = dbtemp.Instances.FindByInstanceId(strResId);
		//						if(rwins!=null)
		//						{
		//							break;
		//						}
		//					}
		//					if(rwins==null && database!=m_SystemDatabase)
		//					{
		//						rwins = database.Instances.NewInstancesRow();
		//						rwins.InstanceId = strResId;
		//						database.Instances.AddInstancesRow(rwins);
		//					}
		//					if(rwins!=null)
		//					{
		//						rwins.ComponentId = s.ComponentId;
		//						Cerebrum.Integrator.RuntimeDatabase.InstancesRow rwrun = rwins as Cerebrum.Integrator.RuntimeDatabase.InstancesRow;
		//						if(rwrun!=null) rwrun.RuntimeObject = s;
		//						s.Serialize(context, SerializeOperation.Save);
		//					}
		//				}
		//				else
		//				{
		//
		//					if(rw.IsResourceIdNull() || rw.ResourceId.Length < 1)
		//					{
		//						string guid = System.Guid.NewGuid().ToString().ToUpper();
		//						if(!system)
		//						{
		//							Cerebrum.Integrator.SystemDatabase.InstancesRow rwins = database.Instances.NewInstancesRow();
		//							rwins.InstanceId = guid;
		//							database.Instances.AddInstancesRow(rwins);
		//						}
		//
		//						newResId = true;
		//						strResId = guid;
		//					}
		//					else
		//					{
		//						strResId = rw.ResourceId;
		//					}
		//					SetSystemResource(strResId, value);
		//				}
		//				if(newResId)
		//				{
		//					if(!rw.IsResourceIdNull() && rw.ResourceId != strResId)
		//					{
		//						Cerebrum.Integrator.SystemDatabase.InstancesRow rwi = database.Instances.FindByInstanceId(instanceId);
		//						if(rwi!=null && rwi.IsComponentIdNull())
		//						{
		//							DelSystemResource(database, rw.ResourceId);
		//						}
		//					}
		//
		//					rw.ResourceId = strResId;
		//				}
		//			}
		//		}
		//
		//		public object NewSystemInstance(string componentId, object context)
		//		{
		//			object o = this.AcquireComponent(componentId);
		//			string guid = System.Guid.NewGuid().ToString().ToUpper();
		//			
		//			ISerializable s = o as ISerializable;
		//			if(s!=null)
		//			{
		//				s.InstanceId = guid;
		//			}
		//			
		//			SystemDatabase database;
		//			int dbcnt = this.m_FallbackDatabases.Count;
		//			if(dbcnt>0)
		//			{
		//				database = this.m_FallbackDatabases[dbcnt-1];
		//			}
		//			else
		//			{
		//				database = m_SystemDatabase;
		//			}
		//
		//			this.SetSystemInstance(database, guid, componentId, o);
		//
		//			if(s!=null)
		//			{
		//				s.Serialize(context, Cerebrum.Integrator.SerializeOperation.Init);
		//			}
		//
		//			return o;
		//		}
		//
		//		protected void SetSystemInstance(SystemDatabase database, string instanceId, string componentId, object value)
		//		{
		//			Cerebrum.Integrator.SystemDatabase.InstancesRow rwins = null;
		//			rwins = database.Instances.FindByInstanceId(instanceId);
		//			
		//			if(value==null)
		//			{
		//				if(rwins!=null)	rwins.Delete();
		//			}
		//			else
		//			{
		//
		//				if(rwins==null && database!=m_SystemDatabase)
		//				{
		//					rwins = database.Instances.NewInstancesRow();
		//					rwins.InstanceId = instanceId;
		//					rwins.ComponentId = componentId;
		//					database.Instances.AddInstancesRow(rwins);
		//				}
		//				else if(rwins!=null)
		//				{
		//					rwins.InstanceId = instanceId;
		//					if(componentId==null || componentId.Length < 1)
		//					{
		//						rwins.SetComponentIdNull();
		//					}
		//					else
		//					{
		//						rwins.ComponentId = componentId;
		//					}
		//				}
		//
		//				if(rwins!=null)
		//				{
		//					Cerebrum.Integrator.RuntimeDatabase.InstancesRow rwrun = rwins as Cerebrum.Integrator.RuntimeDatabase.InstancesRow;
		//					if(rwrun!=null) rwrun.RuntimeObject = value;
		//				}
		//			}
		//		}
		//
		//		/// <summary>
		//		/// ������������� � �� ������������ (������� Resources) ��������
		//		/// ��������. � �� ������������ �������� �������� ��������
		//		/// ��� �������� �����, � ���� base64byte.
		//		/// </summary>
		//		/// <param name="resourceId">�����(��) ��������������� ����������</param>
		//		public void SetSystemResource(string resourceId, object value)
		//		{
		//			for(int i=this.m_FallbackDatabases.Count-1; i>=0; i--)
		//			{
		//				SystemDatabase database = this.m_FallbackDatabases[i];
		//				if(database.Instances.FindByInstanceId(resourceId)!=null)
		//				{
		//					SetSystemResource(database, resourceId, value);
		//					return;
		//				}
		//			}
		//			SetSystemResource(m_SystemDatabase, resourceId, value);
		//		}
		//		protected static void SetSystemResource(SystemDatabase database, string resourceId, object value)
		//		{
		//			Cerebrum.Integrator.SystemDatabase.ResourcesRow rw = database.Resources.FindByResourceId(resourceId);
		//			if(rw==null) 
		//			{
		//				rw = database.Resources.NewResourcesRow();
		//				rw.ResourceId = resourceId;
		//				database.Resources.AddResourcesRow(rw);
		//			}
		//			
		//			if(value!=null)
		//			{
		//				System.IO.MemoryStream ms = new System.IO.MemoryStream();
		//
		//				System.Runtime.Serialization.IFormatter formatter;
		//				formatter = (System.Runtime.Serialization.IFormatter) new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
		//        
		//				formatter.Serialize(ms, value);
		//				rw.EncodedValue = ms.GetBuffer();
		//				ms.Close();
		//			}
		//			else
		//			{
		//				rw.SetEncodedValueNull();
		//			}
		//		}
		//
		//		/// <summary>
		//		/// ������� �� �� ������������ (������� Variables) ��������
		//		/// ����������. � �� ������������ �������� ���������� ��������
		//		/// ��� �������� �����, � ���� base64byte.
		//		/// </summary>
		//		/// <param name="instanceId">�����(��) ���������� ������� ������� �������� ����������</param>
		//		/// <param name="variableId">�����(��) ��������� ����������</param>
		//		public void DelSystemVariable(string instanceId, string variableId, object context)
		//		{
		//			for(int i=this.m_FallbackDatabases.Count-1; i>=0; i--)
		//			{
		//				SystemDatabase database = this.m_FallbackDatabases[i];
		//				if(database.Instances.FindByInstanceId(instanceId)!=null)
		//				{
		//					DelSystemVariable(database, instanceId, variableId, context);
		//					return;
		//				}
		//			}
		//			DelSystemVariable(m_SystemDatabase, instanceId, variableId, context);
		//		}
		//		protected void DelSystemVariable(SystemDatabase database, string instanceId, string variableId, object context)
		//		{
		//			Cerebrum.Integrator.SystemDatabase.VariablesRow rw = database.Variables.FindByInstanceIdVariableId(instanceId, variableId);
		//			if(rw!=null) 
		//			{
		//				if(!rw.IsResourceIdNull() && rw.ResourceId.Length > 0)
		//				{
		//					DelSystemResource(rw.ResourceId, context);
		//				}
		//				rw.Delete();
		//			}
		//		}
		//		/// <summary>
		//		/// ������� �� �� ������������ (������� Resources) ��������
		//		/// ��������. � �� ������������ �������� �������� ��������
		//		/// ��� �������� �����, � ���� base64byte.
		//		/// </summary>
		//		/// <param name="instanceId">�����(��) ���������� ������� ������� �������� ����������</param>
		//		public void DelSystemInstance(string instanceId, object context)
		//		{
		//			for(int i=this.m_FallbackDatabases.Count-1; i>=0; i--)
		//			{
		//				SystemDatabase database = this.m_FallbackDatabases[i];
		//				Cerebrum.Integrator.SystemDatabase.InstancesRow rwsys = database.Instances.FindByInstanceId(instanceId);
		//				if(rwsys!=null)
		//				{
		//					Cerebrum.Integrator.RuntimeDatabase.InstancesRow rwrun = rwsys as Cerebrum.Integrator.RuntimeDatabase.InstancesRow;
		//					if(rwrun!=null && !rwrun.IsRuntimeObjectNull())
		//					{
		//						object o = rwrun.RuntimeObject;
		//						ISerializable s = o as ISerializable;
		//						if(s!=null)
		//						{
		//							s.Serialize(context, SerializeOperation.Kill);
		//						}
		//						else
		//						{
		//							IReleasable ir = o as IReleasable;
		//							if(ir!=null) ir.Clean(this, System.EventArgs.Empty);
		//						}
		//						rwrun.RuntimeObject = Convert.DBNull;
		//					}
		//					rwsys.Delete();
		//
		//					System.Data.DataView view = new System.Data.DataView(database.Variables, "InstanceId='" + instanceId + "'", "", System.Data.DataViewRowState.CurrentRows);
		//					foreach(System.Data.DataRowView row in view)
		//					{
		//						Cerebrum.Integrator.SystemDatabase.VariablesRow rw = row.Row as Cerebrum.Integrator.SystemDatabase.VariablesRow;
		//						if(!rw.IsResourceIdNull())
		//							DelSystemResource(database, rw.ResourceId);
		//						rw.Delete();
		//					}
		//					DelSystemResource(database, instanceId);
		//					return;
		//				}
		//			}
		//			DelSystemResource(m_SystemDatabase, instanceId);
		//		}
		//		public void DelSystemResource(string resourceId, object context)
		//		{
		//			for(int i=this.m_FallbackDatabases.Count-1; i>=0; i--)
		//			{
		//				SystemDatabase database = this.m_FallbackDatabases[i];
		//				Cerebrum.Integrator.SystemDatabase.InstancesRow rwsys = database.Instances.FindByInstanceId(resourceId);
		//				if(rwsys!=null)
		//				{
		//					Cerebrum.Integrator.RuntimeDatabase.InstancesRow rwrun = rwsys as Cerebrum.Integrator.RuntimeDatabase.InstancesRow;
		//					if(rwrun!=null && !rwrun.IsRuntimeObjectNull())
		//					{
		//						object o = rwrun.RuntimeObject;
		//						ISerializable s = o as ISerializable;
		//						if(s!=null)
		//						{
		//							s.Serialize(context, SerializeOperation.Drop);
		//						}
		//					}
		//					DelSystemResource(database, resourceId);
		//					return;
		//				}
		//			}
		//			DelSystemResource(m_SystemDatabase, resourceId);
		//		}
		//		protected static void DelSystemResource(SystemDatabase database, string resourceId)
		//		{
		//			Cerebrum.Integrator.SystemDatabase.ResourcesRow rw = database.Resources.FindByResourceId(resourceId);
		//			if(rw!=null) 
		//			{
		//				rw.Delete();
		//			}
		//		}
		//		/// <summary>
		//		/// ���������� ���������� ������ �� �� ������
		//		/// </summary>
		//		/// <param name="variableId">�����(��) ������������ ����������</param>
		//		/// <returns>�������� ����������</returns>
		//		public object GetSessionVariable(string variableId)
		//		{
		//			if(m_SessionVariables.ContainsKey(variableId))
		//			{
		//				return m_SessionVariables[variableId];
		//			}
		//			else
		//				return null;
		//		}
		//		/// <summary>
		//		/// ������������� �������� ���������� ������ �� �� ������
		//		/// </summary>
		//		/// <param name="variableId">�����(��) ��������������� ����������</param>
		//		public void SetSessionVariable(string variableId, object value)
		//		{
		//			m_SessionVariables[variableId] = value;
		//		}
		//		/// <summary>
		//		/// ������� ���������� �� ������ �� �� ������
		//		/// </summary>
		//		/// <param name="variableId">�����(��) ��������� ����������</param>
		//		public void DelSessionVariable(string variableId)
		//		{
		//			if(m_SessionVariables.ContainsKey(variableId))
		//			{
		//				m_SessionVariables.Remove(variableId);
		//			}
		//		}
		//
		//
		//		/// <summary>
		//		/// ���������� ������ Clear ���������� IReleasable
		//		/// </summary>
		//		public void Clean(object sender, System.EventArgs e)
		//		{
		//			if(Cleaning!=null)Cleaning(this, e);
		//			m_Application.Relaxing -= new System.ComponentModel.CancelEventHandler(this.Relax);
		//			m_Application.Cleaning -= new System.EventHandler(this.Clean);
		//			m_Application = null;
		//		}
		//
		//		/// <summary>
		//		/// ���������� ������ Relax ���������� IReleasable
		//		/// </summary>
		//		public void Relax(object sender, System.ComponentModel.CancelEventArgs e)
		//		{
		//			if(Relaxing!=null)Relaxing(this, e);
		//		}
		//
		//		/// <summary>
		//		/// ���������� ������� Clearing ���������� IReleasable
		//		/// </summary>
		//		public event System.EventHandler Cleaning;
		//
		//		/// <summary>
		//		/// ���������� ������� Relaxing ���������� IReleasable
		//		/// </summary>
		//		public event System.ComponentModel.CancelEventHandler Relaxing;
		//        
		//
		//		protected SystemDatabaseCollection m_FallbackDatabases;
		//		public SystemDatabaseCollection FallbackDatabases
		//		{
		//			get
		//			{
		//				return m_FallbackDatabases;
		//			}
		//		}
	}
//
//	public class SystemDatabaseCollection: Cerebrum.Collections.AbstractContainer
//	{
//
//		public Cerebrum.Integrator.SystemDatabase this[ int index ]  
//		{
//			get  
//			{
//				return( (Cerebrum.Integrator.SystemDatabase) (this as System.Collections.IList)[index] );
//			}
//			set  
//			{
//				(this as System.Collections.IList)[index] = value;
//			}
//		}
//
//		public int Add( Cerebrum.Integrator.SystemDatabase value )  
//		{
//			return( (this as System.Collections.IList).Add( value ) );
//		}
//
//		public virtual void AddRange(Cerebrum.Integrator.SystemDatabase[] controls)
//		{
//			int num1;
//			if (controls == null)
//			{
//				throw new ArgumentNullException("controls"); 
//			}
//			if (controls.Length > 0)
//			{
//				for (num1 = 0; (num1 < controls.Length); num1 = (num1 + 1))
//				{
//					this.Add(controls[num1]);
// 
//				}
//				//this.m_Owner.ResumeLayout(true);
//			}
//		}
//
//		public int IndexOf( Cerebrum.Integrator.SystemDatabase value )  
//		{
//			return( (this as System.Collections.IList).IndexOf( value ) );
//		}
//
//		public void Insert( int index, Cerebrum.Integrator.SystemDatabase value )  
//		{
//			(this as System.Collections.IList).Insert( index, value );
//		}
//
//		public void Remove( Cerebrum.Integrator.SystemDatabase value )  
//		{
//			(this as System.Collections.IList).Remove( value );
//		}
//
//		public void RemoveAt(int index)
//		{
//			this.OnRemove(index, this[index]);
// 
//		}
//
//		public bool Contains( Cerebrum.Integrator.SystemDatabase value )  
//		{
//			// If value is not of type Cerebrum.Integrator.SystemDatabase, this will return false.
//			return( (this as System.Collections.IList).Contains( value ) );
//		}
//
//		protected override bool IsReadOnly()
//		{
//			return false;
//		}
//
//		protected override void OnClear()
//		{
//			base.ClearItems();
//		}
//
//		protected override void OnInsert(int index, object value)
//		{
//			base.InsertItem (index, value);
//		}
//
//
//		protected override void OnRemove(int index, object value)
//		{
//			base.RemoveItem (index);
//		}
//
//
//		protected override void OnUpdate(int index, object value)
//		{
//			int i = base.SearchItem(value);
//			if(i<0)
//			{
//				this.OnInsert(index, value);
//			}
//			else
//			{
//				base.MoveItem(i, index);
//			}
//		}
//
//		protected override bool Validate(object value, bool throwException)
//		{
//			if ( !typeof(Cerebrum.Integrator.SystemDatabase).IsAssignableFrom(value.GetType()) )
//			{
//				if(throwException)
//				{
//					throw new ArgumentException( "value must be inherited from type Cerebrum.Integrator.SystemDatabase", "value" );
//				}
//				else
//				{
//					return false;
//				}
//			}
//			return true;
//		}
//
//	}
}
