// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator
{
	/// <summary>
	/// Summary description for Activator.
	/// </summary>
	public class Activator : Cerebrum.IActivator
	{
		public Activator()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		/*#region IObjectActivator Members

		public Cerebrum.ObjectHandle CreateTypeRecord(Cerebrum.IConnector domain, Type type)
		{
			// TODO:  Add Activator.CreateTypeRecord implementation
			return new Cerebrum.ObjectHandle ();
		}

		#endregion*/

		#region IActivator Members

		void Cerebrum.IActivator.CreateInstance(Cerebrum.IConnector domain, Cerebrum.ObjectHandle typeID, object [] args, ref object instance, ref Cerebrum.ObjectHandle classType)
		{
			if(((System.IComparable)typeID).CompareTo(Cerebrum.ObjectHandle.Null) > 0)
			{
				if(typeID==Cerebrum.Specialized.Concepts.WardenNodeTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = null;
				}
				else if(typeID==Cerebrum.Specialized.Concepts.ScalarNodeTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Scalar;
					instance = null;
				}
				else if(typeID==Cerebrum.Specialized.Concepts.AttributeDescriptorTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = new Cerebrum.Reflection.AttributeDescriptor();
				}
				else if(typeID==Cerebrum.Specialized.Concepts.TypeDescriptorTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = new Cerebrum.Reflection.TypeDescriptor();
				}
				else if(typeID==Cerebrum.Specialized.Concepts.TableDescriptorTypeId)
				{
					classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
					instance = new Cerebrum.Reflection.TableDescriptor();
				}
				else
				{
					Cerebrum.Runtime.INativeDomain nd = (Cerebrum.Runtime.INativeDomain)domain;

					using(Cerebrum.IContainer sector = nd.GetSector())
					{
						using(Cerebrum.IConnector ctype = sector.AttachConnector(typeID))
						{
							if(ctype!=null)
							{
								Cerebrum.Reflection.TypeDescriptor tds = (Cerebrum.Reflection.TypeDescriptor)ctype.Component;
								if(tds!=null)
								{
									classType = tds.KernelObjectKindId;
									if(classType==Cerebrum.Runtime.KernelObjectKindId.Scalar)
									{
										instance = null;
									}
									else
									{
										System.Type componentType = tds.ComponentType;
										if(componentType!=null)
										{
											if(componentType.IsAbstract || componentType.IsArray || System.Object.ReferenceEquals(componentType, typeof(System.Object)))
											{
												instance = null;
											}
											else
											{
												if(args==null)
												{
													instance = System.Activator.CreateInstance(componentType);
												}
												else
												{
													instance = System.Activator.CreateInstance(componentType, args);
												}
											}
										}
										else
										{
											throw new System.Exception("Could not create component of type='" + tds.QualifiedTypeName + "', typeId='" + typeID.ToString() + "'");
										}
									}
								}
								else
								{
									throw new System.Exception("TypeId='" + typeID.ToString() + "' is not a type descritor");
								}
							}
							else
							{
								throw new System.Exception("Could not find node for typeId='" + typeID.ToString() + "'");
							}
						}
					}
				}
			}
			else
			{
				classType = Cerebrum.Runtime.KernelObjectKindId.Warden;
				instance = null;
			}
		}

		#endregion
	}
}
