﻿// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator
{

    /// <summary>
    /// Реализация интерфейса IContextProperties
    /// Позволяет в DomainContext хранить список обьектов, относящихся к этому DomainContext
    /// </summary>
    public class ContextProperties : IContextProperties
    {
        private System.Collections.Hashtable hashTbl;
        private Cerebrum.Integrator.DomainContext dc;

        public ContextProperties(Cerebrum.Integrator.DomainContext domainContext)
        {
            dc = domainContext;
            hashTbl = new System.Collections.Hashtable();
        }

        /// <summary>
        /// Добавляет, изменяет или возвращает объект-свойство по ключу.
        /// </summary>
        /// <param name="key">Заданный ключ.</param>
        /// <returns>Возвращаемый объект-свойство.</returns>
        public object this[object key]
        {
            set
            {
                if (hashTbl.ContainsKey(key) == false)
                {
                    hashTbl.Add(key, value);
                }
                else
                {
                    hashTbl[key] = value;
                }
            }
            get
            {
                if (hashTbl.ContainsKey(key) == false)
                {
                    return null;
                }
                else
                {
                    return hashTbl[key];
                }
            }
        }

        /// <summary>
        /// Проверяет существование ключа.
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>true - если такой ключ существует</returns>
        public bool ContainsProperty(object key)
        {
            return hashTbl.ContainsKey(key);
        }

        /// <summary>
        /// Возвращает Cerebrum.Integrator.DomainContext
        /// </summary>
        public Cerebrum.Integrator.DomainContext DomainContext
        {
            get
            {
                return dc;
            }
        }
    }
}
