// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data
{
	/// <summary>
	/// Summary description for TableViewInternal.
	/// </summary>
	internal class TableViewInternal : Cerebrum.Integrator.GenericComponent 
	{
		internal System.ComponentModel.PropertyDescriptorCollection m_Attributes;// = new System.ComponentModel.PropertyDescriptorCollection(new System.ComponentModel.PropertyDescriptor[0]);
		internal Cerebrum.Reflection.TableDescriptor m_Table;
		
		public TableViewInternal(Cerebrum.Reflection.TableDescriptor table) 
		{
			using(Cerebrum.Runtime.NativeWarden columns = table.GetSelectedAttributesVector()/*, cv = table.GetSelectedComponentsVector()*/)
			{
				m_Attributes = Cerebrum.Data.SimpleView.DescriptorsFromVector(columns);
			}
			m_Table = table;
		}

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_Attributes!=null)
				{
					foreach(object o in m_Attributes)
					{
						System.IDisposable d = o as System.IDisposable;
						if(d!=null) d.Dispose();
					}
					m_Attributes = null;
				}
			}
			base.Dispose (disposing);
		}

	}
}
