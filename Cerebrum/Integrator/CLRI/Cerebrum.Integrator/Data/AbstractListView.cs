// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data
{
	/// <summary>
	/// Summary description for AbstractListView.
	/// </summary>
//	public abstract class AbstractListView : Cerebrum.DisposableObject, System.ComponentModel.IBindingList, System.ComponentModel.ISupportInitialize, System.ComponentModel.ITypedList
//	{
//		protected internal abstract Cerebrum.IContainer GetContainer();
//		protected internal abstract Cerebrum.Runtime.IDictionary GetDictionary();
//		
//		protected System.ComponentModel.PropertyDescriptorCollection m_Attributes;// = new System.ComponentModel.PropertyDescriptorCollection(new System.ComponentModel.PropertyDescriptor[0]);
//		
//
//		public AbstractListView(System.ComponentModel.PropertyDescriptorCollection attributes)
//		{
//			m_Attributes = attributes;
//		}
//
//
//		public static System.ComponentModel.PropertyDescriptorCollection DescriptorsFromVector(Cerebrum.IContainer warden, Cerebrum.Runtime.NativeVector columns)
//		{
//			System.Collections.ArrayList list = new System.Collections.ArrayList();
//			foreach(System.Collections.DictionaryEntry de in columns)
//			{
//				Cerebrum.Reflection.AttributeDescriptor dsc;
//				using(Cerebrum.IConnector c0 = warden.AttachConnector((Cerebrum.ObjectHandle)de.Value))
//				{
//					dsc = (Cerebrum.Reflection.AttributeDescriptor)c0.Component;
//					
//					if(dsc.ObjectHandle==Cerebrum.Specialized.Concepts.PlasmaHandleAttribute)
//					{
//						list.Add(new Data.Specialized.HandleAttributePropertyDescriptor(dsc));
//					}
//					else if(dsc.ObjectHandle==Cerebrum.Specialized.Concepts.SelectedAttributesAttribute)
//					{
//						list.Add(new Data.Specialized.VectorAttributePropertyDescriptor(dsc));
//					}
//					else if(dsc.ObjectHandle==Cerebrum.Specialized.Concepts.SelectedComponentsAttribute)
//					{
//						list.Add(new Data.Specialized.VectorAttributePropertyDescriptor(dsc));
//					}
//					else
//					{
//						list.Add(new Data.Specialized.ScalarAttributePropertyDescriptor(dsc));
//					}
//				}
//			}
//			return new System.ComponentModel.PropertyDescriptorCollection((System.ComponentModel.PropertyDescriptor[])list.ToArray(typeof(System.ComponentModel.PropertyDescriptor)));
//		}
//
//
//		private int Add(object value)
//		{
//			return -1;
//		}
//
//
//		public int Add(Cerebrum.ObjectHandle handle)
//		{
//			int i = this.Count;
//			m_Components.SetMap(new Cerebrum.ObjectHandle(i+1), handle);
//			return i;
//		}
//
//		public Cerebrum.ObjectHandle GetMap(int index)
//		{
//			return m_Components.GetMap(new Cerebrum.ObjectHandle(index+1));
//		}
//
//		public void SetMap(int index, Cerebrum.ObjectHandle handle)
//		{
//			m_Components.SetMap(new Cerebrum.ObjectHandle(index+1), handle);
//		}
//
//		public int Add(Cerebrum.IConnector connector)
//		{
//			return this.Add(connector.PlasmaHandle);
//		}
//
//		int System.Collections.IList.Add(object value)
//		{
//			return this.Add(value);
//		}
//
//
//		int _suspendCount = 0;
//		public void SuspendEvents()
//		{
//			_suspendCount ++;
//		}
//		public void ResumeEvents()
//		{
//			_suspendCount --;
//			if(_suspendCount == 0)
//			{
//				Reset();
//			}
//		}
//
//		public void Clear()
//		{
//			Reset();
//		}
//		public void Reset()
//		{
//			this.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.Reset, -1));
//		}
//
//		protected void OnListChanged(System.ComponentModel.ListChangedEventArgs e)
//		{
//			if(_suspendCount == 0 && this.ListChanged!=null)
//			{
//				this.ListChanged(this, e);
//			}
//		}
//
//		public bool Contains(object value)
//		{
//			return false;
//		}
//
//		public void CopyTo(System.Array array, int index)
//		{
//			//Rows.CopyTo(array, index);
//		}
//
//		public virtual int Count
//		{
//			get
//			{
//				return m_Components.Count;
//				//			m_ComponentsCount = m_Components.GetMap(new Cerebrum.ObjectHandle(0)).HandleIntPtr.ToInt32();
//			}
//		}
//
//		public System.Collections.IEnumerator GetEnumerator()
//		{
//			return null;//Rows.GetEnumerator();
//		}
//
//		private int IndexOf(object value)
//		{
//			return -1;//Rows.IndexOf(value);
//		}
//
//		int System.Collections.IList.IndexOf(object value)
//		{
//			return -1;//Rows.IndexOf(value);
//		}
//
//		/*public Cerebrum.ObjectHandle IndexOf(Cerebrum.ObjectHandle handle)
//		{
//			foreach(System.Collections.DictionaryEntry de in m_Components)
//			{
//				if((Cerebrum.ObjectHandle)de.Value==handle)
//				{
//					return (Cerebrum.ObjectHandle)de.Key;
//				}
//			}
//			return Cerebrum.ObjectHandle.Null;
//		}*/
//
//		public void Insert(int index, object value)
//		{
//			throw new System.NotSupportedException();
//		}
//
//		public bool IsSynchronized
//		{
//			get
//			{
//				return false;//Rows.IsSynchronized;
//			}
//		}
// 
//
//		public virtual object this[int index]
//		{
//			get
//			{
//				Cerebrum.ObjectHandle h = GetMap(index);
//				return new ComponentView(m_Container.AttachConnector(h), this, index);
//			}//			set
//			{
//				/*Rows[index] = value;
//				if(_suspendCount == 0 && this.ListChanged!=null)
//				{
//					this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemChanged, index));
//				}*/
//			}//
//		}
//
//		public void Remove(object value)
//		{
//			/*int i = Rows.IndexOf(value);
//			if(i>=0)
//			{
//				this.RemoveAt(i);
//			}*/
//		}
//
//		public void RemoveAt(int index)
//		{
//			/*Rows.RemoveAt(index);
//			if(_suspendCount == 0 && this.ListChanged!=null)
//			{
//				this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemDeleted, index));
//			}*/
//		}
//
//		public int Find(System.ComponentModel.PropertyDescriptor property, object key)
//		{
//			/*int i;
//			System.Collections.IList s = Rows;
//			for (i=0; i < s.Count; i++)
//			{
//				if (key.Equals(property.GetValue(s[i])))
//				{
//					return i;
//				}
//
//			}*/
//
//			return -1;
//		}
//
//		#region IBindingList Members
//
//		public void AddIndex(System.ComponentModel.PropertyDescriptor property)
//		{
//			// TODO:  Add DataView.AddIndex implementation
//		}
//
//		public virtual bool AllowNew
//		{
//			get
//			{
//				// TODO:  Add DataView.AllowNew getter implementation
//				return false;
//			}
//		}
//
//		public void ApplySort(System.ComponentModel.PropertyDescriptor property, System.ComponentModel.ListSortDirection direction)
//		{
//			// TODO:  Add DataView.ApplySort implementation
//		}
//
//		public System.ComponentModel.PropertyDescriptor SortProperty
//		{
//			get
//			{
//				// TODO:  Add DataView.SortProperty getter implementation
//				return null;
//			}
//		}
//
//		public bool SupportsSorting
//		{
//			get
//			{
//				// TODO:  Add DataView.SupportsSorting getter implementation
//				return false;
//			}
//		}
//
//		public bool IsSorted
//		{
//			get
//			{
//				// TODO:  Add DataView.IsSorted getter implementation
//				return false;
//			}
//		}
//
//		public bool AllowRemove
//		{
//			get
//			{
//				// TODO:  Add DataView.AllowRemove getter implementation
//				return true;
//			}
//		}
//
//		public bool SupportsSearching
//		{
//			get
//			{
//				// TODO:  Add DataView.SupportsSearching getter implementation
//				return true;
//			}
//		}
//
//		public System.ComponentModel.ListSortDirection SortDirection
//		{
//			get
//			{
//				// TODO:  Add DataView.SortDirection getter implementation
//				return System.ComponentModel.ListSortDirection.Ascending;
//			}
//		}
//
//		public event System.ComponentModel.ListChangedEventHandler ListChanged;
//
//		public bool SupportsChangeNotification
//		{
//			get
//			{
//				// TODO:  Add DataView.SupportsChangeNotification getter implementation
//				return true;
//			}
//		}
//
//		public void RemoveSort()
//		{
//			// TODO:  Add DataView.RemoveSort implementation
//		}
//
//		public virtual object AddNew()
//		{
//			// TODO:  Add DataView.AddNew implementation
//			return null;
//		}
//
//		public bool AllowEdit
//		{
//			get
//			{
//				// TODO:  Add DataView.AllowEdit getter implementation
//				return true;
//			}
//		}
//
//		public void RemoveIndex(System.ComponentModel.PropertyDescriptor property)
//		{
//			// TODO:  Add DataView.RemoveIndex implementation
//		}
//
//		#endregion
//
//		#region IList Members
//
//		public bool IsReadOnly
//		{
//			get
//			{
//				// TODO:  Add DataView.IsReadOnly getter implementation
//				return false;
//			}
//		}
//
//		public bool IsFixedSize
//		{
//			get
//			{
//				// TODO:  Add DataView.IsFixedSize getter implementation
//				return false;
//			}
//		}
//
//		#endregion
//
//		#region ICollection Members
//
//		public object SyncRoot
//		{
//			get
//			{
//				// TODO:  Add DataView.SyncRoot getter implementation
//				return this;//Rows.SyncRoot;
//			}
//		}
//
//		#endregion
//
//		#region ISupportInitialize Members
//
//		public void BeginInit()
//		{
//			// TODO:  Add DataView.BeginInit implementation
//		}
//
//		public void EndInit()
//		{
//			// TODO:  Add DataView.EndInit implementation
//		}
//
//		#endregion
//
//		/*public System.ComponentModel.PropertyDescriptorCollection SelectedColumns
//		{
//			get
//			{
//				return m_Attributes;
//			}
//		}
//
//		public void SelectColumns(string [] names)
//		{
//			m_Attributes.Clear();
//			for(int i=0;i<names.Length;i++)
//			{
//				m_Attributes.Add(new CubeStone.MDP.UI.Data.DataColumnPropertyDescriptor(names[i]));
//			}
//		}
//
//		public string[] ColumnNames
//		{
//			get
//			{
//				string [] names = new string[m_Attributes.Count];
//				for(int i=0;i<m_Attributes.Count;i++)
//				{
//					names[i] = m_Attributes[i].Name;
//				}
//				return names;
//			}
//		}*/
//		#region ITypedList Members
//
//		public System.ComponentModel.PropertyDescriptorCollection GetItemProperties(System.ComponentModel.PropertyDescriptor[] listAccessors)
//		{
//			/*if(Rows.Count>0)
//			{
//				DataRowView rv = Rows[0] as DataRowView;
//				return rv.GetProperties(null);
//			}*/
//			return m_Attributes;
//		}
//
//		public string GetListName(System.ComponentModel.PropertyDescriptor[] listAccessors)
//		{
//			return System.String.Empty;
//		}
//
//		#endregion
//	}
//
}
