// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Integrator.Data
{
	/// <summary>
	/// Summary description for AbstractItemView.
	/// </summary>
	public abstract class AbstractItemView : Cerebrum.DisposableObject, System.ComponentModel.ICustomTypeDescriptor
	{
		public AbstractItemView()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public abstract Cerebrum.IConnector GetConnector();

		#region ICustomTypeDescriptor Members

		public System.ComponentModel.TypeConverter GetConverter()
		{
			// TODO:  Add AbstractItemView.GetConverter implementation
			return null;
		}

		public System.ComponentModel.EventDescriptorCollection GetEvents(Attribute[] attributes)
		{
			return new System.ComponentModel.EventDescriptorCollection(null);
		}

		public System.ComponentModel.EventDescriptorCollection GetEvents()
		{
			return new System.ComponentModel.EventDescriptorCollection(null);
		}

		public string GetComponentName()
		{
			// TODO:  Add AbstractItemView.GetComponentName implementation
			return null;
		}

		public object GetPropertyOwner(System.ComponentModel.PropertyDescriptor pd)
		{
			return this;
		}

		public System.ComponentModel.AttributeCollection GetAttributes()
		{
			return new System.ComponentModel.AttributeCollection(null);
		}

		public abstract System.ComponentModel.PropertyDescriptorCollection GetProperties(Attribute[] attributes);
		
		public System.ComponentModel.PropertyDescriptorCollection GetProperties()
		{
			return GetProperties(null);
		}

		public object GetEditor(Type editorBaseType)
		{
			// TODO:  Add AbstractItemView.GetEditor implementation
			return null;
		}

		public System.ComponentModel.PropertyDescriptor GetDefaultProperty()
		{
			// TODO:  Add AbstractItemView.GetDefaultProperty implementation
			return null;
		}

		public System.ComponentModel.EventDescriptor GetDefaultEvent()
		{
			// TODO:  Add AbstractItemView.GetDefaultEvent implementation
			return null;
		}

		public string GetClassName()
		{
			// TODO:  Add AbstractItemView.GetClassName implementation
			return null;
		}

		#endregion

		#region IDisposable Members

		/*protected override void Dispose(bool disposing)
		{
			base.Dispose (disposing);
		}*/


		#endregion

		public object this[string name]
		{
			get
			{
				return GetProperties()[name].GetValue(this);
			}
			set
			{
				GetProperties()[name].SetValue(this, value);
			}
		}

	}
}
