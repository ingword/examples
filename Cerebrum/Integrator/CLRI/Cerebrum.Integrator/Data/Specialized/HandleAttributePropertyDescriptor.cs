// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data.Specialized
{
	/// <summary>
	/// Summary description for HandleAttributePropertyDescriptor.
	/// </summary>
	public class HandleAttributePropertyDescriptor : Cerebrum.Data.AttributePropertyDescriptor 
	{
		private Cerebrum.ObjectHandle m_PrecedentTableId;

		public Cerebrum.ObjectHandle PrecedentTableId
		{
			get
			{
				return this.m_PrecedentTableId;
			}
		}

		public HandleAttributePropertyDescriptor(Cerebrum.IComposite connector) : base(connector)
		{
			Cerebrum.Reflection.FieldDescriptor fd = connector.Component as Cerebrum.Reflection.FieldDescriptor;
			if(fd!=null)
			{
				this.m_PrecedentTableId = fd.PrecedentTableId;
			}
			else
			{
				this.m_PrecedentTableId = Cerebrum.ObjectHandle.Null;
			}
		}
	
		public override bool CanResetValue(object component)
		{
			return false;
		}

		public override object GetValue(object component)
		{
			Cerebrum.Data.ComponentItemView item = component as Cerebrum.Data.ComponentItemView;
			if(item!=null)
			{
				if(this.AttributePlasmaHandle==Cerebrum.Specialized.Concepts.PlasmaHandleAttribute)
				{
					return item.ObjectHandle;
				}
				else if(this.AttributePlasmaHandle==Cerebrum.Specialized.Concepts.TypeIdHandleAttribute)
				{
					using(Cerebrum.IComposite connector = item.GetComposite())
					{
						if(connector==null)
						{
							return Cerebrum.ObjectHandle.Null;
						}
						else
						{
							return connector.TypeIdHandle;
						}
					}
				}
				else if(this.AttributePlasmaHandle==Cerebrum.Specialized.Concepts.KindIdHandleAttribute)
				{
					using(Cerebrum.IComposite connector = item.GetComposite())
					{
						if(connector==null)
						{
							return Cerebrum.ObjectHandle.Null;
						}
						else
						{
							return connector.KindIdHandle;
						}
					}
				}
				else if(this.AttributePlasmaHandle==Cerebrum.Specialized.Concepts.GroundHandleAttribute)
				{
					using(Cerebrum.IComposite connector = item.GetComposite())
					{
						if(connector==null)
						{
							return Cerebrum.ObjectHandle.Null;
						}
						else
						{
							return connector.GroundHandle;
						}
					}
				}
			}
			return null;
		}

		public override void ResetValue(object component)
		{
		}

		public override void SetValue(object component, object value)
		{
			Cerebrum.Data.HandledComponentItemView item = component as Cerebrum.Data.HandledComponentItemView;
			if(item!=null)
			{
				Cerebrum.ObjectHandle h;
				if(value is Cerebrum.ObjectHandle)
				{
					h = (Cerebrum.ObjectHandle)value;
				}
				else
				{
					System.ComponentModel.TypeConverter conv = System.ComponentModel.TypeDescriptor.GetConverter(typeof(Cerebrum.ObjectHandle));
					h = (Cerebrum.ObjectHandle)conv.ConvertFrom(value);
				}
				if(this.AttributePlasmaHandle==Cerebrum.Specialized.Concepts.PlasmaHandleAttribute)
				{
					using(Cerebrum.IComposite connector = item.GetComposite())
					{
						if(connector==null || connector.PlasmaHandle!=h)
						{
							//already in using
							//System.IDisposable temp = connector;
							Cerebrum.IContainer container = null;
							if(item.m_TypedList is TableView)
							{
								container = (item.m_TypedList as TableView).m_Container;
							}
							if(item.m_TypedList is VectorView)
							{
								container = (item.m_TypedList as VectorView).m_Container;
							}
							if(container!=null)
							{
								using(Cerebrum.IConnector c2 = container.AttachConnector(h))
								{
									item.SetConnector(h, c2);
								}
							}
							//item.m_CachedObjectHandle = Cerebrum.ObjectHandle.Null;
							if(connector==null && item.m_TypedList is VectorView)
							{
								(item.m_TypedList as VectorView).m_NewRow = null;
							}
							if(item.m_TypedList!=null)
							{
								int i = item.m_TypedList.IndexOf(item);
								item.m_TypedList.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemChanged, i, i));
							}
							//if(temp!=null) temp.Dispose();
						}
					}
				}
			}
		}

		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}

		public override bool IsBrowsable
		{
			get
			{
				return true;
			}
		}

		public override bool IsReadOnly
		{
			get
			{
				return this.AttributePlasmaHandle!=Cerebrum.Specialized.Concepts.PlasmaHandleAttribute;
			}
		}
		
		/*public override System.Type PropertyType
		{
			get
			{
				return typeof(int);
			}
		}*/


	}
}
