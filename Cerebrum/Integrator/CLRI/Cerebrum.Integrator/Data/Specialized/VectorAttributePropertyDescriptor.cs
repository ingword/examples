// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data.Specialized
{
	/// <summary>
	/// Summary description for VectorAttributePropertyDescriptor.
	/// </summary>
	public class VectorAttributePropertyDescriptor : Cerebrum.Data.AttributePropertyDescriptor 
	{
		private Cerebrum.ObjectHandle m_PrecedentTableId;

		public Cerebrum.ObjectHandle PrecedentTableId
		{
			get
			{
				return this.m_PrecedentTableId;
			}
		}

		public VectorAttributePropertyDescriptor(Cerebrum.IComposite connector) : base(connector)
		{
			Cerebrum.Reflection.FieldDescriptor fd = connector.Component as Cerebrum.Reflection.FieldDescriptor;
			if(fd!=null)
			{
				this.m_PrecedentTableId = fd.PrecedentTableId;
			}
			else
			{
				this.m_PrecedentTableId = Cerebrum.ObjectHandle.Null;
			}
		}

		public override bool CanResetValue(object component)
		{
			return false;
		}

		public System.ComponentModel.PropertyDescriptorCollection GetItemDescriptors(Cerebrum.Integrator.DomainContext domainContext)
		{
#warning " GetSector should be depricated "
			using(Cerebrum.Runtime.NativeSector sector = domainContext.GetSector())
			{
				Cerebrum.IConnector c2 = null;
				try
				{
					if(this.m_PrecedentTableId != Cerebrum.ObjectHandle.Null)
					{
						using(Cerebrum.IConnector ctbl = sector.AttachConnector(this.m_PrecedentTableId))
						{
							c2 = ((Cerebrum.Reflection.TableDescriptor)ctbl.Component).GetSelectedAttributesVector();
						}
					}
					if(c2==null)
					{
						c2 = sector.AttachConnector(Cerebrum.Specialized.Concepts.SingleObjectHandleVectorId);
					}
					return SimpleView.DescriptorsFromVector(c2 as Cerebrum.Runtime.NativeWarden);
				}
				finally
				{
					if(c2!=null)
					{
						c2.Dispose();
					}
				}
			}
		}

		public override object GetValue(object component)
		{
			Cerebrum.Data.ComponentItemView item = component as Cerebrum.Data.ComponentItemView;
			if(item!=null)
			{
				using(Cerebrum.IConnector vc = item.GetComposite())
				{
					Cerebrum.IContainer c0 = vc as Cerebrum.IContainer;
					if(c0==null) 
					{
						return null;
					}

					using(Cerebrum.Runtime.NativeSector sector = vc.Workspace.GetSector() as Cerebrum.Runtime.NativeSector)
					{
						Cerebrum.IConnector c2 = null;
						Cerebrum.IConnector cc = null;
						try
						{
							if(this.m_PrecedentTableId != Cerebrum.ObjectHandle.Null)
							{
								using(Cerebrum.IConnector ctbl = sector.AttachConnector(this.m_PrecedentTableId))
								{
									c2 = ((Cerebrum.Reflection.TableDescriptor)ctbl.Component).GetSelectedAttributesVector();
								}
							}
							if(c2==null)
							{
								c2 = sector.AttachConnector(Cerebrum.Specialized.Concepts.SingleObjectHandleVectorId);
							}


							cc = c0.AttachConnector(this.AttributePlasmaHandle);
							if(cc==null)
							{
								cc = c0.CreateConnector(this.AttributePlasmaHandle, this.AttributeTypeIdHandle);
							}
						
							return new Data.VectorView(sector, c2 as Cerebrum.Runtime.NativeWarden, cc as Cerebrum.Runtime.NativeWarden);
						}
						finally
						{
							if(c2!=null)
							{
								c2.Dispose();
							}
							if(cc!=null)
							{
								cc.Dispose();
							}
						}
					}
				}				
				//return new System.Data.DataTable().DefaultView;
				/*using(Cerebrum.IContainer c0 = item.m_Connector.GetContainer() as Cerebrum.IContainer)
				{
					using(Cerebrum.IConnector c1 = c0.AttachConnector(m_Attribute.ObjectHandle))
					{
						if(c1==null)
						{
							return System.DBNull.Value;
						}
						else
						{
							return c1.Component;
						}
					}
				}*/
			}
			return null;
		}

		public override void ResetValue(object component)
		{
		}

		public override void SetValue(object component, object value)
		{
		}

		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}

		public override bool IsBrowsable
		{
			get
			{
				return true;
			}
		}

		public override bool IsReadOnly
		{
			get
			{
				return false;
			}
		}
	}
}
