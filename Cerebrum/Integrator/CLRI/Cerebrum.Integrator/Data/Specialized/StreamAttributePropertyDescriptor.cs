// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data.Specialized
{
	/// <summary>
	/// Summary description for ScalarAttributePropertyDescriptor.
	/// </summary>
	public class StreamAttributePropertyDescriptor : Cerebrum.Data.AttributePropertyDescriptor 
	{
		public StreamAttributePropertyDescriptor(Cerebrum.IComposite connector) : base(connector)
		{
		}

	
		public override bool CanResetValue(object component)
		{
			return false;
		}

		public override object GetValue(object component)
		{
			Cerebrum.Data.ComponentItemView item = component as Cerebrum.Data.ComponentItemView;
			if(item!=null)
			{
				using(Cerebrum.IContainer c0 = item.GetComposite() as Cerebrum.IContainer)
				{
					using(Cerebrum.IComposite cstm = c0.AttachConnector(this.AttributePlasmaHandle))
					{
						System.IO.Stream stm = cstm as System.IO.Stream;
						if(stm==null)
						{
							return System.DBNull.Value;
						}
						else
						{
							byte [] bytes;
							bytes = new byte[stm.Length];
							stm.Read(bytes, 0, bytes.Length);
							return bytes;
						}
					}
				}
			}
			return null;
		}

		public override void ResetValue(object component)
		{
			/*Cerebrum.Data.ComponentItemView item = component as Cerebrum.Data.ComponentItemView;
			if(item!=null)
			{
			}*/
		}

		public override void SetValue(object component, object value)
		{
			Cerebrum.Data.ComponentItemView item = component as Cerebrum.Data.ComponentItemView;
			if(item!=null)
			{
				using(Cerebrum.IContainer c0 = item.GetComposite() as Cerebrum.IContainer)
				{
					if(c0==null) return;

					object o = value;
					System.Type t = this.PropertyType;
					if(o!=null & t!=null)
					{
						if(o.GetType()!=t)
						{
							System.ComponentModel.TypeConverter conv = System.ComponentModel.TypeDescriptor.GetConverter(t);
							if(conv.CanConvertFrom(o.GetType()))
							{
								o = conv.ConvertFrom(o);
							}
						}
					}
					Cerebrum.ObjectHandle attrHandle = this.AttributePlasmaHandle;
					Cerebrum.IComposite cc = null;
					bool removePending = false;
					try
					{
						if(o!=null && o is byte[])
						{
							cc = c0.AttachConnector(attrHandle);
							if(cc==null)
							{
								cc = c0.CreateConnector(attrHandle, this.AttributeTypeIdHandle);
							}
						
							System.IO.Stream stm = cc as System.IO.Stream;
							if(stm!=null)
							{
								byte[] bytes = o as byte[];
								stm.Write(bytes, 0, bytes.Length);
								stm.SetLength(bytes.Length);
							}
						}
						else
						{
							cc = c0.AttachConnector(attrHandle);
							if(cc!=null)
							{
								removePending = (cc.KindIdHandle == Cerebrum.Runtime.KernelObjectKindId.Stream);
								cc.Component = null;
							}
						}
					}
					finally
					{
						if(cc!=null)
						{
							cc.Dispose();
						}
					}

					if(removePending)
					{
						c0.RemoveConnector(attrHandle);
					}

					Cerebrum.Data.HandledComponentItemView vitem = item as Cerebrum.Data.HandledComponentItemView;
					if(vitem!=null && vitem.m_TypedList!=null)
					{
						int i = vitem.m_TypedList.IndexOf(vitem);
						vitem.m_TypedList.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemChanged, i, i));
					}
				}
			}
		}

		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}

		public override bool IsBrowsable
		{
			get
			{
				return true;
			}
		}

		public override bool IsReadOnly
		{
			get
			{
				return false;
			}
		}
	}
}


