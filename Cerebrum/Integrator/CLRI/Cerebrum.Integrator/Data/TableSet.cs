// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data
{
	public class TableSetItemView : System.ComponentModel.ICustomTypeDescriptor, System.IDisposable
	{
		protected internal System.ComponentModel.PropertyDescriptorCollection m_Descriptors;
		protected Cerebrum.Data.TableSet m_TableSet;
		public TableSetItemView(Cerebrum.Data.TableSet tableSet)
		{
			m_TableSet = tableSet;
		}

		/*public override bool Equals(object obj)
		{
			if(base.Equals(obj))
			{
				return true;
			}
			else
			{
				if(obj is DataRowView)
				{
					return this.Row.Equals((obj as DataRowView).Row);
				}
				else
					return false;
			}
		}

		public override int GetHashCode()
		{
			return this.Row.GetHashCode();
		}*/

		public System.ComponentModel.AttributeCollection GetAttributes()
		{
			return new System.ComponentModel.AttributeCollection(null);
		}

		public string GetClassName()
		{
			return null;
		}

		public string GetComponentName()
		{
			return null;
		}

		public System.ComponentModel.TypeConverter GetConverter()
		{
			return null;
		}

		public System.ComponentModel.EventDescriptor GetDefaultEvent()
		{
			return null;
		}

		public System.ComponentModel.PropertyDescriptor GetDefaultProperty()
		{
			return null;
		}

		public object GetEditor(System.Type editorBaseType)
		{
			return null;
		}

		public System.ComponentModel.EventDescriptorCollection GetEvents(System.Attribute[] attributes)
		{
			return new System.ComponentModel.EventDescriptorCollection(null);
		}

		public System.ComponentModel.EventDescriptorCollection GetEvents()
		{
			return new System.ComponentModel.EventDescriptorCollection(null);
		}

		public System.ComponentModel.PropertyDescriptorCollection GetProperties(System.Attribute[] attributes)
		{
			System.ComponentModel.PropertyDescriptor[] properties;
			if (this.m_Descriptors == null)
			{
				properties = null;
				TableSet tableSet = this.m_TableSet;
				if (tableSet != null)
				{
					int tablesCount = tableSet.m_SuperTable.Count;
					properties = new System.ComponentModel.PropertyDescriptor[tablesCount];
					for (int i = 0; (i < tablesCount); i++)
					{
						using(Cerebrum.IConnector connector = (tableSet.m_SuperTable[i] as ComponentItemView).GetComposite())
						{
							properties[i] = new CerebrumTableDescriptor((connector.Component as Cerebrum.Reflection.TableDescriptor).GetTableView());
						}
					}
				}
				this.m_Descriptors = new System.ComponentModel.PropertyDescriptorCollection(properties);

			}
			return this.m_Descriptors; 
		}
		public System.ComponentModel.PropertyDescriptorCollection GetProperties()
		{
			return GetProperties(null);
		}

		public object GetPropertyOwner(System.ComponentModel.PropertyDescriptor pd)
		{
			return this;
		}

		/*public object this[string name]
		{
			get
			{
				return GetProperties()[name].GetValue(this);
			}
			set
			{
				GetProperties()[name].SetValue(this, value);
			}
		}*/
		#region IDisposable Members

		public void Dispose()
		{
			/*if(m_SuperTable!=null)
			{
				m_SuperTable.Dispose();
				m_SuperTable = null;
			}*/
			if(m_Descriptors!=null)
			{
				foreach(System.IDisposable o in m_Descriptors)
				{
					o.Dispose();
				}
				m_Descriptors = null;
			}
		}

		#endregion
	}

	public class CerebrumTableDescriptor : System.ComponentModel.PropertyDescriptor, System.IDisposable
	{
		Cerebrum.Data.TableView m_TableView;
		
		public Cerebrum.Data.TableView TableView
		{
			get
			{
				return this.m_TableView;
			}
		}
		public CerebrumTableDescriptor(Cerebrum.Data.TableView tableView) : base(tableView.TableDescriptor.Name, null)
		{
			m_TableView = tableView;
		}

		public override bool CanResetValue(object component)
		{
			return false;
		}

		public override object GetValue(object component)
		{
			Cerebrum.Data.TableSetItemView item = component as Cerebrum.Data.TableSetItemView;
			if(item!=null)
			{
				return this.m_TableView;
			}
			return null;
		}

		public override void ResetValue(object component)
		{
		}

		public override void SetValue(object component, object value)
		{
		}

		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}

		public override System.Type ComponentType
		{
			get
			{
				return typeof(Cerebrum.Data.ComponentItemView);
			}
		}

		public override bool IsBrowsable
		{
			get
			{
				return true;
			}
		}

		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		public override System.Type PropertyType
		{
			get
			{
				return typeof(System.ComponentModel.IBindingList); 
			}
		}
		#region IDisposable Members

		public void Dispose()
		{
			if(m_TableView!=null)
			{
				m_TableView.Dispose();
				m_TableView = null;
			}
		}

		#endregion
	}

	public class TableSet : Cerebrum.DisposableObject, System.ComponentModel.IBindingList, System.ComponentModel.ISupportInitialize, System.ComponentModel.ITypedList
	{
		protected internal Cerebrum.Data.TableView m_SuperTable;
		protected TableSetItemView m_Item;

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_SuperTable!=null)
				{
					m_SuperTable.Dispose();
					m_SuperTable = null;
				}
				if(m_Item!=null)
				{
					m_Item.Dispose();
					m_Item = null;
				}
			}
			base.Dispose (disposing);
		}


		public TableSet(Cerebrum.Data.TableView superTable)
		{
			m_SuperTable = superTable;
			m_Item = new TableSetItemView(this);
		}

		public int Add(object value)
		{
			return -1;
		}

		int System.Collections.IList.Add(object value)
		{
			return this.Add(value);
		}


		int _suspendCount = 0;
		public void SuspendEvents()
		{
			_suspendCount ++;
		}
		public void ResumeEvents()
		{
			_suspendCount --;
			if(_suspendCount == 0)
			{
				Reset();
			}
		}

		public void Clear()
		{
			Reset();
		}
		public void Reset()
		{
			if(m_Item!=null)
				foreach(System.ComponentModel.PropertyDescriptor prp in m_Item.GetProperties())
				{
					Cerebrum.Data.SimpleView vw = prp.GetValue(m_Item) as Cerebrum.Data.SimpleView;
					if(vw!=null)
					{
						vw.Reset();
					}
				}
			if(_suspendCount == 0 && this.ListChanged!=null)
			{
				this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.Reset, -1));
			}
		}

		public bool Contains(object value)
		{
			return (m_Item == value);
		}

		public void CopyTo(System.Array array, int index)
		{
			//Rows.CopyTo(array, index);
		}

		public int Count
		{
			get
			{
				return 1;
				//			m_RowsCount = m_Rows.GetMap(new Cerebrum.ObjectHandle(0)).HandleIntPtr.ToInt32();
			}
		}

		public System.Collections.IEnumerator GetEnumerator()
		{
			return null;//Rows.GetEnumerator();
		}

		public int IndexOf(object value)
		{
			return -1;//Rows.IndexOf(value);
		}

		public void Insert(int index, object value)
		{
			throw new System.NotSupportedException();
		}

		public bool IsSynchronized
		{
			get
			{
				return false;//Rows.IsSynchronized;
			}
		}
 

		public object this[int index]
		{
			get
			{
				return m_Item;
			}			set
			{
				/*Rows[index] = value;
				if(_suspendCount == 0 && this.ListChanged!=null)
				{
					this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemChanged, index));
				}*/
			}
		}

		public void Remove(object value)
		{
			/*int i = Rows.IndexOf(value);
			if(i>=0)
			{
				this.RemoveAt(i);
			}*/
		}

		public void RemoveAt(int index)
		{
			/*Rows.RemoveAt(index);
			if(_suspendCount == 0 && this.ListChanged!=null)
			{
				this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemDeleted, index));
			}*/
		}

		public int Find(System.ComponentModel.PropertyDescriptor property, object key)
		{
			/*int i;
			System.Collections.IList s = Rows;
			for (i=0; i < s.Count; i++)
			{
				if (key.Equals(property.GetValue(s[i])))
				{
					return i;
				}

			}*/

			return -1;
		}

		#region IBindingList Members

		public void AddIndex(System.ComponentModel.PropertyDescriptor property)
		{
			// TODO:  Add DataView.AddIndex implementation
		}

		public bool AllowNew
		{
			get
			{
				// TODO:  Add DataView.AllowNew getter implementation
				return false;
			}
		}

		public void ApplySort(System.ComponentModel.PropertyDescriptor property, System.ComponentModel.ListSortDirection direction)
		{
			// TODO:  Add DataView.ApplySort implementation
		}

		public System.ComponentModel.PropertyDescriptor SortProperty
		{
			get
			{
				// TODO:  Add DataView.SortProperty getter implementation
				return null;
			}
		}

		public bool SupportsSorting
		{
			get
			{
				// TODO:  Add DataView.SupportsSorting getter implementation
				return false;
			}
		}

		public bool IsSorted
		{
			get
			{
				// TODO:  Add DataView.IsSorted getter implementation
				return false;
			}
		}

		public bool AllowRemove
		{
			get
			{
				// TODO:  Add DataView.AllowRemove getter implementation
				return true;
			}
		}

		public bool SupportsSearching
		{
			get
			{
				// TODO:  Add DataView.SupportsSearching getter implementation
				return true;
			}
		}

		public System.ComponentModel.ListSortDirection SortDirection
		{
			get
			{
				// TODO:  Add DataView.SortDirection getter implementation
				return System.ComponentModel.ListSortDirection.Ascending;
			}
		}

		public event System.ComponentModel.ListChangedEventHandler ListChanged;

		public bool SupportsChangeNotification
		{
			get
			{
				// TODO:  Add DataView.SupportsChangeNotification getter implementation
				return true;
			}
		}

		public void RemoveSort()
		{
			// TODO:  Add DataView.RemoveSort implementation
		}

		public object AddNew()
		{
			// TODO:  Add DataView.AddNew implementation
			return null;
		}

		public bool AllowEdit
		{
			get
			{
				// TODO:  Add DataView.AllowEdit getter implementation
				return false;
			}
		}

		public void RemoveIndex(System.ComponentModel.PropertyDescriptor property)
		{
			// TODO:  Add DataView.RemoveIndex implementation
		}

		#endregion

		#region IList Members

		public bool IsReadOnly
		{
			get
			{
				// TODO:  Add DataView.IsReadOnly getter implementation
				return false;
			}
		}

		public bool IsFixedSize
		{
			get
			{
				// TODO:  Add DataView.IsFixedSize getter implementation
				return false;
			}
		}

		#endregion

		#region ICollection Members

		public object SyncRoot
		{
			get
			{
				// TODO:  Add DataView.SyncRoot getter implementation
				return this;//Rows.SyncRoot;
			}
		}

		#endregion

		#region ISupportInitialize Members

		public void BeginInit()
		{
			// TODO:  Add DataView.BeginInit implementation
		}

		public void EndInit()
		{
			// TODO:  Add DataView.EndInit implementation
		}

		#endregion

		internal System.ComponentModel.PropertyDescriptorCollection FindTable(System.ComponentModel.PropertyDescriptorCollection baseTable, System.ComponentModel.PropertyDescriptor[] props, int propStart)
		{
			System.ComponentModel.PropertyDescriptor descriptor;
			if (props.Length < (propStart + 1))
			{
				return baseTable; 
			}
			descriptor = props[propStart];
			if (baseTable == null)
			{
				if ((descriptor as CerebrumTableDescriptor) != null)
				{
					return this.FindTable(((CerebrumTableDescriptor) descriptor).TableView.GetItemProperties(null), props, (propStart + 1)); 
				}
				return null; 
			}
			if ((descriptor as Data.Specialized.VectorAttributePropertyDescriptor) != null)
			{
				return this.FindTable(((Data.Specialized.VectorAttributePropertyDescriptor) descriptor).GetItemDescriptors(Cerebrum.Integrator.DomainContext.FromConnector(this.m_SuperTable.m_Container)), props, (propStart + 1)); 
			}
			/*if ((descriptor1 as DataRelationPropertyDescriptor) != null)
			{
				return this.FindTable(((DataRelationPropertyDescriptor) descriptor1).Relation.ChildTable, props, (propStart + 1)); 
			}*/
			return null; 
		}

		#region ITypedList Members

		public System.ComponentModel.PropertyDescriptorCollection GetItemProperties(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			System.ComponentModel.PropertyDescriptorCollection properties;
			if ((listAccessors == null) || (listAccessors.Length == 0))
			{
				return m_Item.GetProperties();
			}
			properties = this.FindTable(null, listAccessors, 0);
			if (properties != null)
			{
				return properties; 
			}
			return new System.ComponentModel.PropertyDescriptorCollection(null);
		}

		public string GetListName(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			return System.String.Empty;
		}

		#endregion
	}
}
