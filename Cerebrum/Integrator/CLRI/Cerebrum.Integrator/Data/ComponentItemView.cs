// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data
{
	/// <summary>
	/// Summary description for ComponentItemView.
	/// </summary>
	public abstract class ComponentItemView : System.ComponentModel.ICustomTypeDescriptor, System.IDisposable
	{
		public ComponentItemView()
		{
		}

		public abstract Cerebrum.ObjectHandle ObjectHandle {get;}

		public abstract Cerebrum.IComposite GetComposite();

		public abstract void SetConnector(Cerebrum.ObjectHandle plasma, Cerebrum.IConnector connector);

		public System.ComponentModel.AttributeCollection GetAttributes()
		{
			return new System.ComponentModel.AttributeCollection(null);
		}

		public string GetClassName()
		{
			return null;
		}

		public string GetComponentName()
		{
			return null;
		}

		public System.ComponentModel.TypeConverter GetConverter()
		{
			return null;
		}

		public System.ComponentModel.EventDescriptor GetDefaultEvent()
		{
			return null;
		}

		public System.ComponentModel.PropertyDescriptor GetDefaultProperty()
		{
			return null;
		}

		public object GetEditor(System.Type editorBaseType)
		{
			return null;
		}

		public System.ComponentModel.EventDescriptorCollection GetEvents(System.Attribute[] attributes)
		{
			return new System.ComponentModel.EventDescriptorCollection(null);
		}

		public System.ComponentModel.EventDescriptorCollection GetEvents()
		{
			return new System.ComponentModel.EventDescriptorCollection(null);
		}

		public abstract System.ComponentModel.PropertyDescriptorCollection GetProperties(System.Attribute[] attributes);

		public System.ComponentModel.PropertyDescriptorCollection GetProperties()
		{
			return GetProperties(null);
		}

		public object GetPropertyOwner(System.ComponentModel.PropertyDescriptor pd)
		{
			return this;
		}

		public object this[string name]
		{
			get
			{
				return GetProperties()[name].GetValue(this);
			}
			set
			{
				GetProperties()[name].SetValue(this, value);
			}
		}

		#region IDisposable Members

		public virtual void Dispose()
		{
		}

		#endregion
	}

	/*public class IndexedComponentItemView : ComponentItemView
	{
		//protected internal Cerebrum.IConnector m_Connector;
		//protected System.ComponentModel.ITypedList m_TypedList;
		protected internal SimpleView m_TypedList;
		protected internal int m_Index;
		public IndexedComponentItemView(/-*Cerebrum.IConnector connector, *-/SimpleView list, int index)
		{
			//m_Connector = connector;
			m_TypedList = list;
			m_Index = index;
		}

		public override Cerebrum.ObjectHandle ObjectHandle
		{
			get
			{
				return this.m_TypedList.GetMap(m_Index);
			}
			set
			{
			}
		}

#warning unsafe optimization
		protected internal Cerebrum.ObjectHandle m_CachedObjectHandle = Cerebrum.ObjectHandle.Null;
		public override Cerebrum.IConnector GetConnector()
		{
			//Cerebrum.ObjectHandle h = this.m_TypedList.GetMap(m_Index);
			if(Cerebrum.ObjectHandle.Null==this.m_CachedObjectHandle)
			{
				m_CachedObjectHandle = this.m_TypedList.GetMap(m_Index);
			}
			return this.m_TypedList.m_Container.AttachConnector(m_CachedObjectHandle);
		}

		public override System.ComponentModel.PropertyDescriptorCollection GetProperties(System.Attribute[] attributes)
		{
			/-*System.ComponentModel.PropertyDescriptor[] ps = new System.ComponentModel.PropertyDescriptor[Row.Table.Columns.Count];
			int i;
			for (i=0; i <= Row.Table.Columns.Count- 1; i++)
			{
				ps[i] = new CubeStone.MDP.UI.Data.DataColumnPropertyDescriptor(Row.Table.Columns[i].ColumnName);
			}

			return new System.ComponentModel.PropertyDescriptorCollection(ps);*-/
			return m_TypedList.GetItemProperties(null);
		}

		public override bool Equals(object obj)
		{
			if(obj is IndexedComponentItemView)
			{
				IndexedComponentItemView item = obj as IndexedComponentItemView;
				if(this.m_TypedList==item.m_TypedList && this.m_Index==item.m_Index)
				{
					return true;
				}
			}
			return false;
		}

		public override int GetHashCode()
		{
			return this.m_Index.GetHashCode() ^ this.m_TypedList.GetHashCode();
		}

	}

	*/
	internal class HandledComponentItemView : ComponentItemView
	{
		//protected internal Cerebrum.IConnector m_Connector;
		//protected System.ComponentModel.ITypedList m_TypedList;
		protected internal SimpleView m_TypedList;
		protected internal Cerebrum.ObjectHandle m_PlasmaHandle;

		public HandledComponentItemView(SimpleView list, Cerebrum.ObjectHandle handle)
		{
			m_TypedList = list;
			m_PlasmaHandle = handle;
		}

		public override Cerebrum.ObjectHandle ObjectHandle
		{
			get
			{
				return this.m_PlasmaHandle;
			}
		}

		public override Cerebrum.IComposite GetComposite()
		{
			if(!Cerebrum.ObjectHandle.Null.Equals(m_PlasmaHandle))
			{
				return this.m_TypedList.m_Components.AttachConnector(m_PlasmaHandle);
			}
			else
			{
				return null;
			}
		}

		public override void SetConnector(Cerebrum.ObjectHandle handle, Cerebrum.IConnector connector)
		{
			bool changed = false;
			if(!Cerebrum.ObjectHandle.Null.Equals(m_PlasmaHandle))
			{
				changed = true;
				this.m_TypedList.m_Components.RemoveConnector(m_PlasmaHandle);
			}
			m_PlasmaHandle = handle;
			if(connector!=null && !Cerebrum.ObjectHandle.Null.Equals(m_PlasmaHandle))
			{
				changed = true;
				//Cerebrum.Integrator.Utilites.EngageConnector(this.m_TypedList.m_Components, m_PlasmaHandle, connector);
				this.m_TypedList.m_Components.EngageConnector(m_PlasmaHandle, connector);
			}
			if(changed)
			{
				this.m_TypedList.Reset();
			}
		}


		public override System.ComponentModel.PropertyDescriptorCollection GetProperties(System.Attribute[] attributes)
		{
			return m_TypedList.GetItemProperties(null);
		}

		public override bool Equals(object obj)
		{
			HandledComponentItemView item = obj as HandledComponentItemView;
			if(item!=null)
			{
				if(this.m_TypedList==item.m_TypedList && this.m_PlasmaHandle==item.m_PlasmaHandle)
				{
					return true;
				}
			}
			return false;
		}

		public override int GetHashCode()
		{
			return this.m_PlasmaHandle.GetHashCode() ^ this.m_TypedList.GetHashCode();
		}

	}
}
