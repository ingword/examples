// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data
{
	/// <summary>
	/// Summary description for AttributePropertyDescriptor.
	/// </summary>
	public abstract class AttributePropertyDescriptor : System.ComponentModel.PropertyDescriptor, System.IDisposable
	{
//#if DEBUG
//		string debug_DisposeStackTrace = null;
//#endif


		private System.Type m_PropertyType;
		private Cerebrum.ObjectHandle m_AttributePlasmaHandle;
		private Cerebrum.ObjectHandle m_AttributeTypeIdHandle;
		private bool m_Derived;
		private string m_DisplayName;

		public override string DisplayName
		{
			get
			{
				if(this.m_DisplayName==null || this.m_DisplayName==string.Empty)
				{
					return base.DisplayName;
				}
				else
				{
					return this.m_DisplayName;
				}
			}
		}

#warning Cerebrum.IConnector
		public AttributePropertyDescriptor(Cerebrum.IComposite connector) : base(((Cerebrum.Reflection.IAttributeDescriptor)connector.Component).Name, null)
		{
			//m_Connector = connector;
			Cerebrum.Reflection.IAttributeDescriptor attribute = (Cerebrum.Reflection.IAttributeDescriptor)connector.Component;
			m_PropertyType = attribute.AttributeType;
			m_AttributePlasmaHandle = connector.PlasmaHandle;
			m_AttributeTypeIdHandle = attribute.AttributeTypeIdHandle;
			m_Derived = attribute.Derived;
			this.m_DisplayName = attribute.DisplayName;
		}

		public Cerebrum.ObjectHandle AttributePlasmaHandle
		{
			get
			{
				return m_AttributePlasmaHandle;
			}
		}

		public Cerebrum.ObjectHandle AttributeTypeIdHandle
		{
			get
			{
				return m_AttributeTypeIdHandle;
			}
		}

		public override System.Type ComponentType
		{
			get
			{
				return typeof(Cerebrum.Data.ComponentItemView);
			}
		}

		public override System.Type PropertyType
		{
			get
			{
				return m_PropertyType;
			}
		}

		public bool Derived
		{
			get
			{
				return m_Derived;
			}
		}
		#region IDisposable Members

		public void Dispose()
		{
//			if(m_Connector!=null)
//			{
//				m_Connector.Dispose();
//				m_Connector = null;
//			}
//#if DEBUG
//			debug_DisposeStackTrace = (new System.Diagnostics.StackTrace()).ToString();
//#endif
		}

		#endregion
	}

}
