// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data
{

	public abstract class SimpleView : Cerebrum.DisposableObject, System.ComponentModel.IBindingList, System.ComponentModel.ISupportInitialize, System.ComponentModel.ITypedList
	{

		protected internal Cerebrum.Runtime.NativeWarden m_Components;
		protected System.Collections.ArrayList m_Items;

		protected override void Dispose(bool disposing)
		{
			if(m_Components!=null)
			{
				m_Components.Dispose();
				m_Components = null;
			}
			base.Dispose (disposing);
		}

		/*public TableView(Cerebrum.Runtime.INativeDomain domain, Cerebrum.IContainer container) : this(domain, (container as Cerebrum.Runtime.NativeWarden).AttachConnector(Cerebrum.Specialized.Concepts.VisibleAttributesAttribute).Container as Cerebrum.Runtime.NativeVector, (container as Cerebrum.Runtime.NativeWarden).AttachConnector(Cerebrum.Specialized.Concepts.SelectedComponentsAttribute).Container as Cerebrum.Runtime.NativeVector)
		{
		}*/

		public static System.ComponentModel.PropertyDescriptorCollection DescriptorsFromVector(Cerebrum.Runtime.NativeWarden columns)
		{
			System.Collections.ArrayList list = new System.Collections.ArrayList();
			foreach(System.Collections.DictionaryEntry de in columns)
			{
				using(Cerebrum.IComposite c0 = columns.AttachConnector((Cerebrum.ObjectHandle)de.Key))
				{
					if(c0.PlasmaHandle==Cerebrum.Specialized.Concepts.PlasmaHandleAttribute)
					{
						list.Add(new Data.Specialized.HandleAttributePropertyDescriptor(c0));
					}
					else if(c0.PlasmaHandle==Cerebrum.Specialized.Concepts.TypeIdHandleAttribute)
					{
						list.Add(new Data.Specialized.HandleAttributePropertyDescriptor(c0));
					}
					else if(c0.PlasmaHandle==Cerebrum.Specialized.Concepts.KindIdHandleAttribute)
					{
						list.Add(new Data.Specialized.HandleAttributePropertyDescriptor(c0));
					}
					else if(c0.PlasmaHandle==Cerebrum.Specialized.Concepts.GroundHandleAttribute)
					{
						list.Add(new Data.Specialized.HandleAttributePropertyDescriptor(c0));
					}
					else if(c0.PlasmaHandle==Cerebrum.Specialized.Concepts.SelectedAttributesAttribute)
					{
						list.Add(new Data.Specialized.VectorAttributePropertyDescriptor(c0));
					}
					else if(c0.PlasmaHandle==Cerebrum.Specialized.Concepts.SelectedComponentsAttribute)
					{
						list.Add(new Data.Specialized.VectorAttributePropertyDescriptor(c0));
					}
					else
					{
						Cerebrum.Reflection.IAttributeDescriptor ad = c0.Component as Cerebrum.Reflection.IAttributeDescriptor;
						if(ad!=null)
						{
							Cerebrum.ObjectHandle kc = Cerebrum.Runtime.KernelObjectKindId.Scalar;
							using(Cerebrum.IConnector tdc = ad.GetTypeDescriptor())
							{
								if(tdc!=null)
								{
									kc = ((Cerebrum.Reflection.TypeDescriptor)tdc.Component).KernelObjectKindId;
								}
							}
							if(kc==Cerebrum.Runtime.KernelObjectKindId.Warden)
							{
								list.Add(new Data.Specialized.VectorAttributePropertyDescriptor(c0));
							}
							else if(kc==Cerebrum.Runtime.KernelObjectKindId.Stream)
							{
								list.Add(new Data.Specialized.StreamAttributePropertyDescriptor(c0));
							}
							else
							{
								list.Add(new Data.Specialized.ScalarAttributePropertyDescriptor(c0));
							}
						}
					}
				}
				
			}
			return new System.ComponentModel.PropertyDescriptorCollection((System.ComponentModel.PropertyDescriptor[])list.ToArray(typeof(System.ComponentModel.PropertyDescriptor)));
		}

		public SimpleView()
		{
		}

		protected void Init(Cerebrum.Runtime.NativeWarden rows)
		{
			m_Components = (Cerebrum.Runtime.NativeWarden)((System.ICloneable)rows).Clone();
			ResetItemsCache();
		}

		private void ResetItemsCache()
		{
			m_Items = new System.Collections.ArrayList(m_Components.Count);
			foreach(System.Collections.DictionaryEntry de in m_Components)
			{
				m_Items.Add(this.CreateItemViewByObjectHandle((Cerebrum.ObjectHandle)de.Key));
			}
		}

		private int Add(object value)
		{
			return -1;
		}

		/*public int Add(Cerebrum.IContainer container, Cerebrum.ObjectHandle plasma)
		{
			using(Cerebrum.IComposite connector = container.AttachConnector(plasma))
			{
				return this.Add(plasma, connector);
			}
		}*/

		public int EngageConnector(Cerebrum.ObjectHandle plasma, Cerebrum.IConnector connector)
		{
			m_Components.EngageConnector(plasma, connector);
			int i = m_Items.Add(this.CreateItemViewByObjectHandle(plasma));
			this.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemAdded, i));
			return i;
		}

		public int CreateConnector(Cerebrum.ObjectHandle plasma, Cerebrum.ObjectHandle typeId)
		{
			using(Cerebrum.IComposite connector = m_Components.CreateConnector(plasma, typeId))
			{
				int i = m_Items.Add(this.CreateItemViewByObjectHandle(plasma));
				this.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemAdded, i));
				return i;
			}
		}

		/*public int Add(Cerebrum.ObjectHandle plasma, Cerebrum.Runtime.MapAccess access, Cerebrum.ObjectHandle ground)
		{
			//int i = this.Count;
			m_Components.SetMap(plasma, access, ground);
			int i = m_Items.Add(this.CreateItemViewByObjectHandle(plasma));
			this.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemAdded, i));
			return i;
		}*/
//
//		public Cerebrum.ObjectHandle GetMap(int index)
//		{
//			return m_Components.GetMap(new Cerebrum.ObjectHandle(index+1));
//		}

//		public void SetMap(int index, Cerebrum.ObjectHandle handle)
//		{
//			m_Components.SetMap(new Cerebrum.ObjectHandle(index+1), handle);
//		}
//
//		public void SetMap2(Cerebrum.ObjectHandle key, Cerebrum.ObjectHandle handle)
//		{
//			m_Components.SetMap(key, handle);
//		}


		int System.Collections.IList.Add(object value)
		{
			return this.Add(value);
		}


		int _suspendCount = 0;
		public void SuspendEvents()
		{
			_suspendCount ++;
		}
		public void ResumeEvents()
		{
			_suspendCount --;
			if(_suspendCount == 0)
			{
				Reset();
			}
		}

		public virtual void Clear()
		{
			m_Components.Clear();
			Reset();
		}
		public void Reset()
		{
			ResetItemsCache();
			this.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.Reset, -1));
		}

		protected internal void OnListChanged(System.ComponentModel.ListChangedEventArgs e)
		{
			if(_suspendCount == 0 && this.ListChanged!=null)
			{
				this.ListChanged(this, e);
			}
		}

		public bool Contains(object value)
		{
			return this.m_Items.Contains(value);
		}

		public bool ContainsObject(Cerebrum.ObjectHandle handle)
		{
			return this.m_Components.HasMap(handle);
		}

		public void CopyTo(System.Array array, int index)
		{
			this.m_Items.CopyTo(array, index);
		}

		public virtual int Count
		{
			get
			{
				return this.m_Items.Count;
				//return m_Components.Count;
				//			m_ComponentsCount = m_Components.GetMap(new Cerebrum.ObjectHandle(0)).HandleIntPtr.ToInt32();
			}
		}

		public System.Collections.IEnumerator GetEnumerator()
		{
//			object []items = new object[this.Count];
//			for(int i=0;i<items.Length;i++)
//			{
//				items[i] = this[i];
//			}
//			return items.GetEnumerator();
			return m_Items.GetEnumerator();
		}

		public virtual int IndexOf(object value)
		{
//			if(value is IndexedComponentItemView)
//			{
//				IndexedComponentItemView item = value as IndexedComponentItemView;
//				if(item.m_TypedList==this)
//				{
//					return item.m_Index;
//				}
//			}
//			return -1;//Rows.IndexOf(value);
			for(int i = 0; i < m_Items.Count; i++)
			{
				ComponentItemView item = (ComponentItemView)m_Items[i];
				if(item.Equals(value))
				{
					return i;
				}
			}
			return -1;
//			return m_Items.IndexOf(value);
		}

		int System.Collections.IList.IndexOf(object value)
		{
			return this.IndexOf(value);//Rows.IndexOf(value);
		}
//
//		public Cerebrum.ObjectHandle KeyOf(Cerebrum.ObjectHandle handle)
//		{
//			return m_Components.KeyOf(handle);
//		}

		public void Insert(int index, object value)
		{
			throw new System.NotSupportedException();
		}

		public bool IsSynchronized
		{
			get
			{
				return false;//Rows.IsSynchronized;
			}
		}

		public virtual ComponentItemView CreateItemViewByObjectHandle(Cerebrum.ObjectHandle handle)
		{
			return new HandledComponentItemView(this, handle);
		}
		public virtual ComponentItemView FindItemViewByObjectHandle(Cerebrum.ObjectHandle handle)
		{
			if(Cerebrum.ObjectHandle.Null.Equals(handle))
			{
				return null;
			}
			else
			{
				foreach(ComponentItemView item in this.m_Items)
				{
					if(item.ObjectHandle==handle)
					{
						return item;
					}
				}
				return null;
			}
		} 

		object System.Collections.IList.this[int index]
		{
			get
			{
				return this[index];
			}			set
			{
			}
		}

		public virtual ComponentItemView this[int index]
		{
			get
			{
				//Cerebrum.ObjectHandle h = GetMap(index);
				//return new IndexedComponentItemView(/*m_Container.AttachConnector(h),*/ this, index);
				return (ComponentItemView)this.m_Items[index];
			}			set
			{
				/*Rows[index] = value;
				if(_suspendCount == 0 && this.ListChanged!=null)
				{
					this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemChanged, index));
				}*/
			}
		}

		public void Remove(object value)
		{
			int i = this.IndexOf(value);
			if(i>=0)
			{
				this.RemoveAt(i);
			}
		}

		public void RemoveAt(int index)
		{
			this.m_Components.SetMap((this.m_Items[index] as ComponentItemView).ObjectHandle, Cerebrum.Runtime.MapAccess.Scalar, Cerebrum.ObjectHandle.Null);
			this.m_Items.RemoveAt(index);
			if(_suspendCount == 0 && this.ListChanged!=null)
			{
				this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemDeleted, index));
			}
		}

		public int Find(System.ComponentModel.PropertyDescriptor property, object key)
		{
			int i;
			for (i=0; i < this.Count; i++)
			{
				if (key.Equals(property.GetValue(this[i])))
				{
					return i;
				}

			}
			return -1;
		}

		#region IBindingList Members

		public void AddIndex(System.ComponentModel.PropertyDescriptor property)
		{
			// TODO:  Add DataView.AddIndex implementation
		}

		public virtual bool AllowNew
		{
			get
			{
				// TODO:  Add DataView.AllowNew getter implementation
				return false;
			}
		}

		public void ApplySort(System.ComponentModel.PropertyDescriptor property, System.ComponentModel.ListSortDirection direction)
		{
			// TODO:  Add DataView.ApplySort implementation
		}

		public System.ComponentModel.PropertyDescriptor SortProperty
		{
			get
			{
				// TODO:  Add DataView.SortProperty getter implementation
				return null;
			}
		}

		public bool SupportsSorting
		{
			get
			{
				// TODO:  Add DataView.SupportsSorting getter implementation
				return false;
			}
		}

		public bool IsSorted
		{
			get
			{
				// TODO:  Add DataView.IsSorted getter implementation
				return false;
			}
		}

		public bool AllowRemove
		{
			get
			{
				// TODO:  Add DataView.AllowRemove getter implementation
				return true;
			}
		}

		public bool SupportsSearching
		{
			get
			{
				// TODO:  Add DataView.SupportsSearching getter implementation
				return true;
			}
		}

		public System.ComponentModel.ListSortDirection SortDirection
		{
			get
			{
				// TODO:  Add DataView.SortDirection getter implementation
				return System.ComponentModel.ListSortDirection.Ascending;
			}
		}

		public event System.ComponentModel.ListChangedEventHandler ListChanged;

		public bool SupportsChangeNotification
		{
			get
			{
				// TODO:  Add DataView.SupportsChangeNotification getter implementation
				return true;
			}
		}

		public void RemoveSort()
		{
			// TODO:  Add DataView.RemoveSort implementation
		}

		public virtual Cerebrum.Data.ComponentItemView AddNew()
		{
			// TODO:  Add DataView.AddNew implementation
			return null;
		}

		object System.ComponentModel.IBindingList.AddNew()
		{
			return this.AddNew();
		}

		public bool AllowEdit
		{
			get
			{
				// TODO:  Add DataView.AllowEdit getter implementation
				return true;
			}
		}

		public void RemoveIndex(System.ComponentModel.PropertyDescriptor property)
		{
			// TODO:  Add DataView.RemoveIndex implementation
		}

		#endregion

		#region IList Members

		public bool IsReadOnly
		{
			get
			{
				// TODO:  Add DataView.IsReadOnly getter implementation
				return false;
			}
		}

		public bool IsFixedSize
		{
			get
			{
				// TODO:  Add DataView.IsFixedSize getter implementation
				return false;
			}
		}

		#endregion

		#region ICollection Members

		public object SyncRoot
		{
			get
			{
				// TODO:  Add DataView.SyncRoot getter implementation
				return this;//Rows.SyncRoot;
			}
		}

		#endregion

		#region ISupportInitialize Members

		public void BeginInit()
		{
			// TODO:  Add DataView.BeginInit implementation
		}

		public void EndInit()
		{
			// TODO:  Add DataView.EndInit implementation
		}

		#endregion

		/*public System.ComponentModel.PropertyDescriptorCollection SelectedColumns
		{
			get
			{
				return m_Attributes;
			}
		}

		public void SelectColumns(string [] names)
		{
			m_Attributes.Clear();
			for(int i=0;i<names.Length;i++)
			{
				m_Attributes.Add(new CubeStone.MDP.UI.Data.DataColumnPropertyDescriptor(names[i]));
			}
		}

		public string[] ColumnNames
		{
			get
			{
				string [] names = new string[m_Attributes.Count];
				for(int i=0;i<m_Attributes.Count;i++)
				{
					names[i] = m_Attributes[i].Name;
				}
				return names;
			}
		}*/
		#region ITypedList Members

		public abstract System.ComponentModel.PropertyDescriptorCollection GetItemProperties(System.ComponentModel.PropertyDescriptor[] listAccessors);

		public virtual string GetListName(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			return System.String.Empty;
		}

		#endregion
	}

	public class TableView : SimpleView
	{
#warning "direct access to component"
		Cerebrum.IConnector m_TableDescConn;
		protected internal Cerebrum.Runtime.NativeWarden m_Container;
		/*Cerebrum.Reflection.TableDescriptor m_Table;
		public TableView(Cerebrum.IContainer sector, Cerebrum.Reflection.TableDescriptor table) : base()
		{
			using(Cerebrum.Runtime.NativeVector av = table.GetSelectedAttributesVector(), cv = table.GetSelectedComponentsVector())
			{
				Init(sector, av, cv);
			}
			m_Table = table;
		}*/

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_Container!=null)
				{
					m_Container.Dispose();
					m_Container = null;
				}
				if(m_TableDescConn!=null)
				{
					m_TableDescConn.Dispose();
					m_TableDescConn = null;
				}
			}
			base.Dispose (disposing);
		}

		public TableView(Cerebrum.IContainer sector, Cerebrum.IConnector tableConn) : base()
		{
			using(Cerebrum.Runtime.NativeWarden rows = (tableConn.Component as Cerebrum.Reflection.TableDescriptor).GetSelectedComponentsVector())
			{
				m_Container = (Cerebrum.Runtime.NativeWarden)((System.ICloneable)sector).Clone();
				Init(rows);
//				m_Components = (Cerebrum.Runtime.NativeVector)((System.ICloneable)rows).Clone();
//				m_Attributes = null;
			}
			m_TableDescConn = (tableConn.Component as Cerebrum.Reflection.TableDescriptor).GetConnector();
		}

		public Cerebrum.Reflection.TableDescriptor TableDescriptor
		{
			get
			{
				return /*m_TableDescConn==null?null:*/(m_TableDescConn.Component as Cerebrum.Reflection.TableDescriptor);
			}
		}


		public override bool AllowNew
		{
			get
			{
				return true;
			}
		}

		public override Cerebrum.Data.ComponentItemView AddNew()
		{
			return AddNew((m_Container as Cerebrum.Runtime.NativeSector).NextSequence());
		}


		public Cerebrum.Data.ComponentItemView AddNew(Cerebrum.ObjectHandle h)
		{
			return AddNew(h, this.TableDescriptor.ComponentTypeIdHandle);
			/*using(Cerebrum.IConnector connector = m_Container.CreateConnector(h, m_Table.ComponentTypeIdHandle, Cerebrum.Runtime.KernelObjectKindId.Warden))
			{
				object o = this[this.Add(h)];
				this.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemAdded, this.Count - 1/*(int)this.IndexOf(h)* /));
				return o;
			}*/
		}

		public Cerebrum.Data.ComponentItemView AddNew(Cerebrum.ObjectHandle h, Cerebrum.ObjectHandle typeId)
		{
			using(Cerebrum.IComposite connector = m_Container.CreateConnector(h, typeId))
			{
				Cerebrum.Data.ComponentItemView o = this[this.EngageConnector(h, connector)];
				return o;
			}
		}

		public override string GetListName(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			if(listAccessors==null)
			{
				return this.TableDescriptor.Name;
			}
			return base.GetListName(listAccessors);
		}


		public override System.ComponentModel.PropertyDescriptorCollection GetItemProperties(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			Cerebrum.Reflection.TableDescriptor tbl = this.TableDescriptor;
			return tbl==null?null:tbl.InternalView.m_Attributes; //base.GetItemProperties (listAccessors);
		}

	}

#warning "remove not using m_NewRow"
	public class VectorView : SimpleView
	{
		protected internal Cerebrum.Runtime.NativeWarden m_Container;
		protected System.ComponentModel.PropertyDescriptorCollection m_Attributes;// = new System.ComponentModel.PropertyDescriptorCollection(new System.ComponentModel.PropertyDescriptor[0]);
		//Cerebrum.Runtime.NativeVector m_Vector;
		public VectorView(Cerebrum.IContainer sector, Cerebrum.Runtime.NativeWarden attributes, Cerebrum.Runtime.NativeWarden components) : base()
		{
			m_Container = (Cerebrum.Runtime.NativeWarden)((System.ICloneable)sector).Clone();
			if(attributes!=null)
			{
				m_Attributes = SimpleView.DescriptorsFromVector(attributes);
			}
			else
			{
				m_Attributes = null;
			}
			Init(components);
			//m_Vector = components;
		}

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_Container!=null)
				{
					m_Container.Dispose();
					m_Container = null;
				}
				if(m_Attributes!=null)
				{
					foreach(object o in m_Attributes)
					{
						System.IDisposable d = o as System.IDisposable;
						if(d!=null) d.Dispose();
					}
					m_Attributes = null;
				}
			}
			base.Dispose (disposing);
		}

		public override bool AllowNew
		{
			get
			{
				return true;
			}
		}

		internal ComponentItemView m_NewRow;

		public override Cerebrum.Data.ComponentItemView AddNew()
		{
			m_NewRow = this.CreateItemViewByObjectHandle(Cerebrum.ObjectHandle.Null);
			this.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemAdded, this.Count/* - 1(int)this.IndexOf(h)*/));
			return m_NewRow;
		}

		public override Cerebrum.Data.ComponentItemView this[int index]
		{
			get
			{
#if DEBUG
				//because m_Components.Count can differ from base.Count
				//it should blow up in debug mode
				if(base.Count != m_Components.Count)
				{
					throw new Exception("base.Count != m_Components.Count");
				}
#endif
				if(index < base.Count)
				{
					return base[index];
				}
				else
				{
					return m_NewRow;
				}
			}
			set
			{
				base[index] = value;
			}
		}

		public override int Count
		{
			get
			{
				if(m_NewRow==null)
				{
					return base.Count;
				}
				else
				{
					return base.Count + 1;
				}
			}
		}

		public override int IndexOf(object value)
		{
			if(value == m_NewRow)
			{
				return base.Count;
			}
			else
			{
				return base.IndexOf (value);
			}
		}

		public override System.ComponentModel.PropertyDescriptorCollection GetItemProperties(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			/*if(Rows.Count>0)
				{
					DataRowView rv = Rows[0] as DataRowView;
					return rv.GetProperties(null);
				}*/
			return m_Attributes;
		}

	}
}
