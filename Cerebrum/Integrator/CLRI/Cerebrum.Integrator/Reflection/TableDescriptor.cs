// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Reflection
{
	/// <summary>
	/// Summary description for TableDescriptor.
	/// </summary>
	public class TableDescriptor : MemberDescriptor, System.ComponentModel.IListSource
	{
		public TableDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//


		}
		

		public Cerebrum.Runtime.NativeWarden GetSelectedAttributesVector()
		{
			return this.GetAttributeContainer(Cerebrum.Specialized.Concepts.SelectedAttributesAttribute) as Cerebrum.Runtime.NativeWarden;
		}

		public Cerebrum.Runtime.NativeWarden GetSelectedComponentsVector()
		{
			return this.GetAttributeContainer(Cerebrum.Specialized.Concepts.SelectedComponentsAttribute) as Cerebrum.Runtime.NativeWarden;
		}

		public Cerebrum.ObjectHandle ComponentTypeIdHandle
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute);
				return o==null?Cerebrum.ObjectHandle.Null:(Cerebrum.ObjectHandle)o;
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute, value);
			}
		}

		protected System.Type m_ComponentType;
		public System.Type ComponentType
		{
			get
			{
				if(m_ComponentType==null)
				{
					Cerebrum.ObjectHandle h = this.ComponentTypeIdHandle;

					using(Cerebrum.IConnector c = this.m_Connector.Workspace.AttachConnector(h))
					{
						System.Object o = c.Component;
						m_ComponentType = (o as TypeDescriptor).ComponentType;
					}
				}
				return m_ComponentType;
			}
		}

/*		protected System.WeakReference m_DefaultViewWeak;
		
		public Cerebrum.Data.TableView DefaultView
		{
			get
			{
				Cerebrum.Data.TableView view = null;
				view = m_DefaultViewWeak==null?null:(m_DefaultViewWeak.Target as Cerebrum.Data.TableView);
				if(view==null)
				{
					using(Cerebrum.IConnector dc = this.m_Connector.GetDomain())
					{
						using(Cerebrum.Runtime.INativeDomain nd = dc.GetContainer() as Cerebrum.Runtime.INativeDomain)
						{
							using(Cerebrum.IContainer sector = nd.GetSector())
							{
								view = new Cerebrum.Data.TableView(sector, this);
								view.Disposing += new EventHandler(view_Disposing);
							}
						}
					}
					m_DefaultViewWeak = new WeakReference(view);
				}
				return view;
			}
		}

		public void ResetDefaultView()
		{
			m_DefaultViewWeak = null;
		}

		private void view_Disposing(object sender, EventArgs e)
		{
			if(sender is Cerebrum.IDisposable)
			{
				((Cerebrum.IDisposable)sender).Disposing -= new EventHandler(view_Disposing);
			}
			if(m_DefaultViewWeak!=null && m_DefaultViewWeak.Target==sender)
			{
				m_DefaultViewWeak = null;
			}
		}
*/
//		protected Cerebrum.Data.TableView m_DefaultView;
		private Cerebrum.Data.TableViewInternal m_InternalView = null;
		internal Cerebrum.Data.TableViewInternal InternalView
		{
			get
			{
				if(m_InternalView==null)
				{
					m_InternalView = new Cerebrum.Data.TableViewInternal(this);
				}
				return m_InternalView;
			}
		}
		
		public Cerebrum.Data.TableView GetTableView()
		{
				Cerebrum.Data.TableView view = null;
				using(Cerebrum.IContainer sector = this.m_Connector.Workspace.GetSector())
				{
					view = new Cerebrum.Data.TableView(sector, this.m_Connector);
				}
				return view;
		}

		public void ResetDefaultView()
		{
			m_InternalView = null;
		}
		
//		private void view_Disposing(object sender, EventArgs e)
//		{
//			if(sender is Cerebrum.IDisposable)
//			{
//				((Cerebrum.IDisposable)sender).Disposing -= new EventHandler(view_Disposing);
//			}
//			if(m_DefaultView==sender)
//			{
//				ResetDefaultView();
//			}
//		}

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_InternalView!=null)
				{
					m_InternalView.Dispose();
					m_InternalView = null;
				}
			}
			base.Dispose (disposing);
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						warden.Newobj(Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
						using(Cerebrum.IComposite composite = warden.AttachConnector(Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute))
						{
							composite.Component = Cerebrum.Specialized.Concepts.WardenNodeTypeId;
						}

						//view
						warden.Newobj(Cerebrum.Specialized.Concepts.SelectedAttributesAttribute, Cerebrum.Runtime.KernelObjectKindId.Warden);
						warden.Newobj(Cerebrum.Specialized.Concepts.SelectedComponentsAttribute, Cerebrum.Runtime.KernelObjectKindId.Warden);
					}
					break;
				}
			}
		}

		public Cerebrum.IConnector GetDisplayFieldAttribute()
		{
			object o = GetAttributeComponent(Cerebrum.Specialized.Concepts.DisplayFieldIdAttribute);
			if(o!=null)
			{
				Cerebrum.ObjectHandle h = (Cerebrum.ObjectHandle)o;
				return this.m_Connector.Workspace.AttachConnector(h);
			}
			return null;
		}

		public static Cerebrum.IConnector CreateTable(Cerebrum.IContainer sector, Cerebrum.ObjectHandle handle, Cerebrum.ObjectHandle []descriptors)
		{
			Cerebrum.IConnector connector = sector.CreateConnector(handle, Cerebrum.Specialized.Concepts.TableDescriptorTypeId);
			TableDescriptor descriptor = (TableDescriptor)connector.Component;

			Cerebrum.Runtime.NativeWarden warden = connector as Cerebrum.Runtime.NativeWarden;
			InitView(sector, warden, descriptors);
			return connector;
		}
		/*public static void CreateView(Cerebrum.Runtime.NativeWarden warden, Cerebrum.ObjectHandle []descriptors)
		{
			warden.Newobj(Cerebrum.Specialized.Concepts.SelectedAttributesAttribute, Cerebrum.Runtime.KernelObjectKindId.Warden);
			warden.Newobj(Cerebrum.Specialized.Concepts.SelectedComponentsAttribute, Cerebrum.Runtime.KernelObjectKindId.Warden);
			InitView(warden, descriptors);
		}*/

		public static void InitView(Cerebrum.IContainer sector, Cerebrum.Runtime.NativeWarden warden, Cerebrum.ObjectHandle []descriptors)
		{
			using(Cerebrum.IContainer container = warden.AttachConnector(Cerebrum.Specialized.Concepts.SelectedAttributesAttribute) as Cerebrum.IContainer)
			{
				for(int i=0;i<descriptors.Length;i++)
				{
					Cerebrum.ObjectHandle descriptor = descriptors[i];
					//vector.SetMap(descriptor, Cerebrum.Runtime.MapAccess.Scalar, new Cerebrum.ObjectHandle(1));
					using(Cerebrum.IComposite c2 = sector.AttachConnector(descriptor))
					{
						container.EngageConnector(descriptor, c2);
					}
				}
			}
		}


		#region IListSource Members

		public System.Collections.IList GetList()
		{
			return this.GetTableView();
		}

		public bool ContainsListCollection
		{
			get
			{
				return false;
			}
		}

		#endregion
	}
}
