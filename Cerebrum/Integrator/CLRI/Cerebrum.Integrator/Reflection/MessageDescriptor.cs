// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Reflection
{
	/// <summary>
	/// Summary description for MessageDescriptor.
	/// </summary>
	public class MessageDescriptor : MemberDescriptor
	{
		public MessageDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		public static Cerebrum.IConnector Create(Cerebrum.Runtime.NativeSector sector, Cerebrum.ObjectHandle attrHandle)
		{
			return sector.CreateConnector(attrHandle, Cerebrum.Specialized.Concepts.MessageDescriptorTypeId);
		}

	}
}
