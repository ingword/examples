// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Reflection
{
	/// <summary>
	/// Summary description for AttributeDescriptor.
	/// </summary>
	public class AttributeDescriptor : MemberDescriptor, IAttributeDescriptor
	{
		public AttributeDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#region IAttributeDescriptor Members

		public Cerebrum.ObjectHandle AttributeTypeIdHandle
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute);
				if(o==null)
				{
					return Cerebrum.ObjectHandle.Null;
				}
				else
				{
					return (Cerebrum.ObjectHandle)o;
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute, value);
			}
		}

		public bool Derived
		{
			get
			{
				return false;
			}
		}
		#endregion

		protected System.Type m_AttributeType;
		public System.Type AttributeType
		{
			get
			{
				if(m_AttributeType==null)
				{
					using(Cerebrum.IConnector c = GetTypeDescriptor())
					{
						if(c==null)
						{
							m_AttributeType = typeof(object);
						}
						else
						{
							System.Object o = c.Component;
							m_AttributeType = (o as TypeDescriptor).ComponentType;
						}
					}
				}
				return m_AttributeType;
			}
		}

		public Cerebrum.IConnector GetTypeDescriptor()
		{
			Cerebrum.ObjectHandle h = this.AttributeTypeIdHandle;

			return this.m_Connector.Workspace.AttachConnector(h);
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						warden.Newobj(Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
						using(Cerebrum.IComposite composite = warden.AttachConnector(Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute))
						{
							composite.Component = Cerebrum.Specialized.Concepts.ScalarNodeTypeId;
						}
					}
					break;
				}
			}
		}

		public static Cerebrum.IConnector CreateInstance(Cerebrum.IContainer container, Cerebrum.ObjectHandle attrHandle)
		{
			return container.CreateConnector(attrHandle, Cerebrum.Specialized.Concepts.AttributeDescriptorTypeId);
		}
	}





	public interface IAttributeDescriptor
	{
		string Name {get;}
		string DisplayName {get;}
		Cerebrum.ObjectHandle AttributeTypeIdHandle{get;set;}
		System.Type AttributeType {get;}
		Cerebrum.IConnector GetTypeDescriptor();

		/// <summary>
		/// true when attribute is derived from another instance
		/// </summary>
		bool Derived {get;}
	}



}
