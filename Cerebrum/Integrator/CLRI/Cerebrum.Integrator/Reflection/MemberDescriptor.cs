// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Reflection
{
	/// <summary>
	/// Summary description for MemberDescriptor.
	/// </summary>
	public class MemberDescriptor : Cerebrum.Integrator.GenericComponent
	{
		public MemberDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string Name
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Specialized.Concepts.NameAttribute));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.NameAttribute, value);
			}
		}

		public string DisplayName
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Specialized.Concepts.DisplayNameAttribute));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.DisplayNameAttribute, value);
			}
		}

		public string Description
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Specialized.Concepts.DescriptionAttribute));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.DescriptionAttribute, value);
			}
		}


		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						warden.Newobj(Cerebrum.Specialized.Concepts.NameAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
						//warden.Newobj(Cerebrum.Specialized.Concepts.DisplayNameAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
						//warden.Newobj(Cerebrum.Specialized.Concepts.DescriptionAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
					}
					break;
				}
			}
		}
	}
}
