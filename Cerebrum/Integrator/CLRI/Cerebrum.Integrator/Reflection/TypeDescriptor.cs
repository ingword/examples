// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Reflection
{
	/// <summary>
	/// Summary description for TypeDescriptor.
	/// </summary>
	public class TypeDescriptor : MemberDescriptor
	{
		public TypeDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		
		public string QualifiedTypeName
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Specialized.Concepts.QualifiedTypeNameAttribute));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.QualifiedTypeNameAttribute, value);
			}
		}
		
		public Cerebrum.ObjectHandle KernelObjectKindId
		{
			get
			{
				return (Cerebrum.ObjectHandle)GetAttributeComponent(Cerebrum.Specialized.Concepts.KernelObjectKindIdAttribute);
			}
			set
			{
				SetAttributeComponent(Cerebrum.Specialized.Concepts.KernelObjectKindIdAttribute, value);
			}
		}
		
		protected System.Type m_SystemType;
		public System.Type ComponentType
		{
			get
			{
				if(m_SystemType==null)
				{
					string strQualifiedTypeName = QualifiedTypeName;
					if(strQualifiedTypeName==null || strQualifiedTypeName==string.Empty)
					{
						throw new System.ArgumentOutOfRangeException("QualifiedTypeName can't be empty");
					}
					m_SystemType = System.Type.GetType(strQualifiedTypeName);
					if(m_SystemType==null)
					{
						int sep = strQualifiedTypeName.IndexOf(',');
						if(sep >= 0)
						{
							string strAssemblyName = strQualifiedTypeName.Substring(sep + 1).Trim();
							if(strAssemblyName==null || strAssemblyName==string.Empty)
							{
								throw new System.ArgumentOutOfRangeException("AssemblyName can't be empty");
							}
							System.Reflection.Assembly [] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
							System.Reflection.Assembly assembly = null;
							foreach(System.Reflection.Assembly asm_tmp in assemblies)
							{
								if(strAssemblyName==asm_tmp.GetName().Name)
								{
									assembly = asm_tmp;
									break;
								}
							}
							if(assembly==null)
							{
								assembly = System.Reflection.Assembly.LoadWithPartialName(strAssemblyName);

								if(assembly==null)
								{
									string fileName = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(this.DomainContext.FileName), strAssemblyName) + ".dll";
									if(System.IO.File.Exists(fileName))
									{
										assembly = System.Reflection.Assembly.LoadFrom(fileName);
									}
								}
							}
							if(assembly==null)
							{
								throw new System.Exception("Could not find or load assembly '" + strAssemblyName + "'");
							}
							string typename = strQualifiedTypeName.Substring(0, sep);
							m_SystemType = assembly.GetType(typename);
						}
					}
				}
				return m_SystemType;
			}
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector(direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						warden.Newobj(Cerebrum.Specialized.Concepts.QualifiedTypeNameAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
						warden.Newobj(Cerebrum.Specialized.Concepts.KernelObjectKindIdAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
					}
					break;
				}
			}
		}

		/*public static TypeDescriptor Create(Cerebrum.Runtime.NativeSector sector, Cerebrum.ObjectHandle handle)
		{
			using(Cerebrum.IConnector connector = sector.CreateConnector(handle, Cerebrum.Specialized.Concepts.TypeDescriptorTypeId))
			{
				return (TypeDescriptor)connector.Component;
			}
		}*/	

		public static Cerebrum.IConnector Create(Cerebrum.Runtime.NativeSector sector, Cerebrum.ObjectHandle handle)
		{
			return sector.CreateConnector(handle, Cerebrum.Specialized.Concepts.TypeDescriptorTypeId);
		}	
	}
}
