// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Reflection
{
	/// <summary>
	/// Summary description for AttributeRelation.
	/// </summary>
	public class FieldDescriptor : Cerebrum.Integrator.GenericComponent, Cerebrum.Reflection.IAttributeDescriptor
	{
		public FieldDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#region IAttributeDescriptor Members

		public string Name
		{
			get
			{
				using(Cerebrum.IConnector connector = this.GetRelatedAttribute())
				{
					return (connector.Component as Cerebrum.Reflection.IAttributeDescriptor).Name;
				}
			}
		}

		public string DisplayName
		{
			get
			{
				using(Cerebrum.IConnector connector = this.GetRelatedAttribute())
				{
					return (connector.Component as Cerebrum.Reflection.IAttributeDescriptor).DisplayName;
				}
			}
		}

		public Cerebrum.ObjectHandle AttributeTypeIdHandle
		{
			get
			{
				using(Cerebrum.IConnector connector = this.GetRelatedAttribute())
				{
					return (connector.Component as Cerebrum.Reflection.IAttributeDescriptor).AttributeTypeIdHandle;
				}
			}
			set
			{
				using(Cerebrum.IConnector connector = this.GetRelatedAttribute())
				{
					(connector.Component as Cerebrum.Reflection.IAttributeDescriptor).AttributeTypeIdHandle = value;
				}
			}
		}

		public Type AttributeType
		{
			get
			{
				using(Cerebrum.IConnector connector = this.GetRelatedAttribute())
				{
					return (connector.Component as Cerebrum.Reflection.IAttributeDescriptor).AttributeType;
				}
			}
		}

		public Cerebrum.IConnector GetTypeDescriptor()
		{
			using(Cerebrum.IConnector connector = this.GetRelatedAttribute())
			{
				return (connector.Component as Cerebrum.Reflection.IAttributeDescriptor).GetTypeDescriptor();
			}
		}

		public bool Derived
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Specialized.Concepts.DerivedFlagAttribute);
				if(o==null)
				{
					return false;
				}
				else
				{
					return (bool)o;
				}
			}
		}

		#endregion

		public Cerebrum.IConnector GetRelatedAttribute()
		{
			Cerebrum.ObjectHandle h = this.DependentFieldId;

			return this.m_Connector.Workspace.AttachConnector(h);
		}

		public Cerebrum.ObjectHandle DependentFieldId
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.Specialized.Concepts.DependentFieldIdAttribute);
				if(o==null)
				{
					return Cerebrum.ObjectHandle.Null;
				}
				else
				{
					return (Cerebrum.ObjectHandle)o;
				}
			}
		}

		public Cerebrum.ObjectHandle PrecedentTableId
		{
			get
			{
				return (Cerebrum.ObjectHandle)this.GetAttributeComponent(Cerebrum.Specialized.Concepts.PrecedentTableIdAttribute);
			}
			set
			{
				this.SetAttributeComponent(Cerebrum.Specialized.Concepts.PrecedentTableIdAttribute, value);
			}
		}

		public Cerebrum.IConnector GetRelatedTable()
		{
			return this.m_Connector.Workspace.AttachConnector(this.PrecedentTableId);
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			using(Cerebrum.IContainerEvents events = this.GetChildComponents() as Cerebrum.IContainerEvents)
			{
				events.ConnectorAttach += new Cerebrum.ConnectorAttachEventHandler(OnAttachConnector);
			}
		}


		public static void OnAttachConnector(object sender, Cerebrum.ConnectorAttachEventArgs e)
		{
			if(!e.Handled && (e.PlasmaHandle==Cerebrum.Specialized.Concepts.NameAttribute || e.PlasmaHandle==Cerebrum.Specialized.Concepts.DisplayNameAttribute || e.PlasmaHandle==Cerebrum.Specialized.Concepts.DescriptionAttribute || e.PlasmaHandle==Cerebrum.Specialized.Concepts.DefaultTypeIdAttribute))
			{
				using(Cerebrum.IContainer container = (e.Container.Component as FieldDescriptor).GetRelatedAttribute() as Cerebrum.IContainer)
				{
					e.Dependent = container.AttachConnector(e.PlasmaHandle);
					e.Handled = true;
				}
			}
		}

	}
}
