// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Cerebrum.Desktop.Configuration	
{
	/// <summary>
	/// Summary description for ConfigColor.
	/// </summary>
	public class ConfigColor : Cerebrum.Windows.Forms.ControlComponent,IValueEditor
	{	

	

		private IValueSource m_valueSource;
		public IValueSource ValueSource
		{
			get
			{
				return m_valueSource;
			}
			set
			{			
				m_valueSource	=	value;		
				if(m_valueSource is Cerebrum.Desktop.Configuration.ComponentElement)
				{
					object objAllowTransparency = (m_valueSource as Cerebrum.Desktop.Configuration.ComponentElement).Parameters["AllowTransparency"];
					if(objAllowTransparency!=null && objAllowTransparency.GetType()==typeof(System.Boolean))
					{
						this.AllowTransparency = Convert.ToBoolean(objAllowTransparency);
					}
				}
				if (m_valueSource	!=	null &&	m_valueSource.Value	!=	null)
				{						
					m_color			=	(Color)m_valueSource.Value;		
					trackBar1.Value	=	m_color.A;	
					Alpha			=	m_color.A;
					ActiveNodeChange();
				}
				else 
				{						
					m_color			=	Color.Empty;
					trackBar1.Value	=	255;
					Alpha			=	255;
					ActiveNodeChange();
				}
			}
		}	

		private Cerebrum.Windows.Forms.ColorPanel colpan;
		private System.Windows.Forms.Label labelpanel2;
		private System.Windows.Forms.ColorDialog colorDialog1;
		private System.Windows.Forms.Button button2;	
		private System.Windows.Forms.TrackBar trackBar1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private Color m_color=Color.Empty;
		private Color color
		{
			get{return m_color;}
			set
			{
				m_color=System.Drawing.Color.FromArgb(Alpha,value);				
				m_valueSource.Value=m_color;
				ActiveNodeChange();
			}
		}


		public ConfigColor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		
			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.colpan = new Cerebrum.Windows.Forms.ColorPanel();
			this.labelpanel2 = new System.Windows.Forms.Label();
			this.colorDialog1 = new System.Windows.Forms.ColorDialog();
			this.button2 = new System.Windows.Forms.Button();
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			this.SuspendLayout();
			// 
			// colpan
			// 
			this.colpan.CustomColors = new System.Drawing.Color[] {
																	  System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(192)), ((System.Byte)(192))),
																	  System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(128)), ((System.Byte)(128))),
																	  System.Drawing.Color.Red,
																	  System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0))),
																	  System.Drawing.Color.Maroon,
																	  System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(255)), ((System.Byte)(255))),
																	  System.Drawing.Color.FromArgb(((System.Byte)(128)), ((System.Byte)(255)), ((System.Byte)(255))),
																	  System.Drawing.Color.Aqua,
																	  System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(192)), ((System.Byte)(192))),
																	  System.Drawing.Color.Teal,
																	  System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(192)), ((System.Byte)(255))),
																	  System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(128)), ((System.Byte)(255))),
																	  System.Drawing.Color.Magenta,
																	  System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(192))),
																	  System.Drawing.Color.Purple,
																	  System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(192))),
																	  System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(128))),
																	  System.Drawing.Color.Yellow,
																	  System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(0))),
																	  System.Drawing.Color.Olive};
			this.colpan.ForeColor = System.Drawing.Color.Transparent;
			this.colpan.Location = new System.Drawing.Point(8, 8);
			this.colpan.Name = "colpan";
			this.colpan.Size = new System.Drawing.Size(148, 260);
			this.colpan.TabIndex = 0;
			this.colpan.TabStop = false;
			this.colpan.ColorChanged += new Cerebrum.Windows.Forms.ColorChangedEventHandler(this.colpan_ColorChanged);
			// 
			// labelpanel2
			// 
			this.labelpanel2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.labelpanel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.labelpanel2.Location = new System.Drawing.Point(80, 280);
			this.labelpanel2.Name = "labelpanel2";
			this.labelpanel2.Size = new System.Drawing.Size(112, 64);
			this.labelpanel2.TabIndex = 5;
			this.labelpanel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// colorDialog1
			// 
			this.colorDialog1.AnyColor = true;
			this.colorDialog1.FullOpen = true;
			this.colorDialog1.SolidColorOnly = true;
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Gainsboro;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Location = new System.Drawing.Point(8, 280);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(64, 64);
			this.button2.TabIndex = 8;
			this.button2.TabStop = false;
			this.button2.Text = "Other...";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// trackBar1
			// 
			this.trackBar1.Location = new System.Drawing.Point(168, 8);
			this.trackBar1.Maximum = 255;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.trackBar1.Size = new System.Drawing.Size(42, 260);
			this.trackBar1.TabIndex = 9;
			this.trackBar1.TabStop = false;
			this.trackBar1.TickFrequency = 5;
			this.trackBar1.Value = 255;
			this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
			// 
			// ConfigColor
			// 
			this.Controls.Add(this.trackBar1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.labelpanel2);
			this.Controls.Add(this.colpan);
			this.Name = "ConfigColor";
			this.Size = new System.Drawing.Size(200, 380);
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void button2_Click(object sender, System.EventArgs e)
		{
			DialogResult res = colorDialog1.ShowDialog();
			if (res == DialogResult.OK) 
			{
				color=System.Drawing.Color.FromArgb(Alpha,colorDialog1.Color);
				ActiveNodeChange();				
			}
		}
		private void colpan_ColorChanged(object sender, Cerebrum.Windows.Forms.ColorChangedEventArgs e)
		{
			color=System.Drawing.Color.FromArgb(Alpha,e.Color);			
		}
		private bool ActiveNodeChange()
		{
			SetControlColor( labelpanel2, m_color );		
			return true;
		}


		private void SetControlColor(Control ctrl, Color c)
		{
			ctrl.BackColor = c;	
			string s = string.Format( "{0}, {1:X}", c.Name, c.ToArgb() );
			ctrl.Text = s;
			ctrl.ForeColor = ( c.GetBrightness() < 0.3 ) ? (Color.White) : (Color.Black);
		}


		
		private System.Byte m_Alpha;
		public System.Byte Alpha
		{
			get
			{
				if(this.AllowTransparency)
				{
					return m_Alpha;
				}
				else
				{
					return 255;
				}
			}
			set
			{
				m_Alpha=value;
				ActiveNodeChange();
			}
		}
		
		private void trackBar1_Scroll(object sender, System.EventArgs e)
		{		
			Alpha	=	(byte)((System.Windows.Forms.TrackBar)sender).Value;
			color	=	System.Drawing.Color.FromArgb(Alpha,m_color);		
		}


		public bool AllowTransparency
		{
			get
			{
				return trackBar1.Visible;
			}
			set
			{
				trackBar1.Visible = value;
			}
		}

		

	}
}
