// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Cerebrum.Desktop.Configuration
{
	/// <summary>
	/// Summary description for ConfigImage.
	/// </summary>
	public class ConfigString :Cerebrum.Windows.Forms.ControlComponent,IValueEditor
	{	

		#region  interface IValueEditorEx	� ��� ����� ����������� ���������� ������� ����� �������� � ���� ���������� ����	

		public IWizardEditor GetWizardEditor
		{
			get{return null;}
		} 		
		
		private IValueSource m_valueSource;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.RichTextBox rtbDescription;
		private System.Windows.Forms.GroupBox grpValue;
		private System.Windows.Forms.Label lblStringType;
		private System.Windows.Forms.TextBox txtStringType;

		public IValueSource ValueSource
		{
			get
			{
				return m_valueSource;
			}
			set
			{			
				m_valueSource=value;			
				if (m_valueSource!=null && m_valueSource.Value!=null)
				{
					txtStringType.Text	=	(string)m_valueSource.Value;				
				}

				//				if (m_valueSource==null)
				//				MessageBox.Show("m_valuesourse == null"); // �������������� ������
			
			
			}
		}	
		#endregion

		#region default
		
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ConfigString()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblDescription = new System.Windows.Forms.Label();
			this.rtbDescription = new System.Windows.Forms.RichTextBox();
			this.grpValue = new System.Windows.Forms.GroupBox();
			this.txtStringType = new System.Windows.Forms.TextBox();
			this.lblStringType = new System.Windows.Forms.Label();
			this.grpValue.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(8, 24);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(72, 20);
			this.lblDescription.TabIndex = 2;
			this.lblDescription.Text = "Description";
			// 
			// rtbDescription
			// 
			this.rtbDescription.Enabled = false;
			this.rtbDescription.Location = new System.Drawing.Point(8, 56);
			this.rtbDescription.Name = "rtbDescription";
			this.rtbDescription.Size = new System.Drawing.Size(216, 96);
			this.rtbDescription.TabIndex = 3;
			this.rtbDescription.Text = "Sample text";
			// 
			// grpValue
			// 
			this.grpValue.Controls.Add(this.txtStringType);
			this.grpValue.Controls.Add(this.lblStringType);
			this.grpValue.Location = new System.Drawing.Point(8, 176);
			this.grpValue.Name = "grpValue";
			this.grpValue.Size = new System.Drawing.Size(216, 104);
			this.grpValue.TabIndex = 5;
			this.grpValue.TabStop = false;
			this.grpValue.Text = "Value";
			// 
			// txtStringType
			// 
			this.txtStringType.Location = new System.Drawing.Point(24, 64);
			this.txtStringType.Name = "txtStringType";
			this.txtStringType.Size = new System.Drawing.Size(184, 20);
			this.txtStringType.TabIndex = 11;
			this.txtStringType.Text = "";
			this.txtStringType.TextChanged += new System.EventHandler(this.txtStringType_TextChanged);
			// 
			// lblStringType
			// 
			this.lblStringType.Location = new System.Drawing.Point(12, 42);
			this.lblStringType.Name = "lblStringType";
			this.lblStringType.Size = new System.Drawing.Size(80, 20);
			this.lblStringType.TabIndex = 1;
			this.lblStringType.Text = "String :";
			this.lblStringType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ConfigString
			// 
			this.Controls.Add(this.grpValue);
			this.Controls.Add(this.rtbDescription);
			this.Controls.Add(this.lblDescription);
			this.Name = "ConfigString";
			this.Size = new System.Drawing.Size(232, 380);
			this.grpValue.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
	

		
		#endregion			

		private void txtStringType_TextChanged(object sender, System.EventArgs e)
		{
			m_valueSource.Value	=	txtStringType.Text;		
		}
	
	

	
		
	}
}
