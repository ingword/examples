// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Desktop.Configuration
{
	/// <summary>
	/// Summary description for ElementsCollection.
	/// </summary>
	public class ElementsCollection: Cerebrum.Collections.ConcreteListBase, Cerebrum.Collections.ICollectionEvents
	{
		public ElementsCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public IConfigurationElement this[ int index ]  
		{
			get  
			{
				return( (IConfigurationElement) (this as System.Collections.IList)[index] );
			}
			set  
			{
				(this as System.Collections.IList)[index] = value;
			}
		}

		public IConfigurationElement this[ string caption ]  
		{
			get  
			{
				foreach(IConfigurationElement cfe in this)
				{
					if(cfe.Caption==caption) return cfe;
				}
				return null;
			}
		}

		public int Add( IConfigurationElement value )  
		{
			return( (this as System.Collections.IList).Add( value ) );
		}

		public virtual void AddRange(IConfigurationElement[] elements)
		{
			int num1;
			if (elements == null)
			{
				throw new ArgumentNullException("elements"); 
			}
			if (elements.Length > 0)
			{
				for (num1 = 0; (num1 < elements.Length); num1 = (num1 + 1))
				{
					this.Add(elements[num1]);
				}
			}
		}

		public int IndexOf( IConfigurationElement value )  
		{
			return( (this as System.Collections.IList).IndexOf( value ) );
		}

		public void Insert( int index, IConfigurationElement value )  
		{
			(this as System.Collections.IList).Insert( index, value );
		}

		public void Remove( IConfigurationElement value )  
		{
			(this as System.Collections.IList).Remove( value );
		}

		public void RemoveAt(int index)
		{
			this.OnRemove(index, this[index]);
 
		}
		public void Clear()
		{
			(this as System.Collections.IList).Clear();
		}

		public bool Contains( IConfigurationElement value )  
		{
			// If value is not of type IConfigurationElement, this will return false.
			return( (this as System.Collections.IList).Contains( value ) );
		}

//		protected override void OnClear()
//		{
//			/*if(this.Clearing!=null)
//			{
//				this.Clearing(this, System.EventArgs.Empty);
//			}*/
//			base.OnClear();
//			/*if(this.Cleared!=null)
//			{
//				this.Cleared(this, System.EventArgs.Empty);
//			}*/
//		}

		protected override void OnInsert(int index, object value)
		{
			if(this.Inserting!=null)
			{
				this.Inserting(this, new Cerebrum.Collections.CollectionItemChangeEventArgs(index, value));
			}
			base.OnInsert(index, value);
			if(this.Inserted!=null)
			{
				this.Inserted(this, new Cerebrum.Collections.CollectionItemChangeEventArgs(index, value));
			}
		}


		protected override void OnRemove(int index, object value)
		{
			if(this.Removing!=null)
			{
				this.Removing(this, new Cerebrum.Collections.CollectionItemChangeEventArgs(index, value));
			}
			base.OnRemove(index, value);
			if(this.Removed!=null)
			{
				this.Removed(this, new Cerebrum.Collections.CollectionItemChangeEventArgs(index, value));
			}
		}


		protected override void OnUpdate(int index, object value)
		{
			IConfigurationElement old = this.m_Items[index] as IConfigurationElement;
			if(this.Removing!=null)
			{
				this.Removing(this, new Cerebrum.Collections.CollectionItemChangeEventArgs(index, old));
			}
			if(this.Inserting!=null)
			{
				this.Inserting(this, new Cerebrum.Collections.CollectionItemChangeEventArgs(index, value));
			}
			base.OnUpdate(index, value);
			if(this.Removed!=null)
			{
				this.Removed(this, new Cerebrum.Collections.CollectionItemChangeEventArgs(index, old));
			}
			if(this.Inserted!=null)
			{
				this.Inserted(this, new Cerebrum.Collections.CollectionItemChangeEventArgs(index, value));
			}
		}

		protected override bool Validate(object value, bool throwException)
		{
			if ( !typeof(IConfigurationElement).IsAssignableFrom(value.GetType()) )
			{
				if(throwException)
				{
					throw new ArgumentException( "value must be inherited from type IConfigurationElement", "value" );
				}
				else
				{
					return false;
				}
			}
			return true;
		}

		#region ICollectionEvents Members

		//public event System.EventHandler Clearing;

		public event Cerebrum.Collections.CollectionItemChangeEventHandler Inserted;

		public event Cerebrum.Collections.CollectionItemChangeEventHandler Inserting;

		public event Cerebrum.Collections.CollectionItemChangeEventHandler Removing;

		//public event System.EventHandler Cleared;

		public event Cerebrum.Collections.CollectionItemChangeEventHandler Removed;

		#endregion
	}

}
