// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Desktop.Configuration
{
	/// <summary>
	/// Summary description for EditorFactory.
	/// </summary>
	public class ComponentFactory : Cerebrum.Integrator.GenericComponent, Cerebrum.Desktop.Configuration.IObjectFactory
	{
		public ComponentFactory()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		#region IObjectFactory Members

		public object CreateInstance(string componentTypeName)
		{
			Cerebrum.Integrator.DomainContext dc = this.DomainContext;
			Cerebrum.Integrator.DomainContext mc = (dc.Application as Cerebrum.Integrator.Application).MasterContext;
			Cerebrum.ObjectHandle typeIdHandle = Cerebrum.Management.Utilites.ResolveHandle(mc, "Types", "Name", componentTypeName);
			object instance = Cerebrum.Integrator.Utilites.CreateInstance(mc.Workspace, typeIdHandle, null);
			Cerebrum.IComponent component = instance as Cerebrum.IComponent;
			if(component!=null)
			{
				component.SetConnector(Cerebrum.SerializeDirection.None, new Cerebrum.Runtime.SurrogateComposite(dc.Workspace));
			}
			return instance;
		}

		#endregion
	}
}
