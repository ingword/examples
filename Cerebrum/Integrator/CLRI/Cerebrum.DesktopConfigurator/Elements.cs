// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Collections;

namespace Cerebrum.Desktop.Configuration
{

	public interface IValueSource
	{
		object Value{get;set;}
		event System.EventHandler ValueUpdated;
	}
	public interface IValueEditor
	{
		IValueSource ValueSource{get;set;}
	}
	public interface IConfigurable
	{
		object GetValue(string attributeName);
		void SetValue(string attributeName, object value);
		bool HasDefault(string attributeName);
	}

	public class ConfigureInstanceEventArgs : System.EventArgs 
	{
		public ConfigureInstanceEventArgs(/*Cerebrum.Integrator.UiCommand command, */object instance, object description)
		{
			this.Instance = instance;
			this.Description = description;
		}

		public object Instance;
		public object Description;
	}

	public interface IWizardSource:IValueEditor//��������� ����������� editor'a
	{
		IWizardEditor GetWizardEditor{get;}		
	}

	public interface IValueEditorEx:IValueEditor//��������� ����������� editor'a
	{
		bool IsValid{get;}		
	}
	


	public interface IWizardEditor
	{
		bool Next();
		bool Prev();
		bool IsFinish{get;}
		bool IsFirst{get;}
		bool IsNext{get;}
	}

	public interface IComponentTypeProvider
	{
		string GetComponentTypeName(System.Type componentType);
	}

	public interface IObjectFactory
	{
		object CreateInstance(string componentTypeName);
	}

	public interface IConfigurationElement:IValueSource,IComponentTypeProvider
	{		
		ElementsCollection Elements{get;}
		IConfigurationElement Parent{get;set;}	
		string Caption{get;}
		string Description{get;}
		System.Drawing.Image NormalNodeImage{get;}
		System.Drawing.Image SelectedNodeImage{get;}
		bool IsWizard{get;}
		bool Default{get;}
	}


	public interface ITransactee
	{
		void Accept(object sender, System.EventArgs e);
		void Reject(object sender, System.EventArgs e);
	}
	public interface ITransactor
	{
		event System.EventHandler Accepting;
		event System.EventHandler Rejecting;
	}


}
