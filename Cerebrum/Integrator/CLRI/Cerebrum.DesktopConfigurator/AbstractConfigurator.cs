// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Windows.Forms;
using System.Reflection;
using System.Collections;


namespace Cerebrum.Desktop.Configuration
{

	public class  ConfigurationElement:IConfigurationElement
	{
		public ConfigurationElement()
		{
			m_Elements = new ElementsCollection();
			m_Elements.Inserting += new Cerebrum.Collections.CollectionItemChangeEventHandler(m_Elements_Inserting);
			m_Elements.Removed += new Cerebrum.Collections.CollectionItemChangeEventHandler(m_Elements_Removed);

			m_Parameters = new Hashtable();
		}

		public string ComponentTypeName = null;// ����� ������� ��������	�������� public ��� �����	
		public virtual string GetComponentTypeName(System.Type componentType)
		{
			if(componentType!=null)
			{
				if(typeof(IValueEditor).IsAssignableFrom(componentType))
				{
					return ComponentTypeName;
				}
			}
			return null;
		}
		
		public  event System.EventHandler ValueUpdated;	
		protected virtual void OnValueUpdated(System.EventArgs e)
		{
			if(this.ValueUpdated!=null)
			{
				this.ValueUpdated(this, e);
			}
		}


		public bool Default
		{
			get{return GetDefault();}
		}

		protected virtual bool GetDefault()
		{
			return true;
		}


		public object Value
		{
			get
			{
				//����� � ������ ���� ���� ���� ���������� load
				return GetValue();
			}
			set
			{
				SetValue(value);	
			}
		}	
	
		protected virtual object GetValue()
		{
			return null;
		}
	
		protected virtual void SetValue(object value)
		{
		}
	

		private bool m_IsWizard=false;

		public bool IsWizard
		{
			get
			{
				return m_IsWizard;
			}
			set//temp ����� ������
			{
				m_IsWizard=value;
			}
		}

		protected string m_Caption;
		public string Caption
		{
			get{return m_Caption;}
			set{m_Caption=value;}
		}

		protected string m_Description;
		public string Description
		{
			get{return m_Description;}
			set{m_Description=value;}
		}

		/*--private string m_NodeNormalImageId;		
		public string NodeNormalImageId
		{
			get
			{				
				return m_NodeNormalImageId;
			}
			set
			{
				m_NodeNormalImageId=value;
			}
		}
		
		private string m_NodeSelectImageId;		
		public string NodeSelectImageId
		{
			get
			{				
				return m_NodeSelectImageId;
			}
			set{m_NodeSelectImageId=value;}
		}--*/
	
		public System.Drawing.Image NormalNodeImage
		{
			get
			{				
				return GetNormalNodeImage();
			}
		}
		
		public System.Drawing.Image SelectedNodeImage
		{
			get
			{				
				return GetSelectedNodeImage();
			}
		}

		protected System.Drawing.Image GetNormalNodeImage()
		{
			return null;
		}
		
		protected System.Drawing.Image GetSelectedNodeImage()
		{
			return null;
		}

		protected ElementsCollection m_Elements;
		public ElementsCollection Elements
		{
			get
			{
				return m_Elements;
			}
		}

		protected IConfigurationElement m_Parent;				
		public IConfigurationElement Parent
		{
			get{return m_Parent;}
			set
			{
				m_Parent=value;
			}
		}

		protected System.Collections.Hashtable m_Parameters;
		public System.Collections.Hashtable Parameters
		{
			get
			{
				return m_Parameters;
			}
		}

		private void m_Elements_Inserting(object sender, Cerebrum.Collections.CollectionItemChangeEventArgs e)
		{
			(e.Value as IConfigurationElement).Parent = this;
		}

		private void m_Elements_Removed(object sender, Cerebrum.Collections.CollectionItemChangeEventArgs e)
		{
			(e.Value as IConfigurationElement).Parent = null;
		}
	}


	/*--public class ResourceElement:ConfigurationElement
	{
		protected Cerebrum.Integrator.DomainContext m_DomainContext;
		public Cerebrum.Integrator.DomainContext DomainContext
		{
			get{return m_DomainContext;}
			set{m_DomainContext=value;}
		}
		protected string m_ResourceId;
		public string ResourceId
		{
			get{return m_ResourceId;}
			set{m_ResourceId=value;}
		}


		protected override object GetValue()
		{
			return this.m_DomainContext.GetSystemInstance(m_ResourceId);
		}

		protected override void SetValue(object value)
		{
			this.m_DomainContext.SetSystemResource(m_ResourceId, value);							
			this.OnValueUpdated(System.EventArgs.Empty);
		}

		protected override bool GetDefault()
		{
#warning "not optimized"
			return GetValue()==null;
		}

	}--*/
	
		
	public class ComponentElement:ConfigurationElement
	{	
		protected object m_Component;// ��������	
		public object Component// ��������	
		{
			get
			{
				return this.m_Component;
			}
			set
			{
				this.m_Component = value;
			}
		}
	}

	public class PropertyElement:ComponentElement
	{	

		public System.ComponentModel.PropertyDescriptor PropertyDescriptor;

		protected override object GetValue()
		{
			return this.PropertyDescriptor.GetValue(this.Component);
		}

		protected override void SetValue(object value)
		{
			this.PropertyDescriptor.SetValue(this.Component, value);
			this.OnValueUpdated(System.EventArgs.Empty);
		}

		protected override bool GetDefault()
		{
			return false;
		}


	}

	public class VariableElement:ComponentElement
	{	

		protected string m_AttributeId;// �������� �� ���
		public string AttributeId
		{
			get{return m_AttributeId; }
			set{m_AttributeId=value;}
		}

		public VariableElement(){}
		/*--public VariableElement(string caption)
		{
			m_Caption=caption;
		}--*/

#warning "not optimized"
		protected override object GetValue()
		{
			// true ��������� �� ��� ���� � ����� ������
			object v = (this.Component as Cerebrum.Desktop.Configuration.IConfigurable).GetValue(this.m_AttributeId);
			return v;
		}

		protected override void SetValue(object value)
		{
			(this.Component as Cerebrum.Desktop.Configuration.IConfigurable).SetValue(this.m_AttributeId, value);
			this.OnValueUpdated(System.EventArgs.Empty);
		}

		protected override bool GetDefault()
		{
			return (this.Component as Cerebrum.Desktop.Configuration.IConfigurable).HasDefault(this.m_AttributeId);
		}


	}

	public class AttributeElement:ConfigurationElement
	{	
		protected Cerebrum.Integrator.DomainContext m_DomainContext;
		public Cerebrum.Integrator.DomainContext DomainContext
		{
			get{return m_DomainContext;}
			set{m_DomainContext=value;}
		}

		protected Cerebrum.ObjectHandle m_AttributeHandle;// �������� �� ���
		public Cerebrum.ObjectHandle AttributeHandle
		{
			get{return m_AttributeHandle; }
			set{m_AttributeHandle=value;}
		}
		protected Cerebrum.ObjectHandle m_ComponentHanle;
		public Cerebrum.ObjectHandle ComponentHanle// ��������	
		{
			get{return m_ComponentHanle; }
			set{m_ComponentHanle=value;}
		}

		public AttributeElement(){}
		public AttributeElement(string caption)
		{
			m_Caption=caption;
		}

		protected override object GetValue()
		{
			using(Cerebrum.IConnector connector = this.m_DomainContext.AttachConnector(this.m_ComponentHanle))
			{
				object val1 = Cerebrum.Integrator.Utilites.ObtainComponent((Cerebrum.IContainer)connector, this.m_AttributeHandle);
				return val1;
			}
		}

		protected override void SetValue(object value)
		{
			using(Cerebrum.IConnector connector = this.m_DomainContext.AttachConnector(this.m_ComponentHanle))
			{
				Cerebrum.Integrator.Utilites.UpdateComponent((Cerebrum.IContainer)connector, this.m_AttributeHandle, value);
			}
			this.OnValueUpdated(System.EventArgs.Empty);
		}

		protected override bool GetDefault()
		{
			object val0 = this.GetValue();
			return (val0==null);
		}


	}

	/*--public class SystemVariableElement:ConfigurationElement
	{	

		protected string m_VariableId;// �������� �� ���
		public string VariableId
		{
			get{return m_VariableId; }
			set{m_VariableId=value;}
		}
		protected string m_InstanceId;// �������� �� ���
		public string InstanceId
		{
			get{return m_InstanceId; }
			set{m_InstanceId=value;}
		}

		public Cerebrum.Integrator.DomainContext m_DomainContext;
		public SystemVariableElement(){}
		public SystemVariableElement(Cerebrum.Integrator.DomainContext domainContext)
		{
			m_DomainContext=domainContext;
		}

#warning "not optimized"
		protected override object GetValue()
		{
			// true ��������� �� ��� ���� � ����� ������
			object val1 = SystemContext.GetSystemVariable(m_InstanceId,m_VariableId);
			return val1;			
		}

		protected override void SetValue(object value)
		{
			SystemContext.SetSystemVariable(m_InstanceId,m_VariableId,value);
			this.OnValueUpdated(System.EventArgs.Empty);
		}

		protected override bool GetDefault()
		{
			return GetValue()==null;
		}
	}--*/


	internal class BaseNode : TreeNode
	{
		private IConfigurationElement m_ConfigurationElement=null;//��������� �� ��������� ��� dom ��� ������������� ����� ����
		 
		public IConfigurationElement ConfigurationElement
		{
			get{return m_ConfigurationElement;}
		}


		protected BaseNode()
		{
		}

		public void Release()
		{
			if(m_ConfigurationElement!=null)
			{
				m_ConfigurationElement.ValueUpdated -= new EventHandler(ConfigurationElement_ValueUpdated);
				m_ConfigurationElement.Elements.Inserted -= new Cerebrum.Collections.CollectionItemChangeEventHandler(Elements_Inserted);
				m_ConfigurationElement.Elements.Removed -= new Cerebrum.Collections.CollectionItemChangeEventHandler(Elements_Removed);
				m_ConfigurationElement=null;
			}
		}

		public BaseNode( IConfigurationElement  configurationElement , System.Int32 imageIndex , System.Int32 selectedImageIndex ):base(configurationElement.Caption,imageIndex,selectedImageIndex)
		{				
			m_ConfigurationElement=configurationElement;
			m_ConfigurationElement.ValueUpdated += new EventHandler(ConfigurationElement_ValueUpdated);
			m_ConfigurationElement.Elements.Inserted += new Cerebrum.Collections.CollectionItemChangeEventHandler(Elements_Inserted);
			m_ConfigurationElement.Elements.Removed += new Cerebrum.Collections.CollectionItemChangeEventHandler(Elements_Removed);
		}		

	
		public void Update()
		{
			//System.Drawing.Font f;
			if (m_ConfigurationElement.Default)
			{
				//this.NodeFont=this.TreeView.Font;;//font;
				this.ForeColor = System.Drawing.Color.DimGray;//this.TreeView.ForeColor;
			}
			else
			{
				//f= new System.Drawing.Font(this.TreeView.Font,System.Drawing.FontStyle.Bold);
				//this.NodeFont=f;//font;
				if (this.BackColor == System.Drawing.Color.MidnightBlue)
					this.ForeColor = System.Drawing.Color.White;
				else
					this.ForeColor = this.TreeView.ForeColor;//System.Drawing.Color.Black;
				//this.ForeColor = System.Drawing.Color.Navy;
			}
			//this.Text = this.Text;
		}

		private void ConfigurationElement_ValueUpdated(object sender, EventArgs e)
		{
			Update();
		}

		private void Elements_Inserted(object sender, Cerebrum.Collections.CollectionItemChangeEventArgs e)
		{
			(this.TreeView.FindForm() as Cerebrum.Desktop.Configuration.ConfiguratorDialog).GenerateNode(e.Value as IConfigurationElement, this);
			this.Expand();
		}

		private void Elements_Removed(object sender, Cerebrum.Collections.CollectionItemChangeEventArgs e)
		{
			foreach(BaseNode bn in this.Nodes)
			{
				if(bn.ConfigurationElement==e.Value)
				{
					this.Nodes.Remove(bn);
					bn.Release();
				}
			}
		}
	}


}
