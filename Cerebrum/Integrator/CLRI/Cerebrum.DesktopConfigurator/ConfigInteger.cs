// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Cerebrum.Desktop.Configuration
{
	/// <summary>
	/// Summary description for ConfigImage.
	/// </summary>
	public class ConfigInteger :Cerebrum.Windows.Forms.ControlComponent,IValueEditor
	{	

		#region  interface IValueEditorEx	� ��� ����� ����������� ���������� ������� ����� �������� � ���� ���������� ����	

		public IWizardEditor GetWizardEditor
		{
			get{return null;}
		} 		
		
		private IValueSource m_valueSource;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.RichTextBox rtbDescription;
		private System.Windows.Forms.GroupBox grpValue;
		public System.Windows.Forms.NumericUpDown nudInteger;
		private System.Windows.Forms.Label lblIntegerType;

		public IValueSource ValueSource
		{
			get
			{
				return m_valueSource;
			}
			set
			{			
				m_valueSource=value;			
				if (m_valueSource!=null && m_valueSource.Value!=null)
				{
					nudInteger.Value	=	(int)m_valueSource.Value;				
				}
				else 
					nudInteger.Value	=	0;


				//				if (m_valueSource==null)
				//				MessageBox.Show("m_valuesourse == null"); // �������������� ������
			
			
			}
		}	
		#endregion

		#region default
		
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ConfigInteger()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblDescription = new System.Windows.Forms.Label();
			this.rtbDescription = new System.Windows.Forms.RichTextBox();
			this.grpValue = new System.Windows.Forms.GroupBox();
			this.nudInteger = new System.Windows.Forms.NumericUpDown();
			this.lblIntegerType = new System.Windows.Forms.Label();
			this.grpValue.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudInteger)).BeginInit();
			this.SuspendLayout();
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(8, 24);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(72, 20);
			this.lblDescription.TabIndex = 2;
			this.lblDescription.Text = "Description";
			// 
			// rtbDescription
			// 
			this.rtbDescription.Enabled = false;
			this.rtbDescription.Location = new System.Drawing.Point(8, 56);
			this.rtbDescription.Name = "rtbDescription";
			this.rtbDescription.Size = new System.Drawing.Size(216, 96);
			this.rtbDescription.TabIndex = 3;
			this.rtbDescription.Text = "Sample text";
			// 
			// grpValue
			// 
			this.grpValue.Controls.Add(this.nudInteger);
			this.grpValue.Controls.Add(this.lblIntegerType);
			this.grpValue.Location = new System.Drawing.Point(8, 176);
			this.grpValue.Name = "grpValue";
			this.grpValue.Size = new System.Drawing.Size(216, 104);
			this.grpValue.TabIndex = 5;
			this.grpValue.TabStop = false;
			this.grpValue.Text = "Value";
			// 
			// nudInteger
			// 
			this.nudInteger.Location = new System.Drawing.Point(108, 42);
			this.nudInteger.Maximum = new System.Decimal(new int[] {
																	   9999,
																	   0,
																	   0,
																	   0});
			this.nudInteger.Minimum = new System.Decimal(new int[] {
																	   9999,
																	   0,
																	   0,
																	   -2147483648});
			this.nudInteger.Name = "nudInteger";
			this.nudInteger.Size = new System.Drawing.Size(96, 20);
			this.nudInteger.TabIndex = 10;
			this.nudInteger.ValueChanged += new System.EventHandler(this.nudInteger_ValueChanged);
			this.nudInteger.Leave += new System.EventHandler(this.nudInteger_Leave);
			// 
			// lblIntegerType
			// 
			this.lblIntegerType.Location = new System.Drawing.Point(12, 42);
			this.lblIntegerType.Name = "lblIntegerType";
			this.lblIntegerType.Size = new System.Drawing.Size(80, 20);
			this.lblIntegerType.TabIndex = 1;
			this.lblIntegerType.Text = "Integer Type :";
			this.lblIntegerType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ConfigInteger
			// 
			this.BackColor = System.Drawing.Color.Linen;
			this.Controls.Add(this.grpValue);
			this.Controls.Add(this.rtbDescription);
			this.Controls.Add(this.lblDescription);
			this.Name = "ConfigInteger";
			this.Size = new System.Drawing.Size(232, 380);
			this.grpValue.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.nudInteger)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	

		private void nudInteger_ValueChanged(object sender, System.EventArgs e)
		{
			if(m_valueSource!=null)
			{
				m_valueSource.Value	=	(int)nudInteger.Value;
			}
		}
		#endregion


		private void nudInteger_Leave(object sender, System.EventArgs e)
		{
			SmartUpdateValue();
		}

		private void SmartUpdateValue()
		{
			nudInteger.UpButton();
			nudInteger.DownButton();
		}
	
	

	
		
	}
}
