// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

//using System.Resources;
using System.IO;
using System.Diagnostics;
//using Asmex.FileViewer;

namespace Cerebrum.Desktop.Configuration
{
	
	/// <summary>
	/// Summary description for AddIndicator.
	/// </summary>
	public class ConfiguratorDialog : Cerebrum.Windows.Forms.DesktopComponent, Cerebrum.Desktop.Configuration.ITransactor
	{				
		private System.Windows.Forms.Panel panel_treeview;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.ImageList imageList_Treview;
		private System.Windows.Forms.Panel panel_for_editor;
		private System.Windows.Forms.Panel panel5;

		private IWizardEditor iwizard;
		private Hashtable SingletonHTControls= new Hashtable();//
		private System.ComponentModel.IContainer components;
	
	
		private object currentComponent=null;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnAccept;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnApply;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.Button btnFinish;
		private System.Windows.Forms.Button btnBack;
		private System.Windows.Forms.Panel panel6;		
		private System.Collections.Hashtable localTableVar = new Hashtable();
		private System.Windows.Forms.TreeView tvwConfiguration;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.Panel panel8;
		private System.Windows.Forms.Panel panel9;// ����� �� �� assembly	
		
		private IObjectFactory m_EditorFactory;
		public IObjectFactory EditorFactory
		{
			get
			{
				return m_EditorFactory;
			}
			set
			{
				m_EditorFactory = value;
			}
		}

		private IValueEditor/*Ex*/ m_ActiveValueEditor;// ������ ��� ������ ����
		protected IValueEditor ActiveValueEditor
		{
			get
			{
				return m_ActiveValueEditor;
			}
			set
			{
				if(m_ActiveValueEditor!=value)
				{
					if(m_ActiveValueEditor!=null)
					{
						/*ObjectiveDraw.Platform.ITransactee itr = this.m_ActiveValueEditor as ObjectiveDraw.Platform.ITransactee;
						if(itr!=null)
						{
							itr.Accept(this, System.EventArgs.Empty);
						}*/
						(m_ActiveValueEditor as System.Windows.Forms.Control).Visible = false;
						m_ActiveValueEditor.ValueSource = null;
					}
					m_ActiveValueEditor = value;
					if(m_ActiveValueEditor!=null)
					{
						(m_ActiveValueEditor as System.Windows.Forms.Control).Visible = true;
					}
				}
			}
		}

		public ConfiguratorDialog():base(Cerebrum.Windows.Forms.DesktopComponentDisposition.Modal)//Child)
		{			
			InitializeComponent();			
			Bitmap bmp=new Bitmap(1,1);
			imageList_Treview.Images.Add(bmp);// ���������� � imagelist 0-��������� ������ ��������				
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.tvwConfiguration = new System.Windows.Forms.TreeView();
			this.imageList_Treview = new System.Windows.Forms.ImageList(this.components);
			this.panel_treeview = new System.Windows.Forms.Panel();
			this.panel9 = new System.Windows.Forms.Panel();
			this.panel8 = new System.Windows.Forms.Panel();
			this.panel7 = new System.Windows.Forms.Panel();
			this.btnAccept = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnApply = new System.Windows.Forms.Button();
			this.btnNext = new System.Windows.Forms.Button();
			this.btnFinish = new System.Windows.Forms.Button();
			this.btnBack = new System.Windows.Forms.Button();
			this.panel_for_editor = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel6 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel_treeview.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel6.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// tvwConfiguration
			// 
			this.tvwConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvwConfiguration.ImageList = this.imageList_Treview;
			this.tvwConfiguration.Location = new System.Drawing.Point(0, 10);
			this.tvwConfiguration.Name = "tvwConfiguration";
			this.tvwConfiguration.ShowLines = false;
			this.tvwConfiguration.Size = new System.Drawing.Size(243, 473);
			this.tvwConfiguration.TabIndex = 1;
			this.tvwConfiguration.GotFocus += new System.EventHandler(this.tvwConfiguration_GotFocus);
			this.tvwConfiguration.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwConfiguration_AfterSelect);
			this.tvwConfiguration.LostFocus += new System.EventHandler(this.tvwConfiguration_LostFocus);
			// 
			// imageList_Treview
			// 
			this.imageList_Treview.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList_Treview.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// panel_treeview
			// 
			this.panel_treeview.Controls.Add(this.tvwConfiguration);
			this.panel_treeview.Controls.Add(this.panel9);
			this.panel_treeview.Controls.Add(this.panel8);
			this.panel_treeview.Controls.Add(this.panel7);
			this.panel_treeview.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel_treeview.Location = new System.Drawing.Point(0, 0);
			this.panel_treeview.Name = "panel_treeview";
			this.panel_treeview.Size = new System.Drawing.Size(250, 493);
			this.panel_treeview.TabIndex = 8;
			// 
			// panel9
			// 
			this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel9.Location = new System.Drawing.Point(243, 10);
			this.panel9.Name = "panel9";
			this.panel9.Size = new System.Drawing.Size(7, 473);
			this.panel9.TabIndex = 4;
			// 
			// panel8
			// 
			this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel8.Location = new System.Drawing.Point(0, 0);
			this.panel8.Name = "panel8";
			this.panel8.Size = new System.Drawing.Size(250, 10);
			this.panel8.TabIndex = 3;
			// 
			// panel7
			// 
			this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel7.Location = new System.Drawing.Point(0, 483);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(250, 10);
			this.panel7.TabIndex = 2;
			// 
			// btnAccept
			// 
			this.btnAccept.BackColor = System.Drawing.Color.Gainsboro;
			this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAccept.Location = new System.Drawing.Point(88, 0);
			this.btnAccept.Name = "btnAccept";
			this.btnAccept.TabIndex = 10;
			this.btnAccept.Text = "OK";
			this.btnAccept.Click += new System.EventHandler(this.button_ok_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Gainsboro;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(0, 0);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.button_Cancel_Click);
			// 
			// btnApply
			// 
			this.btnApply.BackColor = System.Drawing.Color.Gainsboro;
			this.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnApply.Location = new System.Drawing.Point(168, 0);
			this.btnApply.Name = "btnApply";
			this.btnApply.TabIndex = 12;
			this.btnApply.Text = "Apply";
			this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
			// 
			// btnNext
			// 
			this.btnNext.BackColor = System.Drawing.Color.Gainsboro;
			this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnNext.Location = new System.Drawing.Point(168, 0);
			this.btnNext.Name = "btnNext";
			this.btnNext.TabIndex = 11;
			this.btnNext.Text = "Next";
			this.btnNext.Click += new System.EventHandler(this.button_Next_Click);
			// 
			// btnFinish
			// 
			this.btnFinish.BackColor = System.Drawing.Color.Gainsboro;
			this.btnFinish.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnFinish.Location = new System.Drawing.Point(256, 0);
			this.btnFinish.Name = "btnFinish";
			this.btnFinish.TabIndex = 12;
			this.btnFinish.Text = "Finish";
			// 
			// btnBack
			// 
			this.btnBack.BackColor = System.Drawing.Color.Gainsboro;
			this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBack.Location = new System.Drawing.Point(88, 0);
			this.btnBack.Name = "btnBack";
			this.btnBack.TabIndex = 14;
			this.btnBack.Text = "Back";
			this.btnBack.Click += new System.EventHandler(this.button_back_Click);
			// 
			// panel_for_editor
			// 
			this.panel_for_editor.AutoScroll = true;
			this.panel_for_editor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel_for_editor.Location = new System.Drawing.Point(255, 0);
			this.panel_for_editor.Name = "panel_for_editor";
			this.panel_for_editor.Size = new System.Drawing.Size(425, 493);
			this.panel_for_editor.TabIndex = 19;
			// 
			// panel5
			// 
			this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel5.Location = new System.Drawing.Point(696, 0);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(16, 549);
			this.panel5.TabIndex = 21;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panel6);
			this.panel2.Controls.Add(this.panel1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 493);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(696, 56);
			this.panel2.TabIndex = 16;
			// 
			// panel6
			// 
			this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.panel6.Controls.Add(this.btnApply);
			this.panel6.Controls.Add(this.btnCancel);
			this.panel6.Controls.Add(this.btnAccept);
			this.panel6.Controls.Add(this.btnBack);
			this.panel6.Controls.Add(this.btnNext);
			this.panel6.Controls.Add(this.btnFinish);
			this.panel6.Location = new System.Drawing.Point(360, 16);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(332, 24);
			this.panel6.TabIndex = 15;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(152)), ((System.Byte)(152)), ((System.Byte)(102)));
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(696, 8);
			this.panel1.TabIndex = 13;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.panel_for_editor);
			this.panel3.Controls.Add(this.splitter1);
			this.panel3.Controls.Add(this.panel_treeview);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(16, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(680, 493);
			this.panel3.TabIndex = 17;
			// 
			// splitter1
			// 
			this.splitter1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(152)), ((System.Byte)(152)), ((System.Byte)(102)));
			this.splitter1.Location = new System.Drawing.Point(250, 0);
			this.splitter1.MinExtra = 250;
			this.splitter1.MinSize = 250;
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(5, 493);
			this.splitter1.TabIndex = 16;
			this.splitter1.TabStop = false;
			this.splitter1.SplitterMoving += new System.Windows.Forms.SplitterEventHandler(this.splitter1_SplitterMoving);
			// 
			// panel4
			// 
			this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(16, 493);
			this.panel4.TabIndex = 17;
			// 
			// ConfiguratorDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(212)), ((System.Byte)(208)), ((System.Byte)(200)));
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(712, 549);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel5);
			this.MinimumSize = new System.Drawing.Size(480, 480);
			this.Name = "ConfiguratorDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Configurator";
			this.panel_treeview.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel6.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		internal void GenerateNode(IConfigurationElement i)
		{
			if (i.Parent==null)
			{
				foreach(IConfigurationElement  ff in i.Elements)
				{	
					GenerateNode(ff, null);
				}
			}
		}
	
		internal BaseNode GenerateNode(IConfigurationElement i, BaseNode baseNode)
		{
			BaseNode bn;
			int i1=0;
			int i2=0;			
			Image img;
											
			img=i.NormalNodeImage;	
			if (img!=null)	
			{
				i1=imageList_Treview.Images.Add(img,Color.Transparent);
			}
			else i1=0;

						
			img= i.SelectedNodeImage;	
			if (img!=null)	
			{
				i2=imageList_Treview.Images.Add(img,Color.Transparent);
			}
			else i2=0;
			
			bn=new BaseNode(i,i1,i2);

			if (baseNode==null) tvwConfiguration.Nodes.Add(bn);
			else baseNode.Nodes.Add(bn);

			bn.Update();

			foreach(IConfigurationElement  ff in i.Elements)
			{	
				GenerateNode(ff,bn);
			}
			return bn;
		}

		/// <summary>
		///  ������� ���������� ���� ������ ������������� ��� � ������
		/// </summary>
		private void ActiveNode(BaseNode tn)
		{		
			if (((tn as BaseNode).ConfigurationElement as ConfigurationElement).ComponentTypeName!= null)
			{
				tvwConfiguration.SelectedNode = tn;
				return;
			}
			else
				if (tn.Nodes.Count>0)
			{				
				ActiveNode(tn.Nodes[0] as BaseNode);
				tn = tn.NextNode as BaseNode;
			}
		}
		
		private void tvwConfiguration_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			BaseNode bn = (BaseNode)e.Node;
			if (activeNodeLostFocus!= null)
			{
				activeNodeLostFocus.BackColor=tvwConfiguration.BackColor;
				if (activeNodeLostFocus.ConfigurationElement.Default)
				{										
					activeNodeLostFocus.ForeColor= System.Drawing.Color.DimGray;	
				}
				else
				{
					activeNodeLostFocus.ForeColor = tvwConfiguration.ForeColor;
				}
			}
			else tvwConfiguration_LostFocus(tvwConfiguration,EventArgs.Empty);

			activeNodeLostFocus = bn;

			string editorTypeName=bn.ConfigurationElement.GetComponentTypeName(typeof(IValueEditor));	
			if(bn.ConfigurationElement.IsWizard)
			{
				btnFinish.Visible = true;
				btnBack.Visible = true;
				btnNext.Visible = true;
				btnCancel.Visible = true;
				btnCancel.Location = new Point(0, 0);				
				btnAccept.Visible = false;
				btnApply.Visible = false;
				this.AcceptButton = btnFinish;
			}
			else
			{
				btnFinish.Visible = false;
				btnBack.Visible = false;
				btnNext.Visible = false;
				btnCancel.Visible = true;				
				btnAccept.Visible = true;
				btnApply.Visible = this.m_AllowApply;
				if (this.m_AllowApply)
					btnCancel.Location = new Point(256, 0);
				else 
					btnCancel.Location = new Point(190, 0);
				this.AcceptButton = btnAccept;
			}

			if (editorTypeName!=null && editorTypeName.Length > 0)
			{
				IValueEditor ive;
				if (!SingletonHTControls.ContainsKey(editorTypeName))
				{
					object obj= m_EditorFactory.CreateInstance(editorTypeName);// ObjectiveDraw.Platform.Workspace.InitializeInstance(this.DomainContext, editorTypeName, null);//������� ����� �������
					ive=obj as IValueEditor;	
					if (ive!=null)
					{					
						Cerebrum.Windows.Forms.ControlComponent m_cc=(Cerebrum.Windows.Forms.ControlComponent)obj;
						m_cc.Dock=DockStyle.Fill;							
						this.panel_for_editor.Controls.Add(m_cc);							
						SingletonHTControls[editorTypeName]=ive;	
					}
				}	
				else			
				{
					ive=SingletonHTControls[editorTypeName] as IValueEditor;//����� �� ������� ��� �������� �������				
					
				}

				if(ive!=null)
				{
					ive.ValueSource = bn.ConfigurationElement;// �������� ������� �������� �� ���� � ������� ���� ������ �� ����� ����������� ������� ���������� �� ����																
				}

				this.ActiveValueEditor = ive;
			}
			else 
				this.ActiveValueEditor = null;		
		}


		private void button_ok_Click(object sender, System.EventArgs e)
		{
			if(Apply())
			{
				if(this.m_ActiveValueEditor!=null) this.m_ActiveValueEditor.ValueSource = null;
				this.DialogResult=DialogResult.OK;
				this.Close();
			}
		}

		private void button_Cancel_Click(object sender, System.EventArgs e)
		{
			if(this.Rejecting!=null)
			{
				//--this.Rejecting(this, new ConfigureInstanceEventArgs(currentComponent.SystemContext.PrimaryWindow.GetCommand(ObjectiveDraw.Platform.KnownDrawerCommands.ConfigurationReject), currentComponent, null));
			}
			this.DialogResult=DialogResult.Cancel;
			this.Close();		
		}	
		
		protected bool m_AllowApply = true;
		public bool AllowApply
		{
			get
			{
				return m_AllowApply;
			}
			set
			{
				m_AllowApply = value;
			}
		}
		private void button_Next_Click(object sender, EventArgs e)
		{			
			#region rem
			//			Abstract.BaseNode bn = (Abstract.BaseNode)tvwConfiguration.SelectedNode;
			//
			//			string editorTypeName=bn.RefIConfigurationElement.GetComponentId(typeof(Abstract.IValueEditorEx));									
			//			ive=SingletonHTControls[editorTypeName] as Abstract.IValueEditorEx;
			#endregion
			IWizardSource ivex = this.m_ActiveValueEditor as IWizardSource;
			iwizard=ivex==null?null:ivex.GetWizardEditor;
			if (iwizard!=null)
			{
				btnBack.Enabled=!iwizard.IsFirst;
				btnFinish.Enabled=!iwizard.IsFinish;										
				iwizard.Next();			
			}
		}

		private void button_back_Click(object sender, System.EventArgs e)
		{
			IWizardSource ivex = this.m_ActiveValueEditor as IWizardSource;
			iwizard=ivex==null?null:ivex.GetWizardEditor;
			if (iwizard!=null)
			{
				btnBack.Enabled=!iwizard.IsFirst;
				btnFinish.Enabled=!iwizard.IsFinish;				
				iwizard.Prev();						
			}			
		}


		private void splitter1_SplitterMoving(object sender, System.Windows.Forms.SplitterEventArgs e)
		{	
//			this.Width-=(oldx-e.SplitX);
//			oldx=e.SplitX;			
		}
		
		#region 2 ����������� ��� ������� "�������� ������" ������ 
		private	BaseNode activeNodeLostFocus;
		private void tvwConfiguration_LostFocus(object sender, EventArgs e)
		{		
			activeNodeLostFocus = tvwConfiguration.SelectedNode as BaseNode;
			if (activeNodeLostFocus!=null)
			{			
				activeNodeLostFocus.BackColor=Color.MidnightBlue;
				activeNodeLostFocus.ForeColor=Color.White;
				tvwConfiguration.Invalidate();
			}
		}

		private void tvwConfiguration_GotFocus(object sender, EventArgs e)
		{		
			if (activeNodeLostFocus!=null)
			{		
				activeNodeLostFocus.BackColor=tvwConfiguration.BackColor;
				BaseNode bn = tvwConfiguration.SelectedNode as BaseNode;
				if ( bn!= null)
				{		
					if (bn.ConfigurationElement.Default)
					{
						activeNodeLostFocus.ForeColor= System.Drawing.Color.DimGray;
					}
					else
					{
						activeNodeLostFocus.ForeColor = tvwConfiguration.ForeColor;
					}
				}				
				else 
					activeNodeLostFocus.ForeColor= tvwConfiguration.ForeColor;		
				tvwConfiguration.Invalidate();
			}
		}
		#endregion		

		public void ExecuteInvoke(ConfigureInstanceEventArgs e)
		{
			currentComponent = e.Instance;// ��� ���� ��� ��������
			GenerateNode(e.Description as IConfigurationElement);// ������ ������	
			ActiveNode(tvwConfiguration.Nodes[0] as BaseNode); // ������������� �������� ������ �������������
			//--e.Cancel = true;					
			tvwConfiguration.ExpandAll();
		}

		private void btnApply_Click(object sender, System.EventArgs e)
		{
			Apply();
		}

		private bool Apply()
		{
			Cerebrum.Desktop.Configuration.IValueEditorEx ivex = this.m_ActiveValueEditor as Cerebrum.Desktop.Configuration.IValueEditorEx;
			if(ivex!=null)
			{
				if(!ivex.IsValid) return false;
			}
			else
			{
				if(this.m_ActiveValueEditor==null) return false;
			}
			Cerebrum.Desktop.Configuration.ITransactee itr = this.m_ActiveValueEditor as Cerebrum.Desktop.Configuration.ITransactee;
			if(itr!=null)
			{
				itr.Accept(this, System.EventArgs.Empty);
			}
			if(this.Accepting!=null)
			{
				//--this.Accepting(this, new ConfigureInstanceEventArgs(currentComponent.SystemContext.PrimaryWindow.GetCommand(ObjectiveDraw.Platform.KnownDrawerCommands.ConfigurationAccept), currentComponent, null));
			}
			return true;
		}

		

		

	

		#region ITransacter Members

		public event System.EventHandler Accepting;

		public event System.EventHandler Rejecting;

		#endregion
	}
}


#region rem init old
#region Abstract.VariableElement 
		
//		private Abstract.VariableElement vardata1;
//		private Abstract.VariableElement vardata2;
//		private Abstract.VariableElement vardata11;
//		private Abstract.VariableElement vardata12;
//		private Abstract.VariableElement vardata13;
//		private Abstract.VariableElement vardata14;
//		private Abstract.VariableElement vardata21;
	
//		private Abstract.VariableElement vardata15;		
#endregion			
//private void init()
//		{
//			getKnownDrawerVariables();// ������� ��� � ��� ���� � BC.ObjectiveDraw.Platform.KnowDrawerVariables- 
//
//			#region resdata
//			resdata = new ObjectiveDraw.Platform.Configurator.Abstract.ResourceElement();
//			resdata.SystemContext=SystemContext;
//			resdata.ResourceId="test";//temp
//			resdata.Caption="ResourceElement";
//			resdata.ComponentId="TradingPlatform.Configurator.ConfigImage";
//
//			#endregion			
//
//			root=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string)  "root");
//				
//			vardata=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string)  "GraphBackgroundImage");
//
//			vardata_=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string) "ADD Indicator");
//			vardata1=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string) "GraphBackgroundColor");
//			vardata15 =new Abstract.VariableElement((string)"ChartBackgroundColor");
//			vardata2=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string) "ChartHorizontalStrideColor");			
//			vardata11=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string)"ChartHorizontalDatumColor");
//			vardata12=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string)"ChartOrdinateTenColor");
//			vardata13=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string)"ChartOrdinateFiveColor");
//			vardata14=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string)"ChartOrdinateOneColor");
//			vardata21=new ObjectiveDraw.Platform.Configurator.Abstract.VariableElement((string)"ChartBarGrowColor");			
//
//			vardata.NodeSelectImageId="4DADA825-AD7D-44F6-8C85-7BFF61A0E9E7";
//			vardata.NodeNormalImageId="075eaab7-0f0e-4bfd-9d9e-17d891a58a9e";
//			vardata2.NodeNormalImageId="FD1349F5-49B9-418F-AC06-C249458215A4";	
//			vardata_.NodeNormalImageId="4DADA825-AD7D-44F6-8C85-7BFF61A0E9E7";
//
//			vardata.VariableId=(string) localTableVar[vardata.Caption];
//			vardata1.VariableId=(string)localTableVar[vardata1.Caption];
//			vardata2.VariableId=(string)localTableVar[vardata2.Caption];
//			vardata11.VariableId=(string)localTableVar[vardata11.Caption];
//			vardata12.VariableId=(string)localTableVar[vardata12.Caption];
//			vardata13.VariableId=(string)localTableVar[vardata13.Caption];
//			vardata14.VariableId=(string)localTableVar[vardata14.Caption];
//			vardata15.VariableId=(string)localTableVar[vardata15.Caption];
//			vardata21.VariableId=(string)localTableVar[vardata21.Caption];			
//			
//			vardata.ComponentId="TradingPlatform.Configurator.ConfigImage";
//			vardata1.ComponentId="TradingPlatform.Configurator.ConfigColor";
//			vardata2.ComponentId="TradingPlatform.Configurator.ConfigColor";
//			vardata_.ComponentId="TradingPlatform.Configurator.Indicator";
//			vardata_.IsWizard=true;
//			vardata11.ComponentId="TradingPlatform.Configurator.ConfigColor";
//			vardata12.ComponentId="TradingPlatform.Configurator.ConfigColor";
//			vardata13.ComponentId="TradingPlatform.Configurator.ConfigColor";
//			vardata14.ComponentId="TradingPlatform.Configurator.ConfigColor";
//			vardata15.ComponentId="TradingPlatform.Configurator.ConfigColor";
//			vardata21.ComponentId="TradingPlatform.Configurator.ConfigColor";
//
//			vardata.parentIDrawerComponent = currentComponent;
//			vardata_.parentIDrawerComponent = currentComponent;//add indicator
//
//			vardata1.parentIDrawerComponent = currentComponent;
//			vardata2.parentIDrawerComponent = currentComponent;
//			
//			vardata11.parentIDrawerComponent = currentComponent;
//			vardata12.parentIDrawerComponent = currentComponent;
//			vardata13.parentIDrawerComponent = currentComponent;
//			vardata14.parentIDrawerComponent = currentComponent;
//			vardata15.parentIDrawerComponent = currentComponent;			
//			vardata21.parentIDrawerComponent = currentComponent;
//
//			vardata2.Add(vardata21);
//			vardata1.Add(vardata15);
//			vardata1.Add(vardata11);
//			vardata1.Add(vardata12);
//			vardata1.Add(vardata13);
//			vardata1.Add(vardata14);
//			vardata.Add(vardata1);
//			vardata.Add(vardata2);
//			root.Add(vardata);
//			root.Add(vardata_);
//			root.Add(resdata);
//
//			root.Parent=null;		
//			
//			///****************
//			///
//	
//			GenericNode(root);// ������ ������	
//
//		}
//

#endregion