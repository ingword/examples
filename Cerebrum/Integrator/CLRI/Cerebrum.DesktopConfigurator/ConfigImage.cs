// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Cerebrum.Desktop.Configuration
{
	/// <summary>
	/// Summary description for ConfigImage.
	/// </summary>
	public class ConfigImage :Cerebrum.Windows.Forms.ControlComponent,IValueEditor
	{

		#region  interface IValueEditorEx	� ��� ����� ����������� ���������� ������� ����� �������� � ���� ���������� ����	

		public IWizardEditor GetWizardEditor
		{
			get{return null;}
		} 		
		
		private IValueSource m_valueSource;

		public IValueSource ValueSource
		{
			get
			{
				return m_valueSource;
			}
			set
			{			
				m_valueSource=value;				
				if (m_valueSource!=null && m_valueSource.Value!=null)
				{					
					image=(Image)m_valueSource.Value;				
					ActiveNodeChange();
				}
				else 
				{
					image=null;
					//				m_color=Color.Empty;
					//				trackBar1.Value=255;
					ActiveNodeChange();
				}
			}
		}	
		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button button1;
		private string editingFileName;
		//private System.Drawing.Bitmap m_Bitmap;
		private Image image;
		private System.Windows.Forms.PictureBox panel_Preview;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ConfigImage()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.button1 = new System.Windows.Forms.Button();
			this.panel_Preview = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(56, 104);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "preview";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.DefaultExt = "*";
			this.openFileDialog1.Filter = "All valid files (*.*)|*.*";
			this.openFileDialog1.FilterIndex = 2;
			this.openFileDialog1.InitialDirectory = "c:\\";
			this.openFileDialog1.RestoreDirectory = true;
			this.openFileDialog1.Title = "Open file Indicator";
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.LightGray;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Location = new System.Drawing.Point(52, 40);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(96, 48);
			this.button1.TabIndex = 2;
			this.button1.TabStop = false;
			this.button1.Text = "Load Image";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// panel_Preview
			// 
			this.panel_Preview.BackColor = System.Drawing.Color.WhiteSmoke;
			this.panel_Preview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel_Preview.Location = new System.Drawing.Point(16, 144);
			this.panel_Preview.Name = "panel_Preview";
			this.panel_Preview.Size = new System.Drawing.Size(168, 176);
			this.panel_Preview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.panel_Preview.TabIndex = 3;
			this.panel_Preview.TabStop = false;
			// 
			// ConfigImage
			// 
			this.BackColor = System.Drawing.Color.Linen;
			this.Controls.Add(this.panel_Preview);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label1);
			this.Name = "ConfigImage";
			this.Size = new System.Drawing.Size(200, 380);
			this.ResumeLayout(false);

		}
		#endregion

		private void ActiveNodeChange()
		{
			panel_Preview.Image=image;//m_Bitmap;
			//				panel_Preview.AutoScroll = true;
			//				panel_Preview.AutoScrollMinSize = new Size ((int)(m_Bitmap.Width * Zoom), (int)(m_Bitmap.Height * Zoom));
			panel_Preview.Invalidate();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{							
			//double Zoom = 1.0;
			if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) 
			{
				editingFileName = openFileDialog1.FileName;										
				image= Image.FromFile(openFileDialog1.FileName);
				ActiveNodeChange();				
				Bitmap bm = new Bitmap(image);
				m_valueSource.Value=bm;
			}
		}
	}
}
