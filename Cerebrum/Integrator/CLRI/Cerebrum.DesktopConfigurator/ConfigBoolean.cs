// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Cerebrum.Desktop.Configuration
{
	/// <summary>
	/// Summary description for ConfigImage.
	/// </summary>
public class ConfigBoolean :Cerebrum.Windows.Forms.ControlComponent,IValueEditor
{	

	#region  interface IValueEditorEx	� ��� ����� ����������� ���������� ������� ����� �������� � ���� ���������� ����	

	public IWizardEditor GetWizardEditor
	{
		get{return null;}
	} 		
		
	private IValueSource m_valueSource;
	private System.Windows.Forms.Label label1;
	private System.Windows.Forms.Label lblDescription;
	private System.Windows.Forms.RichTextBox rtbDescription;
	private System.Windows.Forms.GroupBox grpValue;
	private System.Windows.Forms.CheckBox chkBoolType;

	public IValueSource ValueSource
	{
		get
		{
			return m_valueSource;
		}
		set
		{			
			m_valueSource=value;				
			if (m_valueSource!=null && m_valueSource.Value!=null)
				chkBoolType.Checked	=	(bool)m_valueSource.Value;	
			else 		
				chkBoolType.Checked = false;
		}
	}	
	#endregion

	#region default
		
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ConfigBoolean()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.lblDescription = new System.Windows.Forms.Label();
			this.rtbDescription = new System.Windows.Forms.RichTextBox();
			this.grpValue = new System.Windows.Forms.GroupBox();
			this.chkBoolType = new System.Windows.Forms.CheckBox();
			this.grpValue.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 42);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 20);
			this.label1.TabIndex = 1;
			this.label1.Text = "Bool Type :";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(8, 24);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(72, 20);
			this.lblDescription.TabIndex = 2;
			this.lblDescription.Text = "Description";
			// 
			// rtbDescription
			// 
			this.rtbDescription.Enabled = false;
			this.rtbDescription.Location = new System.Drawing.Point(8, 56);
			this.rtbDescription.Name = "rtbDescription";
			this.rtbDescription.Size = new System.Drawing.Size(216, 96);
			this.rtbDescription.TabIndex = 3;
			this.rtbDescription.Text = "Sample text";
			// 
			// grpValue
			// 
			this.grpValue.Controls.Add(this.chkBoolType);
			this.grpValue.Controls.Add(this.label1);
			this.grpValue.Location = new System.Drawing.Point(8, 176);
			this.grpValue.Name = "grpValue";
			this.grpValue.Size = new System.Drawing.Size(216, 104);
			this.grpValue.TabIndex = 4;
			this.grpValue.TabStop = false;
			this.grpValue.Text = "Value";
			// 
			// chkBoolType
			// 
			this.chkBoolType.BackColor = System.Drawing.SystemColors.Control;
			this.chkBoolType.Location = new System.Drawing.Point(104, 42);
			this.chkBoolType.Name = "chkBoolType";
			this.chkBoolType.Size = new System.Drawing.Size(96, 20);
			this.chkBoolType.TabIndex = 0;
			this.chkBoolType.Text = "FALSE";
			this.chkBoolType.CheckedChanged += new System.EventHandler(this.chkBoolType_CheckedChanged);
			// 
			// ConfigBoolean
			// 
			this.Controls.Add(this.grpValue);
			this.Controls.Add(this.rtbDescription);
			this.Controls.Add(this.lblDescription);
			this.Name = "ConfigBoolean";
			this.Size = new System.Drawing.Size(232, 380);
			this.grpValue.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
	#endregion

	
	private void chkBoolType_CheckedChanged(object sender, System.EventArgs e)
	{
		if (chkBoolType.Checked) 
			chkBoolType.Text	=	"TRUE";
		else
			chkBoolType.Text	=	"FALSE";
		
		if(m_valueSource!=null)
		{
			m_valueSource.Value		=	chkBoolType.Checked;
		}
	}
	
	}
}
