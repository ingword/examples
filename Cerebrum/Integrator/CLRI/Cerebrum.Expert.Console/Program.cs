// Copyright (C) Dmitry Shuklin 2008-2009. All rights reserved.

using System;

namespace Cerebrum.Expert.Console
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Program
	{
		private static void ShowCopy()
		{
			string text = @"SHDsoftware Cerebrum Elex Expert System Version 2.0
Copyright (C) SHDsoftware 2000-2009. All rights reserved.";
			System.Console.WriteLine(text);
		}

		private static void ShowHelp()
		{
			string text = @"
        -c    Compile knowledge base,
        -lv   Load values into knowledge base,
        -r    Run reasoning engine,
        -so   Save outputs,
        -sh   Save hiddens,
        -si   Save inputs";
			System.Console.WriteLine(text);
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			ShowCopy();

			if(args==null || args.Length < 1)
			{
				ShowHelp();
				return;
			}

			string databaseFileName = String.Empty;
			string sourceFileName = String.Empty;
			string mode = "error";

			switch(args[0].ToLower())
			{
				case "-lv":
				case "/lv":
					if(args.Length == 3)
					{
						databaseFileName = args[1];
						sourceFileName = args[2];
						mode = "load_values";
					}
					break;
				case "-c":
				case "/c":
					if(args.Length == 3)
					{
						databaseFileName = args[1];
						sourceFileName = args[2];
						mode = "compile";
					}
					break;
				case "-r":
				case "/r":
					if(args.Length == 2)
					{
						databaseFileName = args[1];
						sourceFileName = string.Empty;
						mode = "run";
					}
					break;
				case "-si":
				case "/si":
					if(args.Length == 3)
					{
						databaseFileName = args[1];
						sourceFileName = args[2];
						mode = "save_inputs";
					}
					break;
				case "-sh":
				case "/sh":
					if(args.Length == 3)
					{
						databaseFileName = args[1];
						sourceFileName = args[2];
						mode = "save_hiddens";
					}
					break;
				case "-so":
				case "/so":
					if(args.Length == 3)
					{
						databaseFileName = args[1];
						sourceFileName = args[2];
						mode = "save_outputs";
					}
					break;
				default:
					break;
			}

			switch(mode)
			{
				case "compile":
					if(System.IO.File.Exists(databaseFileName))
					{
						System.IO.File.Delete(databaseFileName);
					}
					break;
				case "run":
				case "load_values":
				case "save_inputs":
				case "save_hiddens":
				case "save_outputs":
					if(!System.IO.File.Exists(databaseFileName))
					{
						throw new System.IO.FileNotFoundException("Elex Knowledge Base not found", databaseFileName);
					}
					break;
				default:
					ShowHelp();
					return;
			}

			Application app = new Application();
			app.MasterContextDomainName = Cerebrum.Elex.Specialized.Concepts.CerebrumExpertElexDomainName;
			app.DatabaseFileName = databaseFileName;
			Application.Instance = app;
			app.Initialize();

			Cerebrum.Runtime.Extension.KernelObjectKindId.Register();

			try
			{
				using(Cerebrum.Expert.KnowledgeBase kb = new Cerebrum.Expert.KnowledgeBase())
				{
					((Cerebrum.IComponent)kb).SetConnector(Cerebrum.SerializeDirection.Init, new Cerebrum.Runtime.SurrogateComposite(app.MasterContext.Workspace));
					kb.RefreshCache();

					switch(mode)
					{
						case "load_values":
							using(System.IO.StreamReader sr = new System.IO.StreamReader(sourceFileName, System.Text.Encoding.Default))
							{
								string text = sr.ReadToEnd();
								LoadInputs(kb, text);
							}
							break;
						case "compile":
							using(System.IO.StreamReader sr = new System.IO.StreamReader(sourceFileName, System.Text.Encoding.Default))
							{
								string text = sr.ReadToEnd();
								kb.Compile(text);
							}
							break;
						case "run":
							kb.Calculate();
							break;
						case "save_inputs":
						case "save_hiddens":
						case "save_outputs":
							System.Text.StringBuilder sb = new System.Text.StringBuilder();
							if("save_inputs"==mode)
							{
								SaveInputs(kb, sb, true);
							}
							if("save_hiddens"==mode)
							{
								SaveInputs(kb, sb, false);
							}
							if("save_outputs"==mode)
							{
								SaveOutputs(kb, sb);
							}
							using(System.IO.FileStream fs = new System.IO.FileStream(sourceFileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
							{
								using(System.IO.StreamWriter sw = new System.IO.StreamWriter(fs, System.Text.Encoding.Default))
								{
									sw.Write(sb.ToString());
								}
							}
							break;
					}
				}
			}
			finally
			{
				app.Shutdown();
				app.Dispose();
			}
		}


		public static void SaveInputs(Cerebrum.Expert.KnowledgeBase kb, System.Text.StringBuilder sb, bool enabled)
		{
			sb.Append("Description;Value" + System.Environment.NewLine);
			for(int i=0;i<kb.ElexNodesView.Count;i++)
			{
				Cerebrum.Data.ComponentItemView node = kb.ElexNodesView[i];
				string custprops = Convert.ToString(node["CustomProperties"]);
				if(!("layer".Equals(custprops)||"out".Equals(custprops)))
				{
					using(Cerebrum.Data.VectorView vvv = node["Precedents"] as Cerebrum.Data.VectorView)
					{
						if((vvv==null || vvv.Count == 0) == enabled)
						{
							string description = Convert.ToString(node[Cerebrum.Specialized.KnownNames.Description]);
							int val = kb.GetNodeValue(node);
							sb.Append(description + ";" + val.ToString() + ";" + System.Environment.NewLine);
						}
					}
				}
			}
		}

		public static void SaveOutputs(Cerebrum.Expert.KnowledgeBase kb, System.Text.StringBuilder sb)
		{
			sb.Append("Description;Value" + System.Environment.NewLine);
			for(int i=0;i<kb.ElexNodesView.Count;i++)
			{
				Cerebrum.Data.ComponentItemView node = kb.ElexNodesView[i];
				if("out".Equals(node["CustomProperties"])) 
				{
					string description = Convert.ToString(node[Cerebrum.Specialized.KnownNames.Description]);
					int val = kb.GetNodeValue(node);
					sb.Append(description + ";" + val.ToString() + ";" + System.Environment.NewLine);
				}
			}
		}
		public static void LoadInputs(Cerebrum.Expert.KnowledgeBase kb, string text)
		{
			System.Collections.Hashtable hshNodes = new System.Collections.Hashtable();

			for(int i=0;i<kb.ElexNodesView.Count;i++)
			{
				Cerebrum.Data.ComponentItemView node = kb.ElexNodesView[i];
				string description = Convert.ToString(node[Cerebrum.Specialized.KnownNames.Description]);
				hshNodes.Add(description, node);
			}

			int idxDesc = -1;
			int idxVal = -1;
			bool header = true;
			foreach(string line in text.Split(System.Environment.NewLine.ToCharArray()))
			{
				string []slots = line.Split(';');
				if(header)
				{
					for(int i=0 ; i< slots.Length ; i++)
					{
						string name = slots[i].ToLower();
						if(name=="description")
						{
							idxDesc = i;
						}
						if(name=="value")
						{
							idxVal = i;
						}
					}
					header = false;
				}
				else
				{
					Cerebrum.Data.ComponentItemView node = hshNodes[slots[idxDesc]] as Cerebrum.Data.ComponentItemView;
					if(node != null)
					{
						int v = int.Parse(slots[idxVal]);
						kb.SetNodeValue(node, v);
					}
				}
			}
		}
	}
}
