// Copyright (C) Dmitry Shuklin 2008-2009. All rights reserved.

using System;

namespace Cerebrum.Expert.Console
{
	/// <summary>
	/// Summary description for Application.
	/// </summary>
	public class Application : Cerebrum.Integrator.Application
	{
		protected static Application m_Instance;

		public string DatabaseFileName;

		public static Cerebrum.Expert.Console.Application Instance
		{
			get
			{
				return m_Instance;
			}
			set
			{
				m_Instance = value;
			}
		}

		public Application()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string ApplicationDirectory
		{
			get
			{
				return System.IO.Path.GetDirectoryName(this.GetType().Assembly.Location);
			}
		}

		public override Cerebrum.Integrator.DomainContext CreateMasterContext(string domainName, bool creating)
		{
			return new Cerebrum.Integrator.DomainContext(this);
		}

		protected override Cerebrum.Integrator.IContextFactory CreateContextFactory()
		{
			string currentDirectory = System.Environment.CurrentDirectory;
			string primaryDirectory = this.ApplicationDirectory;
			string databaseFileName = System.IO.Path.Combine(currentDirectory, this.DatabaseFileName);
			string activityFileName = System.IO.Path.Combine(currentDirectory, "Cerebrum.Expert.Console.log");
			string templateFileName = System.IO.Path.Combine(primaryDirectory, "Elex\\Import\\elexdb");

			return new Cerebrum.Management.SimpleContextFactory(this, databaseFileName, activityFileName, 4, true, templateFileName, true);
		}

		public override void ProcessException(Exception ex, string source)
		{
			System.Console.WriteLine(source);
			System.Console.WriteLine(ex.ToString());
		}

	}
}
