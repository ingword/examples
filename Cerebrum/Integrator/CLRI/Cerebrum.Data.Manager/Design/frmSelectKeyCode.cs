using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmSelectKeyCode.
	/// </summary>
	public class frmSelectKeyCode : System.Windows.Forms.Form
	{
		private string PressKey;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmSelectKeyCode()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			PressKey = "";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.label1.ForeColor = System.Drawing.Color.Red;
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(272, 32);
			this.label1.TabIndex = 0;
			this.label1.Text = "������� ������� �������";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// frmSelectKeyCode
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.MediumAquamarine;
			this.ClientSize = new System.Drawing.Size(288, 45);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmSelectKeyCode";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Select Key Code";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// ������������ ������� ������ �� ���� MDI Child
		/// </summary>
		/// <param name="m">��������� � ����� ������� �������</param>
		/// <returns>true ���� ������� ���� ����������</returns>
		protected bool PreviewKeyMessage(ref System.Windows.Forms.Message m)
		{
			try
			{
				if(m.Msg == 0x00000104 || //WM_SYSKEYDOWN
					m.Msg == 0x00000100) //WM_KEYDOWN
				{
					string key = "";

					if(m.Msg == 0x00000104) //WM_SYSKEYDOWN
						key += "A";
					if( (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Control) > 0 )
						key += "C";
					if( (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Shift) > 0 )
						key += "S";
					key += string.Format("{0:X4}", m.WParam.ToInt32());
					if(key!="A0012" && key!="C0011" && key!="S0010")
					{
						PressKey = key;
						this.DialogResult = DialogResult.OK;
						this.Close();
						return true;
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
			return false;
		}

		/// <summary>
		/// ������������ ������� ������ �� ���� MDI Container
		/// </summary>
		/// <param name="m">��������� � ����� ������� �������</param>
		/// <returns>true ���� ������� ���� ����������</returns>
		protected override bool ProcessKeyMessage(ref System.Windows.Forms.Message m)
		{
			if(PreviewKeyMessage(ref m)) return true;
			return base.ProcessKeyMessage(ref m);
		}

		/// <summary>
		/// ������������ ������� ������ �� ���� MDI Child � MDI Container
		/// </summary>
		/// <param name="m">��������� � ����� ������� �������</param>
		/// <returns>true ���� ������� ���� ����������</returns>
		protected override bool ProcessKeyPreview(ref System.Windows.Forms.Message m)
		{
			if(PreviewKeyMessage(ref m)) return true;
			return base.ProcessKeyPreview(ref m);
		}

		public string KeyCode
		{
			get
			{
				return PressKey;
			}
		}
	}
}
