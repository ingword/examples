// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmHotKeyItem.
	/// </summary>
	public class frmHotKeyItem : System.Windows.Forms.Form
	{
		private Cerebrum.Integrator.DomainContext DC;
		private Cerebrum.ObjectHandle HotKeyHandle;
		private bool IsFD;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbOH;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbDescription;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbKeyCode;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnSelKeyCode;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmHotKeyItem(Cerebrum.Integrator.DomainContext DomainContext, Cerebrum.ObjectHandle CtrlHandle, bool IsFillData)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			DC = DomainContext;
			HotKeyHandle = CtrlHandle;
			IsFD = IsFillData;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.tbKeyCode = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbDescription = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbOH = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnSelKeyCode = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.tbKeyCode);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.tbDescription);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.tbName);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.tbOH);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.btnSelKeyCode);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(384, 112);
			this.panel1.TabIndex = 0;
			// 
			// tbKeyCode
			// 
			this.tbKeyCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbKeyCode.Location = new System.Drawing.Point(120, 80);
			this.tbKeyCode.Name = "tbKeyCode";
			this.tbKeyCode.TabIndex = 3;
			this.tbKeyCode.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 20);
			this.label4.TabIndex = 6;
			this.label4.Text = "Key Code";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbDescription
			// 
			this.tbDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDescription.Location = new System.Drawing.Point(120, 56);
			this.tbDescription.Name = "tbDescription";
			this.tbDescription.Size = new System.Drawing.Size(256, 20);
			this.tbDescription.TabIndex = 2;
			this.tbDescription.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 56);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 20);
			this.label3.TabIndex = 4;
			this.label3.Text = "Description";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbName
			// 
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(120, 32);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(256, 20);
			this.tbName.TabIndex = 1;
			this.tbName.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 20);
			this.label2.TabIndex = 2;
			this.label2.Text = "Name";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbOH
			// 
			this.tbOH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbOH.Location = new System.Drawing.Point(120, 8);
			this.tbOH.Name = "tbOH";
			this.tbOH.ReadOnly = true;
			this.tbOH.TabIndex = 7;
			this.tbOH.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Object Handle";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(304, 120);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "&Cancel";
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(224, 120);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 5;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnSelKeyCode
			// 
			this.btnSelKeyCode.BackColor = System.Drawing.Color.Turquoise;
			this.btnSelKeyCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSelKeyCode.Location = new System.Drawing.Point(224, 80);
			this.btnSelKeyCode.Name = "btnSelKeyCode";
			this.btnSelKeyCode.Size = new System.Drawing.Size(24, 20);
			this.btnSelKeyCode.TabIndex = 4;
			this.btnSelKeyCode.Text = "...";
			this.btnSelKeyCode.Click += new System.EventHandler(this.btnSelKeyCode_Click);
			// 
			// frmHotKeyItem
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(384, 149);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmHotKeyItem";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Hot Key item";
			this.Load += new System.EventHandler(this.frmHotKeyItem_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmHotKeyItem_Load(object sender, System.EventArgs e)
		{
			try
			{
				if(IsFD == true)
				{
					using(Cerebrum.Data.TableView tvHK = Cerebrum.Management.Utilites.GetView(DC, "UiHotKeys"))
					{
						Cerebrum.Data.ComponentItemView civ = tvHK.FindItemViewByObjectHandle(HotKeyHandle);
						tbOH.Text = civ.ObjectHandle.ToString();
						tbName.Text = civ["Name"].ToString();
						tbDescription.Text = civ["Description"].ToString();
						tbKeyCode.Text = civ["KeyCode"].ToString();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Data.ComponentItemView civ = null;

			try
			{
				using(Cerebrum.Data.TableView tvHK = Cerebrum.Management.Utilites.GetView(DC, "UiHotKeys"))
				{
					if(IsFD == true)
					{
						civ = tvHK.FindItemViewByObjectHandle(HotKeyHandle);
					}
					else
					{
						civ = tvHK.AddNew();
						HotKeyHandle = civ.ObjectHandle;
					}
					civ["Name"] = tbName.Text;
					civ["Description"] = tbDescription.Text;
					civ["KeyCode"] = tbKeyCode.Text;
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnSelKeyCode_Click(object sender, System.EventArgs e)
		{
			frmSelectKeyCode frm = new frmSelectKeyCode();
			if(frm.ShowDialog() == DialogResult.OK)
			{
				tbKeyCode.Text = frm.KeyCode;
			}
		}

		public Cerebrum.ObjectHandle ControlHandle
		{
			get
			{
				return HotKeyHandle;
			}
		}

		public string ControlName
		{
			get
			{
				return tbName.Text;
			}
		}

		public string Description
		{
			get
			{
				return tbDescription.Text;
			}
		}

		public string KeyCode
		{
			get
			{
				return tbKeyCode.Text;
			}
		}
	}
}
