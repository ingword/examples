// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmCmdList.
	/// </summary>
	public class frmCmdList : System.Windows.Forms.Form
	{
		private Cerebrum.ObjectHandle CmdHandle;
		private string CmdName;
		private string MsgName;
		private Cerebrum.Integrator.DomainContext SelectedDC;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnEdit;
		private System.Windows.Forms.Button btnDel;
		private System.Windows.Forms.ListView lvCmd;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmCmdList(Cerebrum.Integrator.DomainContext DC)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			SelectedDC = DC;
			CmdHandle = Cerebrum.ObjectHandle.Null;
			CmdName = "";
			MsgName = "";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.lvCmd = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.btnDel = new System.Windows.Forms.Button();
			this.btnEdit = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.lvCmd);
			this.panel1.Controls.Add(this.btnDel);
			this.panel1.Controls.Add(this.btnEdit);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(880, 360);
			this.panel1.TabIndex = 0;
			// 
			// lvCmd
			// 
			this.lvCmd.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					this.columnHeader1,
																					this.columnHeader2,
																					this.columnHeader3,
																					this.columnHeader4,
																					this.columnHeader5,
																					this.columnHeader6});
			this.lvCmd.FullRowSelect = true;
			this.lvCmd.GridLines = true;
			this.lvCmd.HideSelection = false;
			this.lvCmd.Location = new System.Drawing.Point(8, 32);
			this.lvCmd.MultiSelect = false;
			this.lvCmd.Name = "lvCmd";
			this.lvCmd.Size = new System.Drawing.Size(864, 320);
			this.lvCmd.TabIndex = 6;
			this.lvCmd.View = System.Windows.Forms.View.Details;
			this.lvCmd.DoubleClick += new System.EventHandler(this.lvCmd_DoubleClick);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "ObjectHandle";
			this.columnHeader1.Width = 83;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Name";
			this.columnHeader2.Width = 159;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Message";
			this.columnHeader3.Width = 138;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Ordinal";
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "NormalLeaveImageStreamName";
			this.columnHeader5.Width = 194;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "NormalEnterImageStreamName";
			this.columnHeader6.Width = 193;
			// 
			// btnDel
			// 
			this.btnDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDel.Location = new System.Drawing.Point(832, 8);
			this.btnDel.Name = "btnDel";
			this.btnDel.Size = new System.Drawing.Size(40, 23);
			this.btnDel.TabIndex = 5;
			this.btnDel.Text = "Del";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// btnEdit
			// 
			this.btnEdit.BackColor = System.Drawing.Color.Turquoise;
			this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.Location = new System.Drawing.Point(792, 8);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(40, 23);
			this.btnEdit.TabIndex = 4;
			this.btnEdit.Text = "Edit";
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// btnNew
			// 
			this.btnNew.BackColor = System.Drawing.Color.Turquoise;
			this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnNew.Location = new System.Drawing.Point(752, 8);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(40, 23);
			this.btnNew.TabIndex = 3;
			this.btnNew.Text = "New";
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(720, 368);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(800, 368);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "&Cancel";
			// 
			// frmCmdList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(880, 397);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmCmdList";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Commands list";
			this.Load += new System.EventHandler(this.frmCmdList_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmCmdList_Load(object sender, System.EventArgs e)
		{
			try
			{
				using(Cerebrum.Data.TableView tvUC = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiCommands"))
				{
					for(int i=0; i<tvUC.Count; i++)
					{
						Cerebrum.Data.ComponentItemView civUC = tvUC[i];
						ListViewItem liv = lvCmd.Items.Add(civUC.ObjectHandle.ToString());
						liv.SubItems.Add(civUC[Cerebrum.Specialized.KnownNames.Name].ToString());
						Cerebrum.ObjectHandle MessageHandle = (Cerebrum.ObjectHandle)civUC[Cerebrum.Specialized.KnownNames.MessageId];
						using(Cerebrum.IComposite comp = SelectedDC.AttachConnector(MessageHandle))
						{
							Cerebrum.IContainer cont = (Cerebrum.IContainer)comp;
							liv.SubItems.Add(Convert.ToString(Cerebrum.Integrator.Utilites.ObtainComponent(cont, Cerebrum.Specialized.Concepts.NameAttribute)));
						}
//						using(Cerebrum.Data.TableView tvM = Cerebrum.Management.Utilites.GetView(SelectedDC, "Messages"))
//						{
//							Cerebrum.Data.ComponentItemView civM = tvM.FindItemViewByObjectHandle(MessageHandle);
//							liv.SubItems.Add(civM["Name"].ToString());
//						}
						liv.SubItems.Add(civUC[Cerebrum.Specialized.KnownNames.Ordinal].ToString());
						liv.SubItems.Add(civUC[Cerebrum.Windows.Forms.Specialized.KnownNames.NormalLeaveImageStreamName].ToString());
						liv.SubItems.Add(civUC[Cerebrum.Windows.Forms.Specialized.KnownNames.NormalEnterImageStreamName].ToString());
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "Cerebrum.Data.Manager.Design.Forms");
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvCmd.SelectedItems.Count > 0)
				{
					ListViewItem liv = lvCmd.SelectedItems[0];
					CmdHandle = new ObjectHandle(liv.Text);
					CmdName = liv.SubItems[1].Text;
					MsgName = liv.SubItems[2].Text;
				}
				else
				{
					CmdHandle = Cerebrum.ObjectHandle.Null;
					CmdName = "";
					MsgName = "";
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				frmCmdItem frm = new frmCmdItem(SelectedDC, Cerebrum.ObjectHandle.Null, false);
				if(frm.ShowDialog() == DialogResult.OK)
				{
					ListViewItem liv = lvCmd.Items.Add(frm.CommandHandle.ToString());
					liv.SubItems.Add(frm.CommandName);
					liv.SubItems.Add(frm.MessageName);
					liv.SubItems.Add(frm.Ordinal.ToString());
					liv.SubItems.Add(frm.NormalLeaveImageStreamName);
					liv.SubItems.Add(frm.NormalEnterImageStreamName);
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnEdit_Click(object sender, System.EventArgs e)
		{
			ViewCmdEditForm();
		}

		private void ViewCmdEditForm()
		{
			try
			{
				if(lvCmd.SelectedItems.Count > 0)
				{
					ListViewItem liv = lvCmd.SelectedItems[0];
					Cerebrum.ObjectHandle CH = new ObjectHandle(liv.Text);
					frmCmdItem frm = new frmCmdItem(SelectedDC, CH, true);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						liv.SubItems.Add(frm.CommandName);
						liv.SubItems.Add(frm.MessageName);
						liv.SubItems.Add(frm.Ordinal.ToString());
						liv.SubItems.Add(frm.NormalLeaveImageStreamName);
						liv.SubItems.Add(frm.NormalEnterImageStreamName);
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnDel_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("���� �� ��������.");
		}

		private void lvCmd_DoubleClick(object sender, System.EventArgs e)
		{
			ViewCmdEditForm();
		}

		public Cerebrum.ObjectHandle CommandHandle
		{
			get
			{
				return CmdHandle;
			}
		}

		public string CommandName
		{
			get
			{
				return CmdName;
			}
		}
		
		public string MessageName
		{
			get
			{
				return MsgName;
			}
		}
	}
}
