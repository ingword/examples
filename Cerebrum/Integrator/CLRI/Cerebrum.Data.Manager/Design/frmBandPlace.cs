// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmBandPlace.
	/// </summary>
	public class frmBandPlace : System.Windows.Forms.Form
	{
		private Cerebrum.Integrator.DomainContext DC;
		private Cerebrum.ObjectHandle BPHandle;
		private bool IsFD;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbMO;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbDescription;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbDisplayName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbOH;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox tbWidth;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmBandPlace(Cerebrum.Integrator.DomainContext DomainContext, Cerebrum.ObjectHandle CtrlHandle, bool IsFillData)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			DC = DomainContext;
			BPHandle = CtrlHandle;
			IsFD = IsFillData;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label5 = new System.Windows.Forms.Label();
			this.tbMO = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbDescription = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbDisplayName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tbOH = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.tbWidth = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 104);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 20);
			this.label5.TabIndex = 18;
			this.label5.Text = "Merge Order:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbMO
			// 
			this.tbMO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbMO.Location = new System.Drawing.Point(120, 104);
			this.tbMO.Name = "tbMO";
			this.tbMO.TabIndex = 4;
			this.tbMO.Text = "0";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 20);
			this.label4.TabIndex = 16;
			this.label4.Text = "Description:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbDescription
			// 
			this.tbDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDescription.Location = new System.Drawing.Point(120, 80);
			this.tbDescription.Name = "tbDescription";
			this.tbDescription.Size = new System.Drawing.Size(320, 20);
			this.tbDescription.TabIndex = 3;
			this.tbDescription.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 56);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 20);
			this.label3.TabIndex = 15;
			this.label3.Text = "Display Name:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbDisplayName
			// 
			this.tbDisplayName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDisplayName.Location = new System.Drawing.Point(120, 56);
			this.tbDisplayName.Name = "tbDisplayName";
			this.tbDisplayName.Size = new System.Drawing.Size(320, 20);
			this.tbDisplayName.TabIndex = 2;
			this.tbDisplayName.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 20);
			this.label2.TabIndex = 12;
			this.label2.Text = "Name:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbName
			// 
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(120, 32);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(320, 20);
			this.tbName.TabIndex = 1;
			this.tbName.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 20);
			this.label1.TabIndex = 9;
			this.label1.Text = "Object Handle:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbOH
			// 
			this.tbOH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbOH.Location = new System.Drawing.Point(120, 8);
			this.tbOH.Name = "tbOH";
			this.tbOH.ReadOnly = true;
			this.tbOH.TabIndex = 8;
			this.tbOH.Text = "";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 128);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 20);
			this.label6.TabIndex = 20;
			this.label6.Text = "Width:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbWidth
			// 
			this.tbWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbWidth.Location = new System.Drawing.Point(120, 128);
			this.tbWidth.Name = "tbWidth";
			this.tbWidth.TabIndex = 5;
			this.tbWidth.Text = "60";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.tbMO);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.tbDescription);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.tbDisplayName);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.tbName);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.tbOH);
			this.panel1.Controls.Add(this.tbWidth);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(448, 160);
			this.panel1.TabIndex = 21;
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(368, 168);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 7;
			this.btnCancel.Text = "&Cancel";
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(288, 168);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 6;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// frmBandPlace
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(448, 197);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmBandPlace";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Band Place item";
			this.Load += new System.EventHandler(this.frmBandPlace_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmBandPlace_Load(object sender, System.EventArgs e)
		{
			try
			{
				if(IsFD == true)
				{
					using(Cerebrum.Data.TableView tvBP = Cerebrum.Management.Utilites.GetView(DC, "UiBandPlaces"))
					{
						Cerebrum.Data.ComponentItemView civ = tvBP.FindItemViewByObjectHandle(BPHandle);
						tbOH.Text = civ.ObjectHandle.ToString();
						tbName.Text = civ["Name"].ToString();
						tbDisplayName.Text = civ["DisplayName"].ToString();
						tbDescription.Text = civ["Description"].ToString();
						tbMO.Text = civ["MergeOrder"].ToString();
						tbWidth.Text = civ["Width"].ToString();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Data.ComponentItemView civ = null;

			try
			{
				using(Cerebrum.Data.TableView tvBP = Cerebrum.Management.Utilites.GetView(DC, "UiBandPlaces"))
				{
					if(IsFD == true)
					{
						civ = tvBP.FindItemViewByObjectHandle(BPHandle);
					}
					else
					{
						civ = tvBP.AddNew();
						BPHandle = civ.ObjectHandle;
					}
					civ["Name"] = tbName.Text;
					civ["DisplayName"] = tbDisplayName.Text;
					civ["Description"] = tbDescription.Text;
					if(tbMO.Text != "")
					{
						civ["MergeOrder"] = Convert.ToInt32(tbMO.Text);
					}
					if(tbWidth.Text != "")
					{
						civ["Width"] = Convert.ToInt32(tbWidth.Text);
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		public string ControlName
		{
			get
			{
				return tbName.Text;
			}
		}

		public Cerebrum.ObjectHandle ControlHandle
		{
			get
			{
				return BPHandle;
			}
		}

		public string DisplayName
		{
			get
			{
				return tbDisplayName.Text;
			}
		}

		public string Description
		{
			get
			{
				return tbDescription.Text;
			}
		}

		public int MergeOrder
		{
			get
			{
				int MO = 0;
				if(tbMO.Text != "")
				{
					MO = Convert.ToInt32(tbMO.Text);
				}
				return MO;
			}
		}

		public int PlaceWidth
		{
			get
			{
				int w = 0;
				if(tbWidth.Text != "")
				{
					w = Convert.ToInt32(tbWidth.Text);
				}
				return w;
			}
		}
	}
}
