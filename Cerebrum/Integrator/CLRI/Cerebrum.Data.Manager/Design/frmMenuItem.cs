// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmMenuItem.
	/// </summary>
	public class frmMenuItem : System.Windows.Forms.Form
	{
		private Cerebrum.Integrator.DomainContext DC;
		private Cerebrum.ObjectHandle MIHandle;
		private Cerebrum.ObjectHandle PIHandle;
		private bool IsFD;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbOH;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbDisplayName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbMO;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.CheckBox cbIsRoot;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmMenuItem(Cerebrum.Integrator.DomainContext DomainContext, Cerebrum.ObjectHandle CtrlHandle, Cerebrum.ObjectHandle ParentHandle, bool IsFillData)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			DC = DomainContext;
			if(CtrlHandle != Cerebrum.ObjectHandle.Null)
			{
				MIHandle = CtrlHandle;
			}
			else
			{
				MIHandle = Cerebrum.ObjectHandle.Null;
			}
			if(ParentHandle != Cerebrum.ObjectHandle.Null)
			{
				PIHandle = ParentHandle;
			}
			else
			{
				PIHandle = Cerebrum.ObjectHandle.Null;
			}
			IsFD = IsFillData;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.tbMO = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbDisplayName = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbOH = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.cbIsRoot = new System.Windows.Forms.CheckBox();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.cbIsRoot);
			this.panel1.Controls.Add(this.tbMO);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.tbDisplayName);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.tbName);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.tbOH);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(378, 136);
			this.panel1.TabIndex = 0;
			// 
			// tbMO
			// 
			this.tbMO.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbMO.Location = new System.Drawing.Point(112, 80);
			this.tbMO.Name = "tbMO";
			this.tbMO.TabIndex = 3;
			this.tbMO.Text = "0";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 20);
			this.label4.TabIndex = 6;
			this.label4.Text = "Merge Order";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbDisplayName
			// 
			this.tbDisplayName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDisplayName.Location = new System.Drawing.Point(112, 56);
			this.tbDisplayName.Name = "tbDisplayName";
			this.tbDisplayName.Size = new System.Drawing.Size(256, 20);
			this.tbDisplayName.TabIndex = 2;
			this.tbDisplayName.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 56);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 20);
			this.label3.TabIndex = 4;
			this.label3.Text = "Display Name";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbName
			// 
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(112, 32);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(256, 20);
			this.tbName.TabIndex = 1;
			this.tbName.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 20);
			this.label2.TabIndex = 2;
			this.label2.Text = "Name";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbOH
			// 
			this.tbOH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbOH.Location = new System.Drawing.Point(112, 8);
			this.tbOH.Name = "tbOH";
			this.tbOH.ReadOnly = true;
			this.tbOH.TabIndex = 7;
			this.tbOH.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Object Handle";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(296, 144);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "&Cancel";
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(216, 144);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 5;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// cbIsRoot
			// 
			this.cbIsRoot.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbIsRoot.Location = new System.Drawing.Point(8, 104);
			this.cbIsRoot.Name = "cbIsRoot";
			this.cbIsRoot.Size = new System.Drawing.Size(120, 24);
			this.cbIsRoot.TabIndex = 4;
			this.cbIsRoot.Text = "Is Root";
			// 
			// frmMenuItem
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(378, 175);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmMenuItem";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Menu item";
			this.Load += new System.EventHandler(this.frmMenuItem_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmMenuItem_Load(object sender, System.EventArgs e)
		{
			try
			{
				if(PIHandle == Cerebrum.ObjectHandle.Null)
				{
					cbIsRoot.Checked = true;
				}
				if(IsFD == true)
				{
					Cerebrum.ObjectHandle PH = Cerebrum.ObjectHandle.Null;
					tbOH.Text = MIHandle.ToString();
					tbMO.Text = "0";
					if(MIHandle == Cerebrum.Windows.Forms.Specialized.Concepts.SystemMenuItemId)
					{
						tbName.Text = "File";
						tbDisplayName.Text = "File";
					}
					else if(MIHandle == Cerebrum.Windows.Forms.Specialized.Concepts.SecurityMenuItemId)
					{
						tbName.Text = "Security";
						tbDisplayName.Text = "Security";
					}
					else if(MIHandle == Cerebrum.Windows.Forms.Specialized.Concepts.WindowMenuItemId)
					{
						tbName.Text = "Window";
						tbDisplayName.Text = "Window";
					}
					else if(MIHandle == Cerebrum.Windows.Forms.Specialized.Concepts.HelpMenuItemId)
					{
						tbName.Text = "Help";
						tbDisplayName.Text = "Help";
					}
					else
					{
						using(Cerebrum.Data.TableView tvMI = Cerebrum.Management.Utilites.GetView(DC, "UiMenuItems"))
						{
							Cerebrum.Data.ComponentItemView civ = tvMI.FindItemViewByObjectHandle(MIHandle);
							tbOH.Text = civ.ObjectHandle.ToString();
							tbName.Text = civ["Name"].ToString();
							tbDisplayName.Text = civ["DisplayName"].ToString();
							tbMO.Text = civ["MergeOrder"].ToString();
							PH = (Cerebrum.ObjectHandle)civ["ParentId"];
						}
					}
					if(PH == Cerebrum.ObjectHandle.Null)
					{
						cbIsRoot.Checked = true;
					}
					cbIsRoot.Enabled = false;
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Data.ComponentItemView civ = null;

			try
			{
				using(Cerebrum.Data.TableView tvMI = Cerebrum.Management.Utilites.GetView(DC, "UiMenuItems"))
				{
					if(IsFD == true)
					{
						civ = tvMI.FindItemViewByObjectHandle(MIHandle);
					}
					else
					{
						civ = tvMI.AddNew();
						MIHandle = civ.ObjectHandle;
						if(cbIsRoot.Checked == true)
						{
							PIHandle = Cerebrum.ObjectHandle.Null;
						}
						civ["ParentId"] = PIHandle;
					}
					civ["Name"] = tbName.Text;
					civ["DisplayName"] = tbDisplayName.Text;
					if(tbMO.Text != "")
					{
						civ["MergeOrder"] = Convert.ToInt32(tbMO.Text);
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		public static bool IsChildrenMenuItem(Cerebrum.Integrator.DomainContext DC, Cerebrum.ObjectHandle MenuItemHandle)
		{
			bool IsDel = true;
			try
			{
				// ��������� ������� �������� ������� ����.
				using(Cerebrum.Data.TableView tvMI = Cerebrum.Management.Utilites.GetView(DC, "UiMenuItems"))
				{
					for(int i=0; i<tvMI.Count; i++)
					{
						Cerebrum.Data.ComponentItemView civ = tvMI[i];
						Cerebrum.ObjectHandle PMH = (Cerebrum.ObjectHandle)civ["ParentId"];
						if(PMH == MenuItemHandle)
						{
							IsDel = false;
							break;
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
			return IsDel;
		}

		public Cerebrum.ObjectHandle ControlHandle
		{
			get
			{
				return MIHandle;
			}
		}

		public Cerebrum.ObjectHandle ParentHandle
		{
			get
			{
				return PIHandle;
			}
		}

		public string ControlName
		{
			get
			{
				return tbName.Text;
			}
		}

		public string DisplayName
		{
			get
			{
				return tbDisplayName.Text;
			}
		}

		public int MergeOrder
		{
			get
			{
				int MO = 0;
				if(tbMO.Text != "")
				{
					MO = Convert.ToInt32(tbMO.Text);
				}
				return MO;
			}
		}
	}
}
