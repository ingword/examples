// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Cerebrum.Data.Manager.Design;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for ServiceItemForm.
	/// </summary>
	public class frmServiceItem : System.Windows.Forms.Form
	{
		private Cerebrum.Integrator.DomainContext DC;
		private Cerebrum.ObjectHandle SH;
		private bool IsFD;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbOH;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbOrdinal;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox cbServiceType;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnRegServType;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmServiceItem(Cerebrum.Integrator.DomainContext DomainContext, Cerebrum.ObjectHandle ServiceHandle, bool IsFillData)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			DC = DomainContext;
			SH = ServiceHandle;
			IsFD = IsFillData;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tbOH = new System.Windows.Forms.TextBox();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbOrdinal = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.cbServiceType = new System.Windows.Forms.ComboBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnRegServType = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Object Handle:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbOH
			// 
			this.tbOH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbOH.Location = new System.Drawing.Point(96, 8);
			this.tbOH.Name = "tbOH";
			this.tbOH.ReadOnly = true;
			this.tbOH.TabIndex = 6;
			this.tbOH.Text = "";
			// 
			// tbName
			// 
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(96, 32);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(416, 20);
			this.tbName.TabIndex = 1;
			this.tbName.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 20);
			this.label2.TabIndex = 2;
			this.label2.Text = "Name:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbOrdinal
			// 
			this.tbOrdinal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbOrdinal.Location = new System.Drawing.Point(96, 56);
			this.tbOrdinal.Name = "tbOrdinal";
			this.tbOrdinal.TabIndex = 2;
			this.tbOrdinal.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 56);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(80, 20);
			this.label3.TabIndex = 4;
			this.label3.Text = "Ordinal:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Turquoise;
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Location = new System.Drawing.Point(360, 120);
			this.button1.Name = "button1";
			this.button1.TabIndex = 4;
			this.button1.Text = "OK";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Turquoise;
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Location = new System.Drawing.Point(440, 120);
			this.button2.Name = "button2";
			this.button2.TabIndex = 5;
			this.button2.Text = "Cancel";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(80, 20);
			this.label4.TabIndex = 8;
			this.label4.Text = "Service type:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbServiceType
			// 
			this.cbServiceType.BackColor = System.Drawing.Color.Honeydew;
			this.cbServiceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbServiceType.Location = new System.Drawing.Point(96, 80);
			this.cbServiceType.Name = "cbServiceType";
			this.cbServiceType.Size = new System.Drawing.Size(416, 21);
			this.cbServiceType.TabIndex = 3;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.cbServiceType);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.tbOrdinal);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.tbName);
			this.panel1.Controls.Add(this.tbOH);
			this.panel1.Controls.Add(this.btnRegServType);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(522, 112);
			this.panel1.TabIndex = 10;
			// 
			// btnRegServType
			// 
			this.btnRegServType.BackColor = System.Drawing.Color.Turquoise;
			this.btnRegServType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRegServType.Location = new System.Drawing.Point(360, 56);
			this.btnRegServType.Name = "btnRegServType";
			this.btnRegServType.Size = new System.Drawing.Size(152, 23);
			this.btnRegServType.TabIndex = 11;
			this.btnRegServType.Text = "Register service type";
			this.btnRegServType.Click += new System.EventHandler(this.btnRegServType_Click);
			// 
			// frmServiceItem
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(522, 151);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmServiceItem";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ServiceItemForm";
			this.Load += new System.EventHandler(this.ServiceItemForm_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void ServiceItemForm_Load(object sender, System.EventArgs e)
		{
			try
			{
				Cerebrum.ObjectHandle ServiceTypeHandle = Cerebrum.ObjectHandle.Null;
				if(IsFD == true)
				{
					using(Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(DC, "UiServices"))
					{
						Cerebrum.Data.ComponentItemView civ = view.FindItemViewByObjectHandle(SH);
						tbOH.Text = civ.ObjectHandle.ToString();
						tbName.Text = civ["Name"].ToString();
						tbOrdinal.Text = civ["Ordinal"].ToString();
						ServiceTypeHandle = (Cerebrum.ObjectHandle)civ["TypeIdHandle"];
					}
				}
				TypeInfo SelectedTI = null;
				using(Cerebrum.Data.TableView stv = Cerebrum.Management.Utilites.GetView(DC, "Types"))
				{
					for(int i=0; i<stv.Count; i++)
					{
						Cerebrum.Data.ComponentItemView civSTV = stv[i];
						TypeInfo ti = new TypeInfo();
						ti.TypeHandle = civSTV.ObjectHandle;
						if(ti.TypeHandle == ServiceTypeHandle)
						{
							SelectedTI = ti;
						}
						ti.TypeName = civSTV["Name"].ToString();
						ti.QualifiedTypeName = civSTV["QualifiedTypeName"].ToString();
						ti.KernelObjectType = (Cerebrum.ObjectHandle)civSTV["KernelObjectClass"];
						ti.DisplayName = civSTV["DisplayName"].ToString();
						ti.Description = civSTV["Description"].ToString();
						cbServiceType.Items.Add(ti);
					}
				}
				if(IsFD == true)
				{
					if(SelectedTI != null)
					{
						cbServiceType.SelectedItem = SelectedTI;
					}
					cbServiceType.Enabled = false;
					btnRegServType.Visible = false;
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(IsFD == true)
				{
					using(Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(DC, "UiServices"))
					{
						Cerebrum.Data.ComponentItemView civ = view.FindItemViewByObjectHandle(SH);
						civ["Name"] = tbName.Text;
						if(tbOrdinal.Text != "")
						{
							civ["Ordinal"] = Convert.ToInt32(tbOrdinal.Text);
						}
					}
				}
				else
				{
					if(cbServiceType.SelectedItem != null)
					{
						Cerebrum.ObjectHandle TblUiServisesHandle = Cerebrum.Management.Utilites.ResolveHandle(DC, "Tables", "Name", "UiServices");
						using(Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(DC, "Tables"))
						{
							Cerebrum.Data.ComponentItemView civ = view.FindItemViewByObjectHandle(TblUiServisesHandle);
							Cerebrum.ObjectHandle DTI = (Cerebrum.ObjectHandle)civ["DefaultTypeId"];
							TypeInfo ti = (TypeInfo)cbServiceType.SelectedItem;
							civ["DefaultTypeId"] = ti.TypeHandle;
							using(Cerebrum.Data.TableView USV = Cerebrum.Management.Utilites.GetView(DC, "UiServices"))
							{
								Cerebrum.Data.ComponentItemView civS = USV.AddNew();
								SH = civS.ObjectHandle;
								civS["Name"] = tbName.Text;
								if(tbOrdinal.Text != "")
								{
									civS["Ordinal"] = Convert.ToInt32(tbOrdinal.Text);
								}
							}
							civ["DefaultTypeId"] = DTI;
						}
					}
					else
					{
						MessageBox.Show("�� ������ ��� �������. ����������� ����������.");
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnRegServType_Click(object sender, System.EventArgs e)
		{
			try
			{
				string File = "";
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.DefaultExt = "*.dll|*.dll";
				dlg.Filter = "*.dll|*.dll|*.*|*.*";
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					File = dlg.FileName;
					frmModuleTypes frm = new frmModuleTypes(File);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						if(frm.SelectedAssemblyType != "")
						{
							Cerebrum.ObjectHandle TH = Cerebrum.Management.Utilites.ResolveHandle(DC, "Types", "QualifiedTypeName", frm.SelectedAssemblyType);
							if(TH != Cerebrum.ObjectHandle.Null)
							{
								MessageBox.Show("���� ��� ��� ��������������� � ���� ������.");
							}
							else
							{
								using(Cerebrum.Data.TableView tvTypes = Cerebrum.Management.Utilites.GetView(DC, "Types"))
								{
									Cerebrum.Data.ComponentItemView civ = tvTypes.AddNew();

									string[] Cols = frm.SelectedAssemblyType.Split(new char[]{','});
									TypeInfo ti = new TypeInfo();
									ti.TypeName = Cols[0].Trim();
									ti.TypeHandle = civ.ObjectHandle;
									ti.QualifiedTypeName = frm.SelectedAssemblyType;
									ti.KernelObjectType = new ObjectHandle(9);
									TH = civ.ObjectHandle;

									civ["QualifiedTypeName"] = ti.QualifiedTypeName;
									civ["KernelObjectClass"] = ti.KernelObjectType;
									civ["Name"] = ti.TypeName;

									cbServiceType.Items.Add(ti);
								}
								Cerebrum.Management.Utilites.RefreshHandle(DC, "Types", "QualifiedTypeName", TH);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		public string ServiceName
		{
			get
			{
				return tbName.Text;
			}
		}

		public Cerebrum.ObjectHandle ServiceHandle
		{
			get
			{
				return SH;
			}
		}
	}
}
