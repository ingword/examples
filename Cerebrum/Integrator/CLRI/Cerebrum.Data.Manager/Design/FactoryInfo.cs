// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;

namespace Cerebrum.Data.Manager.Design
{
	/// <summary>
	/// Summary description for FactoryInfo.
	/// </summary>
	public class FactoryInfo
	{
		public Cerebrum.ObjectHandle FactoryHandle;
		public string FactoryName;
		public string Description;
		public string DisplayName;

		public FactoryInfo()
		{
			FactoryHandle = Cerebrum.ObjectHandle.Null;
			FactoryName = "";
			Description = "";
			DisplayName = "";
		}

		public override string ToString()
		{
			return FactoryName;
		}
	}
}
