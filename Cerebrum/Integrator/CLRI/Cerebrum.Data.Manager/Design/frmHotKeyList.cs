// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmHotKeyList.
	/// </summary>
	public class frmHotKeyList : System.Windows.Forms.Form
	{
		private Cerebrum.ObjectHandle CtrlHandle;
		private string CtrlName;
		private string KeyCodeVal;
		private Cerebrum.Integrator.DomainContext SelectedDC;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnEdit;
		private System.Windows.Forms.Button btnDel;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.ListView lvHotKeys;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmHotKeyList(Cerebrum.Integrator.DomainContext DC)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			SelectedDC = DC;
			CtrlHandle = Cerebrum.ObjectHandle.Null;
			CtrlName = "";
			KeyCodeVal = "";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.lvHotKeys = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.btnEdit = new System.Windows.Forms.Button();
			this.btnDel = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.lvHotKeys);
			this.panel1.Controls.Add(this.btnEdit);
			this.panel1.Controls.Add(this.btnDel);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(618, 416);
			this.panel1.TabIndex = 0;
			// 
			// lvHotKeys
			// 
			this.lvHotKeys.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.columnHeader1,
																						this.columnHeader2,
																						this.columnHeader3,
																						this.columnHeader4});
			this.lvHotKeys.FullRowSelect = true;
			this.lvHotKeys.GridLines = true;
			this.lvHotKeys.HideSelection = false;
			this.lvHotKeys.Location = new System.Drawing.Point(8, 32);
			this.lvHotKeys.Name = "lvHotKeys";
			this.lvHotKeys.Size = new System.Drawing.Size(600, 376);
			this.lvHotKeys.TabIndex = 6;
			this.lvHotKeys.View = System.Windows.Forms.View.Details;
			this.lvHotKeys.DoubleClick += new System.EventHandler(this.lvHotKeys_DoubleClick);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Object Handle";
			this.columnHeader1.Width = 91;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Name";
			this.columnHeader2.Width = 149;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Description";
			this.columnHeader3.Width = 251;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Key Code";
			this.columnHeader4.Width = 73;
			// 
			// btnEdit
			// 
			this.btnEdit.BackColor = System.Drawing.Color.Turquoise;
			this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.Location = new System.Drawing.Point(528, 8);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(40, 23);
			this.btnEdit.TabIndex = 4;
			this.btnEdit.Text = "Edit";
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// btnDel
			// 
			this.btnDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDel.Location = new System.Drawing.Point(568, 8);
			this.btnDel.Name = "btnDel";
			this.btnDel.Size = new System.Drawing.Size(40, 23);
			this.btnDel.TabIndex = 5;
			this.btnDel.Text = "Del";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// btnNew
			// 
			this.btnNew.BackColor = System.Drawing.Color.Turquoise;
			this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnNew.Location = new System.Drawing.Point(488, 8);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(40, 23);
			this.btnNew.TabIndex = 3;
			this.btnNew.Text = "New";
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(536, 424);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "&Cancel";
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(456, 424);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// frmHotKeyList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(618, 455);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmHotKeyList";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Hot Key list";
			this.Load += new System.EventHandler(this.frmHotKeyList_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmHotKeyList_Load(object sender, System.EventArgs e)
		{
			try
			{
				using(Cerebrum.Data.TableView tvHK = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiHotKeys"))
				{
					for(int i=0; i<tvHK.Count; i++)
					{
						Cerebrum.Data.ComponentItemView civHK = tvHK[i];
						ListViewItem liv = lvHotKeys.Items.Add(civHK.ObjectHandle.ToString());
						liv.SubItems.Add(civHK[Cerebrum.Specialized.KnownNames.Name].ToString());
						liv.SubItems.Add(civHK[Cerebrum.Specialized.KnownNames.Description].ToString());
						liv.SubItems.Add(civHK["KeyCode"].ToString());
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "Cerebrum.Data.Manager.Design.Forms");
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvHotKeys.SelectedItems.Count > 0)
				{
					ListViewItem liv = lvHotKeys.SelectedItems[0];
					CtrlHandle = new ObjectHandle(liv.Text);
					CtrlName = liv.SubItems[1].Text;
					KeyCodeVal = liv.SubItems[3].Text;
				}
				else
				{
					CtrlHandle = Cerebrum.ObjectHandle.Null;
					CtrlName = "";
					KeyCodeVal = "";
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			frmHotKeyItem frm = new frmHotKeyItem(SelectedDC, Cerebrum.ObjectHandle.Null, false);
			if(frm.ShowDialog() == DialogResult.OK)
			{
				ListViewItem liv = lvHotKeys.Items.Add(frm.ControlHandle.ToString());
				liv.SubItems.Add(frm.ControlName);
				liv.SubItems.Add(frm.Description);
				liv.SubItems.Add(frm.KeyCode);
			}
		}

		private void btnEdit_Click(object sender, System.EventArgs e)
		{
			ViewHotKeyEditForm();
		}

		private void ViewHotKeyEditForm()
		{
			if(lvHotKeys.SelectedItems.Count > 0)
			{
				ListViewItem liv = lvHotKeys.SelectedItems[0];
				Cerebrum.ObjectHandle CtrlHandle = new ObjectHandle(liv.Text);
				frmHotKeyItem frm = new frmHotKeyItem(SelectedDC, CtrlHandle, true);
				if(frm.ShowDialog() == DialogResult.OK)
				{
					liv.SubItems[0].Text = frm.ControlHandle.ToString();
					liv.SubItems[1].Text = frm.ControlName;
					liv.SubItems[2].Text = frm.Description;
					liv.SubItems[3].Text = frm.KeyCode;
				}
			}
		}

		private void btnDel_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvHotKeys.SelectedItems.Count > 0)
				{
					ListViewItem liv = lvHotKeys.SelectedItems[0];
					Cerebrum.ObjectHandle CtrlHandle = new ObjectHandle(liv.Text);
					if(frmDesigner.IsDeleteControl(SelectedDC, CtrlHandle) == true)
					{
						if(MessageBox.Show(this, "������� ������� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
						{
							frmDesigner.DeleteItem(SelectedDC, "UiHotKeys", CtrlHandle);
							lvHotKeys.Items.Remove(liv);
						}
					}
					else
					{
						MessageBox.Show("���������� ������� ������� �������. ��� ������������ ���������.");
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void lvHotKeys_DoubleClick(object sender, System.EventArgs e)
		{
			ViewHotKeyEditForm();
		}

		public Cerebrum.ObjectHandle HotKeyHandle
		{
			get
			{
				return CtrlHandle;
			}
		}

		public string HotKeyName
		{
			get
			{
				return CtrlName;
			}
		}

		public string KeyCode
		{
			get
			{
				return KeyCodeVal;
			}
		}
	}
}
