// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmBandPlaceList.
	/// </summary>
	public class frmBandPlaceList : System.Windows.Forms.Form
	{
		private Cerebrum.ObjectHandle CtrlHandle;
		private string CtrlName;
		private Cerebrum.Integrator.DomainContext SelectedDC;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnEdit;
		private System.Windows.Forms.Button btnDel;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.ListView lvBandPlaces;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmBandPlaceList(Cerebrum.Integrator.DomainContext DC)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			SelectedDC = DC;
			CtrlHandle = Cerebrum.ObjectHandle.Null;
			CtrlName = "";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.lvBandPlaces = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnEdit = new System.Windows.Forms.Button();
			this.btnDel = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.lvBandPlaces);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Controls.Add(this.btnEdit);
			this.panel1.Controls.Add(this.btnDel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(730, 384);
			this.panel1.TabIndex = 0;
			// 
			// lvBandPlaces
			// 
			this.lvBandPlaces.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						   this.columnHeader1,
																						   this.columnHeader2,
																						   this.columnHeader3,
																						   this.columnHeader4,
																						   this.columnHeader5,
																						   this.columnHeader6});
			this.lvBandPlaces.FullRowSelect = true;
			this.lvBandPlaces.GridLines = true;
			this.lvBandPlaces.HideSelection = false;
			this.lvBandPlaces.Location = new System.Drawing.Point(8, 32);
			this.lvBandPlaces.Name = "lvBandPlaces";
			this.lvBandPlaces.Size = new System.Drawing.Size(712, 344);
			this.lvBandPlaces.TabIndex = 10;
			this.lvBandPlaces.View = System.Windows.Forms.View.Details;
			this.lvBandPlaces.DoubleClick += new System.EventHandler(this.lvBandPlaces_DoubleClick);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Object Handle";
			this.columnHeader1.Width = 87;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Name";
			this.columnHeader2.Width = 133;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Display Name";
			this.columnHeader3.Width = 133;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Description";
			this.columnHeader4.Width = 170;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Merge Order";
			this.columnHeader5.Width = 77;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "Width";
			this.columnHeader6.Width = 79;
			// 
			// btnNew
			// 
			this.btnNew.BackColor = System.Drawing.Color.Turquoise;
			this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnNew.Location = new System.Drawing.Point(600, 8);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(40, 23);
			this.btnNew.TabIndex = 7;
			this.btnNew.Text = "New";
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnEdit
			// 
			this.btnEdit.BackColor = System.Drawing.Color.Turquoise;
			this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.Location = new System.Drawing.Point(640, 8);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(40, 23);
			this.btnEdit.TabIndex = 8;
			this.btnEdit.Text = "Edit";
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// btnDel
			// 
			this.btnDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDel.Location = new System.Drawing.Point(680, 8);
			this.btnDel.Name = "btnDel";
			this.btnDel.Size = new System.Drawing.Size(40, 23);
			this.btnDel.TabIndex = 9;
			this.btnDel.Text = "Del";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(648, 392);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "&Cancel";
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(568, 392);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 3;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// frmBandPlaceList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(730, 423);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmBandPlaceList";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Band Place list";
			this.Load += new System.EventHandler(this.frmBandPlaceList_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmBandPlaceList_Load(object sender, System.EventArgs e)
		{
			try
			{
				using(Cerebrum.Data.TableView tvBP = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiBandPlaces"))
				{
					for(int i=0; i<tvBP.Count; i++)
					{
						Cerebrum.Data.ComponentItemView civ = tvBP[i];
						ListViewItem liv = lvBandPlaces.Items.Add(civ.ObjectHandle.ToString());
						liv.SubItems.Add(civ[Cerebrum.Specialized.KnownNames.Name].ToString());
						liv.SubItems.Add(civ[Cerebrum.Specialized.KnownNames.DisplayName].ToString());
						liv.SubItems.Add(civ[Cerebrum.Specialized.KnownNames.Description].ToString());
						liv.SubItems.Add(civ["MergeOrder"].ToString());
						liv.SubItems.Add(civ["Width"].ToString());
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "Cerebrum.Data.Manager.Design.Forms");
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvBandPlaces.SelectedItems.Count > 0)
				{
					ListViewItem liv = lvBandPlaces.SelectedItems[0];
					CtrlHandle = new ObjectHandle(liv.Text);
					CtrlName = liv.SubItems[1].Text;
				}
				else
				{
					CtrlHandle = Cerebrum.ObjectHandle.Null;
					CtrlName = "";
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			frmBandPlace frm = new frmBandPlace(SelectedDC, Cerebrum.ObjectHandle.Null, false);
			if(frm.ShowDialog() == DialogResult.OK)
			{
				ListViewItem liv = lvBandPlaces.Items.Add(frm.ControlHandle.ToString());
				liv.SubItems.Add(frm.ControlName);
				liv.SubItems.Add(frm.DisplayName);
				liv.SubItems.Add(frm.Description);
				liv.SubItems.Add(frm.MergeOrder.ToString());
				liv.SubItems.Add(frm.PlaceWidth.ToString());
			}
		}

		private void btnEdit_Click(object sender, System.EventArgs e)
		{
			ViewBandPlaceEditForm();
		}

		private void btnDel_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvBandPlaces.SelectedItems.Count > 0)
				{
					ListViewItem liv = lvBandPlaces.SelectedItems[0];
					Cerebrum.ObjectHandle CtrlHandle = new ObjectHandle(liv.Text);
					if(frmDesigner.IsDeleteControl(SelectedDC, CtrlHandle) == true)
					{
						if(MessageBox.Show(this, "������� ������ � ������ ������������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
						{
							frmDesigner.DeleteItem(SelectedDC, "UiBandPlaces", CtrlHandle);
							lvBandPlaces.Items.Remove(liv);
						}
					}
					else
					{
						MessageBox.Show("���������� ������� ������ � ������ ������������. ��� ������������ ���������.");
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void lvBandPlaces_DoubleClick(object sender, System.EventArgs e)
		{
			ViewBandPlaceEditForm();
		}

		private void ViewBandPlaceEditForm()
		{
			try
			{
				if(lvBandPlaces.SelectedItems.Count > 0)
				{
					ListViewItem liv = lvBandPlaces.SelectedItems[0];
					Cerebrum.ObjectHandle CtrlHandle = new ObjectHandle(liv.Text);
					frmBandPlace frm = new frmBandPlace(SelectedDC, CtrlHandle, true);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						liv.SubItems[1].Text = frm.ControlName;
						liv.SubItems[2].Text = frm.DisplayName;
						liv.SubItems[3].Text = frm.Description;
						liv.SubItems[4].Text = frm.MergeOrder.ToString();
						liv.SubItems[5].Text = frm.PlaceWidth.ToString();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		public Cerebrum.ObjectHandle BandPlaceHandle
		{
			get
			{
				return CtrlHandle;
			}
		}

		public string BandPlaceName
		{
			get
			{
				return CtrlName;
			}
		}
	}
}
