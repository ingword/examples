// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Cerebrum.Windows.Forms.Reflection;
using Cerebrum.Data.Manager.Forms;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for DesignerForm.
	/// </summary>
	public class frmDesigner : System.Windows.Forms.Form
	{
		private Cerebrum.Integrator.DomainContext SelectedDC;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ListView lvCommandList;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ListView lvBandItems;
		private System.Windows.Forms.ListView lvHotKeys;
		private System.Windows.Forms.ColumnHeader columnHeader7;
		private System.Windows.Forms.ColumnHeader columnHeader8;
		private System.Windows.Forms.ColumnHeader columnHeader9;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ListView lvServiceList;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ColumnHeader columnHeader10;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.Button btnMenuItemAdd;
		private System.Windows.Forms.Button btnMenuItemDisconnect;
		private System.Windows.Forms.ColumnHeader columnHeader11;
		private System.Windows.Forms.ColumnHeader columnHeader12;
		private System.Windows.Forms.ListView lvMenuItems;
		private System.Windows.Forms.Button btnMenuItemDel;
		private System.Windows.Forms.Button btnServiceNew;
		private System.Windows.Forms.Button btnServiceEdit;
		private System.Windows.Forms.Button btnServiceDel;
		private System.Windows.Forms.Button btnCmdAdd;
		private System.Windows.Forms.Button btnCmdEdit;
		private System.Windows.Forms.Button btnCmdDisconnect;
		private System.Windows.Forms.Button btnCmdDel;
		private System.Windows.Forms.Button btnBandItemAdd;
		private System.Windows.Forms.Button btnBandItemEdit;
		private System.Windows.Forms.Button btnBandItemDisconnect;
		private System.Windows.Forms.Button btnBandItemDel;
		private System.Windows.Forms.Button btnHotKeyAdd;
		private System.Windows.Forms.Button btnHotKeyEdit;
		private System.Windows.Forms.Button btnHotKeyDisconnect;
		private System.Windows.Forms.Button btnHotKeyDel;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnBandPlaceDisconnect;
		private System.Windows.Forms.ListView lvBandPlaces;
		private System.Windows.Forms.ColumnHeader columnHeader13;
		private System.Windows.Forms.ColumnHeader columnHeader14;
		private System.Windows.Forms.Button btnBandPlaceAdd;
		private System.Windows.Forms.Button btnBandPlaceDel;
		private System.Windows.Forms.Button btnBandPlaceEdit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmDesigner(Cerebrum.Integrator.DomainContext DC)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			SelectedDC = DC;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCmdDel = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.lvCommandList = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
			this.lvBandItems = new System.Windows.Forms.ListView();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.label3 = new System.Windows.Forms.Label();
			this.lvHotKeys = new System.Windows.Forms.ListView();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.lvServiceList = new System.Windows.Forms.ListView();
			this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
			this.label6 = new System.Windows.Forms.Label();
			this.btnServiceDel = new System.Windows.Forms.Button();
			this.btnServiceEdit = new System.Windows.Forms.Button();
			this.btnServiceNew = new System.Windows.Forms.Button();
			this.btnCmdDisconnect = new System.Windows.Forms.Button();
			this.btnCmdAdd = new System.Windows.Forms.Button();
			this.btnCmdEdit = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.btnBandItemDisconnect = new System.Windows.Forms.Button();
			this.btnBandItemAdd = new System.Windows.Forms.Button();
			this.btnBandItemDel = new System.Windows.Forms.Button();
			this.btnBandItemEdit = new System.Windows.Forms.Button();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.btnHotKeyAdd = new System.Windows.Forms.Button();
			this.btnHotKeyDisconnect = new System.Windows.Forms.Button();
			this.btnHotKeyDel = new System.Windows.Forms.Button();
			this.btnHotKeyEdit = new System.Windows.Forms.Button();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.lvMenuItems = new System.Windows.Forms.ListView();
			this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
			this.btnMenuItemAdd = new System.Windows.Forms.Button();
			this.btnMenuItemDisconnect = new System.Windows.Forms.Button();
			this.btnMenuItemDel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.label1 = new System.Windows.Forms.Label();
			this.btnBandPlaceDisconnect = new System.Windows.Forms.Button();
			this.lvBandPlaces = new System.Windows.Forms.ListView();
			this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
			this.btnBandPlaceAdd = new System.Windows.Forms.Button();
			this.btnBandPlaceDel = new System.Windows.Forms.Button();
			this.btnBandPlaceEdit = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnCmdDel
			// 
			this.btnCmdDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCmdDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCmdDel.Location = new System.Drawing.Point(336, 160);
			this.btnCmdDel.Name = "btnCmdDel";
			this.btnCmdDel.Size = new System.Drawing.Size(40, 23);
			this.btnCmdDel.TabIndex = 9;
			this.btnCmdDel.Text = "Del";
			this.btnCmdDel.Click += new System.EventHandler(this.btnCmdDel_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 160);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(176, 23);
			this.label2.TabIndex = 4;
			this.label2.Text = "������� � ��������� �������";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lvCommandList
			// 
			this.lvCommandList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																							this.columnHeader1,
																							this.columnHeader2,
																							this.columnHeader7});
			this.lvCommandList.FullRowSelect = true;
			this.lvCommandList.GridLines = true;
			this.lvCommandList.HideSelection = false;
			this.lvCommandList.Location = new System.Drawing.Point(8, 184);
			this.lvCommandList.MultiSelect = false;
			this.lvCommandList.Name = "lvCommandList";
			this.lvCommandList.Size = new System.Drawing.Size(368, 120);
			this.lvCommandList.TabIndex = 22;
			this.lvCommandList.View = System.Windows.Forms.View.Details;
			this.lvCommandList.DoubleClick += new System.EventHandler(this.lvCommandList_DoubleClick);
			this.lvCommandList.SelectedIndexChanged += new System.EventHandler(this.lvCommandList_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "ObjectHandle";
			this.columnHeader1.Width = 77;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Name";
			this.columnHeader2.Width = 141;
			// 
			// columnHeader7
			// 
			this.columnHeader7.Text = "Message";
			this.columnHeader7.Width = 122;
			// 
			// lvBandItems
			// 
			this.lvBandItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						  this.columnHeader3,
																						  this.columnHeader4});
			this.lvBandItems.FullRowSelect = true;
			this.lvBandItems.GridLines = true;
			this.lvBandItems.HideSelection = false;
			this.lvBandItems.Location = new System.Drawing.Point(8, 32);
			this.lvBandItems.MultiSelect = false;
			this.lvBandItems.Name = "lvBandItems";
			this.lvBandItems.Size = new System.Drawing.Size(312, 176);
			this.lvBandItems.TabIndex = 23;
			this.lvBandItems.View = System.Windows.Forms.View.Details;
			this.lvBandItems.DoubleClick += new System.EventHandler(this.lvBandItems_DoubleClick);
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "ObjectHandle";
			this.columnHeader3.Width = 78;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Name";
			this.columnHeader4.Width = 197;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 8);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(120, 23);
			this.label3.TabIndex = 6;
			this.label3.Text = "������ ������������";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lvHotKeys
			// 
			this.lvHotKeys.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.columnHeader5,
																						this.columnHeader6});
			this.lvHotKeys.FullRowSelect = true;
			this.lvHotKeys.GridLines = true;
			this.lvHotKeys.HideSelection = false;
			this.lvHotKeys.Location = new System.Drawing.Point(8, 32);
			this.lvHotKeys.MultiSelect = false;
			this.lvHotKeys.Name = "lvHotKeys";
			this.lvHotKeys.Size = new System.Drawing.Size(312, 176);
			this.lvHotKeys.TabIndex = 24;
			this.lvHotKeys.View = System.Windows.Forms.View.Details;
			this.lvHotKeys.DoubleClick += new System.EventHandler(this.lvHotKeys_DoubleClick);
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "ObjectHandle";
			this.columnHeader5.Width = 78;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "KeyCode";
			this.columnHeader6.Width = 197;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 8);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(104, 23);
			this.label4.TabIndex = 8;
			this.label4.Text = "������� �������";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 8);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 23);
			this.label5.TabIndex = 10;
			this.label5.Text = "������ ����";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lvServiceList
			// 
			this.lvServiceList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																							this.columnHeader8,
																							this.columnHeader9,
																							this.columnHeader10});
			this.lvServiceList.FullRowSelect = true;
			this.lvServiceList.GridLines = true;
			this.lvServiceList.HideSelection = false;
			this.lvServiceList.Location = new System.Drawing.Point(8, 32);
			this.lvServiceList.MultiSelect = false;
			this.lvServiceList.Name = "lvServiceList";
			this.lvServiceList.Size = new System.Drawing.Size(536, 120);
			this.lvServiceList.TabIndex = 21;
			this.lvServiceList.View = System.Windows.Forms.View.Details;
			this.lvServiceList.DoubleClick += new System.EventHandler(this.lvServiceList_DoubleClick);
			this.lvServiceList.SelectedIndexChanged += new System.EventHandler(this.lvServiceList_SelectedIndexChanged);
			// 
			// columnHeader8
			// 
			this.columnHeader8.Text = "ObjectHandle";
			this.columnHeader8.Width = 78;
			// 
			// columnHeader9
			// 
			this.columnHeader9.Text = "Name";
			this.columnHeader9.Width = 136;
			// 
			// columnHeader10
			// 
			this.columnHeader10.Text = "Type name";
			this.columnHeader10.Width = 290;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 8);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(104, 23);
			this.label6.TabIndex = 12;
			this.label6.Text = "������ ��������";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnServiceDel
			// 
			this.btnServiceDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnServiceDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnServiceDel.Location = new System.Drawing.Point(504, 8);
			this.btnServiceDel.Name = "btnServiceDel";
			this.btnServiceDel.Size = new System.Drawing.Size(40, 23);
			this.btnServiceDel.TabIndex = 5;
			this.btnServiceDel.Text = "Del";
			this.btnServiceDel.Click += new System.EventHandler(this.btnServiceDel_Click);
			// 
			// btnServiceEdit
			// 
			this.btnServiceEdit.BackColor = System.Drawing.Color.Turquoise;
			this.btnServiceEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnServiceEdit.Location = new System.Drawing.Point(464, 8);
			this.btnServiceEdit.Name = "btnServiceEdit";
			this.btnServiceEdit.Size = new System.Drawing.Size(40, 23);
			this.btnServiceEdit.TabIndex = 4;
			this.btnServiceEdit.Text = "Edit";
			this.btnServiceEdit.Click += new System.EventHandler(this.btnServiceEdit_Click);
			// 
			// btnServiceNew
			// 
			this.btnServiceNew.BackColor = System.Drawing.Color.Turquoise;
			this.btnServiceNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnServiceNew.Location = new System.Drawing.Point(424, 8);
			this.btnServiceNew.Name = "btnServiceNew";
			this.btnServiceNew.Size = new System.Drawing.Size(40, 23);
			this.btnServiceNew.TabIndex = 3;
			this.btnServiceNew.Text = "New";
			this.btnServiceNew.Click += new System.EventHandler(this.btnServiceNew_Click);
			// 
			// btnCmdDisconnect
			// 
			this.btnCmdDisconnect.BackColor = System.Drawing.Color.Turquoise;
			this.btnCmdDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCmdDisconnect.Location = new System.Drawing.Point(264, 160);
			this.btnCmdDisconnect.Name = "btnCmdDisconnect";
			this.btnCmdDisconnect.Size = new System.Drawing.Size(72, 23);
			this.btnCmdDisconnect.TabIndex = 8;
			this.btnCmdDisconnect.Text = "Disconnect";
			this.btnCmdDisconnect.Click += new System.EventHandler(this.btnCmdDisconnect_Click);
			// 
			// btnCmdAdd
			// 
			this.btnCmdAdd.BackColor = System.Drawing.Color.Turquoise;
			this.btnCmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCmdAdd.Location = new System.Drawing.Point(184, 160);
			this.btnCmdAdd.Name = "btnCmdAdd";
			this.btnCmdAdd.Size = new System.Drawing.Size(40, 23);
			this.btnCmdAdd.TabIndex = 6;
			this.btnCmdAdd.Text = "Add";
			this.btnCmdAdd.Click += new System.EventHandler(this.btnCmdAdd_Click);
			// 
			// btnCmdEdit
			// 
			this.btnCmdEdit.BackColor = System.Drawing.Color.Turquoise;
			this.btnCmdEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCmdEdit.Location = new System.Drawing.Point(224, 160);
			this.btnCmdEdit.Name = "btnCmdEdit";
			this.btnCmdEdit.Size = new System.Drawing.Size(40, 23);
			this.btnCmdEdit.TabIndex = 7;
			this.btnCmdEdit.Text = "Edit";
			this.btnCmdEdit.Click += new System.EventHandler(this.btnCmdEdit_Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.tabControl1);
			this.panel1.Controls.Add(this.btnCmdEdit);
			this.panel1.Controls.Add(this.btnCmdDel);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.lvCommandList);
			this.panel1.Controls.Add(this.lvServiceList);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.btnServiceDel);
			this.panel1.Controls.Add(this.btnServiceEdit);
			this.panel1.Controls.Add(this.btnServiceNew);
			this.panel1.Controls.Add(this.btnCmdDisconnect);
			this.panel1.Controls.Add(this.btnCmdAdd);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(554, 552);
			this.panel1.TabIndex = 20;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.tabControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.tabControl1.Location = new System.Drawing.Point(0, 312);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(554, 240);
			this.tabControl1.TabIndex = 27;
			// 
			// tabPage1
			// 
			this.tabPage1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.btnBandItemDisconnect);
			this.tabPage1.Controls.Add(this.lvBandItems);
			this.tabPage1.Controls.Add(this.btnBandItemAdd);
			this.tabPage1.Controls.Add(this.btnBandItemDel);
			this.tabPage1.Controls.Add(this.btnBandItemEdit);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(546, 214);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Band items";
			// 
			// btnBandItemDisconnect
			// 
			this.btnBandItemDisconnect.BackColor = System.Drawing.Color.Turquoise;
			this.btnBandItemDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBandItemDisconnect.Location = new System.Drawing.Point(208, 8);
			this.btnBandItemDisconnect.Name = "btnBandItemDisconnect";
			this.btnBandItemDisconnect.Size = new System.Drawing.Size(72, 23);
			this.btnBandItemDisconnect.TabIndex = 12;
			this.btnBandItemDisconnect.Text = "Disconnect";
			this.btnBandItemDisconnect.Click += new System.EventHandler(this.btnBandItemDisconnect_Click);
			// 
			// btnBandItemAdd
			// 
			this.btnBandItemAdd.BackColor = System.Drawing.Color.Turquoise;
			this.btnBandItemAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBandItemAdd.Location = new System.Drawing.Point(128, 8);
			this.btnBandItemAdd.Name = "btnBandItemAdd";
			this.btnBandItemAdd.Size = new System.Drawing.Size(40, 23);
			this.btnBandItemAdd.TabIndex = 10;
			this.btnBandItemAdd.Text = "Add";
			this.btnBandItemAdd.Click += new System.EventHandler(this.btnBandItemAdd_Click);
			// 
			// btnBandItemDel
			// 
			this.btnBandItemDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnBandItemDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBandItemDel.Location = new System.Drawing.Point(280, 8);
			this.btnBandItemDel.Name = "btnBandItemDel";
			this.btnBandItemDel.Size = new System.Drawing.Size(40, 23);
			this.btnBandItemDel.TabIndex = 13;
			this.btnBandItemDel.Text = "Del";
			this.btnBandItemDel.Click += new System.EventHandler(this.btnBandItemDel_Click);
			// 
			// btnBandItemEdit
			// 
			this.btnBandItemEdit.BackColor = System.Drawing.Color.Turquoise;
			this.btnBandItemEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBandItemEdit.Location = new System.Drawing.Point(168, 8);
			this.btnBandItemEdit.Name = "btnBandItemEdit";
			this.btnBandItemEdit.Size = new System.Drawing.Size(40, 23);
			this.btnBandItemEdit.TabIndex = 11;
			this.btnBandItemEdit.Text = "Edit";
			this.btnBandItemEdit.Click += new System.EventHandler(this.btnBandItemEdit_Click);
			// 
			// tabPage2
			// 
			this.tabPage2.BackColor = System.Drawing.Color.MediumAquamarine;
			this.tabPage2.Controls.Add(this.lvHotKeys);
			this.tabPage2.Controls.Add(this.btnHotKeyAdd);
			this.tabPage2.Controls.Add(this.btnHotKeyDisconnect);
			this.tabPage2.Controls.Add(this.btnHotKeyDel);
			this.tabPage2.Controls.Add(this.btnHotKeyEdit);
			this.tabPage2.Controls.Add(this.label4);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(546, 214);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Hot keys";
			// 
			// btnHotKeyAdd
			// 
			this.btnHotKeyAdd.BackColor = System.Drawing.Color.Turquoise;
			this.btnHotKeyAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnHotKeyAdd.Location = new System.Drawing.Point(128, 8);
			this.btnHotKeyAdd.Name = "btnHotKeyAdd";
			this.btnHotKeyAdd.Size = new System.Drawing.Size(40, 23);
			this.btnHotKeyAdd.TabIndex = 14;
			this.btnHotKeyAdd.Text = "Add";
			this.btnHotKeyAdd.Click += new System.EventHandler(this.btnHotKeyAdd_Click);
			// 
			// btnHotKeyDisconnect
			// 
			this.btnHotKeyDisconnect.BackColor = System.Drawing.Color.Turquoise;
			this.btnHotKeyDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnHotKeyDisconnect.Location = new System.Drawing.Point(208, 8);
			this.btnHotKeyDisconnect.Name = "btnHotKeyDisconnect";
			this.btnHotKeyDisconnect.Size = new System.Drawing.Size(72, 23);
			this.btnHotKeyDisconnect.TabIndex = 16;
			this.btnHotKeyDisconnect.Text = "Disconnect";
			this.btnHotKeyDisconnect.Click += new System.EventHandler(this.btnHotKeyDisconnect_Click);
			// 
			// btnHotKeyDel
			// 
			this.btnHotKeyDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnHotKeyDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnHotKeyDel.Location = new System.Drawing.Point(280, 8);
			this.btnHotKeyDel.Name = "btnHotKeyDel";
			this.btnHotKeyDel.Size = new System.Drawing.Size(40, 23);
			this.btnHotKeyDel.TabIndex = 17;
			this.btnHotKeyDel.Text = "Del";
			this.btnHotKeyDel.Click += new System.EventHandler(this.btnHotKeyDel_Click);
			// 
			// btnHotKeyEdit
			// 
			this.btnHotKeyEdit.BackColor = System.Drawing.Color.Turquoise;
			this.btnHotKeyEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnHotKeyEdit.Location = new System.Drawing.Point(168, 8);
			this.btnHotKeyEdit.Name = "btnHotKeyEdit";
			this.btnHotKeyEdit.Size = new System.Drawing.Size(40, 23);
			this.btnHotKeyEdit.TabIndex = 15;
			this.btnHotKeyEdit.Text = "Edit";
			this.btnHotKeyEdit.Click += new System.EventHandler(this.btnHotKeyEdit_Click);
			// 
			// tabPage3
			// 
			this.tabPage3.BackColor = System.Drawing.Color.MediumAquamarine;
			this.tabPage3.Controls.Add(this.lvMenuItems);
			this.tabPage3.Controls.Add(this.btnMenuItemAdd);
			this.tabPage3.Controls.Add(this.btnMenuItemDisconnect);
			this.tabPage3.Controls.Add(this.btnMenuItemDel);
			this.tabPage3.Controls.Add(this.label5);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(546, 214);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Menu items";
			// 
			// lvMenuItems
			// 
			this.lvMenuItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						  this.columnHeader11,
																						  this.columnHeader12});
			this.lvMenuItems.FullRowSelect = true;
			this.lvMenuItems.GridLines = true;
			this.lvMenuItems.HideSelection = false;
			this.lvMenuItems.Location = new System.Drawing.Point(8, 32);
			this.lvMenuItems.MultiSelect = false;
			this.lvMenuItems.Name = "lvMenuItems";
			this.lvMenuItems.Size = new System.Drawing.Size(312, 176);
			this.lvMenuItems.TabIndex = 25;
			this.lvMenuItems.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader11
			// 
			this.columnHeader11.Text = "ObjectHandle";
			this.columnHeader11.Width = 78;
			// 
			// columnHeader12
			// 
			this.columnHeader12.Text = "Name";
			this.columnHeader12.Width = 197;
			// 
			// btnMenuItemAdd
			// 
			this.btnMenuItemAdd.BackColor = System.Drawing.Color.Turquoise;
			this.btnMenuItemAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnMenuItemAdd.Location = new System.Drawing.Point(168, 8);
			this.btnMenuItemAdd.Name = "btnMenuItemAdd";
			this.btnMenuItemAdd.Size = new System.Drawing.Size(40, 23);
			this.btnMenuItemAdd.TabIndex = 18;
			this.btnMenuItemAdd.Text = "Add";
			this.btnMenuItemAdd.Click += new System.EventHandler(this.btnMenuItemAdd_Click);
			// 
			// btnMenuItemDisconnect
			// 
			this.btnMenuItemDisconnect.BackColor = System.Drawing.Color.Turquoise;
			this.btnMenuItemDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnMenuItemDisconnect.Location = new System.Drawing.Point(208, 8);
			this.btnMenuItemDisconnect.Name = "btnMenuItemDisconnect";
			this.btnMenuItemDisconnect.Size = new System.Drawing.Size(72, 23);
			this.btnMenuItemDisconnect.TabIndex = 19;
			this.btnMenuItemDisconnect.Text = "Disconnect";
			this.btnMenuItemDisconnect.Click += new System.EventHandler(this.btnMenuItemDisconnect_Click);
			// 
			// btnMenuItemDel
			// 
			this.btnMenuItemDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnMenuItemDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnMenuItemDel.Location = new System.Drawing.Point(280, 8);
			this.btnMenuItemDel.Name = "btnMenuItemDel";
			this.btnMenuItemDel.Size = new System.Drawing.Size(40, 23);
			this.btnMenuItemDel.TabIndex = 20;
			this.btnMenuItemDel.Text = "Del";
			this.btnMenuItemDel.Click += new System.EventHandler(this.btnMenuItemDel_Click);
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(392, 560);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "&OK";
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(472, 560);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "&Cancel";
			// 
			// tabPage4
			// 
			this.tabPage4.BackColor = System.Drawing.Color.MediumAquamarine;
			this.tabPage4.Controls.Add(this.label1);
			this.tabPage4.Controls.Add(this.btnBandPlaceDisconnect);
			this.tabPage4.Controls.Add(this.lvBandPlaces);
			this.tabPage4.Controls.Add(this.btnBandPlaceAdd);
			this.tabPage4.Controls.Add(this.btnBandPlaceDel);
			this.tabPage4.Controls.Add(this.btnBandPlaceEdit);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Size = new System.Drawing.Size(546, 214);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "Band places";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 23);
			this.label1.TabIndex = 24;
			this.label1.Text = "������ ������������";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnBandPlaceDisconnect
			// 
			this.btnBandPlaceDisconnect.BackColor = System.Drawing.Color.Turquoise;
			this.btnBandPlaceDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBandPlaceDisconnect.Location = new System.Drawing.Point(208, 8);
			this.btnBandPlaceDisconnect.Name = "btnBandPlaceDisconnect";
			this.btnBandPlaceDisconnect.Size = new System.Drawing.Size(72, 23);
			this.btnBandPlaceDisconnect.TabIndex = 27;
			this.btnBandPlaceDisconnect.Text = "Disconnect";
			this.btnBandPlaceDisconnect.Click += new System.EventHandler(this.btnBandPlaceDisconnect_Click);
			// 
			// lvBandPlaces
			// 
			this.lvBandPlaces.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						   this.columnHeader13,
																						   this.columnHeader14});
			this.lvBandPlaces.FullRowSelect = true;
			this.lvBandPlaces.GridLines = true;
			this.lvBandPlaces.HideSelection = false;
			this.lvBandPlaces.Location = new System.Drawing.Point(8, 32);
			this.lvBandPlaces.MultiSelect = false;
			this.lvBandPlaces.Name = "lvBandPlaces";
			this.lvBandPlaces.Size = new System.Drawing.Size(312, 176);
			this.lvBandPlaces.TabIndex = 29;
			this.lvBandPlaces.View = System.Windows.Forms.View.Details;
			this.lvBandPlaces.DoubleClick += new System.EventHandler(this.lvBandPlaces_DoubleClick);
			// 
			// columnHeader13
			// 
			this.columnHeader13.Text = "ObjectHandle";
			this.columnHeader13.Width = 78;
			// 
			// columnHeader14
			// 
			this.columnHeader14.Text = "Name";
			this.columnHeader14.Width = 197;
			// 
			// btnBandPlaceAdd
			// 
			this.btnBandPlaceAdd.BackColor = System.Drawing.Color.Turquoise;
			this.btnBandPlaceAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBandPlaceAdd.Location = new System.Drawing.Point(128, 8);
			this.btnBandPlaceAdd.Name = "btnBandPlaceAdd";
			this.btnBandPlaceAdd.Size = new System.Drawing.Size(40, 23);
			this.btnBandPlaceAdd.TabIndex = 25;
			this.btnBandPlaceAdd.Text = "Add";
			this.btnBandPlaceAdd.Click += new System.EventHandler(this.btnBandPlaceAdd_Click);
			// 
			// btnBandPlaceDel
			// 
			this.btnBandPlaceDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnBandPlaceDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBandPlaceDel.Location = new System.Drawing.Point(280, 8);
			this.btnBandPlaceDel.Name = "btnBandPlaceDel";
			this.btnBandPlaceDel.Size = new System.Drawing.Size(40, 23);
			this.btnBandPlaceDel.TabIndex = 28;
			this.btnBandPlaceDel.Text = "Del";
			this.btnBandPlaceDel.Click += new System.EventHandler(this.btnBandPlaceDel_Click);
			// 
			// btnBandPlaceEdit
			// 
			this.btnBandPlaceEdit.BackColor = System.Drawing.Color.Turquoise;
			this.btnBandPlaceEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBandPlaceEdit.Location = new System.Drawing.Point(168, 8);
			this.btnBandPlaceEdit.Name = "btnBandPlaceEdit";
			this.btnBandPlaceEdit.Size = new System.Drawing.Size(40, 23);
			this.btnBandPlaceEdit.TabIndex = 26;
			this.btnBandPlaceEdit.Text = "Edit";
			this.btnBandPlaceEdit.Click += new System.EventHandler(this.btnBandPlaceEdit_Click);
			// 
			// frmDesigner
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(554, 591);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmDesigner";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "UI Design";
			this.Load += new System.EventHandler(this.DesignerForm_Load);
			this.panel1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void DesignerForm_Load(object sender, System.EventArgs e)
		{
			FillServiceList();
		}

		private void FillServiceList()
		{
			try
			{
				lvServiceList.Items.Clear();
				lvCommandList.Items.Clear();
				lvBandItems.Items.Clear();
				lvHotKeys.Items.Clear();
				lvMenuItems.Items.Clear();

				Cerebrum.Integrator.DomainContext dc = this.SelectedDC;
				if(dc != null)
				{
					using(Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(dc, "UiServices"))
					{
						if(view != null)
						{
							for(int i=0; i<view.Count; i++)
							{
								Cerebrum.Data.ComponentItemView civ = view[i];
								ListViewItem liv = lvServiceList.Items.Add(civ.ObjectHandle.ToString());
								liv.SubItems.Add(civ[Cerebrum.Specialized.KnownNames.Name].ToString());
								Cerebrum.ObjectHandle typeID = (Cerebrum.ObjectHandle)civ[Cerebrum.Specialized.KnownNames.TypeIdHandle];
								using(Cerebrum.IConnector connType = dc.AttachConnector(typeID))
								{
									if(connType!=null && connType.Component is Cerebrum.Reflection.TypeDescriptor)
									{
										Cerebrum.Reflection.TypeDescriptor tds = connType.Component as Cerebrum.Reflection.TypeDescriptor;
										liv.SubItems.Add(tds.QualifiedTypeName);
									}
									else
									{
										liv.SubItems.Add(typeID.ToString());
									}
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "DesignerForm_Load");
			}
		}

		private void lbDomainList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				lvServiceList.Items.Clear();
				lvCommandList.Items.Clear();
				lvBandItems.Items.Clear();
				lvHotKeys.Items.Clear();
				lvMenuItems.Items.Clear();

				using(Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiServices"))
				{
					if(view != null)
					{
						for(int i=0; i<view.Count; i++)
						{
							Cerebrum.Data.ComponentItemView civ = view[i];
							ListViewItem liv = lvServiceList.Items.Add(civ.ObjectHandle.ToString());
							liv.SubItems.Add(civ["Name"].ToString());
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void lvServiceList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				lvCommandList.Items.Clear();
				lvBandItems.Items.Clear();
				lvHotKeys.Items.Clear();
				lvMenuItems.Items.Clear();

				if(lvServiceList.SelectedItems.Count > 0)
				{
					ListViewItem liv1 = lvServiceList.SelectedItems[0];
					Cerebrum.ObjectHandle ServiceHandle = new ObjectHandle(liv1.Text);
					using(Cerebrum.Data.TableView view1 = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiServices"))
					{
						Cerebrum.Data.ComponentItemView civ1 = view1.FindItemViewByObjectHandle(ServiceHandle);
						Cerebrum.Data.VectorView vvAttachedCommands = (Cerebrum.Data.VectorView)civ1["AttachedCommands"];
						using(Cerebrum.Data.TableView view2 = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiCommands"))
						{
							for(int i=0; i<vvAttachedCommands.Count; i++)
							{
								Cerebrum.ObjectHandle CmdHandle = vvAttachedCommands[i].ObjectHandle;
								Cerebrum.Data.ComponentItemView civ2 = view2.FindItemViewByObjectHandle(CmdHandle);
								ListViewItem liv2 = lvCommandList.Items.Add(civ2.ObjectHandle.ToString());
								liv2.SubItems.Add(civ2["Name"].ToString());
								Cerebrum.ObjectHandle MessageHandle = (Cerebrum.ObjectHandle)civ2["MessageId"];
								using(Cerebrum.IComposite compM = SelectedDC.AttachConnector(MessageHandle))
								{
									Cerebrum.Reflection.MessageDescriptor md = (Cerebrum.Reflection.MessageDescriptor)compM.Component;
									Cerebrum.ObjectHandle AttrNameHandle = new ObjectHandle(1000);
									string MessageName = md.GetAttributeComponent(AttrNameHandle).ToString();
									liv2.SubItems.Add(MessageName);
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void lvCommandList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				FillControls();
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		/// <summary>
		/// ��������� ������� ���������� �� �������.
		/// </summary>
		/// <param name="CtrlHandle">����� �������� ����������.</param>
		/// <param name="CommandHandle">����� �������.</param>
		public static void DisableControlFromCommand(Cerebrum.Integrator.DomainContext DC, Cerebrum.ObjectHandle CtrlHandle, Cerebrum.ObjectHandle CommandHandle)
		{
			// ��������� ������ �� �������.
			using(Cerebrum.Data.TableView tvCmd = Cerebrum.Management.Utilites.GetView(DC, "UiCommands"))
			{
				Cerebrum.Data.ComponentItemView civCmd = tvCmd.FindItemViewByObjectHandle(CommandHandle);
				Cerebrum.Data.VectorView vvAC = (Cerebrum.Data.VectorView)civCmd["AttachedControls"];
				Cerebrum.Data.ComponentItemView civCtrl = vvAC.FindItemViewByObjectHandle(CtrlHandle);
				vvAC.Remove(civCtrl);
			}
		}

		private void EngageConnectorCtrl(Cerebrum.ObjectHandle CtrlHandle)
		{
			ListViewItem liv1 = lvCommandList.SelectedItems[0];
			Cerebrum.ObjectHandle CmdHandle = new ObjectHandle(liv1.Text);
			using(Cerebrum.Data.TableView viewUC = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiCommands"))
			{
				Cerebrum.Data.ComponentItemView civC = viewUC.FindItemViewByObjectHandle(CmdHandle);
				Cerebrum.Data.VectorView vvAC = (Cerebrum.Data.VectorView)civC["AttachedControls"];
				using (Cerebrum.IComposite comp = SelectedDC.AttachConnector(CtrlHandle))
				{
					vvAC.EngageConnector(CtrlHandle, comp);
				}
			}
		}

		private void EngageConnectorCmd(Cerebrum.ObjectHandle CommandHandle, string CommandName, string MessageName)
		{
			ListViewItem liv1 = lvServiceList.SelectedItems[0];
			Cerebrum.ObjectHandle ServiceHandle = new ObjectHandle(liv1.Text);
			using(Cerebrum.Data.TableView viewUS = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiServices"))
			{
				Cerebrum.Data.ComponentItemView civS = viewUS.FindItemViewByObjectHandle(ServiceHandle);
				Cerebrum.Data.VectorView vvAC = (Cerebrum.Data.VectorView)civS["AttachedCommands"];
				using (Cerebrum.IComposite comp = SelectedDC.AttachConnector(CommandHandle))
				{
					vvAC.EngageConnector(CommandHandle, comp);
				}
			}

			ListViewItem liv = lvCommandList.Items.Add(CommandHandle.ToString());
			liv.SubItems.Add(CommandName);
			liv.SubItems.Add(MessageName);
		}

		private void FillControls()
		{
			Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor CommandDescriptor = null;
			Cerebrum.Runtime.NativeWarden AttachedControls = null;

			lvBandItems.Items.Clear();
			lvHotKeys.Items.Clear();
			lvMenuItems.Items.Clear();
			lvBandPlaces.Items.Clear();

			if(lvCommandList.SelectedItems.Count > 0)
			{
				ListViewItem livCmd = lvCommandList.SelectedItems[0];
				Cerebrum.ObjectHandle CommandHandle = new Cerebrum.ObjectHandle(livCmd.SubItems[0].Text);
				using(Cerebrum.IComposite compCmd = SelectedDC.AttachConnector(CommandHandle))
				{
					CommandDescriptor = (Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor)compCmd.Component;
					using(AttachedControls = CommandDescriptor.GetAttachedControlsVector())
					{
						foreach(System.Collections.DictionaryEntry de in AttachedControls)
						{
							Cerebrum.ObjectHandle h = (Cerebrum.ObjectHandle)de.Key;
							using(Cerebrum.IComposite comp = SelectedDC.AttachConnector(h))
							{
								if(comp.Component.GetType() == typeof(Cerebrum.Windows.Forms.Reflection.UiMenuItemDescriptor))
								{
									ListViewItem livMI = lvMenuItems.Items.Add(h.ToString());
									Cerebrum.Windows.Forms.Reflection.UiMenuItemDescriptor mid = null;
									mid = (Cerebrum.Windows.Forms.Reflection.UiMenuItemDescriptor)comp.Component;
									livMI.SubItems.Add(mid.DisplayName);
								}
								else if(comp.Component.GetType() == typeof(Cerebrum.Windows.Forms.Reflection.UiBandItemDescriptor))
								{
									ListViewItem livBI = lvBandItems.Items.Add(h.ToString());
									Cerebrum.Windows.Forms.Reflection.UiBandItemDescriptor bid = null;
									bid = (Cerebrum.Windows.Forms.Reflection.UiBandItemDescriptor)comp.Component;
									livBI.SubItems.Add(bid.DisplayName);
								}
								else if(comp.Component.GetType() == typeof(Cerebrum.Windows.Forms.Reflection.UiHotKeyDescriptor))
								{
									ListViewItem livHK = lvHotKeys.Items.Add(h.ToString());
									Cerebrum.Windows.Forms.Reflection.UiHotKeyDescriptor hkd = null;
									hkd = (Cerebrum.Windows.Forms.Reflection.UiHotKeyDescriptor)comp.Component;
									livHK.SubItems.Add(hkd.KeyCode);
								}
								else if(comp.Component.GetType() == typeof(Cerebrum.Windows.Forms.Reflection.UiBandPlaceDescriptor))
								{
									ListViewItem livBP = lvBandPlaces.Items.Add(h.ToString());
									Cerebrum.Windows.Forms.Reflection.UiBandPlaceDescriptor bpd = null;
									bpd = (Cerebrum.Windows.Forms.Reflection.UiBandPlaceDescriptor)comp.Component;
									livBP.SubItems.Add(bpd.DisplayName);
								}
							}
						}
					}
				}
			}
		}

		private void DeleteCommand(ListViewItem livCmd)
		{
			Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor CommandDescriptor = null;

			System.Collections.ArrayList alDelHandleList = new ArrayList();
			Cerebrum.ObjectHandle CommandHandle = new Cerebrum.ObjectHandle(livCmd.Text);
			using(Cerebrum.IComposite compCmd = SelectedDC.AttachConnector(CommandHandle))
			{
				CommandDescriptor = (Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor)compCmd.Component;
				using(Cerebrum.Runtime.NativeWarden nwAttachedControls = CommandDescriptor.GetAttachedControlsVector())
				{
					foreach(System.Collections.DictionaryEntry de in nwAttachedControls)
					{
						alDelHandleList.Add(de.Key);
					}
					// � ��������� ������� �� ������� AttachedControls ������� ��� ������.
					for(int i=0; i<alDelHandleList.Count; i++)
					{
						Cerebrum.ObjectHandle DelHandle = (Cerebrum.ObjectHandle)alDelHandleList[i];
						nwAttachedControls.RemoveConnector(DelHandle);
					}
				}
			}

			for(int i=0; i<alDelHandleList.Count; i++)
			{
				// �������� ����� ���������� �������� ����������.
				Cerebrum.ObjectHandle CtrlHandle = (Cerebrum.ObjectHandle)alDelHandleList[i];
				// ���� ��������� ������� ���������� ����� ������ �� ������������...
				if(IsDeleteControl(SelectedDC, CtrlHandle) == true)
				{
					using(Cerebrum.IComposite comp = SelectedDC.AttachConnector(CtrlHandle))
					{
						if(comp.Component.GetType() == typeof(UiBandItemDescriptor))
						{
							string Message = "������ '" + CtrlHandle.ToString() + "' ������ ������������ �� ������������ � ������ ��������. ������� �?";
							if(MessageBox.Show(this, Message, "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
							{
								// ������� ������ ������ ������������.
								DeleteBandItem(CtrlHandle, false);
							}
						}
						else if(comp.Component.GetType() == typeof(UiHotKeyDescriptor))
						{
							string Message = "������� ������� '" + CtrlHandle.ToString() + "' �� ������������ � ������ ��������. ������� �?";
							if(MessageBox.Show(this, Message, "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
							{
								// ������� ������� �������.
								DeleteHotKey(CtrlHandle, false);
							}
						}
						else if(comp.Component.GetType() == typeof(UiMenuItemDescriptor))
						{
							string Message = "����� ���� '" + CtrlHandle.ToString() + "' �� ������������ � ������ ��������. ������� ���?";
							if(MessageBox.Show(this, Message, "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
							{
								DeleteMenuItem(CtrlHandle, false);
							}
						}
					}
				}
			}

			// ����������� ������� �� �������.
			ListViewItem liv2 = lvServiceList.SelectedItems[0];
			Cerebrum.ObjectHandle ServiceHandle = new ObjectHandle(liv2.Text);
			using(Cerebrum.Data.TableView sv = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiServices"))
			{
				if(sv != null)
				{
					Cerebrum.Data.ComponentItemView civS = sv.FindItemViewByObjectHandle(ServiceHandle);
					Cerebrum.Data.VectorView vvAttachedCommands = (Cerebrum.Data.VectorView)civS["AttachedCommands"];
					Cerebrum.Data.ComponentItemView civCmd = vvAttachedCommands.FindItemViewByObjectHandle(CommandHandle);
					if(civCmd != null)
					{
						vvAttachedCommands.Remove(civCmd);
					}
				}
				else
				{
					throw(new Exception("�� ������� " + CommandHandle.ToString() + " ���� ����������� �������� ����������, �� �������� ������� ���� �������� ������, ��� �� ������� ������� ������� 'UiServices' ��� ������������ ������� �� ������� " + ServiceHandle.ToString()));
				}
			}

			using(Cerebrum.Data.TableView cv = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiCommands"))
			{
				if(cv != null)
				{
					Cerebrum.Data.ComponentItemView civ = cv.FindItemViewByObjectHandle(CommandHandle);
					// �������� ����� ���������, ������� ������������ � ��������� �������.
					Cerebrum.ObjectHandle MessageHandle = (Cerebrum.ObjectHandle)civ["MessageId"];
					string Message = livCmd.SubItems[2].Text;
					// ������� �������.
					DeleteItem(SelectedDC, cv, civ, CommandHandle);
					// ���� ��������� �� ������������ � ������ ��������...
					if(IsDeleteMessage(MessageHandle, CommandHandle) == true)
					{
						if(MessageBox.Show(this, "��������� '" + Message + "' �� ������ ������� �� ������������ � ������ ��������. ������� ���?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
						{
							// ������� ���������.
							DeleteItem(SelectedDC, "Messages", MessageHandle);
						}
					}
					lvCommandList.Items.Remove(livCmd);
				}
			}
		}

		private void DeleteBandItem(Cerebrum.ObjectHandle CtrlHandle, bool ListViewOnly)
		{
			if(ListViewOnly == false)
			{
				if(IsDeleteControl(SelectedDC, CtrlHandle) == true)
				{
					// ������� ������ ������ ������������.
					DeleteItem(SelectedDC, "UiBandItems", CtrlHandle);
				}
				else
				{
					MessageBox.Show(this, "����������� ������������ ������ �� �������. �������� �� ��������. ������ ������������ � ������ ��������.", "��������!!!");
				}
			}
			int BandItemCount = lvBandItems.Items.Count;
			for(int i2=0; i2<BandItemCount; i2++)
			{
				ListViewItem livBI = lvBandItems.Items[i2];
				Cerebrum.ObjectHandle BandItemHandle = new Cerebrum.ObjectHandle(livBI.Text);
				if(BandItemHandle == CtrlHandle)
				{
					lvBandItems.Items.Remove(livBI);
					break;
				}
			}
		}

		private void DeleteHotKey(Cerebrum.ObjectHandle CtrlHandle, bool ListViewOnly)
		{
			if(ListViewOnly == false)
			{
				if(IsDeleteControl(SelectedDC, CtrlHandle) == true)
				{
					// ������� ������� �������.
					DeleteItem(SelectedDC, "UiHotKeys", CtrlHandle);
				}
				else
				{
					MessageBox.Show(this, "����������� ������������ ������� ������� �� �������. �������� �� ��������. ������� ������� ������������ � ������ ��������.", "��������!!!");
				}
			}
			int HotKeyCount = lvHotKeys.Items.Count;
			for(int i2=0; i2<HotKeyCount; i2++)
			{
				ListViewItem livHK = lvHotKeys.Items[i2];
				Cerebrum.ObjectHandle HotKeysHandle = new Cerebrum.ObjectHandle(livHK.Text);
				if(HotKeysHandle == CtrlHandle)
				{
					lvHotKeys.Items.Remove(livHK);
					break;
				}
			}
		}

		private void DeleteMenuItem(Cerebrum.ObjectHandle CtrlHandle, bool TreeViewOnly)
		{
			if(TreeViewOnly == false)
			{
				if(IsDeleteControl(SelectedDC, CtrlHandle) == true)
				{
					// ������� ����� ����.
					DeleteItem(SelectedDC, "UiMenuItems", CtrlHandle);
				}
				else
				{
					MessageBox.Show(this, "����������� ������������ ������ ���� �� �������. �������� �� ��������. ���� ����� ���� ������������ � ������ ��������.", "��������!!!");
				}
			}
			for(int i2=0; i2<lvMenuItems.Items.Count; i2++)
			{
				ListViewItem liv = lvMenuItems.Items[i2];
				Cerebrum.ObjectHandle HotKeysHandle = new Cerebrum.ObjectHandle(liv.Text);
				if(HotKeysHandle == CtrlHandle)
				{
					lvMenuItems.Items.Remove(liv);
					break;
				}
			}
		}

		public static void DeleteItem(Cerebrum.Integrator.DomainContext DC, string TableName, Cerebrum.ObjectHandle ItemHandle)
		{
			using(Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(DC, TableName))
			{
				Cerebrum.Data.ComponentItemView civ = view.FindItemViewByObjectHandle(ItemHandle);
				DeleteItem(DC, view, civ, ItemHandle);
			}
		}

		public static void DeleteItem(Cerebrum.Integrator.DomainContext DC, Cerebrum.Data.TableView view, Cerebrum.ObjectHandle ItemHandle)
		{
			Cerebrum.Data.ComponentItemView civ = view.FindItemViewByObjectHandle(ItemHandle);
			DeleteItem(DC, view, civ, ItemHandle);
		}

		public static void DeleteItem(Cerebrum.Integrator.DomainContext DC, Cerebrum.Data.TableView view, Cerebrum.Data.ComponentItemView civ, Cerebrum.ObjectHandle ItemHandle)
		{
			view.Remove(civ);
			using (Cerebrum.Runtime.NativeSector sector = DC.GetSector())
			{
				sector.RemoveConnector(ItemHandle);
			}
		}

		public static bool IsDeleteControl(Cerebrum.Integrator.DomainContext DC, Cerebrum.ObjectHandle ControlHandle)
		{
			bool res = true;
			using(Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(DC, "UiCommands"))
			{
				for(int i=0; i<view.Count; i++)
				{
					Cerebrum.Data.ComponentItemView civ = view[i];
					Cerebrum.Data.VectorView vv = (Cerebrum.Data.VectorView)civ["AttachedControls"];
					if(vv.ContainsObject(ControlHandle) == true)
					{
						res = false;
						break;
					}
				}
			}
			return res;
		}

		private bool IsDeleteMessage(Cerebrum.ObjectHandle MessageHandle, Cerebrum.ObjectHandle PassCmdHandle)
		{
			bool res = true;
			using(Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiCommands"))
			{
				for(int i=0; i<view.Count; i++)
				{
					Cerebrum.Data.ComponentItemView civ = view[i];
					Cerebrum.ObjectHandle MH = (Cerebrum.ObjectHandle)civ["MessageId"];
					if((MessageHandle == MH)&&(civ.ObjectHandle != PassCmdHandle))
					{
						res = false;
						break;
					}
				}
			}
			return res;
		}

		private void btnMenuItemAdd_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvCommandList.SelectedItems.Count > 0)
				{
					frmMenuTree frm = new frmMenuTree(SelectedDC);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						if(frm.MenuItemHandle != Cerebrum.ObjectHandle.Null)
						{
							EngageConnectorCtrl(frm.MenuItemHandle);
							FillControls();
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnMenuItemDisconnect_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvMenuItems.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "��������� ����� ����?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem livCmd = lvCommandList.SelectedItems[0];
						Cerebrum.ObjectHandle CommandHandle = new ObjectHandle(livCmd.Text);
						ListViewItem livMI = lvMenuItems.SelectedItems[0];
						Cerebrum.ObjectHandle MenuItemHandle = new ObjectHandle(livMI.Text);
						// ��������� ����� ���� �� �������.
						DisableControlFromCommand(SelectedDC, MenuItemHandle, CommandHandle);
						// ������� ����� ���� �� ������ tvMenuItems.
						DeleteMenuItem(MenuItemHandle, true);
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnMenuItemDel_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvMenuItems.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "������� ����� ����?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem livCmd = lvCommandList.SelectedItems[0];
						Cerebrum.ObjectHandle CommandHandle = new ObjectHandle(livCmd.Text);
						ListViewItem livMI = lvMenuItems.SelectedItems[0];
						Cerebrum.ObjectHandle MenuItemHandle = new ObjectHandle(livMI.Text);
						// ��������� ������� �������� ������� ����.
						bool IsDel = frmMenuItem.IsChildrenMenuItem(SelectedDC, MenuItemHandle);
						if(IsDel == true)
						{
							// ��������� ����� ���� �� �������.
							DisableControlFromCommand(SelectedDC, MenuItemHandle, CommandHandle);
							// ������� ����� ����.
							DeleteMenuItem(MenuItemHandle, false);
							FillControls();
						}
						else
						{
							if(MessageBox.Show(this, "� ����� ������ ���� ���� �������� ������ ����. �������� ����������. ������� ��������� ����� ���� �� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
							{
								// ��������� ����� ���� �� �������.
								DisableControlFromCommand(SelectedDC, MenuItemHandle, CommandHandle);
								FillControls();
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnServiceNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				frmServiceItem frm = new frmServiceItem(SelectedDC, Cerebrum.ObjectHandle.Null, false);
				if(frm.ShowDialog() == DialogResult.OK)
				{
					FillServiceList();
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnServiceEdit_Click(object sender, System.EventArgs e)
		{
			ViewServiceEditForm();
		}

		private void btnServiceDel_Click(object sender, System.EventArgs e)
		{
			System.Collections.ArrayList alCmdForDel = new ArrayList();

			try
			{
				if(lvServiceList.SelectedItems.Count > 0)
				{
					if(MessageBox.Show(this, "������� ������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem liv = lvServiceList.SelectedItems[0];
						Cerebrum.ObjectHandle ServiceHandle = new ObjectHandle(liv.Text);
						using(Cerebrum.Data.TableView viewUS = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiServices"))
						{
							// �������� ��������� ������.
							Cerebrum.Data.ComponentItemView civS1 = viewUS.FindItemViewByObjectHandle(ServiceHandle);
							// ��������� ������ ������, ������������ � ������� �������.
							Cerebrum.Data.VectorView vvAC1 = (Cerebrum.Data.VectorView)civS1["AttachedCommands"];
							// �������� �� ���� �������� � ��������� ����������� ������ ���������� ������� 
							// � ������ ��������.
							for(int i1=0; i1<viewUS.Count; i1++)
							{
								Cerebrum.Data.VectorView vvAC2 = (Cerebrum.Data.VectorView)viewUS[i1]["AttachedCommands"];
								for(int i2=0; i2<vvAC1.Count; i2++)
								{
									Cerebrum.ObjectHandle CmdHandle = vvAC1[i2].ObjectHandle;
									if(vvAC2.ContainsObject(CmdHandle) == false)
									{
										if(alCmdForDel.Contains(CmdHandle) == false)
										{
											//��������� ����� ������� � ������ �� ��������.
											alCmdForDel.Add(CmdHandle);
										}
									}
								}
							}
							// ��������� ��� ������� �� ���������� �������.
							vvAC1.Clear();
							// ������� ��� �������, ������� ����� ������ �� ������������.
							int CmdCount = lvCommandList.Items.Count;
							for(int i=0; i<CmdCount; i++)
							{
								ListViewItem livCmd = lvCommandList.Items[0];
								Cerebrum.ObjectHandle CmdHandle = new Cerebrum.ObjectHandle(livCmd.Text);
								if(alCmdForDel.Contains(CmdHandle) == true)
								{
									if(MessageBox.Show(this, "������� '" + CmdHandle.ToString() + "' ����� �� ������������. ������� �?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
									{
										DeleteCommand(livCmd);
									}
								}
							}
							// ������� ������.
							DeleteItem(SelectedDC, viewUS, civS1, ServiceHandle);
							lvServiceList.Items.Remove(liv);
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnCmdAdd_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvServiceList.SelectedItems.Count > 0)
				{
					if(MessageBox.Show(this, "������� ����� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						frmCmdItem frm = new frmCmdItem(SelectedDC, Cerebrum.ObjectHandle.Null, false);
						if(frm.ShowDialog() == DialogResult.OK)
						{
							if(frm.CommandHandle != Cerebrum.ObjectHandle.Null)
							{
								//if(MessageBox.Show(this, "����� ������� �������. ���������� � � �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
								//{
								EngageConnectorCmd(frm.CommandHandle, frm.CommandName, frm.MessageName);
								//}
							}
						}
					}
					else
					{
						frmCmdList frm = new frmCmdList(SelectedDC);
						if(frm.ShowDialog() == DialogResult.OK)
						{
							if(frm.CommandHandle != Cerebrum.ObjectHandle.Null)
							{
								EngageConnectorCmd(frm.CommandHandle, frm.CommandName, frm.MessageName);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnCmdEdit_Click(object sender, System.EventArgs e)
		{
			ViewCmdEditForm();
		}

		private void btnCmdDisconnect_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "��������� ������� �� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem liv1 = lvServiceList.SelectedItems[0];
						Cerebrum.ObjectHandle ServiceHandle = new ObjectHandle(liv1.Text);
						ListViewItem liv2 = lvCommandList.SelectedItems[0];
						Cerebrum.ObjectHandle CmdHandle = new Cerebrum.ObjectHandle(liv2.Text);
						using(Cerebrum.Data.TableView viewUS = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiServices"))
						{
							Cerebrum.Data.ComponentItemView civS = viewUS.FindItemViewByObjectHandle(ServiceHandle);
							Cerebrum.Data.VectorView vvAC = (Cerebrum.Data.VectorView)civS["AttachedCommands"];
							Cerebrum.Data.ComponentItemView civCmd = vvAC.FindItemViewByObjectHandle(CmdHandle);
							vvAC.Remove(civCmd);
						}
						lvCommandList.Items.Remove(liv2);
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnCmdDel_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "������� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem livCmd = lvCommandList.SelectedItems[0];
						DeleteCommand(livCmd);
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnBandItemAdd_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvCommandList.SelectedItems.Count > 0)
				{
					if(MessageBox.Show(this, "������� ����� ������ �� ������ ������������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						frmBandItem frm = new frmBandItem(SelectedDC, Cerebrum.ObjectHandle.Null, false);
						if(frm.ShowDialog() == DialogResult.OK)
						{
							if(frm.ControlHandle != Cerebrum.ObjectHandle.Null)
							{
								EngageConnectorCtrl(frm.ControlHandle);
								FillControls();
							}
						}
					}
					else
					{
						frmBandItemList frm = new frmBandItemList(SelectedDC);
						if(frm.ShowDialog() == DialogResult.OK)
						{
							if(frm.BandItemHandle != Cerebrum.ObjectHandle.Null)
							{
								EngageConnectorCtrl(frm.BandItemHandle);
								FillControls();
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnBandItemEdit_Click(object sender, System.EventArgs e)
		{
			ViewBandItemEditForm();
		}

		private void btnBandItemDisconnect_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvBandItems.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "��������� ������ �� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem livCmd = lvCommandList.SelectedItems[0];
						Cerebrum.ObjectHandle CommandHandle = new ObjectHandle(livCmd.Text);
						ListViewItem liv = lvBandItems.SelectedItems[0];
						Cerebrum.ObjectHandle BandItemHandle = new ObjectHandle(liv.Text);
						// ��������� ������ ������ ������������ �� �������.
						DisableControlFromCommand(SelectedDC, BandItemHandle, CommandHandle);
						// ������� ������ ������ ������������ ������ � ListView.
						DeleteBandItem(BandItemHandle, true);
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnBandItemDel_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvBandItems.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "������� ������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem livCmd = lvCommandList.SelectedItems[0];
						Cerebrum.ObjectHandle CommandHandle = new ObjectHandle(livCmd.Text);
						ListViewItem liv = lvBandItems.SelectedItems[0];
						Cerebrum.ObjectHandle BandItemHandle = new ObjectHandle(liv.Text);
						// ��������� ������ ������ ������������ �� �������.
						DisableControlFromCommand(SelectedDC, BandItemHandle, CommandHandle);
						// ������� ������ ������ ������������.
						DeleteBandItem(BandItemHandle, false);
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnHotKeyAdd_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvCommandList.SelectedItems.Count > 0)
				{
					if(MessageBox.Show(this, "������� ����� ������� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						frmHotKeyItem frm = new frmHotKeyItem(SelectedDC, Cerebrum.ObjectHandle.Null, false);
						if(frm.ShowDialog() == DialogResult.OK)
						{
							if(frm.ControlHandle != Cerebrum.ObjectHandle.Null)
							{
								EngageConnectorCtrl(frm.ControlHandle);
								FillControls();
							}
						}
					}
					else
					{
						frmHotKeyList frm = new frmHotKeyList(SelectedDC);
						if(frm.ShowDialog() == DialogResult.OK)
						{
							if(frm.HotKeyHandle != Cerebrum.ObjectHandle.Null)
							{
								EngageConnectorCtrl(frm.HotKeyHandle);
								FillControls();
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnHotKeyEdit_Click(object sender, System.EventArgs e)
		{
			ViewHotKeyEditForm();
		}

		private void btnHotKeyDisconnect_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvHotKeys.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "��������� ������ �� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem livCmd = lvCommandList.SelectedItems[0];
						Cerebrum.ObjectHandle CommandHandle = new ObjectHandle(livCmd.Text);
						ListViewItem liv = lvHotKeys.SelectedItems[0];
						Cerebrum.ObjectHandle HotKeyHandle = new ObjectHandle(liv.Text);
						// ��������� ������ ������ ������������ �� �������.
						DisableControlFromCommand(SelectedDC, HotKeyHandle, CommandHandle);
						// ������� ������ ������ ������������ ������ � ListView.
						DeleteHotKey(HotKeyHandle, true);
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnHotKeyDel_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvHotKeys.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "������� ������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem livCmd = lvCommandList.SelectedItems[0];
						Cerebrum.ObjectHandle CommandHandle = new ObjectHandle(livCmd.Text);
						ListViewItem liv = lvHotKeys.SelectedItems[0];
						Cerebrum.ObjectHandle HotKeyHandle = new ObjectHandle(liv.Text);
						// ��������� ������ ������ ������������ �� �������.
						DisableControlFromCommand(SelectedDC, HotKeyHandle, CommandHandle);
						// ������� ������ ������ ������������.
						DeleteHotKey(HotKeyHandle, false);
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void lvServiceList_DoubleClick(object sender, System.EventArgs e)
		{
			ViewServiceEditForm();
		}

		private void ViewServiceEditForm()
		{
			try
			{
				if(lvServiceList.SelectedItems.Count > 0)
				{
					ListViewItem liv = lvServiceList.SelectedItems[0];
					Cerebrum.ObjectHandle ServiceHandle = new ObjectHandle(liv.Text);
					frmServiceItem frm = new frmServiceItem(SelectedDC, ServiceHandle, true);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						liv.SubItems[1].Text = frm.ServiceName;
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void ViewCmdEditForm()
		{
			try
			{
				if((lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					ListViewItem liv = lvCommandList.SelectedItems[0];
					Cerebrum.ObjectHandle CmdHandle = new ObjectHandle(liv.Text);
					frmCmdItem frm = new frmCmdItem(SelectedDC, CmdHandle, true);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						liv.SubItems[1].Text = frm.CommandName;
						liv.SubItems[2].Text = frm.MessageName;
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void ViewBandItemEditForm()
		{
			try
			{
				if((lvBandItems.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					ListViewItem liv = lvBandItems.SelectedItems[0];
					Cerebrum.ObjectHandle CtrlHandle = new ObjectHandle(liv.Text);
					frmBandItem frm = new frmBandItem(SelectedDC, CtrlHandle, true);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void ViewHotKeyEditForm()
		{
			try
			{
				if((lvHotKeys.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					ListViewItem liv = lvHotKeys.SelectedItems[0];
					Cerebrum.ObjectHandle HotKeyHandle = new ObjectHandle(liv.Text);
					frmHotKeyItem frm = new frmHotKeyItem(SelectedDC, HotKeyHandle, true);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void lvCommandList_DoubleClick(object sender, System.EventArgs e)
		{
			ViewCmdEditForm();
		}

		private void lvBandItems_DoubleClick(object sender, System.EventArgs e)
		{
			ViewBandItemEditForm();
		}

		private void lvHotKeys_DoubleClick(object sender, System.EventArgs e)
		{
			ViewHotKeyEditForm();
		}

		private void ViewPandPlaceEditForm()
		{
			try
			{
				if((lvBandPlaces.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					ListViewItem liv = lvBandPlaces.SelectedItems[0];
					Cerebrum.ObjectHandle CtrlHandle = new ObjectHandle(liv.Text);
					frmBandPlace frm = new frmBandPlace(SelectedDC, CtrlHandle, true);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void lvBandPlaces_DoubleClick(object sender, System.EventArgs e)
		{
			ViewPandPlaceEditForm();
		}

		private void btnBandPlaceAdd_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lvCommandList.SelectedItems.Count > 0)
				{
					if(MessageBox.Show(this, "������� ����� ������ �� ������ ������������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						frmBandPlace frm = new frmBandPlace(SelectedDC, Cerebrum.ObjectHandle.Null, false);
						if(frm.ShowDialog() == DialogResult.OK)
						{
							if(frm.ControlHandle != Cerebrum.ObjectHandle.Null)
							{
								EngageConnectorCtrl(frm.ControlHandle);
								FillControls();
							}
						}
					}
					else
					{
						frmBandPlaceList frm = new frmBandPlaceList(SelectedDC);
						if(frm.ShowDialog() == DialogResult.OK)
						{
							if(frm.BandPlaceHandle != Cerebrum.ObjectHandle.Null)
							{
								EngageConnectorCtrl(frm.BandPlaceHandle);
								FillControls();
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnBandPlaceEdit_Click(object sender, System.EventArgs e)
		{
			ViewPandPlaceEditForm();
		}

		private void btnBandPlaceDisconnect_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvBandPlaces.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "��������� ������ �� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem livCmd = lvCommandList.SelectedItems[0];
						Cerebrum.ObjectHandle CommandHandle = new ObjectHandle(livCmd.Text);
						ListViewItem liv = lvBandPlaces.SelectedItems[0];
						Cerebrum.ObjectHandle BPHandle = new ObjectHandle(liv.Text);
						// ��������� ������ ������ ������������ �� �������.
						DisableControlFromCommand(SelectedDC, BPHandle, CommandHandle);
						// ������� ������ ������ ������������ ������ � ListView.
						DeleteBandPlace(BPHandle, true);
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnBandPlaceDel_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((lvBandPlaces.SelectedItems.Count > 0)&&(lvCommandList.SelectedItems.Count > 0)&&(lvServiceList.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "������� ������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						ListViewItem livCmd = lvCommandList.SelectedItems[0];
						Cerebrum.ObjectHandle CommandHandle = new ObjectHandle(livCmd.Text);
						ListViewItem liv = lvBandPlaces.SelectedItems[0];
						Cerebrum.ObjectHandle BPHandle = new ObjectHandle(liv.Text);
						// ��������� ������ ������ ������������ �� �������.
						DisableControlFromCommand(SelectedDC, BPHandle, CommandHandle);
						// ������� ������ ������ ������������.
						DeleteBandItem(BPHandle, false);
						FillControls();
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void DeleteBandPlace(Cerebrum.ObjectHandle CtrlHandle, bool ListViewOnly)
		{
			if(ListViewOnly == false)
			{
				if(IsDeleteControl(SelectedDC, CtrlHandle) == true)
				{
					// ������� ������ ������ ������������.
					DeleteItem(SelectedDC, "UiBandPlaces", CtrlHandle);
				}
				else
				{
					MessageBox.Show(this, "����������� ������������ ������ �� �������. �������� �� ��������. ������ ������������ � ������ ��������.", "��������!!!");
				}
			}
			int BPCount = lvBandPlaces.Items.Count;
			for(int i2=0; i2<BPCount; i2++)
			{
				ListViewItem livBP = lvBandPlaces.Items[i2];
				Cerebrum.ObjectHandle BPHandle = new Cerebrum.ObjectHandle(livBP.Text);
				if(BPHandle == CtrlHandle)
				{
					lvBandPlaces.Items.Remove(livBP);
					break;
				}
			}
		}
	}
}
