// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;

namespace Cerebrum.Data.Manager.Design
{
	/// <summary>
	/// Summary description for ServiceTypeInfo.
	/// </summary>
	public class TypeInfo
	{
		public Cerebrum.ObjectHandle TypeHandle;
		public string TypeName;
		public string Description;
		public string QualifiedTypeName;
		public string DisplayName;
		public Cerebrum.ObjectHandle KernelObjectType;

		public TypeInfo()
		{
			TypeHandle = Cerebrum.ObjectHandle.Null;
			TypeName = "";
			Description = "";
			QualifiedTypeName = "";
			DisplayName = "";
			KernelObjectType = Cerebrum.ObjectHandle.Null;
		}

		public override string ToString()
		{
			return QualifiedTypeName;
		}
	}
}
