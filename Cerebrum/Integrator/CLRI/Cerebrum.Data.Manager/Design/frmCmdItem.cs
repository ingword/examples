// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Cerebrum.Data.Manager.Design;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmCmdItem.
	/// </summary>
	public class frmCmdItem : System.Windows.Forms.Form
	{
		private object NLIBD;
		private object NEIBD;
		private Cerebrum.Integrator.DomainContext DC;
		private Cerebrum.ObjectHandle CmdHandle;
		private bool IsFD;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbOH;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox cbMessage;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.TextBox tbOrdinal;
		private System.Windows.Forms.TextBox tbNLISN;
		private System.Windows.Forms.TextBox tbNEISN;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmCmdItem(Cerebrum.Integrator.DomainContext DomainContext, Cerebrum.ObjectHandle CommandHandle, bool IsFillData)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			DC = DomainContext;
			CmdHandle = CommandHandle;
			IsFD = IsFillData;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tbOH = new System.Windows.Forms.TextBox();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.cbMessage = new System.Windows.Forms.ComboBox();
			this.tbOrdinal = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbNLISN = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.tbNEISN = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(176, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Object Handle:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbOH
			// 
			this.tbOH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbOH.Location = new System.Drawing.Point(192, 8);
			this.tbOH.Name = "tbOH";
			this.tbOH.ReadOnly = true;
			this.tbOH.Size = new System.Drawing.Size(112, 20);
			this.tbOH.TabIndex = 10;
			this.tbOH.Text = "";
			// 
			// tbName
			// 
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(192, 32);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(256, 20);
			this.tbName.TabIndex = 1;
			this.tbName.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(176, 20);
			this.label2.TabIndex = 2;
			this.label2.Text = "Name:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 56);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(176, 20);
			this.label3.TabIndex = 4;
			this.label3.Text = "Message:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbMessage
			// 
			this.cbMessage.BackColor = System.Drawing.Color.Honeydew;
			this.cbMessage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbMessage.Location = new System.Drawing.Point(192, 56);
			this.cbMessage.Name = "cbMessage";
			this.cbMessage.Size = new System.Drawing.Size(256, 21);
			this.cbMessage.TabIndex = 2;
			// 
			// tbOrdinal
			// 
			this.tbOrdinal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbOrdinal.Location = new System.Drawing.Point(192, 80);
			this.tbOrdinal.Name = "tbOrdinal";
			this.tbOrdinal.Size = new System.Drawing.Size(112, 20);
			this.tbOrdinal.TabIndex = 3;
			this.tbOrdinal.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(176, 20);
			this.label4.TabIndex = 6;
			this.label4.Text = "Ordinal:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbNLISN
			// 
			this.tbNLISN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbNLISN.Location = new System.Drawing.Point(192, 104);
			this.tbNLISN.Name = "tbNLISN";
			this.tbNLISN.Size = new System.Drawing.Size(256, 20);
			this.tbNLISN.TabIndex = 4;
			this.tbNLISN.Text = "";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 104);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(176, 20);
			this.label5.TabIndex = 8;
			this.label5.Text = "NormalLeaveImageStreamName:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbNEISN
			// 
			this.tbNEISN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbNEISN.Location = new System.Drawing.Point(192, 128);
			this.tbNEISN.Name = "tbNEISN";
			this.tbNEISN.Size = new System.Drawing.Size(256, 20);
			this.tbNEISN.TabIndex = 5;
			this.tbNEISN.Text = "";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 128);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(176, 20);
			this.label6.TabIndex = 10;
			this.label6.Text = "NormalEnterImageStreamName:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Turquoise;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Location = new System.Drawing.Point(8, 152);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(216, 23);
			this.button1.TabIndex = 6;
			this.button1.Text = "NormalLeaveImageBinaryData";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.Turquoise;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Location = new System.Drawing.Point(232, 152);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(216, 23);
			this.button2.TabIndex = 7;
			this.button2.Text = "NormalEnterImageBinaryData";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.tbOH);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.tbNEISN);
			this.panel1.Controls.Add(this.tbName);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.cbMessage);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.tbNLISN);
			this.panel1.Controls.Add(this.tbOrdinal);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(458, 184);
			this.panel1.TabIndex = 14;
			// 
			// btnOK
			// 
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(296, 192);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 8;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(376, 192);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "&Cancel";
			// 
			// frmCmdItem
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(458, 223);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmCmdItem";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Command info";
			this.Load += new System.EventHandler(this.frmCmdItem_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmCmdItem_Load(object sender, System.EventArgs e)
		{
			MessageInfo miSelected = null;
			Cerebrum.ObjectHandle MessageHandle = Cerebrum.ObjectHandle.Null;

			try
			{
				if(IsFD == true)
				{
					using(Cerebrum.Data.TableView tvUC = Cerebrum.Management.Utilites.GetView(DC, "UiCommands"))
					{
						Cerebrum.Data.ComponentItemView civ = tvUC.FindItemViewByObjectHandle(CmdHandle);
						tbOH.Text = civ.ObjectHandle.ToString();
						tbName.Text = civ["Name"].ToString();
						tbOrdinal.Text = civ["Ordinal"].ToString();
						tbNLISN.Text = civ["NormalLeaveImageStreamName"].ToString();
						tbNEISN.Text = civ["NormalEnterImageStreamName"].ToString();
						NLIBD = civ["NormalLeaveImageBinaryData"];
						NEIBD = civ["NormalEnterImageBinaryData"];
						MessageHandle = (Cerebrum.ObjectHandle)civ["MessageId"];
					}
				}
				using(Cerebrum.Data.TableView tvM = Cerebrum.Management.Utilites.GetView(DC, "Messages"))
				{
					for(int i=0; i<tvM.Count; i++)
					{
						Cerebrum.Data.ComponentItemView civ = tvM[i];
						MessageInfo mi = new MessageInfo();
						mi.MessageHandle = civ.ObjectHandle;
						mi.MessageName = civ["Name"].ToString();
						mi.DisplayName = civ["DisplayName"].ToString();
						mi.Description = civ["Description"].ToString();
						if(MessageHandle == mi.MessageHandle)
						{
							miSelected = mi;
						}
						cbMessage.Items.Add(mi);
					}
				}
				if(IsFD == true)
				{
					if(miSelected != null)
					{
						cbMessage.SelectedItem = miSelected;
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Data.Manager.Forms.frmObjectBrowser frm = new Cerebrum.Data.Manager.Forms.frmObjectBrowser(true);
			if(NLIBD != null)
			{
				frm.Value = NLIBD;
			}
			if(frm.ShowDialog() == DialogResult.OK)
			{
				NLIBD = frm.Value;
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Data.Manager.Forms.frmObjectBrowser frm = new Cerebrum.Data.Manager.Forms.frmObjectBrowser(true);
			if(NEIBD != null)
			{
				frm.Value = NEIBD;
			}
			if(frm.ShowDialog() == DialogResult.OK)
			{
				NEIBD = frm.Value;
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Data.ComponentItemView civ = null;

			try
			{
				using(Cerebrum.Data.TableView tvUC = Cerebrum.Management.Utilites.GetView(DC, "UiCommands"))
				{
					if(IsFD == true)
					{
						civ = tvUC.FindItemViewByObjectHandle(CmdHandle);
					}
					else
					{
						civ = tvUC.AddNew();
						CmdHandle = civ.ObjectHandle;
					}
					civ["Name"] = tbName.Text;
					if(tbOrdinal.Text != "")
					{
						civ["Ordinal"] = Convert.ToInt32(tbOrdinal.Text);
					}
					civ["NormalLeaveImageStreamName"] = tbNLISN.Text;
					civ["NormalEnterImageStreamName"] = tbNEISN.Text;
					if(NLIBD != null)
					{
						civ["NormalLeaveImageBinaryData"] = NLIBD;
					}
					if(NEIBD != null)
					{
						civ["NormalEnterImageBinaryData"] = NEIBD;
					}
					MessageInfo mi = (MessageInfo)cbMessage.SelectedItem;
					if(mi != null)
					{
						civ["MessageId"] = mi.MessageHandle;
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		public string CommandName
		{
			get
			{
				return tbName.Text;
			}
		}

		public string MessageName
		{
			get
			{
				MessageInfo mi = (MessageInfo)cbMessage.SelectedItem;
				return mi.MessageName;
			}
		}

		public Cerebrum.ObjectHandle MessageHandle
		{
			get
			{
				MessageInfo mi = (MessageInfo)cbMessage.SelectedItem;
				return mi.MessageHandle;
			}
		}

		public Cerebrum.ObjectHandle CommandHandle
		{
			get
			{
				return CmdHandle;
			}
		}

		public int Ordinal
		{
			get
			{
				int ordinal = 0;
				if(tbOrdinal.Text != "")
				{
					ordinal = Convert.ToInt32(tbOrdinal.Text);
				}
				return ordinal;
			}
		}

		public string NormalLeaveImageStreamName
		{
			get
			{
				return tbNLISN.Text;
			}
		}

		public string NormalEnterImageStreamName
		{
			get
			{
				return tbNEISN.Text;
			}
		}
	}
}
