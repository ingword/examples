// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Cerebrum.Data.Manager.Design;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmMenuTree.
	/// </summary>
	public class frmMenuTree : System.Windows.Forms.Form
	{
		private System.Collections.ArrayList alMenuItem;
		private Cerebrum.ObjectHandle CtrlHandle;
		private string CtrlName;
		private Cerebrum.Integrator.DomainContext SelectedDC;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnDel;
		private System.Windows.Forms.Button btnEdit;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.TreeView tvMenu;
		private System.Windows.Forms.ListView lvCommands;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.Button btnDisconnect;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmMenuTree(Cerebrum.Integrator.DomainContext DC)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			SelectedDC = DC;
			CtrlHandle = Cerebrum.ObjectHandle.Null;
			CtrlName = "";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.btnDisconnect = new System.Windows.Forms.Button();
			this.lvCommands = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.btnDel = new System.Windows.Forms.Button();
			this.btnEdit = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.tvMenu = new System.Windows.Forms.TreeView();
			this.label2 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.btnDisconnect);
			this.panel1.Controls.Add(this.lvCommands);
			this.panel1.Controls.Add(this.btnDel);
			this.panel1.Controls.Add(this.btnEdit);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Controls.Add(this.tvMenu);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(602, 408);
			this.panel1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(296, 16);
			this.label1.TabIndex = 10;
			this.label1.Text = "������ ����";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnDisconnect
			// 
			this.btnDisconnect.BackColor = System.Drawing.Color.Turquoise;
			this.btnDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDisconnect.Location = new System.Drawing.Point(520, 8);
			this.btnDisconnect.Name = "btnDisconnect";
			this.btnDisconnect.Size = new System.Drawing.Size(72, 23);
			this.btnDisconnect.TabIndex = 6;
			this.btnDisconnect.Text = "Disconnect";
			this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
			// 
			// lvCommands
			// 
			this.lvCommands.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						 this.columnHeader1,
																						 this.columnHeader2});
			this.lvCommands.FullRowSelect = true;
			this.lvCommands.GridLines = true;
			this.lvCommands.HideSelection = false;
			this.lvCommands.Location = new System.Drawing.Point(312, 48);
			this.lvCommands.Name = "lvCommands";
			this.lvCommands.Size = new System.Drawing.Size(280, 352);
			this.lvCommands.TabIndex = 8;
			this.lvCommands.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "ObjectHandle";
			this.columnHeader1.Width = 85;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Name";
			this.columnHeader2.Width = 148;
			// 
			// btnDel
			// 
			this.btnDel.BackColor = System.Drawing.Color.Turquoise;
			this.btnDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDel.Location = new System.Drawing.Point(264, 8);
			this.btnDel.Name = "btnDel";
			this.btnDel.Size = new System.Drawing.Size(40, 23);
			this.btnDel.TabIndex = 5;
			this.btnDel.Text = "Del";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// btnEdit
			// 
			this.btnEdit.BackColor = System.Drawing.Color.Turquoise;
			this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.Location = new System.Drawing.Point(224, 8);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(40, 23);
			this.btnEdit.TabIndex = 4;
			this.btnEdit.Text = "Edit";
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// btnNew
			// 
			this.btnNew.BackColor = System.Drawing.Color.Turquoise;
			this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnNew.Location = new System.Drawing.Point(184, 8);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(40, 23);
			this.btnNew.TabIndex = 3;
			this.btnNew.Text = "New";
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// tvMenu
			// 
			this.tvMenu.HideSelection = false;
			this.tvMenu.ImageIndex = -1;
			this.tvMenu.Location = new System.Drawing.Point(8, 48);
			this.tvMenu.Name = "tvMenu";
			this.tvMenu.SelectedImageIndex = -1;
			this.tvMenu.Size = new System.Drawing.Size(296, 352);
			this.tvMenu.TabIndex = 7;
			this.tvMenu.DoubleClick += new System.EventHandler(this.tvMenu_DoubleClick);
			this.tvMenu.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvMenu_AfterSelect);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(312, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(280, 16);
			this.label2.TabIndex = 11;
			this.label2.Text = "������� � ������� ��������� ����� ����";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(520, 416);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "&Cancel";
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(440, 416);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// frmMenuTree
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(602, 445);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmMenuTree";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Menu tree";
			this.Load += new System.EventHandler(this.frmMenuTree_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmMenuTree_Load(object sender, System.EventArgs e)
		{
			try
			{
				alMenuItem = new ArrayList();
				using(Cerebrum.Data.TableView tvMI = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiMenuItems"))
				{
					// ��������� � ������ ���� �������� ������ 'File'.
					MenuTreeNode mtn = new MenuTreeNode();
					Cerebrum.ObjectHandle CurH = Cerebrum.Windows.Forms.Specialized.Concepts.SystemMenuItemId;
					mtn.MenuCurrentHandle = CurH;
					mtn.MenuName = "File";
					mtn.MenuDisplayName = "File";
					mtn.MenuParentHandle = Cerebrum.ObjectHandle.Null;
					mtn.MenuMergeOrder = 0;
					mtn.Text = "(" + CurH.ToString() + ") " + mtn.MenuName;
					alMenuItem.Add(mtn);

					// ��������� � ������ �������� ���� �� ���� ������������� ��� ������� ����������.
					for(int i=0; i<tvMI.Count; i++)
					{
						mtn = new MenuTreeNode();
						Cerebrum.Data.ComponentItemView civ = tvMI[i];
						mtn.MenuCurrentHandle = civ.ObjectHandle;
						mtn.MenuName = civ[Cerebrum.Specialized.KnownNames.Name].ToString();
						mtn.MenuDisplayName = civ[Cerebrum.Specialized.KnownNames.DisplayName].ToString();
						mtn.MenuParentHandle = (Cerebrum.ObjectHandle)civ["ParentId"];
						if(civ["MergeOrder"] != null)
						{
							mtn.MenuMergeOrder = Convert.ToInt32(civ["MergeOrder"]);
						}
						else
						{
							mtn.MenuMergeOrder = 0;
						}
						mtn.Text = "(" + civ.ObjectHandle.ToString() + ") " + mtn.MenuName;
						alMenuItem.Add(mtn);
					}

					// ��������� � ������ ���� �������� ������ 'Security'.
					mtn = new MenuTreeNode();
					CurH = Cerebrum.Windows.Forms.Specialized.Concepts.SecurityMenuItemId;
					mtn.MenuCurrentHandle = CurH;
					mtn.MenuName = "Security";
					mtn.MenuDisplayName = "Security";
					mtn.MenuParentHandle = Cerebrum.ObjectHandle.Null;
					mtn.MenuMergeOrder = 0;
					mtn.Text = "(" + CurH.ToString() + ") " + mtn.MenuName;
					alMenuItem.Add(mtn);

					// ��������� � ������ ���� �������� ������ 'Window'.
					mtn = new MenuTreeNode();
					CurH = Cerebrum.Windows.Forms.Specialized.Concepts.WindowMenuItemId;
					mtn.MenuCurrentHandle = CurH;
					mtn.MenuName = "Window";
					mtn.MenuDisplayName = "Window";
					mtn.MenuParentHandle = Cerebrum.ObjectHandle.Null;
					mtn.MenuMergeOrder = 0;
					mtn.Text = "(" + CurH.ToString() + ") " + mtn.MenuName;
					alMenuItem.Add(mtn);

					// ��������� � ������ ���� �������� ������ 'Help'.
					mtn = new MenuTreeNode();
					CurH = Cerebrum.Windows.Forms.Specialized.Concepts.HelpMenuItemId;
					mtn.MenuCurrentHandle = CurH;
					mtn.MenuName = "Help";
					mtn.MenuDisplayName = "Help";
					mtn.MenuParentHandle = Cerebrum.ObjectHandle.Null;
					mtn.MenuMergeOrder = 0;
					mtn.Text = "(" + CurH.ToString() + ") " + mtn.MenuName;
					alMenuItem.Add(mtn);
				}

				// ��������� ������ ����.
				FillTreeView(null);
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "Cerebrum.Data.Manager.Design.Forms");
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(tvMenu.SelectedNode != null)
				{
					MenuTreeNode mtn = (MenuTreeNode)tvMenu.SelectedNode;
					CtrlHandle = mtn.MenuCurrentHandle;
					CtrlName = mtn.MenuDisplayName;
				}
				else
				{
					CtrlHandle = Cerebrum.ObjectHandle.Null;
					CtrlName = "";
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				MenuTreeNode mtnSel = null;
				Cerebrum.ObjectHandle CurMIHandle = Cerebrum.ObjectHandle.Null;
				if(tvMenu.SelectedNode != null)
				{
					mtnSel = (MenuTreeNode)tvMenu.SelectedNode;
					CurMIHandle = mtnSel.MenuCurrentHandle;
				}
				frmMenuItem frm = new frmMenuItem(SelectedDC, Cerebrum.ObjectHandle.Null, CurMIHandle, false);
				if(frm.ShowDialog() == DialogResult.OK)
				{
					MenuTreeNode mtnNew = new MenuTreeNode();
					mtnNew.MenuCurrentHandle = frm.ControlHandle;
					mtnNew.MenuParentHandle = frm.ParentHandle;
					mtnNew.MenuName = frm.ControlName;
					mtnNew.MenuDisplayName = frm.DisplayName;
					mtnNew.MenuMergeOrder = frm.MergeOrder;
					mtnNew.Text = "(" + frm.ControlHandle.ToString() + ") " + frm.ControlName;
					if((mtnSel != null)&&(mtnNew.MenuParentHandle != Cerebrum.ObjectHandle.Null))
					{
						mtnSel.Nodes.Add(mtnNew);
					}
					else
					{
						tvMenu.Nodes.Add(mtnNew);
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnEdit_Click(object sender, System.EventArgs e)
		{
			ViewMenuItemEditForm();
		}

		private void ViewMenuItemEditForm()
		{
			try
			{
				if(tvMenu.SelectedNode != null)
				{
					MenuTreeNode mtnSel = (MenuTreeNode)tvMenu.SelectedNode;
					frmMenuItem frm = new frmMenuItem(SelectedDC, mtnSel.MenuCurrentHandle, mtnSel.MenuParentHandle, true);
					if(frm.ShowDialog() == DialogResult.OK)
					{
						MenuTreeNode mtnEdit = new MenuTreeNode();
						mtnEdit.MenuName = frm.ControlName;
						mtnEdit.MenuDisplayName = frm.DisplayName;
						mtnEdit.MenuMergeOrder = frm.MergeOrder;
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnDisconnect_Click(object sender, System.EventArgs e)
		{
			try
			{
				if((tvMenu.SelectedNode != null)&&(lvCommands.SelectedItems.Count > 0))
				{
					if(MessageBox.Show(this, "��������� ����� ���� �� �������?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						MenuTreeNode mtnSel = (MenuTreeNode)tvMenu.SelectedNode;
						ListViewItem liv = lvCommands.SelectedItems[0];
						Cerebrum.ObjectHandle CmdHandle = new ObjectHandle(liv.Text);
						frmDesigner.DisableControlFromCommand(SelectedDC, mtnSel.MenuCurrentHandle, CmdHandle);
						lvCommands.Items.Remove(liv);
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnDel_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(tvMenu.SelectedNode != null)
				{
					if(frmDesigner.IsDeleteControl(SelectedDC, CtrlHandle) == true)
					{
						if(MessageBox.Show(this, "������� ����� ����?", "��������!!!", MessageBoxButtons.YesNo) == DialogResult.Yes)
						{
							MenuTreeNode mtnSel = (MenuTreeNode)tvMenu.SelectedNode;
							frmDesigner.DeleteItem(SelectedDC, "UiMenuItems", mtnSel.MenuCurrentHandle);
							mtnSel.Remove();
						}
					}
					else
					{
						MessageBox.Show("���������� ������� ����� ����. �� ������������ ���������.");
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void tvMenu_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			if(tvMenu.SelectedNode != null)
			{
				MenuTreeNode mtnSel = (MenuTreeNode)tvMenu.SelectedNode;
				lvCommands.Items.Clear();
				using(Cerebrum.Data.TableView tvCmd = Cerebrum.Management.Utilites.GetView(SelectedDC, "UiCommands"))
				{
					for(int i=0; i<tvCmd.Count; i++)
					{
						Cerebrum.Data.ComponentItemView civ = tvCmd[i];
						Cerebrum.Data.VectorView vvAC = (Cerebrum.Data.VectorView)civ["AttachedControls"];
						if(vvAC.FindItemViewByObjectHandle(mtnSel.MenuCurrentHandle) != null)
						{
							ListViewItem liv = lvCommands.Items.Add(civ.ObjectHandle.ToString());
							liv.SubItems.Add(civ["Name"].ToString());
						}
					}
				}
			}		
		}

		private void FillTreeView(MenuTreeNode mtnParent)
		{
			ArrayList alMI = (ArrayList)alMenuItem.Clone();
			for(int i=0; i<alMI.Count; i++)
			{
				MenuTreeNode mtn = (MenuTreeNode)alMI[i];
				if(mtnParent == null)
				{
					if(mtn.MenuParentHandle == Cerebrum.ObjectHandle.Null)
					{
						tvMenu.Nodes.Add(mtn);
						FillTreeView(mtn);
					}
				}
				else
				{
					if(mtnParent.MenuCurrentHandle == mtn.MenuParentHandle)
					{
						mtnParent.Nodes.Add(mtn);
						FillTreeView(mtn);
					}
				}
			}
		}

		private void tvMenu_DoubleClick(object sender, System.EventArgs e)
		{
			ViewMenuItemEditForm();
		}

		public Cerebrum.ObjectHandle MenuItemHandle
		{
			get
			{
				return CtrlHandle;
			}
		}

		public string MenuItemName
		{
			get
			{
				return CtrlName;
			}
		}
	}
}
