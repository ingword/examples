// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;

namespace Cerebrum.Data.Manager.Design
{
	/// <summary>
	/// Summary description for MenuItemInfo.
	/// </summary>
	public class MenuTreeNode : System.Windows.Forms.TreeNode
	{
		public Cerebrum.ObjectHandle MenuCurrentHandle;
		public Cerebrum.ObjectHandle MenuParentHandle;
		public string MenuName;
		public string MenuDisplayName;
		public int MenuMergeOrder;

		public MenuTreeNode()
		{
			MenuCurrentHandle = Cerebrum.ObjectHandle.Null;
			MenuParentHandle = Cerebrum.ObjectHandle.Null;
			MenuName = "";
			MenuDisplayName = "";
			MenuMergeOrder = 0;
		}
	}
}
