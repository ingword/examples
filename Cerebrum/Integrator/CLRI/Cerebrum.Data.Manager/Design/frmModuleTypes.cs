// ------------------------------------------------------------------------------------
// ��������� ��� ������� ��������� ����, ������� ������ � ������ �� ������ ������������ 
// � Cerebrum.DesktopClient.exe.
// ����������: ����� ������ ����������
// Email: BoykoSV@mail.ru
// 
// The program for convenient customisation of the menu, hotkeys and buttons on the toolbar 
// in Cerebrum. DesktopClient.exe. 
// Has developed: Boyko Sergey Vasilievich. 
// Email: BoykoSV@mail.ru.
// ------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Design.Forms
{
	/// <summary>
	/// Summary description for frmModuleTypes.
	/// </summary>
	public class frmModuleTypes : System.Windows.Forms.Form
	{
		private string SelAssemblyType;
		private string File;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ListBox lbAssemblyTypes;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmModuleTypes(string file)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			File = file;
			SelAssemblyType = "";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.lbAssemblyTypes = new System.Windows.Forms.ListBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel1.Controls.Add(this.lbAssemblyTypes);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(560, 280);
			this.panel1.TabIndex = 0;
			// 
			// lbAssemblyTypes
			// 
			this.lbAssemblyTypes.Location = new System.Drawing.Point(8, 8);
			this.lbAssemblyTypes.Name = "lbAssemblyTypes";
			this.lbAssemblyTypes.Size = new System.Drawing.Size(544, 264);
			this.lbAssemblyTypes.TabIndex = 3;
			this.lbAssemblyTypes.DoubleClick += new System.EventHandler(this.lbAssemblyTypes_DoubleClick);
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(480, 288);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "&Cancel";
			// 
			// btnOK
			// 
			this.btnOK.BackColor = System.Drawing.Color.Turquoise;
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOK.Location = new System.Drawing.Point(400, 288);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// frmModuleTypes
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(560, 319);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "frmModuleTypes";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Module types";
			this.Load += new System.EventHandler(this.frmModuleTypes_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmModuleTypes_Load(object sender, System.EventArgs e)
		{
			try
			{
				System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFile(File);
				string[] Cols = assembly.FullName.Split(new char[]{','});
				string ModuleName = Cols[0].Trim();
				System.Type[] types = assembly.GetTypes();
				for(int i=0; i<types.Length; i++)
				{
					System.Type type = types[i];
#warning � ������� ������ �������� ������ ������� ���. �� ������� ��������� ���������� ����� ������������� ���� �� �����.
					if(type.BaseType == typeof(Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor))
					{
						lbAssemblyTypes.Items.Add(type.FullName + ", " + ModuleName);
					}
				}
			}
			catch (Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "");
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			SelAssemblyType = lbAssemblyTypes.SelectedItem.ToString();
		}

		private void lbAssemblyTypes_DoubleClick(object sender, System.EventArgs e)
		{
			SelAssemblyType = lbAssemblyTypes.SelectedItem.ToString();
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		public string SelectedAssemblyType
		{
			get
			{
				return SelAssemblyType;
			}
		}
	}
}
