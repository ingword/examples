// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data
{
  public class NameValueView : System.ComponentModel.ICustomTypeDescriptor
  {
    protected internal System.Collections.IDictionary m_Attributes;
    protected System.WeakReference m_PropertiesCache;
    
    public NameValueView(System.Collections.IDictionary attributes)
    {
      m_Attributes = attributes;
      m_PropertiesCache = null; 
    }

    public System.ComponentModel.AttributeCollection GetAttributes()
    {
      return new System.ComponentModel.AttributeCollection(null);
    }

    public string GetClassName()
    {
      return null;
    }

    public string GetComponentName()
    {
      return null;
    }

    public System.ComponentModel.TypeConverter GetConverter()
    {
      return null;
    }

    public System.ComponentModel.EventDescriptor GetDefaultEvent()
    {
      return null;
    }

    public System.ComponentModel.PropertyDescriptor GetDefaultProperty()
    {
      return null;
    }

    public object GetEditor(System.Type editorBaseType)
    {
      return null;
    }

    public System.ComponentModel.EventDescriptorCollection GetEvents(System.Attribute[] attributes)
    {
      return new System.ComponentModel.EventDescriptorCollection(null);
    }

    public System.ComponentModel.EventDescriptorCollection GetEvents()
    {
      return new System.ComponentModel.EventDescriptorCollection(null);
    }

    public System.ComponentModel.PropertyDescriptorCollection GetProperties(System.Attribute[] attributes)
    {
      System.ComponentModel.PropertyDescriptorCollection result = m_PropertiesCache==null?null:(m_PropertiesCache.Target as System.ComponentModel.PropertyDescriptorCollection);
      if(result==null)
      {
        System.ComponentModel.PropertyDescriptor[] ps = new System.ComponentModel.PropertyDescriptor[m_Attributes.Count];
        int i=0;
        foreach(string key in m_Attributes.Keys)
        {
          ps[i] = new Cerebrum.Data.NameValuePropertyDescriptor(key);
          i++;
        }
        result = new System.ComponentModel.PropertyDescriptorCollection(ps);
        m_PropertiesCache = new WeakReference(result);
      }
      return result;
    }

    public System.ComponentModel.PropertyDescriptorCollection GetProperties()
    {
      return GetProperties(null);
    }

    public object GetPropertyOwner(System.ComponentModel.PropertyDescriptor pd)
    {
      return this;
    }

    public object this[string name]
    {
      get
      {
        return m_Attributes[name];
      }
      set
      {
        m_Attributes[name] = value;
      }
    }
  }

  public class NameValuePropertyDescriptor : System.ComponentModel.PropertyDescriptor
  {
    private string MappingName;
    public NameValuePropertyDescriptor(string name) : base(name, null)
    {
      MappingName = name;
    }

    public override bool CanResetValue(object component)
    {
      return true;
    }

    public override object GetValue(object component)
    {
      Cerebrum.Data.NameValueView v1=component as Cerebrum.Data.NameValueView;
      if(v1!=null)
      {
        return v1.m_Attributes[MappingName];
      }
      return null;
    }

    public override void ResetValue(object component)
    {
      Cerebrum.Data.NameValueView v1=component as Cerebrum.Data.NameValueView;
      if(v1!=null)
      {
        v1.m_Attributes[MappingName] = System.Convert.DBNull;
      }
    }

    public override void SetValue(object component, object value)
    {
      Cerebrum.Data.NameValueView v1=component as Cerebrum.Data.NameValueView;
      if(v1!=null)
      {
        v1.m_Attributes[MappingName] = value;
      }
    }

    public override bool ShouldSerializeValue(object component)
    {
      return false;
    }

    public override System.Type ComponentType
    {
      get
      {
        return typeof(Cerebrum.Data.NameValueView);
      }
    }

    public override bool IsBrowsable
    {
      get
      {
        return true;
      }
    }

    public override bool IsReadOnly
    {
      get
      {
        return false;
      }
    }

    public override System.Type PropertyType
    {
      get
      {
        return typeof(object);
      }
    }
  }

}
