// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data.Manager
{
	/// <summary>
	/// Summary description for DataManagerUiService.
	/// </summary>
	public class UiService : Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor 
	{
		public UiService()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public override bool CommandActivated(Cerebrum.IConnector message, object sender, object argument)
		{
			string name = ((Cerebrum.Reflection.MessageDescriptor)message.Component).Name;

			switch(name)
			{
				case "Open":
				{
					CreateDocument();
					return true;
				}

			}
			return base.CommandActivated(message, sender, argument);
		}

		public static Cerebrum.Data.Manager.frmDomainList CreateDocument()
		{
			Cerebrum.Data.Manager.frmDomainList dlg0 = new frmDomainList();
			((Cerebrum.IComponent)dlg0).SetConnector(Cerebrum.SerializeDirection.Init, new Cerebrum.Runtime.SurrogateComposite(Cerebrum.Windows.Forms.Application.Instance.MasterContext.Workspace));
			dlg0.MdiParent = Cerebrum.Windows.Forms.Application.Instance.PrimaryWindow;
			dlg0.Show();

			return dlg0;
		}
	}
}
