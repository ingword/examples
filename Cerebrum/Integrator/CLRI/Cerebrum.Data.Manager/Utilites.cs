// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

namespace Cerebrum.Data
{
  
	public interface IDataFieldInfo
	{
		string TableName {get;}
		string FieldName {get;}
		string RelatedTableName {get;}
		string RelatedFieldName {get;}
		bool Browsable {get;}
		bool IsPassword {get;}
		bool Related {get;}
		bool ReadOnly {get;}
		bool Filter {get;}
		bool AllowDBNull {get;}
	}
	public interface IDataTableInfo
	{
		string DisplayName {get;}
		string DisplayFieldName {get;}
		bool Browsable {get;}
	}
	public interface IDataContext
	{
		IDataTableInfo GetTable(string tableName);
		IDataFieldInfo GetColumn(string tableName, string FieldName);
		IDataFieldInfo[] GetColumns(string tableName);
	}

	public interface IUpdateDS
	{
		bool TransactionsEnabled {get;}
		void UpdateDataSource(System.ComponentModel.IBindingList updatedDataSource);
		void RejectDataSource(System.ComponentModel.IBindingList updatedDataSource);
	}
}

