// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Windows.Forms;
namespace Cerebrum.Data
{
    //  Cerebrum.Data.DataGridComboBoxColumn
    public class DataGridTextBoxColumn : Cerebrum.Data.DataGridColumnStyle
     // UI Constants
    {
        private System.Windows.Forms.TextBox mTextBox;
        // // Used to track editing state
        private object oldValue = null;
        private bool inEdit = false;
         /// <summary>
         ///     Create a new column
         /// </summary>
        public DataGridTextBoxColumn()
        {
            yMargin = 0;
            mTextBox = new System.Windows.Forms.TextBox();
            mTextBox.Visible = false;
            mTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        }

         //----------------------------------------------------------         //Properties to allow us to set up the datasource for the TextBox         //----------------------------------------------------------
         //----------------------------------------------------------         //Methods overridden from DataGridColumnStyleStyle         //----------------------------------------------------------
        protected override void Abort(int rowNum)
        {
            RollBack();
            HideTextBox();
            EndEdit();
        }

        protected override bool Commit(System.Windows.Forms.CurrencyManager dataSource, int rowNum)
        {
            HideTextBox();
            // //If we are not in an edit then simply return
            if ((! inEdit))
            {
                return true;
            }

            try
            {
                object value;
                if (mTextBox.Text.Length < 1)
                {
                    value = Convert.DBNull;
                }
                else
                {
                    value = mTextBox.Text;
                    if (value == null || (NullText.Equals(value)))
                    {
                        value = Convert.DBNull;
                    }
                }

                if (! value.Equals(oldValue))
                {
                    SetColumnValueAtRow(dataSource, rowNum, value);
                }

            }
            catch //(System.Exception ex)
            {
                RollBack();
                return false;
            }

            EndEdit();
            return true;
        }

        protected override void ConcedeFocus()
        {
            mTextBox.Visible = false;
        }

        protected override void Edit(
			System.Windows.Forms.CurrencyManager source, 
			int rowNum, 
			System.Drawing.Rectangle bounds, 
			bool readOnly, 
			string instantText, 
			bool cellIsVisible)
        {
            if ((rowNum &  1) ==  0)
            {
                mTextBox.BackColor = this.mBackColor;
            }
            else
            {
                mTextBox.BackColor = this.mAlternatingBackColor;
            }

            mTextBox.Text = "";

			System.Drawing.Rectangle originalBounds = bounds;
            if (instantText == null)
            {
                oldValue = GetColumnValueAtRow(source, rowNum);
            }
            else
            {
                oldValue = instantText;
            }

            try
            {
                mTextBox.Text = GetText(oldValue);
            }
            catch //(System.Exception ex)
            {
            }

            if (readOnly || this.ReadOnly)
            {
                mTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
                mTextBox.ReadOnly = true;
            }

            else
            {
                mTextBox.BackColor = System.Drawing.SystemColors.Window;
                mTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                mTextBox.ReadOnly = false;
            }

            if (cellIsVisible)
            {
                bounds.Offset(xMargin, yMargin);
                bounds.Width -= xMargin* 2;
                bounds.Height -= yMargin;
                mTextBox.Bounds = bounds;
                mTextBox.Visible = true;
            }

            else
            {
                mTextBox.Bounds = originalBounds;
                mTextBox.Visible = false;
            }

            mTextBox.RightToLeft = this.DataGridTableStyle.DataGrid.RightToLeft;
            mTextBox.Focus();


            if ((mTextBox.Visible))
            {
                this.DataGridTableStyle.DataGrid.Invalidate(originalBounds);
            }

            inEdit = true;
        }

        protected override int GetMinimumHeight()
        {
        //Set the minimum height to the height of the TextBox            return mTextBox.PreferredHeight+ yMargin;
        }

        protected override void SetDataGridInColumn(System.Windows.Forms.DataGrid value)
        {
            base.SetDataGridInColumn(value);
            if (mTextBox.Parent != null && mTextBox.Parent !=  value)
            {
                mTextBox.Parent.Controls.Remove(mTextBox);
            }

            if ( value != null)
            {
                value.Controls.Add(mTextBox);
            }

        }

        protected override void UpdateUI(System.Windows.Forms.CurrencyManager source, int rowNum, string instantText)
        {
			if (instantText == null)
			{
				mTextBox.Text = GetText(GetColumnValueAtRow(source, rowNum));
			}
			else
            {
                mTextBox.Text = instantText;
            }

        }

        // //----------------------------------------------------------
        // //Helper Methods 
        // //----------------------------------------------------------
        private void EndEdit()
        {
            inEdit = false;
            Invalidate();
        }

        protected override string GetText(object value)
        {
            if (value == null || value is System.DBNull)
            {
                return NullText;
            }

            try
            {
                return Convert.ToString(value);
            }

            catch //(System.Exception ex)
            {
                return ("####");
            }

        }

        private void HideTextBox()
        {
            if ((mTextBox.Focused))
            {
                this.DataGridTableStyle.DataGrid.Focus();
            }

            mTextBox.Visible = false;
        }

        private void RollBack()
        {
            try
            {
                mTextBox.Text = oldValue.ToString();
            }

            catch //(System.Exception ex)
            {
            }

        }

        public System.Windows.Forms.TextBox TextBox
        {
			get
			{
				return mTextBox;
			}
        }
    }

}

