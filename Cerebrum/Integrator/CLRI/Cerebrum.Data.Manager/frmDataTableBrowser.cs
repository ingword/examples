// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Forms
{
	/// <summary>
	/// Summary description for frmDataTableBrowser.
	/// </summary>
	public class frmDataTableBrowser : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button dsb_btnAccept;
		private Cerebrum.Data.DataGrid dsb_dgBrowse;
		private System.Windows.Forms.Button btnEngage;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmDataTableBrowser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnEngage = new System.Windows.Forms.Button();
			this.dsb_btnAccept = new System.Windows.Forms.Button();
			this.dsb_dgBrowse = new Cerebrum.Data.DataGrid();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dsb_dgBrowse)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnEngage);
			this.panel1.Controls.Add(this.dsb_btnAccept);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 233);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(292, 40);
			this.panel1.TabIndex = 0;
			// 
			// btnEngage
			// 
			this.btnEngage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEngage.BackColor = System.Drawing.Color.Turquoise;
			this.btnEngage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEngage.Location = new System.Drawing.Point(128, 8);
			this.btnEngage.Name = "btnEngage";
			this.btnEngage.TabIndex = 2;
			this.btnEngage.Text = "Engage";
			this.btnEngage.Click += new System.EventHandler(this.btnEngage_Click);
			// 
			// dsb_btnAccept
			// 
			this.dsb_btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_btnAccept.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.dsb_btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnAccept.Location = new System.Drawing.Point(208, 8);
			this.dsb_btnAccept.Name = "dsb_btnAccept";
			this.dsb_btnAccept.TabIndex = 1;
			this.dsb_btnAccept.Text = "&Ok";
			// 
			// dsb_dgBrowse
			// 
			this.dsb_dgBrowse.AllowNavigation = false;
			this.dsb_dgBrowse.AlternatingBackColor = System.Drawing.Color.Ivory;
			this.dsb_dgBrowse.BackColor = System.Drawing.Color.LightYellow;
			this.dsb_dgBrowse.BackgroundColor = System.Drawing.Color.SteelBlue;
			this.dsb_dgBrowse.CaptionBackColor = System.Drawing.Color.SlateGray;
			this.dsb_dgBrowse.CaptionFont = new System.Drawing.Font("Times New Roman", 12F);
			this.dsb_dgBrowse.DataMember = "";
			this.dsb_dgBrowse.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dsb_dgBrowse.GridLineColor = System.Drawing.Color.LightGray;
			this.dsb_dgBrowse.HeaderBackColor = System.Drawing.Color.PaleGoldenrod;
			this.dsb_dgBrowse.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dsb_dgBrowse.LinkColor = System.Drawing.Color.Blue;
			this.dsb_dgBrowse.Location = new System.Drawing.Point(0, 0);
			this.dsb_dgBrowse.Name = "dsb_dgBrowse";
			this.dsb_dgBrowse.ParentRowsBackColor = System.Drawing.Color.Aquamarine;
			this.dsb_dgBrowse.SelectionBackColor = System.Drawing.Color.Firebrick;
			this.dsb_dgBrowse.SelectionForeColor = System.Drawing.Color.White;
			this.dsb_dgBrowse.Size = new System.Drawing.Size(292, 233);
			this.dsb_dgBrowse.TabIndex = 1;
			// 
			// frmDataTableBrowser
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.dsb_dgBrowse);
			this.Controls.Add(this.panel1);
			this.Name = "frmDataTableBrowser";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "frmDataTableBrowser";
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dsb_dgBrowse)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		System.ComponentModel.IBindingList DataSource;
		public System.ComponentModel.IBindingList DataView;

		public void PopulateGrid(System.ComponentModel.IBindingList DataSource, System.ComponentModel.IBindingList DataView, System.ComponentModel.PropertyDescriptor [] Attributes, Cerebrum.Data.IDataContext context, string caption)
		{
			this.DataSource = DataSource;
			this.DataView = DataView;
			dsb_dgBrowse.TableStyles.Clear();
			dsb_dgBrowse.CaptionText = caption;
			Cerebrum.Data.DataGrid.PopulateTableStyle(dsb_dgBrowse, DataSource, DataView, Attributes, context);
		}

		private void btnEngage_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Data.Manager.frmEngageObject dlg = new frmEngageObject();
			dlg.Initialize(this.DataSource);
			if(dlg.ShowDialog()==System.Windows.Forms.DialogResult.OK)
			{
				Cerebrum.Data.ComponentItemView item = dlg.SelectedItem as Cerebrum.Data.ComponentItemView;
				using(Cerebrum.IComposite composite = item.GetComposite())
				{
					(this.DataView as Cerebrum.Data.SimpleView).EngageConnector(Cerebrum.ObjectHandle.Parse(dlg.strObjectHandle), composite);
				}
			}
		}
	}
}
