// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager
{
	/// <summary>
	/// Summary description for frmDomainList.
	/// </summary>
	public class frmDomainList : Cerebrum.Windows.Forms.DesktopComponent
		//public class frmDomainList : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnTables;
		private System.Windows.Forms.ListBox lstDomains;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.Button btnUnload;
		private System.Windows.Forms.Button btnImport;
		private System.Windows.Forms.Button btnCreate;
		private System.Windows.Forms.Button btnExport;
		private System.Windows.Forms.Button btnDesign;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmDomainList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			Cerebrum.Windows.Forms.Application.Instance.DomainOpened += new Cerebrum.Integrator.DomainContextEventHandler(Application_DomainOpened);
			Cerebrum.Windows.Forms.Application.Instance.DomainClosed += new Cerebrum.Integrator.DomainContextEventHandler(Application_DomainClosed);
			RefreshDomainsList();
		}

		public void RefreshDomainsList()
		{
			this.ClearList();
			for(int i = -1; i < Cerebrum.Windows.Forms.Application.Instance.OpenedDomains.Count;i++)
			{
				Cerebrum.Integrator.DomainContext c;
				if(i < 0)
				{
					c = Cerebrum.Windows.Forms.Application.Instance.MasterContext;
				}
				else
				{
					c = Cerebrum.Windows.Forms.Application.Instance.OpenedDomains[i] as Cerebrum.Integrator.DomainContext;
				}
					
				string title = c.DomainName;
 
				this.AddContext(c, (i + 1).ToString() + ((title==null || title.Length<1)?"":(" - " + title)));
			}
			RefreshLayout();
		}
		public void RefreshLayout()
		{
			Cerebrum.Integrator.DomainContext c = this.SelectedContext;
			if(c==null)
			{
				btnUnload.Enabled = false;
				btnExport.Enabled = false;
				btnImport.Enabled = false;
				btnTables.Enabled = false;
				btnDesign.Enabled = false;

				return;
			}
			else
			{
				if(c==Cerebrum.Windows.Forms.Application.Instance.MasterContext)
				{
					btnUnload.Enabled = false;
				}
				else
				{
					btnUnload.Enabled = true;
				}
				btnExport.Enabled = true;
				btnImport.Enabled = true;
				btnTables.Enabled = true;
				btnDesign.Enabled = true;
			}
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnExport = new System.Windows.Forms.Button();
			this.btnCreate = new System.Windows.Forms.Button();
			this.btnImport = new System.Windows.Forms.Button();
			this.btnUnload = new System.Windows.Forms.Button();
			this.btnLoad = new System.Windows.Forms.Button();
			this.btnTables = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lstDomains = new System.Windows.Forms.ListBox();
			this.btnDesign = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnDesign);
			this.panel1.Controls.Add(this.btnExport);
			this.panel1.Controls.Add(this.btnCreate);
			this.panel1.Controls.Add(this.btnImport);
			this.panel1.Controls.Add(this.btnUnload);
			this.panel1.Controls.Add(this.btnLoad);
			this.panel1.Controls.Add(this.btnTables);
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(320, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(88, 453);
			this.panel1.TabIndex = 0;
			// 
			// btnExport
			// 
			this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnExport.BackColor = System.Drawing.Color.Turquoise;
			this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnExport.Location = new System.Drawing.Point(8, 168);
			this.btnExport.Name = "btnExport";
			this.btnExport.TabIndex = 6;
			this.btnExport.Text = "Export";
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// btnCreate
			// 
			this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCreate.BackColor = System.Drawing.Color.Turquoise;
			this.btnCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCreate.Location = new System.Drawing.Point(8, 8);
			this.btnCreate.Name = "btnCreate";
			this.btnCreate.TabIndex = 5;
			this.btnCreate.Text = "Create";
			this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
			// 
			// btnImport
			// 
			this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnImport.BackColor = System.Drawing.Color.Turquoise;
			this.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnImport.Location = new System.Drawing.Point(8, 144);
			this.btnImport.Name = "btnImport";
			this.btnImport.TabIndex = 4;
			this.btnImport.Text = "Import";
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			// 
			// btnUnload
			// 
			this.btnUnload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnUnload.BackColor = System.Drawing.Color.Turquoise;
			this.btnUnload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnUnload.Location = new System.Drawing.Point(8, 208);
			this.btnUnload.Name = "btnUnload";
			this.btnUnload.TabIndex = 3;
			this.btnUnload.Text = "Unload";
			this.btnUnload.Click += new System.EventHandler(this.btnUnload_Click);
			// 
			// btnLoad
			// 
			this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnLoad.BackColor = System.Drawing.Color.Turquoise;
			this.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLoad.Location = new System.Drawing.Point(8, 32);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.TabIndex = 2;
			this.btnLoad.Text = "Load";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// btnTables
			// 
			this.btnTables.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnTables.BackColor = System.Drawing.Color.Turquoise;
			this.btnTables.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTables.Location = new System.Drawing.Point(8, 96);
			this.btnTables.Name = "btnTables";
			this.btnTables.TabIndex = 1;
			this.btnTables.Text = "Tables ...";
			this.btnTables.Click += new System.EventHandler(this.btnTables_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Location = new System.Drawing.Point(8, 416);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 0;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lstDomains
			// 
			this.lstDomains.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstDomains.IntegralHeight = false;
			this.lstDomains.Location = new System.Drawing.Point(0, 0);
			this.lstDomains.Name = "lstDomains";
			this.lstDomains.Size = new System.Drawing.Size(320, 453);
			this.lstDomains.TabIndex = 1;
			this.lstDomains.DoubleClick += new System.EventHandler(this.lstDomains_DoubleClick);
			this.lstDomains.SelectedIndexChanged += new System.EventHandler(this.lstDomains_SelectedIndexChanged);
			// 
			// btnDesign
			// 
			this.btnDesign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDesign.BackColor = System.Drawing.Color.Turquoise;
			this.btnDesign.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDesign.Location = new System.Drawing.Point(8, 120);
			this.btnDesign.Name = "btnDesign";
			this.btnDesign.TabIndex = 7;
			this.btnDesign.Text = "Design ...";
			this.btnDesign.Click += new System.EventHandler(this.btnDesign_Click);
			// 
			// frmDomainList
			// 
			this.AcceptButton = this.btnTables;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(408, 453);
			this.Controls.Add(this.lstDomains);
			this.Controls.Add(this.panel1);
			this.Name = "frmDomainList";
			this.ShowInTaskbar = false;
			this.Text = "Select domain";
			this.Load += new System.EventHandler(this.frmDomainList_Load);
			this.Closed += new System.EventHandler(this.frmDomainList_Closed);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void lstDomains_DoubleClick(object sender, System.EventArgs e)
		{
			btnTables_Click(sender, e);
		}

		private void btnTables_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Integrator.DomainContext c = this.SelectedContext;
			if(c==null) return;

			foreach(Cerebrum.Data.Manager.frmDataBaseBrowser dlg0 in this.m_Browsers)
			{
				if(System.Object.ReferenceEquals(dlg0.DomainContext, c))
				{
					dlg0.Activate();
					return;
				}
			}

			Cerebrum.Data.Manager.frmDataBaseBrowser dlg1 = new Cerebrum.Data.Manager.frmDataBaseBrowser();
			dlg1.Closed += new EventHandler(DataBaseBrowser_Closed);
			this.m_Browsers.Add(dlg1);
			dlg1.DataSourceObjRef = new Cerebrum.Data.Manager.DataSourceObjRef();
			((Cerebrum.IComponent)dlg1.DataSourceObjRef).SetConnector(Cerebrum.SerializeDirection.Init, new Cerebrum.Runtime.SurrogateComposite(c.Workspace));
			((Cerebrum.IComponent)dlg1).SetConnector(Cerebrum.SerializeDirection.Init, new Cerebrum.Runtime.SurrogateComposite(c.Workspace));
			dlg1.MdiParent = Cerebrum.Windows.Forms.Application.Instance.PrimaryWindow;
			dlg1.Show();
			//dlg1.ShowDialog(Cerebrum.Windows.Forms.Application.Instance.PrimaryWindow);
		}

		public void AddContext(Cerebrum.Integrator.DomainContext context, string displayName)
		{
			lstDomains.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem(displayName, context));
		}
		public void ClearList()
		{
			lstDomains.Items.Clear();
		}
		
		private Cerebrum.Integrator.DomainContext SelectedContext
		{
			get
			{
				if(lstDomains.SelectedIndex>=0)
				{
					return (lstDomains.SelectedItem as Cerebrum.Windows.Forms.DropDownStringItem).Value as Cerebrum.Integrator.DomainContext;
				}
				else
				{
					return null;
				}
			}
		}

		private void frmDomainList_Load(object sender, System.EventArgs e)
		{
			if(lstDomains.Items.Count>0)
			{
				lstDomains.SelectedIndex = 0;
			}
			this.MinimumSize = this.Size;
		}

		private void btnUnload_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Integrator.DomainContext c = this.SelectedContext;
			c.Shutdown();
		}

		private void Application_DomainOpened(object sender, Cerebrum.Integrator.DomainContextEventArgs e)
		{
			RefreshDomainsList();
		}

		private void Application_DomainClosed(object sender, Cerebrum.Integrator.DomainContextEventArgs e)
		{
			RefreshDomainsList();
		}

		private void frmDomainList_Closed(object sender, System.EventArgs e)
		{
			while(this.m_Browsers.Count > 0)
			{
				Cerebrum.Data.Manager.frmDataBaseBrowser dlg0 = (Cerebrum.Data.Manager.frmDataBaseBrowser)this.m_Browsers[0];
				dlg0.Close();
			}

			Cerebrum.Windows.Forms.Application.Instance.DomainOpened -= new Cerebrum.Integrator.DomainContextEventHandler(Application_DomainOpened);
			Cerebrum.Windows.Forms.Application.Instance.DomainClosed -= new Cerebrum.Integrator.DomainContextEventHandler(Application_DomainClosed);
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void btnImport_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Integrator.DomainContext c = this.SelectedContext;
			if(c==null) return;
			System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
			dlg.DefaultExt = "xvmg";
			dlg.Filter = "Cerebrum Import/Export Merge file (*.xvmg)|*.xvmg|Cerebrum Export/Import Dump file (*.xvnn)|*.xvnn";
			if(dlg.ShowDialog(Cerebrum.Windows.Forms.Application.Instance.PrimaryWindow)==System.Windows.Forms.DialogResult.OK)
			{
				int mode = 1;
				if(dlg.FilterIndex==2)
				{
					mode = 2;
				}
				using(System.IO.Stream fs = dlg.OpenFile())
				{
					using(System.IO.StreamReader sr = new System.IO.StreamReader(fs, System.Text.Encoding.Default))
					{
						System.Xml.XmlTextReader xr = new System.Xml.XmlTextReader(sr);
						System.Collections.Hashtable handleMap;
						if(mode != 2)
						{
							handleMap = new System.Collections.Hashtable();
						}
						else
						{
							handleMap = null;
						}
						Cerebrum.Management.Utilites.ImportDatabase(c.Workspace, xr, handleMap);
						xr.Close();
						sr.Close();
					}
					fs.Close();
				}
			}
		}

		private void btnCreate_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Windows.Forms.Application application = this.DomainContext.Application as Cerebrum.Windows.Forms.Application;
			Cerebrum.Windows.Forms.CreateDatabase dlg = new Cerebrum.Windows.Forms.CreateDatabase(Cerebrum.Windows.Forms.CreateDatabaseOptions.All);
			dlg.DomainName = "Cerebrum.Custom.Application";
			if(dlg.ShowDialog(application.PrimaryWindow)==System.Windows.Forms.DialogResult.OK)
			{
				Cerebrum.Integrator.IContextFactory lcs = new Cerebrum.Management.SimpleContextFactory(application, dlg.ClusterFileName, null, dlg.ClusterFrameSize, dlg.Versioning, string.Empty, true);

				Cerebrum.IWorkspace domain = lcs.InitContext(dlg.ClusterFileName, dlg.DomainName);
				Cerebrum.Integrator.DomainContext c = domain.Component as Cerebrum.Integrator.DomainContext;
				application.AddDomain(c);
						
			}
		}

		private void btnLoad_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Windows.Forms.Application application = this.DomainContext.Application as Cerebrum.Windows.Forms.Application;
			System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
			dlg.ShowReadOnly = true;
			dlg.DefaultExt = "vnn";
			dlg.Filter = "Cerebrum Database files (*.vnn)|*.vnn|Cerebrum Database files (*.bin)|*.bin|All files|*.*";
			if(dlg.ShowDialog(application.PrimaryWindow)==System.Windows.Forms.DialogResult.OK)
			{
				Cerebrum.Integrator.IContextFactory lcs = new Cerebrum.Management.SimpleContextFactory(application, dlg.FileName, null, 4, true, "", false);
				Cerebrum.IWorkspace domain = lcs.LoadContext(dlg.FileName, dlg.ReadOnlyChecked);
				Cerebrum.Integrator.DomainContext c = domain.Component as Cerebrum.Integrator.DomainContext;
				application.AddDomain(c);
			}
		}

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Windows.Forms.Application application = this.DomainContext.Application as Cerebrum.Windows.Forms.Application;
			Cerebrum.Integrator.DomainContext c = this.SelectedContext;
			if(c==null) return;
			Cerebrum.Data.Manager.Forms.frmExportMode dlgMode = new Cerebrum.Data.Manager.Forms.frmExportMode();
			if(dlgMode.ShowDialog()==System.Windows.Forms.DialogResult.OK)
			{
				System.Windows.Forms.SaveFileDialog dlg = new System.Windows.Forms.SaveFileDialog();
				dlg.DefaultExt = "xvnn";
				dlg.Filter = "Cerebrum Export/Import Dump file (*.xvnn)|*.xvnn";
				if(dlg.ShowDialog(application.PrimaryWindow)==System.Windows.Forms.DialogResult.OK)
				{
					using(System.IO.Stream fs = dlg.OpenFile())
					{
						using(System.IO.StreamWriter sw = new System.IO.StreamWriter(fs, System.Text.Encoding.Default))
						{
							System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(sw);

							xw.Formatting = System.Xml.Formatting.Indented;

							if(dlgMode.SelectedMode=="N1")
							{
								Cerebrum.Management.Utilites.ExportWorkspace(c.Workspace, xw);
							}
							else if(dlgMode.SelectedMode=="R2")
							{
								Cerebrum.Management.Utilites.ExportDatabase(c.Workspace, xw, true);
							}
							else
							{
								Cerebrum.Management.Utilites.ExportDatabase(c.Workspace, xw, false);
							}

							xw.Close();
							sw.Close();
						}
						fs.Close();
					}
				}
			}
		}


		private void lstDomains_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			RefreshLayout();
		}

		private System.Collections.ArrayList m_Browsers = new System.Collections.ArrayList();
		private void DataBaseBrowser_Closed(object sender, EventArgs e)
		{
			(sender as Cerebrum.Data.Manager.frmDataBaseBrowser).Closed -= new EventHandler(DataBaseBrowser_Closed);
			m_Browsers.Remove(sender);
		}

		private void btnDesign_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Integrator.DomainContext c = this.SelectedContext;
			if(c==null) return;

			bool IsContinue = true;
			string TblList = "";
			Cerebrum.ObjectHandle TblHandle = Cerebrum.ObjectHandle.Null;
			TblHandle = Cerebrum.Management.Utilites.ResolveHandle(c, "Tables", "Name", "Messages");
			if(TblHandle == Cerebrum.ObjectHandle.Null)
			{
				IsContinue = false;
				TblList += " Messages" + System.Environment.NewLine;
			}
			TblHandle = Cerebrum.Management.Utilites.ResolveHandle(c, "Tables", "Name", "UiCommands");
			if(TblHandle == Cerebrum.ObjectHandle.Null)
			{
				IsContinue = false;
				TblList += " UiCommands" + System.Environment.NewLine;
			}
			TblHandle = Cerebrum.Management.Utilites.ResolveHandle(c, "Tables", "Name", "UiMenuItems");
			if(TblHandle == Cerebrum.ObjectHandle.Null)
			{
				IsContinue = false;
				TblList += " UiMenuItems" + System.Environment.NewLine;
			}
			TblHandle = Cerebrum.Management.Utilites.ResolveHandle(c, "Tables", "Name", "UiServices");
			if(TblHandle == Cerebrum.ObjectHandle.Null)
			{
				IsContinue = false;
				TblList += " UiServices" + System.Environment.NewLine;
			}
			TblHandle = Cerebrum.Management.Utilites.ResolveHandle(c, "Tables", "Name", "UiBandItems");
			if(TblHandle == Cerebrum.ObjectHandle.Null)
			{
				IsContinue = false;
				TblList += " UiBandItems" + System.Environment.NewLine;
			}
			TblHandle = Cerebrum.Management.Utilites.ResolveHandle(c, "Tables", "Name", "UiHotKeys");
			if(TblHandle == Cerebrum.ObjectHandle.Null)
			{
				IsContinue = false;
				TblList += " UiHotKeys" + System.Environment.NewLine;
			}
			TblHandle = Cerebrum.Management.Utilites.ResolveHandle(c, "Tables", "Name", "UiBandPlaces");
			if(TblHandle == Cerebrum.ObjectHandle.Null)
			{
				IsContinue = false;
				TblList += " UiBandPlaces" + System.Environment.NewLine;
			}
			TblHandle = Cerebrum.Management.Utilites.ResolveHandle(c, "Tables", "Name", "UiFactories");
			if(TblHandle == Cerebrum.ObjectHandle.Null)
			{
				IsContinue = false;
				TblList += " UiFactories" + System.Environment.NewLine;
			}
			TblHandle = Cerebrum.Management.Utilites.ResolveHandle(c, "Tables", "Name", "UiFileTypes");
			if(TblHandle == Cerebrum.ObjectHandle.Null)
			{
				IsContinue = false;
				TblList += " UiFileTypes" + System.Environment.NewLine;
			}
			if(IsContinue == true)
			{
				Cerebrum.Data.Manager.Design.Forms.frmDesigner frm = new Cerebrum.Data.Manager.Design.Forms.frmDesigner(c);
				frm.ShowDialog();
			}
			else
			{
				string Message = "� ���� ����������� ��������� �������: " + System.Environment.NewLine;
				Message += TblList;
				MessageBox.Show(this, Message, "������ ������� ��������� ���������� ������������ ����������.");
			}
		}

	}
}
