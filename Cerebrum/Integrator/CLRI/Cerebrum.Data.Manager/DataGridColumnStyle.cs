// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Windows.Forms;
namespace Cerebrum.Data
{
	//  Cerebrum.Data.DataGridComboBoxColumn
	public abstract class DataGridColumnStyle : System.Windows.Forms.DataGridColumnStyle
	{
		protected System.Drawing.Color mBackColor;
		protected System.Drawing.Color mAlternatingBackColor;
		protected int xMargin = 1;
		//  2
		protected int yMargin = 1;
		public System.Drawing.Color BackColor
		{
			get
			{
				return mBackColor;
			}			set			{
				mBackColor = value;
			}
		}

		public System.Drawing.Color AlternatingBackColor
		{
			get
			{
				return mAlternatingBackColor;
			}			set
			{
				mAlternatingBackColor = value;
			}
		}

		#region " DataColumn Overrides "
		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum)
		{
			Paint(g, bounds, source, rowNum, false);
		}

		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, bool alignToRight)
		{
			Brush backBrush = new SolidBrush(this.DataGridTableStyle.DataGrid.BackColor);
			Brush foreBrush = new SolidBrush(this.DataGridTableStyle.DataGrid.ForeColor);
			PaintText(g, bounds, GetText(GetColumnValueAtRow(source, rowNum)), rowNum, backBrush, foreBrush, alignToRight, this.DataGridTableStyle.DataGrid.IsSelected(rowNum));
		}

		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
		{
			bool bs = false;
			try
			{
				bs = this.DataGridTableStyle.DataGrid.IsSelected(rowNum);
			}
			catch //(System.Exception ex)
			{
			}

			PaintText(g, bounds, GetText(GetColumnValueAtRow(source, rowNum)), rowNum, backBrush, foreBrush, alignToRight, bs);
		}

		protected override int GetPreferredHeight(Graphics g, Object value)
		{
			int newLineIndex = -1;
			int newLines = 0;
			string valueString = this.GetText(value);
			do
			{
				newLineIndex = valueString.IndexOf(Environment.NewLine, newLineIndex+ 1);
				newLines += 1;
			}while ((newLineIndex !=  - 1));

			return FontHeight * newLines + yMargin;
		}

		protected override Size GetPreferredSize(Graphics g, Object value)
		{
			Size extents = Size.Ceiling(g.MeasureString(GetText(value), this.DataGridTableStyle.DataGrid.Font));
			extents.Width += xMargin * 2+ this.DataGridTableGridLineWidth;
			extents.Height += yMargin;
			return extents;
		}

		/*protected override void SetColumnValueAtRow(CurrencyManager source, int rowNum, object value)
		{
			object newValue;
			if(value==null || Convert.IsDBNull(value))
			{
				newValue = value;
			}
			else if(value!=null && value.GetType() == this.PropertyDescriptor.PropertyType)
			{
				newValue = value;
			}
			else
			{
				newValue = System.ComponentModel.TypeDescriptor.GetConverter(this.PropertyDescriptor.PropertyType).ConvertFrom(value);
			}
			base.SetColumnValueAtRow (source, rowNum, newValue);
		}*/

		#endregion

		protected virtual void PaintText(Graphics g, Rectangle textBounds, string text, int rowNum, Brush backBrush, Brush foreBrush, bool alignToRight, bool selectedRow)
		{
			if (! selectedRow)
			{
				if ((rowNum & 1) ==  0)
				{
					backBrush = new SolidBrush(this.mBackColor);
				}

				else
				{
					backBrush = new SolidBrush(this.mAlternatingBackColor);
				}

			}

			Rectangle rect = textBounds;
			StringFormat format = new StringFormat();
			if (alignToRight)
			{
				format.FormatFlags = format.FormatFlags |  StringFormatFlags.DirectionRightToLeft;
			}

			if (this.Alignment ==  HorizontalAlignment.Left)
			{
				format.Alignment = StringAlignment.Near;
			}
			else if (this.Alignment ==  HorizontalAlignment.Center)
			{
				format.Alignment = StringAlignment.Center;
			}
			else
			{
				format.Alignment = StringAlignment.Far;
			}

			format.FormatFlags = format.FormatFlags | StringFormatFlags.NoWrap;
			g.FillRectangle(backBrush, rect);
			rect.Offset(0, yMargin);
			rect.Height -= yMargin;
			g.DrawString(text, this.DataGridTableStyle.DataGrid.Font, foreBrush, rect, format);
			format.Dispose();
		}

		protected int DataGridTableGridLineWidth
		{
			get
			{
				if (this.DataGridTableStyle.GridLineStyle ==  DataGridLineStyle.Solid)
				{
					return 1;
				}

				else
				{
					return 0;
				}
			}		}

		protected abstract string GetText(Object value);

	}

}

