// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data
{
	public class DataListUnion : System.ComponentModel.Component, System.ComponentModel.IBindingList, System.ComponentModel.ISupportInitialize, System.ComponentModel.ITypedList
	{
		private System.Collections.ArrayList Lists;
		public DataListUnion()
		{
			Lists = new System.Collections.ArrayList();
		}

		public void Add(System.ComponentModel.IBindingList v)
		{
			Lists.Add(v);
			v.ListChanged +=new System.ComponentModel.ListChangedEventHandler(DataView_ListChanged);
			this.Reset();
		}

		public void Add(System.ComponentModel.ICustomTypeDescriptor r)
		{
			int c = this.Lists.Count;
			DataItemUnion ru = null;
			if(!(c>0 && (ru=this.Lists[c - 1] as DataItemUnion)!=null))
			{
				ru = new DataItemUnion();
				this.Lists.Add(ru);
				ru.ListChanged +=new System.ComponentModel.ListChangedEventHandler(DataView_ListChanged);
			}
			ru.Add(r);
			//this.Reset(); <-- ru.ListChanged
		}

		public void Add(System.ComponentModel.ICustomTypeDescriptor[] r)
		{
			foreach (System.ComponentModel.ICustomTypeDescriptor row in r)
			{
				this.Add(row);
			}
		}

		public int Add(object value)
		{
			/*if(value is System.Data.DataRow)
			{
			  this.Add((System.Data.DataRow)value);
			}
			else if(value is System.Data.DataTable)
			{
			  this.Add((System.Data.DataTable)value);
			}
			else if(value is System.ComponentModel.ICustomTypeDescriptor)
			{
			  this.Add((System.ComponentModel.ICustomTypeDescriptor)value);
			}
			else*/ if(value is System.ComponentModel.IBindingList)
				   {
					   this.Add((System.ComponentModel.IBindingList)value);
				   }
			throw new System.ArgumentException("Not supported type");
		}

		int System.Collections.IList.Add(object value)
		{
			//throw new System.ArgumentException("Not supported type");
			return Add(value);
		}

		public void Clear()
		{
			Lists.Clear();
			Reset();
		}
		public void Reset()
		{
			if(this.ListChanged!=null)
			{
				this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.Reset, -1));
			}
		}

		public bool Contains(object value)
		{
			foreach(System.Collections.IList list in this.Lists)
			{
				if(list.Contains(value))
				{
					return true;
				}
			}
			return false;
		}

		public void CopyTo(System.Array array, int index)
		{
			int count = 0;
			foreach(System.Collections.IList list in this.Lists)
			{
				list.CopyTo(array, count + index);
				count += list.Count;
			}
		}

		public int Count
		{
			get
			{
				int count = 0;
				foreach(System.Collections.IList list in this.Lists)
				{
					count += list.Count;
				}
				return count;
			}
		}

		private class ListsEnumerator:System.Collections.IEnumerator
		{
			public ListsEnumerator(System.Collections.ArrayList lists)
			{
				this.Lists = lists;
				this.itemi= -1;
				this.listi = 0;
			}
			public System.Collections.ArrayList Lists;
			public int itemi;
			public int listi;

			#region IEnumerator Members

			public void Reset()
			{
				this.itemi= -1;
				this.listi = 0;
			}

			public object Current
			{
				get
				{
					System.Collections.IList list = this.Lists[listi] as System.Collections.IList;
					return list[itemi];
				}
			}

			public bool MoveNext()
			{
				System.Collections.IList list = null;
				if(listi < this.Lists.Count)
				{
					list = this.Lists[listi] as System.Collections.IList;
					itemi++;
					if(itemi < list.Count)
					{
						return true;
					}
					else
					{
						listi++;
						if(listi < this.Lists.Count)
						{
							itemi = -1;
							return MoveNext();
						}
					}
				}
				return false;
			}

			#endregion
		}

		public System.Collections.IEnumerator GetEnumerator()
		{
			return new ListsEnumerator(this.Lists);
		}

		public int IndexOf(object value)
		{
			int index = 0;
			foreach(System.Collections.IList list in this.Lists)
			{
				int i = list.IndexOf(value);
				if(i<0)
				{
					index += list.Count;
				}
				else
				{
					return index + i;
				}
			}
			return -1;
		}

		public void Insert(int index, object value)
		{
			throw new System.NotSupportedException();
		}

		public bool IsSynchronized
		{
			get
			{
				return Lists.IsSynchronized;
			}
		}
 

		public object this[int index]
		{
			get
			{
				int c = 0;
				foreach(System.Collections.IList list in this.Lists)
				{
					int c1 = c + list.Count;
					if(c <= index && index < c1)
					{
						return list[index-c];
					}
					else
					{
						c = c1;
					}
				}
				throw new System.ArgumentOutOfRangeException();
			}
			set
			{
				int c = 0;
				foreach(System.Collections.IList list in this.Lists)
				{
					int c1 = c + list.Count;
					if(c <= index && index < c1)
					{
						list[index-c] = value;
						return;
					}
					else
					{
						c = c1;
					}
				}
				throw new System.ArgumentOutOfRangeException();
			}		}

		public void Remove(object value)
		{
			int i = this.IndexOf(value);
			if(i>=0)
			{
				this.RemoveAt(i);
			}
		}

		public void RemoveAt(int index)
		{
			int c = 0;
			foreach(System.Collections.IList list in this.Lists)
			{
				int c1 = c + list.Count;
				if(c <= index && index < c1)
				{
					list.RemoveAt(index-c);
					if(this.ListChanged!=null)
					{
						this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemDeleted, index));
					}
					return;
				}
				else
				{
					c = c1;
				}
			}
			throw new System.ArgumentOutOfRangeException();
		}

		public int Find(System.ComponentModel.PropertyDescriptor property, object key)
		{
			int index = 0;
			foreach(System.Collections.IList list in this.Lists)
			{
				System.ComponentModel.ITypedList list3 = list as System.ComponentModel.ITypedList;
				System.ComponentModel.IBindingList list2 = list as System.ComponentModel.IBindingList;
				System.ComponentModel.PropertyDescriptor property2 = null;
				if(list2!=null && list3!=null && (property2 = list3.GetItemProperties(null)[property.Name])!=null)
				{
					int i = list2.Find(property2, key);
					if(i<0)
					{
						index += list.Count;
					}
					else
					{
						return index + i;
					}
				}
				else
				{
					for (int i=0; i < list.Count; i++)
					{
						if (key.Equals(property.GetValue(list[i])))
						{
							return index + i;
						}
					}
					index += list.Count;
				}
			}
			return -1;
		}

		#region IBindingList Members

		public void AddIndex(System.ComponentModel.PropertyDescriptor property)
		{
			// TODO:  Add DataView.AddIndex implementation
		}

		public bool AllowNew
		{
			get
			{
				// TODO:  Add DataView.AllowNew getter implementation
				return false;
			}
		}

		public void ApplySort(System.ComponentModel.PropertyDescriptor property, System.ComponentModel.ListSortDirection direction)
		{
			// TODO:  Add DataView.ApplySort implementation
		}

		public System.ComponentModel.PropertyDescriptor SortProperty
		{
			get
			{
				// TODO:  Add DataView.SortProperty getter implementation
				return null;
			}
		}

		public bool SupportsSorting
		{
			get
			{
				// TODO:  Add DataView.SupportsSorting getter implementation
				return false;
			}
		}

		public bool IsSorted
		{
			get
			{
				// TODO:  Add DataView.IsSorted getter implementation
				return false;
			}
		}

		public bool AllowRemove
		{
			get
			{
				// TODO:  Add DataView.AllowRemove getter implementation
				return true;
			}
		}

		public bool SupportsSearching
		{
			get
			{
				// TODO:  Add DataView.SupportsSearching getter implementation
				return true;
			}
		}

		public System.ComponentModel.ListSortDirection SortDirection
		{
			get
			{
				// TODO:  Add DataView.SortDirection getter implementation
				return System.ComponentModel.ListSortDirection.Ascending;
			}
		}

		public event System.ComponentModel.ListChangedEventHandler ListChanged;

		public bool SupportsChangeNotification
		{
			get
			{
				// TODO:  Add DataView.SupportsChangeNotification getter implementation
				return true;
			}
		}

		public void RemoveSort()
		{
			// TODO:  Add DataView.RemoveSort implementation
		}

		public object AddNew()
		{
			// TODO:  Add DataView.AddNew implementation
			return null;
		}

		public bool AllowEdit
		{
			get
			{
				// TODO:  Add DataView.AllowEdit getter implementation
				return false;
			}
		}

		public void RemoveIndex(System.ComponentModel.PropertyDescriptor property)
		{
			// TODO:  Add DataView.RemoveIndex implementation
		}

		#endregion

		#region IList Members

		public bool IsReadOnly
		{
			get
			{
				// TODO:  Add DataView.IsReadOnly getter implementation
				return false;
			}
		}

		public bool IsFixedSize
		{
			get
			{
				// TODO:  Add DataView.IsFixedSize getter implementation
				return false;
			}
		}

		#endregion

		#region ICollection Members

		public object SyncRoot
		{
			get
			{
				// TODO:  Add DataView.SyncRoot getter implementation
				return Lists.SyncRoot;
			}
		}

		#endregion

		#region ISupportInitialize Members

		public void BeginInit()
		{
			// TODO:  Add DataView.BeginInit implementation
		}

		public void EndInit()
		{
			// TODO:  Add DataView.EndInit implementation
		}

		#endregion

		protected System.ComponentModel.PropertyDescriptorCollection m_SelectedColumns = new System.ComponentModel.PropertyDescriptorCollection(new System.ComponentModel.PropertyDescriptor[0]);

		public string[] FieldNames
		{
			get
			{
				string [] names = new string[m_SelectedColumns.Count];
				for(int i=0;i<m_SelectedColumns.Count;i++)
				{
					names[i] = m_SelectedColumns[i].Name;
				}
				return names;
			}
			set
			{
				m_SelectedColumns.Clear();
				for(int i=0;i<value.Length;i++)
				{
					m_SelectedColumns.Add(new Cerebrum.Data.DataItemPropertyDescriptor(value[i]));
				}
			}
		}

		public string SelectedColumns
		{
			get
			{
				return System.String.Join(",", FieldNames);
			}
			set
			{
				FieldNames = value.Split(new char[]{','});
			}
		}

		#region ITypedList Members

		public System.ComponentModel.PropertyDescriptorCollection GetItemProperties(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			/*if(Lists.Count>0)
			{
			  DataItemView rv = Lists[0] as DataItemView;
			  return rv.GetProperties(null);
			}*/
			return m_SelectedColumns;
		}

		public string GetListName(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			return System.String.Empty;
		}

		#endregion

		private void DataView_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
		{
			this.Reset();
		}

		public static System.ComponentModel.IBindingList FromBindingList(System.ComponentModel.IBindingList dv, string key, string caption, object keyvalue, string description)
		{
			Cerebrum.Data.DataListUnion du = new Cerebrum.Data.DataListUnion();
			//Cerebrum.Data.DataItemUnion du = new Cerebrum.Data.DataItemUnion();
			if(key==caption)
			{
				du.FieldNames = new string[] {key};
			}
			else
			{
				du.FieldNames = new string[] {key, caption};
			}

			System.Collections.Specialized.ListDictionary values = new System.Collections.Specialized.ListDictionary();
			values[key] = keyvalue;
			values[caption] = description;
			du.Add(new Cerebrum.Data.NameValueView(values));
			du.Add(dv);
			return du;
		}
	}

  public class DataItemView : System.ComponentModel.ICustomTypeDescriptor
  {
    protected internal System.ComponentModel.ICustomTypeDescriptor m_Item;
    public DataItemView(System.ComponentModel.ICustomTypeDescriptor item)
    {
      m_Item = item;
    }

    public override bool Equals(object obj)
    {
      if(base.Equals(obj))
      {
        return true;
      }
      else
      {
        if(obj is DataItemView)
        {
          return this.m_Item.Equals((obj as DataItemView).m_Item);
        }
        else
          return false;
      }
    }

    public override int GetHashCode()
    {
      return this.m_Item.GetHashCode();
    }

    public System.ComponentModel.AttributeCollection GetAttributes()
    {
      return new System.ComponentModel.AttributeCollection(null);
    }

    public string GetClassName()
    {
      return null;
    }

    public string GetComponentName()
    {
      return null;
    }

    public System.ComponentModel.TypeConverter GetConverter()
    {
      return null;
    }

    public System.ComponentModel.EventDescriptor GetDefaultEvent()
    {
      return null;
    }

    public System.ComponentModel.PropertyDescriptor GetDefaultProperty()
    {
      return null;
    }

    public object GetEditor(System.Type editorBaseType)
    {
      return null;
    }

    public System.ComponentModel.EventDescriptorCollection GetEvents(System.Attribute[] attributes)
    {
      return new System.ComponentModel.EventDescriptorCollection(null);
    }

    public System.ComponentModel.EventDescriptorCollection GetEvents()
    {
      return new System.ComponentModel.EventDescriptorCollection(null);
    }

    public System.ComponentModel.PropertyDescriptorCollection GetProperties(System.Attribute[] attributes)
    {
      System.ComponentModel.PropertyDescriptorCollection psItems = m_Item.GetProperties();
      System.ComponentModel.PropertyDescriptor[] ps = new System.ComponentModel.PropertyDescriptor[psItems.Count];
      int i;
      for (i=0; i < psItems.Count; i++)
      {
        ps[i] = new Cerebrum.Data.DataItemPropertyDescriptor(psItems[i].Name);
      }

      return new System.ComponentModel.PropertyDescriptorCollection(ps);
    }

    public System.ComponentModel.PropertyDescriptorCollection GetProperties()
    {
      return GetProperties(null);
    }

    public object GetPropertyOwner(System.ComponentModel.PropertyDescriptor pd)
    {
      return this;
    }

    public object this[string name]
    {
      get
      {
        System.ComponentModel.PropertyDescriptorCollection ps = this.m_Item.GetProperties();
        return ps[name].GetValue(this);
      }
      set
      {
        System.ComponentModel.PropertyDescriptorCollection ps = this.m_Item.GetProperties();
        ps[name].SetValue(this, value);
      }
    }
  }

  public class DataItemPropertyDescriptor : System.ComponentModel.PropertyDescriptor
  {
    private string MappingName;
    public DataItemPropertyDescriptor(string name) : base(name, null)
    {
      MappingName = name;
    }

    public override bool CanResetValue(object component)
    {
      return true;
    }

    public override object GetValue(object component)
    {
      Cerebrum.Data.DataItemView v0=component as Cerebrum.Data.DataItemView;
      if(v0!=null)
      {
        System.ComponentModel.PropertyDescriptorCollection ps = v0.m_Item.GetProperties();
		  return /*ps==null?System.Convert.DBNull:*/(ps[MappingName].GetValue(v0.m_Item));
      }
      System.ComponentModel.ICustomTypeDescriptor v1=component as System.ComponentModel.ICustomTypeDescriptor;
      if(v1!=null)
      {
        System.ComponentModel.PropertyDescriptorCollection ps = v1.GetProperties();
        return /*ps==null?System.Convert.DBNull:*/(ps[MappingName].GetValue(v1));
      }
      return System.Convert.DBNull;
    }

    public override void ResetValue(object component)
    {
      Cerebrum.Data.DataItemView v0=component as Cerebrum.Data.DataItemView;
      if(v0!=null)
      {
        System.ComponentModel.PropertyDescriptorCollection ps = v0.m_Item.GetProperties();
        ps[MappingName].SetValue(v0.m_Item, System.Convert.DBNull);
      }
      System.ComponentModel.ICustomTypeDescriptor v1=component as System.ComponentModel.ICustomTypeDescriptor;
      if(v1!=null)
      {
        System.ComponentModel.PropertyDescriptorCollection ps = v1.GetProperties();
        ps[MappingName].SetValue(v1, System.Convert.DBNull);
      }
    }

    public override void SetValue(object component, object value)
    {
      Cerebrum.Data.DataItemView v0=component as Cerebrum.Data.DataItemView;
      if(v0!=null)
      {
        System.ComponentModel.PropertyDescriptorCollection ps = v0.m_Item.GetProperties();
        ps[MappingName].SetValue(v0.m_Item, value);
      }
      System.ComponentModel.ICustomTypeDescriptor v1=component as System.ComponentModel.ICustomTypeDescriptor;
      if(v1!=null)
      {
        System.ComponentModel.PropertyDescriptorCollection ps = v1.GetProperties();
        ps[MappingName].SetValue(v1, value);
      }
    }

    public override bool ShouldSerializeValue(object component)
    {
      return false;
    }

    public override System.Type ComponentType
    {
      get
      {
        return typeof(object); //typeof(Cerebrum.Data.DataItemView);
      }
    }

    public override bool IsBrowsable
    {
      get
      {
        return true;
      }
    }

    public override bool IsReadOnly
    {
      get
      {
        return false;
      }
    }

    public override System.Type PropertyType
    {
      get
      {
        return typeof(object);
      }
    }
  }

}
