// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace Cerebrum.Data
{
	public class ctlDataSourceBrowser : System.Windows.Forms.UserControl
	{
		private Cerebrum.Data.IUpdateDS m_UpdateDS = null;

		public ctlDataSourceBrowser()
		{
			components = null;
			// This call is required by the Win Form Designer.
			InitializeComponent();
		}
		public void Initialize(Cerebrum.Data.IUpdateDS updateDS, System.ComponentModel.IBindingList source, Cerebrum.Data.IDataContext tableMappings, bool strictMappings)
		{
			DataSource = source;
			TableMappings = tableMappings;
			m_UpdateDS = updateDS;

			System.ComponentModel.PropertyDescriptorCollection prpsTables = ((System.ComponentModel.ITypedList)DataSource).GetItemProperties(null);
			foreach (System.ComponentModel.PropertyDescriptor tableDesc in prpsTables)
			{
				//System.ComponentModel.IBindingList table = tableDesc.GetValue(DataSource[0]) as System.ComponentModel.IBindingList;
				Cerebrum.Data.IDataTableInfo mp = TableMappings==null?null:TableMappings.GetTable(tableDesc.Name);

				if(mp!=null && (mp.Browsable || !strictMappings))
				{
					string dispName = mp.DisplayName;
					lstDataSetTables.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem((dispName!=null && dispName.Length>0)?dispName:tableDesc.Name, tableDesc.Name));
				}
				else if(!strictMappings)
				{
					lstDataSetTables.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem(tableDesc.Name, tableDesc.Name));
				}
			}

		}

		// Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (! (components == null))
				{
					components.Dispose();
				}

			}
			base.Dispose(disposing);
		}

		#region " Windows Form Designer generated code "
		// Required by the Windows Form Designer
		private System.ComponentModel.Container components;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel pnlTableBrowsers;
		private System.Windows.Forms.ListBox lstDataSetTables;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.  
		// Do not modify it using the code editor.
		private void InitializeComponent()
		{
			this.lstDataSetTables = new System.Windows.Forms.ListBox();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.pnlTableBrowsers = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// lstDataSetTables
			// 
			this.lstDataSetTables.Dock = System.Windows.Forms.DockStyle.Left;
			this.lstDataSetTables.IntegralHeight = false;
			this.lstDataSetTables.Location = new System.Drawing.Point(0, 0);
			this.lstDataSetTables.Name = "lstDataSetTables";
			this.lstDataSetTables.Size = new System.Drawing.Size(106, 413);
			this.lstDataSetTables.TabIndex = 0;
			this.lstDataSetTables.SelectedIndexChanged += new System.EventHandler(this.lstDataSetTables_SelectedIndexChanged);
			// 
			// splitter1
			// 
			this.splitter1.BackColor = System.Drawing.Color.Turquoise;
			this.splitter1.Location = new System.Drawing.Point(106, 0);
			this.splitter1.MinExtra = 512;
			this.splitter1.MinSize = 102;
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 413);
			this.splitter1.TabIndex = 1;
			this.splitter1.TabStop = false;
			// 
			// pnlTableBrowsers
			// 
			this.pnlTableBrowsers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlTableBrowsers.Location = new System.Drawing.Point(109, 0);
			this.pnlTableBrowsers.Name = "pnlTableBrowsers";
			this.pnlTableBrowsers.Size = new System.Drawing.Size(513, 413);
			this.pnlTableBrowsers.TabIndex = 2;
			// 
			// ctlDataSourceBrowser
			// 
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.Controls.Add(this.pnlTableBrowsers);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.lstDataSetTables);
			this.Name = "ctlDataSourceBrowser";
			this.Size = new System.Drawing.Size(622, 413);
			this.ResumeLayout(false);

		}

		#endregion
		public System.ComponentModel.IBindingList DataSource;
		public Cerebrum.Data.IDataContext TableMappings;
		
		public event System.EventHandler AcceptClick;
		public event System.EventHandler CancelClick;

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			if(CancelClick!=null)
				CancelClick(this, e);
		}

		protected void btnAccept_Click(object sender, System.EventArgs e)
		{
			if(AcceptClick!=null)
				AcceptClick(this, e);
		}

		private System.Collections.Hashtable m_TableBrowsers = new System.Collections.Hashtable();

		private void lstDataSetTables_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ctlDataTableBrowser ctl =null;

			if (lstDataSetTables.SelectedIndex >= 0)
			{
				string tblname = (lstDataSetTables.SelectedItem as Cerebrum.Windows.Forms.DropDownStringItem).Value as string;
				//System.ComponentModel.IBindingList tbl = (lstDataSetTables.SelectedItem as Cerebrum.Data.DropDownStringItem).Value as System.ComponentModel.IBindingList;
				ctl = this.m_TableBrowsers[tblname] as ctlDataTableBrowser;
				if(ctl==null)
				{
					ctl = new ctlDataTableBrowser();
					ctl.UpdateDS = this.m_UpdateDS;
					ctl.DataSource = this.DataSource;
					/*foreach(System.Data.DataTable tblt in this.DataSource.Tables)
					{
						ctl.BrowserTables.AddTableInstance(tblt.TableName, 0, tblt);
					}*/
					ctl.DataContext = this.TableMappings;
					ctl.DataMember = tblname;
					ctl.Text = ctl.DataMember;
					ctl.AcceptClick += new System.EventHandler(btnAccept_Click);
					ctl.CancelClick += new System.EventHandler(btnCancel_Click);
					pnlTableBrowsers.Controls.Add(ctl);
					ctl.Render();
					ctl.Dock = System.Windows.Forms.DockStyle.Fill;
					this.m_TableBrowsers[tblname] = ctl;
				}
			}

			ctl.Visible = true;

			foreach(System.Windows.Forms.Control c0 in this.pnlTableBrowsers.Controls)
			{
				if(c0!=ctl && c0 is ctlDataTableBrowser)
				{
					(c0 as ctlDataTableBrowser).EndCurrentEdit();
					c0.Visible = false;
				}
			}

			/*while(pnlTableBrowsers.Controls.Count > 0)
			{
				System.Windows.Forms.Control ctl0 = pnlTableBrowsers.Controls[0];
				if(ctl0 is ctlDataTableBrowser)
				{
					(ctl0 as ctlDataTableBrowser).EndCurrentEdit();
				}
				pnlTableBrowsers.Controls.Remove(ctl0);
				ctl0.Dispose();
			}*/

		
		}

		public virtual void UpdateDataSource(System.ComponentModel.IBindingList updatedDataSet)
		{
			if(CancelClick!=null)
				CancelClick(this, System.EventArgs.Empty);
		}

		public virtual void RejectDataSource(System.ComponentModel.IBindingList updatedDataSet)
		{
			if(AcceptClick!=null)
				AcceptClick(this, System.EventArgs.Empty);
		}

	}

}