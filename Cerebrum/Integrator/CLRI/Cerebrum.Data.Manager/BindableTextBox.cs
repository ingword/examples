// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Forms
{
	/// <summary>
	/// Summary description for BindableTextBox.
	/// </summary>
	public class BindableTextBox : System.Windows.Forms.TextBox
	{

		public BindableTextBox()
		{
			// TODO: Add any initialization after the InitializeComponent call
			this.m_ValueType = null;

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/*protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}*/

		private System.Type m_ValueType;
		public System.Type ValueType
		{
			get
			{
				return this.m_ValueType;
			}
			set
			{
				this.m_ValueType = value;
			}
		}
		public object Value
		{
			get
			{
				string text = this.Text;
				if(text==string.Empty)
				{
					return System.DBNull.Value;
				}
				else if(m_ValueType!=null)
				{
					return System.ComponentModel.TypeDescriptor.GetConverter(this.m_ValueType).ConvertFromString(text);
				}
				else
				{
					return text;
				}
			}
			set
			{
				/*if(value!=null && !Convert.IsDBNull(value))
				{
					m_ValueType = value.GetType();
				}
				else
				{
					m_ValueType = null;
				}*/
				this.Text = Convert.ToString(value);
			}
		}
	}
}
