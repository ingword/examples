// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Cerebrum.Data
{
    public class DataGridDatePickerColumn : Cerebrum.Data.DataGridColumnStyle
    // UI Constants    {
        private System.Windows.Forms.DateTimePicker picker;
        // Used to track editing state
        private Object oldValue = null;
        private bool inEdit = false;
		/// <summary>        ///     Create a new column        /// </summary>        public DataGridDatePickerColumn()
        {
            picker = new System.Windows.Forms.DateTimePicker();
            picker.Visible = false;
            picker.Format = DateTimePickerFormat.Short;
            picker.ShowCheckBox = true;
        }

         //----------------------------------------------------------         //Properties to allow us to set up the datasource for the DatePicker         //----------------------------------------------------------
         //----------------------------------------------------------         //Methods overridden from DataGridColumnStyleStyle         //----------------------------------------------------------
        protected override void Abort(int rowNum)
        {
            RollBack();
            HideDatePicker();
            EndEdit();
        }

        protected override bool Commit(System.Windows.Forms.CurrencyManager dataSource, int rowNum)
        {
            HideDatePicker();
             //If we are not in an edit then simply return
            if ((! inEdit))
            {
                return true;
            }

            try
            {
                Object value;
                if (! picker.Checked)
                {
                    value = Convert.DBNull;
                }

                else
                {
                    value = DateTrunc(picker.Value);
                    if (value == null ||  (NullText.Equals(value)))
                    {
                        value = Convert.DBNull;
                    }

                }

                if (! value.Equals(oldValue))
                {
                    SetColumnValueAtRow(dataSource, rowNum, value);
                }

            }

            catch //(System.Exception ex)
            {
                RollBack();
                return false;
            }

            EndEdit();
            return true;
        }

        protected override void ConcedeFocus()
        {
            picker.Visible = false;
        }

        protected override void Edit(System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
        {
            if ((rowNum &  1) ==  0)
            {
                picker.BackColor = this.mBackColor;
            }

            else
            {
                picker.BackColor = this.mAlternatingBackColor;
            }

            // picker.Text = ""
            System.Drawing.Rectangle originalBounds = bounds;
            oldValue = GetColumnValueAtRow(source, rowNum);
            try
            {
                if (oldValue ==  null || oldValue is System.DBNull)
                {
                    picker.Checked = false;
                }
                else
                {
                    picker.Checked = true;
                    picker.Value = DateTrunc(Convert.ToDateTime(oldValue));
                }

            }

            catch //(System.Exception ex)
            {
            }

            try
            {
                if ( instantText != null)
                {
                    picker.Value = DateTrunc(Convert.ToDateTime(instantText));
                    oldValue = picker.Value;
                }

            }

            catch //(System.Exception ex)
            {
            }

            //  WARNING there are i want textbox
            if ((cellIsVisible) &&  ! (readOnly ||  this.ReadOnly))
            {
                bounds.Offset(xMargin, yMargin);
                bounds.Width -= xMargin* 2;
                bounds.Height -= yMargin;
                picker.Bounds = bounds;
                picker.Visible = true;
            }

            else
            {
                picker.Bounds = originalBounds;
                picker.Visible = false;
            }

            picker.RightToLeft = this.DataGridTableStyle.DataGrid.RightToLeft;
            picker.Focus();
            // If (instantText = Nothing) Then
            //     picker.SelectAll()
            // Else
            //     Dim mend As Integer = picker.Text.Length
            //     picker.Select(mend, 0)
            // End If
            if ((picker.Visible))
            {
                this.DataGridTableStyle.DataGrid.Invalidate(originalBounds);
            }

            inEdit = true;
        }

        protected override int GetMinimumHeight()
        // //Set the minimum height to the height of the DatePicker
        {
            return picker.PreferredHeight+ yMargin;
        }

        protected override void SetDataGridInColumn(System.Windows.Forms.DataGrid value)
        {
            base.SetDataGridInColumn(value);
            if (picker.Parent !=  null &&  picker.Parent !=  value)
            {
                picker.Parent.Controls.Remove(picker);
            }

            if (value != null)
            {
                value.Controls.Add(picker);
            }

        }

        protected override void UpdateUI(System.Windows.Forms.CurrencyManager source, int rowNum, string instantText)
        {
            picker.Text = GetText(GetColumnValueAtRow(source, rowNum));
            if ((instantText != null))
            {
                picker.Text = instantText;
            }

        }

         //----------------------------------------------------------
         //Helper Methods 
         //----------------------------------------------------------
        private void EndEdit()
        {
            inEdit = false;
            Invalidate();
        }

        protected override string GetText(Object value)
        {
            if (value == null ||  value is System.DBNull)
            {
                return NullText;
            }

            try
            {
                return Convert.ToDateTime(value).ToShortDateString();
            }

            catch //(System.Exception ex)
            {
                return ("####");
            }

        }

        private void HideDatePicker()
        {
            if ((picker.Focused))
            {
                this.DataGridTableStyle.DataGrid.Focus();
            }

            picker.Visible = false;
        }

        private void RollBack()
        {
            try
            {
                picker.Value = DateTrunc(Convert.ToDateTime(oldValue));
            }

            catch //(System.Exception ex)
            {
            }

        }

		private static System.DateTime DateTrunc(System.DateTime value)
		{
			return new System.DateTime(value.Year, value.Month, value.Day);
		}

    }

}

