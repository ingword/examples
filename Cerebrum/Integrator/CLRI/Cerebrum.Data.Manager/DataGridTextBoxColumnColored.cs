// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Windows.Forms;
namespace Cerebrum.Data
{
	//  Cerebrum.Data.DataGridTextBoxColumnColored
	public class DataGridTextBoxColumnColored : System.Windows.Forms.DataGridTextBoxColumn 
	{
		protected System.Drawing.Color mBackColor;
		protected System.Drawing.Color mAlternatingBackColor;
		protected System.Drawing.Color mNullBackColor;
		protected System.Drawing.Color mNullAlternatingBackColor;
		protected int yMargin = 1;
		protected char mPasswordChar = '\0';
		
		public DataGridTextBoxColumnColored()
		{
			this.TextBox.VisibleChanged += new EventHandler(TextBox_VisibleChanged);
		}

		public char PasswordChar
		{
			get
			{
				return mPasswordChar;
			}			set			{
				mPasswordChar = value;
			}
		}

		public System.Drawing.Color BackColor
		{
			get
			{
				return mBackColor;
			}			set			{
				mBackColor = value;
			}
		}

		public System.Drawing.Color AlternatingBackColor
		{
			get
			{
				return mAlternatingBackColor;
			}			set
			{
				mAlternatingBackColor = value;
			}
		}

		public System.Drawing.Color NullBackColor
		{
			get
			{
				return mNullBackColor;
			}			set			{
				mNullBackColor = value;
			}
		}

		public System.Drawing.Color NullAlternatingBackColor
		{
			get
			{
				return mNullAlternatingBackColor;
			}			set
			{
				mNullAlternatingBackColor = value;
			}
		}

		#region " DataColumn Overrides "
		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum)
		{
			Paint(g, bounds, source, rowNum, false);
		}

		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, bool alignToRight)
		{
			Brush backBrush = new SolidBrush(this.DataGridTableStyle.DataGrid.BackColor);
			Brush foreBrush = new SolidBrush(this.DataGridTableStyle.DataGrid.ForeColor);
			object v = GetColumnValueAtRow(source, rowNum);
			PaintText(g, bounds, GetText(v), (v == null || v is System.DBNull), rowNum, backBrush, foreBrush, alignToRight, this.DataGridTableStyle.DataGrid.IsSelected(rowNum));
		}

		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
		{
			bool bs = false;
			try
			{
				bs = this.DataGridTableStyle.DataGrid.IsSelected(rowNum);
			}
			catch //(System.Exception ex)
			{
			}

			object v = GetColumnValueAtRow(source, rowNum);
			PaintText(g, bounds, GetText(v), (v == null || v is System.DBNull), rowNum, backBrush, foreBrush, alignToRight, bs);
		}

		#endregion

		protected virtual void PaintText(Graphics g, Rectangle textBounds, string text, bool valueIsNull, int rowNum, Brush backBrush, Brush foreBrush, bool alignToRight, bool selectedRow)
		{
			if (! selectedRow)
			{
				if(!valueIsNull)
				{
					if ((rowNum & 1) ==  0)
					{
						backBrush = new SolidBrush(this.mBackColor);
					}

					else
					{
						backBrush = new SolidBrush(this.mAlternatingBackColor);
					}
				}
				else
				{
					if ((rowNum & 1) ==  0)
					{
						backBrush = new SolidBrush(this.mNullBackColor);
					}

					else
					{
						backBrush = new SolidBrush(this.mNullAlternatingBackColor);
					}
				}
			}

			Rectangle rect = textBounds;
			StringFormat format = new StringFormat();
			if (alignToRight)
			{
				format.FormatFlags = format.FormatFlags |  StringFormatFlags.DirectionRightToLeft;
			}

			if (this.Alignment ==  HorizontalAlignment.Left)
			{
				format.Alignment = StringAlignment.Near;
			}
			else if (this.Alignment ==  HorizontalAlignment.Center)
			{
				format.Alignment = StringAlignment.Center;
			}
			else
			{
				format.Alignment = StringAlignment.Far;
			}

			format.FormatFlags = format.FormatFlags | StringFormatFlags.NoWrap;
			g.FillRectangle(backBrush, rect);
			rect.Offset(0, yMargin);
			rect.Height -= yMargin;
			g.DrawString(text, this.DataGridTableStyle.DataGrid.Font, foreBrush, rect, format);
			format.Dispose();
		}

		protected string GetText(object value)
		{
			if (value == null || value is System.DBNull)
			{
				return NullText;
			}

			try
			{
				string s = Convert.ToString(value);
				if(this.mPasswordChar!='\0')
				{
					s = new String(this.mPasswordChar, s.Length);
				}
				return s;
			}

			catch //(System.Exception ex)
			{
				return ("####");
			}
		}

		private void TextBox_VisibleChanged(object sender, EventArgs e)
		{
			if(this.mPasswordChar!='\0')
			{
				this.TextBox.PasswordChar = mPasswordChar;
				this.TextBox.Multiline = false;
			}
		}
	}

}

