// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
namespace Cerebrum.Data
{
	public class ctlDataTableBrowser : System.Windows.Forms.UserControl
	{
		public ctlDataTableBrowser()
		{
			DataContext = null;

			components = null;
			// This call is required by the Win Form Designer.
			InitializeComponent();
			// TODO: Add any initialization after the InitializeComponent() call
			dsb_dgBrowse.Dock = System.Windows.Forms.DockStyle.Fill;
			dsb_pnlFieldsList.Dock = System.Windows.Forms.DockStyle.Fill;
		}

		// Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (! (components == null))
				{
					components.Dispose();
				}

			}
			base.Dispose(disposing);
		}

		#region " Windows Form Designer generated code "
		// Required by the Windows Form Designer
		private System.ComponentModel.Container components;
		private System.Windows.Forms.Button dsb_btnReturn;
		private System.Windows.Forms.Panel dsb_pnlBrowse;
		private Cerebrum.Data.DataGrid dsb_dgBrowse;
		private System.Windows.Forms.Panel dsb_pnlPrimary;
		private System.Windows.Forms.Button dsb_btnBrowse;
		private System.Windows.Forms.Button dsb_btnCancel;
		private System.Windows.Forms.Button dsb_btnApply;
		private System.Windows.Forms.Button dsb_btnAccept;
		private System.Windows.Forms.Panel dsb_pnlControl;
		private System.Windows.Forms.Button dsb_btnRevert;
		private System.Windows.Forms.Button dsb_btnDelete;
		private System.Windows.Forms.Button dsb_btnInsert;
		private System.Windows.Forms.Button dsb_btnNavLast;
		private System.Windows.Forms.Button dsb_btnNavNext;
		private System.Windows.Forms.Label dsb_lblNavLocation;
		private System.Windows.Forms.Button dsb_btnNavPrev;
		private System.Windows.Forms.Button dsb_btnNavFirst;
		private System.Windows.Forms.Panel dsb_pnlOperation;
		private System.Windows.Forms.Panel dsb_pnlNavigation;
		private System.Windows.Forms.Button dsb_btnClose;
		private System.Windows.Forms.Panel dsb_pnlFieldsList;
		// NOTE: The following procedure is required by the Windows Form Designer
		// It can be modified using the Windows Form Designer.  
		// Do not modify it using the code editor.
		private void InitializeComponent()
		{
			this.dsb_pnlFieldsList = new System.Windows.Forms.Panel();
			this.dsb_pnlOperation = new System.Windows.Forms.Panel();
			this.dsb_btnInsert = new System.Windows.Forms.Button();
			this.dsb_btnDelete = new System.Windows.Forms.Button();
			this.dsb_btnRevert = new System.Windows.Forms.Button();
			this.dsb_btnNavFirst = new System.Windows.Forms.Button();
			this.dsb_btnNavPrev = new System.Windows.Forms.Button();
			this.dsb_lblNavLocation = new System.Windows.Forms.Label();
			this.dsb_btnNavNext = new System.Windows.Forms.Button();
			this.dsb_btnNavLast = new System.Windows.Forms.Button();
			this.dsb_btnApply = new System.Windows.Forms.Button();
			this.dsb_dgBrowse = new Cerebrum.Data.DataGrid();
			this.dsb_pnlControl = new System.Windows.Forms.Panel();
			this.dsb_btnClose = new System.Windows.Forms.Button();
			this.dsb_btnBrowse = new System.Windows.Forms.Button();
			this.dsb_btnCancel = new System.Windows.Forms.Button();
			this.dsb_btnAccept = new System.Windows.Forms.Button();
			this.dsb_pnlPrimary = new System.Windows.Forms.Panel();
			this.dsb_btnReturn = new System.Windows.Forms.Button();
			this.dsb_pnlBrowse = new System.Windows.Forms.Panel();
			this.dsb_pnlNavigation = new System.Windows.Forms.Panel();
			this.dsb_pnlOperation.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dsb_dgBrowse)).BeginInit();
			this.dsb_pnlControl.SuspendLayout();
			this.dsb_pnlPrimary.SuspendLayout();
			this.dsb_pnlBrowse.SuspendLayout();
			this.dsb_pnlNavigation.SuspendLayout();
			this.SuspendLayout();
			// 
			// dsb_pnlFieldsList
			// 
			this.dsb_pnlFieldsList.AutoScroll = true;
			this.dsb_pnlFieldsList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.dsb_pnlFieldsList.Location = new System.Drawing.Point(0, 0);
			this.dsb_pnlFieldsList.Name = "dsb_pnlFieldsList";
			this.dsb_pnlFieldsList.Size = new System.Drawing.Size(456, 224);
			this.dsb_pnlFieldsList.TabIndex = 0;
			// 
			// dsb_pnlOperation
			// 
			this.dsb_pnlOperation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_pnlOperation.Controls.Add(this.dsb_btnInsert);
			this.dsb_pnlOperation.Controls.Add(this.dsb_btnDelete);
			this.dsb_pnlOperation.Controls.Add(this.dsb_btnRevert);
			this.dsb_pnlOperation.Location = new System.Drawing.Point(264, 272);
			this.dsb_pnlOperation.Name = "dsb_pnlOperation";
			this.dsb_pnlOperation.Size = new System.Drawing.Size(240, 24);
			this.dsb_pnlOperation.TabIndex = 2;
			// 
			// dsb_btnInsert
			// 
			this.dsb_btnInsert.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnInsert.Location = new System.Drawing.Point(0, 0);
			this.dsb_btnInsert.Name = "dsb_btnInsert";
			this.dsb_btnInsert.TabIndex = 5;
			this.dsb_btnInsert.Text = "&Add";
			this.dsb_btnInsert.Click += new System.EventHandler(this.dsb_btnInsert_Click);
			// 
			// dsb_btnDelete
			// 
			this.dsb_btnDelete.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnDelete.Location = new System.Drawing.Point(80, 0);
			this.dsb_btnDelete.Name = "dsb_btnDelete";
			this.dsb_btnDelete.TabIndex = 6;
			this.dsb_btnDelete.Text = "&Delete";
			this.dsb_btnDelete.Click += new System.EventHandler(this.dsb_btnDelete_Click);
			// 
			// dsb_btnRevert
			// 
			this.dsb_btnRevert.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnRevert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnRevert.Location = new System.Drawing.Point(160, 0);
			this.dsb_btnRevert.Name = "dsb_btnRevert";
			this.dsb_btnRevert.TabIndex = 7;
			this.dsb_btnRevert.Text = "&Revert";
			this.dsb_btnRevert.Click += new System.EventHandler(this.dsb_btnRevert_Click);
			// 
			// dsb_btnNavFirst
			// 
			this.dsb_btnNavFirst.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnNavFirst.Location = new System.Drawing.Point(0, 0);
			this.dsb_btnNavFirst.Name = "dsb_btnNavFirst";
			this.dsb_btnNavFirst.Size = new System.Drawing.Size(40, 23);
			this.dsb_btnNavFirst.TabIndex = 0;
			this.dsb_btnNavFirst.Text = "<<";
			this.dsb_btnNavFirst.Click += new System.EventHandler(this.dsb_btnNavFirst_Click);
			// 
			// dsb_btnNavPrev
			// 
			this.dsb_btnNavPrev.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnNavPrev.Location = new System.Drawing.Point(40, 0);
			this.dsb_btnNavPrev.Name = "dsb_btnNavPrev";
			this.dsb_btnNavPrev.Size = new System.Drawing.Size(35, 23);
			this.dsb_btnNavPrev.TabIndex = 1;
			this.dsb_btnNavPrev.Text = "<";
			this.dsb_btnNavPrev.Click += new System.EventHandler(this.dsb_btnNavPrev_Click);
			// 
			// dsb_lblNavLocation
			// 
			this.dsb_lblNavLocation.BackColor = System.Drawing.Color.White;
			this.dsb_lblNavLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_lblNavLocation.Location = new System.Drawing.Point(72, 0);
			this.dsb_lblNavLocation.Name = "dsb_lblNavLocation";
			this.dsb_lblNavLocation.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.dsb_lblNavLocation.Size = new System.Drawing.Size(95, 23);
			this.dsb_lblNavLocation.TabIndex = 2;
			this.dsb_lblNavLocation.Text = "No Records";
			this.dsb_lblNavLocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dsb_btnNavNext
			// 
			this.dsb_btnNavNext.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnNavNext.Location = new System.Drawing.Point(168, 0);
			this.dsb_btnNavNext.Name = "dsb_btnNavNext";
			this.dsb_btnNavNext.Size = new System.Drawing.Size(35, 23);
			this.dsb_btnNavNext.TabIndex = 3;
			this.dsb_btnNavNext.Text = ">";
			this.dsb_btnNavNext.Click += new System.EventHandler(this.dsb_btnNavNext_Click);
			// 
			// dsb_btnNavLast
			// 
			this.dsb_btnNavLast.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnNavLast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnNavLast.Location = new System.Drawing.Point(200, 0);
			this.dsb_btnNavLast.Name = "dsb_btnNavLast";
			this.dsb_btnNavLast.Size = new System.Drawing.Size(40, 23);
			this.dsb_btnNavLast.TabIndex = 4;
			this.dsb_btnNavLast.Text = ">>";
			this.dsb_btnNavLast.Click += new System.EventHandler(this.dsb_btnNavLast_Click);
			// 
			// dsb_btnApply
			// 
			this.dsb_btnApply.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnApply.Location = new System.Drawing.Point(336, 0);
			this.dsb_btnApply.Name = "dsb_btnApply";
			this.dsb_btnApply.TabIndex = 1;
			this.dsb_btnApply.Text = "Apply";
			this.dsb_btnApply.Click += new System.EventHandler(this.dsb_btnApply_Click);
			// 
			// dsb_dgBrowse
			// 
			this.dsb_dgBrowse.AllowNavigation = false;
			this.dsb_dgBrowse.AlternatingBackColor = System.Drawing.Color.Ivory;
			this.dsb_dgBrowse.BackColor = System.Drawing.Color.LightYellow;
			this.dsb_dgBrowse.BackgroundColor = System.Drawing.Color.SteelBlue;
			this.dsb_dgBrowse.CaptionBackColor = System.Drawing.Color.SlateGray;
			this.dsb_dgBrowse.CaptionFont = new System.Drawing.Font("Times New Roman", 12F);
			this.dsb_dgBrowse.DataMember = "";
			this.dsb_dgBrowse.GridLineColor = System.Drawing.Color.LightGray;
			this.dsb_dgBrowse.HeaderBackColor = System.Drawing.Color.PaleGoldenrod;
			this.dsb_dgBrowse.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dsb_dgBrowse.LinkColor = System.Drawing.Color.Blue;
			this.dsb_dgBrowse.Location = new System.Drawing.Point(0, 0);
			this.dsb_dgBrowse.Name = "dsb_dgBrowse";
			this.dsb_dgBrowse.ParentRowsBackColor = System.Drawing.Color.Aquamarine;
			this.dsb_dgBrowse.SelectionBackColor = System.Drawing.Color.Firebrick;
			this.dsb_dgBrowse.SelectionForeColor = System.Drawing.Color.White;
			this.dsb_dgBrowse.Size = new System.Drawing.Size(200, 100);
			this.dsb_dgBrowse.TabIndex = 1;
			this.dsb_dgBrowse.Visible = false;
			//this.dsb_dgBrowse.DoubleClick += new System.EventHandler(this.dsb_btnReturn_Click);
			// 
			// dsb_pnlControl
			// 
			this.dsb_pnlControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_pnlControl.Controls.Add(this.dsb_btnClose);
			this.dsb_pnlControl.Controls.Add(this.dsb_btnBrowse);
			this.dsb_pnlControl.Controls.Add(this.dsb_btnCancel);
			this.dsb_pnlControl.Controls.Add(this.dsb_btnApply);
			this.dsb_pnlControl.Controls.Add(this.dsb_btnAccept);
			this.dsb_pnlControl.Location = new System.Drawing.Point(8, 300);
			this.dsb_pnlControl.Name = "dsb_pnlControl";
			this.dsb_pnlControl.Size = new System.Drawing.Size(496, 24);
			this.dsb_pnlControl.TabIndex = 3;
			// 
			// dsb_btnClose
			// 
			this.dsb_btnClose.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.dsb_btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnClose.Location = new System.Drawing.Point(416, 0);
			this.dsb_btnClose.Name = "dsb_btnClose";
			this.dsb_btnClose.TabIndex = 4;
			this.dsb_btnClose.Text = "Close";
			this.dsb_btnClose.Click += new System.EventHandler(this.dsb_btnClose_Click);
			// 
			// dsb_btnBrowse
			// 
			this.dsb_btnBrowse.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnBrowse.Location = new System.Drawing.Point(0, 0);
			this.dsb_btnBrowse.Name = "dsb_btnBrowse";
			this.dsb_btnBrowse.TabIndex = 3;
			this.dsb_btnBrowse.Text = "&Browse";
			this.dsb_btnBrowse.Click += new System.EventHandler(this.dsb_btnBrowse_Click);
			// 
			// dsb_btnCancel
			// 
			this.dsb_btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.dsb_btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnCancel.Location = new System.Drawing.Point(416, 0);
			this.dsb_btnCancel.Name = "dsb_btnCancel";
			this.dsb_btnCancel.TabIndex = 2;
			this.dsb_btnCancel.Text = "&Cancel";
			this.dsb_btnCancel.Click += new System.EventHandler(this.dsb_btnCancel_Click);
			// 
			// dsb_btnAccept
			// 
			this.dsb_btnAccept.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.dsb_btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnAccept.Location = new System.Drawing.Point(256, 0);
			this.dsb_btnAccept.Name = "dsb_btnAccept";
			this.dsb_btnAccept.TabIndex = 0;
			this.dsb_btnAccept.Text = "&Ok";
			this.dsb_btnAccept.Click += new System.EventHandler(this.dsb_btnAccept_Click);
			// 
			// dsb_pnlPrimary
			// 
			this.dsb_pnlPrimary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_pnlPrimary.BackColor = System.Drawing.Color.MediumAquamarine;
			this.dsb_pnlPrimary.Controls.Add(this.dsb_dgBrowse);
			this.dsb_pnlPrimary.Controls.Add(this.dsb_pnlFieldsList);
			this.dsb_pnlPrimary.Location = new System.Drawing.Point(8, 8);
			this.dsb_pnlPrimary.Name = "dsb_pnlPrimary";
			this.dsb_pnlPrimary.Size = new System.Drawing.Size(496, 256);
			this.dsb_pnlPrimary.TabIndex = 0;
			// 
			// dsb_btnReturn
			// 
			this.dsb_btnReturn.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnReturn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnReturn.Location = new System.Drawing.Point(0, 0);
			this.dsb_btnReturn.Name = "dsb_btnReturn";
			this.dsb_btnReturn.TabIndex = 0;
			this.dsb_btnReturn.Text = "&Return";
			this.dsb_btnReturn.Click += new System.EventHandler(this.dsb_btnReturn_Click);
			// 
			// dsb_pnlBrowse
			// 
			this.dsb_pnlBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_pnlBrowse.Controls.Add(this.dsb_btnReturn);
			this.dsb_pnlBrowse.Location = new System.Drawing.Point(8, 300);
			this.dsb_pnlBrowse.Name = "dsb_pnlBrowse";
			this.dsb_pnlBrowse.Size = new System.Drawing.Size(488, 24);
			this.dsb_pnlBrowse.TabIndex = 4;
			this.dsb_pnlBrowse.Visible = false;
			// 
			// dsb_pnlNavigation
			// 
			this.dsb_pnlNavigation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_pnlNavigation.Controls.Add(this.dsb_btnNavFirst);
			this.dsb_pnlNavigation.Controls.Add(this.dsb_btnNavPrev);
			this.dsb_pnlNavigation.Controls.Add(this.dsb_lblNavLocation);
			this.dsb_pnlNavigation.Controls.Add(this.dsb_btnNavNext);
			this.dsb_pnlNavigation.Controls.Add(this.dsb_btnNavLast);
			this.dsb_pnlNavigation.Location = new System.Drawing.Point(8, 272);
			this.dsb_pnlNavigation.Name = "dsb_pnlNavigation";
			this.dsb_pnlNavigation.Size = new System.Drawing.Size(248, 24);
			this.dsb_pnlNavigation.TabIndex = 5;
			// 
			// ctlDataTableBrowser
			// 
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.Controls.Add(this.dsb_pnlNavigation);
			this.Controls.Add(this.dsb_pnlPrimary);
			this.Controls.Add(this.dsb_pnlOperation);
			this.Controls.Add(this.dsb_pnlControl);
			this.Controls.Add(this.dsb_pnlBrowse);
			this.Name = "ctlDataTableBrowser";
			this.Size = new System.Drawing.Size(512, 333);
			this.dsb_pnlOperation.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dsb_dgBrowse)).EndInit();
			this.dsb_pnlControl.ResumeLayout(false);
			this.dsb_pnlPrimary.ResumeLayout(false);
			this.dsb_pnlBrowse.ResumeLayout(false);
			this.dsb_pnlNavigation.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		public string DataMember;
		
		public System.ComponentModel.IBindingList DataSource;
		public Cerebrum.Data.IDataContext DataContext;
		public Cerebrum.Data.IUpdateDS UpdateDS = null;

		protected System.ComponentModel.IBindingList DataTableView;

		private	System.Windows.Forms.Control[] dsb_Controls;
		private	System.Windows.Forms.Control[] dsb_Labels;
			
		private int FieldsCount;
		public void Render()
		{
			try
			{
				dsb_dgBrowse.CaptionText = this.DataMember;

				System.ComponentModel.PropertyDescriptorCollection prpsTables = ((System.ComponentModel.ITypedList)DataSource).GetItemProperties(null);
				DataTableView = prpsTables[DataMember].GetValue(DataSource[0]) as System.ComponentModel.IBindingList;

				this.BindingContext[DataTableView].PositionChanged += new System.EventHandler(frmDataSetBrowser_PositionChanged);
				DataTableView.ListChanged += new ListChangedEventHandler(ctlDataTableBrowser_ListChanged);

				Cerebrum.Data.IDataContext constraints = this.DataContext;

				System.ComponentModel.PropertyDescriptorCollection prpsColumns = ((System.ComponentModel.ITypedList)DataTableView).GetItemProperties(null);
				FieldsCount = prpsColumns.Count;

				System.Windows.Forms.ComboBox dsb_cmbBox;
				Cerebrum.Data.Manager.Forms.BindableTextBox dsb_txtBox;
				System.Windows.Forms.Label dsb_lblBox;
				System.Windows.Forms.Button dsb_btnBox;
				dsb_pnlFieldsList.SuspendLayout();
			
				dsb_Controls = new System.Windows.Forms.Control[FieldsCount];
				dsb_Labels = new System.Windows.Forms.Control[FieldsCount];
			
				int ColumnIndex;
				int BrowseIndex;
				System.ComponentModel.IBindingList [] dvFiltered= new System.ComponentModel.IBindingList[FieldsCount];

				int GroupIndex;
				for(GroupIndex = 0, BrowseIndex = 0; GroupIndex < 2; GroupIndex++)
					for(ColumnIndex = 0; ColumnIndex < FieldsCount; ColumnIndex++)
					{
						System.ComponentModel.PropertyDescriptor Column = prpsColumns[ColumnIndex];

						Cerebrum.Data.IDataFieldInfo clmn = constraints==null?null:constraints.GetColumn(DataMember, Column.Name);
						if(((GroupIndex==0 && !Column.PropertyType.IsAbstract) || (GroupIndex==1 && typeof(System.ComponentModel.IBindingList).IsAssignableFrom(Column.PropertyType))) && (clmn==null || clmn.Browsable))
						{
							dsb_lblBox = new System.Windows.Forms.Label();
							dsb_Labels[ColumnIndex] = dsb_lblBox;

							dsb_lblBox.Location = new System.Drawing.Point(10, 10 + 33 * BrowseIndex + 3);
							dsb_lblBox.Text = Column.DisplayName;
							dsb_lblBox.Size = new System.Drawing.Size(157, 23);
							dsb_lblBox.TabIndex = BrowseIndex * 2;

							dsb_pnlFieldsList.Controls.Add(dsb_lblBox);

							if (clmn==null || (typeof(System.ComponentModel.IBindingList).IsAssignableFrom(Column.PropertyType) || clmn.RelatedTableName==null || clmn.RelatedFieldName==null || clmn.RelatedTableName.Length < 1 || clmn.RelatedFieldName.Length < 1))
							{
								if (typeof(System.ComponentModel.IBindingList).IsAssignableFrom(Column.PropertyType))
								{
									dsb_btnBox = new System.Windows.Forms.Button();
									dsb_Controls[ColumnIndex] = dsb_btnBox;

									dsb_btnBox.Anchor = System.Windows.Forms.AnchorStyles.Top |  System.Windows.Forms.AnchorStyles.Left;
									dsb_btnBox.Location = new System.Drawing.Point(170, 10 + 33 * BrowseIndex);
									dsb_btnBox.TabIndex = BrowseIndex * 2 + 1;
									dsb_btnBox.Size = new System.Drawing.Size(32, 20);
									dsb_btnBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
									dsb_btnBox.BackColor = System.Drawing.Color.Turquoise;
									dsb_btnBox.Text = "...";
									dsb_btnBox.Tag = ColumnIndex;
									dsb_btnBox.Click += new System.EventHandler(dsb_btnBox_BindingList_Click);

									dsb_pnlFieldsList.Controls.Add(dsb_btnBox);
								}
								else if (System.Object.ReferenceEquals(typeof(System.Object), Column.PropertyType))
								{
									dsb_btnBox = new System.Windows.Forms.Button();
									dsb_Controls[ColumnIndex] = dsb_btnBox;

									dsb_btnBox.Anchor = System.Windows.Forms.AnchorStyles.Top |  System.Windows.Forms.AnchorStyles.Left;
									dsb_btnBox.Location = new System.Drawing.Point(170, 10 + 33 * BrowseIndex);
									dsb_btnBox.TabIndex = BrowseIndex * 2 + 1;
									dsb_btnBox.Size = new System.Drawing.Size(32, 20);
									dsb_btnBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
									dsb_btnBox.BackColor = System.Drawing.Color.Turquoise;
									dsb_btnBox.Text = "...";
									dsb_btnBox.Tag = ColumnIndex;
									dsb_btnBox.Click += new System.EventHandler(dsb_btnBox_SystemObject_Click);

									dsb_pnlFieldsList.Controls.Add(dsb_btnBox);
								}
								else if (System.Object.ReferenceEquals(typeof(System.Byte[]), Column.PropertyType))
								{
									dsb_btnBox = new System.Windows.Forms.Button();
									dsb_Controls[ColumnIndex] = dsb_btnBox;

									dsb_btnBox.Anchor = System.Windows.Forms.AnchorStyles.Top |  System.Windows.Forms.AnchorStyles.Left;
									dsb_btnBox.Location = new System.Drawing.Point(170, 10 + 33 * BrowseIndex);
									dsb_btnBox.TabIndex = BrowseIndex * 2 + 1;
									dsb_btnBox.Size = new System.Drawing.Size(32, 20);
									dsb_btnBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
									dsb_btnBox.BackColor = System.Drawing.Color.Turquoise;
									dsb_btnBox.Text = "...";
									dsb_btnBox.Tag = ColumnIndex;
									dsb_btnBox.Click += new System.EventHandler(dsb_btnBox_SystemByteArray_Click);

									dsb_pnlFieldsList.Controls.Add(dsb_btnBox);
								}
								else if (Column.PropertyType ==  typeof(System.DateTime)/* ||  Column.DataType ==  GetType(DateTime)*/)
								{
									dsb_txtBox = new Cerebrum.Data.Manager.Forms.BindableTextBox();
									dsb_Controls[ColumnIndex] = dsb_txtBox;

									dsb_txtBox.ValueType = Column.PropertyType;
									dsb_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Top |  System.Windows.Forms.AnchorStyles.Left |  System.Windows.Forms.AnchorStyles.Right;
									dsb_txtBox.Location = new System.Drawing.Point(170, 10 + 33 * BrowseIndex);
									dsb_txtBox.TabIndex = BrowseIndex * 2 + 1;
									dsb_txtBox.Size = new System.Drawing.Size(this.Width - 210, 20);
									dsb_txtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
									dsb_txtBox.ReadOnly = Column.IsReadOnly;
									dsb_txtBox.BackColor = dsb_txtBox.ReadOnly?System.Drawing.Color.Gainsboro:System.Drawing.Color.AliceBlue;
									dsb_txtBox.DataBindings.Add(new System.Windows.Forms.Binding("Value", DataTableView, Column.Name));

									dsb_pnlFieldsList.Controls.Add(dsb_txtBox);
								}
								else
								{
									dsb_txtBox = new Cerebrum.Data.Manager.Forms.BindableTextBox();
									dsb_Controls[ColumnIndex] = dsb_txtBox;

									dsb_txtBox.ValueType = Column.PropertyType;
									dsb_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Top |  System.Windows.Forms.AnchorStyles.Left |  System.Windows.Forms.AnchorStyles.Right;
									dsb_txtBox.Location = new System.Drawing.Point(170, 10 + 33 * BrowseIndex);
									dsb_txtBox.TabIndex = BrowseIndex* 2+ 1;
									dsb_txtBox.Size = new System.Drawing.Size(this.Width - 210, 20);
									dsb_txtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
									dsb_txtBox.ReadOnly = Column.IsReadOnly;
									dsb_txtBox.BackColor = dsb_txtBox.ReadOnly?System.Drawing.Color.Gainsboro:System.Drawing.Color.White;
									dsb_txtBox.DataBindings.Add(new System.Windows.Forms.Binding("Value", DataTableView, Column.Name));

									dsb_pnlFieldsList.Controls.Add(dsb_txtBox);
							
									if(clmn!=null && clmn.IsPassword)
									{
										dsb_txtBox.PasswordChar = '*';
									}
								}
							}
							else
							{
								System.ComponentModel.IBindingList RelatedTable = prpsTables[clmn.RelatedTableName].GetValue(DataSource[0]) as System.ComponentModel.IBindingList; 
								dsb_cmbBox = new System.Windows.Forms.ComboBox();
								dsb_Controls[ColumnIndex] = dsb_cmbBox;

								if(Column.IsReadOnly)
								{
									dsb_cmbBox.Enabled = false;
									dsb_cmbBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
								}

								dsb_cmbBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
								dsb_cmbBox.Location = new System.Drawing.Point(170, 10 + 33 * BrowseIndex);
								dsb_cmbBox.TabIndex = BrowseIndex * 2 + 1;
								dsb_cmbBox.Size = new System.Drawing.Size(this.Width - 210, 20);
								dsb_cmbBox.BackColor = System.Drawing.Color.Honeydew;//System.Drawing.Color.Linen;
								dsb_cmbBox.Anchor = System.Windows.Forms.AnchorStyles.Top |  System.Windows.Forms.AnchorStyles.Left |  System.Windows.Forms.AnchorStyles.Right;
								string DisplayMember;
								string ValueMember;
								ValueMember = clmn.RelatedFieldName;
								Cerebrum.Data.IDataTableInfo map = DataContext==null?null:DataContext.GetTable(clmn.RelatedTableName);
								if(map!=null && map.DisplayFieldName!=null && map.DisplayFieldName.Length > 0)
								{
									DisplayMember = map.DisplayFieldName;
								}
								else
								{
									DisplayMember = ValueMember;
								}
								dsb_cmbBox.DisplayMember = DisplayMember;
								dsb_cmbBox.ValueMember = ValueMember;
								dvFiltered[ColumnIndex] = RelatedTable;

#warning wrapper
								Cerebrum.Data.DataItemUnion du = new DataItemUnion();
								du.Add(dvFiltered[ColumnIndex]);
								du.FieldNames = new string[] {ValueMember, DisplayMember};

								if(clmn.AllowDBNull)
								{
									//Cerebrum.Data.DataListUnion du = 
									/*new Cerebrum.Data.DataListUnion();
									dv.SelectColumns(new string[]{ValueMember, DisplayMember});
									System.Data.DataRow rw = RelatedTable.NewRow();
									try
									{
										rw[dsb_cmbBox.DisplayMember] = "<null>";
									}
									catch
									{
									}
									rw[dsb_cmbBox.ValueMember] = System.DBNull.Value;
									dv.Add(rw);
									dv.Add(dvFiltered[ColumnIndex]);*/

									dsb_cmbBox.DataSource = Cerebrum.Data.DataListUnion.FromBindingList(du, ValueMember, DisplayMember, System.DBNull.Value, "<null>");
									//dsb_cmbBox.DataSource = Cerebrum.Data.DataListUnion.FromBindingList(dvFiltered[ColumnIndex], ValueMember, DisplayMember, System.DBNull.Value, "<null>");
								}
								else
								{
									dsb_cmbBox.DataSource = du;
									//dsb_cmbBox.DataSource = dvFiltered[ColumnIndex];
								}

								dsb_cmbBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", DataTableView, Column.Name));

								dsb_pnlFieldsList.Controls.Add(dsb_cmbBox);
							}
							BrowseIndex++;
						}
						if(clmn is System.IDisposable)
						{
							(clmn as System.IDisposable).Dispose();
						}
					}
#warning Filter
				/*for (ColumnIndex=0; ColumnIndex <= FieldsCount- 1; ColumnIndex++)
				{
			System.ComponentModel.PropertyDescriptor Column = prpsColumns[ColumnIndex];

			DBEXP.IDataFieldInfo clmn = constraints.GetColumn(DataMember, Column.Name);
					if(clmn!=null && clmn.Related && clmn.RelatedTableName!=null && clmn.RelatedTableName.Length > 0)
					{
						System.ComponentModel.IBindingList dv = dvFiltered[ColumnIndex];
						if(dv!=null)
						{

							foreach(DBEXP.IDataFieldInfo clmn2 in DataContext.GetColumns(clmn.RelatedTableName))
							{

								if(clmn2!=null && clmn2.Filter)
								{

									for (int ColumnIndex3=0; ColumnIndex3 <= FieldsCount- 1; ColumnIndex3++)
									{
					  System.ComponentModel.PropertyDescriptor Column3 = prpsColumns[ColumnIndex3];

					  DBEXP.IDataFieldInfo clmn3 = constraints.GetColumn(DataMember, Column3.Name);
										if(clmn3!=null && clmn3.RelatedTableName!=null && clmn3.RelatedTableName==clmn2.RelatedTableName)
										{
											dsb_cmbBox = dsb_Controls[ColumnIndex3] as System.Windows.Forms.ComboBox;
											if(dsb_cmbBox!=null)
											{
												DataViewFilter flt = new DataViewFilter();
	#warning System.Data.DataView
						  flt.DataView = dv as System.Data.DataView;
												flt.FilterField = clmn2.FieldName;
												dsb_cmbBox.SelectedIndexChanged += new System.EventHandler(flt.ComboSelectedIndexChanged);
											}
										}
									}

								}
							}
						}
					}
				}*/

				dsb_pnlFieldsList.ResumeLayout(false);

				RefreshLayout();
			}
			catch(System.Exception ex)
			{
				Cerebrum.Windows.Forms.Application.Instance.ProcessException(ex, "Cerebrum.Data.ctlDataTableBrowser.Render");
			}
		}

		public void RefreshLayout()
		{
			bool enable = this.BindingContext[DataTableView].Count > 0;

			foreach(System.Windows.Forms.Control control in this.dsb_Controls)
			{
				if(control is System.Windows.Forms.ComboBox)
				{
					if( (control as System.Windows.Forms.ComboBox).DropDownStyle != System.Windows.Forms.ComboBoxStyle.Simple) 
					{
						control.Enabled = enable;
					}
				}
				else
					if(control is System.Windows.Forms.TextBox)
				{
					if( ! (control as System.Windows.Forms.TextBox).ReadOnly ) 
					{
						control.Enabled = enable;
					}
				}
			}
			if(enable)
			{
				this.dsb_lblNavLocation.Text = (this.BindingContext[DataTableView].Position+ 1) + " of  " + this.DataTableView.Count;
			}
			else
			{
				this.dsb_lblNavLocation.Text = "";
			}
			if(this.UpdateDS.TransactionsEnabled)
			{
				this.dsb_btnAccept.Visible = true;
				this.dsb_btnApply.Visible = true;
				this.dsb_btnCancel.Visible = true;
				this.dsb_btnClose.Visible = false;
			}
			else
			{
				this.dsb_btnClose.Visible = true;
				this.dsb_btnAccept.Visible = false;
				this.dsb_btnApply.Visible = false;
				this.dsb_btnCancel.Visible = false;
			}
		}

		private void dsb_btnNavNext_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.BindingContext[DataTableView].Position += 1;
			}

			catch (System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(this.FindForm(), ex.Message);
			}
		}

		private void dsb_btnNavPrev_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.BindingContext[DataTableView].Position -= 1;
			}

			catch (System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(this.FindForm(), ex.Message);
			}
		}

		private void dsb_btnNavLast_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.BindingContext[DataTableView].Position = ((this.DataTableView.Count)- (1));
			}

			catch (System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(this.FindForm(), ex.Message);
			}
		}

		private void dsb_btnNavFirst_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.BindingContext[DataTableView].Position = 0;
			}

			catch (System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(this.FindForm(), ex.Message);
			}
		}

		private void dsb_btnInsert_Click(object sender, System.EventArgs e)
		{
			try
				// Clear out the current edits
			{
				this.BindingContext[DataTableView].EndCurrentEdit();
				this.BindingContext[DataTableView].AddNew();
			}

			catch (System.Exception eEndEdit)
			{
				System.Windows.Forms.MessageBox.Show(this.FindForm(), eEndEdit.Message);
			}
		}

		private void dsb_btnDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.BindingContext[DataTableView].RemoveAt(this.BindingContext[DataTableView].Position);
			}

			catch (System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(this.FindForm(), ex.Message);
			}
		}

		private void dsb_btnRevert_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.BindingContext[DataTableView].CancelCurrentEdit();
			}
			catch (System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(this.FindForm(), ex.Message);
			}
			try
			{
				UpdateDS.RejectDataSource(this.DataSource);
			}
			catch (System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(this.FindForm(), ex.Message);
			}
		}

		public void dsb_btnBrowse_Click(object sender, System.EventArgs e)
		{
			// dsb_dgBrowse.DataSource = BrowserDataSet.Tables(DataMember)
			dsb_dgBrowse.TableStyles.Clear();
			Cerebrum.Data.DataGrid.PopulateTableStyle(dsb_dgBrowse, DataSource, DataTableView, /*DataMember, */null, DataContext);
			dsb_dgBrowse.Visible = true;
			dsb_pnlFieldsList.Visible = false;
			dsb_pnlBrowse.Visible = true;
			dsb_pnlBrowse.Enabled = true;
			dsb_pnlControl.Enabled = false;
			dsb_pnlControl.Visible = false;
			dsb_pnlOperation.Enabled = false;
		}

		public void dsb_btnApply_Click(object sender, System.EventArgs e)
		{
			try
			{
				UpdateDS.UpdateDataSource(this.DataSource);
			}
			catch (System.Exception eUpdate)
			{
				System.Windows.Forms.MessageBox.Show(this.FindForm(), eUpdate.Message);
			}

		}

		public void EndCurrentEdit()
		{
			// Clear out the current edits
			this.BindingContext[DataTableView].EndCurrentEdit();
		}

		/*public void UpdateDataSet()
		{
		   System.Data.DataSet BrowserDataSet = (DataSource as System.Data.DataViewManager).DataSet;

			// Get a new dataset that holds only the changes that have been made to the main dataset
			System.ComponentModel.IBindingList objDataSetChanges;
			System.ComponentModel.IBindingList objDataSetUpdated;
			// Clear out the current edits
			this.BindingContext[DataTableView].EndCurrentEdit();
			// Get a new dataset that holds only the changes that have been made to the main dataset
			objDataSetChanges = BrowserDataSet.GetChanges().DefaultViewManager;
			// Check to see if the objCustomersDatasetChanges holds any records
			if (objDataSetChanges !=  null)
			{
				try
					// Call the update method passing inthe dataset and any parameters
				{
					objDataSetUpdated = this.UpdateDataSource(objDataSetChanges);
				}
				catch (System.Exception eUpdate)
				{
					// If the update failed and is part of a transaction, this is the place to put your rollback					throw eUpdate;
				}

				// If the update succeeded and is part of a transaction, this is the place to put your commit				// Add code to Check the returned dataset - objCustomersDataSetUpdate for any errors that may have been				// pushed into the row object's error				// Merge the returned changes back into the main dataset
				try
				{
#warning DataViewManager
		  BrowserDataSet.Merge((objDataSetUpdated as System.Data.DataViewManager).DataSet);
				}
				catch (System.Exception eUpdateMerge)
				{
					// Add exception handling code here					throw eUpdateMerge;
				}

				// Commit the changes that were just merged
				// This moves any rows marked as updated, inserted or changed to being marked as original values
				BrowserDataSet.AcceptChanges();
			}

		}
*/
		public void dsb_btnAccept_Click(object sender, System.EventArgs e)
		{
			dsb_btnApply_Click(sender, e);

			if(this.AcceptClick!=null)
			{
				this.AcceptClick(this, e);
			}
		}

		public void dsb_btnReturn_Click(object sender, System.EventArgs e)
		{
			dsb_dgBrowse.DataSource = null;
			dsb_dgBrowse.Visible = false;
			dsb_pnlFieldsList.Visible = true;
			dsb_pnlBrowse.Visible = false;
			dsb_pnlBrowse.Enabled = false;
			dsb_pnlControl.Enabled = true;
			dsb_pnlControl.Visible = true;
			dsb_pnlOperation.Enabled = true;
		}

		private void frmDataSetBrowser_PositionChanged(object sender, System.EventArgs e)
		{
			RefreshLayout();
		}

		private void dsb_btnCancel_Click(object sender, System.EventArgs e)
		{
			dsb_btnRevert_Click(sender, e);

			if(this.CancelClick!=null)
			{
				this.CancelClick(this, e);
			}
		}

		public event System.EventHandler AcceptClick;
		public event System.EventHandler CancelClick;

		[DefaultValue(true)]
		public bool AllowAccept 
		{
			get
			{
				return dsb_btnAccept.Visible;
			}
			set
			{
				dsb_btnAccept.Visible = value;
			}
		}

		[DefaultValue(true)]
		public bool AllowCancel
		{
			get
			{
				return dsb_btnCancel.Visible;
			}
			set
			{
				dsb_btnCancel.Visible = value;
			}
		}

		[DefaultValue(true)]
		public bool AllowBrowse
		{
			get
			{
				return dsb_btnBrowse.Visible;
			}
			set
			{
				dsb_btnBrowse.Visible = value;
			}
		}

		private void ctlDataTableBrowser_ListChanged(object sender, ListChangedEventArgs e)
		{
			if(e.ListChangedType==System.ComponentModel.ListChangedType.ItemAdded && DataContext!=null)
			{
#warning ListChangedType.ItemAdded
				/*System.Data.DataTable Table = (sender as System.Data.DataView).Table;
				System.Data.DataRow row = (sender as System.Data.DataView)[e.NewIndex].Row;

				Cerebrum.DataContext constraints = new Cerebrum.DataContext();
				constraints.EnforceConstraints = false;
				BL.Data.DataEngine.MergeConstraints(constraints.DataColumns, TableMappings, DataSource, Table.TableName);


				foreach(Cerebrum.DataContext.DataColumnInfo rel in  constraints.DataColumns)
				{
					if(rel.Create)
					{
						Cerebrum.DataContext.DataTableInfo map = TableMappings.DataTables.FindByTableName(rel.RelatedTableName);
						DBEXP.RuntimeContext.TableInstance rti = DataSource[rel.RelatedTableName];
						System.Data.DataTable tbl = rti.RuntimeObject as System.Data.DataTable;
						if(rti!=null && tbl!=null)
						{
							
							row[rel.FieldName] =  tbl.Rows[rti.Position][rel.RelatedFieldName];
						}
					}
				}*/
			}
		}
#warning 	DataViewFilter
		/*private class DataViewFilter
		{
			public System.Data.DataView DataView;
			public string FilterField;
			
			public void ComboSelectedIndexChanged(object sender, System.EventArgs e)
			{
				object v = (sender as System.Windows.Forms.ComboBox).SelectedValue;

				if(v!=null && !(v is System.DBNull))
				{
					DataView.RowFilter = FilterField + "='" +  v.ToString() + "'";
				}
				else
				{
					DataView.RowFilter = FilterField + " is NULL";
				}
			}

		}*/

		private void dsb_btnClose_Click(object sender, System.EventArgs e)
		{
			dsb_btnAccept_Click(sender, e);
		}

		private void dsb_btnBox_BindingList_Click(object sender, System.EventArgs e)
		{
			int ColumnIndex = (int)((System.Windows.Forms.Button)sender).Tag;
			System.ComponentModel.PropertyDescriptorCollection prpsColumns = ((System.ComponentModel.ITypedList)DataTableView).GetItemProperties(null);
			System.ComponentModel.PropertyDescriptor Column = prpsColumns[ColumnIndex];
			int position = this.BindingContext[DataTableView].Position;
			System.ComponentModel.IBindingList ds = (System.ComponentModel.IBindingList)Column.GetValue(DataTableView[position]);
			Cerebrum.Data.Manager.Forms.frmDataTableBrowser dlg = new Cerebrum.Data.Manager.Forms.frmDataTableBrowser();
			dlg.PopulateGrid(this.DataSource, ds, null, this.DataContext, Column.DisplayName);
			dlg.ShowDialog(this.FindForm());
			dlg.Dispose();
		}

		private void dsb_btnBox_SystemObject_Click(object sender, System.EventArgs e)
		{
			int ColumnIndex = (int)((System.Windows.Forms.Button)sender).Tag;
			System.ComponentModel.PropertyDescriptorCollection prpsColumns = ((System.ComponentModel.ITypedList)DataTableView).GetItemProperties(null);
			System.ComponentModel.PropertyDescriptor Column = prpsColumns[ColumnIndex];
			int position = this.BindingContext[DataTableView].Position;
			Cerebrum.Data.Manager.Forms.frmObjectBrowser dlg = new Cerebrum.Data.Manager.Forms.frmObjectBrowser(false);
			dlg.Value = Column.GetValue(DataTableView[position]);
			if(dlg.ShowDialog(this.FindForm())==System.Windows.Forms.DialogResult.OK)
			{
				Column.SetValue(DataTableView[position], dlg.Value);
			}
			dlg.Dispose();
		}

		private void dsb_btnBox_SystemByteArray_Click(object sender, System.EventArgs e)
		{
			int ColumnIndex = (int)((System.Windows.Forms.Button)sender).Tag;
			System.ComponentModel.PropertyDescriptorCollection prpsColumns = ((System.ComponentModel.ITypedList)DataTableView).GetItemProperties(null);
			System.ComponentModel.PropertyDescriptor Column = prpsColumns[ColumnIndex];
			int position = this.BindingContext[DataTableView].Position;
			Cerebrum.Data.Manager.Forms.frmObjectBrowser dlg = new Cerebrum.Data.Manager.Forms.frmObjectBrowser(true);
			dlg.Value = Column.GetValue(DataTableView[position]);
			if(dlg.ShowDialog(this.FindForm())==System.Windows.Forms.DialogResult.OK)
			{
				Column.SetValue(DataTableView[position], dlg.Value);
			}
			dlg.Dispose();
		}
	}

}
