// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

namespace Cerebrum.Data
{
	public class DataItemUnion : System.ComponentModel.Component, System.ComponentModel.IBindingList, System.ComponentModel.ISupportInitialize, System.ComponentModel.ITypedList
	{
		//private System.Collections.ArrayList Tables;
		private System.Collections.ArrayList Rows;
		public DataItemUnion()
		{
			//Tables = new System.Collections.ArrayList();
			Rows = new System.Collections.ArrayList();
		}

		public void Add(System.ComponentModel.IBindingList v)
		{
			int i;
			for (i=0; i <= v.Count- 1; i++)
			{
				this.Add(new Cerebrum.Data.DataItemView((System.ComponentModel.ICustomTypeDescriptor)v[i]));
			}
		}

		/*public void Add(System.Data.DataTable t)
		{
			this.Add(t.DefaultView);
		}

		public void Add(System.Data.DataTable[] t)
		{
			foreach (System.Data.DataTable tbl in t)
			{
				this.Add(tbl.DefaultView);
			}
		}*/

		public int Add(System.ComponentModel.ICustomTypeDescriptor r)
		{
			return this.Add(new Cerebrum.Data.DataItemView(r));
		}

		public void Add(System.ComponentModel.ICustomTypeDescriptor[] r)
		{
			foreach (System.ComponentModel.ICustomTypeDescriptor row in r)
			{
				this.Add(new Cerebrum.Data.DataItemView(row));
			}

		}

		public int Add(object value)
		{
			return this.Add((Cerebrum.Data.DataItemView)value);
		}


		int System.Collections.IList.Add(object value)
		{
			return this.Add((Cerebrum.Data.DataItemView)value);
		}

		protected int Add(Cerebrum.Data.DataItemView rv)
		{
			int i = Rows.Add(rv);
			if(_suspendCount == 0 && this.ListChanged!=null)
			{
				this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemAdded, i));
			}
			return i;
		}

		int _suspendCount = 0;
		public void SuspendEvents()
		{
			_suspendCount ++;
		}
		public void ResumeEvents()
		{
			_suspendCount --;
			if(_suspendCount == 0)
			{
				Reset();
			}
		}

		public void Clear()
		{
			/*for(int i=Rows.Count-1;i>=0;i--)
			{
				RemoveAt(i);
			}*/
			Rows.Clear();
			Reset();
		}
		public void Reset()
		{
			if(_suspendCount == 0 && this.ListChanged!=null)
			{
				this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.Reset, -1));
			}
		}

		public bool Contains(object value)
		{
			return Rows.Contains(value);
		}

		public void CopyTo(System.Array array, int index)
		{
			Rows.CopyTo(array, index);
		}

		public int Count
		{
			get
			{
				return Rows.Count;
			}
		}

		public System.Collections.IEnumerator GetEnumerator()
		{
			return Rows.GetEnumerator();
		}

		public int IndexOf(object value)
		{
			return Rows.IndexOf(value);
		}

		public void Insert(int index, object value)
		{
			throw new System.NotSupportedException();
		}

		public bool IsSynchronized
		{
			get
			{
				return Rows.IsSynchronized;
			}
		}
 

		public object this[int index]
		{
			get
			{
				return Rows[index];
			}
			set
			{
				Rows[index] = value;
				if(_suspendCount == 0 && this.ListChanged!=null)
				{
					this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemChanged, index));
				}
			}
		}

		public void Remove(object value)
		{
			int i = Rows.IndexOf(value);
			if(i>=0)
			{
				this.RemoveAt(i);
			}
		}

		public void RemoveAt(int index)
		{
			Rows.RemoveAt(index);
			if(_suspendCount == 0 && this.ListChanged!=null)
			{
				this.ListChanged(this, new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemDeleted, index));
			}
		}

		public int Find(System.ComponentModel.PropertyDescriptor property, object key)
		{
			int i;
			System.Collections.IList s = Rows;
			for (i=0; i < s.Count; i++)
			{
				if (key.Equals(property.GetValue(s[i])))
				{
					return i;
				}

			}

			return -1;
		}

		#region IBindingList Members

		public void AddIndex(System.ComponentModel.PropertyDescriptor property)
		{
			// TODO:  Add DataView.AddIndex implementation
		}

		public bool AllowNew
		{
			get
			{
				// TODO:  Add DataView.AllowNew getter implementation
				return false;
			}
		}

		public void ApplySort(System.ComponentModel.PropertyDescriptor property, System.ComponentModel.ListSortDirection direction)
		{
			// TODO:  Add DataView.ApplySort implementation
		}

		public System.ComponentModel.PropertyDescriptor SortProperty
		{
			get
			{
				// TODO:  Add DataView.SortProperty getter implementation
				return null;
			}
		}

		public bool SupportsSorting
		{
			get
			{
				// TODO:  Add DataView.SupportsSorting getter implementation
				return false;
			}
		}

		public bool IsSorted
		{
			get
			{
				// TODO:  Add DataView.IsSorted getter implementation
				return false;
			}
		}

		public bool AllowRemove
		{
			get
			{
				// TODO:  Add DataView.AllowRemove getter implementation
				return true;
			}
		}

		public bool SupportsSearching
		{
			get
			{
				// TODO:  Add DataView.SupportsSearching getter implementation
				return true;
			}
		}

		public System.ComponentModel.ListSortDirection SortDirection
		{
			get
			{
				// TODO:  Add DataView.SortDirection getter implementation
				return System.ComponentModel.ListSortDirection.Ascending;
			}
		}

		public event System.ComponentModel.ListChangedEventHandler ListChanged;

		public bool SupportsChangeNotification
		{
			get
			{
				// TODO:  Add DataView.SupportsChangeNotification getter implementation
				return true;
			}
		}

		public void RemoveSort()
		{
			// TODO:  Add DataView.RemoveSort implementation
		}

		public object AddNew()
		{
			// TODO:  Add DataView.AddNew implementation
			return null;
		}

		public bool AllowEdit
		{
			get
			{
				// TODO:  Add DataView.AllowEdit getter implementation
				return false;
			}
		}

		public void RemoveIndex(System.ComponentModel.PropertyDescriptor property)
		{
			// TODO:  Add DataView.RemoveIndex implementation
		}

		#endregion

		#region IList Members

		public bool IsReadOnly
		{
			get
			{
				// TODO:  Add DataView.IsReadOnly getter implementation
				return false;
			}
		}

		public bool IsFixedSize
		{
			get
			{
				// TODO:  Add DataView.IsFixedSize getter implementation
				return false;
			}
		}

		#endregion

		#region ICollection Members

		public object SyncRoot
		{
			get
			{
				// TODO:  Add DataView.SyncRoot getter implementation
				return Rows.SyncRoot;
			}
		}

		#endregion

		#region ISupportInitialize Members

		public void BeginInit()
		{
			// TODO:  Add DataView.BeginInit implementation
		}

		public void EndInit()
		{
			// TODO:  Add DataView.EndInit implementation
		}

		#endregion

		protected System.ComponentModel.PropertyDescriptorCollection m_SelectedColumns = new System.ComponentModel.PropertyDescriptorCollection(new System.ComponentModel.PropertyDescriptor[0]);


		public string[] FieldNames
		{
			get
			{
				string [] names = new string[m_SelectedColumns.Count];
				for(int i=0;i<m_SelectedColumns.Count;i++)
				{
					names[i] = m_SelectedColumns[i].Name;
				}
				return names;
			}
			set
			{
				m_SelectedColumns.Clear();
				for(int i=0;i<value.Length;i++)
				{
					m_SelectedColumns.Add(new Cerebrum.Data.DataItemPropertyDescriptor(value[i]));
				}
			}
		}

		public string SelectedColumns
		{
			get
			{
				return System.String.Join(",", FieldNames);
			}
			set
			{
				FieldNames = value.Split(new char[]{','});
			}
		}
		#region ITypedList Members

		public System.ComponentModel.PropertyDescriptorCollection GetItemProperties(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			/*if(Rows.Count>0)
			{
				DataItemView rv = Rows[0] as DataItemView;
				return rv.GetProperties(null);
			}*/
			return m_SelectedColumns;
		}

		public string GetListName(System.ComponentModel.PropertyDescriptor[] listAccessors)
		{
			return System.String.Empty;
		}

		#endregion
	}

}

