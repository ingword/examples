// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager
{
	/// <summary>
	/// Summary description for frmEngageObject.
	/// </summary>
	public class frmEngageObject : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button dsb_btnAccept;
		private System.Windows.Forms.ComboBox cmbTables;
		private System.Windows.Forms.Label lblTable;
		private System.Windows.Forms.Panel panel2;
		private Cerebrum.Data.DataGrid dsb_dgBrowse;
		private System.Windows.Forms.TextBox txtObjectHandle;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmEngageObject()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.txtObjectHandle = new System.Windows.Forms.TextBox();
			this.dsb_btnAccept = new System.Windows.Forms.Button();
			this.cmbTables = new System.Windows.Forms.ComboBox();
			this.lblTable = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.dsb_dgBrowse = new Cerebrum.Data.DataGrid();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dsb_dgBrowse)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.txtObjectHandle);
			this.panel1.Controls.Add(this.dsb_btnAccept);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 233);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(292, 40);
			this.panel1.TabIndex = 1;
			// 
			// txtObjectHandle
			// 
			this.txtObjectHandle.Location = new System.Drawing.Point(96, 8);
			this.txtObjectHandle.Name = "txtObjectHandle";
			this.txtObjectHandle.TabIndex = 2;
			this.txtObjectHandle.Text = "";
			// 
			// dsb_btnAccept
			// 
			this.dsb_btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_btnAccept.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.dsb_btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnAccept.Location = new System.Drawing.Point(208, 8);
			this.dsb_btnAccept.Name = "dsb_btnAccept";
			this.dsb_btnAccept.TabIndex = 1;
			this.dsb_btnAccept.Text = "&Ok";
			this.dsb_btnAccept.Click += new System.EventHandler(this.dsb_btnAccept_Click);
			// 
			// cmbTables
			// 
			this.cmbTables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbTables.DropDownWidth = 120;
			this.cmbTables.Location = new System.Drawing.Point(56, 8);
			this.cmbTables.Name = "cmbTables";
			this.cmbTables.Size = new System.Drawing.Size(121, 21);
			this.cmbTables.TabIndex = 2;
			this.cmbTables.SelectedIndexChanged += new System.EventHandler(this.cmbTables_SelectedIndexChanged);
			// 
			// lblTable
			// 
			this.lblTable.AutoSize = true;
			this.lblTable.Location = new System.Drawing.Point(8, 8);
			this.lblTable.Name = "lblTable";
			this.lblTable.Size = new System.Drawing.Size(36, 16);
			this.lblTable.TabIndex = 3;
			this.lblTable.Text = "Table:";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.lblTable);
			this.panel2.Controls.Add(this.cmbTables);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(292, 40);
			this.panel2.TabIndex = 4;
			// 
			// dsb_dgBrowse
			// 
			this.dsb_dgBrowse.AllowNavigation = false;
			this.dsb_dgBrowse.AlternatingBackColor = System.Drawing.Color.Ivory;
			this.dsb_dgBrowse.BackColor = System.Drawing.Color.LightYellow;
			this.dsb_dgBrowse.BackgroundColor = System.Drawing.Color.SteelBlue;
			this.dsb_dgBrowse.CaptionBackColor = System.Drawing.Color.SlateGray;
			this.dsb_dgBrowse.CaptionFont = new System.Drawing.Font("Times New Roman", 12F);
			this.dsb_dgBrowse.DataMember = "";
			this.dsb_dgBrowse.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dsb_dgBrowse.GridLineColor = System.Drawing.Color.LightGray;
			this.dsb_dgBrowse.HeaderBackColor = System.Drawing.Color.PaleGoldenrod;
			this.dsb_dgBrowse.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dsb_dgBrowse.LinkColor = System.Drawing.Color.Blue;
			this.dsb_dgBrowse.Location = new System.Drawing.Point(0, 40);
			this.dsb_dgBrowse.Name = "dsb_dgBrowse";
			this.dsb_dgBrowse.ParentRowsBackColor = System.Drawing.Color.Aquamarine;
			this.dsb_dgBrowse.SelectionBackColor = System.Drawing.Color.Firebrick;
			this.dsb_dgBrowse.SelectionForeColor = System.Drawing.Color.White;
			this.dsb_dgBrowse.Size = new System.Drawing.Size(292, 193);
			this.dsb_dgBrowse.TabIndex = 5;
			// 
			// frmEngageObject
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.dsb_dgBrowse);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "frmEngageObject";
			this.Text = "frmEngageObject";
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dsb_dgBrowse)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		System.ComponentModel.IBindingList DataSource;
		public System.ComponentModel.IBindingList DataView;
		public object SelectedItem;
		System.Collections.Hashtable m_BindedViews = new Hashtable();
		public string strObjectHandle;

		public void Initialize(System.ComponentModel.IBindingList DataSource)
		{
			this.DataSource = DataSource;
			System.ComponentModel.PropertyDescriptorCollection prpsTables = ((System.ComponentModel.ITypedList)DataSource).GetItemProperties(null);
			foreach (System.ComponentModel.PropertyDescriptor tableDesc in prpsTables)
			{
				cmbTables.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem(tableDesc.Name, tableDesc.Name));
			}
		}

		private void cmbTables_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			txtObjectHandle.Text = string.Empty;
			System.ComponentModel.PropertyDescriptorCollection prpsTables = ((System.ComponentModel.ITypedList)DataSource).GetItemProperties(null);
			System.ComponentModel.PropertyDescriptor prpTbl = prpsTables[Convert.ToString((cmbTables.SelectedItem as Cerebrum.Windows.Forms.DropDownStringItem).Value)];
			this.DataView = (System.ComponentModel.IBindingList)prpTbl.GetValue(this.DataSource[0]);
			if(!this.m_BindedViews.Contains(this.DataView))
			{
				this.m_BindedViews.Add(this.DataView, true);
				this.BindingContext[this.DataView].PositionChanged += new EventHandler(frmSelectObject_PositionChanged);
			}
			PopulateGrid(this.DataSource, this.DataView, null, null, prpTbl.Name);
			frmSelectObject_PositionChanged(sender, e);
		}

		
		public void PopulateGrid(System.ComponentModel.IBindingList DataSource, System.ComponentModel.IBindingList DataView, System.ComponentModel.PropertyDescriptor [] Attributes, Cerebrum.Data.IDataContext context, string caption)
		{
			dsb_dgBrowse.TableStyles.Clear();
			dsb_dgBrowse.CaptionText = caption;
			Cerebrum.Data.DataGrid.PopulateTableStyle(dsb_dgBrowse, DataSource, DataView, Attributes, context);
		}

		private void frmSelectObject_PositionChanged(object sender, EventArgs e)
		{
			SelectedItem = this.DataView[this.BindingContext[this.DataView].Position];
			txtObjectHandle.Text = Convert.ToString((this.DataView as System.ComponentModel.ITypedList).GetItemProperties(null)[Cerebrum.Specialized.KnownNames.ObjectHandle].GetValue(this.SelectedItem));
		}

		private void dsb_btnAccept_Click(object sender, System.EventArgs e)
		{
			strObjectHandle = txtObjectHandle.Text;
		}
	}
}
