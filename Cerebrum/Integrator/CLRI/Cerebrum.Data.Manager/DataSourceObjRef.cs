// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data.Manager
{
	/// <summary>
	/// Summary description for DataSourceObjRef.
	/// </summary>
	internal class DataSourceObjRef : Cerebrum.Integrator.GenericComponent 
	{
		public DataSourceObjRef()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private Cerebrum.Data.TableSet m_DataSource = null;
		public Cerebrum.Data.TableSet DataSource
		{
			get
			{
				return this.m_DataSource;
			}
		}

		bool m_TransactionStarted = false;
		bool m_TransactionsEnabled = false;

		public bool TransactionsEnabled
		{
			get
			{
				return m_TransactionsEnabled;
			}
			set
			{
				this.m_TransactionsEnabled = value;
			}
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			this.DomainContext.Disposing += new EventHandler(DomainContext_Disposing);
		}

		public void BeginTransaction()
		{
			if(m_TransactionsEnabled && !m_TransactionStarted)
			{
				m_TransactionStarted = true;
				this.DomainContext.BeginTransaction(true);
			}
			if(m_DataSource == null)
			{
				m_DataSource = new TableSet(this.DomainContext.TablesView);
			}
			else
			{
				m_DataSource.Reset();
			}
		}
	
		public void RejectTransaction()
		{
			if(m_TransactionsEnabled && m_TransactionStarted)
			{
				m_TransactionStarted = false;
				Cerebrum.Integrator.DomainContext dc = this.DomainContext;
				if(dc!=null)
				{
					dc.RollbackTransaction();
				}
			}
		}

		public void CommitTransaction()
		{
			if(m_TransactionsEnabled && m_TransactionStarted)
			{
				m_TransactionStarted = false;
				Cerebrum.Integrator.DomainContext dc = this.DomainContext;
				if(dc!=null)
				{
					dc.CommitTransaction();
				}
			}
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(m_DataSource!=null)
				{
					m_DataSource.Dispose();
					m_DataSource = null;
				}
				if(this.m_Connector!=null)
				{
					this.DomainContext.Disposing -= new EventHandler(DomainContext_Disposing);
					this.m_Connector = null;
				}
			}
			base.Dispose( disposing );
		}

		private void DomainContext_Disposing(object sender, EventArgs e)
		{
			this.Dispose();
		}
	}
}
