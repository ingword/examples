// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Windows.Forms;
namespace Cerebrum.Data
{
	//  Cerebrum.Data.DataGridComboBoxColumn
	public class DataGridComboBoxColumn : Cerebrum.Data.DataGridColumnStyle
	{
		public event System.EventHandler Enter;
        
		#region "Constants and ComboBox properties"
		// UI Constants
		private DataGridComboBox mCombo;
		// Used to track editing state
		private Object oldItem = null;
		private System.ComponentModel.IBindingList mBindingList;
		/// <summary>		///     Create a new column		/// </summary>		public DataGridComboBoxColumn()
		{
			mCombo = new DataGridComboBox();
			mCombo.Visible = false;
			mCombo.DropDownStyle = ComboBoxStyle.DropDownList;
		}

		//----------------------------------------------------------		//Properties to allow us to set up the datasource for the mComboBox		//----------------------------------------------------------		/// <summary>		///     The DataSource for values to display in the mComboBox		/// </summary>		public Object DataSource
		{
			get
			{
				return mCombo.DataSource;
			}
			set
			{
				mCombo.DataSource = value;
				try
				{
					try
					{
						if(value is System.ComponentModel.IBindingList)
						{
							mBindingList = (value as System.ComponentModel.IBindingList);
						}
#warning DataTable
						/*else
							if(value is System.Data.DataTable)
						{
							mBindingList = new System.Data.DataView((value as System.Data.DataTable));
						}*/
						else
						{
							mBindingList = null;
						}
					}

					catch //(System.Exception ex)
					{
					}

				}

				catch //(System.Exception ex)
				{
					mBindingList = null;
				}

			}
		}

		/// <summary>		///     The property to display for each item in the mCombobox		/// </summary>		public string DisplayMember
		{
			get
			{
				return mCombo.DisplayMember;
			}
			set
			{   
				mCombo.DisplayMember = value;
			}
		}
		/// <summary>		///     The name of the property that we are going to set the value for		/// </summary>		public string ValueMember
		{
			get
			{
				return mCombo.ValueMember;
			}
			set
			{
				mCombo.ValueMember = value;
			}
		}

		#endregion
		//  *********************************************************************		//  Methods overridden from DataGridColumnStyle		//  *********************************************************************		protected override void Abort(int rowNum)
		{
			RollBack();
			HideComboBox();
			EndEdit();
		}

		protected override bool Commit(System.Windows.Forms.CurrencyManager dataSource, int rowNum)
		{
			HideComboBox();
			//If we are not in an edit then simply return
			if ((mCombo.IsInEditOrNavigateMode))
			{
				return true;
			}

			try
			{
				Object value;
				if (mCombo.SelectedItem == null)
				{
					value = Convert.DBNull;
				}

				else
				{
					value = mCombo.SelectedValue;
					if (value == null)
					{
						value = mCombo.SelectedItem;
					}

					if (value == null ||  NullText.Equals(value))
					{
						value = Convert.DBNull;
					}

				}

				if (oldItem==null || !oldItem.Equals(mCombo.SelectedItem))
				{
					SetColumnValueAtRow(dataSource, rowNum, value);
				}

			}

			catch //(System.Exception ex)
			{
				RollBack();
				return false;
			}

			EndEdit();
			return true;
		}

		protected override void ConcedeFocus()
		{
			mCombo.Visible = false;
		}

		protected override void Edit(System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			if ((rowNum &  1) ==  0)
			{
				mCombo.BackColor = this.mBackColor;
			}

			else
			{
				mCombo.BackColor = this.mAlternatingBackColor;
			}

			//mCombo.Text = "";
			mCombo.SelectedItem = null;
			System.Drawing.Rectangle originalBounds = bounds;
			Object originalValue = this.GetColumnValueAtRow(source, rowNum);
			if(this.Enter!=null)
			{
				this.Enter(this, System.EventArgs.Empty);
			}
			try
			{
				if (mCombo.DataSource == null)
					// .GetType.BaseType.Equals(GetType(ArrayList)) Then
				{
					mCombo.SelectedItem = GetText(originalValue);
				}

					// If combo.DataSource.GetType.BaseType.Equals(GetType(DataTable)) Then
				else
				{
					mCombo.SelectedValue = originalValue;
				}

			}

			catch //(System.Exception ex)
			{
			}

			if ((instantText != null) && instantText.Length >  0)
			{
				mCombo.SelectedValue = instantText;
			}

			oldItem = mCombo.SelectedItem;
			if (readOnly ||  this.ReadOnly)
			{
				mCombo.BackColor = System.Drawing.SystemColors.Control;
			}

			if (cellIsVisible &&  ! (readOnly ||  this.ReadOnly))
			{
				bounds.Offset(xMargin, yMargin);
				bounds.Width -= xMargin* 2;
				bounds.Height -= yMargin;
				mCombo.Bounds = bounds;
				mCombo.Visible = true;
			}

			else
			{
				mCombo.Bounds = originalBounds;
				mCombo.Visible = false;
			}

			mCombo.RightToLeft = this.DataGridTableStyle.DataGrid.RightToLeft;
			mCombo.Focus();
			if (instantText == null ||  instantText.Length < 1)
			{
				mCombo.SelectAll();
			}

			else
			{
				mCombo.Select(mCombo.Text.Length, 0);
			}

			if ((mCombo.Visible))
			{
				this.DataGridTableStyle.DataGrid.Invalidate(originalBounds);
			}

			mCombo.IsInEditOrNavigateMode = false;
		}

		// Set the minimum height to the height of the mCombobox		protected override int GetMinimumHeight()
		{
			return mCombo.PreferredHeight+ yMargin;
		}

		protected override void SetDataGridInColumn(System.Windows.Forms.DataGrid value)
		{
			base.SetDataGridInColumn(value);
			if (mCombo.Parent !=  value)
			{
				if (mCombo.Parent != null)
				{
					mCombo.Parent.Controls.Remove(mCombo);
				}

			}

			if (value != null)
			{
				value.Controls.Add(mCombo);
				mCombo.DataGrid = value as DataGrid;
			}

		}

		protected override void UpdateUI(System.Windows.Forms.CurrencyManager source, int rowNum, string instantText)
		{
			if ((instantText != null) &&  instantText.Length >  0)
			{
				mCombo.Text = instantText;
			}

			else
			{
				mCombo.SelectedValue = GetColumnValueAtRow(source, rowNum);
			}

		}

		#region "Helper Functions"
		//  *********************************************************************		//  Helper functions		//  *********************************************************************
		private void EndEdit()
		{
			mCombo.IsInEditOrNavigateMode = true;
			Invalidate();
		}

		protected override string GetText(Object value)
		{
			if (value == null ||  value is System.DBNull)
			{
				return NullText;
			}

			try
			{
				if (value != null)
				{
					if (mBindingList == null ||  mBindingList.Count <  1)
					{
						return value.ToString();
					}

					else
					{
						System.ComponentModel.PropertyDescriptorCollection prps = (mBindingList as System.ComponentModel.ITypedList).GetItemProperties(null);
						int i = mBindingList.Find(prps[mCombo.ValueMember], value);
						if ( i >=  0)
						{
							return prps[mCombo.DisplayMember].GetValue(mBindingList[i]).ToString();
						}

					}

				}

				return ("");
			}

			catch //(System.Exception ex)
			{
				return ("####");
			}

		}

		private void HideComboBox()
		{
			if ((mCombo.Focused))
			{
				this.DataGridTableStyle.DataGrid.Focus();
			}

			mCombo.Visible = false;
		}

		private void RollBack()
		{
			mCombo.SelectedItem = oldItem;
		}

		public DataGridComboBox ComboBox
		{
			get
			{
				return mCombo;
			}
		}
		#endregion
	}

	/// <summary>
	/// Summary description for DataGridComboBox.
	/// </summary>
	public class DataGridComboBox : ComboBox
	{
		private bool m_IsInEditOrNavigateMode = true;
		private DataGrid m_DataGrid;

		public DataGrid DataGrid
		{
			get
			{
				return m_DataGrid;
			}
			set
			{
				m_DataGrid = value;
			}
		}
		public bool IsInEditOrNavigateMode
		{
			get
			{
				return m_IsInEditOrNavigateMode;
			}
			set
			{
				m_IsInEditOrNavigateMode = value;
			}
		}

		public DataGridComboBox() 
		{
		}

		protected override void OnClick(EventArgs e)
		{
			base.OnClick (e);
			if(m_DataGrid!=null)
				m_DataGrid.StartedEditingColumn(this);
			this.DroppedDown = true;
		}

	}
}