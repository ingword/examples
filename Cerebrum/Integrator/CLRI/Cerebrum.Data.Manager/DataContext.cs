// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data.Manager
{
	/// <summary>
	/// Summary description for DataContext.
	/// </summary>
	public class DataContext : Cerebrum.Data.IDataContext
	{
		Cerebrum.Integrator.DomainContext m_Context;

		public DataContext(Cerebrum.Integrator.DomainContext context)
		{
			m_Context = context;
		}
		#region IDataContext Members

		public IDataTableInfo GetTable(string tableName)
		{
			using(Cerebrum.IConnector cn = m_Context.GetTable(tableName))
			{
				return DataTableInfo.FromTable(cn.Component as Cerebrum.Reflection.TableDescriptor);
			}
		}

		public IDataFieldInfo GetColumn(string tableName, string fieldName)
		{
			using(Cerebrum.IConnector cn = m_Context.GetTable(tableName))
			{
				if(cn==null)
				{
					return null;
				}
				Cerebrum.Reflection.TableDescriptor tbl = cn.Component as Cerebrum.Reflection.TableDescriptor;
				using(Cerebrum.Data.TableView tvv = tbl.GetTableView())
				{
					Cerebrum.Data.AttributePropertyDescriptor atr = tvv.GetItemProperties(null)[fieldName] as Cerebrum.Data.AttributePropertyDescriptor;
					Cerebrum.ObjectHandle PrecedentTableId = Cerebrum.ObjectHandle.Null;
					if(atr is Cerebrum.Data.Specialized.VectorAttributePropertyDescriptor)
					{
						PrecedentTableId = (atr as Cerebrum.Data.Specialized.VectorAttributePropertyDescriptor).PrecedentTableId;
					}
					if(atr is Cerebrum.Data.Specialized.HandleAttributePropertyDescriptor)
					{
						PrecedentTableId = (atr as Cerebrum.Data.Specialized.HandleAttributePropertyDescriptor).PrecedentTableId;
					}
					if(atr is Cerebrum.Data.Specialized.ScalarAttributePropertyDescriptor)
					{
						PrecedentTableId = (atr as Cerebrum.Data.Specialized.ScalarAttributePropertyDescriptor).PrecedentTableId;
					}
					if(PrecedentTableId != Cerebrum.ObjectHandle.Null)
					{
						using(Cerebrum.IConnector connector = m_Context.AttachConnector(PrecedentTableId))
						{
							return new DataFieldInfo(tableName, fieldName, (connector.Component as Cerebrum.Reflection.TableDescriptor).Name);
						}
					}
					return null;
				}
			}
		}

		public IDataFieldInfo[] GetColumns(string tableName)
		{
			// TODO:  Add DataContext.GetColumns implementation
			return null;
		}

		#endregion
	}

	public class DataTableInfo : Cerebrum.Data.IDataTableInfo
	{
		private string m_DisplayName;
		private string m_DisplayFieldName;

		public static DataTableInfo FromTable(Cerebrum.Reflection.TableDescriptor table)
		{
			string displayFieldName = null;
			using(Cerebrum.IConnector connector = table.GetDisplayFieldAttribute())
			{
				if(connector!=null)
				{
					displayFieldName = (connector.Component as Cerebrum.Reflection.IAttributeDescriptor).Name;
				}
			}
			return new DataTableInfo(table.DisplayName, displayFieldName);
		}

		private DataTableInfo(string displayName, string displayFieldName)
		{
			m_DisplayName = displayName;
			m_DisplayFieldName = displayFieldName;
		}

		#region IDataTableInfo Members

		public string DisplayName
		{
			get
			{
				return m_DisplayName;
			}
		}

		public string DisplayFieldName
		{
			get
			{
				return m_DisplayFieldName;
			}
		}

		public bool Browsable
		{
			get
			{
				return true;
			}
		}

		#endregion

	}

	public class DataFieldInfo : Cerebrum.Data.IDataFieldInfo
	{
		private string m_TableName;
		private string m_FieldName;
		private string m_RelatedTableName;

		public DataFieldInfo(string tableName, string fieldName, string relatedTableName)
		{
			m_TableName = tableName;
			m_FieldName = fieldName;
			m_RelatedTableName = relatedTableName;
		}

		public string TableName
		{
			get
			{
				return m_TableName;
			}
		}

		public string FieldName
		{
			get
			{
				return m_FieldName;
			}
		}

		public string RelatedTableName
		{
			get
			{
				return m_RelatedTableName;
			}
		}

		public string RelatedFieldName
		{
			get
			{
				return "ObjectHandle";
			}
		}

		public bool Browsable
		{
			get
			{
				return true;
			}
		}

		public bool IsPassword
		{
			get
			{
				return false;
			}
		}

		public bool Related
		{
			get
			{
				// TODO:  Add DataFieldInfo.Related getter implementation
				return false;
			}
		}

		public bool ReadOnly
		{
			get
			{
				return false;
			}
		}

		public bool Filter
		{
			get
			{
				return false;
			}
		}

		public bool AllowDBNull
		{
			get
			{
				return true;
			}
		}

	}

}
