// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Forms
{
	/// <summary>
	/// Summary description for frmObjectBrowser.
	/// </summary>
	public class frmObjectBrowser : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button dsb_btnAccept;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox txtValue;
		private System.Windows.Forms.ComboBox ddlTypeList;
		private System.Windows.Forms.Button dsb_btnBrowse;
		private System.Windows.Forms.Button dsb_btnCancel;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private bool m_IsByteArray;

		public frmObjectBrowser(bool isByteArray)
		{
			m_IsByteArray = isByteArray;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			ddlTypeList.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem("System.Array", typeof(byte[])));
			ddlTypeList.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem("System.String", typeof(System.String)));
			ddlTypeList.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem("System.Object", typeof(System.Object)));
			ddlTypeList.SelectedIndex = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.dsb_btnCancel = new System.Windows.Forms.Button();
			this.dsb_btnAccept = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.ddlTypeList = new System.Windows.Forms.ComboBox();
			this.txtValue = new System.Windows.Forms.TextBox();
			this.dsb_btnBrowse = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dsb_btnCancel);
			this.panel1.Controls.Add(this.dsb_btnAccept);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 233);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(292, 40);
			this.panel1.TabIndex = 0;
			// 
			// dsb_btnCancel
			// 
			this.dsb_btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.dsb_btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnCancel.Location = new System.Drawing.Point(208, 8);
			this.dsb_btnCancel.Name = "dsb_btnCancel";
			this.dsb_btnCancel.TabIndex = 2;
			this.dsb_btnCancel.Text = "Cancel";
			// 
			// dsb_btnAccept
			// 
			this.dsb_btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_btnAccept.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.dsb_btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnAccept.Location = new System.Drawing.Point(120, 8);
			this.dsb_btnAccept.Name = "dsb_btnAccept";
			this.dsb_btnAccept.TabIndex = 1;
			this.dsb_btnAccept.Text = "&Ok";
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel2.Controls.Add(this.ddlTypeList);
			this.panel2.Controls.Add(this.txtValue);
			this.panel2.Controls.Add(this.dsb_btnBrowse);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(292, 233);
			this.panel2.TabIndex = 1;
			// 
			// ddlTypeList
			// 
			this.ddlTypeList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddlTypeList.Location = new System.Drawing.Point(16, 16);
			this.ddlTypeList.Name = "ddlTypeList";
			this.ddlTypeList.Size = new System.Drawing.Size(176, 21);
			this.ddlTypeList.TabIndex = 2;
			// 
			// txtValue
			// 
			this.txtValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtValue.Location = new System.Drawing.Point(16, 48);
			this.txtValue.Multiline = true;
			this.txtValue.Name = "txtValue";
			this.txtValue.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtValue.Size = new System.Drawing.Size(264, 168);
			this.txtValue.TabIndex = 1;
			this.txtValue.Text = "";
			// 
			// dsb_btnBrowse
			// 
			this.dsb_btnBrowse.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnBrowse.Location = new System.Drawing.Point(200, 16);
			this.dsb_btnBrowse.Name = "dsb_btnBrowse";
			this.dsb_btnBrowse.TabIndex = 0;
			this.dsb_btnBrowse.Text = "Browse";
			this.dsb_btnBrowse.Click += new System.EventHandler(this.dsb_btnBrowse_Click);
			// 
			// frmObjectBrowser
			// 
			this.AcceptButton = this.dsb_btnAccept;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.CancelButton = this.dsb_btnCancel;
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "frmObjectBrowser";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "frmDataTableBrowser";
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void dsb_btnBrowse_Click(object sender, System.EventArgs e)
		{
			if(openFileDialog1.ShowDialog(this)==System.Windows.Forms.DialogResult.OK)
			{
				using(System.IO.Stream fs = openFileDialog1.OpenFile())
				{
					object selectedValue = (ddlTypeList.SelectedItem as Cerebrum.Windows.Forms.DropDownStringItem).Value;
					if(selectedValue == typeof(System.Object))
					{
						System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
						this.Value = formatter.Deserialize(fs);
					}
					else if(selectedValue == typeof(System.String))
					{
						using(System.IO.StreamReader sr = new System.IO.StreamReader(fs, System.Text.Encoding.Default))
						{
							this.Value = sr.ReadToEnd();
						}
					}
					else
					{
						byte [] array = new byte[fs.Length];
						fs.Read(array, 0, array.Length);
						this.Value = array;
					}
				}
			}
		}

		public object Value
		{
			get
			{
				if(this.m_IsByteArray)
				{
					if(txtValue.Text!=null && txtValue.Text!=string.Empty)
					{
						return Convert.FromBase64String(txtValue.Text);
					}
					else
					{
						return null;
					}
				}
				else
				{
					using(System.IO.MemoryStream ms = new System.IO.MemoryStream(Convert.FromBase64String(txtValue.Text)))
					{
						System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
						return formatter.Deserialize(ms);
					}
				}
			}
			set
			{
				if(this.m_IsByteArray)
				{
					if(value is Byte[])
					{
						txtValue.Text = Convert.ToBase64String((byte[])value);
					}
					else
					{
						txtValue.Text = "";
					}
				}
				else
				{
					using(System.IO.MemoryStream ms = new System.IO.MemoryStream())
					{
						System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
						formatter.Serialize(ms, value);
						txtValue.Text = Convert.ToBase64String(ms.ToArray());
					}
				}
			}
		}
	}
}
