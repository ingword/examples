// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager
{
	/// <summary>
	/// Summary description for frmDataBaseBrowser.
	/// </summary>
	public class frmDataBaseBrowser : Cerebrum.Windows.Forms.DesktopComponent, IUpdateDS //System.Windows.Forms.Form
	{
		private Cerebrum.Data.ctlDataSourceBrowser ctlDataSourceBrowser1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmDataBaseBrowser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.ClientSize = this.ctlDataSourceBrowser1.Size;
			this.MinimumSize = this.Size;
			this.ctlDataSourceBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_DataSourceObjRef = null;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(m_DataSourceObjRef!=null)
				{
					Cerebrum.Windows.Forms.Application application = Cerebrum.Windows.Forms.Application.Instance;
					if(application!=null)
					{
						System.Windows.Forms.Form form = application.PrimaryWindow;
						if(form!=null)
						{
							form.BeginInvoke(new System.CrossAppDomainDelegate(m_DataSourceObjRef.Dispose), null);
						}
						else
						{
							System.AppDomain.CurrentDomain.DoCallBack(new System.CrossAppDomainDelegate(m_DataSourceObjRef.Dispose));
						}
					}
					m_DataSourceObjRef = null;
				}

				if(this.m_Connector!=null)
				{
					this.DomainContext.Disposing -= new EventHandler(DomainContext_Disposing);
					this.m_Connector = null;
				}

				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ctlDataSourceBrowser1 = new Cerebrum.Data.ctlDataSourceBrowser();
			this.SuspendLayout();
			// 
			// ctlDataSourceBrowser1
			// 
			this.ctlDataSourceBrowser1.BackColor = System.Drawing.Color.Aquamarine;
			this.ctlDataSourceBrowser1.Location = new System.Drawing.Point(0, 0);
			this.ctlDataSourceBrowser1.Name = "ctlDataSourceBrowser1";
			this.ctlDataSourceBrowser1.Size = new System.Drawing.Size(622, 413);
			this.ctlDataSourceBrowser1.TabIndex = 0;
			this.ctlDataSourceBrowser1.CancelClick += new System.EventHandler(this.ctlDataSourceBrowser1_CancelClick);
			this.ctlDataSourceBrowser1.AcceptClick += new System.EventHandler(this.ctlDataSourceBrowser1_AcceptClick);
			// 
			// frmDataBaseBrowser
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(648, 429);
			this.Controls.Add(this.ctlDataSourceBrowser1);
			this.Name = "frmDataBaseBrowser";
			this.Text = "frmDataBaseBrowser";
			this.Load += new System.EventHandler(this.frmDataBaseBrowser_Load);
			this.Closed += new System.EventHandler(this.frmDataBaseBrowser_Closed);
			this.ResumeLayout(false);

		}
		#endregion

		private Cerebrum.Data.Manager.DataSourceObjRef m_DataSourceObjRef;
		internal Cerebrum.Data.Manager.DataSourceObjRef DataSourceObjRef
		{
			get
			{
				return this.m_DataSourceObjRef;
			}
			set
			{
				this.m_DataSourceObjRef = value;
			}
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			this.DomainContext.Disposing += new EventHandler(DomainContext_Disposing);

			Cerebrum.Windows.Forms.Application application = this.DomainContext.Application as Cerebrum.Windows.Forms.Application;
			if(application.MessageBox("Start new Transaction ?", "Data modification is possible", Cerebrum.Windows.Forms.MessageBoxStyle.ShortYesNo, System.Windows.Forms.MessageBoxIcon.Warning, System.Windows.Forms.MessageBoxDefaultButton.Button2)==System.Windows.Forms.DialogResult.Yes)
			{
				m_DataSourceObjRef.TransactionsEnabled = true;
			}
			else
			{
				m_DataSourceObjRef.TransactionsEnabled = false;
			}
			m_DataSourceObjRef.BeginTransaction();
			ctlDataSourceBrowser1.Initialize(this, m_DataSourceObjRef.DataSource, new Cerebrum.Data.Manager.DataContext(this.DomainContext), false);
		}

		private void frmDataBaseBrowser_Load(object sender, System.EventArgs e)
		{
		}

		private void frmDataBaseBrowser_Closed(object sender, System.EventArgs e)
		{
			m_DataSourceObjRef.RejectTransaction();
		}

		#region IUpdateDS Members

		public bool TransactionsEnabled
		{
			get
			{
				return m_DataSourceObjRef.TransactionsEnabled;
			}
		}

		public void UpdateDataSource(IBindingList updatedDataSource)
		{
			m_DataSourceObjRef.CommitTransaction();
			m_DataSourceObjRef.BeginTransaction();
		}

		public void RejectDataSource(IBindingList updatedDataSource)
		{
			m_DataSourceObjRef.RejectTransaction();
			m_DataSourceObjRef.BeginTransaction();
		}

		#endregion

		private void DomainContext_Disposing(object sender, EventArgs e)
		{
			if(this.m_Connector!=null)
			{
				this.DomainContext.Disposing -= new EventHandler(DomainContext_Disposing);
				this.m_Connector = null;
			}
			this.Close();
		}

		private void ctlDataSourceBrowser1_AcceptClick(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void ctlDataSourceBrowser1_CancelClick(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
