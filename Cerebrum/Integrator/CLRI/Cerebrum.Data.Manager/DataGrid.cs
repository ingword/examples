// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Data
{
	/// <summary>
	/// Summary description for DataGrid.
	/// </summary>
	public class DataGrid : System.Windows.Forms.DataGrid 
	{
		public void StartedEditingColumn ( System.Windows.Forms.Control editingControl  )
		{
			this.ColumnStartedEditing(editingControl.Bounds);
		}

		/*public static System.Windows.Forms.DataGridTableStyle PopulateTableStyle(System.Windows.Forms.DataGrid DataGrid, string TableName, System.ComponentModel.IBindingList DataSource, System.Data.DataColumn[] DataColumns, Cerebrum.DataContext context)
		{
		  System.Data.DataTable DataTable = DataSource[TableName].RuntimeObject as System.Data.DataTable;
		  return PopulateTableStyle(DataGrid, DataTable.DefaultView, DataSource, DataColumns, context);
		}*/
		public static System.Windows.Forms.DataGridTableStyle PopulateTableStyle(System.Windows.Forms.DataGrid DataGrid, System.ComponentModel.IBindingList DataSource, System.ComponentModel.IBindingList DataView, System.ComponentModel.PropertyDescriptor [] Attributes, Cerebrum.Data.IDataContext context)
		{
			System.ComponentModel.PropertyDescriptorCollection prpsTables = ((System.ComponentModel.ITypedList)DataSource).GetItemProperties(null);
      
			//      System.ComponentModel.IBindingList DataView = prpsTables[DataMember].GetValue(DataSource[0]) as System.ComponentModel.IBindingList;
			string DataMember = ((System.ComponentModel.ITypedList)DataView).GetListName(null);

			Cerebrum.Data.IDataContext constraints = context;//new Cerebrum.DataContext();
			//constraints.EnforceConstraints = false;
			//UI.Data.DataEngine.MergeConstraints(constraints.DataColumns, context, DataSource, TableName);

			int ColumnIndex = 0;
			//int FormatIndex = 0;
			// Dim DataView As System.Data.DataTable = DataSet.Tables(TableName)
			System.Windows.Forms.DataGridTableStyle GridTable;
			// Dim NumericFormats() As System.Windows.Forms.ComponentModel.NumericFormat
			//System.Windows.Forms.DataGridColumnStyle[] GridColumns;
			System.Collections.ArrayList GridColumns = new System.Collections.ArrayList();
			//System.Data.DataColumn DataColumn;
			// ReDim NumericFormats(0)
			System.ComponentModel.PropertyDescriptorCollection tblColumns = ((System.ComponentModel.ITypedList)DataView).GetItemProperties(null);
			System.ComponentModel.PropertyDescriptor[] allColumns;
			if (Attributes == null)
			{
				//allColumns = new System.ComponentModel.PropertyDescriptor[tblColumns.Count];
				//tblColumns.CopyTo(allColumns, 0);
				System.Collections.ArrayList arr = new System.Collections.ArrayList();
				foreach(System.ComponentModel.PropertyDescriptor tblClmn in tblColumns)
				{
					if(!tblClmn.PropertyType.IsAbstract)
					{
						arr.Add(tblClmn);
					}
				}
				allColumns = (System.ComponentModel.PropertyDescriptor[])arr.ToArray(typeof(System.ComponentModel.PropertyDescriptor));
			}
			else
			{
				allColumns = Attributes;
			}

			for (ColumnIndex=0; ColumnIndex < allColumns.Length ; ColumnIndex++)
			{
				System.ComponentModel.PropertyDescriptor DataColumn;
				DataColumn = allColumns[ColumnIndex];

				Cerebrum.Data.IDataFieldInfo clmn = constraints==null?null:constraints.GetColumn(DataMember, DataColumn.Name);
				if(clmn==null || clmn.Browsable)
				{
					bool readOnly = (clmn!=null && clmn.ReadOnly);

					/*foreach (System.Data.DataColumn DataColumn in allColumns)
					{
					  int j;
					  System.Data.ForeignKeyConstraint ForeignKey = null;
					  for (j=0; j <= DataView.Constraints.Count- 1; j++)
					  {
						ForeignKey = (DataView.Constraints[j] as System.Data.ForeignKeyConstraint);
						if (ForeignKey!=null && ForeignKey.Columns.GetValue(0) ==  DataColumn)
						{
						  break;
						}
						else
						{
						  ForeignKey = null;
						}

					  }

					  if (ForeignKey == null)
					  {*/
					if (clmn==null || (clmn.RelatedTableName==null || clmn.RelatedFieldName==null || clmn.RelatedTableName.Length < 1 || clmn.RelatedFieldName.Length <1))
					{
						if (! DataColumn.PropertyType.IsArray)
						{
							if (DataColumn.PropertyType ==  typeof(System.DateTime))
							{
								Cerebrum.Data.DataGridDatePickerColumn DatePickerColumn;
								DatePickerColumn = new Cerebrum.Data.DataGridDatePickerColumn();

								DatePickerColumn.ReadOnly = DataColumn.IsReadOnly || readOnly;
								DatePickerColumn.Width = 120;

								DatePickerColumn.HeaderText = DataColumn.DisplayName;
								DatePickerColumn.MappingName = DataColumn.Name;
								DatePickerColumn.AlternatingBackColor = System.Drawing.Color.AliceBlue;
								DatePickerColumn.BackColor = System.Drawing.Color.Lavender;
								// DatePickerColumn.BackColor = System.Drawing.Color.AliceBlue								// DatePickerColumn.AlternatingBackColor = System.Drawing.Color.Ivory
								DatePickerColumn.NullText = "";

								GridColumns.Add(DatePickerColumn);
							}
							else if (DataColumn.PropertyType ==  typeof(System.Boolean))
							{
								System.Windows.Forms.DataGridBoolColumn BoolColumn;
								BoolColumn = new System.Windows.Forms.DataGridBoolColumn();

								BoolColumn.ReadOnly = DataColumn.IsReadOnly || readOnly;
								BoolColumn.AllowNull = (clmn==null || clmn.AllowDBNull);
								BoolColumn.Width = 120;

								BoolColumn.HeaderText = DataColumn.DisplayName;
								BoolColumn.MappingName = DataColumn.Name;
								//BoolColumn.AlternatingBackColor = System.Drawing.Color.AliceBlue;
								//BoolColumn.BackColor = System.Drawing.Color.Lavender;

								BoolColumn.NullText = "";

								GridColumns.Add(BoolColumn);
							}
							else
							{
								Cerebrum.Data.DataGridTextBoxColumnColored TextBoxColumn;
								TextBoxColumn = new Cerebrum.Data.DataGridTextBoxColumnColored();

								TextBoxColumn.ReadOnly = DataColumn.IsReadOnly || readOnly;
								TextBoxColumn.Width = 90;

								TextBoxColumn.HeaderText = DataColumn.DisplayName;
								TextBoxColumn.MappingName = DataColumn.Name;
								TextBoxColumn.AlternatingBackColor = System.Drawing.Color.Honeydew;
								TextBoxColumn.BackColor = System.Drawing.Color.Aquamarine;
								TextBoxColumn.NullAlternatingBackColor = System.Drawing.Color.Honeydew;
								TextBoxColumn.NullBackColor = System.Drawing.Color.Aquamarine;

								TextBoxColumn.NullText = "";
								
								if(clmn!=null && clmn.IsPassword)
								{
									TextBoxColumn.PasswordChar = '*';
								}

								GridColumns.Add(TextBoxColumn);
							}

						}
					}
					else
					{
						System.ComponentModel.IBindingList RelatedTable = prpsTables[clmn.RelatedTableName].GetValue(DataSource[0]) as System.ComponentModel.IBindingList;

						Cerebrum.Data.DataGridComboBoxColumn ComboBoxColumn;
						ComboBoxColumn = new Cerebrum.Data.DataGridComboBoxColumn();

						ComboBoxColumn.ReadOnly = DataColumn.IsReadOnly || readOnly;
						ComboBoxColumn.Width = 120;
						ComboBoxColumn.AlternatingBackColor = System.Drawing.Color.Ivory;
						ComboBoxColumn.BackColor = System.Drawing.Color.Beige;
						ComboBoxColumn.NullText = "";
						ComboBoxColumn.HeaderText = DataColumn.DisplayName;
						ComboBoxColumn.MappingName = DataColumn.Name;
						string DisplayMember;
						string ValueMember;
						ValueMember = clmn.RelatedFieldName;
						Cerebrum.Data.IDataTableInfo map = constraints==null?null:constraints.GetTable(clmn.RelatedTableName);
						if(map!=null && map.DisplayFieldName!=null && map.DisplayFieldName.Length > 0)
						{
							DisplayMember = map.DisplayFieldName;
						}
						else
						{
							DisplayMember = ValueMember;
						}
						ComboBoxColumn.DisplayMember = DisplayMember;
						ComboBoxColumn.ValueMember = ValueMember;
#warning wrapper
						Cerebrum.Data.DataItemUnion du = new DataItemUnion();
						du.Add(RelatedTable);
						du.FieldNames = new string[] {ValueMember, DisplayMember};

						if(clmn!=null && clmn.AllowDBNull)
						{
							//Cerebrum.Data.DataListUnion du = 
							/*new Cerebrum.Data.DataListUnion();
							dv.SelectColumns(new string[]{ValueMember, DisplayMember});
							System.Data.DataRow rw = RelatedTable.NewRow();
							rw[ComboBoxColumn.DisplayMember] = "<null>";//ComboBoxColumn.NullText;
							rw[ComboBoxColumn.ValueMember] = System.DBNull.Value;
							dv.Add(rw);
							dv.Add(RelatedTable);*/
							ComboBoxColumn.DataSource = Cerebrum.Data.DataListUnion.FromBindingList(du, ValueMember, DisplayMember, System.DBNull.Value, "<null>");
							//ComboBoxColumn.DataSource = Cerebrum.Data.DataListUnion.FromBindingList(RelatedTable, ValueMember, DisplayMember, System.DBNull.Value, "<null>");
						}
						else
						{
							ComboBoxColumn.DataSource = du;
							//ComboBoxColumn.DataSource = RelatedTable;
						}

						GridColumns.Add(ComboBoxColumn);
					}
				}
			}

			GridTable = new System.Windows.Forms.DataGridTableStyle();

			GridTable.AlternatingBackColor = DataGrid.AlternatingBackColor;
			GridTable.BackColor = DataGrid.BackColor;
			GridTable.ForeColor = DataGrid.ForeColor;
			GridTable.GridLineColor = DataGrid.GridLineColor;
			GridTable.HeaderBackColor = DataGrid.HeaderBackColor;
			GridTable.HeaderForeColor = DataGrid.HeaderForeColor;
			GridTable.LinkColor = DataGrid.LinkColor;
			// GridTable.AlternatingBackColor = System.Drawing.Color.LightYellow
			// GridTable.BackColor = System.Drawing.Color.White
			// GridTable.ForeColor = System.Drawing.Color.Black
			// GridTable.GridLineColor = System.Drawing.Color.Gray
			// GridTable.HeaderBackColor = System.Drawing.Color.PaleGoldenrod
			// GridTable.HeaderForeColor = System.Drawing.Color.Black
			// GridTable.LinkColor = System.Drawing.Color.Firebrick
			// GridTable.GridTableName = DataTable.TableName
			GridTable.MappingName = DataMember;
			GridTable.DataGrid = DataGrid;
			GridTable.GridColumnStyles.AddRange((System.Windows.Forms.DataGridColumnStyle[])GridColumns.ToArray(typeof(System.Windows.Forms.DataGridColumnStyle)));

			DataGrid.BeginInit();
			DataGrid.DataMember = "";

			DataGrid.DataSource = DataView;
			//DataGrid.DataMember = DataView.TableName;
			

			DataGrid.TableStyles.Add(GridTable);
			DataGrid.EndInit();
			int i;
			for (i=0; i <= GridTable.GridColumnStyles.Count- 1; i++)
			{
				GridTable.GridColumnStyles[i].HeaderText = tblColumns[GridTable.GridColumnStyles[i].MappingName].DisplayName;
			}

			return GridTable;
		}

		//public static System.Windows.Forms.DataGridTableStyle PopulateTableStyle(System.Windows.Forms.DataGrid DataGrid, System.Data.DataView DataView, System.Data.DataColumn[] DataColumns, System.ComponentModel.IBindingList DataSource, DBEXP.IDataContext context)
		//{
		//return PopulateTableStyle(DataGrid, DataView, DataSource, DataColumns, context);
		/*Cerebrum.DataContext.DataViewsDataTable tableMappings = context.DataTables;
		  //int ColumnIndex = 0;
		  //int FormatIndex = 0;
		  // Dim DataTable As System.Data.DataTable = DataSet.Tables(TableName)
		  System.Windows.Forms.DataGridTableStyle GridTable;
		  // Dim NumericFormats() As System.Windows.Forms.ComponentModel.NumericFormat
		  //System.Windows.Forms.DataGridColumnStyle[] GridColumns;
		  System.Collections.ArrayList GridColumns = new System.Collections.ArrayList();
		  //System.Data.DataColumn DataColumn;
		  // ReDim NumericFormats(0)
		  System.Data.DataColumn[] allColumns;
		  if (DataColumns == null)
		  {
			allColumns = new System.Data.DataColumn[DataTable.Columns.Count];
			DataTable.Columns.CopyTo(allColumns, 0);
		  }
		  else
		  {
			allColumns = DataColumns;
		  }

		  foreach (System.Data.DataColumn DataColumn in allColumns)
		  {
			int j;
			System.Data.ForeignKeyConstraint ForeignKey = null;
			for (j=0; j <= DataTable.Constraints.Count- 1; j++)
			{
			  ForeignKey = (DataTable.Constraints[j] as System.Data.ForeignKeyConstraint);
			  if (ForeignKey!=null && ForeignKey.Columns.GetValue(0) ==  DataColumn)
			  {
				break;
			  }
			  else
			  {
				ForeignKey = null;
			  }

			}

			if (ForeignKey == null)
			{
			  if (! DataColumn.DataType.IsArray)
				// ReDim NumericFormats(FormatIndex + 1)
				// NumericFormats(FormatIndex) = New System.Windows.Forms.ComponentModel.NumericFormat()
				// NumericFormats(ColumnIndex).Type = CType(System.Type.GetType("System.Decimal"), System.Type)
				// NumericFormats(ColumnIndex).NullText = ""
				// FormatIndex += 1
			  {
				if (/*DataColumn.DataType ==  GetType(Date) ||  * /DataColumn.DataType ==  typeof(System.DateTime))
				{
				  Cerebrum.Data.DataGridDatePickerColumn DatePickerColumn;
				  DatePickerColumn = new Cerebrum.Data.DataGridDatePickerColumn();

				  DatePickerColumn.ReadOnly = DataColumn.ReadOnly;
				  DatePickerColumn.Width = 120;
				  // DatePickerColumn.Format = NumericFormats(0)
				  DatePickerColumn.HeaderText = DataColumn.Caption;
				  DatePickerColumn.MappingName = DataColumn.FieldName;
				  DatePickerColumn.AlternatingBackColor = System.Drawing.Color.AliceBlue;
				  DatePickerColumn.BackColor = System.Drawing.Color.Lavender;
				  // DatePickerColumn.BackColor = System.Drawing.Color.AliceBlue
				  // DatePickerColumn.AlternatingBackColor = System.Drawing.Color.Ivory
				  DatePickerColumn.NullText = "";

				  GridColumns.Add(DatePickerColumn);
				  //ColumnIndex = 1;
				}

				else
				{
				  Cerebrum.Data.DataGridTextBoxColumnColored TextBoxColumn;
				  TextBoxColumn = new Cerebrum.Data.DataGridTextBoxColumnColored();

				  TextBoxColumn.ReadOnly = DataColumn.ReadOnly;
				  TextBoxColumn.Width = 90;
				  // TextBoxColumn.Format = NumericFormats(0)
				  TextBoxColumn.HeaderText = DataColumn.Caption;
				  TextBoxColumn.MappingName = DataColumn.FieldName;
				  TextBoxColumn.AlternatingBackColor = System.Drawing.Color.Honeydew;
				  TextBoxColumn.BackColor = System.Drawing.Color.Aquamarine;
				  // TextBoxColumn.BackColor = System.Drawing.Color.AliceBlue
				  // TextBoxColumn.AlternatingBackColor = System.Drawing.Color.Ivory
				  TextBoxColumn.NullText = "";

				  GridColumns.Add(TextBoxColumn);
				  //ColumnIndex = 1;
				}

			  }

			}

			else
			{
			  Cerebrum.Data.DataGridComboBoxColumn ComboBoxColumn;
			  ComboBoxColumn = new Cerebrum.Data.DataGridComboBoxColumn();

			  ComboBoxColumn.ReadOnly = DataColumn.ReadOnly;
			  ComboBoxColumn.Width = 120;
			  ComboBoxColumn.AlternatingBackColor = System.Drawing.Color.Ivory;
			  ComboBoxColumn.BackColor = System.Drawing.Color.Beige;
			  ComboBoxColumn.NullText = "";
			  ComboBoxColumn.HeaderText = DataColumn.Caption;
			  ComboBoxColumn.MappingName = DataColumn.FieldName;
			  string DisplayMember;
			  string ValueMember;
			  ValueMember = (ForeignKey.RelatedColumns[0] as System.Data.DataColumn).FieldName;
			  Cerebrum.DataContext.DataTableInfo map = tableMappings==null?null:tableMappings.FindByTableName(ForeignKey.RelatedTable.TableName);
			  if(map!=null && !map.IsDisplayFieldNameNull())
			  {
				DisplayMember = map.DisplayFieldName;
			  }
			  else
			  {
				DisplayMember = (ForeignKey.RelatedColumns[0] as System.Data.DataColumn).FieldName;
			  }
			  ComboBoxColumn.DisplayMember = DisplayMember;
			  ComboBoxColumn.ValueMember = ValueMember;
			  if(DataColumn.AllowDBNull)
			  {
				Cerebrum.Data.DataListUnion du = Tools.UnionFromDataTable(ForeignKey.RelatedTable, ValueMember, DisplayMember, System.DBNull.Value, "<null>");
				  /*new Cerebrum.Data.DataListUnion();
				dv.SelectColumns(new string[]{ValueMember, DisplayMember});
				System.Data.DataRow rw = ForeignKey.RelatedTable.NewRow();
				rw[ComboBoxColumn.DisplayMember] = "<null>";//ComboBoxColumn.NullText;
				rw[ComboBoxColumn.ValueMember] = System.DBNull.Value;
				dv.Add(rw);
				dv.Add(ForeignKey.RelatedTable);* /
				ComboBoxColumn.DataSource = du;
			  }
			  else
			  {
				ComboBoxColumn.DataSource = ForeignKey.RelatedTable;
			  }

			  GridColumns.Add(ComboBoxColumn);
			  //ColumnIndex = ColumnIndex+ 1;
			}
		  }

		  GridTable = new System.Windows.Forms.DataGridTableStyle();

		  GridTable.AlternatingBackColor = DataGrid.AlternatingBackColor;
		  GridTable.BackColor = DataGrid.BackColor;
		  GridTable.ForeColor = DataGrid.ForeColor;
		  GridTable.GridLineColor = DataGrid.GridLineColor;
		  GridTable.HeaderBackColor = DataGrid.HeaderBackColor;
		  GridTable.HeaderForeColor = DataGrid.HeaderForeColor;
		  GridTable.LinkColor = DataGrid.LinkColor;
		  // GridTable.AlternatingBackColor = System.Drawing.Color.LightYellow
		  // GridTable.BackColor = System.Drawing.Color.White
		  // GridTable.ForeColor = System.Drawing.Color.Black
		  // GridTable.GridLineColor = System.Drawing.Color.Gray
		  // GridTable.HeaderBackColor = System.Drawing.Color.PaleGoldenrod
		  // GridTable.HeaderForeColor = System.Drawing.Color.Black
		  // GridTable.LinkColor = System.Drawing.Color.Firebrick
		  // GridTable.GridTableName = DataTable.TableName
		  GridTable.MappingName = DataTable.TableName;
		  GridTable.DataGrid = DataGrid;
		  GridTable.GridColumnStyles.AddRange((System.Windows.Forms.DataGridColumnStyle[])GridColumns.ToArray(typeof(System.Windows.Forms.DataGridColumnStyle)));

		  DataGrid.BeginInit();
		  DataGrid.DataMember = "";
		  if (DataView == null)
		  {
			DataGrid.DataSource = DataTable.DataSet;
			DataGrid.DataMember = DataTable.TableName;
		  }

		  else
		  {
			DataView.Table = DataTable;
			DataGrid.DataSource = DataView;
		  }

		  DataGrid.TableStyles.Add(GridTable);
		  DataGrid.EndInit();
		  int i;
		  for (i=0; i <= GridTable.GridColumnStyles.Count- 1; i++)
		  {
			GridTable.GridColumnStyles[i].HeaderText = DataTable.Columns[GridTable.GridColumnStyles[i].MappingName].Caption;
		  }

		  return GridTable;*/
		//}
	}

}
