// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Data.Manager.Forms
{
	/// <summary>
	/// Summary description for frmObjectBrowser.
	/// </summary>
	public class frmExportMode : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button dsb_btnAccept;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button dsb_btnCancel;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.ComboBox ddlModeList;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;


		public frmExportMode()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			ddlModeList.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem("Standard V.1", "R1"));
			ddlModeList.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem("Standard V.2", "R2"));
			ddlModeList.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem("Extended V.1", "N1"));
			ddlModeList.SelectedIndex = 0;
			ddlModeList_SelectedIndexChanged(ddlModeList, null);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.dsb_btnCancel = new System.Windows.Forms.Button();
			this.dsb_btnAccept = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.ddlModeList = new System.Windows.Forms.ComboBox();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dsb_btnCancel);
			this.panel1.Controls.Add(this.dsb_btnAccept);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 53);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(292, 40);
			this.panel1.TabIndex = 0;
			// 
			// dsb_btnCancel
			// 
			this.dsb_btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_btnCancel.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.dsb_btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnCancel.Location = new System.Drawing.Point(208, 8);
			this.dsb_btnCancel.Name = "dsb_btnCancel";
			this.dsb_btnCancel.TabIndex = 2;
			this.dsb_btnCancel.Text = "Cancel";
			// 
			// dsb_btnAccept
			// 
			this.dsb_btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.dsb_btnAccept.BackColor = System.Drawing.Color.Turquoise;
			this.dsb_btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.dsb_btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dsb_btnAccept.Location = new System.Drawing.Point(120, 8);
			this.dsb_btnAccept.Name = "dsb_btnAccept";
			this.dsb_btnAccept.TabIndex = 1;
			this.dsb_btnAccept.Text = "&Ok";
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.MediumAquamarine;
			this.panel2.Controls.Add(this.ddlModeList);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(292, 53);
			this.panel2.TabIndex = 1;
			// 
			// ddlModeList
			// 
			this.ddlModeList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ddlModeList.Location = new System.Drawing.Point(16, 16);
			this.ddlModeList.Name = "ddlModeList";
			this.ddlModeList.Size = new System.Drawing.Size(264, 21);
			this.ddlModeList.TabIndex = 2;
			this.ddlModeList.SelectedIndexChanged += new System.EventHandler(this.ddlModeList_SelectedIndexChanged);
			// 
			// frmExportMode
			// 
			this.AcceptButton = this.dsb_btnAccept;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.CancelButton = this.dsb_btnCancel;
			this.ClientSize = new System.Drawing.Size(292, 93);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "frmExportMode";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Export mode";
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void ddlModeList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			m_SelectedMode = Convert.ToString((ddlModeList.SelectedItem as Cerebrum.Windows.Forms.DropDownStringItem).Value);
		}

		private string m_SelectedMode;
		public string SelectedMode
		{
			get
			{
				return m_SelectedMode;
			}
		}
	}
}
