// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Samples.Objects01
{
	/// <summary>
	/// Summary description for CollectionTest.
	/// </summary>
	public class CollectionTest : Cerebrum.Windows.Forms.DesktopComponent
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CollectionTest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnInitialize = new System.Windows.Forms.Button();
			this.txtIndex = new System.Windows.Forms.TextBox();
			this.txtValue = new System.Windows.Forms.TextBox();
			this.btnGet = new System.Windows.Forms.Button();
			this.btnSet = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnInitialize
			// 
			this.btnInitialize.Location = new System.Drawing.Point(32, 16);
			this.btnInitialize.Name = "btnInitialize";
			this.btnInitialize.TabIndex = 0;
			this.btnInitialize.Text = "Initialize";
			this.btnInitialize.Click += new System.EventHandler(this.btnInitialize_Click);
			// 
			// txtIndex
			// 
			this.txtIndex.Location = new System.Drawing.Point(32, 72);
			this.txtIndex.Name = "txtIndex";
			this.txtIndex.TabIndex = 1;
			this.txtIndex.Text = "";
			// 
			// txtValue
			// 
			this.txtValue.Location = new System.Drawing.Point(32, 96);
			this.txtValue.Name = "txtValue";
			this.txtValue.TabIndex = 2;
			this.txtValue.Text = "textBox2";
			// 
			// btnGet
			// 
			this.btnGet.Location = new System.Drawing.Point(176, 96);
			this.btnGet.Name = "btnGet";
			this.btnGet.TabIndex = 3;
			this.btnGet.Text = "Get";
			this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
			// 
			// btnSet
			// 
			this.btnSet.Location = new System.Drawing.Point(176, 120);
			this.btnSet.Name = "btnSet";
			this.btnSet.TabIndex = 4;
			this.btnSet.Text = "Set";
			this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(176, 72);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.TabIndex = 5;
			this.btnAdd.Text = "Add";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// CollectionTest
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.btnSet);
			this.Controls.Add(this.btnGet);
			this.Controls.Add(this.txtValue);
			this.Controls.Add(this.txtIndex);
			this.Controls.Add(this.btnInitialize);
			this.Name = "CollectionTest";
			this.Text = "CollectionTest";
			this.ResumeLayout(false);

		}
		#endregion

		private System.Windows.Forms.Button btnInitialize;
		private System.Windows.Forms.TextBox txtIndex;
		private System.Windows.Forms.TextBox txtValue;
		private System.Windows.Forms.Button btnGet;
		private System.Windows.Forms.Button btnSet;
		private System.Windows.Forms.Button btnAdd;

		private readonly Cerebrum.ObjectHandle coll = new Cerebrum.ObjectHandle(333);

		private void btnInitialize_Click(object sender, System.EventArgs e)
		{
			using(Cerebrum.IConnector cnt = this.DomainContext.AttachConnector(coll))
			{
				if(cnt==null)
				{
					using(Cerebrum.Runtime.NativeSector sector = this.DomainContext.GetSector())
					{
						sector.CreateConnector(coll, Cerebrum.Samples.Objects01.Specialized.Concepts.ObjectCollectionType(this.DomainContext)).Dispose();
					}
				}
			}
		
		}

		private void btnGet_Click(object sender, System.EventArgs e)
		{
			using(Cerebrum.IConnector cnt = this.DomainContext.AttachConnector(coll))
			{
				if(cnt!=null)
				{
					txtValue.Text = Convert.ToString((cnt.Component as ObjectCollection).GetObject(Convert.ToInt32(txtIndex.Text)));
				}
			}
		}

		private void btnSet_Click(object sender, System.EventArgs e)
		{
			using(Cerebrum.IConnector cnt = this.DomainContext.AttachConnector(coll))
			{
				if(cnt!=null)
				{
					(cnt.Component as ObjectCollection).UpdateObject(Convert.ToInt32(txtIndex.Text), txtValue.Text);
				}
			}
		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			using(Cerebrum.IConnector cnt = this.DomainContext.AttachConnector(coll))
			{
				if(cnt!=null)
				{
					(cnt.Component as ObjectCollection).AddObject(txtValue.Text);
				}
			}
		}
	}
}
