// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Objects01
{
	/// <summary>
	/// Summary description for SymbolNeuron.
	/// </summary>
	public class SymbolNeuron : NeuronBase, ILineralTreeNodeEx
	{
		public SymbolNeuron()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		
		public char Symbol
		{
			get
			{
				return Convert.ToChar(GetAttributeComponent(Cerebrum.Samples.Objects01.Specialized.Concepts.SymbolAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Samples.Objects01.Specialized.Concepts.SymbolAttribute(this.DomainContext), Convert.ToInt32(value));
			}
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						warden.Newobj(Cerebrum.Samples.Objects01.Specialized.Concepts.SymbolAttribute(this.DomainContext), Cerebrum.Runtime.KernelObjectKindId.Scalar);
					}
					break;
				}
			}
		}

		#region ILineralTreeNode Members

		public Cerebrum.ObjectHandle[] GenerateDependents(Cerebrum.IComposite outer, string text)
		{
			//��������� ������� ���������
			if(text==null || text.Length<1) return null;
			
			//�������� ������ ������ �� ������
			char symbol = text[0];
			
			//���� ������ ������ ��������� � ��� ��� ���������� ���� ������ - ��� ���� ������
			if(this.Symbol!=symbol) return null;

			//�������� ������� ������
			string tail = text.Substring(1);

			if(tail.Length>0)
			{
				//�������� ����� �������, ������������ ������� ������
				using(Cerebrum.Runtime.NativeWarden vector = this.GetDependentObjectsListVector())
				{
					foreach(System.Collections.DictionaryEntry de in vector)
					{
						Cerebrum.ObjectHandle h = (Cerebrum.ObjectHandle)de.Key;
						using(Cerebrum.IComposite connector = this.m_Connector.Workspace.AttachConnector(h))
						{
							ILineralTreeNode node = connector.Component as ILineralTreeNode;
							if(node!=null)
							{
								//����������� ����� ���� �� ������� �� ��� � ������� �������
								Cerebrum.ObjectHandle[] hs = node.GenerateDependents(connector, tail);
								if(hs!=null)
								{
									Cerebrum.ObjectHandle[] hs2 = new Cerebrum.ObjectHandle[hs.Length + 1];
									hs.CopyTo(hs2, 1);
									hs2[0] = outer.PlasmaHandle;
									return hs2;
								}
							}
						}
					}
				}
				//���� �� �����, �� ����� ������ �� ������� - ������� �� ���� ��������� (������� ����).

				// ������� ID ��� ������ �������
				Cerebrum.ObjectHandle h0 = this.m_Connector.Workspace.NextSequence();
				// ������� ����� ��������� �������
				using(Cerebrum.IComposite connector = this.m_Connector.Workspace.CreateConnector(h0, Cerebrum.Samples.Objects01.Specialized.Concepts.SymbolNeuronType(this.DomainContext)))
				{
					// � ����� �� �������� � ILineralTreeNodeEx
					// ��� ���� ���� ��������� �� ������� AddPrecedent ������� �� ������� � ���������� ILineralTreeNode
					ILineralTreeNodeEx node = connector.Component as ILineralTreeNodeEx;
					if(node!=null)
					{
						//������ ������� ��� ������ ��� �������������
						node.Symbol = tail[0];
						//��������� ��������� ������ � ����� - ��������� ���� � ��� ��������� Precedents � ��� � ���� ��������� Dependents
						node.AddPrecedent(outer.PlasmaHandle);
						this.AddDependent(connector.PlasmaHandle);

						//����������� ����� ���� �� ������� �� ��� � ������ �������
						Cerebrum.ObjectHandle[] hs = node.GenerateDependents(connector, tail);
						if(hs!=null)
						{
							//��������� ������ �� ���� � ������ ��������������� �������, ��� ��� �� �������� �����
							Cerebrum.ObjectHandle[] hs2 = new Cerebrum.ObjectHandle[hs.Length + 1];
							hs.CopyTo(hs2, 1);
							hs2[0] = outer.PlasmaHandle;
							return hs2;
						}
					}
				}
			}
			// �� ���� ���������� � ������ - ������ ������������ ������, ���������� ����
			return new Cerebrum.ObjectHandle[] {outer.PlasmaHandle};
		}

		public string[] RestoreStrings()
		{
			//���������� ���� ���������������� � �������� ������, ������� ��� ����������
			System.Collections.ArrayList result = new System.Collections.ArrayList();
			using(Cerebrum.Runtime.NativeWarden vector = this.GetPrecedentObjectsListVector())
			{
				foreach(System.Collections.DictionaryEntry de in vector)
				{
					Cerebrum.ObjectHandle h = (Cerebrum.ObjectHandle)de.Key;
					using(Cerebrum.IConnector connector = this.m_Connector.Workspace.AttachConnector(h))
					{
						ILineralTreeNode node = connector.Component as ILineralTreeNode;
						if(node!=null)
						{
							//����������� ����� ���� �� ������� �� ��� � ������� �������
							string [] heads = node.RestoreStrings();
							//������ � heads ����� ���� ������ ���� ������ - ��� ��� ������ �������������� � ������ ������ ����
							//�� ������ ��������� �������������� � ���������� ����� ��������������� ����� ����������� ���������������
							//����� ������� ������� �� ������� �����������������
							foreach(string head in heads)
							{
								result.Add(head + this.Symbol.ToString());
							}
						}
					}
				}
			}
			// ��� �������� ���� ��� ���������������� - ��������� ���� � ������ ������
			if(result.Count<1)
			{
				result.Add(this.Symbol.ToString());
			}
			return (string[]) result.ToArray(typeof(string));
		}


		#endregion

		public void FindAllLeafs(System.Collections.ArrayList leafs)
		{
			bool hasDependents = false;
			//������� ��� �������� ����
			using(Cerebrum.Runtime.NativeWarden vector = this.GetDependentObjectsListVector())
			{
				foreach(System.Collections.DictionaryEntry de in vector)
				{
					Cerebrum.ObjectHandle h = (Cerebrum.ObjectHandle)de.Key;
					using(Cerebrum.IConnector connector = this.m_Connector.Workspace.AttachConnector(h))
					{
						ILineralTreeNodeEx node = connector.Component as ILineralTreeNodeEx;
						if(node!=null)
						{
							//����������� ����� ���� �� ������� �� ��� � ������� �������
							node.FindAllLeafs(leafs);
							hasDependents = true;
						}
					}
				}
			}
			//���� ��� ��������� ���� �� � ���� ��� ��������� - �������� ����.
			if(!hasDependents)
			{
				leafs.AddRange(this.RestoreStrings());
			}
		}
	}
}
