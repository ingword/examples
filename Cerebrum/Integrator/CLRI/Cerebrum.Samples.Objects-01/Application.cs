// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Objects01
{
	/// <summary>
	/// Summary description for Application.
	/// </summary>
	public class Application : Cerebrum.Windows.Forms.Application 
	{
		public Application()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public override Cerebrum.Integrator.DomainContext CreateMasterContext(string domainName, bool creating)
		{
			return new Cerebrum.Integrator.DomainContext(this);
		}

		protected override System.Windows.Forms.Form CreatePrimaryWindow()
		{
			return new PrimaryWindow();
		}

		protected override Cerebrum.Integrator.IContextFactory CreateContextFactory()
		{
			string primaryDirectory = this.ApplicationDirectory;
			string databaseFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.bin");
			string activityFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.log");
			string templateFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.xdb");

			return new Cerebrum.Management.SimpleContextFactory(this, databaseFileName, activityFileName, 4, true, templateFileName, false);
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application app = new Application();
			Cerebrum.Windows.Forms.Application.Instance = app;
			app.Initialize();
			app.Show();
			System.Windows.Forms.Application.Run(app.PrimaryWindow);
			app.Shutdown();
			app.Dispose();
			Cerebrum.Runtime.Environment.Current.Dispose();
		}
	}
}
