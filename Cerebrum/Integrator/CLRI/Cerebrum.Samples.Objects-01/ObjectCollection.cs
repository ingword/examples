// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Objects01
{
	/// <summary>
	/// Summary description for ObjectCollection.
	/// </summary>
	public class ObjectCollection : Cerebrum.Integrator.GenericComponent 
	{
		private class Concepts
		{
			internal static readonly Cerebrum.ObjectHandle CollectionCountAttribute = new Cerebrum.ObjectHandle(1);
		}

		public ObjectCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		//���� ������� ����� ����������� �������� ����������. ��� ���� �������� - �� ����
		public int Count
		{
			get
			{
				return Convert.ToInt32(this.GetAttributeComponent(Concepts.CollectionCountAttribute));
			}
			set
			{
				this.SetAttributeComponent(Concepts.CollectionCountAttribute, Convert.ToInt32(value));
			}
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						warden.Newobj(Concepts.CollectionCountAttribute, Cerebrum.Runtime.KernelObjectKindId.Scalar);
						this.Count = 0;
					}
					break;
				}
			}
		}

		public int AddObject(object value)
		{
			int count = this.Count;

			count ++;

			Cerebrum.ObjectHandle h = new Cerebrum.ObjectHandle(count + 1); // because h=0 is null and h=1 is reserved for count

			//��� ������ ����� ��� ��� �������� h �� ������ � ������� ���������� - ������� ��� � ������.
			using(Cerebrum.Runtime.NativeWarden container = this.GetChildComponents() as Cerebrum.Runtime.NativeWarden)
			{
				container.Newobj(h, Cerebrum.Runtime.KernelObjectKindId.Scalar);
			}

			this.SetAttributeComponent(h, value);

			this.Count = count;

			return count - 1;
 
		}

 

		public void UpdateObject(int nIndex, object value)
		{
			Cerebrum.ObjectHandle h = new Cerebrum.ObjectHandle(nIndex + 2); // because h=0 is null and h=1 is reserved for count

			this.SetAttributeComponent(h, value);
		}

 

		public void DeleteObject(int nIndex)
		{
			Cerebrum.ObjectHandle h = new Cerebrum.ObjectHandle(nIndex + 2); // because h=0 is null and h=1 is reserved for count

			using(Cerebrum.IContainer container = this.GetChildComponents())
			{
				container.RemoveConnector(h);
			}
		}

 

		public object GetObject(int nIndex)
		{
			Cerebrum.ObjectHandle h = new Cerebrum.ObjectHandle(nIndex + 2); // because h=0 is null and h=1 is reserved for count

			return this.GetAttributeComponent(h);
		}

	}
}
