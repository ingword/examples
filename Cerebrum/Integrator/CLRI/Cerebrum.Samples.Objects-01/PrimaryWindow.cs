// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Samples.Objects01
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class PrimaryWindow : Cerebrum.Windows.Forms.DesktopComponent 
	{
		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.Button btnGenerate;
		private System.Windows.Forms.Button btnRestore;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnLoadText;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.Button btnRestoreText;
		private System.Windows.Forms.Button btnFindAllLeafs;
		private System.Windows.Forms.TextBox txtString;
		private System.Windows.Forms.TextBox txtHandle;
		private System.Windows.Forms.Button btnBeginTran;
		private System.Windows.Forms.Button btnCommitTran;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.Button btnCollect;
		private System.Windows.Forms.Button btnCollection;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PrimaryWindow() : base(Cerebrum.Windows.Forms.DesktopComponentDisposition.Alone)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.txtString = new System.Windows.Forms.TextBox();
			this.txtHandle = new System.Windows.Forms.TextBox();
			this.btnGenerate = new System.Windows.Forms.Button();
			this.btnRestore = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnCollect = new System.Windows.Forms.Button();
			this.lblMessage = new System.Windows.Forms.Label();
			this.btnCommitTran = new System.Windows.Forms.Button();
			this.btnBeginTran = new System.Windows.Forms.Button();
			this.btnFindAllLeafs = new System.Windows.Forms.Button();
			this.btnRestoreText = new System.Windows.Forms.Button();
			this.btnLoadText = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.btnCollection = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataGrid1
			// 
			this.dataGrid1.DataMember = "";
			this.dataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid1.Location = new System.Drawing.Point(0, 0);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.Size = new System.Drawing.Size(624, 373);
			this.dataGrid1.TabIndex = 0;
			// 
			// txtString
			// 
			this.txtString.Location = new System.Drawing.Point(8, 16);
			this.txtString.Name = "txtString";
			this.txtString.TabIndex = 1;
			this.txtString.Text = "";
			// 
			// txtHandle
			// 
			this.txtHandle.Location = new System.Drawing.Point(8, 48);
			this.txtHandle.Name = "txtHandle";
			this.txtHandle.TabIndex = 2;
			this.txtHandle.Text = "";
			// 
			// btnGenerate
			// 
			this.btnGenerate.Location = new System.Drawing.Point(136, 16);
			this.btnGenerate.Name = "btnGenerate";
			this.btnGenerate.TabIndex = 3;
			this.btnGenerate.Text = "Generate";
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			// 
			// btnRestore
			// 
			this.btnRestore.Location = new System.Drawing.Point(136, 48);
			this.btnRestore.Name = "btnRestore";
			this.btnRestore.TabIndex = 4;
			this.btnRestore.Text = "Restore";
			this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCollection);
			this.panel1.Controls.Add(this.btnCollect);
			this.panel1.Controls.Add(this.lblMessage);
			this.panel1.Controls.Add(this.btnCommitTran);
			this.panel1.Controls.Add(this.btnBeginTran);
			this.panel1.Controls.Add(this.btnFindAllLeafs);
			this.panel1.Controls.Add(this.btnRestoreText);
			this.panel1.Controls.Add(this.btnLoadText);
			this.panel1.Controls.Add(this.btnRestore);
			this.panel1.Controls.Add(this.btnGenerate);
			this.panel1.Controls.Add(this.txtHandle);
			this.panel1.Controls.Add(this.txtString);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 261);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(624, 112);
			this.panel1.TabIndex = 5;
			// 
			// btnCollect
			// 
			this.btnCollect.Location = new System.Drawing.Point(224, 80);
			this.btnCollect.Name = "btnCollect";
			this.btnCollect.TabIndex = 11;
			this.btnCollect.Text = "Collect";
			this.btnCollect.Click += new System.EventHandler(this.btnCollect_Click);
			// 
			// lblMessage
			// 
			this.lblMessage.AutoSize = true;
			this.lblMessage.Location = new System.Drawing.Point(328, 48);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(50, 16);
			this.lblMessage.TabIndex = 10;
			this.lblMessage.Text = "Message";
			// 
			// btnCommitTran
			// 
			this.btnCommitTran.Location = new System.Drawing.Point(224, 48);
			this.btnCommitTran.Name = "btnCommitTran";
			this.btnCommitTran.TabIndex = 9;
			this.btnCommitTran.Text = "Commit";
			this.btnCommitTran.Click += new System.EventHandler(this.btnCommitTran_Click);
			// 
			// btnBeginTran
			// 
			this.btnBeginTran.Location = new System.Drawing.Point(224, 16);
			this.btnBeginTran.Name = "btnBeginTran";
			this.btnBeginTran.TabIndex = 8;
			this.btnBeginTran.Text = "Begin Tran";
			this.btnBeginTran.Click += new System.EventHandler(this.btnBeginTran_Click);
			// 
			// btnFindAllLeafs
			// 
			this.btnFindAllLeafs.Location = new System.Drawing.Point(520, 16);
			this.btnFindAllLeafs.Name = "btnFindAllLeafs";
			this.btnFindAllLeafs.Size = new System.Drawing.Size(96, 23);
			this.btnFindAllLeafs.TabIndex = 7;
			this.btnFindAllLeafs.Text = "Find All Leafs";
			this.btnFindAllLeafs.Click += new System.EventHandler(this.btnFindAllLeafs_Click);
			// 
			// btnRestoreText
			// 
			this.btnRestoreText.Location = new System.Drawing.Point(424, 16);
			this.btnRestoreText.Name = "btnRestoreText";
			this.btnRestoreText.Size = new System.Drawing.Size(88, 23);
			this.btnRestoreText.TabIndex = 6;
			this.btnRestoreText.Text = "Restore Text";
			this.btnRestoreText.Click += new System.EventHandler(this.btnRestoreText_Click);
			// 
			// btnLoadText
			// 
			this.btnLoadText.Location = new System.Drawing.Point(328, 16);
			this.btnLoadText.Name = "btnLoadText";
			this.btnLoadText.Size = new System.Drawing.Size(88, 23);
			this.btnLoadText.TabIndex = 5;
			this.btnLoadText.Text = "Load Text";
			this.btnLoadText.Click += new System.EventHandler(this.btnLoadText_Click);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.Filter = "Text files *.txt|*.txt";
			// 
			// saveFileDialog1
			// 
			this.saveFileDialog1.Filter = "Text files *.txt|*.txt";
			// 
			// btnCollection
			// 
			this.btnCollection.Location = new System.Drawing.Point(352, 80);
			this.btnCollection.Name = "btnCollection";
			this.btnCollection.TabIndex = 12;
			this.btnCollection.Text = "Collection";
			this.btnCollection.Click += new System.EventHandler(this.btnCollection_Click);
			// 
			// PrimaryWindow
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(624, 373);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.dataGrid1);
			this.Name = "PrimaryWindow";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.PrimaryWindow_Load);
			this.Closed += new System.EventHandler(this.PrimaryWindow_Closed);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		Cerebrum.Data.TableView view;

		private void PrimaryWindow_Load(object sender, System.EventArgs e)
		{
			using(Cerebrum.IConnector connector = this.DomainContext.GetTable("Neurons"))
			{
				view = (connector.Component as Cerebrum.Reflection.TableDescriptor).GetTableView();
				dataGrid1.DataSource = view;
			}
		}

		private void PrimaryWindow_Closed(object sender, System.EventArgs e)
		{
			view.Dispose();
		}

		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			using(Cerebrum.IComposite connector = view[0].GetComposite())
			{
				Cerebrum.Samples.Objects01.ILineralTreeNode node = connector.Component as Cerebrum.Samples.Objects01.ILineralTreeNode;
				Cerebrum.ObjectHandle [] hs = node.GenerateDependents(connector, "\0" + txtString.Text);
				//��� ��������� ������ ��������� �������������� ������, 
				//������� ���������� ��������� ID � �������
				txtHandle.Text = hs[hs.Length-1].ToString();
			}
		}


		private void btnRestore_Click(object sender, System.EventArgs e)
		{
			Cerebrum.ObjectHandle h = Cerebrum.ObjectHandle.Parse(txtHandle.Text);
			using(Cerebrum.IConnector connector = this.DomainContext.AttachConnector(h))
			{
				Cerebrum.Samples.Objects01.ILineralTreeNode node = connector.Component as Cerebrum.Samples.Objects01.ILineralTreeNode;
				string [] res = node.RestoreStrings();
				txtString.Text = res[0].Substring(1);
			}
		}

		private void btnLoadText_Click(object sender, System.EventArgs e)
		{
			System.DateTime start = System.DateTime.Now;

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			if(openFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)
			{
				if(saveFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)
				{
					string text;
					using(System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName, System.Text.Encoding.Default))
					{
						text = sr.ReadToEnd();
					}
					string []words = text.Split(' ', ',', '.', '\r', '\n', '\t');

					foreach(string word in words)
					{
						using(Cerebrum.IComposite connector = view[0].GetComposite())
						{
							Cerebrum.Samples.Objects01.ILineralTreeNode node = connector.Component as Cerebrum.Samples.Objects01.ILineralTreeNode;
							Cerebrum.ObjectHandle [] hs = node.GenerateDependents(connector, "\0" + word.ToLower());
							sb.Append(hs[hs.Length-1].ToString());
							sb.Append("\r\n");
						}
					}
					using(System.IO.Stream fs = saveFileDialog1.OpenFile())
					{
						System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
						sw.Write(sb.ToString());
						sw.Close();
					}
				}
			}
			System.TimeSpan elapsed = System.DateTime.Now - start;
			lblMessage.Text = elapsed.TotalMilliseconds.ToString();
		}

		private void btnRestoreText_Click(object sender, System.EventArgs e)
		{
			System.DateTime start = System.DateTime.Now;

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			if(openFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)
			{
				if(saveFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)
				{
					string text;
					using(System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName, System.Text.Encoding.Default))
					{
						text = sr.ReadToEnd();
					}
					string []codes = text.Split(' ', ',', '.', '\r', '\n');
					foreach(string code in codes)
					{
						if(code.Trim().Length<1) continue;
						Cerebrum.ObjectHandle h = Cerebrum.ObjectHandle.Parse(code);
						using(Cerebrum.IConnector connector = this.DomainContext.AttachConnector(h))
						{
							Cerebrum.Samples.Objects01.ILineralTreeNode node = connector.Component as Cerebrum.Samples.Objects01.ILineralTreeNode;
							string [] res = node.RestoreStrings();
							sb.Append(res[0].Substring(1));
							sb.Append("\r\n");
						}
					}
					using(System.IO.Stream fs = saveFileDialog1.OpenFile())
					{
						System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
						sw.Write(sb.ToString());
						sw.Close();
					}
				}
			}
			System.TimeSpan elapsed = System.DateTime.Now - start;
			lblMessage.Text = elapsed.TotalMilliseconds.ToString();
		}

		private void btnFindAllLeafs_Click(object sender, System.EventArgs e)
		{
			System.DateTime start = System.DateTime.Now;

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			System.Collections.ArrayList ar = new ArrayList();
			if(saveFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)
			{
				using(Cerebrum.IConnector connector = view[0].GetComposite())
				{
					ILineralTreeNodeEx node = connector.Component as ILineralTreeNodeEx;
					node.FindAllLeafs(ar);
				}

				foreach(string s in ar)
				{
					sb.Append(s.Substring(1));
					sb.Append("\r\n");
				}

				using(System.IO.Stream fs = saveFileDialog1.OpenFile())
				{
					System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
					sw.Write(sb.ToString());
					sw.Close();
				}
			}
			System.TimeSpan elapsed = System.DateTime.Now - start;
			lblMessage.Text = elapsed.TotalMilliseconds.ToString();
		}

		private void btnCommitTran_Click(object sender, System.EventArgs e)
		{
			this.DomainContext.CommitTransaction();
		}

		private void btnBeginTran_Click(object sender, System.EventArgs e)
		{
			this.DomainContext.BeginTransaction(true);
		}

		private void btnCollect_Click(object sender, System.EventArgs e)
		{
			System.GC.Collect();
			System.GC.WaitForPendingFinalizers();
			Cerebrum.Runtime.Environment.Current.Collect();
		}

		private void btnCollection_Click(object sender, System.EventArgs e)
		{
			Cerebrum.Samples.Objects01.CollectionTest dlg = new CollectionTest();
			(dlg as Cerebrum.IComponent).SetConnector(Cerebrum.SerializeDirection.None, this.m_Connector);
			dlg.ShowDialog();
		}
	}
}
