// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Objects01
{
	/// <summary>
	/// Summary description for NeuronBase.
	/// </summary>
	public class NeuronBase : Cerebrum.Integrator.GenericComponent 
	{
		public NeuronBase()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public Cerebrum.Runtime.NativeWarden GetPrecedentObjectsListVector()
		{
			return this.GetAttributeContainer(Cerebrum.Samples.Objects01.Specialized.Concepts.PrecedentObjectsListAttribute(this.DomainContext)) as Cerebrum.Runtime.NativeWarden;
		}

		public Cerebrum.Runtime.NativeWarden GetDependentObjectsListVector()
		{
			return this.GetAttributeContainer(Cerebrum.Samples.Objects01.Specialized.Concepts.DependentObjectsListAttribute(this.DomainContext)) as Cerebrum.Runtime.NativeWarden;
		}

		public void AddPrecedent(Cerebrum.ObjectHandle h)
		{
			using(Cerebrum.Runtime.NativeWarden v = GetPrecedentObjectsListVector())
			{
				v.SetMap(h, Cerebrum.Runtime.MapAccess.Scalar, new Cerebrum.ObjectHandle(1));
			}
		}

		public void AddDependent(Cerebrum.ObjectHandle h)
		{
			using(Cerebrum.Runtime.NativeWarden v = GetDependentObjectsListVector())
			{
				v.SetMap(h, Cerebrum.Runtime.MapAccess.Scalar, new Cerebrum.ObjectHandle(1));
			}
		}

		public void DelPrecedent(Cerebrum.ObjectHandle h)
		{
			using(Cerebrum.Runtime.NativeWarden v = GetPrecedentObjectsListVector())
			{
				v.SetMap(h, Cerebrum.Runtime.MapAccess.Scalar, Cerebrum.ObjectHandle.Null);
			}
		}

		public void DelDependent(Cerebrum.ObjectHandle h)
		{
			using(Cerebrum.Runtime.NativeWarden v = GetDependentObjectsListVector())
			{
				v.SetMap(h, Cerebrum.Runtime.MapAccess.Scalar, Cerebrum.ObjectHandle.Null);
			}
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				{
					using(Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
					{
						warden.Newobj(Cerebrum.Samples.Objects01.Specialized.Concepts.PrecedentObjectsListAttribute(this.DomainContext), Cerebrum.Runtime.KernelObjectKindId.Warden);
						warden.Newobj(Cerebrum.Samples.Objects01.Specialized.Concepts.DependentObjectsListAttribute(this.DomainContext), Cerebrum.Runtime.KernelObjectKindId.Warden);
					}
					break;
				}
			}
		}
	}
}
