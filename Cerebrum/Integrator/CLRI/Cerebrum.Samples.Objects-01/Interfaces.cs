// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Objects01
{
	/// <summary>
	/// Summary description for ILineralTreeNode.
	/// </summary>
	public interface ILineralTreeNode
	{
		Cerebrum.ObjectHandle[] GenerateDependents(Cerebrum.IComposite outer, string text);
		string [] RestoreStrings();
		char Symbol {get;set;}
	}
	
	public interface ILineralTreeNodeEx : INeuronLinks, ILineralTreeNode
	{
		void FindAllLeafs(System.Collections.ArrayList leafs);
	}

	public interface INeuronLinks
	{
		void AddPrecedent(Cerebrum.ObjectHandle h);
		void AddDependent(Cerebrum.ObjectHandle h);

		void DelPrecedent(Cerebrum.ObjectHandle h);
		void DelDependent(Cerebrum.ObjectHandle h);
	}
}
