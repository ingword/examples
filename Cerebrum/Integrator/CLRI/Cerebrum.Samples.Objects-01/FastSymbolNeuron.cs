// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Objects01
{
	/// <summary>
	/// Summary description for FastSymbolNeuron.
	/// </summary>
	public class FastSymbolNeuron : Cerebrum.Integrator.GenericComponent, Cerebrum.IPersistent, Cerebrum.IDiscovery, ILineralTreeNodeEx
	{
		//private System.Collections.ArrayList m_Precedents;
		//private System.Collections.ArrayList m_Dependents;
		private Cerebrum.Collections.Specialized.HandleCollection m_Precedents;
		private Cerebrum.Collections.Specialized.HandleCollection m_Dependents;
		private int m_Symbol;
		private bool m_IsChanged;

		public FastSymbolNeuron()
		{
			m_IsChanged = true;
			m_Precedents = null;
			m_Dependents = null;
		}

		#region ILineralTreeNodeEx Members

		public void FindAllLeafs(System.Collections.ArrayList leafs)
		{
			bool hasDependents = false;
			//������� ��� �������� ����
			foreach(Cerebrum.ObjectHandle h in m_Dependents)
			{
				using(Cerebrum.IConnector connector = this.DomainContext.AttachConnector(h))
				{
					ILineralTreeNodeEx node = connector.Component as ILineralTreeNodeEx;
					if(node!=null)
					{
						//����������� ����� ���� �� ������� �� ��� � ������� �������
						node.FindAllLeafs(leafs);
						hasDependents = true;
					}
				}
			}
			//���� ��� ��������� ���� �� � ���� ��� ��������� - �������� ����.
			if(!hasDependents)
			{
				leafs.AddRange(this.RestoreStrings());
			}
		}

		#endregion

		#region INeuronLinks Members

		public void AddPrecedent(Cerebrum.ObjectHandle h)
		{
			m_Precedents.Add(h);
			m_IsChanged = true;
		}

		public void AddDependent(Cerebrum.ObjectHandle h)
		{
			m_Dependents.Add(h);
			m_IsChanged = true;
		}

		public void DelPrecedent(Cerebrum.ObjectHandle h)
		{
			m_Precedents.Remove(h);
			m_IsChanged = true;
		}

		public void DelDependent(Cerebrum.ObjectHandle h)
		{
			m_Dependents.Remove(h);
			m_IsChanged = true;
		}

		#endregion

		#region ILineralTreeNode Members

		public Cerebrum.ObjectHandle[] GenerateDependents(Cerebrum.IComposite outer, string text)
		{
			//��������� ������� ���������
			if(text==null || text.Length<1) return null;
			
			//�������� ������ ������ �� ������
			char symbol = text[0];
			
			//���� ������ ������ ��������� � ��� ��� ���������� ���� ������ - ��� ���� ������
			if(this.Symbol!=symbol) return null;

			//�������� ������� ������
			string tail = text.Substring(1);

			if(tail.Length>0)
			{
				//�������� ����� �������, ������������ ������� ������
				foreach(Cerebrum.ObjectHandle h in m_Dependents)
				{
					using(Cerebrum.IComposite connector = this.DomainContext.AttachConnector(h))
					{
						ILineralTreeNode node = connector.Component as ILineralTreeNode;
						if(node!=null)
						{
							//����������� ����� ���� �� ������� �� ��� � ������� �������
							Cerebrum.ObjectHandle[] hs = node.GenerateDependents(connector, tail);
							if(hs!=null)
							{
								Cerebrum.ObjectHandle[] hs2 = new Cerebrum.ObjectHandle[hs.Length + 1];
								hs.CopyTo(hs2, 1);
								hs2[0] = outer.PlasmaHandle;
								return hs2;
							}
						}
					}
				}
				//���� �� �����, �� ����� ������ �� ������� - ������� �� ���� ��������� (������� ����).

				// ������� ID ��� ������ �������
				Cerebrum.ObjectHandle h0 = this.DomainContext.Workspace.NextSequence();
				// ������� ����� ��������� �������
				using(Cerebrum.IComposite connector = this.DomainContext.Workspace.CreateConnector(h0, Cerebrum.Samples.Objects01.Specialized.Concepts.SymbolNeuronType(this.DomainContext)))
				{
					// � ����� �� �������� � ILineralTreeNodeEx
					// ��� ���� ���� ��������� �� ������� AddPrecedent ������� �� ������� � ���������� ILineralTreeNode
					ILineralTreeNodeEx node = connector.Component as ILineralTreeNodeEx;
					if(node!=null)
					{
						//������ ������� ��� ������ ��� �������������
						node.Symbol = tail[0];
						//��������� ��������� ������ � ����� - ��������� ���� � ��� ��������� Precedents � ��� � ���� ��������� Dependents
						node.AddPrecedent(outer.PlasmaHandle);
						this.AddDependent(connector.PlasmaHandle);

						//����������� ����� ���� �� ������� �� ��� � ������ �������
						Cerebrum.ObjectHandle[] hs = node.GenerateDependents(connector, tail);
						if(hs!=null)
						{
							Cerebrum.ObjectHandle[] hs2 = new Cerebrum.ObjectHandle[hs.Length + 1];
							hs.CopyTo(hs2, 1);
							hs2[0] = outer.PlasmaHandle;
							return hs2;
						}
					}
				}
			}
			// �� ���� ���������� � ������ - ������ ������������ ������, ���������� ����
			return new Cerebrum.ObjectHandle[] {outer.PlasmaHandle};
		}

		public string[] RestoreStrings()
		{
			//���������� ���� ���������������� � �������� ������, ������� ��� ����������
			System.Collections.ArrayList result = new System.Collections.ArrayList();
			foreach(Cerebrum.ObjectHandle h in m_Precedents)
			{
				using(Cerebrum.IConnector connector = this.DomainContext.AttachConnector(h))
				{
					ILineralTreeNode node = connector.Component as ILineralTreeNode;
					if(node!=null)
					{
						//����������� ����� ���� �� ������� �� ��� � ������� �������
						string [] heads = node.RestoreStrings();
						//������ � heads ����� ���� ������ ���� ������ - ��� ��� ������ �������������� � ������ ������ ����
						//�� ������ ��������� �������������� � ���������� ����� ��������������� ����� ����������� ���������������
						//����� ������� ������� �� ������� �����������������
						foreach(string head in heads)
						{
							result.Add(head + this.Symbol.ToString());
						}
					}
				}
			}
			// ��� �������� ���� ��� ���������������� - ��������� ���� � ������ ������
			if(result.Count<1)
			{
				result.Add(this.Symbol.ToString());
			}
			return (string[]) result.ToArray(typeof(string));
		}

		public char Symbol
		{
			get
			{
				return Convert.ToChar(m_Symbol);
			}
			set
			{
				m_Symbol = (int) value;
				m_IsChanged = true;
			}
		}

		#endregion

		#region IPersistent Members

		public void Serialize(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector, System.IO.Stream stream)
		{
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Load:
				{
					m_Symbol = Cerebrum.ObjectHandle.ReadFrom(stream).ToInt32();
					break;
				}
				case Cerebrum.SerializeDirection.Save:
				{
					Cerebrum.ObjectHandle.WriteTo(stream, new Cerebrum.ObjectHandle(m_Symbol));
					break;
				}
			}

			/*
			SerializeArrayList(direction, stream, ref m_Precedents);
			SerializeArrayList(direction, stream, ref m_Dependents);
			*/
			m_Precedents.Serialize(direction, connector, stream);
			m_Dependents.Serialize(direction, connector, stream);

			m_IsChanged = false;
		}

		#endregion

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Init:
				case Cerebrum.SerializeDirection.Load:
				{
					m_Precedents = new Cerebrum.Collections.Specialized.HandleCollection();
					m_Dependents = new Cerebrum.Collections.Specialized.HandleCollection();
					m_IsChanged = true;
					break;
				}
			}
		}

		/*
		private static void SerializeArrayList(Cerebrum.SerializeDirection direction, System.IO.Stream stream, ref System.Collections.ArrayList list)
		{
			switch(direction)
			{
				case Cerebrum.SerializeDirection.Load:
				{
					int count;
					Cerebrum.ObjectHandle hcnt = Cerebrum.ObjectHandle.ReadFrom(stream);
					count = hcnt.ToInt32();
					list = new System.Collections.ArrayList(count);
					for(int i=0;i<count;i++)
					{
						list.Add(Cerebrum.ObjectHandle.ReadFrom(stream));
					}
					break;
				}
				case Cerebrum.SerializeDirection.Save:
				{
					Cerebrum.ObjectHandle.WriteTo(stream, new Cerebrum.ObjectHandle(list.Count));
					foreach(Cerebrum.ObjectHandle h in list)
					{
						Cerebrum.ObjectHandle.WriteTo(stream, h);
					}
					break;
				}
			}
		}
		*/

		#region IDiscovery Members

		public bool IsChanged
		{
			get
			{
				return this.m_IsChanged;
			}
		}

		#endregion
	}
}
