// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Objects01.Specialized
{
	/// <summary>
	/// Summary description for Concepts.
	/// </summary>
	public class Concepts
	{
		private Concepts()
		{
			//
			// TODO: Add constructor logic here
			//
		}
 
		public static Cerebrum.ObjectHandle DependentObjectsListAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "DependentObjectsList");
		}
		public static Cerebrum.ObjectHandle PrecedentObjectsListAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "PrecedentObjectsList");
		}
		public static Cerebrum.ObjectHandle SymbolAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "Symbol");
		}
		public static Cerebrum.ObjectHandle SymbolNeuronType(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Types", "Name", "SymbolNeuron");
		}
		public static Cerebrum.ObjectHandle ObjectCollectionType(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Types", "Name", "ObjectCollection");
		}

	}
}
