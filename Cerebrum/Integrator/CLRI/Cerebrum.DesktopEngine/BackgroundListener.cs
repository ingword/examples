// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

namespace Cerebrum.DesktopEngine
{

	/// <summary>
	/// ����� ������������ � EXE ������.
	/// �������� � ������������ ������� �� Notify Icon
	/// </summary>
	public class BackgroundListener : System.ComponentModel.Component, Cerebrum.DesktopEngine.IAppListener
	{
		private System.Windows.Forms.MenuItem menuExitApplication;
		private System.Windows.Forms.MenuItem menuShowApplication;
		private System.Windows.Forms.MenuItem menuHideApplication;
		private System.ComponentModel.IContainer components;

		internal Cerebrum.DesktopEngine.Application Application;
		private System.Windows.Forms.NotifyIcon QuickNotifyIcon;
		private System.Windows.Forms.ContextMenu QuickNotifyMenu;

		private int m_ApplicationReady = 0;
		private bool m_ApplicationEnabled;

		private System.Type m_ApplicationType;
		private System.Type m_MasterContextType;
		private System.Type m_PrimaryWindowType;

		public BackgroundListener(System.ComponentModel.IContainer container)
		{
			/// <summary>
			/// Required for Windows.Forms Class Composition Designer support
			/// </summary>
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public BackgroundListener(System.Type applicationType, System.Type masterContextType, System.Type primaryWindowType)
		{
			m_ApplicationType = applicationType;
			m_MasterContextType = masterContextType;
			m_PrimaryWindowType = primaryWindowType;

			/// <summary>
			/// Required for Windows.Forms Class Composition Designer support
			/// </summary>
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			InitApplication();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(BackgroundListener));
			System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();
			this.QuickNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.QuickNotifyMenu = new System.Windows.Forms.ContextMenu();
			this.menuShowApplication = new System.Windows.Forms.MenuItem();
			this.menuHideApplication = new System.Windows.Forms.MenuItem();
			this.menuExitApplication = new System.Windows.Forms.MenuItem();
			// 
			// QuickNotifyIcon
			// 
			this.QuickNotifyIcon.ContextMenu = this.QuickNotifyMenu;
			this.QuickNotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("QuickNotifyIcon.Icon")));
			this.QuickNotifyIcon.Text = ((string)(configurationAppSettings.GetValue("QuickNotifyIcon.Text", typeof(string))));
			this.QuickNotifyIcon.Visible = true;
			this.QuickNotifyIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDown);
			this.QuickNotifyIcon.Click += new System.EventHandler(this.notifyIcon1_Click);
			// 
			// QuickNotifyMenu
			// 
			this.QuickNotifyMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							this.menuShowApplication,
																							this.menuHideApplication,
																							this.menuExitApplication});
			// 
			// menuShowApplication
			// 
			this.menuShowApplication.Enabled = false;
			this.menuShowApplication.Index = 0;
			this.menuShowApplication.Text = ((string)(resources.GetObject("menuShowApplication.Text"))); //"��������";
			this.menuShowApplication.Click += new System.EventHandler(this.menuShowApplication_Click);
			// 
			// menuHideApplication
			// 
			this.menuHideApplication.Enabled = false;
			this.menuHideApplication.Index = 1;
			this.menuHideApplication.Text = ((string)(resources.GetObject("menuHideApplication.Text"))); //"������";
			this.menuHideApplication.Click += new System.EventHandler(this.menuHideApplication_Click);
			// 
			// menuExitApplication
			// 
			this.menuExitApplication.Index = 2;
			this.menuExitApplication.Text = ((string)(resources.GetObject("menuExitApplication.Text"))); //"�����";
			this.menuExitApplication.Click += new System.EventHandler(this.menuExitApplication_Click);
		}
		#endregion


		private void InitApplication()
		{
			if(this.Application==null) 
			{
				this.SuspendReady();
				m_ApplicationEnabled = false;
				this.Application = (Cerebrum.DesktopEngine.Application)System.Activator.CreateInstance(m_ApplicationType, new object[] {this});
				Cerebrum.Windows.Forms.Application.Instance = this.Application;
				this.Application.Disposing += new System.EventHandler(this.Application_Disposing);
				this.Application.EnabledChanged += new System.EventHandler(this.Application_EnabledChanged);
				this.Application.NotifyIcon = this.QuickNotifyIcon;
				this.Application.Initialize();
				this.ResetReady();
			};
		}

		internal bool ShowApplication()
		{
			this.InitApplication();
			return this.Application.Show();
		}
		
		private void Application_EnabledChanged(object sender, System.EventArgs e)
		{
			m_ApplicationEnabled = this.Application.Enabled;
			//menuExitApplication.Enabled = m_ApplicationEnabled;
			menuShowApplication.Enabled = m_ApplicationEnabled;
			menuHideApplication.Enabled = m_ApplicationEnabled;
		}

		private void Application_Disposing(object sender, System.EventArgs e)
		{
			this.SuspendReady();
			if(this.Application!=null) 
			{
				this.Application.EnabledChanged -= new System.EventHandler(this.Application_EnabledChanged);
				this.Application.Disposing -= new System.EventHandler(this.Application_Disposing);
				this.Application = null;
			}
			QuickNotifyIcon.Visible = false;
		}

		private void menuShowApplication_Click(object sender, System.EventArgs e)
		{
			if(m_ApplicationReady < 1 && m_ApplicationEnabled)
			{
				this.SuspendReady();
				ShowApplication();
				this.ResumeReady();
			}
		}

		private void menuExitApplication_Click(object sender, System.EventArgs e)
		{
			this.SuspendReady();
			if(this.Application!=null) 
			{
				if(!this.Application.Enabled)
				{
					System.Resources.ResourceManager resources = new System.Resources.ResourceManager("Cerebrum.DesktopEngine.Resources", typeof(BackgroundListener).Assembly);
					if(System.Windows.Forms.MessageBox.Show(((string)(resources.GetObject("ExitingWhenApplicationBusy.Message"))), ((string)(resources.GetObject("ExitingWhenApplicationBusy.Caption"))), System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning, System.Windows.Forms.MessageBoxDefaultButton.Button2)==System.Windows.Forms.DialogResult.No)
					{
						return;
					}
				}
				this.SuspendReady();
				bool cancel = false;
				Cerebrum.IReleasable rel = this.Application.PrimaryWindow as Cerebrum.IReleasable;
				if(rel!=null)
				{
					cancel = rel.Release(cancel);
				}
				if(!cancel)
				{
					cancel = this.Application.Release(cancel);
				}
				this.ResetReady();
				if(!cancel)
				{
					this.Application.Exit();
				}
			}
			else
			{
				Application_Disposing(null, null);
				System.Windows.Forms.Application.Exit();
			};
			this.ResumeReady();
		}

		private void menuHideApplication_Click(object sender, System.EventArgs e)
		{
			if(m_ApplicationReady < 1 && m_ApplicationEnabled)
			{
				this.SuspendReady();
				this.Application.Hide();
				this.ResumeReady();
			}
		}

		System.Windows.Forms.MouseButtons notifyMouseButton;

		private void notifyIcon1_Click(object sender, System.EventArgs e)
		{
			if(m_ApplicationReady < 1 && m_ApplicationEnabled)
			{
				this.SuspendReady();
				if(notifyMouseButton==System.Windows.Forms.MouseButtons.Left)
				{
					if(this.Application==null) 
					{
						ShowApplication();
					}
					else
					{
						this.Application.Visible ^= true;
					};
				};
				this.ResumeReady();
			}
		}

		private void notifyIcon1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			notifyMouseButton = e.Button;
		}

		internal void ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			if(this.Application!=null)
				this.Application.ProcessException(e.Exception, "ThreadException");
			//System.Windows.Forms.MessageBox.Show(e.Exception.ToString(), "Unhandled Exception");
		}
		#region IAppListener Members

		public Cerebrum.Integrator.DomainContext CreateMasterContext(string domainName, bool creating)
		{
			return (Cerebrum.Integrator.DomainContext)System.Activator.CreateInstance(m_MasterContextType, new object[]{this.Application}/*, new Cerebrum.Integrator.RuntimeDatabase(), new Cerebrum.Integrator.SystemActivity()*/);
		}
		public System.Windows.Forms.Form CreatePrimaryWindow()
		{
			return (Cerebrum.DesktopEngine.PrimaryWindowForm)System.Activator.CreateInstance(m_PrimaryWindowType);
		}

		public void SuspendReady()
		{
			m_ApplicationReady++;
		}
		public void ResumeReady()
		{
			m_ApplicationReady--;
		}
		public void ResetReady()
		{
			m_ApplicationReady = 0;
		}
		#endregion
	}
}
