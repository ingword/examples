// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.DesktopEngine
{
	/// <summary>
	/// Summary description for Application.
	/// </summary>
	public class Application : Cerebrum.Windows.Forms.Application 
	{
		public static void Run(bool quickNotifyIconEnable, System.Type applicationType, System.Type masterContextType, System.Type primaryWindowType) 
		{
			if(quickNotifyIconEnable)
			{
				BackgroundListener bl = new BackgroundListener(applicationType, masterContextType, primaryWindowType);
				Cerebrum.DesktopEngine.Application app = bl.Application;
				if(app!=null)
				{
					app.Show();
					app = bl.Application;
					if(app!=null)
					{
						System.Windows.Forms.Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(bl.ThreadException);
						System.Windows.Forms.Application.Run();
					}
				}
			}
			else
			{
				ForegroundListener fl = new ForegroundListener(applicationType, masterContextType, primaryWindowType);
				Cerebrum.DesktopEngine.Application app = fl.Application;
				if(app!=null)
				{
					app.Show();
					app = fl.Application;
					if(app!=null)
					{
						System.Windows.Forms.Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(fl.ThreadException);
						System.Windows.Forms.Application.Run();
					}
				}
			}
		}

		private Cerebrum.DesktopEngine.IAppListener m_AppListener;
		public Cerebrum.DesktopEngine.IAppListener AppListener
		{
			get
			{
				return m_AppListener;
			}
		}
		public Application(Cerebrum.DesktopEngine.IAppListener appListener)
		{
			m_MasterContextFactory = null;
			m_AppListener = appListener;
		}
		/// <summary>
		/// ���������� �������� ������ �� ��������� ������� ��������� � ������
		/// ����� ���������.
		/// </summary>
		protected Cerebrum.Integrator.IContextFactory m_MasterContextFactory;
		/// <summary>
		/// �������� ������������ ������ �� ��������� ������� ��������� � ������
		/// ����� ���������. 
		/// 
		/// ������ ���������� ��������� �������� ������ ������������
		/// ��������� � ComponentId = "Cerebrum.Integrator.LocalContextFactory";
		/// ������� ��� ���� �� ���������� �������������� ����������. ��� ���������
		/// ������� ���������� ����� �������� ������� ���������� ��������� � ComponentId
		/// �������� � ������� Instances.
		/// </summary>
		public Cerebrum.Integrator.IContextFactory MasterContextFactory
		{
			get
			{
				return m_MasterContextFactory;
			}
		}

		protected override System.Windows.Forms.Form CreatePrimaryWindow()
		{
			return m_AppListener.CreatePrimaryWindow();
		}

		public override Cerebrum.Integrator.DomainContext CreateMasterContext(string domainName, bool creating)
		{
			return m_AppListener.CreateMasterContext(domainName, creating);
		}

		protected override Cerebrum.Integrator.IContextFactory CreateContextFactory()
		{
			if(m_MasterContextFactory==null)
			{
				bool versioning = true;

				System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

				try
				{
					versioning = ((bool)(configurationAppSettings.GetValue("Database.Master.Versioning", typeof(bool))));
				}
				catch
				{
				}

				string primaryDirectory = this.ApplicationDirectory;
				string databaseFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.bin");
				string activityFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.log");

				m_MasterContextFactory = new Cerebrum.DesktopEngine.MasterContextFactory(this, primaryDirectory, databaseFileName, activityFileName, 8, versioning);
			}
			return m_MasterContextFactory;
		}


		/// <summary>
		/// ������ ������ �� ���� ����
		/// </summary>
		protected System.Windows.Forms.NotifyIcon m_NotifyIcon;

		/// <summary>
		/// ���������� � ������������� ������ �� ���� ����
		/// </summary>
		public System.Windows.Forms.NotifyIcon NotifyIcon
		{
			get
			{
				return m_NotifyIcon;
			}
			set
			{
				m_NotifyIcon = value;
			}
		}
		internal static System.Windows.Forms.DialogResult MessageBox(Cerebrum.Windows.Forms.Application application, string resource, Cerebrum.Windows.Forms.MessageBoxStyle style, System.Windows.Forms.MessageBoxIcon icon, string caption)
		{
			try
			{
				System.Resources.ResourceManager resources = new System.Resources.ResourceManager("Cerebrum.DesktopEngine.Resources", typeof(BackgroundListener).Assembly);
				string smessage = ((string)(resources.GetObject(resource + ".Message")));
				string scaption = caption==null?((string)(resources.GetObject(resource + ".Caption"))):((string)(resources.GetObject(caption + ".Caption")));

				return application.MessageBox(smessage, scaption, style, icon);
			}
			catch(System.Exception ex)
			{
				return System.Windows.Forms.MessageBox.Show(ex.ToString(), "Unexpected error", System.Windows.Forms.MessageBoxButtons.OK, icon);
			}
		}

		internal static void ProcessException(Cerebrum.Integrator.Application application, string caption, System.Exception ex)
		{
			try
			{
				System.Resources.ResourceManager resources = new System.Resources.ResourceManager("Cerebrum.DesktopEngine.Resources", typeof(BackgroundListener).Assembly);
				string scaption = ((string)(resources.GetObject(caption + ".Caption")));

				application.ProcessException(ex, scaption);
			}
			catch/*(System.Exception e)*/
			{
				System.Windows.Forms.MessageBox.Show(ex.ToString(), "Unexpected error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
		}

		public override bool Release(bool cancel)
		{
			cancel = base.Release (cancel);
			if(!cancel && !MsgSaveMasterDatabase(Cerebrum.Windows.Forms.MessageBoxStyle.ShortYesNoCancel))
			{
				cancel = true;
			}
			return cancel;
		}

		/// <summary>
		/// ����� ����������� �������� �������� � ��������������
		/// ������� ���������. 
		/// </summary>
		/// <param name="buttons">��������� ������� ������ �� MessageBox</param>
		/// <param name="message">���������, ���������� ������������</param>
		/// <returns>���������� true ���� ���������� ������ �������. false ����
		/// ���������� ���� ��������.</returns>
		protected bool MsgSaveMasterDatabase(Cerebrum.Windows.Forms.MessageBoxStyle style)
		{
			if(this.MasterContext!=null /*-
				&& m_ActiveContext.SystemDatabase!=null */
				&& this.MasterContext.HasChanges)
			{
				System.Windows.Forms.DialogResult dr = Cerebrum.DesktopEngine.Application.MessageBox(this, "SaveSystemDatabaseChanges", style, System.Windows.Forms.MessageBoxIcon.Question, "SaveSystemDatabaseChanges");
				if(dr == System.Windows.Forms.DialogResult.Cancel)
				{
					return false;
				}
				else if(dr == System.Windows.Forms.DialogResult.Yes)
				{
				}
				else if(dr == System.Windows.Forms.DialogResult.No)
				{
					//-RejectContext();
					return true;
				}
				else
					return false;
				
				SaveMasterDatabase();
			}
			return true;
		}

		/// <summary>
		/// ��������� ��������� ������������
		/// </summary>
		/// <returns>true</returns>
		protected bool SaveMasterDatabase()
		{
			if(this.MasterContext!=null/*-
				&& m_ActiveContext.SystemDatabase!=null 
				&& m_ActiveContext.SystemDatabase.HasChanges()*/)
			{
				Cerebrum.Integrator.IContextFactory l_ContextFactory;
				l_ContextFactory = this.CreateContextFactory();
				l_ContextFactory.ExportDatabase(this.MasterContext);
				//-this.AddActivityRowResource(SystemActivityEvents.DatabaseAccepted, "Application.SaveActiveDatabase", "SystemActivityChangesAccepted.Message", null);
			}
			return true;
		}

		/// <summary>
		/// ��������� ������ ����������
		/// </summary>
		public void Exit()
		{
			this.m_AppListener.SuspendReady();
			try
			{
				Cerebrum.Runtime.Environment env = this.m_Environment;
				this.LogOut();
				this.Shutdown();
				if(env!=null)
				{
					env.Dispose();
				}
				this.Dispose();
			}
			catch(System.Exception ex)
			{
				ProcessException(this, "CriticalErrorDuringExiting", ex);
			}
			System.Windows.Forms.Application.Exit();
			this.m_AppListener.ResumeReady();
		}

	}
	public interface IAppListener
	{
		System.Windows.Forms.Form CreatePrimaryWindow();
		Cerebrum.Integrator.DomainContext CreateMasterContext(string domainName, bool creating);
		void SuspendReady();
		void ResumeReady();
	}
}