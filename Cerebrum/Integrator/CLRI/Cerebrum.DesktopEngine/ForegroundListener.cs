// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

namespace Cerebrum.DesktopEngine
{
	/// <summary>
	/// Summary description for ForegroundListener.
	/// </summary>
	public class ForegroundListener : System.ComponentModel.Component, Cerebrum.DesktopEngine.IAppListener
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		internal Cerebrum.DesktopEngine.Application Application;

		private int m_ApplicationReady;
		private bool m_ApplicationEnabled;

		private System.Type m_ApplicationType;
		private System.Type m_MasterContextType;
		private System.Type m_PrimaryWindowType;

		public ForegroundListener(System.ComponentModel.IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public ForegroundListener(System.Type applicationType, System.Type masterContextType, System.Type primaryWindowType)
		{
			m_ApplicationType = applicationType;
			m_MasterContextType = masterContextType;
			m_PrimaryWindowType = primaryWindowType;
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			InitApplication();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		private void InitApplication()
		{
			if(this.Application==null) 
			{
				this.SuspendReady();
				m_ApplicationEnabled = false;
				this.Application = (Cerebrum.DesktopEngine.Application)System.Activator.CreateInstance(m_ApplicationType, new object[] {this});
				Cerebrum.Windows.Forms.Application.Instance = this.Application;
				this.Application.Disposing += new System.EventHandler(this.Application_Disposing);
				this.Application.EnabledChanged += new System.EventHandler(this.Application_EnabledChanged);
				this.Application.PrimaryWindowChanged += new EventHandler(Application_PrimaryWindowChanged);
				this.Application.Initialize();
				this.ResetReady();
			};
		}

		internal bool ShowApplication()
		{
			this.InitApplication();
			return this.Application.Show();
		}
		
		private void Application_EnabledChanged(object sender, System.EventArgs e)
		{
			m_ApplicationEnabled = this.Application.Enabled;
		}

		private void Application_Disposing(object sender, System.EventArgs e)
		{
			this.SuspendReady();
			if(this.Application!=null) 
			{
				this.Application.PrimaryWindowChanged -= new EventHandler(Application_PrimaryWindowChanged);
				this.Application.EnabledChanged -= new System.EventHandler(this.Application_EnabledChanged);
				this.Application.Disposing -= new System.EventHandler(this.Application_Disposing);
				this.Application = null;
			}
		}

		internal void ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			if(this.Application!=null)
				this.Application.ProcessException(e.Exception, "ThreadException");
			//System.Windows.Forms.MessageBox.Show(e.Exception.ToString(), "Unhandled Exception");
		}

		#region IAppListener Members

		public Cerebrum.Integrator.DomainContext CreateMasterContext(string domainName, bool creating)
		{
			return (Cerebrum.Integrator.DomainContext)System.Activator.CreateInstance(m_MasterContextType, new object[]{this.Application}/*, new Cerebrum.Integrator.RuntimeDatabase(), new Cerebrum.Integrator.SystemActivity()*/);
		}

		public System.Windows.Forms.Form CreatePrimaryWindow()
		{
			Cerebrum.DesktopEngine.PrimaryWindowForm PrimaryWindow = (Cerebrum.DesktopEngine.PrimaryWindowForm)System.Activator.CreateInstance(m_PrimaryWindowType);
			((Cerebrum.IDisposable)PrimaryWindow).Disposing += new EventHandler(PrimaryWindow_Disposing);
			((Cerebrum.IReleasable)PrimaryWindow).Releasing += new CancelEventHandler(PrimaryWindow_Releasing);
			return PrimaryWindow;
		}

		public void SuspendReady()
		{
			m_ApplicationReady++;
		}
		public void ResumeReady()
		{
			m_ApplicationReady--;
		}
		public void ResetReady()
		{
			m_ApplicationReady = 0;
		}

		#endregion

		private void PrimaryWindow_Disposing(object sender, EventArgs e)
		{
			((Cerebrum.IReleasable)sender).Releasing -= new CancelEventHandler(PrimaryWindow_Releasing);
			((Cerebrum.IDisposable)sender).Disposing -= new EventHandler(PrimaryWindow_Disposing);
		}

		private void PrimaryWindow_Releasing(object sender, CancelEventArgs e)
		{
			if(this.m_ApplicationReady < 1)
			{
				e.Cancel = this.Application.Release(e.Cancel);
			}
		}

		private void Application_PrimaryWindowChanged(object sender, EventArgs e)
		{
			if(this.Application.PrimaryWindow==null && this.m_ApplicationReady < 1)
			{
				this.Application.Exit();
			}
		}
	}
}
