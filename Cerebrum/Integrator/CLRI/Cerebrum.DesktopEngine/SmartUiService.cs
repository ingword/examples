// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.DesktopEngine
{
	/// <summary>
	/// Summary description for SmartUiService.
	/// </summary>
	public class SmartUiService : Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor
	{
		private struct FileTypeEntry
		{
			public FileTypeEntry(ObjectHandle id, string name, string displayName, int ordinal)
			{
				this.Id = id;
				this.Name = name;
				this.DisplayName = displayName;
				this.Ordinal = ordinal;
			}
			public Cerebrum.ObjectHandle Id;
			public string Name;
			public string DisplayName;
			public int Ordinal;
		}
		private class FileTypeEntryComparer : System.Collections.IComparer
		{
			#region IComparer Members

			public int Compare(object x, object y)
			{
				int fx = ((FileTypeEntry)x).Ordinal;
				int fy = ((FileTypeEntry)y).Ordinal;

				return fx==fy?0:(fx>fy?-1:1);
			}

			#endregion
		}
		public SmartUiService()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public override bool CommandActivated(Cerebrum.IConnector message, object sender, object argument)
		{
			string name = ((Cerebrum.Reflection.MessageDescriptor)message.Component).Name;

			switch(name)
			{
				case "Open":
				{
					Cerebrum.Windows.Forms.Application application = this.DomainContext.Application as Cerebrum.Windows.Forms.Application;

					System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();

					System.Collections.ArrayList filters = new System.Collections.ArrayList();
					using(Cerebrum.Data.TableView tbl = Cerebrum.Management.Utilites.GetView(this.DomainContext, "UiFileTypes"))
					{
						if(tbl!=null)
						{
							foreach(Cerebrum.Data.ComponentItemView item in tbl)
							{
								using(Cerebrum.IComposite composite = item.GetComposite())
								{
									Cerebrum.Reflection.UiFileTypeDescriptor fd = composite.Component as Cerebrum.Reflection.UiFileTypeDescriptor;
									filters.Add(new FileTypeEntry(composite.PlasmaHandle, fd.Name.Replace("|", "/"), fd.DisplayName.Replace("|", "/"), fd.Ordinal));
								}
							}
						}
					}
					filters.Add(new FileTypeEntry(Cerebrum.ObjectHandle.Null, "*.vnn", "Cerebrum Database files (*.vnn)", -100));
					filters.Add(new FileTypeEntry(Cerebrum.ObjectHandle.Null, "*.bin", "Cerebrum Database files (*.bin)", -200));
					filters.Add(new FileTypeEntry(Cerebrum.ObjectHandle.Null, "*.*", "All files", -300));

					filters.Sort(new FileTypeEntryComparer());

					bool first = true;
					string filter = "";
					foreach(FileTypeEntry fe in filters)
					{
						if(first)
						{
							first = false;
						}
						else
						{
							filter += "|";
						}
						filter += fe.DisplayName + "|" + fe.Name;
					}

					dlg.Filter = filter;
					dlg.DefaultExt = "vnn";

					if(dlg.ShowDialog(application.PrimaryWindow)==System.Windows.Forms.DialogResult.OK)
					{
						int mode = -1;
						if(dlg.FilterIndex > 0 && dlg.FilterIndex < filters.Count)
						{
							mode = dlg.FilterIndex - 1;
						}
						else
						{
							string ext = System.IO.Path.GetExtension(dlg.FileName).ToLower();
							for(int i=0;i<filters.Count-1;i++)
							{
								FileTypeEntry fe = (FileTypeEntry)filters[i];
								if(ext==fe.Name.ToLower())
								{
									mode = i;
									break;
								}
							}
						}
						Cerebrum.ObjectHandle hExt = Cerebrum.ObjectHandle.Null;
						if(mode>=0)
						{
							hExt = ((FileTypeEntry)filters[mode]).Id;
						}
						if(Cerebrum.ObjectHandle.Null.Equals(hExt))
						{
							Cerebrum.Integrator.IContextFactory lcs = new Cerebrum.DesktopEngine.SmartContextFactory(application, dlg.FileName, null, 4, true, "", false);
							((Cerebrum.IComponent)lcs).SetConnector(Cerebrum.SerializeDirection.None, new Cerebrum.Runtime.SurrogateComposite(this.DomainContext.Workspace));
#warning "TODO: implement QueryDomainName to avoid build all domain infrastructure for acquiring domain name, also can't know 'ReadOnly' before open domain"
							Cerebrum.IWorkspace domain = lcs.LoadContext(dlg.FileName, false);
							Cerebrum.Integrator.DomainContext c = domain.Component as Cerebrum.Integrator.DomainContext;
							application.AddDomain(c);

							Cerebrum.ObjectHandle h = Cerebrum.Management.Utilites.ResolveHandle(this.DomainContext, "DomainContexts", "Name", c.DomainName);
							Cerebrum.ObjectHandle h2 = Cerebrum.ObjectHandle.Null;
							if(!Cerebrum.ObjectHandle.Null.Equals(h))
							{
								using(Cerebrum.IComposite connector = this.DomainContext.AttachConnector(h))
								{
									if(connector!=null)
									{
										Cerebrum.Reflection.DomainContextDescriptor dcd = connector.Component as Cerebrum.Reflection.DomainContextDescriptor;
										h2 = dcd.UiServiceIdHandle;
									}
								}
							}
							if(!Cerebrum.ObjectHandle.Null.Equals(h2))
							{
								using(Cerebrum.IComposite connector = this.DomainContext.AttachConnector(h2))
								{
									if(connector!=null)
									{
										Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor uid = connector.Component as Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor;
										uid.CommandActivated(message, sender, new Cerebrum.Integrator.DomainContextEventArgs(c));
									}
								}
							}
							else
							{
								string domName = c.Workspace.Name;
								c.Shutdown();
								application.MessageBox("This Database has unknown Domain Name and can't be opened\r\n\r\nPlease reinstall components for DomainName = '" + domName + "'", "Opening Database error", Cerebrum.Windows.Forms.MessageBoxStyle.ShortOK, System.Windows.Forms.MessageBoxIcon.Error);
							}
						}
						else
						{
							Cerebrum.ObjectHandle h2 = Cerebrum.ObjectHandle.Null;
							using(Cerebrum.IComposite composite = this.DomainContext.AttachConnector(hExt))
							{
								Cerebrum.Reflection.UiFileTypeDescriptor fd = composite.Component as Cerebrum.Reflection.UiFileTypeDescriptor;
								h2 = fd.UiServiceIdHandle;
							}
							if(!Cerebrum.ObjectHandle.Null.Equals(h2))
							{
								using(Cerebrum.IComposite connector = this.DomainContext.AttachConnector(h2))
								{
									if(connector!=null)
									{
										Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor uid = connector.Component as Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor;
										uid.CommandActivated(message, sender, new Cerebrum.Windows.Forms.FileNameEventArgs(dlg.FileName));
									}
								}
							}
						}
					}
					return true;
				}
			}
			return base.CommandActivated(message, sender, argument);
		}
	}
}
