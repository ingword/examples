// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.DesktopEngine
{
	/// <summary>
	/// Summary description for SmartContextFactory.
	/// </summary>
	public class SmartContextFactory : Cerebrum.Integrator.ContextFactoryBase
	{
		protected string m_TemplateFileName;
		protected bool m_MergeImport;

		/// <summary>
		/// ����������� ������
		/// </summary>
		/// <param name="application"></param>
		/// <param name="e"></param>
		/// <param name="clusterFrameSize"></param>
		/// <param name="importFileName">������ ���� � XML ����� � �� ������������</param>
		/// <param name="mergeImport">true - ���������� XML ��� XVMG, false - ���������� XML ��� XVNN</param>
		/// <!-- param name="activityFileName">������ ���� � XML ����� � �� ������� ����������</param -->
		public SmartContextFactory(Cerebrum.Integrator.Application application, string databaseFileName, string activityFileName, int clusterFrameSize, bool versions, string templateFileName, bool mergeImport):base(application, databaseFileName, activityFileName, clusterFrameSize, versions)
		{
			m_TemplateFileName = templateFileName;
			m_MergeImport = mergeImport;
		}

		public string ImportFileName
		{
			get
			{
				string folderPath;
				if(m_TemplateFileName!=null)
				{
					folderPath = m_TemplateFileName;
				}
				else
				{
					throw new System.NotSupportedException();
					//folderPath = (string)m_DomainContext.GetSystemVariable(this.m_ComponentId, "incoming");
				}
				//				if(folderPath!=null)
				//				{
				//					folderPath = System.IO.Path.Combine(m_DomainContext.SystemDirectory, folderPath);
				//				}
				return folderPath;
			}
		}

		/// <summary>
		/// ��������� �� ������������ � �����
		/// </summary>
		/// <param name="c">������ �� ������ DomainContext � �� ������������</param>
		public override void ExportDatabase(Cerebrum.Integrator.DomainContext c)
		{
			//			System.IO.FileStream fs = null;
			//			
			//			try
			//			{
			//				using(Cerebrum.Runtime.INativeDomain domain = c.DomainConnector.GetContainer() as Cerebrum.Runtime.INativeDomain)
			//				{
			//
			//					//fs = new System.IO.FileStream(DatabaseFileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
			//					if(!System.IO.Directory.Exists(ExportFolder))
			//					{
			//						System.IO.Directory.CreateDirectory(ExportFolder);
			//					}
			//					fs = new System.IO.FileStream(System.IO.Path.Combine(ExportFolder, "export.xvnn"), System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
			//					//c.SystemDatabase.AcceptChanges();
			//					//db.WriteXml(fs);
			//					System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(fs, System.Text.Encoding.Default);
			//
			//					xw.Formatting = System.Xml.Formatting.Indented;
			//					Cerebrum.Management.Tools.ExportDatabase(domain, xw);
			//				
			//					xw.Close();
			//				}
			//			}
			//			catch(System.Exception ex)
			//			{
			//				this.m_Application.ProcessException(ex, "LocalContextFactory.SaveDatabase");
			//				m_Application.MessageBox(ex, --"-System-DatabaseWritingError");
			//			}
			//			finally
			//			{
			//				if(fs!=null) fs.Close();
			//			}

		}

		/// <summary>
		/// ��������� �� ������� ���������� � �����
		/// </summary>
		/// <param name="c">������ �� ������ DomainContext � �� ����������</param>
		public override void SaveActivity(Cerebrum.Integrator.DomainContext c)
		{
		}

		/// <summary>
		/// ��������� � ������������ �������� ������������ �� ����� �������� ������.
		/// 
		/// � �������� ������� ������������, ��� ������������� ������� �������� � 
		/// ��������� ���������� incoming ��� �������� ������ ���������.
		/// 
		/// ��������������� ���������� ������� �������� ��������� ������ 
		/// ������������ � ���� ������� � ���������� ���� ������ � �� ����� 
		/// ��������� �������� �������, � ������� ��� ������ ������ ��������������� 
		/// � ������� ������.
		/// </summary>
		/// <param name="c">��������, � ������� ���������� ������������</param>
		public override void ImportDatabase(Cerebrum.Integrator.DomainContext c, bool isNew)
		{
			if(!isNew) return;
			try
			{
				string bcfg = this.ImportFileName;
				if(bcfg!=null && bcfg.Length>0 && System.IO.File.Exists(bcfg))
				{
					using(System.IO.FileStream fs = new System.IO.FileStream(bcfg, System.IO.FileMode.Open, System.IO.FileAccess.Read))
					{
						System.IO.StreamReader sr = new System.IO.StreamReader(fs, System.Text.Encoding.Default);
						System.Xml.XmlTextReader xr = new System.Xml.XmlTextReader(sr);
						Cerebrum.IWorkspace domain = c.Workspace;

						if(m_MergeImport)
						{
							System.Collections.Hashtable map = new System.Collections.Hashtable();
							Cerebrum.Management.Utilites.ImportDatabase(domain, xr, map);
						}
						else
						{
							Cerebrum.Management.Utilites.ImportDatabase(domain, xr);
						}

						xr.Close();
						sr.Close();
						//fs.Close();
					}
				}
			}
			catch(System.Exception ex)
			{
				this.m_Application.ProcessException(ex, "SimpleContextFactory.ImportIncoming");
				//				this.m_DomainContext.Application.AddActivityRow(SystemActivityEvents.ExceptionError, m_ComponentId + ".LoadDatabase", ex.ToString(), m_DomainContext.SystemActivity);
			}
		}

		/// <summary>
		/// ��������� �� ��������� �� �����
		/// </summary>
		/// <param name="c">������ �� ������ DataSet � �� ������������</param>
		public override Cerebrum.IWorkspace LoadContext(string databaseFileName, bool readOnly)
		{
			Cerebrum.IWorkspace domain = base.LoadContext(databaseFileName, readOnly);

			domain.Activator = new Cerebrum.Integrator.Activator();
				
			return domain;
		}

		public override Cerebrum.IWorkspace InitContext(string databaseFileName, string domainName)
		{
			Cerebrum.IWorkspace domain = base.InitContext(databaseFileName, domainName);

			domain.Activator = new Cerebrum.Management.Activator();
			Cerebrum.Management.Utilites.InitDataBase(domain);
			domain.Activator = new Cerebrum.Integrator.Activator();

			return domain;
		}
		public override Cerebrum.IWorkspace InitContextDebug(string domainName)
		{
			Cerebrum.IWorkspace domain = base.InitContextDebug(domainName);

			domain.Activator = new Cerebrum.Management.Activator();
			Cerebrum.Management.Utilites.InitDataBase(domain);
			domain.Activator = new Cerebrum.Integrator.Activator();

			return domain;
		}

		public override object CreateContext(string domainName, bool creating)
		{
			System.Type domainContextType = typeof(Cerebrum.Integrator.DomainContext);
			Cerebrum.ObjectHandle h = Cerebrum.Management.Utilites.ResolveHandle(this.DomainContext, "DomainContexts", "Name", domainName);
			if(!Cerebrum.ObjectHandle.Null.Equals(h))
			{
				using(Cerebrum.IComposite connector = this.DomainContext.AttachConnector(h))
				{
					if(connector!=null)
					{
						Cerebrum.Reflection.DomainContextDescriptor dcd = connector.Component as Cerebrum.Reflection.DomainContextDescriptor;
						domainContextType = dcd.DomainType;
					}
				}
			}
			return System.Activator.CreateInstance(domainContextType, new object[] {this.m_Application});
		}
	}
}
