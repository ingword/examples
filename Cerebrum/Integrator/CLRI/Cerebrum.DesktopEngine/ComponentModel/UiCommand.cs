// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.ComponentModel
{
	/// <summary>
	/// Summary description for UiCommand.
	/// </summary>
	internal class UiCommand : Cerebrum.Windows.Forms.IUiCommand, Cerebrum.Windows.Forms.ComponentModel.IUiContainerInternal
	{
		private Cerebrum.Integrator.DomainContext m_Context;
		private Cerebrum.ObjectHandle m_Handle;
		private string m_CommandName;
		private string m_MessageName;
		private bool m_Visible;
		private bool m_Checked;

		public UiCommand(Cerebrum.Integrator.DomainContext context, Cerebrum.IComposite connector)
		{
			Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor cmd = connector.Component as Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor;
			m_Context = context;
			m_Handle = connector.PlasmaHandle;
			m_Enabled = false;
			m_Checked = false;
			m_Visible = true;
			m_CommandName = cmd.Name;
			using(Cerebrum.IConnector cmsg = this.m_Context.AttachConnector(cmd.MessageId))
			{
				m_MessageName = (cmsg.Component as Cerebrum.Reflection.MessageDescriptor).Name;
			}
		}

		public string CommandName
		{
			get
			{
				return m_CommandName;
			}
		}

		public string MessageName
		{
			get
			{
				return m_MessageName;
			}
		}
		/// <summary>
		/// ���������� ���������� ��������� ������ �������.
		/// ���� true ������ �������� �������� � ���������� ������,
		/// ���� false - ������ ������ ������� �������������
		/// </summary>
		private bool m_Enabled;
		/// <summary>
		/// ���������� � ������������� ��������� ����������
		/// ���� true ������ �������� �������� � ���������� ������,
		/// ���� false - ������ ������ ������� �������������
		/// </summary>
		public bool Enabled
		{
			get
			{
				return m_Enabled;
			}
			set
			{
				if(m_Enabled != value)
				{
					m_Enabled = value;
					if(m_Controls!=null)
						foreach(IUiControl ctl in m_Controls)
						{
							ctl.Enabled = m_Enabled;
						}

					this.OnEnabledChanged(System.EventArgs.Empty);
				}
			}
		}
		public virtual void OnEnabledChanged(System.EventArgs e)
		{
			if(this.EnabledChanged!=null)
			{
				this.EnabledChanged(this, e);
			}
		}

		public virtual void OnVisibleChanged(System.EventArgs e)
		{
			if (this.VisibleChanged != null)
			{
				this.VisibleChanged(this, e);
			}
		}

		public virtual void OnCheckedChanged(System.EventArgs e)
		{
			if (this.CheckedChanged != null)
			{
				this.CheckedChanged(this, e);
			}
		}

		public event System.EventHandler EnabledChanged;

		public event System.EventHandler VisibleChanged;

		public event System.EventHandler CheckedChanged;

		public event ConnectorEventHandler Activate;

		private void Item_InvokeCommand(object sender, System.ComponentModel.CancelEventArgs ce)
		{
			ce.Cancel = ActivateCommand(ce.Cancel);
		}

		public bool ActivateCommand(bool cancel)
		{
			if(Activate!=null)
			{
				using(Cerebrum.IConnector connectorCmd = m_Context.AttachConnector(m_Handle))
				{
					Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor cmd = (Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor)connectorCmd.Component;
					using(Cerebrum.IConnector connector = this.m_Context.AttachConnector(cmd.MessageId))
					{
						ConnectorEventArgs ca = new ConnectorEventArgs(cancel, connector);
						Activate(this, ca);
						return ca.Cancel;
					}
				}
			}
			return cancel;
		}

		private UiControlsCollection m_Controls = null;

		public IUiControlsCollection Controls
		{
			get
			{
				if(m_Controls==null)
				{
					m_Controls = new UiControlsCollection(this);
				}
				return m_Controls;
			}
		}

		void Cerebrum.Windows.Forms.ComponentModel.IUiContainerInternal.AttachControl(IUiControl ctl)
		{
			ctl.CommandId = this.m_Handle;
			ctl.Enabled = this.m_Enabled;
			ctl.Checked = this.m_Checked;
			ctl.Visible = this.m_Visible;
			ctl.InvokeCommand += new System.ComponentModel.CancelEventHandler(this.Item_InvokeCommand);

			using(Cerebrum.IConnector connectorCmd = m_Context.AttachConnector(m_Handle))
			{
				Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor cmd = (Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor)connectorCmd.Component;
				ctl.AttachCommand(cmd);
			}
		}

		void Cerebrum.Windows.Forms.ComponentModel.IUiContainerInternal.DetachControl(IUiControl ctl)
		{
			ctl.InvokeCommand -= new System.ComponentModel.CancelEventHandler(this.Item_InvokeCommand);
		}

		#region IUiControl Members

		public void AttachCommand(Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor command)
		{
			// TODO:  Add UiCommand.AttachCommand implementation
		}

		public Cerebrum.ObjectHandle CommandId
		{
			get
			{
				return m_Handle;
			}
			set
			{
				m_Handle = value;
			}
		}

		public bool Checked
		{
			get
			{
				return m_Checked;
			}
			set
			{
				if (m_Checked != value)
				{
					m_Checked = value;
					if (m_Controls != null)
					{
						foreach (IUiControl ctl in m_Controls)
						{
							ctl.Checked = m_Checked;
						}
					}
					this.OnCheckedChanged(System.EventArgs.Empty);
				}
			}
		}

		/// <summary>
		/// ���������� ��������� ��������� ���������� ��������� � ������ �������.
		/// ���� true ������ �������� �������� � ���������� ������ (����� �������� ����������),
		/// ���� false - �������� ���������� �� �����.
		/// </summary>
		public bool Visible
		{
			get
			{
				return m_Visible;
			}
			set
			{
				if (m_Visible != value)
				{
					m_Visible = value;
					if (m_Controls != null)
					{
						foreach (IUiControl ctl in m_Controls)
						{
							ctl.Visible = m_Visible;
						}
					}
					this.OnVisibleChanged(System.EventArgs.Empty);
				}
			}
		}

		public event System.ComponentModel.CancelEventHandler InvokeCommand;

		#endregion

		#region IReleasable Members

		public bool Release(bool cancel)
		{
			// TODO:  Add UiCommand.Relax implementation
			return cancel;
		}

		/*public void Dispose(object sender, EventArgs e)
		{
			// TODO:  Add UiCommand.Dispose implementation
		}*/

		public event System.ComponentModel.CancelEventHandler Releasing;

		#endregion

		#region IDisposable Members

		public event System.EventHandler Disposing;

		public void Dispose()
		{
		}
		#endregion
	
		protected int m_MergeOrder;
		public int MergeOrder
		{
			get
			{
				return this.m_MergeOrder;
			}
			set
			{
				this.m_MergeOrder = value;
			}
		}
	}

}

