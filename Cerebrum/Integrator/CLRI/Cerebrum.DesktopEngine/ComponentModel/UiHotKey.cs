// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.ComponentModel
{
	/// <summary>
	/// Summary description for UiHotKey.
	/// </summary>
	public class UiHotKey : Cerebrum.DisposableObject, Cerebrum.Windows.Forms.IUiControl
	{
		Cerebrum.Integrator.DomainContext m_Context;
		Cerebrum.ObjectHandle m_Handle;
		string m_KeyCode;

		public UiHotKey(Cerebrum.Integrator.DomainContext context, Cerebrum.ObjectHandle handle, string keyCode)
		{
			m_Context = context;
			m_Handle = handle;
			m_Enabled = true;
			m_KeyCode = keyCode;
		}

		public string KeyCode
		{
			get
			{
				return m_KeyCode;
			}
		}

		protected Cerebrum.ObjectHandle m_CommandId;
		protected bool m_Enabled;

		public void FireCommand(System.ComponentModel.CancelEventArgs ce)
		{
			if(InvokeCommand!=null)
			{
				InvokeCommand(this, ce);
			}
		}

		#region IReleasable Members

		public bool Release(bool cancel)
		{
			System.ComponentModel.CancelEventArgs e = new System.ComponentModel.CancelEventArgs(cancel);
			if(Releasing!=null) Releasing(this, e);
			return e.Cancel;
		}

		public void Dispose(object sender, EventArgs e)
		{
			this.Dispose();
		}

		public event System.ComponentModel.CancelEventHandler Releasing;

		#endregion


		#region IUiControl Members

		public void AttachCommand(Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor command)
		{
		}

		public Cerebrum.ObjectHandle CommandId
		{
			get
			{
				return m_CommandId;
			}
			set
			{
				m_CommandId = value;
			}
		}

		public bool Enabled
		{
			get
			{
				// TODO:  Add IntegratorMenuItem.Enabled getter implementation
				return m_Enabled;
			}
			set
			{
				m_Enabled = value;
			}
		}

		public bool Checked
		{
			get
			{
				// TODO:  Add UiHotKey.Checked getter implementation
				return false;
			}
			set
			{
				// TODO:  Add UiHotKey.Checked setter implementation
			}
		}

		public bool Visible
		{
			get
			{
				// TODO:  Add UiHotKey.Visible getter implementation
				return false;
			}
			set
			{
				// TODO:  Add UiHotKey.Visible setter implementation
			}
		}

		public event System.ComponentModel.CancelEventHandler InvokeCommand;

		#endregion
	
		protected int m_MergeOrder;
		public int MergeOrder
		{
			get
			{
				return this.m_MergeOrder;
			}
			set
			{
				this.m_MergeOrder = value;
			}
		}
	}
}
