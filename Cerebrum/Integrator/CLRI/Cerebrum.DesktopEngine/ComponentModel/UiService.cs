// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.ComponentModel
{
	/// <summary>
	/// Summary description for UiService.
	/// </summary>
	internal class UiService
	{
		Cerebrum.Integrator.DomainContext m_Context;
		Cerebrum.ObjectHandle m_Handle;

		public UiService(Cerebrum.Integrator.DomainContext context, Cerebrum.ObjectHandle handle)
		{
			m_Context = context;
			m_Handle = handle;
		}

		public void CommandActivated(object sender, Cerebrum.Windows.Forms.ConnectorEventArgs e)
		{
			e.Cancel |= this.ActivateCommand(e.Connector, sender, null);
		}

		private Cerebrum.IConnector AttachUIServiceConnector()
		{
			Cerebrum.IConnector connector = m_Context.AttachConnector(m_Handle);
			if(connector==null)
			{
				throw new System.Exception("There is no any object with handle='" + m_Handle.ToString() + "' in the root of the current domain");
			}
			if(connector.Component is Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor)
			{
				return connector;
			}
			else
			{
				string errmsg;

				if(connector.Component==null)
				{
					errmsg = "Node with handle='" + m_Handle.ToString() + "' doesn't contain any user object (connector.Component==null, but UiServiceDescriptor expected)";
				}
				else
				{
					errmsg = "Node with handle='" + m_Handle.ToString() + "' contains instance of type='" + connector.Component.GetType().ToString() + "' instead of UiServiceDescriptor";
				}

				connector.Dispose();

				throw new System.Exception(errmsg);
			}
		}

		public bool ActivateCommand(Cerebrum.IConnector connectorMsg, object sender, object argument)
		{
			using(Cerebrum.IConnector connector = this.AttachUIServiceConnector())
			{
				return ((Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor)connector.Component).CommandActivated(connectorMsg, sender, argument);
			}
		}

		public void ComponentActivated(object component)
		{
			using(Cerebrum.IConnector connector = this.AttachUIServiceConnector())
			{
				((Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor)connector.Component).ComponentActivated(component);
			}
		}
	}
}
