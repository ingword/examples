// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.DesktopEngine.Specialized
{
	/// <summary>
	/// Summary description for Concepts.
	/// </summary>
	public sealed class Concepts
	{
		private Concepts()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static Cerebrum.ObjectHandle DomainTypeIdAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "DomainTypeId");
		}

		public static Cerebrum.ObjectHandle UiServiceIdAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "UiServiceId");
		}

	}
	public sealed class KnownNames
	{
		private KnownNames()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public const string FactoryHandle = "FactoryHandle";
	}
}
