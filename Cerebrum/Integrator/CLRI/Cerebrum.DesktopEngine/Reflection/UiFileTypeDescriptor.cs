// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Reflection
{
	/// <summary>
	/// Summary description for UiFileTypeDescriptor.
	/// </summary>
	public class UiFileTypeDescriptor : Cerebrum.Reflection.MemberDescriptor
	{
		public UiFileTypeDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public Cerebrum.ObjectHandle UiServiceIdHandle
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.DesktopEngine.Specialized.Concepts.UiServiceIdAttribute(this.DomainContext));
				if(o==null)
				{
					return Cerebrum.ObjectHandle.Null;
				}
				else
				{
					return (Cerebrum.ObjectHandle)o;
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.DesktopEngine.Specialized.Concepts.UiServiceIdAttribute(this.DomainContext), value);
			}
		}

		public int Ordinal
		{
			get
			{
				object v = GetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.OrdinalAttribute(this.DomainContext));
				if(v==null || v is System.DBNull)
				{
					return -1;
				}
				return Convert.ToInt32(v);
			}
			set
			{
				SetAttributeComponent(Cerebrum.Windows.Forms.Specialized.Concepts.OrdinalAttribute(this.DomainContext), value);
			}
		}
	}
}
