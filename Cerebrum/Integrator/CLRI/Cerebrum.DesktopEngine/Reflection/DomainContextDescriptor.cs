// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Reflection
{
	/// <summary>
	/// Summary description for DomainContextDescriptor.
	/// </summary>
	public class DomainContextDescriptor : Cerebrum.Reflection.MemberDescriptor
	{
		public DomainContextDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public Cerebrum.ObjectHandle DomainTypeIdHandle
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.DesktopEngine.Specialized.Concepts.DomainTypeIdAttribute(this.DomainContext));
				if(o is Cerebrum.ObjectHandle)
				{
					return (Cerebrum.ObjectHandle)o;
				}
				else
				{
					return Cerebrum.ObjectHandle.Null;
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.DesktopEngine.Specialized.Concepts.DomainTypeIdAttribute(this.DomainContext), value);
			}
		}

		public Cerebrum.ObjectHandle UiServiceIdHandle
		{
			get
			{
				object o = GetAttributeComponent(Cerebrum.DesktopEngine.Specialized.Concepts.UiServiceIdAttribute(this.DomainContext));
				if(o is Cerebrum.ObjectHandle)
				{
					return (Cerebrum.ObjectHandle)o;
				}
				else
				{
					return Cerebrum.ObjectHandle.Null;
				}
			}
			set
			{
				SetAttributeComponent(Cerebrum.DesktopEngine.Specialized.Concepts.UiServiceIdAttribute(this.DomainContext), value);
			}
		}

		protected System.Type m_DomainType;
		public System.Type DomainType
		{
			get
			{
				if(m_DomainType==null)
				{
					using(Cerebrum.IConnector c = GetDomainTypeDescriptor())
					{
						if(c==null)
						{
							m_DomainType = typeof(Cerebrum.Integrator.DomainContext);
						}
						else
						{
							System.Object o = c.Component;
							m_DomainType = (o as Cerebrum.Reflection.TypeDescriptor).ComponentType;
						}
					}
				}
				return m_DomainType;
			}
		}

		public Cerebrum.IConnector GetDomainTypeDescriptor()
		{
			Cerebrum.ObjectHandle h = this.DomainTypeIdHandle;

			return this.m_Connector.Workspace.AttachConnector(h);
		}
	}
}


