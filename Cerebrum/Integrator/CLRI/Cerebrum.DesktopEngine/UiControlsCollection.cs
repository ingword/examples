// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.Windows.Forms.ComponentModel
{

	internal interface IUiContainerInternal : IUiContainer
	{
		void AttachControl(IUiControl ctl);
		void DetachControl(IUiControl ctl);
	}
	/// <summary>
	/// Summary description for UiControlsCollection.
	/// </summary>
	public class UiControlsCollection : Cerebrum.Collections.ConcreteListBase, IUiControlsCollection
	{
		private IUiContainerInternal m_Container;
 
		internal UiControlsCollection(IUiContainerInternal command)
		{
			m_Container = command;
		}

		public int Add(IUiControl control)
		{
			return ((System.Collections.IList)this).Add(control);
		}

		public void Remove(IUiControl control)
		{
			this.OnRemove(this.OnSearch(control), control);
		}

		public IUiControl this[int index]
		{
			get
			{
				return (IUiControl)((System.Collections.IList)this)[index];
			}
			set
			{
				((System.Collections.IList)this)[index] = value;
			}
		}

		protected override bool Validate(object value, bool throwException)
		{
			bool valid = value is Cerebrum.Windows.Forms.IUiControl;
			if(throwException && !valid)
			{
				throw new System.InvalidCastException("UiControlsCollection can store only controls derived from IUiControl interface");
			}
			return valid;
		}

		protected override void OnInsert(int index, object value)
		{
			m_Container.AttachControl((IUiControl)value);
			base.OnInsert(index, value);
		}

		protected override void OnUpdate(int index, object value)
		{
			m_Container.AttachControl((IUiControl)value);
			base.OnUpdate(index, value);
		}

		protected override void OnRemove(int index, object value)
		{
			m_Container.DetachControl((IUiControl)value);
			base.OnRemove(index, value);
		}
	}
}
