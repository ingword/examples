// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;

namespace Cerebrum.DesktopEngine
{
	/// <summary>
	/// Summary description for DesktopContextFactory.
	/// </summary>
	public class MasterContextFactory : Cerebrum.Integrator.ContextFactoryBase
	{

		protected string m_ImportFolder;
		protected string m_ExportFolder;

		/// <summary>
		/// ����������� ������
		/// </summary>
		/// <param name="databaseFileName">������ ���� � XML ����� � �� ������������</param>
		/// <param name="activityFileName">������ ���� � XML ����� � �� ������� ����������</param>
		public MasterContextFactory(Cerebrum.Windows.Forms.Application application, string primaryDirectory, string databaseFileName, string activityFileName, int clusterFrameSize, bool versions):base(application, databaseFileName, activityFileName, clusterFrameSize, versions)
		{
			string directory = primaryDirectory;
			m_ImportFolder = System.IO.Path.Combine(directory, "Import");
			m_ExportFolder = System.IO.Path.Combine(directory, "Export");
		}

		public string ImportFolder
		{
			get
			{
				string folderPath;
				if(m_ImportFolder!=null)
				{
					folderPath = m_ImportFolder;
				}
				else
				{
					throw new System.NotSupportedException();
					//folderPath = (string)m_DomainContext.GetSystemVariable(this.m_ComponentId, "incoming");
				}
				//				if(folderPath!=null)
				//				{
				//					folderPath = System.IO.Path.Combine(m_DomainContext.SystemDirectory, folderPath);
				//				}
				return folderPath;
			}
		}

		public string ExportFolder
		{
			get
			{
				string folderPath;
				if(m_ExportFolder!=null)
				{
					folderPath = m_ExportFolder;
				}
				else
				{
					throw new System.NotSupportedException();
					//folderPath = (string)m_DomainContext.GetSystemVariable(this.m_ComponentId, "incoming");
				}
				//				if(folderPath!=null)
				//				{
				//					folderPath = System.IO.Path.Combine(m_DomainContext.SystemDirectory, folderPath);
				//				}
				return folderPath;
			}
		}

		/// <summary>
		/// ��������� �� ������������ � �����
		/// </summary>
		/// <param name="c">������ �� ������ DomainContext � �� ������������</param>
		public override void ExportDatabase(Cerebrum.Integrator.DomainContext c)
		{
			System.IO.FileStream fs = null;
			
			try
			{
				Cerebrum.IWorkspace domain = c.Workspace;

				//fs = new System.IO.FileStream(DatabaseFileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
				if(!System.IO.Directory.Exists(ExportFolder))
				{
					System.IO.Directory.CreateDirectory(ExportFolder);
				}
				fs = new System.IO.FileStream(System.IO.Path.Combine(ExportFolder, "export.xvnn"), System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
				//c.SystemDatabase.AcceptChanges();
				//db.WriteXml(fs);
				System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(fs, System.Text.Encoding.Default);

				xw.Formatting = System.Xml.Formatting.Indented;
				Cerebrum.Management.Utilites.ExportDatabase(domain, xw, true);
				
				xw.Close();
			}
			catch(System.Exception ex)
			{
				//this.m_Application.ProcessException(ex, "LocalContextFactory.SaveDatabase");
				Cerebrum.DesktopEngine.Application.ProcessException(this.m_Application, "SystemDatabaseWritingError", ex);
			}
			finally
			{
				if(fs!=null) fs.Close();
			}

		}

		/// <summary>
		/// ��������� �� ������� ���������� � �����
		/// </summary>
		/// <param name="c">������ �� ������ DomainContext � �� ����������</param>
		public override void SaveActivity(Cerebrum.Integrator.DomainContext c)
		{
			//			System.IO.FileStream fs = null;
			//			
			//			try
			//			{
			//				fs = new System.IO.FileStream(ActivityFileName, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
			//				c.SystemActivity.AcceptChanges();
			//				c.SystemActivity.WriteXml(fs);
			//			}
			//			catch(System.Exception ex)
			//			{
			//				this.m_DomainContext.Application.AddActivityRow(ex, m_ComponentId + ".SaveActivity");
			//				m_DomainContext.Application.MessageBox(ex, "SystemActivityWritingError");
			//			}
			//			finally
			//			{
			//				if(fs!=null) fs.Close();
			//			}
		}

		/// <summary>
		/// ��������� � ������������ �������� ������������ �� ����� �������� ������.
		/// 
		/// � �������� ������� ������������, ��� ������������� ������� �������� � 
		/// ��������� ���������� incoming ��� �������� ������ ���������.
		/// 
		/// ��������������� ���������� ������� �������� ��������� ������ 
		/// ������������ � ���� ������� � ���������� ���� ������ � �� ����� 
		/// ��������� �������� �������, � ������� ��� ������ ������ ��������������� 
		/// � ������� ������.
		/// </summary>
		/// <param name="c">��������, � ������� ���������� ������������</param>
		public override void ImportDatabase(Cerebrum.Integrator.DomainContext c, bool isNew)
		{
			try
			{
				Cerebrum.IWorkspace domain = c.Workspace;

				string folder = ImportFolder;
				if(folder!=null && System.IO.Directory.Exists(folder))
				{
					// �� ���������� ������������, ���� ������������ �������. ��������������� ���������� � ���������� �������� �� �������.

					string [] xvnns, xvmgs;
					try
					{
						xvnns = System.IO.Directory.GetFiles(folder, "*.xvnn");
						xvmgs = System.IO.Directory.GetFiles(folder, "*.xvmg");
					}
					catch(System.Exception ex)
					{
						this.m_Application.ProcessException(ex, "LocalContextFactory.ImportIncoming"/*m_ComponentId + ".LoadDatabase", ex.ToString(), m_DomainContext.SystemActivity*/);
						return;
					}

					if((xvnns!=null && xvnns.Length > 0) || (xvmgs!=null && xvmgs.Length > 0))
					{
						if(isNew || Cerebrum.DesktopEngine.Application.MessageBox((Cerebrum.Windows.Forms.Application)this.m_Application, "LoadNewComponents", Cerebrum.Windows.Forms.MessageBoxStyle.ShortYesNo, System.Windows.Forms.MessageBoxIcon.Question, null)==System.Windows.Forms.DialogResult.Yes)
						{
							int xvnnsCount = xvnns==null?0:xvnns.Length;
							int xvmgsCount = xvmgs==null?0:xvmgs.Length;

							bool [] flgs;
							flgs = new bool[xvnnsCount + xvmgsCount];

							//							c.SystemDatabase.AcceptChanges();

							//Cerebrum.Integrator.Xml.SystemDatabase db/* = new Cerebrum.Integrator.Xml.SystemDatabase()*/;
							//db.EnforceConstraints = false;

							for(int i = 0; i<xvnnsCount + xvmgsCount; i++)
							{
								bool xvmg;
								string bcfg = null;
								if(i<xvnnsCount)
								{
									bcfg = xvnns[i];
									xvmg = false;
								}
								else
								{
									bcfg = xvmgs[i-xvnnsCount];
									xvmg = true;
								}
								flgs[i] = false;

								System.IO.FileStream fs = null;
								try
								{
									//db.Clear();
									//string FileName = System.IO.Path.Combine(folder, bcfg);
									fs = new System.IO.FileStream(bcfg, System.IO.FileMode.Open, System.IO.FileAccess.Read);
									//System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(typeof(Cerebrum.Integrator.Xml.SystemDatabase));
									//db = (Cerebrum.Integrator.Xml.SystemDatabase)formatter.Deserialize(fs);
									//db.ReadXml(fs);
									System.IO.StreamReader sr = new System.IO.StreamReader(fs, System.Text.Encoding.Default);
									System.Xml.XmlTextReader xr = new System.Xml.XmlTextReader(sr);
									System.Collections.Hashtable handleMap;
									if(xvmg)
									{
										handleMap = new System.Collections.Hashtable();
									}
									else
									{
										handleMap = null;
									}
									Cerebrum.Management.Utilites.ImportDatabase(domain, xr, handleMap);
									//									c.SystemDatabase.Merge(db);
									//									c.SystemDatabase.AcceptChanges();
									flgs[i] = true;
									xr.Close();
									sr.Close();
								}
								catch(System.Exception ex)
								{
									//c.SystemDatabase.RejectChanges();
									//this.m_Application.AddActivityRow(SystemActivityEvents.ExceptionError, m_ComponentId + ".LoadDatabase", ex.ToString(), m_DomainContext.SystemActivity);
									Cerebrum.DesktopEngine.Application.ProcessException(this.m_Application, "DataBaseChangesMergingError", ex);
								}
								finally
								{
									if(fs!=null) fs.Close();
								}
							}

							if(Cerebrum.DesktopEngine.Application.MessageBox((Cerebrum.Windows.Forms.Application)this.m_Application, "SaveSystemDatabaseChanges", Cerebrum.Windows.Forms.MessageBoxStyle.ShortYesNo, System.Windows.Forms.MessageBoxIcon.Question, null)==System.Windows.Forms.DialogResult.Yes)
							{
								ExportDatabase(c);
								/// <summary>
								/// ������� ���������� �� ����� �������� ������
								/// </summary>
								for(int i = 0; i<xvnnsCount + xvmgsCount; i++)
								{
									string bcfg = null;
									if(i<xvnnsCount)
									{
										bcfg = xvnns[i];
									}
									else
									{
										bcfg = xvmgs[i-xvnnsCount];
									}

									if(flgs[i])
									{
										try
										{
											//string FileName = System.IO.Path.Combine(folder, bcfg);
											System.IO.File.Delete(bcfg);
										}
										catch(System.Exception ex)
										{
											Cerebrum.DesktopEngine.Application.ProcessException(this.m_Application, "ConfigurationDifferenceFileDeletionError", ex);
										}
									}
								}
							}
						}
					}
				}
			}
			catch(System.Exception ex)
			{
				this.m_Application.ProcessException(ex, "LocalContextFactory.ImportIncoming");
				//				this.m_DomainContext.Application.AddActivityRow(SystemActivityEvents.ExceptionError, m_ComponentId + ".LoadDatabase", ex.ToString(), m_DomainContext.SystemActivity);
				Cerebrum.DesktopEngine.Application.ProcessException(this.m_Application, "DataBaseChangesMergingError", ex);
			}
		}

		/// <summary>
		/// ��������� �� ��������� �� �����
		/// </summary>
		/// <param name="c">������ �� ������ DataSet � �� ������������</param>
		public override Cerebrum.IWorkspace LoadContext(string databaseFileName, bool readOnly)
		{
			Cerebrum.IWorkspace domain = base.LoadContext(databaseFileName, readOnly);

			domain.Activator = new Cerebrum.Integrator.Activator();
				
			return domain;
		}

		public override Cerebrum.IWorkspace InitContext(string databaseFileName, string domainName)
		{
			Cerebrum.IWorkspace domain = base.InitContext(databaseFileName, domainName);

			domain.Activator = new Cerebrum.Management.Activator();
			Cerebrum.Management.Utilites.InitDataBase(domain);
			domain.Activator = new Cerebrum.Integrator.Activator();

			return domain;
		}
		public override Cerebrum.IWorkspace InitContextDebug(string domainName)
		{
			Cerebrum.IWorkspace domain = base.InitContextDebug(domainName);

			domain.Activator = new Cerebrum.Management.Activator();
			Cerebrum.Management.Utilites.InitDataBase(domain);
			domain.Activator = new Cerebrum.Integrator.Activator();

			return domain;
		}

		public override object CreateContext(string domainName, bool creating)
		{
			return this.m_Application.CreateMasterContext(domainName, creating);
		}

	}

}
