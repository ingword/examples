// Copyright (C) Dmitry Shuklin 2003-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Cerebrum.Windows.Forms;

namespace Cerebrum.DesktopEngine
{
	/// <summary>
	/// ������� MDI ���� ����������. 
	/// </summary>
	public class PrimaryWindowForm : Cerebrum.Windows.Forms.DesktopComponent, Cerebrum.Windows.Forms.IPrimaryWindow
	{
		private System.Windows.Forms.StatusBar statusBar1;
		private Cerebrum.Windows.Forms.TabHeader toolBar1;
		protected Cerebrum.Windows.Forms.DesktopMainMenu m_MainMenu;
		protected Cerebrum.Windows.Forms.DesktopMenuItem m_SystemMenuItem;
		protected Cerebrum.Windows.Forms.DesktopMenuItem m_WindowMenuItem;
		protected Cerebrum.Windows.Forms.DesktopMenuItem m_SecurityMenuItem;
		protected Cerebrum.Windows.Forms.DesktopMenuItem m_HelpMenuItem;
		private Cerebrum.Windows.Forms.DesktopBandButton toolBarButtonHide;
		private System.Windows.Forms.HelpProvider helpProvider1;
		private System.Windows.Forms.ToolTip toolTip1;
		private Cerebrum.Windows.Forms.PaintHelperRect toolbarDrawHelper;
		private Cerebrum.Windows.Forms.ImageButton btnScrollLeft;
		private Cerebrum.Windows.Forms.ImageButton btnSrollRight;
		private System.Windows.Forms.MenuItem menuItemWindowArrangeAll;
		private System.Windows.Forms.MenuItem menuItemWindowTileHorizontal;
		private System.Windows.Forms.MenuItem menuItemWindowTileVertical;
		private System.Windows.Forms.MenuItem menuItemWindowCascade;
		private System.Windows.Forms.MdiClient mdiClient1;
		private System.ComponentModel.IContainer components;

		/// <summary>
		/// ����������� ������
		/// </summary>
		public PrimaryWindowForm():base(Cerebrum.Windows.Forms.DesktopComponentDisposition.Alone)
		{
			try 
			{
				this.m_UiCommands = new System.Collections.ArrayList();
				this.m_HotKeyUiControls = new System.Collections.Hashtable();
			
				//
				// Required for Windows Form Designer support
				//
				InitializeComponent();

				//
				// TODO: Add any constructor code after InitializeComponent call
				//

				this.m_SystemMenuItem.MenuItemId = Cerebrum.Windows.Forms.Specialized.Concepts.SystemMenuItemId;
				this.m_SecurityMenuItem.MenuItemId = Cerebrum.Windows.Forms.Specialized.Concepts.SecurityMenuItemId;
				this.m_WindowMenuItem.MenuItemId = Cerebrum.Windows.Forms.Specialized.Concepts.WindowMenuItemId;
				this.m_HelpMenuItem.MenuItemId = Cerebrum.Windows.Forms.Specialized.Concepts.HelpMenuItemId;
			}
			catch(System.Exception ex)
			{
				Cerebrum.Windows.Forms.Application application = this.m_Application;

				if(application==null)
				{
					application = Cerebrum.Windows.Forms.Application.Instance;
				}

				if(application==null)
				{
					System.Windows.Forms.MessageBox.Show(ex.ToString());
				}
				else
				{
					application.ProcessException(ex, "PrimaryWindow.ctor");
				}
			}
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);

			this.Application = (Cerebrum.DesktopEngine.Application)this.DomainContext.Application;
			this.BuildUi();
		}

		/// <summary>
		/// ������� �������� ������.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
				if(m_UiCommands!=null)
				{
					foreach(object o in m_UiCommands)
					{
						System.IDisposable d = o as System.IDisposable;
						if(d!=null) d.Dispose();
					}
				}
				if(m_UiServices!=null)
				{
					foreach(object o in m_UiServices)
					{
						System.IDisposable d = o as System.IDisposable;
						if(d!=null) d.Dispose();
					}
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(PrimaryWindowForm));
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.toolBar1 = new Cerebrum.Windows.Forms.TabHeader();
			this.btnScrollLeft = new Cerebrum.Windows.Forms.ImageButton();
			this.btnSrollRight = new Cerebrum.Windows.Forms.ImageButton();
			this.toolBarButtonHide = new Cerebrum.Windows.Forms.DesktopBandButton();
			this.m_MainMenu = new Cerebrum.Windows.Forms.DesktopMainMenu(this.components);
			this.m_SystemMenuItem = new Cerebrum.Windows.Forms.DesktopMenuItem();
			this.m_SecurityMenuItem = new Cerebrum.Windows.Forms.DesktopMenuItem();
			this.m_WindowMenuItem = new Cerebrum.Windows.Forms.DesktopMenuItem();
			this.menuItemWindowArrangeAll = new System.Windows.Forms.MenuItem();
			this.menuItemWindowTileHorizontal = new System.Windows.Forms.MenuItem();
			this.menuItemWindowTileVertical = new System.Windows.Forms.MenuItem();
			this.menuItemWindowCascade = new System.Windows.Forms.MenuItem();
			this.m_HelpMenuItem = new Cerebrum.Windows.Forms.DesktopMenuItem();
			this.helpProvider1 = new System.Windows.Forms.HelpProvider();
			this.mdiClient1 = new System.Windows.Forms.MdiClient();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.toolbarDrawHelper = new Cerebrum.Windows.Forms.PaintHelperRect(this.components);
			this.toolBar1.SuspendLayout();
			this.SuspendLayout();
			// 
			// statusBar1
			// 
			this.statusBar1.AccessibleDescription = resources.GetString("statusBar1.AccessibleDescription");
			this.statusBar1.AccessibleName = resources.GetString("statusBar1.AccessibleName");
			this.statusBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("statusBar1.Anchor")));
			this.statusBar1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("statusBar1.BackgroundImage")));
			this.statusBar1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("statusBar1.Dock")));
			this.statusBar1.Enabled = ((bool)(resources.GetObject("statusBar1.Enabled")));
			this.statusBar1.Font = ((System.Drawing.Font)(resources.GetObject("statusBar1.Font")));
			this.helpProvider1.SetHelpKeyword(this.statusBar1, resources.GetString("statusBar1.HelpKeyword"));
			this.helpProvider1.SetHelpNavigator(this.statusBar1, ((System.Windows.Forms.HelpNavigator)(resources.GetObject("statusBar1.HelpNavigator"))));
			this.helpProvider1.SetHelpString(this.statusBar1, resources.GetString("statusBar1.HelpString"));
			this.statusBar1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("statusBar1.ImeMode")));
			this.statusBar1.Location = ((System.Drawing.Point)(resources.GetObject("statusBar1.Location")));
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("statusBar1.RightToLeft")));
			this.helpProvider1.SetShowHelp(this.statusBar1, ((bool)(resources.GetObject("statusBar1.ShowHelp"))));
			this.statusBar1.Size = ((System.Drawing.Size)(resources.GetObject("statusBar1.Size")));
			this.statusBar1.TabIndex = ((int)(resources.GetObject("statusBar1.TabIndex")));
			this.statusBar1.Text = resources.GetString("statusBar1.Text");
			this.toolTip1.SetToolTip(this.statusBar1, resources.GetString("statusBar1.ToolTip"));
			this.statusBar1.Visible = ((bool)(resources.GetObject("statusBar1.Visible")));
			// 
			// toolBar1
			// 
			this.toolBar1.AccessibleDescription = resources.GetString("toolBar1.AccessibleDescription");
			this.toolBar1.AccessibleName = resources.GetString("toolBar1.AccessibleName");
			this.toolBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("toolBar1.Anchor")));
			this.toolBar1.AutoScroll = ((bool)(resources.GetObject("toolBar1.AutoScroll")));
			this.toolBar1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("toolBar1.AutoScrollMargin")));
			this.toolBar1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("toolBar1.AutoScrollMinSize")));
			this.toolBar1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolBar1.BackgroundImage")));
			this.toolBar1.Behavior = Cerebrum.Windows.Forms.SelectionBehavior.ManualSelect;
			this.toolBar1.Controls.Add(this.btnScrollLeft);
			this.toolBar1.Controls.Add(this.btnSrollRight);
			this.toolBar1.Controls.Add(this.toolBarButtonHide);
			this.toolBar1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("toolBar1.Dock")));
			this.toolBar1.Enabled = ((bool)(resources.GetObject("toolBar1.Enabled")));
			this.toolBar1.Font = ((System.Drawing.Font)(resources.GetObject("toolBar1.Font")));
			this.helpProvider1.SetHelpKeyword(this.toolBar1, resources.GetString("toolBar1.HelpKeyword"));
			this.helpProvider1.SetHelpNavigator(this.toolBar1, ((System.Windows.Forms.HelpNavigator)(resources.GetObject("toolBar1.HelpNavigator"))));
			this.helpProvider1.SetHelpString(this.toolBar1, resources.GetString("toolBar1.HelpString"));
			this.toolBar1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("toolBar1.ImeMode")));
			this.toolBar1.Interval = 1;
			this.toolBar1.Location = ((System.Drawing.Point)(resources.GetObject("toolBar1.Location")));
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("toolBar1.RightToLeft")));
			this.toolBar1.ScrollLeftButton = this.btnScrollLeft;
			this.toolBar1.ScrollRightButton = this.btnSrollRight;
			this.helpProvider1.SetShowHelp(this.toolBar1, ((bool)(resources.GetObject("toolBar1.ShowHelp"))));
			this.toolBar1.Size = ((System.Drawing.Size)(resources.GetObject("toolBar1.Size")));
			this.toolBar1.TabIndex = ((int)(resources.GetObject("toolBar1.TabIndex")));
			this.toolBar1.Text = resources.GetString("toolBar1.Text");
			this.toolTip1.SetToolTip(this.toolBar1, resources.GetString("toolBar1.ToolTip"));
			this.toolBar1.Visible = ((bool)(resources.GetObject("toolBar1.Visible")));
			// 
			// btnScrollLeft
			// 
			this.btnScrollLeft.AccessibleDescription = resources.GetString("btnScrollLeft.AccessibleDescription");
			this.btnScrollLeft.AccessibleName = resources.GetString("btnScrollLeft.AccessibleName");
			this.btnScrollLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnScrollLeft.Anchor")));
			this.btnScrollLeft.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnScrollLeft.BackgroundImage")));
			this.btnScrollLeft.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnScrollLeft.Dock")));
			this.btnScrollLeft.Enabled = ((bool)(resources.GetObject("btnScrollLeft.Enabled")));
			this.btnScrollLeft.Font = ((System.Drawing.Font)(resources.GetObject("btnScrollLeft.Font")));
			this.helpProvider1.SetHelpKeyword(this.btnScrollLeft, resources.GetString("btnScrollLeft.HelpKeyword"));
			this.helpProvider1.SetHelpNavigator(this.btnScrollLeft, ((System.Windows.Forms.HelpNavigator)(resources.GetObject("btnScrollLeft.HelpNavigator"))));
			this.helpProvider1.SetHelpString(this.btnScrollLeft, resources.GetString("btnScrollLeft.HelpString"));
			this.btnScrollLeft.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnScrollLeft.ImeMode")));
			this.btnScrollLeft.Location = ((System.Drawing.Point)(resources.GetObject("btnScrollLeft.Location")));
			this.btnScrollLeft.Name = "btnScrollLeft";
			this.btnScrollLeft.NormalLeaveImage = ((System.Drawing.Image)(resources.GetObject("btnScrollLeft.NormalLeaveImage")));
			this.btnScrollLeft.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnScrollLeft.RightToLeft")));
			this.helpProvider1.SetShowHelp(this.btnScrollLeft, ((bool)(resources.GetObject("btnScrollLeft.ShowHelp"))));
			this.btnScrollLeft.Size = ((System.Drawing.Size)(resources.GetObject("btnScrollLeft.Size")));
			this.btnScrollLeft.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("btnScrollLeft.SizeMode")));
			this.btnScrollLeft.TabIndex = ((int)(resources.GetObject("btnScrollLeft.TabIndex")));
			this.btnScrollLeft.TabStop = false;
			this.btnScrollLeft.Text = resources.GetString("btnScrollLeft.Text");
			this.toolTip1.SetToolTip(this.btnScrollLeft, resources.GetString("btnScrollLeft.ToolTip"));
			this.btnScrollLeft.Visible = ((bool)(resources.GetObject("btnScrollLeft.Visible")));
			// 
			// btnSrollRight
			// 
			this.btnSrollRight.AccessibleDescription = resources.GetString("btnSrollRight.AccessibleDescription");
			this.btnSrollRight.AccessibleName = resources.GetString("btnSrollRight.AccessibleName");
			this.btnSrollRight.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnSrollRight.Anchor")));
			this.btnSrollRight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSrollRight.BackgroundImage")));
			this.btnSrollRight.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnSrollRight.Dock")));
			this.btnSrollRight.Enabled = ((bool)(resources.GetObject("btnSrollRight.Enabled")));
			this.btnSrollRight.Font = ((System.Drawing.Font)(resources.GetObject("btnSrollRight.Font")));
			this.helpProvider1.SetHelpKeyword(this.btnSrollRight, resources.GetString("btnSrollRight.HelpKeyword"));
			this.helpProvider1.SetHelpNavigator(this.btnSrollRight, ((System.Windows.Forms.HelpNavigator)(resources.GetObject("btnSrollRight.HelpNavigator"))));
			this.helpProvider1.SetHelpString(this.btnSrollRight, resources.GetString("btnSrollRight.HelpString"));
			this.btnSrollRight.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnSrollRight.ImeMode")));
			this.btnSrollRight.Location = ((System.Drawing.Point)(resources.GetObject("btnSrollRight.Location")));
			this.btnSrollRight.Name = "btnSrollRight";
			this.btnSrollRight.NormalLeaveImage = ((System.Drawing.Image)(resources.GetObject("btnSrollRight.NormalLeaveImage")));
			this.btnSrollRight.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnSrollRight.RightToLeft")));
			this.helpProvider1.SetShowHelp(this.btnSrollRight, ((bool)(resources.GetObject("btnSrollRight.ShowHelp"))));
			this.btnSrollRight.Size = ((System.Drawing.Size)(resources.GetObject("btnSrollRight.Size")));
			this.btnSrollRight.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("btnSrollRight.SizeMode")));
			this.btnSrollRight.TabIndex = ((int)(resources.GetObject("btnSrollRight.TabIndex")));
			this.btnSrollRight.TabStop = false;
			this.btnSrollRight.Text = resources.GetString("btnSrollRight.Text");
			this.toolTip1.SetToolTip(this.btnSrollRight, resources.GetString("btnSrollRight.ToolTip"));
			this.btnSrollRight.Visible = ((bool)(resources.GetObject("btnSrollRight.Visible")));
			// 
			// toolBarButtonHide
			// 
			this.toolBarButtonHide.AccessibleDescription = resources.GetString("toolBarButtonHide.AccessibleDescription");
			this.toolBarButtonHide.AccessibleName = resources.GetString("toolBarButtonHide.AccessibleName");
			this.toolBarButtonHide.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("toolBarButtonHide.Anchor")));
			this.toolBarButtonHide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolBarButtonHide.BackgroundImage")));
			this.toolBarButtonHide.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("toolBarButtonHide.Dock")));
			this.toolBarButtonHide.Enabled = ((bool)(resources.GetObject("toolBarButtonHide.Enabled")));
			this.toolBarButtonHide.Font = ((System.Drawing.Font)(resources.GetObject("toolBarButtonHide.Font")));
			this.helpProvider1.SetHelpKeyword(this.toolBarButtonHide, resources.GetString("toolBarButtonHide.HelpKeyword"));
			this.helpProvider1.SetHelpNavigator(this.toolBarButtonHide, ((System.Windows.Forms.HelpNavigator)(resources.GetObject("toolBarButtonHide.HelpNavigator"))));
			this.helpProvider1.SetHelpString(this.toolBarButtonHide, resources.GetString("toolBarButtonHide.HelpString"));
			this.toolBarButtonHide.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("toolBarButtonHide.ImeMode")));
			this.toolBarButtonHide.Location = ((System.Drawing.Point)(resources.GetObject("toolBarButtonHide.Location")));
			this.toolBarButtonHide.Name = "toolBarButtonHide";
			this.toolBarButtonHide.NormalLeaveImage = ((System.Drawing.Image)(resources.GetObject("toolBarButtonHide.NormalLeaveImage")));
			this.toolBarButtonHide.OwnerDraw = true;
			this.toolBarButtonHide.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("toolBarButtonHide.RightToLeft")));
			this.helpProvider1.SetShowHelp(this.toolBarButtonHide, ((bool)(resources.GetObject("toolBarButtonHide.ShowHelp"))));
			this.toolBarButtonHide.Size = ((System.Drawing.Size)(resources.GetObject("toolBarButtonHide.Size")));
			this.toolBarButtonHide.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("toolBarButtonHide.SizeMode")));
			this.toolBarButtonHide.TabIndex = ((int)(resources.GetObject("toolBarButtonHide.TabIndex")));
			this.toolBarButtonHide.TabStop = false;
			this.toolBarButtonHide.Text = resources.GetString("toolBarButtonHide.Text");
			this.toolTip1.SetToolTip(this.toolBarButtonHide, resources.GetString("toolBarButtonHide.ToolTip"));
			this.toolBarButtonHide.Visible = ((bool)(resources.GetObject("toolBarButtonHide.Visible")));
			this.toolBarButtonHide.Click += new System.EventHandler(this.OnUiCommandItemClick);
			// 
			// m_MainMenu
			// 
			this.m_MainMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.m_MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.m_SystemMenuItem,
																					   this.m_SecurityMenuItem,
																					   this.m_WindowMenuItem,
																					   this.m_HelpMenuItem});
			this.m_MainMenu.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("m_MainMenu.RightToLeft")));
			// 
			// m_SystemMenuItem
			// 
			this.m_SystemMenuItem.Enabled = ((bool)(resources.GetObject("m_SystemMenuItem.Enabled")));
			this.m_SystemMenuItem.Index = 0;
			this.m_SystemMenuItem.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("m_SystemMenuItem.Shortcut")));
			this.m_SystemMenuItem.ShowShortcut = ((bool)(resources.GetObject("m_SystemMenuItem.ShowShortcut")));
			this.m_SystemMenuItem.Text = resources.GetString("m_SystemMenuItem.Text");
			this.m_SystemMenuItem.Visible = ((bool)(resources.GetObject("m_SystemMenuItem.Visible")));
			// 
			// m_SecurityMenuItem
			// 
			this.m_SecurityMenuItem.Enabled = ((bool)(resources.GetObject("m_SecurityMenuItem.Enabled")));
			this.m_SecurityMenuItem.Index = 1;
			this.m_SecurityMenuItem.MergeOrder = 100;
			this.m_SecurityMenuItem.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("m_SecurityMenuItem.Shortcut")));
			this.m_SecurityMenuItem.ShowShortcut = ((bool)(resources.GetObject("m_SecurityMenuItem.ShowShortcut")));
			this.m_SecurityMenuItem.Text = resources.GetString("m_SecurityMenuItem.Text");
			this.m_SecurityMenuItem.Visible = ((bool)(resources.GetObject("m_SecurityMenuItem.Visible")));
			// 
			// m_WindowMenuItem
			// 
			this.m_WindowMenuItem.Enabled = ((bool)(resources.GetObject("m_WindowMenuItem.Enabled")));
			this.m_WindowMenuItem.Index = 2;
			this.m_WindowMenuItem.MdiList = true;
			this.m_WindowMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							 this.menuItemWindowArrangeAll,
																							 this.menuItemWindowTileHorizontal,
																							 this.menuItemWindowTileVertical,
																							 this.menuItemWindowCascade});
			this.m_WindowMenuItem.MergeOrder = 500;
			this.m_WindowMenuItem.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("m_WindowMenuItem.Shortcut")));
			this.m_WindowMenuItem.ShowShortcut = ((bool)(resources.GetObject("m_WindowMenuItem.ShowShortcut")));
			this.m_WindowMenuItem.Text = resources.GetString("m_WindowMenuItem.Text");
			this.m_WindowMenuItem.Visible = ((bool)(resources.GetObject("m_WindowMenuItem.Visible")));
			// 
			// menuItemWindowArrangeAll
			// 
			this.menuItemWindowArrangeAll.Enabled = ((bool)(resources.GetObject("menuItemWindowArrangeAll.Enabled")));
			this.menuItemWindowArrangeAll.Index = 0;
			this.menuItemWindowArrangeAll.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("menuItemWindowArrangeAll.Shortcut")));
			this.menuItemWindowArrangeAll.ShowShortcut = ((bool)(resources.GetObject("menuItemWindowArrangeAll.ShowShortcut")));
			this.menuItemWindowArrangeAll.Text = resources.GetString("menuItemWindowArrangeAll.Text");
			this.menuItemWindowArrangeAll.Visible = ((bool)(resources.GetObject("menuItemWindowArrangeAll.Visible")));
			this.menuItemWindowArrangeAll.Click += new System.EventHandler(this.menuItemWindowArrangeAll_Click);
			// 
			// menuItemWindowTileHorizontal
			// 
			this.menuItemWindowTileHorizontal.Enabled = ((bool)(resources.GetObject("menuItemWindowTileHorizontal.Enabled")));
			this.menuItemWindowTileHorizontal.Index = 1;
			this.menuItemWindowTileHorizontal.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("menuItemWindowTileHorizontal.Shortcut")));
			this.menuItemWindowTileHorizontal.ShowShortcut = ((bool)(resources.GetObject("menuItemWindowTileHorizontal.ShowShortcut")));
			this.menuItemWindowTileHorizontal.Text = resources.GetString("menuItemWindowTileHorizontal.Text");
			this.menuItemWindowTileHorizontal.Visible = ((bool)(resources.GetObject("menuItemWindowTileHorizontal.Visible")));
			this.menuItemWindowTileHorizontal.Click += new System.EventHandler(this.menuItemWindowTileHorizontal_Click);
			// 
			// menuItemWindowTileVertical
			// 
			this.menuItemWindowTileVertical.Enabled = ((bool)(resources.GetObject("menuItemWindowTileVertical.Enabled")));
			this.menuItemWindowTileVertical.Index = 2;
			this.menuItemWindowTileVertical.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("menuItemWindowTileVertical.Shortcut")));
			this.menuItemWindowTileVertical.ShowShortcut = ((bool)(resources.GetObject("menuItemWindowTileVertical.ShowShortcut")));
			this.menuItemWindowTileVertical.Text = resources.GetString("menuItemWindowTileVertical.Text");
			this.menuItemWindowTileVertical.Visible = ((bool)(resources.GetObject("menuItemWindowTileVertical.Visible")));
			this.menuItemWindowTileVertical.Click += new System.EventHandler(this.menuItemWindowTileVertical_Click);
			// 
			// menuItemWindowCascade
			// 
			this.menuItemWindowCascade.Enabled = ((bool)(resources.GetObject("menuItemWindowCascade.Enabled")));
			this.menuItemWindowCascade.Index = 3;
			this.menuItemWindowCascade.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("menuItemWindowCascade.Shortcut")));
			this.menuItemWindowCascade.ShowShortcut = ((bool)(resources.GetObject("menuItemWindowCascade.ShowShortcut")));
			this.menuItemWindowCascade.Text = resources.GetString("menuItemWindowCascade.Text");
			this.menuItemWindowCascade.Visible = ((bool)(resources.GetObject("menuItemWindowCascade.Visible")));
			this.menuItemWindowCascade.Click += new System.EventHandler(this.menuItemWindowCascade_Click);
			// 
			// m_HelpMenuItem
			// 
			this.m_HelpMenuItem.Enabled = ((bool)(resources.GetObject("m_HelpMenuItem.Enabled")));
			this.m_HelpMenuItem.Index = 3;
			this.m_HelpMenuItem.MergeOrder = 900;
			this.m_HelpMenuItem.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("m_HelpMenuItem.Shortcut")));
			this.m_HelpMenuItem.ShowShortcut = ((bool)(resources.GetObject("m_HelpMenuItem.ShowShortcut")));
			this.m_HelpMenuItem.Text = resources.GetString("m_HelpMenuItem.Text");
			this.m_HelpMenuItem.Visible = ((bool)(resources.GetObject("m_HelpMenuItem.Visible")));
			// 
			// helpProvider1
			// 
			this.helpProvider1.HelpNamespace = resources.GetString("helpProvider1.HelpNamespace");
			// 
			// mdiClient1
			// 
			this.mdiClient1.AccessibleDescription = resources.GetString("mdiClient1.AccessibleDescription");
			this.mdiClient1.AccessibleName = resources.GetString("mdiClient1.AccessibleName");
			this.mdiClient1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("mdiClient1.Anchor")));
			this.mdiClient1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mdiClient1.BackgroundImage")));
			this.mdiClient1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("mdiClient1.Dock")));
			this.mdiClient1.Enabled = ((bool)(resources.GetObject("mdiClient1.Enabled")));
			this.mdiClient1.Font = ((System.Drawing.Font)(resources.GetObject("mdiClient1.Font")));
			this.helpProvider1.SetHelpKeyword(this.mdiClient1, resources.GetString("mdiClient1.HelpKeyword"));
			this.helpProvider1.SetHelpNavigator(this.mdiClient1, ((System.Windows.Forms.HelpNavigator)(resources.GetObject("mdiClient1.HelpNavigator"))));
			this.helpProvider1.SetHelpString(this.mdiClient1, resources.GetString("mdiClient1.HelpString"));
			this.mdiClient1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("mdiClient1.ImeMode")));
			this.mdiClient1.Location = ((System.Drawing.Point)(resources.GetObject("mdiClient1.Location")));
			this.mdiClient1.Name = "mdiClient1";
			this.mdiClient1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("mdiClient1.RightToLeft")));
			this.helpProvider1.SetShowHelp(this.mdiClient1, ((bool)(resources.GetObject("mdiClient1.ShowHelp"))));
			this.mdiClient1.Size = ((System.Drawing.Size)(resources.GetObject("mdiClient1.Size")));
			this.mdiClient1.TabIndex = ((int)(resources.GetObject("mdiClient1.TabIndex")));
			this.mdiClient1.Text = resources.GetString("mdiClient1.Text");
			this.toolTip1.SetToolTip(this.mdiClient1, resources.GetString("mdiClient1.ToolTip"));
			this.mdiClient1.Visible = ((bool)(resources.GetObject("mdiClient1.Visible")));
			// 
			// toolbarDrawHelper
			// 
			this.toolbarDrawHelper.Alpha = ((System.Byte)(196));
			this.toolbarDrawHelper.BorderColor = System.Drawing.Color.Navy;
			this.toolbarDrawHelper.CheckBackColor = System.Drawing.Color.Gold;
			this.toolbarDrawHelper.DisabledBackColor = System.Drawing.SystemColors.Control;
			this.toolbarDrawHelper.EnterBackColor = System.Drawing.Color.DeepSkyBlue;
			this.toolbarDrawHelper.LeaveBackColor = System.Drawing.Color.Transparent;
			this.toolbarDrawHelper.PressBackColor = System.Drawing.Color.Pink;
			// 
			// PrimaryWindowForm
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.toolBar1);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.mdiClient1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.helpProvider1.SetHelpKeyword(this, resources.GetString("$this.HelpKeyword"));
			this.helpProvider1.SetHelpNavigator(this, ((System.Windows.Forms.HelpNavigator)(resources.GetObject("$this.HelpNavigator"))));
			this.helpProvider1.SetHelpString(this, resources.GetString("$this.HelpString"));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.IsMdiContainer = true;
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.Menu = this.m_MainMenu;
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "PrimaryWindowForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.helpProvider1.SetShowHelp(this, ((bool)(resources.GetObject("$this.ShowHelp"))));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.toolBar1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		/// <summary>
		/// �������� ������������ ������ �� ������ ���.
		/// ����� ���� ������������ ��� ���������� ������ �����.
		/// </summary>
		public System.Windows.Forms.StatusBar StatusBar
		{
			get
			{
				return this.statusBar1;
			}
		}

		public Cerebrum.Windows.Forms.TabHeader ToolBar
		{
			get
			{
				return this.toolBar1;
			}
		}
		/// <summary>
		/// ��������, ������������ ������ �� �������� ���������������
		/// ���������� ������� ������
		/// </summary>
		public System.Windows.Forms.HelpProvider HelpProvider
		{
			get
			{
				return helpProvider1;
			}
		}

		public Cerebrum.Windows.Forms.IUiCommandsCollection Commands
		{
			get
			{
				if(this.m_UiCommandsCollection==null)
				{
					this.m_UiCommandsCollection = new UiCommandsCollection(this);
				}
				return this.m_UiCommandsCollection;
			}
		}

		public Cerebrum.Windows.Forms.IUiCommand[] GetCommands(string messageName)
		{
			System.Collections.ArrayList list = new System.Collections.ArrayList();
			foreach(Cerebrum.Windows.Forms.ComponentModel.UiCommand cmd in this.m_UiCommands)
			{
				if(cmd.MessageName==messageName)
				{
					list.Add(cmd);
				}
			}
			return (Cerebrum.Windows.Forms.IUiCommand[])list.ToArray(typeof(Cerebrum.Windows.Forms.IUiCommand));
		}
		//
		//		/// <summary>
		//		/// ���������� ��������� � �������� ��������� ��������
		//		/// </summary>
		//		/// <param name="commandId">��� ������ ��������</param>
		//		/// <param name="component">������ �� ������������ ���������</param>
		//		public void AttachToCommand(Cerebrum.Integrator.UiCommand command, Cerebrum.Integrator.IComponent component)
		//		{
		//			if(component!=null && command!=null)
		//			{
		//				if(!command.DynamicCommand)
		//				{
		//					command.Executing += new System.ComponentModel.CancelEventHandler(component.Execute);
		//					component.Cleaning += new System.EventHandler(command.CleaningExecuting);
		//				}
		//				component.Execute(command, new Cerebrum.Integrator.AttachCommandEventArgs(command));
		//			}
		//		}
		//		/// <summary>
		//		/// ���������� ��������� �� ���� ���������, ������������
		//		/// ��� ���� � �� ������������ ��� ������� ������ ������������������� ������������
		//		/// </summary>
		//		/// <param name="component">������ �� ������������ ���������</param>
		//		public void AttachToCommands(Cerebrum.Integrator.IComponent component)
		//		{
		//			string GroupId = m_Application.GroupId;
		//
		//			System.Data.DataView dv = new System.Data.DataView(m_DomainContext.SystemDatabase.UiComponents);
		//			dv.RowFilter = "GroupId='" + GroupId + "' and ComponentId='" + component.ComponentId + "'";
		//			//dv.Sort = "UiCommandsRow.ToolBarIndex";
		//			//, "", System.Data.DataViewRowState.OriginalRows);
		//
		//			foreach(System.Data.DataRowView rv in dv)
		//			{
		//				Cerebrum.Integrator.SystemDatabase.UiComponentsRow rwi = (Cerebrum.Integrator.SystemDatabase.UiComponentsRow)(rv.Row);
		//				AttachToCommand((Cerebrum.Integrator.UiCommand)m_UiCommands[rwi.CommandId], component);
		//			}
		//		}
		
	
		protected System.Collections.ArrayList m_UiServices = new ArrayList();

		/// <summary>
		/// �������� ��������� ������� ������������ � ��������� ����������
		/// </summary>
		protected System.Collections.ArrayList m_UiCommands;
		private Cerebrum.DesktopEngine.PrimaryWindowForm.UiCommandsCollection m_UiCommandsCollection;

		public void BuildHelp()
		{
			//			System.IO.FileStream fs = null;
			//			System.IO.StreamWriter sw = null;
			//			try
			//			{
			//				fs = new System.IO.FileStream(System.IO.Path.Combine(m_DomainContext.SystemDirectory, "Cerebrum.Integrator.IFrame.html"), System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
			//				sw = new System.IO.StreamWriter(fs);
			//			
			//				sw.WriteLine("<html>");
			//				sw.WriteLine("<head>");
			//				sw.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
			//				sw.WriteLine("</head>");
			//				sw.WriteLine("<body>");
			//
			//				string GroupId = m_Application.GroupId;
			//
			//				System.Data.DataView dv = new System.Data.DataView(m_DomainContext.SystemDatabase.Variables);
			//				dv.RowFilter = "VariableId = 'help'";
			//				foreach(System.Data.DataRowView rv in dv)
			//				{
			//					Cerebrum.Integrator.SystemDatabase.VariablesRow rwv = (Cerebrum.Integrator.SystemDatabase.VariablesRow)rv.Row;
			//					System.Data.DataView dv2 = new System.Data.DataView(m_DomainContext.SystemDatabase.UiComponents);
			//					dv2.RowFilter = "GroupId='" + GroupId + "' and ComponentId='" + rwv.InstanceId + "'";
			//					if(dv2.Count > 0)
			//					{
			//						string caption = "";
			//						Cerebrum.Integrator.SystemDatabase.VariablesRow rwv2 = m_DomainContext.SystemDatabase.Variables.FindByInstanceIdVariableId(rwv.InstanceId,"caption");
			//						if(rwv2!=null)
			//						{
			//							caption = (string)m_DomainContext.GetSystemVariable(rwv2.InstanceId, rwv2.VariableId);
			//						}
			//						else
			//						{
			//							caption = rwv.InstanceId;
			//						}
			//						string help = (string)m_DomainContext.GetSystemVariable(rwv.InstanceId, rwv.VariableId);
			//						sw.WriteLine("<a target=\"_top\" href=\"{0}\">{1}</a><br>", help, caption);
			//					}
			//				}
			//				sw.WriteLine("</body>");
			//				sw.WriteLine("</html>");
			//
			//			}
			//			catch(System.Exception ex)
			//			{
			//				m_Application.AddActivityRow(ex, "PrimaryWindow.BuildHelp");
			//			}
			//			finally
			//			{
			//				if(sw!=null) sw.Close();
			//				if(fs!=null) fs.Close();
			//			}

		}
		/// <summary>
		/// �������������� ���� � ������ ������������
		/// </summary>
		public virtual void BuildUi()
		{
			try 
			{
				System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();

				Cerebrum.Windows.Forms.DesktopMenuItem mi;
				m_MenuItemsHashtable = new System.Collections.Hashtable();
				foreach(Cerebrum.Windows.Forms.DesktopMenuItem m1 in m_MainMenu.MenuItems)
				{
					m_MenuItemsHashtable.Add(m1.MenuItemId, m1);
				}

				//				string groupId = "";
				//				string loginId = "";
				//
				//				if(m_DomainContext!=null && m_Application!=null)
				//				{
				//					groupId = m_Application.GroupId;
				//					loginId = m_Application.UserId;
				//				}
				//
				Cerebrum.IConnector connectorInitMsg = null;
				try
				{
					//					System.Data.DataView dvc = new System.Data.DataView(m_DomainContext.SystemDatabase.UiCommands);
					//					System.Data.DataView dvi = new System.Data.DataView(m_DomainContext.SystemDatabase.UiComponents);
					//					System.Data.DataView dvl = new System.Data.DataView(m_DomainContext.SystemDatabase.LoginGroups);
					//
					//					// for each Command
					//					dvc.Sort = "OrderIndex";
					//					dvl.RowFilter = "LoginId='" + loginId + "'";
					//					dvl.Sort = "OrderIndex";
					//
					//					foreach(System.Data.DataRowView rvc in dvc)
					//					{
					//						Cerebrum.Integrator.SystemDatabase.UiCommandsRow rwc = (Cerebrum.Integrator.SystemDatabase.UiCommandsRow)(rvc.Row);
					//
					//						System.Collections.Hashtable huicmps = new Hashtable();
					//
					//						// for each Login Group
					//
					//						System.Collections.IEnumerator enl = dvl.GetEnumerator();
					//						bool ensure = true;
					//						bool canget = true;
					//						while((canget && (canget=enl.MoveNext())) || ensure)
					//						{
					//							ensure = false;
					//
					//							if(canget)
					//							{
					//								Cerebrum.Integrator.SystemDatabase.LoginGroupsRow rwl = (Cerebrum.Integrator.SystemDatabase.LoginGroupsRow)((System.Data.DataRowView)enl.Current).Row;
					//								dvi.RowFilter = "GroupId='" + rwl.GroupId + "' and CommandId='" + rwc.CommandId + "'";
					//							}
					//							else
					//							{
					//								dvi.RowFilter = "GroupId='" + groupId + "' and CommandId='" + rwc.CommandId + "'";
					//							}
					//
					//							//for each UiComponent
					//
					//							foreach(System.Data.DataRowView rvi in dvi)
					//							{
					//								Cerebrum.Integrator.SystemDatabase.UiComponentsRow rwi = (Cerebrum.Integrator.SystemDatabase.UiComponentsRow)(rvi.Row);
					//
					//								if(m_DomainContext.ExistsComponent(rwi.ComponentId))
					//								{
					connectorInitMsg = this.DomainContext.AttachConnector(Cerebrum.Management.Utilites.ResolveHandle(this.DomainContext, "Messages", "Name", "Init"));
					using(Cerebrum.Data.TableView vwvUiServices = Cerebrum.Management.Utilites.GetView(this.DomainContext, "UiServices"), vwvUiCommands = Cerebrum.Management.Utilites.GetView(this.DomainContext, "UiCommands"))
					{
						if(vwvUiServices!=null)
						{
							System.ComponentModel.PropertyDescriptorCollection prpsUiServices = vwvUiServices.GetItemProperties(null);
#warning "direct access to property"
							System.ComponentModel.PropertyDescriptor prpAttachedCommands = prpsUiServices["AttachedCommands"];
							System.ComponentModel.PropertyDescriptor prpAttachedControls = vwvUiCommands.GetItemProperties(null)["AttachedControls"];
							foreach(Cerebrum.Data.ComponentItemView srvv in vwvUiServices)
							{
								try
								{
									Cerebrum.Windows.Forms.ComponentModel.UiService svcProxy = new Cerebrum.Windows.Forms.ComponentModel.UiService(this.DomainContext, srvv.ObjectHandle);

									if(connectorInitMsg!=null)
									{
										svcProxy.ActivateCommand(connectorInitMsg, null, null);
									}

									using(Cerebrum.Data.SimpleView vwvUiAttachedCommands = (Cerebrum.Data.SimpleView)prpAttachedCommands.GetValue(srvv))
									{
										foreach(Cerebrum.Data.ComponentItemView cmdv in vwvUiAttachedCommands)
										{
											Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor cmd;
											using(Cerebrum.IComposite conUiCmd = this.DomainContext.AttachConnector(cmdv.ObjectHandle))
											{
												cmd = conUiCmd.Component as Cerebrum.Windows.Forms.Reflection.UiCommandDescriptor;
												//
												//									cmd = (Cerebrum.Integrator.UiCommand)m_UiCommands[rwc.CommandId];
												//
												//									if(cmd==null)
												//									{
												//										cmd = new Cerebrum.Integrator.UiCommand();
												//										cmd.UiCommandRow = rwc;
												//										cmd.CommandId = rwc.CommandId;
												//										cmd.DynamicCommand = rwc.DynamicEvent;
												//										cmd.PrimaryWindow = this;
												//										this.Cleaning += new System.EventHandler(cmd.Clean);
												//										m_UiCommands.Add(cmd.CommandId, cmd); 
												//
												//										if(rwc.IsHandlerIdNull())
												//										{
												if(cmd!=null)
												{
													Cerebrum.Windows.Forms.ComponentModel.UiCommand cmdProxy = new Cerebrum.Windows.Forms.ComponentModel.UiCommand(this.DomainContext, conUiCmd);
													cmdProxy.Activate += new ConnectorEventHandler(svcProxy.CommandActivated);
#warning "creating clone"
													Cerebrum.Data.ComponentItemView cmdv2 = vwvUiCommands.CreateItemViewByObjectHandle(cmdv.ObjectHandle);
													using(Cerebrum.Data.SimpleView vwvUiAttachedControls = (Cerebrum.Data.SimpleView)prpAttachedControls.GetValue(cmdv))
													{
														Cerebrum.ObjectHandle hattrFactoryHandle = Cerebrum.Management.Utilites.ResolveHandle(this.DomainContext, "Attributes", "Name", Cerebrum.DesktopEngine.Specialized.KnownNames.FactoryHandle);
														foreach(Cerebrum.Data.ComponentItemView ctlv in vwvUiAttachedControls)
														{
															Cerebrum.ObjectHandle itemId = ctlv.ObjectHandle;
															if(itemId!=Cerebrum.ObjectHandle.Null)
															{
																using(Cerebrum.IComposite conUiCtl = ctlv.GetComposite()/*this.DomainContext.AttachConnector(itemId)*/)
																{
																	object item = conUiCtl.Component;
																	Cerebrum.ObjectHandle hFactoryHandle = Cerebrum.ObjectHandle.Null;
																	if(hattrFactoryHandle!=Cerebrum.ObjectHandle.Null)
																	{
																		object oFactoryHandle = Cerebrum.Integrator.Utilites.ObtainComponent(conUiCtl as Cerebrum.IContainer, hattrFactoryHandle);
																		if(oFactoryHandle is Cerebrum.ObjectHandle)
																		{
																			hFactoryHandle = (Cerebrum.ObjectHandle)oFactoryHandle;
																		}
																	}
																	if(Cerebrum.ObjectHandle.Null==hFactoryHandle)
																	{
																		if(item is Cerebrum.Windows.Forms.Reflection.UiMenuItemDescriptor)
																		{
																			mi = MakeMenuItem(itemId);
																			//mi.CommandId = rwc.CommandId;
																			//mi.Click += new System.EventHandler(OnUiCommandItemClick);
																			cmdProxy.Controls.Add(mi);
																		}
																		else if(item is Cerebrum.Windows.Forms.Reflection.UiHotKeyDescriptor)
																		{
																			Cerebrum.Windows.Forms.Reflection.UiHotKeyDescriptor hc = (Cerebrum.Windows.Forms.Reflection.UiHotKeyDescriptor)item;
																			Cerebrum.Windows.Forms.ComponentModel.UiHotKey ui = null;

																			if(m_HotKeyUiControls.ContainsKey(hc.KeyCode))
																			{
																				ui = (Cerebrum.Windows.Forms.ComponentModel.UiHotKey)m_HotKeyUiControls[hc.KeyCode];
																			}
																			else
																			{
																				ui = new Cerebrum.Windows.Forms.ComponentModel.UiHotKey(this.DomainContext, itemId, hc.KeyCode);
																				m_HotKeyUiControls.Add(hc.KeyCode, ui);
																			}
																			cmdProxy.Controls.Add(ui);
																		}
																		else if(item is Cerebrum.Windows.Forms.Reflection.UiBandPlaceDescriptor)
																		{
																			Cerebrum.Windows.Forms.Reflection.UiBandPlaceDescriptor ud = (Cerebrum.Windows.Forms.Reflection.UiBandPlaceDescriptor)item;
																			Cerebrum.Windows.Forms.DesktopBandPlace bt;
																			bt = new Cerebrum.Windows.Forms.DesktopBandPlace();
																			bt.Size = new Size(ud.Width, 23);
																			//bt.CommandId = rwc.CommandId;
																			bt.Fellowship = true;
																			bt.MergeOrder = ud.MergeOrder;
																			this.toolTip1.SetToolTip(bt, ud.DisplayName);
											
																			AddBandItem(bt);

																			cmdProxy.Controls.Add(bt);
																		}
																		else if(item is Cerebrum.Windows.Forms.Reflection.UiBandItemDescriptor)
																		{
																			Cerebrum.Windows.Forms.Reflection.UiBandItemDescriptor ud = (Cerebrum.Windows.Forms.Reflection.UiBandItemDescriptor)item;
																			Cerebrum.Windows.Forms.DesktopBandButton bt;
																			bt = new Cerebrum.Windows.Forms.DesktopBandButton();
																			bt.Size = new Size(23, 23);
																			//bt.CommandId = rwc.CommandId;
																			bt.OwnerDraw = true;
																			//bt.Text = (string)m_DomainContext.GetSystemResource(rwc.ToolBarTipId);
																			bt.MergeOrder = ud.MergeOrder;
																			this.toolTip1.SetToolTip(bt, ud.DisplayName);
																			bt.Paint += new PaintEventHandler(toolbarDrawHelper.Paint);
																			//bt.Click += new EventHandler(OnUiCommandItemClick);
											
																			AddBandItem(bt);

																			cmdProxy.Controls.Add(bt);
																		}
																	}
																	else
																	{
																		using(Cerebrum.IComposite factoryConn = this.m_Connector.Workspace.AttachConnector(hFactoryHandle))
																		{
																			Cerebrum.Windows.Forms.IUiControlFactory factory = (Cerebrum.Windows.Forms.IUiControlFactory)factoryConn.Component;
																			Cerebrum.Windows.Forms.IUiControl ui = factory.CreateControl(item);
																			cmdProxy.Controls.Add(ui);
																		}
																	}
																}
															}
#warning "need IMPL"
															cmdProxy.Enabled = true;
															m_UiCommands.Add(cmdProxy);
														}
													}
												}
												//
												//											if((!rwc.IsBandIdNull()) && rwc.BandId == "system")
												//											{
												//												if((!rwc.IsTextIdNull()) && (rwc.TextId == "-" || rwc.TextId == "=") )
												//												{
												//													Cerebrum.Windows.Forms.IntegratorBandHolder bt;
												//													bt = new Cerebrum.Windows.Forms.IntegratorBandHolder();
												//													bt.Size = new Size(3, 3);
												//													bt.Fellowship = true;
												//													bt.Visible = (rwc.TextId == "-");
												//													//bt.CommandId = rwc.CommandId;
												//													//bt.Text = (string)m_DomainContext.GetSystemResource(rwc.ToolBarTipId);
												//													this.toolTip1.SetToolTip(bt, null);
												//													//bt.Click += new EventHandler(OnUiCommandItemClick);
												//
												//													toolBar1.Controls.Add(bt);
												//
												//													cmd.BandItem = bt;
												//												}
												//												else
												//												{
												//												}
												//											}
												//										}
												//										else
												//										{
												//											IComponent component = (m_DomainContext.AcquireComponent(rwc.HandlerId) as IComponent);
												//											if(component!=null) component.Execute(cmd, new CreateCommandEventArgs(cmd));
												//										}
												//
												//										/*Cerebrum.Windows.Forms.IUiCommandImage cimg;
												//										System.Drawing.Image image = null;
												//										
												//										cimg = cmd.BandItem as Cerebrum.Windows.Forms.IUiCommandImage;
												//										if(cimg!=null)
												//											try
												//											{
												//												if(image == null)
												//												{
												//													image = cmd.GetImage();
												//												}
												//												cimg.Image = image;
												//											}
												//											catch(System.Exception ex)
												//											{
												//												m_Application.AddActivityRow(ex, "PrimaryWindow.BuildTool.BuildToolbar.ImageId");
												//											}
												//										
												//										cimg = cmd.MenuItem as Cerebrum.Windows.Forms.IUiCommandImage;
												//										if(cimg!=null)
												//											try
												//											{
												//												if(image == null)
												//												{
												//													image = cmd.GetImage();
												//												}
												//												cimg.Image = image;
												//											}
												//											catch(System.Exception ex)
												//											{
												//												m_Application.AddActivityRow(ex, "PrimaryWindow.BuildTool.BuildToolbar.ImageId");
												//											}
												//										*/
												//									}
												//
												//
												//									if(rwi.AttachInstance && (!huicmps.ContainsKey(rwi.ComponentId)))
												//									{
												//										object o = m_DomainContext.AcquireComponent(rwi.ComponentId);
												//
												//										huicmps.Add(rwi.ComponentId, o);
												//
												//										if(o!=null && typeof(Cerebrum.Integrator.IComponent).IsAssignableFrom(o.GetType()))
												//										{
												//											Cerebrum.Integrator.IComponent component = (Cerebrum.Integrator.IComponent)o;
												//											cmd.Enabled = true;
												//											AttachToCommand(cmd, component);
												//										}
												//									}
												//								}
												//							}
												//						}
												//						huicmps.Clear();
												//						huicmps = null;
												//					}
											}
										}
									}

									// all done, so it is safe to store svcProxy for future use
									m_UiServices.Add(svcProxy);
								}
								catch(System.Exception ex)
								{
									m_Application.ProcessException(ex, "PrimaryWindow.BuildUi UiService initialization");
								}
							}
						}
					}

					string autorunAssemblies = string.Empty;
					try
					{
						autorunAssemblies = ((string)(configurationAppSettings.GetValue("AutorunAssemblies", typeof(string))));
					}
					catch
					{
					}

					if(autorunAssemblies!=null && autorunAssemblies!=string.Empty)
						foreach(string assemblyName in autorunAssemblies.Split(','))
						{
							System.Type uiServiceType = typeof(Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor);
							System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadWithPartialName(assemblyName.Trim());
							if(assembly!=null)
								foreach(System.Type type in assembly.GetTypes())
								{
									if(uiServiceType.IsAssignableFrom(type))
									{
										Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor uiservice = System.Activator.CreateInstance(type) as Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor;
										if(uiservice!=null)
										{
											((Cerebrum.IComponent)uiservice).SetConnector(Cerebrum.SerializeDirection.None, new Cerebrum.Runtime.SurrogateComposite(this.DomainContext.Workspace));
											uiservice.CommandActivated(connectorInitMsg, this, null);
										}
									}
								}
						}

					UpdateComponentUI(m_LastActiveComponent);
					//
					//			{
					//				Cerebrum.Integrator.UiCommand cmd;
					//
					//				cmd = (Cerebrum.Integrator.UiCommand)m_UiCommands["show"];
					//
					//				if(cmd==null)
					//				{
					//					cmd = new Cerebrum.Integrator.UiCommand();
					//					cmd.CommandId = "show";
					//					cmd.DynamicCommand = true;
					//					cmd.PrimaryWindow = this;
					//					m_UiCommands.Add(cmd.CommandId, cmd); 
					//				}
					//			}
					//

					System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Cerebrum.Windows.Forms.Application));

					bool quickNotifyIconEnable = true;
					try
					{
						quickNotifyIconEnable = ((bool)(configurationAppSettings.GetValue("QuickNotifyIcon.Enabled", typeof(bool))));
					}
					catch
					{
					}

					if(quickNotifyIconEnable)
					{
						mi = new Cerebrum.Windows.Forms.DesktopMenuItem();
						mi.Text = resources.GetString("ApplicationHide.Caption");
						mi.CommandId = Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationHideCommandId;
						mi.Click += new System.EventHandler(OnUiCommandItemClick);
						m_SystemMenuItem.MenuItems.Add(mi);

						this.toolBarButtonHide.CommandId = Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationHideCommandId;
						this.toolBarButtonHide.Paint += new PaintEventHandler(toolbarDrawHelper.Paint);
					}
					else
					{
						this.toolBarButtonHide.Visible = false;
					}

					mi = new Cerebrum.Windows.Forms.DesktopMenuItem();
					mi.Text = "-";
					m_SystemMenuItem.MenuItems.Add(mi);

					mi = new Cerebrum.Windows.Forms.DesktopMenuItem();
					mi.Text = resources.GetString("ApplicationExit.Caption");
					mi.CommandId = Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationExitCommandId;
					mi.Click += new System.EventHandler(OnUiCommandItemClick);
					m_SystemMenuItem.MenuItems.Add(mi);

					/*--
					mi = new Cerebrum.Windows.Forms.DesktopMenuItem();
					mi.Text = resources.GetString("ApplicationLogOut.Caption");
					mi.CommandId = Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationLogOutCommandId;
					mi.Click += new System.EventHandler(OnUiCommandItemClick);
					m_SecurityMenuItem.MenuItems.Add(mi);

					mi = new Cerebrum.Windows.Forms.DesktopMenuItem();
					mi.Text = resources.GetString("ApplicationChangePassword.Caption");
					mi.CommandId = Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationChangePasswordCommandId;
					mi.Click += new System.EventHandler(OnUiCommandItemClick);
					m_SecurityMenuItem.MenuItems.Add(mi);
					--*/

#warning "m_SecurityMenuItem.Visible = false"
					m_SecurityMenuItem.Visible = false;
					//			{
					//				object o = m_DomainContext.GetSystemVariable("system", "help");
					//				if(o!=null)
					//				{
					//					mi = new Cerebrum.Windows.Forms.DesktopMenuItem();
					//					mi.Text = resources.GetString("ApplicationHelp.Caption");
					//					mi.CommandId = "Cerebrum.Windows.Forms.Application.Help";
					//					mi.Click += new System.EventHandler(OnUiCommandItemClick);
					//					mi.MergeOrder = 100;
					//					m_HelpMenuItem.MenuItems.Add(0, mi);
					//				}
					//			}
					//				Cerebrum.Integrator.SystemDatabase.GroupsRow rwg = m_DomainContext.SystemDatabase.Groups.FindByGroupId(groupId);
					//				if(rwg!=null && !rwg.IsDescriptionNull())
					//				{
					//					this.Text = rwg.Description;
					//				}
					//
					mi = new Cerebrum.Windows.Forms.DesktopMenuItem();
					mi.Text = resources.GetString("ApplicationHelp.AboutProgram");
					mi.CommandId = Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationHelpAboutCommandId;
					mi.Click += new System.EventHandler(OnUiCommandItemClick);
					mi.MergeOrder = 100;
					m_HelpMenuItem.MenuItems.Add(mi);

					BuildHelp();

				}
				catch(System.Exception ex)
				{
					m_Application.ProcessException(ex, "PrimaryWindow.BuildUi.Band");
				}
				finally
				{
					if(connectorInitMsg!=null)
					{
						connectorInitMsg.Dispose();
						connectorInitMsg = null;
					}
				}
			}
			catch(System.Exception ex)
			{
				m_Application.ProcessException(ex, "PrimaryWindow.BuildUi");
			}
			finally
			{
				try
				{
					if(this.Ready!=null)
					{
						this.Ready(this, System.EventArgs.Empty);
					}
				}
				catch(System.Exception ex)
				{
					m_Application.ProcessException(ex, "PrimaryWindow.BuildUi.Ready");
				}
			}

		}

		public event System.EventHandler Ready;

		public void AddBandItem(Cerebrum.Windows.Forms.IUiControl control)
		{
			int MergeOrder;
			MergeOrder = control.MergeOrder;

			Cerebrum.Windows.Forms.IUiControl after = null;

			foreach(System.Windows.Forms.Control ci in this.toolBar1.Controls)
			{
				Cerebrum.Windows.Forms.IUiControl iui = ci as Cerebrum.Windows.Forms.IUiControl;
				if(iui!=null)
				{
					if(iui.MergeOrder > MergeOrder) 
						if(after==null || after.MergeOrder > iui.MergeOrder)
							after = iui;
				}
			}

			this.toolBar1.Controls.Add((System.Windows.Forms.Control)control);
			if(after!=null && MergeOrder>=0)
			{
				int i = this.toolBar1.Controls.IndexOf((System.Windows.Forms.Control)after);
				this.toolBar1.Controls.SetChildIndex((System.Windows.Forms.Control)control, i);
			}
		}

		protected System.Collections.Hashtable m_MenuItemsHashtable;

		/// <summary>
		/// ����������� ������� ��� ���������� ������ ����
		/// </summary>
		/// <param name="MenuId">��� ������ ������ ����</param>
		/// <returns>������� ���� �������������� ����������� ������</returns>
		public Cerebrum.Windows.Forms.DesktopMenuItem MakeMenuItem(Cerebrum.ObjectHandle MenuId)
		{
			if(m_MenuItemsHashtable.ContainsKey(MenuId))
			{
				return (Cerebrum.Windows.Forms.DesktopMenuItem)m_MenuItemsHashtable[MenuId];
			}

			Cerebrum.Windows.Forms.Reflection.UiMenuItemDescriptor rw;
			using(Cerebrum.IComposite cmi = this.DomainContext.AttachConnector(MenuId))
			{
				rw = cmi.Component as Cerebrum.Windows.Forms.Reflection.UiMenuItemDescriptor;
				if(rw!=null)
				{
					bool ownerDraw;

					System.Windows.Forms.Menu.MenuItemCollection mc;
					Cerebrum.ObjectHandle ParentId = rw.ParentId;
					if(ParentId!=Cerebrum.ObjectHandle.Null)
					{
						mc = MakeMenuItem(rw.ParentId).MenuItems;
						ownerDraw = true;
					}
					else
					{
						mc = m_MainMenu.MenuItems;
						ownerDraw = false;
					}

					int MergeOrder = -1;

					//--if(!rw.IsMergeOrderNull())
					MergeOrder = rw.MergeOrder;

					System.Windows.Forms.MenuItem after = null;

					foreach(System.Windows.Forms.MenuItem mi in mc)
					{
						Cerebrum.Windows.Forms.DesktopMenuItem imi = mi as Cerebrum.Windows.Forms.DesktopMenuItem;
						if(imi!=null)
						{
							if(imi.MenuItemId==cmi.PlasmaHandle) 
								return imi;
							if(imi.MergeOrder > MergeOrder)
								if(after==null || after.MergeOrder > imi.MergeOrder)
									after = imi;
						}
					}

					Cerebrum.Windows.Forms.DesktopMenuItem m1;
					m1 = new Cerebrum.Windows.Forms.DesktopMenuItem();
					m1.MenuItemId=cmi.PlasmaHandle;
					m1.OwnerDraw = ownerDraw;

					string displayName = rw.DisplayName;
					if(displayName!=null && displayName.Length>0)
					{
						m1.Text = displayName;
					}
					else
					{
						m1.Text = "-";
					}

					if(MergeOrder>=0)
						m1.MergeOrder = MergeOrder;

					if(after==null || MergeOrder<0)
					{
						mc.Add(m1);
					}
					else
					{
						int i = mc.IndexOf(after);
						mc.Add(i, m1);
					}
				
					m_MenuItemsHashtable.Add(m1.MenuItemId, m1);
					return m1;
				}
			}
			return null;
		}

		/// <summary>
		/// ���������� ������� �� ������ ������ ������������ � ��������� ����
		/// </summary>
		/// <param name="sender">�������� �������(������ ������)</param>
		/// <param name="e">��������� �������</param>
		private void OnUiCommandItemClick(object sender, System.EventArgs e)
		{
			try
			{
				Cerebrum.Windows.Forms.IUiControl item = (sender as Cerebrum.Windows.Forms.IUiControl);
				if(item!=null)
				{
					ExecCmd(item.CommandId, e);
				}
			}
			catch(System.Exception ex)
			{
				m_Application.ProcessException(ex, "PrimaryWindow.OnUiCommandItemClick");
			}

		}
		/// <summary>
		/// ������� �������������� ������� �� �������� ����������
		/// </summary>
		/// <param name="ComponentId">����� ����������, �������� ���������� �������</param>
		public void ExecCmd(Cerebrum.ObjectHandle commandId, System.EventArgs e)
		{

			if(commandId==Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationExitCommandId)
			{
				m_Application.AppListener.SuspendReady();
				bool cancel = false;
				cancel = this.Release(cancel);
				if(!cancel)
				{
					cancel = m_Application.Release(cancel);
				}
				m_Application.AppListener.ResumeReady();
				if(!cancel)
				{
					m_Application.Exit();
				}
				return;
			}
			else if(commandId==Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationHideCommandId)
			{
				m_Application.Hide();
				return;
			}
			else if(commandId==Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationHelpCommandId)
			{
				//object o;
				//					o = m_DomainContext.GetSystemVariable("system", "help");
				//					if(o!=null)
				//					{
				//						try
				//						{
				//							System.Windows.Forms.Help.ShowHelpIndex(this, o.ToString()); //m_DomainContext.PrimaryWindow.HelpProvider.HelpNamespace);
				//						}
				//						catch(System.Exception ex)
				//						{
				//							m_Application.AddActivityRow(ex, m_ComponentId + ".Execute(" + commandId + ")");
				//						}
				//					}
				return;
			}
			else if(commandId==Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationHelpAboutCommandId)
			{
				AboutCerebrum dlg1 = new AboutCerebrum();
				dlg1.ShowDialog();
				return;
			}
			/*--
			else if(commandId==Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationLogOutCommandId)
			{
				m_Application.LogOut();
				return;
			}
			else if(commandId==Cerebrum.Windows.Forms.Specialized.Concepts.ApplicationChangePasswordCommandId)
			{
				m_Application.ChangePassword();
				return;
			}
			--*/

			/*Cerebrum.Integrator.UiCommand cmd;
						  cmd = (Cerebrum.Integrator.UiCommand)m_UiCommands[commandId];
						  if(cmd!=null)
						  {
							  cmd.FireExecute(new Cerebrum.Integrator.InvokeCommandEventArgs());
						  }*/
		}
		/// <summary>
		/// �������� ������ �� ���������� ��������� MDI Child
		/// ������������ ��� ������ ��� ������� DeactivatedAsMdiChild
		/// </summary>
		protected object m_LastActiveComponent;
				
		public object ActiveComponent
		{
			get
			{
				return m_LastActiveComponent;
			}
			set
			{
				try
				{
					if(m_LastActiveComponent!=value)
					{
						if(m_LastActiveComponent!=null)
						{
							try
							{
								if(m_LastActiveComponent is Cerebrum.IDisposable)
								{
									(m_LastActiveComponent as Cerebrum.IDisposable).Disposing -= new EventHandler(LastActiveComponent_Disposing);
								}
								//m_LastActiveComponent.OnComponentDeactivated();
							}
							catch(System.Exception ex)
							{
								m_Application.ProcessException(ex, "PrimaryWindow.DectivateComponent");
							}
							finally
							{
								m_LastActiveComponent = null;
							}
						}
				
						m_LastActiveComponent = value;
				
						if(m_LastActiveComponent is Cerebrum.IDisposable)
						{
							(m_LastActiveComponent as Cerebrum.IDisposable).Disposing += new EventHandler(LastActiveComponent_Disposing);
						}
				
						UpdateComponentUI(m_LastActiveComponent);
				
						if(m_LastActiveComponent!=null)
						{
							//m_LastActiveComponent.OnComponentActivated();
						}
					}
				}
				catch(System.Exception ex)
				{
					m_Application.ProcessException(ex, "PrimaryWindow.ActivateComponent");
				}		
			}
		}
				
		private void UpdateComponentUI(object component)
		{
				
			if(m_Destroying || m_Application==null) return;
				
			//			string groupId = m_Application.GroupId;
			//
			//
			//			foreach(Cerebrum.Integrator.SystemDatabase.UiCommandsRow rwc in m_DomainContext.SystemDatabase.UiCommands)
			//			{
			//				if(rwc.DynamicEvent)
			//				{
			//					Cerebrum.Integrator.SystemDatabase.UiComponentsRow rwi = null;
			//					if(m_LastActiveComponent!=null)
			//						rwi = m_DomainContext.SystemDatabase.UiComponents.FindByGroupIdComponentIdCommandId(groupId, m_LastActiveComponent.ComponentId, rwc.CommandId);
			//
			//					Cerebrum.Integrator.UiCommand ui = GetCommand(rwc.CommandId);
			//					if(ui!=null) ui.Enabled = (rwi!=null);
			//				}
			//			}		
			foreach(Cerebrum.Windows.Forms.ComponentModel.UiService svc in m_UiServices)
			{
				try
				{
					svc.ComponentActivated(component);
				}
				catch(System.Exception ex)
				{
					m_Application.ProcessException(ex, "PrimaryWindow.UpdateComponentUI");
				}
			}
		}
		private void LastActiveComponent_Disposing(object sender, EventArgs e)
		{
			if(sender!=null)
			{
				if(sender is Cerebrum.IDisposable)
				{
					(sender as Cerebrum.IDisposable).Disposing -= new EventHandler(LastActiveComponent_Disposing);
				}
				if(sender == m_LastActiveComponent)
				{
					m_LastActiveComponent = null;
				
					UpdateComponentUI(null);
				}
			}		
		}

		protected Cerebrum.DesktopEngine.Application m_Application = null;
		public Cerebrum.DesktopEngine.Application Application
		{
			get
			{
				return m_Application;
			}
			set
			{
				m_Application = value;
			}
		}

		/// <summary>
		/// �������, ���������� Windows.Forms ��� ��������� ��������� MdiChild
		/// ������������ ��� ���������� �������� ���� �� ��������� ���������
		/// �� ����������. ��� ��������� ���������� �������� ���� ������ ����
		/// ����������� ������ Cerebrum.Integrator.MdiFormComponent
		/// </summary>
		/// <param name="e">��������� �������</param>
		protected override void OnMdiChildActivate(System.EventArgs e)
		{

			base.OnMdiChildActivate(e);

			this.ActiveComponent = this.ActiveMdiChild;

		}

		/// <summary>
		/// ������ ��� ������� ������������ ��� ��������� ������� ������.
		/// � �������� ����� ���� ������������ ��� ������� �������
		/// </summary>
		protected System.Collections.Hashtable m_HotKeyUiControls;
		/// <summary>
		/// ������������ ������� ������ �� ���� MDI Child
		/// </summary>
		/// <param name="m">��������� � ����� ������� �������</param>
		/// <returns>true ���� ������� ���� ����������</returns>
		protected bool PreviewKeyMessage(ref System.Windows.Forms.Message m)
		{
			try
			{
				if(m.Msg == 0x00000104 || //WM_SYSKEYDOWN
					m.Msg == 0x00000100) //WM_KEYDOWN
				{
					string key = "";

					if(m.Msg == 0x00000104) //WM_SYSKEYDOWN
						key += "A";
					if( (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Control) > 0 )
						key += "C";
					if( (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Shift) > 0 )
						key += "S";
					key += string.Format("{0:X4}", m.WParam.ToInt32());
				
					if(m_HotKeyUiControls.ContainsKey(key))
					{
						System.ComponentModel.CancelEventArgs ce = new CancelEventArgs(false);
						Cerebrum.Windows.Forms.ComponentModel.UiHotKey ui = (Cerebrum.Windows.Forms.ComponentModel.UiHotKey)m_HotKeyUiControls[key];
						if(ui.Enabled)
						{
							ui.FireCommand(ce);
						}
						return ce.Cancel;
					}
				}
			}
			catch(System.Exception ex)
			{
				m_Application.ProcessException(ex, "PrimaryWindow.PreviewKeyMessage");
			}
			return false;

		}
		/// <summary>
		/// ������������ ������� ������ �� ���� MDI Container
		/// </summary>
		/// <param name="m">��������� � ����� ������� �������</param>
		/// <returns>true ���� ������� ���� ����������</returns>
		protected override bool ProcessKeyMessage(ref System.Windows.Forms.Message m)
		{
			if(PreviewKeyMessage(ref m)) return true;
			return base.ProcessKeyMessage(ref m);
		}
		/// <summary>
		/// ������������ ������� ������ �� ���� MDI Child � MDI Container
		/// </summary>
		/// <param name="m">��������� � ����� ������� �������</param>
		/// <returns>true ���� ������� ���� ����������</returns>
		protected override bool ProcessKeyPreview(ref System.Windows.Forms.Message m)
		{
			if(PreviewKeyMessage(ref m)) return true;
			return base.ProcessKeyPreview(ref m);
		}

		private void menuItemWindowArrangeAll_Click(object sender, System.EventArgs e)
		{
			this.mdiClient1.LayoutMdi(System.Windows.Forms.MdiLayout.ArrangeIcons);
		}

		private void menuItemWindowTileHorizontal_Click(object sender, System.EventArgs e)
		{
			this.mdiClient1.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal);
		}

		private void menuItemWindowTileVertical_Click(object sender, System.EventArgs e)
		{
			this.mdiClient1.LayoutMdi(System.Windows.Forms.MdiLayout.TileVertical);
		}

		private void menuItemWindowCascade_Click(object sender, System.EventArgs e)
		{
			this.mdiClient1.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade);
		}

		private class UiCommandsCollection : Cerebrum.Windows.Forms.IUiCommandsCollection
		{
			/// <summary>
			/// �������� ��������� ������� ������������ � ��������� ����������
			/// </summary>
			private PrimaryWindowForm m_PrimaryWindow;
			
			public UiCommandsCollection(PrimaryWindowForm primaryWindow)
			{
				m_PrimaryWindow = primaryWindow;
			}

			#region IUiCommandsCollection Members

			public IUiCommand this[int index]
			{
				get
				{
					return (IUiCommand)m_PrimaryWindow.m_UiCommands[index];
				}
			}

			/// <summary>
			/// ���������� ������� �� �� �����
			/// </summary>
			/// <param name="name">��� ��������</param>
			/// <returns>��������� ��������, ��� null</returns>
			public IUiCommand this[string name]
			{
				get
				{
					for(int i=0; i<this.m_PrimaryWindow.m_UiCommands.Count; i++)
					{
						Cerebrum.Windows.Forms.ComponentModel.UiCommand cmd = (Cerebrum.Windows.Forms.ComponentModel.UiCommand)this.m_PrimaryWindow.m_UiCommands[i];
						if(cmd.CommandName==name)
						{
							return cmd;
						}
					}

					/*
					for(int i=0; i<this.m_PrimaryWindow.m_UiCommands.Count; i++)
					{
						Cerebrum.Windows.Forms.ComponentModel.UiCommand cmd = (Cerebrum.Windows.Forms.ComponentModel.UiCommand)this.m_PrimaryWindow.m_UiCommands[i];
						if(cmd.MessageName==name)
						{
							return cmd;
						}
					}
					*/

					return null;
				}
			}

			public int Count
			{
				get
				{
					return m_PrimaryWindow.m_UiCommands.Count;
				}
			}

			#endregion

			#region IEnumerable Members

			public IEnumerator GetEnumerator()
			{
				return m_PrimaryWindow.m_UiCommands.GetEnumerator();
			}

			#endregion
		}
	}
}

		/*		protected bool m_ShiftDown;
				protected bool m_CtrlDown;
				protected bool m_AltDown;
				m_ShiftDown = false;
				m_CtrlDown = false;
				m_AltDown = false;
				*/
		/*switch(m.WParam.ToInt32())
				{
					case 16:
						m_ShiftDown = true;
						break;
					case 17:
						m_CtrlDown = true;
						break;
					case 18:
						m_AltDown = true;
						break;
					default:
						break;
				} */
		/*else if(m.Msg == 0x00000101) //WM_KEYUP
			{
				switch(m.WParam.ToInt32())
				{
					case 16:
						m_ShiftDown = false;
						break;
					case 17:
						m_CtrlDown = false;
						break;
					case 18:
						m_AltDown = false;
						break;
				} 
			}*/
