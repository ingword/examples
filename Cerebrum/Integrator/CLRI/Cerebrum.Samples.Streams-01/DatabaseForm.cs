// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Cerebrum.Samples.Streams01
{
	/// <summary>
	/// Summary description for DatabaseForm.
	/// </summary>
	public class DatabaseForm : /*System.Windows.Forms.Form //*/ Cerebrum.Windows.Forms.DesktopComponent 
	{
		private System.Windows.Forms.Button btnEmrg;
		private System.Windows.Forms.Button btnPopulateImages;
		private System.Windows.Forms.ListBox lstImages;
		private System.Windows.Forms.PictureBox picImage;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.Button btnBeginTrans;
		private System.Windows.Forms.Button btnCommitTrans;
		private System.Windows.Forms.Button btnRollbackTrans;
		private System.Windows.Forms.CheckBox chkSnapshot;
		private System.Windows.Forms.Button btnUndo;
		private System.Windows.Forms.Button btnRedo;
		private System.Windows.Forms.GroupBox grpTransactions;
		private System.Windows.Forms.Label lblFound;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public DatabaseForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnEmrg = new System.Windows.Forms.Button();
			this.btnPopulateImages = new System.Windows.Forms.Button();
			this.lstImages = new System.Windows.Forms.ListBox();
			this.picImage = new System.Windows.Forms.PictureBox();
			this.lblMessage = new System.Windows.Forms.Label();
			this.grpTransactions = new System.Windows.Forms.GroupBox();
			this.chkSnapshot = new System.Windows.Forms.CheckBox();
			this.btnRollbackTrans = new System.Windows.Forms.Button();
			this.btnCommitTrans = new System.Windows.Forms.Button();
			this.btnBeginTrans = new System.Windows.Forms.Button();
			this.btnUndo = new System.Windows.Forms.Button();
			this.btnRedo = new System.Windows.Forms.Button();
			this.lblFound = new System.Windows.Forms.Label();
			this.grpTransactions.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnEmrg
			// 
			this.btnEmrg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEmrg.BackColor = System.Drawing.Color.Turquoise;
			this.btnEmrg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEmrg.Location = new System.Drawing.Point(320, 8);
			this.btnEmrg.Name = "btnEmrg";
			this.btnEmrg.TabIndex = 1;
			this.btnEmrg.Text = "Emrg";
			this.btnEmrg.Visible = false;
			this.btnEmrg.Click += new System.EventHandler(this.btnEmrg_Click);
			// 
			// btnPopulateImages
			// 
			this.btnPopulateImages.BackColor = System.Drawing.Color.Turquoise;
			this.btnPopulateImages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPopulateImages.Location = new System.Drawing.Point(16, 8);
			this.btnPopulateImages.Name = "btnPopulateImages";
			this.btnPopulateImages.Size = new System.Drawing.Size(168, 23);
			this.btnPopulateImages.TabIndex = 2;
			this.btnPopulateImages.Text = "Populate Images";
			this.btnPopulateImages.Click += new System.EventHandler(this.btnPopulateImages_Click);
			// 
			// lstImages
			// 
			this.lstImages.Location = new System.Drawing.Point(16, 40);
			this.lstImages.Name = "lstImages";
			this.lstImages.Size = new System.Drawing.Size(168, 199);
			this.lstImages.TabIndex = 3;
			this.lstImages.SelectedIndexChanged += new System.EventHandler(this.lstImages_SelectedIndexChanged);
			// 
			// picImage
			// 
			this.picImage.BackColor = System.Drawing.Color.MediumAquamarine;
			this.picImage.Location = new System.Drawing.Point(200, 48);
			this.picImage.Name = "picImage";
			this.picImage.Size = new System.Drawing.Size(200, 200);
			this.picImage.TabIndex = 4;
			this.picImage.TabStop = false;
			// 
			// lblMessage
			// 
			this.lblMessage.AutoSize = true;
			this.lblMessage.Location = new System.Drawing.Point(200, 8);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(0, 16);
			this.lblMessage.TabIndex = 5;
			// 
			// grpTransactions
			// 
			this.grpTransactions.Controls.Add(this.chkSnapshot);
			this.grpTransactions.Controls.Add(this.btnRollbackTrans);
			this.grpTransactions.Controls.Add(this.btnCommitTrans);
			this.grpTransactions.Controls.Add(this.btnBeginTrans);
			this.grpTransactions.Controls.Add(this.btnUndo);
			this.grpTransactions.Controls.Add(this.btnRedo);
			this.grpTransactions.Location = new System.Drawing.Point(16, 264);
			this.grpTransactions.Name = "grpTransactions";
			this.grpTransactions.Size = new System.Drawing.Size(384, 72);
			this.grpTransactions.TabIndex = 6;
			this.grpTransactions.TabStop = false;
			this.grpTransactions.Text = "Transactions";
			// 
			// chkSnapshot
			// 
			this.chkSnapshot.Checked = true;
			this.chkSnapshot.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkSnapshot.Location = new System.Drawing.Point(8, 16);
			this.chkSnapshot.Name = "chkSnapshot";
			this.chkSnapshot.TabIndex = 3;
			this.chkSnapshot.Text = "Snapshot";
			// 
			// btnRollbackTrans
			// 
			this.btnRollbackTrans.BackColor = System.Drawing.Color.Turquoise;
			this.btnRollbackTrans.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRollbackTrans.Location = new System.Drawing.Point(168, 40);
			this.btnRollbackTrans.Name = "btnRollbackTrans";
			this.btnRollbackTrans.TabIndex = 2;
			this.btnRollbackTrans.Text = "Rollback";
			this.btnRollbackTrans.Click += new System.EventHandler(this.btnRollbackTrans_Click);
			// 
			// btnCommitTrans
			// 
			this.btnCommitTrans.BackColor = System.Drawing.Color.Turquoise;
			this.btnCommitTrans.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCommitTrans.Location = new System.Drawing.Point(88, 40);
			this.btnCommitTrans.Name = "btnCommitTrans";
			this.btnCommitTrans.TabIndex = 1;
			this.btnCommitTrans.Text = "Commit";
			this.btnCommitTrans.Click += new System.EventHandler(this.btnCommitTrans_Click);
			// 
			// btnBeginTrans
			// 
			this.btnBeginTrans.BackColor = System.Drawing.Color.Turquoise;
			this.btnBeginTrans.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBeginTrans.Location = new System.Drawing.Point(8, 40);
			this.btnBeginTrans.Name = "btnBeginTrans";
			this.btnBeginTrans.TabIndex = 0;
			this.btnBeginTrans.Text = "Begin";
			this.btnBeginTrans.Click += new System.EventHandler(this.btnBeginTrans_Click);
			// 
			// btnUndo
			// 
			this.btnUndo.BackColor = System.Drawing.Color.Turquoise;
			this.btnUndo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnUndo.Location = new System.Drawing.Point(255, 40);
			this.btnUndo.Name = "btnUndo";
			this.btnUndo.Size = new System.Drawing.Size(60, 23);
			this.btnUndo.TabIndex = 2;
			this.btnUndo.Text = "< Undo";
			this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
			// 
			// btnRedo
			// 
			this.btnRedo.BackColor = System.Drawing.Color.Turquoise;
			this.btnRedo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRedo.Location = new System.Drawing.Point(320, 40);
			this.btnRedo.Name = "btnRedo";
			this.btnRedo.Size = new System.Drawing.Size(60, 23);
			this.btnRedo.TabIndex = 2;
			this.btnRedo.Text = "Redo >";
			this.btnRedo.Click += new System.EventHandler(this.btnRedo_Click);
			// 
			// lblFound
			// 
			this.lblFound.AutoSize = true;
			this.lblFound.Location = new System.Drawing.Point(16, 240);
			this.lblFound.Name = "lblFound";
			this.lblFound.Size = new System.Drawing.Size(39, 16);
			this.lblFound.TabIndex = 7;
			this.lblFound.Text = "Found:";
			// 
			// DatabaseForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.Aquamarine;
			this.ClientSize = new System.Drawing.Size(408, 341);
			this.Controls.Add(this.lblFound);
			this.Controls.Add(this.grpTransactions);
			this.Controls.Add(this.lblMessage);
			this.Controls.Add(this.picImage);
			this.Controls.Add(this.lstImages);
			this.Controls.Add(this.btnPopulateImages);
			this.Controls.Add(this.btnEmrg);
			this.Name = "DatabaseForm";
			this.Text = "DatabaseForm";
			this.Closed += new System.EventHandler(this.DatabaseForm_Closed);
			this.grpTransactions.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnEmrg_Click(object sender, System.EventArgs e)
		{
//			Cerebrum.Windows.Forms.EmergencyConfigurator dlg1 = new Cerebrum.Windows.Forms.EmergencyConfigurator();
//			((Cerebrum.IComponent)dlg1).SetConnector(Cerebrum.SerializeDirection.Init, new Cerebrum.Runtime.TemporaryConnector(this.DomainContext.DomainConnector));
//			dlg1.MdiParent = Cerebrum.Windows.Forms.Application.Instance.PrimaryWindow;
//			dlg1.Show();
		}

		protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
		{
			base.SetConnector (direction, connector);
			this.DomainContext.Disposing += new EventHandler(DomainContext_Disposing);
		}

		private void DatabaseForm_Closed(object sender, System.EventArgs e)
		{
			if(this.m_Connector!=null)
			{
				this.DomainContext.Shutdown();
			}
		}

		public void PreInit()
		{
			// ���� ������ RootVectorHandle ��� ������, �� AttachConnector ������ ��� ���������.
			// ����� ������� ������ KernelObject
			Cerebrum.IConnector iv = this.DomainContext.AttachConnector(RootVectorHandle);
			if(iv==null)
			{
				using(Cerebrum.Runtime.NativeSector sector = this.DomainContext.GetSector())
				{
					sector.Newobj(RootVectorHandle, Cerebrum.Runtime.KernelObjectKindId.Warden);
					iv = sector.AttachConnector(RootVectorHandle);
				}
			}
			//cleanup
			iv.Dispose();
			InitList();
		}

		/*// ����� ������� ���� ������ �� �������� �� ������ ����� ������ �������� � ���������� �� ������
		private void lstImages_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// �������� ������� ��������� � ������ ����
			Cerebrum.Windows.Forms.DropDownStringItem item = lstImages.SelectedItem as Cerebrum.Windows.Forms.DropDownStringItem;
			// �������� ID �����
			Cerebrum.ObjectHandle objid = (Cerebrum.ObjectHandle)item.Value;
			// �������� ������ - ������ ��
			using(Cerebrum.Runtime.NativeSector sector = this.DomainContext.GetSector())
			{
				// �������� ����� �� ��� ID
				using(System.IO.Stream stm = sector.AttachStream(objid))
				{
					// ��������� �������� - � ��� ��� ���� �����, ������ ����� ������� �� ��������� ������� � .NET
					using(System.Drawing.Image img = System.Drawing.Bitmap.FromStream(stm))
					{
						picImage.Image = new System.Drawing.Bitmap(img);
					}
				}
			}
		}*/

		// ����� ������� ���� ������ �� �������� �� ������ ����� ������ �������� � ���������� �� ������
		private void lstImages_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// �������� ������� ��������� � ������ ����
			Cerebrum.Windows.Forms.DropDownStringItem item = lstImages.SelectedItem as Cerebrum.Windows.Forms.DropDownStringItem;
			// �������� ID �����
			Cerebrum.ObjectHandle objid = (Cerebrum.ObjectHandle)item.Value;
				// �������� ����� �� ��� ID
			using(System.IO.Stream stm = this.DomainContext.AttachConnector(objid) as System.IO.Stream)
			{
				// ��������� �������� - � ��� ��� ���� �����, ������ ����� ������� �� ��������� ������� � .NET
				using(System.Drawing.Image img = System.Drawing.Bitmap.FromStream(stm))
				{
					picImage.Image = new System.Drawing.Bitmap(img);
				}
			}
		}

		int ImageIndex = 1;
/// <summary>
/// �������� ���������� �� �������� � ����������
/// </summary>
		private void btnPopulateImages_Click(object sender, System.EventArgs e)
		{
			System.DateTime dtStart = System.DateTime.Now;
			long len = 0;
			// �������� ������ - ������ ��
			using(Cerebrum.Runtime.NativeSector sector = this.DomainContext.GetSector())
			{
				// � �������� �� ������� ��� ������ - ������ � ��������� ������� �� ������� � ����� ����� ���������.
				// ��� ����� ��� ���� ���� ����� ����������� ����� ������� ����� �������� ����������. 
				// ���� ����� ����� ������� �� ID - ������ � ���� ������
				//
				// �������� ��������� �� ��������� ��� ������ ������
					// �������� ��������� �� ������������ ������ ��� ������
				using(Cerebrum.Runtime.NativeWarden vec = sector.AttachConnector(RootVectorHandle) as Cerebrum.Runtime.NativeWarden)
				{
					// ���� �� 100 ���������� - ������� 100 ��������
					for(int i = 0 ; i< 100 ; i++)
					{
						// ������� ����� ID ��� ������ �������.
						Cerebrum.ObjectHandle objid = sector.NextSequence();
						// ������� ����� ����� � ������� �� (� ������������ �������)
						sector.Newobj(objid, Cerebrum.Runtime.KernelObjectKindId.Stream);
						// ����� ���� ��� ����� ������ �� ����� �������� ��� .NET �������� �� ID
						using(System.IO.Stream stm = sector.AttachStream(objid))
						{
							// ������� �������� ��� ����� � ������� �������� � ���
							using(System.Drawing.Bitmap bmp = CreateImage(100, 100, ImageIndex.ToString().PadLeft(3, '0')))
							{
								// ���������
								bmp.Save(stm, System.Drawing.Imaging.ImageFormat.Png);
							}
							len += stm.Length;
							stm.Close();
						}
						// ��� ���� ����� �� �������� ID ��������� �������� - ��������� ���� ID � ���������
						vec.SetMap(objid, Cerebrum.Runtime.MapAccess.Scalar, new Cerebrum.ObjectHandle(1));
						ImageIndex ++;
					}
				}
			}
			System.DateTime dtFinish = System.DateTime.Now;
			System.TimeSpan tmDiff = dtFinish - dtStart;
			InitList();
			lblMessage.Text = tmDiff.ToString() + " " + (len/100).ToString();
		}

		// �� ���������� � �� ������ ������� �������������� ������ �������� ������� ������������
		private void InitList()
		{
			grpTransactions.Text = "Transactions " + (this.m_Connector.Workspace.Savepoints.Current + 1).ToString() + "/" + this.m_Connector.Workspace.Savepoints.Count.ToString();

			lstImages.Items.Clear();
			using(Cerebrum.Runtime.NativeWarden vec = this.DomainContext.AttachConnector(RootVectorHandle) as Cerebrum.Runtime.NativeWarden)
			{
				if(vec!=null)
				{
#warning " pending refactoring "
					foreach(System.Collections.DictionaryEntry de in vec)
					{
						lstImages.Items.Add(new Cerebrum.Windows.Forms.DropDownStringItem(de.Key.ToString(), de.Key));
					}
				}
			}

			lblFound.Text = "Found: " + lstImages.Items.Count;
		}

		private System.Drawing.Bitmap CreateImage(int width, int height, string text)
		{
			System.Drawing.Bitmap bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			System.Drawing.Graphics g = Graphics.FromImage(bmp);
			g.Clear(System.Drawing.Color.SeaGreen);
			System.Drawing.Pen pen = new Pen(System.Drawing.Color.BlueViolet, 3);
			g.DrawEllipse(pen, 3, 3, width - 6, height - 6);
			g.DrawString(text, new System.Drawing.Font("Verdana", 12, System.Drawing.FontStyle.Bold), new System.Drawing.SolidBrush(System.Drawing.Color.White), 0, 0);
			g.Dispose();
			return bmp;
		}

		/// <summary>
		/// ��������� �������� ��� ������ �������� - �������.
		/// </summary>
		private readonly Cerebrum.ObjectHandle RootVectorHandle = new Cerebrum.ObjectHandle(60000);

		private void btnBeginTrans_Click(object sender, System.EventArgs e)
		{
			this.DomainContext.BeginTransaction(chkSnapshot.Checked);

			InitList();
		}

		private void btnCommitTrans_Click(object sender, System.EventArgs e)
		{
			this.DomainContext.CommitTransaction();

			InitList();
		}

		private void btnRollbackTrans_Click(object sender, System.EventArgs e)
		{
			this.DomainContext.RollbackTransaction();

			InitList();
		}

		private void btnUndo_Click(object sender, System.EventArgs e)
		{
			if(this.DomainContext.Workspace.Savepoints.Current > 0)
			{
				this.DomainContext.Workspace.Savepoints.Select(this.DomainContext.Workspace.Savepoints.Current - 1);
			}

			InitList();
		}

		private void btnRedo_Click(object sender, System.EventArgs e)
		{
			int t = this.DomainContext.Workspace.Savepoints.Current + 1;
			if(t < this.DomainContext.Workspace.Savepoints.Count)
			{
				this.DomainContext.Workspace.Savepoints.Select(t);
			}

		
			InitList();
		}

		private void DomainContext_Disposing(object sender, EventArgs e)
		{
			(sender as Cerebrum.IDisposable).Disposing -= new EventHandler(DomainContext_Disposing);
			this.m_Connector = null;
		}
	}
}
