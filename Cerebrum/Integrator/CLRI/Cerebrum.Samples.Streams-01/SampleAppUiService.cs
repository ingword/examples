// Copyright (C) Dmitry Shuklin 2005-2009. All rights reserved.

using System;

namespace Cerebrum.Samples.Streams01
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class SampleAppUiService : Cerebrum.Windows.Forms.Reflection.UiServiceDescriptor 
	{
		public SampleAppUiService()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public override bool CommandActivated(Cerebrum.IConnector message, object sender, object argument)
		{
			string name = ((Cerebrum.Reflection.MessageDescriptor)message.Component).Name;

			switch(name)
			{
				case "New":
				{
					CreateDocument();
					return true;
				}
				case "Open":
				{
					Cerebrum.Integrator.DomainContext c = null;
					if(argument is Cerebrum.Integrator.DomainContextEventArgs)
					{
						c = (argument as Cerebrum.Integrator.DomainContextEventArgs).DomainContext;
					}
					else
					{
						System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
						dlg.DefaultExt = "vnn";
						dlg.Filter = "Cerebrum Database files|*.vnn|All files|*.*";
						if(dlg.ShowDialog(Cerebrum.Windows.Forms.Application.Instance.PrimaryWindow)==System.Windows.Forms.DialogResult.OK)
						{
							int mode = 0;
							if(dlg.FilterIndex==1)
							{
								mode = 1;
							}
							/*if(dlg.FilterIndex==2)
							{
								mode = 2;
							}*/
							if(mode==0)
							{
								string ext = System.IO.Path.GetExtension(dlg.FileName).ToLower();
								if(ext==".vnn")
								{
									mode = 1;
								}
								/*if(ext==".vnn")
								{
									mode = 2;
								}*/
							}
							/*if(mode==1)
							{
								OpenSource(dlg.FileName);
							}*/
							if(mode==1)
							{
								Cerebrum.Windows.Forms.Application application = Cerebrum.Windows.Forms.Application.Instance;
								Cerebrum.Integrator.IContextFactory lcs = new Cerebrum.Management.SimpleContextFactory(application, dlg.FileName, null, 4, true, "", false);
								Cerebrum.IWorkspace domain = lcs.LoadContext(dlg.FileName, false);
								c = domain.Component as Cerebrum.Integrator.DomainContext;
								application.AddDomain(c);
							}
						}
					}
					if(c!=null)
					{
						Cerebrum.Samples.Streams01.DatabaseForm db = OpenDocument(c);
						db.PreInit();
					}
					return true;
				}

			}
			return base.CommandActivated(message, sender, argument);
		}

		/*public override void ComponentActivated(object component)
		{
			base.ComponentActivated (component);

			Cerebrum.Windows.Forms.IUiControl cmdCompile = Cerebrum.Windows.Forms.Application.Instance.PrimaryWindow.GetCommand("Compile");
			Cerebrum.Windows.Forms.IUiControl cmdCalculate = Cerebrum.Windows.Forms.Application.Instance.PrimaryWindow.GetCommand("Calculate");

			if(component is SourceEditor)
			{
				cmdCompile.Enabled = true;
				cmdCalculate.Enabled = false;
			}
			else if(component is ElexDatabase)
			{
				cmdCompile.Enabled = false;
				cmdCalculate.Enabled = true;
			}
			else
			{
				cmdCompile.Enabled = false;
				cmdCalculate.Enabled = false;
			}
		}*/

		public static Cerebrum.Samples.Streams01.DatabaseForm CreateDocument()
		{
			Cerebrum.Windows.Forms.Application application = Cerebrum.Windows.Forms.Application.Instance;
			Cerebrum.Windows.Forms.CreateDatabase dlg = new Cerebrum.Windows.Forms.CreateDatabase(Cerebrum.Windows.Forms.CreateDatabaseOptions.ClusterSize | Cerebrum.Windows.Forms.CreateDatabaseOptions.Versioning);
			if(dlg.ShowDialog(application.PrimaryWindow)==System.Windows.Forms.DialogResult.OK)
			{
				string bcfg = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(typeof(SampleAppUiService).Assembly.Location), @"Elex\Import\elexdb");
				
				Cerebrum.Integrator.IContextFactory lcs = new Cerebrum.Management.SimpleContextFactory(application, dlg.ClusterFileName, null, dlg.ClusterFrameSize, dlg.Versioning, bcfg, true);

				//lcs.DatabaseFileName = dlg.ClusterFileName;

				//lcs.InitContextDebug(c);
				Cerebrum.IWorkspace domain = lcs.InitContext(dlg.ClusterFileName, "Cerebrum.Samples.Streams01.Application");
				Cerebrum.Integrator.DomainContext c = domain.Component as Cerebrum.Integrator.DomainContext;
				application.AddDomain(c);

				lcs.ImportDatabase(c, true);
				
				/*if(System.IO.File.Exists(bcfg))
				{
					System.IO.FileStream fs = new System.IO.FileStream(bcfg, System.IO.FileMode.Open, System.IO.FileAccess.Read);
					System.IO.StreamReader sr = new System.IO.StreamReader(fs, System.Text.Encoding.Default);
					System.Xml.XmlTextReader xr = new System.Xml.XmlTextReader(sr);
					using(Cerebrum.Runtime.INativeDomain domain = c.DomainConnector.GetContainer() as Cerebrum.Runtime.INativeDomain)
					{
						Cerebrum.Management.Tools.ImportDatabase(domain, xr);
					}
					xr.Close();
					sr.Close();
					fs.Close();
				}*/

				Cerebrum.Samples.Streams01.DatabaseForm db = OpenDocument(c);
				db.PreInit();
				return db;
			}
			return null;
		}

		private static Cerebrum.Samples.Streams01.DatabaseForm OpenDocument(Cerebrum.Integrator.DomainContext c)
		{
			Cerebrum.Samples.Streams01.DatabaseForm dlg1 = new Cerebrum.Samples.Streams01.DatabaseForm();
			((Cerebrum.IComponent)dlg1).SetConnector(Cerebrum.SerializeDirection.Init, new Cerebrum.Runtime.SurrogateComposite(c.Workspace));
			dlg1.MdiParent = Cerebrum.Windows.Forms.Application.Instance.PrimaryWindow;
			dlg1.Show();
			return dlg1;
		}

	}
}
