// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble.Reflection
{
	/// <summary>
	/// Summary description for DomainDescriptor.
	/// </summary>
	public class DomainDescriptor : Cerebrum.Reflection.MemberDescriptor 
	{
		public DomainDescriptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string DatabaseFileName
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Ganglion.Ensemble.Specialized.Concepts.DatabaseFileNameAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Ganglion.Ensemble.Specialized.Concepts.DatabaseFileNameAttribute(this.DomainContext), value);
			}
		}

		public string ActivityFileName
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Ganglion.Ensemble.Specialized.Concepts.ActivityFileNameAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Ganglion.Ensemble.Specialized.Concepts.ActivityFileNameAttribute(this.DomainContext), value);
			}
		}

        public string TemplateFileName
		{
			get
			{
                return Convert.ToString(GetAttributeComponent(Cerebrum.Ganglion.Ensemble.Specialized.Concepts.TemplateFileNameAttribute(this.DomainContext)));
			}
			set
			{
                SetAttributeComponent(Cerebrum.Ganglion.Ensemble.Specialized.Concepts.TemplateFileNameAttribute(this.DomainContext), value);
			}
		}

        public bool ReadOnly
		{
			get
			{
                object v = GetAttributeComponent(Cerebrum.Specialized.Concepts.ReadOnlyAttribute);
                if (v is bool)
                {
                    return (bool)v;
                }
                return false;
			}
			set
			{
                SetAttributeComponent(Cerebrum.Specialized.Concepts.ReadOnlyAttribute, value);
			}
		}
	}
}
