// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble
{
	/// <summary>
	/// Summary description for SyncronizedComposite.
	/// </summary>
	public class SyncronizedComposite : System.MarshalByRefObject, Cerebrum.IComposite
	{
		private Cerebrum.IComposite m_Composite;
		public SyncronizedComposite(Cerebrum.IComposite composite)
		{
			m_Composite = composite;
		}
		#region IComposite Members

		public Cerebrum.ObjectHandle GroundHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.GroundHandle;
				}
			}
		}

		public Cerebrum.ObjectHandle LiquidHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.LiquidHandle;
				}
			}
		}

		public Cerebrum.ObjectHandle TypeIdHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.TypeIdHandle;
				}
			}
            set
            {
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					m_Composite.TypeIdHandle = value;
				}
            }
		}

		public Cerebrum.ObjectHandle KindIdHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.KindIdHandle;
				}
			}
		}

		public Cerebrum.IConnector GetPrecedent()
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				return new SyncronizedConnector(m_Composite.GetPrecedent());
			}
		}

		public Cerebrum.ObjectHandle PlasmaHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.PlasmaHandle;
				}
			}
		}

		#endregion

		#region IConnector Members

		public object Component
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.Component;
				}
			}
			set
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					m_Composite.Component = value;
				}
			}
		}

		public Cerebrum.IWorkspace Workspace
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return new SyncronizedWorkspace(m_Composite.Workspace);
				}
			}
		}

		#endregion

		#region IDisposable Members

		public event System.EventHandler Disposing
		{
			add
			{
			}
			remove
			{
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				m_Composite.Dispose();
			}
		}

		#endregion
	}
}
