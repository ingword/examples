// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>

	public class EnsembleController : System.MarshalByRefObject, Cerebrum.Ganglion.Remoting.IEnsembleController
	{
		public EnsembleController()
		{
		}

		#region IEnsembleController Members

		public void Initialize()
		{
			ControllerImplementation.Instance.Initialize();
		}

		public void Shutdown(bool forceShutdown)
		{
			try
			{
				ControllerImplementation instance = ControllerImplementation.m_Instance;
				if (instance != null)
				{
					instance.Dispose();
				}
			}
			finally
			{
				Cerebrum.Runtime.Environment env = Cerebrum.Runtime.Environment.Current;
				if (env != null)
				{
					env.Dispose();
				}
			}
		}

        public Cerebrum.Ganglion.Remoting.IDatabaseController GetDatabaseController(string name, bool autostart)
        {
            ControllerImplementation ctlr = ControllerImplementation.Instance;
            try
            {
                return ctlr.GetDatabaseController(name, autostart);
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                if (ex.ResultHandle == new Cerebrum.ObjectHandle(0x80000002))
                {
                    try
                    {
                        ctlr.Dispose();
                        ctlr = ControllerImplementation.Instance;
                        return ctlr.GetDatabaseController(name, autostart);
                    }
                    catch (System.Threading.ThreadAbortException)
                    {
                        throw;
                    }
                    catch (System.Exception ex2)
                    {
                        throw new System.Exception("Failed to restart", ex2);
                    }
                }
                else
                {
                    throw;
                }
            }
        }

		public string PrimaryDirectory
		{
			get
			{
				return ControllerImplementation.Instance.PrimaryDirectory;
			}
			set
			{
				ControllerImplementation.Instance.PrimaryDirectory = value;
			}
		}

		#endregion
	}
}
