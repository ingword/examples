// Copyright (C) Dmitry Shuklin 2008-2009. All rights reserved.

using System;
using System.Collections.Generic;
using System.Text;

namespace Cerebrum
{
    class CriticalFinalizerObject : System.Runtime.ConstrainedExecution.CriticalFinalizerObject, System.IDisposable
    {
		protected static readonly object DisposingEvent;

		static CriticalFinalizerObject()
		{
            Cerebrum.CriticalFinalizerObject.DisposingEvent = new object();
		}

        ~CriticalFinalizerObject()
		{
            this.Dispose(false);
            //base.Finalize();
		}

        public event System.EventHandler Disposing
        {
            add
            {
                this.GetEventHandlerList().AddHandler(Cerebrum.CriticalFinalizerObject.DisposingEvent, value);
            }
            remove
            {
                if (this.m_Events != null)
                {
                    this.m_Events.RemoveHandler(Cerebrum.CriticalFinalizerObject.DisposingEvent, value);
                }
            }
        }

        public virtual void Dispose()
        {
            this.OnDisposing(EventArgs.Empty);
            this.Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && (this.m_Events != null))
            {
                this.m_Events.Dispose();
                this.m_Events = null;
            }
        }

        protected internal System.ComponentModel.EventHandlerList GetEventHandlerList()
        {
            if (this.m_Events == null)
            {
                this.m_Events = new System.ComponentModel.EventHandlerList();
            }
            return this.m_Events;
        }

        void System.IDisposable.Dispose()
        {
            this.Dispose();
        }

        protected internal virtual void OnDisposing(EventArgs e)
        {
            if (this.m_Events != null)
            {
                EventHandler handler = (System.EventHandler)this.m_Events[Cerebrum.CriticalFinalizerObject.DisposingEvent];
                if (handler != null)
                {
                    handler(this, e);
                }
            }
        }

        protected internal System.ComponentModel.EventHandlerList m_Events;
    }
}
