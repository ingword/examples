// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble
{
	/// <summary>
	/// Summary description for Application.
	/// </summary>
	public class Application : Cerebrum.Integrator.Application
	{
		private Cerebrum.Ganglion.Ensemble.ControllerImplementation m_ControllerImplementation;

		internal Application(Cerebrum.Ganglion.Ensemble.ControllerImplementation controllerImplementation)
		{
			this.m_ControllerImplementation = controllerImplementation;
		}

        internal Cerebrum.Runtime.Environment Environment
        {
            get
            {
                return this.m_Environment;
            }
        }

		protected string PrimaryDirectory
		{
			get
			{
				string primaryDirectory = this.m_ControllerImplementation != null ? (this.m_ControllerImplementation.PrimaryDirectory) : string.Empty;
				return (primaryDirectory == null || primaryDirectory == string.Empty) ? (System.IO.Path.GetDirectoryName(this.GetType().Assembly.Location)) : primaryDirectory;
			}
		}

        protected override Cerebrum.Integrator.IContextFactory CreateContextFactory()
		{
			string primaryDirectory = this.PrimaryDirectory;
			string databaseFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.bin");
			string activityFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.log");
			string templateFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.xdb");

            return new Cerebrum.Management.SimpleContextFactory(this, databaseFileName, activityFileName, 8, true, templateFileName, true);
		}

		public override Cerebrum.Integrator.DomainContext CreateMasterContext(string domainName, bool creating)
		{
			return new Cerebrum.Integrator.DomainContext(this);
		}

        public override void ProcessException(Exception ex, string source)
        {
            try
            {
                string contextDescription = string.Empty;
                try
                {
                    System.LocalDataStoreSlot slot = System.Threading.Thread.GetNamedDataSlot("Cerebrum.Ganglion.ExecutionContext");
                    if (slot != null)
                    {
                        Cerebrum.Ganglion.Remoting.ExecutionContext executionContext = System.Threading.Thread.GetData(slot) as Cerebrum.Ganglion.Remoting.ExecutionContext;
                        if (executionContext != null)
                        {
                            contextDescription = executionContext.Description;
                        }
                    }
                }
                catch
                {
                }
                string primaryDirectory = this.PrimaryDirectory;
                string activityFileName = System.IO.Path.Combine(primaryDirectory, "Cerebrum.Database.Master.txt");
                string text = string.Format("{1}: {2}{3}{0}{4}{0}", System.Environment.NewLine, System.DateTime.Now.ToString(), source, (null == contextDescription || string.Empty == contextDescription) ? string.Empty : ": " + contextDescription, ex.ToString());
                lock (this)
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(activityFileName, System.IO.FileMode.Append))
                    {
                        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fs, System.Text.Encoding.Default))
                        {
                            sw.WriteLine(text);
                        }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException /*ex*/)
            {
                throw;
            }
            catch
            {
            }
            if (ex is Cerebrum.Runtime.NativeException)
            {
                Cerebrum.Runtime.NativeException cex = ex as Cerebrum.Runtime.NativeException;
                {
                    if (cex.ResultHandle == new Cerebrum.ObjectHandle(0x80000002))
                    {
                        throw new Cerebrum.Runtime.NativeException(cex.ResultHandle, "Attempting to Cerebrum restart", ex);
                        //throw ex;
                    }
                }
            }
        }

        public override void Initialize()
        {
            this.IsMasterDomainReadOnly = true;
            base.Initialize();
        }
	}
}
