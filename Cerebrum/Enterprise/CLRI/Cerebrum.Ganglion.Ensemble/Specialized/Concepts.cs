// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble.Specialized
{
	/// <summary>
	/// Summary description for Concepts.
	/// </summary>
	public class Concepts
	{
		public Concepts()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static Cerebrum.ObjectHandle DatabaseFileNameAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "DatabaseFileName");
		}
		public static Cerebrum.ObjectHandle ActivityFileNameAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "ActivityFileName");
		}
        public static Cerebrum.ObjectHandle TemplateFileNameAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "TemplateFileName");
		}

	}
}
