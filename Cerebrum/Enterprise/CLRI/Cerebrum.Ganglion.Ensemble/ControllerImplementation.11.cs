// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Runtime.InteropServices;

namespace Cerebrum.Ganglion.Ensemble
{
	/// <summary>
	/// Summary description for ControllerImplementation.
	/// </summary>
	internal class ControllerImplementation : Cerebrum.Integrator.GenericComponent // : Cerebrum.Runtime.ConstrainedExecution.Singleton//System.Runtime.ConstrainedExecution.CriticalFinalizerObject
	{
        /*[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr GetModuleHandle(String lpModuleName);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr GetCurrentProcessId();*/

        private static object m_SyncRoot;

        static ControllerImplementation()
        {
            m_SyncRoot = new object();
        }

        internal static System.Exception BuildPublicException(Cerebrum.Runtime.NativeException ex)
        {
            return new System.Exception(string.Format("<Cerebrum.Runtime.NativeException>{0}{1}{0}</Cerebrum.Runtime.NativeException>{0}", System.Environment.NewLine, ex.ToString()));
        }

		internal static ControllerImplementation m_Instance;
        public static ControllerImplementation Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    lock (Cerebrum.Ganglion.Ensemble.ControllerImplementation.m_SyncRoot)
                    {
                        if (m_Instance == null)
                        {
                            m_Instance = new ControllerImplementation();
                        }
                    }
                }
                return m_Instance;
            }
        }

        public static System.IDisposable CreateGlobalSyncLock()
        {
            if (m_Instance == null)
            {
                Cerebrum.Runtime.Environment env = Cerebrum.Runtime.Environment.Current;
                if (env != null)
                {
                    return env.CreateGlobalSyncLock();
                }
            }
            else
            {
                return m_Instance.Environment.CreateGlobalSyncLock();
            }
            return null;
        }

        protected Cerebrum.Runtime.Environment m_Environment;

        internal ControllerImplementation()
        {
            m_PrimaryDirectory = null;
            m_Application = null;
            m_Databases = null;
            System.AppDomain.CurrentDomain.DomainUnload += new EventHandler(CurrentDomain_DomainUnload);

            Cerebrum.Runtime.Environment env = Cerebrum.Runtime.Environment.Current;
            if (env != null)
            {
                env.Dispose();
                env = null;
            }
            env = Cerebrum.Runtime.Environment.ConstructInstance();
            //m_Environment.RuntimeException += new System.Threading.ThreadExceptionEventHandler(Environment_RuntimeException);
            //m_Environment.Collecting += new EventHandler(Environment_Collecting);
            env.Disposing += new EventHandler(Environment_Disposing);

            m_Environment = env;
        }

        //~ControllerImplementation()
        //{
        //    this.Shutdown();
        //}

        protected override void Dispose(bool disposing)
        {
            if (m_Instance == this)
            {
                m_Instance = null;
            }

            System.AppDomain.CurrentDomain.DomainUnload -= new EventHandler(CurrentDomain_DomainUnload);

            Cerebrum.Runtime.Environment env = this.m_Environment;
            if (env == null)
            {
                env = Cerebrum.Runtime.Environment.Current;
            }
            try
            {
                if (disposing)
                {
                    this.ShutdownInternal();
                }
            }
            finally
            {
                if (env != null)
                {
                    env.Dispose();
                }
                base.Dispose(disposing);
            }
        }


        private void DetachEnvironment()
        {
            Cerebrum.Runtime.Environment env = this.m_Environment;
            if (env != null)
            {
                this.m_Environment = null;
                //env.RuntimeException -= new System.Threading.ThreadExceptionEventHandler(Environment_RuntimeException);
                //env.Collecting -= new EventHandler(Environment_Collecting);
                env.Disposing -= new EventHandler(Environment_Disposing);
            }
        }

        private void Environment_Disposing(object sender, EventArgs e)
        {
            if (this.m_Environment == sender)
            {
                DetachEnvironment();
            }
        }

		private Cerebrum.Ganglion.Ensemble.Application m_Application;
		private System.Collections.Hashtable m_Databases;
		private string m_PrimaryDirectory;

		public string PrimaryDirectory
		{
			get
			{
				string primaryDirectory = this.m_PrimaryDirectory;
				return (primaryDirectory == null || primaryDirectory == string.Empty) ? (System.IO.Path.GetDirectoryName(this.GetType().Assembly.Location)) : primaryDirectory;
			}
			set
			{
				this.m_PrimaryDirectory = value;
			}
		}

		private void CurrentDomain_DomainUnload(object sender, EventArgs e)
		{
			this.Dispose();
		}

        public void Initialize()
        {
            if (m_Databases == null)
            {
                using (System.IDisposable sync = this.m_Environment.CreateGlobalSyncLock())
                {
                    if (m_Databases == null)
                    {
                        m_Databases = new System.Collections.Hashtable();
                    }
                }
            }
            if (m_Application == null)
            {
                using (System.IDisposable sync = this.m_Environment.CreateGlobalSyncLock())
                {
                    if (m_Application == null)
                    {
                        m_Application = new Cerebrum.Ganglion.Ensemble.Application(this);
                        m_Application.Disposing += new EventHandler(m_Application_Disposing);
                        m_Application.Initialize();
                    }
                }
            }
            else
            {
                if (m_Application.MasterContext == null)
                {
                    using (System.IDisposable sync = this.m_Environment.CreateGlobalSyncLock())
                    {
						if (m_Application.MasterContext == null)
						{
							try
							{
								m_Application.Initialize();
								throw new System.Exception("Internal warning: MasterContext is null");
							}
							catch (System.Exception ex)
							{
								m_Application.ProcessException(ex, "ControllerImplementation.Initialize");
							}
						}
					}
                }
            }
        }

        void m_Application_Disposing(object sender, EventArgs e)
        {
            Cerebrum.Ganglion.Ensemble.Application app = sender as Cerebrum.Ganglion.Ensemble.Application;
            if (app != null)
            {
                if (app == m_Application)
                {
                    using(System.IDisposable sync = this.m_Environment.CreateGlobalSyncLock())
                    {
                        if (app == m_Application)
                        {
                            m_Application = null;
                        }
                    }
                }
                app.Disposing -= new EventHandler(m_Application_Disposing);
            }
        }

        private void ShutdownInternal()
        {
            if (m_Databases != null)
            {
                using (System.IDisposable sync = this.m_Environment.CreateGlobalSyncLock())
                {
                    System.Collections.Hashtable dbs;
                    //interlocked exchange dbs=null and m_Databases
                    lock (Cerebrum.Ganglion.Ensemble.ControllerImplementation.m_SyncRoot)
                    {
                        dbs = m_Databases;
                        if (dbs != null)
                        {
                            m_Databases = null;
                        }
                    }
                    if (dbs != null)
                    {
                        foreach (DatabaseController c in dbs.Values)
                        {
                            if (c != null)
                            {
                                c.Dispose();
                            }
                        }
                    }
                }
            }

            if (m_Application != null)
            {

                lock (Cerebrum.Ganglion.Ensemble.ControllerImplementation.m_SyncRoot)
                {
                    // interlocked exchange app=null and m_Application
                    Cerebrum.Ganglion.Ensemble.Application app;
                    app = m_Application;
                    if (app != null)
                    {
                        m_Application = null;
                    }

                    if (app != null)
                    {
                        Cerebrum.Runtime.Environment env = app.Environment;

                        app.Shutdown();
                        app.Dispose();

                        if (env != null)
                        {
                            env.Dispose();
                        }
                    }
                }
            }
            /*
            if (Cerebrum.Ganglion.Ensemble.ControllerImplementation.m_Instance == this)
            {
                lock (Cerebrum.Ganglion.Ensemble.ControllerImplementation.m_SyncRoot)
                {
                    if (Cerebrum.Ganglion.Ensemble.ControllerImplementation.m_Instance == this)
                    {
                        Cerebrum.Ganglion.Ensemble.ControllerImplementation.m_Instance = null;
                    }
                }
            }
            */
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
        }

		public Cerebrum.Runtime.Environment Environment
		{
			get
			{
				return this.m_Environment;
			}
		}

		public Cerebrum.Ganglion.Ensemble.Application Application
		{
			get
			{
				return this.m_Application;
			}
		}
		public DatabaseController GetDatabaseController(string name, bool autostart)
		{
			using(System.IDisposable sync = this.m_Environment.CreateGlobalSyncLock())
			{
				DatabaseController c = m_Databases==null?null:(DatabaseController)m_Databases[name];
				if(c==null)
				{
					this.Initialize();
                    if (m_Application == null)
                    {
                        throw new System.Exception("GetDatabaseController: Internal error: Application is null");
                    }
                    Cerebrum.Integrator.DomainContext masterContext = m_Application.MasterContext;
                    if (masterContext == null)
                    {
                        throw new System.Exception("GetDatabaseController: Internal error: MasterContext is null");
                    }
                    using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(masterContext, "Domains"))
                    {
                        if (view == null)
                        {
                            throw new System.Exception("GetDatabaseController: Can't find 'Domains' table in Master Database");
                        }
                        using (Cerebrum.IConnector domainDescriptor = view.FindItemViewByObjectHandle(Cerebrum.Management.Utilites.FindHandle(view, view.GetItemProperties(null)["Name"], name)).GetComposite())
                        {
                            if (domainDescriptor == null)
                            {
                                throw new System.Exception("GetDatabaseController: Internal error: Domain Descriptor is null");
                            }
							
							Cerebrum.Ganglion.Ensemble.Reflection.DomainDescriptor dd = domainDescriptor.Component as Cerebrum.Ganglion.Ensemble.Reflection.DomainDescriptor;

							string primaryDirectory = this.PrimaryDirectory;
							string databaseFileName = System.IO.Path.Combine(primaryDirectory, dd.DatabaseFileName);
							string activityFileName = System.IO.Path.Combine(primaryDirectory, dd.ActivityFileName);
							string templateFileName = System.IO.Path.Combine(primaryDirectory, dd.TemplateFileName);

                            c = new DatabaseController(name, databaseFileName, activityFileName, templateFileName, dd.ReadOnly);
                        }
                    }
                    if (m_Databases == null)
                    {
                        throw new System.Exception("GetDatabaseController: Internal error: Databases Hash is null");
                    }
					m_Databases[name] = c;
				}
				if(autostart)
				{
					c.Initialize();
				}
				return c;
			}
		}

	}
}
