// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Text;

namespace Cerebrum.Ganglion.Ensemble
{
	public class Tools
	{

		public static object InvokeRecord(Cerebrum.Integrator.DomainContext domainContext, string tableName, Cerebrum.ObjectHandle handle, string method, object[] parameters)
		{
			object v = null;
			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, tableName))
			{
				Cerebrum.Data.ComponentItemView item = view.CreateItemViewByObjectHandle(new Cerebrum.ObjectHandle(handle.ToIntPtr()));
				using (Cerebrum.IComposite connector = item.GetComposite())
				{
					object o = connector.Component;
					v = o.GetType().GetMethod(method).Invoke(o, parameters);
				}
			}
			return v;
		}
	}
}
