// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble
{
    /// <summary>
    /// Summary description for DatabaseInterface.
    /// </summary>
    public class RemoteDatabase : System.MarshalByRefObject, Cerebrum.Ganglion.Remoting.IRemoteDatabase
    {
		private class OrderByEntry
		{
			public OrderByEntry(string name, Cerebrum.Ganglion.Remoting.OrderDirection direction)
			{
				this.Name = name;
				this.Direction = direction;
			}
			public string Name;
			public Cerebrum.Ganglion.Remoting.OrderDirection Direction;
		}

        private class QueryContext
        {
            public System.Collections.Hashtable LimitDefWrappers;
            public System.Collections.Hashtable TableDefWrappers;
			public Cerebrum.Collections.Specialized.HandleObjectDictionary TupleDefWrappers;
			public System.Collections.ArrayList DirectHandleLimitDefs;
			public System.Collections.ArrayList CommonSearchLimitDefs;

            public QueryContext()
            {
                LimitDefWrappers = new System.Collections.Hashtable();
                TableDefWrappers = new System.Collections.Hashtable();
				TupleDefWrappers = new Cerebrum.Collections.Specialized.HandleObjectDictionary();
				DirectHandleLimitDefs = new System.Collections.ArrayList();
				CommonSearchLimitDefs = new System.Collections.ArrayList();
            }
        }

        private class BaseDefContext
        {
            public System.Collections.Hashtable OwnedBoundDefs;

            public BaseDefContext()
            {
                OwnedBoundDefs = new System.Collections.Hashtable();
            }
        }

        private class TupleDefContext
        {
			public Cerebrum.Ganglion.Remoting.TupleDef Tuple;

			public TupleDefContext()
            {
				Tuple = null;
            }
        }

        private class TableDefContext : BaseDefContext
        {
			public Cerebrum.Collections.Specialized.HandleObjectDictionary TupleDefWrappers;
			public System.Collections.Hashtable FieldDefWrappers;
            public Cerebrum.Ganglion.Remoting.TableDef TableDef;
			public System.Collections.ArrayList RelatedTableJoins;
			public Cerebrum.Ganglion.Remoting.OrderDirection[] SortMapOrderDirections;
			public System.Collections.SortedList SortMap;
			public bool PropDescsPrepared;

			public TableDefContext(Cerebrum.Ganglion.Remoting.TableDef td)
				: base()
			{
				this.TableDef = td;
				PropDescsPrepared = false;
				SortMap = null;
				SortMapOrderDirections = null;
				RelatedTableJoins = new System.Collections.ArrayList();
				FieldDefWrappers = new System.Collections.Hashtable();
				TupleDefWrappers = new Cerebrum.Collections.Specialized.HandleObjectDictionary();
				if (td.Fields != null)
					foreach (Cerebrum.Ganglion.Remoting.FieldDef fd in td.Fields)
					{
						if (!this.FieldDefWrappers.ContainsKey(fd))
						{
							this.FieldDefWrappers.Add(fd, new FieldDefContext(fd));
						}
						else
						{
							throw new System.Exception("TableDef.Fields must contain only one reference to each FieldDef class instance");
						}
					}
				if (td.Orders != null)
					foreach (Cerebrum.Ganglion.Remoting.OrderDef od in td.Orders)
					{
						if (!this.FieldDefWrappers.ContainsKey(od.Field))
						{
							if (od.Field.Table != td)
							{
								throw new System.Exception("Order field must be owned by the same table");
							}
							this.FieldDefWrappers.Add(od.Field, new FieldDefContext(od.Field));
						}
					}
			}
        }

        private class BoundDefState
        {
            public object Result;

            public BoundDefState()
            {
                this.Result = null;
            }
        }

        private class FieldDefContext
        {
            public Cerebrum.Ganglion.Remoting.FieldDef FieldDef;
			public System.ComponentModel.PropertyDescriptor PropertyDescriptor;
			public bool JoinContainer;
            public FieldDefContext(Cerebrum.Ganglion.Remoting.FieldDef fd)
            {
                FieldDef = fd;
				PropertyDescriptor = null;
				JoinContainer = false;
            }
        }

        private class LimitDefContext
        {
            public Cerebrum.Ganglion.Remoting.LimitDef LimitDef;
            public System.Collections.Hashtable TableNameBoundDefMap;
			public Cerebrum.Ganglion.Remoting.CompareBoundDef DirectHandleBoundDef;

            public LimitDefContext(Cerebrum.Ganglion.Remoting.LimitDef ld)
            {
                LimitDef = ld;
				TableNameBoundDefMap = new System.Collections.Hashtable();
				DirectHandleBoundDef = null;
            }
        }

        public RemoteDatabase()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        private class ArrayComparer : System.Collections.IComparer
        {
            Cerebrum.Ganglion.Remoting.OrderDirection[] OrderDirections;
            public ArrayComparer(Cerebrum.Ganglion.Remoting.OrderDirection[] drords)
            {
                OrderDirections = drords;
            }

            #region IComparer Members

            public int Compare(object x, object y)
            {
                System.Array ax = x as System.Array;
                System.Array ay = y as System.Array;
                for (int i = 0; i < this.OrderDirections.Length; i++)
                {
                    object ox = ax.GetValue(i);
                    object oy = ay.GetValue(i);
                    System.IComparable c = ox as System.IComparable;
                    int r = c.CompareTo(oy);
                    if (r != 0)
                        if (this.OrderDirections[i] == Cerebrum.Ganglion.Remoting.OrderDirection.Ascending)
                        {
                            return r;
                        }
                        else
                        {
                            return -r;
                        }
                }
                return 0;
            }

            #endregion
        }

        /// <summary>
        /// ��������� ���������� �������� ����� ����� ����������
        /// </summary>
        /// <param name="expression">���������� ���������</param>
        /// <param name="Row">���� ������ �� ������� � ����. � �������� ����� � Hashtable �������� �������� ���� (��������).</param>
        /// <returns>��������� ���������� ���������� ��������</returns>
        //private static bool CalculateCompareBoundDef(Cerebrum.Ganglion.Remoting.CompareBoundDef expression, System.Collections.Hashtable Row)
		private static bool CalculateCompareBoundDef(Cerebrum.Ganglion.Remoting.CompareBoundDef expression, Cerebrum.Collections.Specialized.HandleObjectDictionary Row)
        {
            object val1 = null;
            object val2 = null;
            bool res = false;
            double d1 = 0;
            double d2 = 0;
            bool f1 = true;
            bool f2 = true;

            val1 = Row[expression.Field.ObjectHandle];
			val2 = (expression.Value as Cerebrum.Ganglion.Remoting.ValueDef).Value;
			// ���������� ���������� ��������.
            switch (expression.Action)
            {
                case Cerebrum.Ganglion.Remoting.LogicAction.Equal:
                    {
                        try
                        {
                            // �������� ������������� ������ � �����.
                            d1 = double.Parse(val1.ToString());
                            f1 = true;
                        }
                        catch (Exception ex)
                        {
                            string str1 = ex.Message;
                            f1 = false;
                        }
                        try
                        {
                            // �������� ������������� ������ � �����.
                            d2 = double.Parse(val2.ToString());
                            f2 = true;
                        }
                        catch (Exception ex)
                        {
                            string str2 = ex.Message;
                            f2 = false;
                        }
                        if ((f1 == true) && (f2 == true))
                        {
                            // ��������� �����.
                            res = d1 == d2;
                        }
                        else if ((val1.GetType() == typeof(string)) && (val2.GetType() == typeof(string)))
                        {
                            // ��������� �����.
                            res = val1.ToString() == val2.ToString();
                        }
                        else
                        {
                            // ��������� ��������.
                            res = val1 == val2;
                        }
                        break;
                    }
                case Cerebrum.Ganglion.Remoting.LogicAction.NotEqual:
                    {
                        try
                        {
                            // �������� ������������� ������ � �����.
                            d1 = double.Parse(val1.ToString());
                            f1 = true;
                        }
                        catch (Exception ex)
                        {
                            string str1 = ex.Message;
                            f1 = false;
                        }
                        try
                        {
                            // �������� ������������� ������ � �����.
                            d2 = double.Parse(val2.ToString());
                            f2 = true;
                        }
                        catch (Exception ex)
                        {
                            string str2 = ex.Message;
                            f2 = false;
                        }
                        if ((f1 == true) && (f2 == true))
                        {
                            // ��������� �����.
                            res = d1 != d2;
                        }
                        else if ((val1.GetType() == typeof(string)) && (val2.GetType() == typeof(string)))
                        {
                            // ��������� �����.
                            res = val1.ToString() != val2.ToString();
                        }
                        else
                        {
                            // ��������� ��������.
                            res = val1 != val2;
                        }
                        break;
                    }
                case Cerebrum.Ganglion.Remoting.LogicAction.Above:
                    {
                        res = Convert.ToDouble(val1.ToString()) > Convert.ToDouble(val2.ToString());
                        break;
                    }
                case Cerebrum.Ganglion.Remoting.LogicAction.Below:
                    {
                        res = Convert.ToDouble(val1.ToString()) < Convert.ToDouble(val2.ToString());
                        break;
                    }
                case Cerebrum.Ganglion.Remoting.LogicAction.NotBelow:
                    {
                        res = Convert.ToDouble(val1.ToString()) >= Convert.ToDouble(val2.ToString());
                        break;
                    }
                case Cerebrum.Ganglion.Remoting.LogicAction.NotAbove:
                    {
                        res = Convert.ToDouble(val1.ToString()) <= Convert.ToDouble(val2.ToString());
                        break;
                    }
            }
            return res;
        }

        /// <summary>
        /// ��������� �������� OR ����� �����������
        /// </summary>
        /// <param name="LimitDefs">������ ��������� ����� �������� ����� ��������� �������� OR</param>
        /// <param name="Row">���� ������ �� ������� � ����. � �������� ����� � Hashtable �������� �������� ���� (��������).</param>
        /// <returns>��������� ���������� �������� OR.</returns>
        //        private static bool CalculateOR(Cerebrum.Ganglion.Remoting.LimitDef[] LimitDefs, System.Collections.Hashtable Row)
        //        {
        //            bool res = false;
        //            bool r = false;
        //            Cerebrum.Ganglion.Remoting.LimitDef LimitDef;
        //
        //            if (LimitDefs != null)
        //            {
        //                for (int i = 0; i < LimitDefs.Length; i++)
        //                {
        //                    LimitDef = LimitDefs[i];
        //                    r = CalculateAND(LimitDef, Row);
        //                    res = res || r;
        //                }
        //            }
        //            else
        //            {
        //                res = true;
        //            }
        //            return res;
        //        }

        /// <summary>
        /// ��������� �������� AND ����� �����������
        /// </summary>
        /// <param name="LimitDef">������ ��������� ����� �������� ����� ��������� �������� AND</param>
        /// <param name="Row">���� ������ �� ������� � ����. � �������� ����� � Hashtable �������� �������� ���� (��������).</param>
        /// <returns>��������� ���������� �������� AND.</returns>
		private static bool CalculateAND(System.Collections.ArrayList BoundDefList, Cerebrum.Collections.Specialized.HandleObjectDictionary Row)
        {
            object LogicAction;
            bool res = true;
            bool r = false;

            for (int i = 0; i < BoundDefList.Count; i++)
            {
                LogicAction = BoundDefList[i];
                //r = CalculateLogicAction(LogicAction, Row);
                if (LogicAction.GetType() == typeof(Cerebrum.Ganglion.Remoting.CompareBoundDef))
                {
                    r = CalculateCompareBoundDef((Cerebrum.Ganglion.Remoting.CompareBoundDef)LogicAction, Row);
                    res = res && r;
                }
            }
            return res;
        }
        //		private static bool CalculateAND(Cerebrum.Ganglion.Remoting.LimitDef LimitDef, System.Collections.Hashtable Row)
        //		{
        //			Cerebrum.Ganglion.Remoting.BoundDef LogicAction;
        //			bool res = true;
        //			bool r = false;
        //
        //			for(int i=0; i<LimitDef.Expression.Length; i++)
        //			{
        //				LogicAction = LimitDef.Expression[i];
        //				//r = CalculateLogicAction(LogicAction, Row);
        //				if(LogicAction.GetType() == typeof(Cerebrum.Ganglion.Remoting.CompareBoundDef))
        //				{
        //					r = CalculateCompareBoundDef((Cerebrum.Ganglion.Remoting.CompareBoundDef)LogicAction, Row);
        //				}
        //				res = res && r;
        //			}
        //			return res;
        //		}

        //		public Cerebrum.Ganglion.Remoting.TableDef QueryTable(string dbName, string tableName, string[] columns, int from, int count, object query)
        //		{
        //			Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
        //			Cerebrum.Ganglion.Ensemble.IDatabaseController database = ensemble.GetDatabaseController(dbName, true) as Cerebrum.Ganglion.Ensemble.IDatabaseController;
        //			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
        //			{
        //				Cerebrum.Integrator.DomainContext domain = Cerebrum.Integrator.DomainContext.FromConnector(database.Workspace as Cerebrum.IConnector);
        //				using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, tableName))
        //				{
        //					return FromView(view, columns, from, count, query);
        //				}
        //			}
        //		}

		public Cerebrum.Ganglion.Remoting.TableDef[] ExecuteQuery(string dbName, Cerebrum.Ganglion.Remoting.QueryDef query)
        {
			return ExecuteQueryInternal(dbName, query);
        }

		public Cerebrum.Ganglion.Remoting.QueryDef BuildQuery(string dbName, string table, string[] columns, string[] orderby)
        {
			Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
			Cerebrum.Ganglion.Ensemble.IDatabaseController database = ensemble.GetDatabaseController(dbName, true) as Cerebrum.Ganglion.Ensemble.IDatabaseController;

			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				Cerebrum.Integrator.DomainContext domain = Cerebrum.Integrator.DomainContext.FromConnector(database.Workspace as Cerebrum.IConnector);
				using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, table))
				{
#warning "ResolveHandle should be depricated"
					Cerebrum.ObjectHandle tableHandle = database.ResolveHandle("Tables", "Name", table);

					Cerebrum.Ganglion.Remoting.TableDef tdf = new Cerebrum.Ganglion.Remoting.TableDef(tableHandle);

					System.ComponentModel.PropertyDescriptorCollection pds = (view as System.ComponentModel.ITypedList).GetItemProperties(null);

					System.Collections.ArrayList flst = new System.Collections.ArrayList();
					System.Collections.Hashtable fhsh = new System.Collections.Hashtable();
					for (int c = 0; c < columns.Length; c++)
					{
						string column = columns[c];
						System.ComponentModel.PropertyDescriptor pd = pds[column];
						if (pd != null)
						{
							Cerebrum.ObjectHandle ch;
							if (pd is Cerebrum.Data.AttributePropertyDescriptor)
							{
								ch = (pd as Cerebrum.Data.AttributePropertyDescriptor).AttributePlasmaHandle;
							}
							else
							{
								ch = new Cerebrum.ObjectHandle(-c - 1);
							}
							Cerebrum.Ganglion.Remoting.FieldDef fd = fhsh[ch] as Cerebrum.Ganglion.Remoting.FieldDef;
							if (fd == null)
							{
								System.Type propType = pd.PropertyType;
								fd = new Cerebrum.Ganglion.Remoting.FieldDef(ch, tdf, column, propType);
								fhsh.Add(ch, fd);
							}
							flst.Add(fd);
						}
					}
					tdf.Fields = (Cerebrum.Ganglion.Remoting.FieldDef[])flst.ToArray(typeof(Cerebrum.Ganglion.Remoting.FieldDef));
					if (orderby != null && orderby.Length > 0)
					{
						System.Collections.ArrayList olst = new System.Collections.ArrayList();
						for (int c = 0; c < orderby.Length; c++)
						{
							string column = orderby[c];
							Cerebrum.Ganglion.Remoting.OrderDirection od = Cerebrum.Ganglion.Remoting.OrderDirection.Ascending;
							if (column.EndsWith("+"))
							{
								od = Cerebrum.Ganglion.Remoting.OrderDirection.Ascending;
								column = column.Substring(0, column.Length - 1);
							}
							else if (column.EndsWith("-"))
							{
								od = Cerebrum.Ganglion.Remoting.OrderDirection.Descending;
								column = column.Substring(0, column.Length - 1);
							}

							System.ComponentModel.PropertyDescriptor pd = pds[column];
							if (pd != null)
							{
								Cerebrum.ObjectHandle ch;
								if (pd is Cerebrum.Data.AttributePropertyDescriptor)
								{
									ch = (pd as Cerebrum.Data.AttributePropertyDescriptor).AttributePlasmaHandle;
								}
								else
								{
									ch = new Cerebrum.ObjectHandle(-c - 1);
								}
								Cerebrum.Ganglion.Remoting.FieldDef fd = fhsh[ch] as Cerebrum.Ganglion.Remoting.FieldDef;
								if (fd == null)
								{
									System.Type propType = pd.PropertyType;
									fd = new Cerebrum.Ganglion.Remoting.FieldDef(ch, tdf, column, propType);
									fhsh.Add(ch, fd);
								}
								olst.Add(new Cerebrum.Ganglion.Remoting.OrderDef(fd, od));
							}
						}
						tdf.Orders = (Cerebrum.Ganglion.Remoting.OrderDef[])olst.ToArray(typeof(Cerebrum.Ganglion.Remoting.OrderDef));
					}
					else
					{
						tdf.Orders = null;
					}

					Cerebrum.Ganglion.Remoting.TableDef[] tables = new Cerebrum.Ganglion.Remoting.TableDef[1];
					tables[0] = tdf;
					Cerebrum.Ganglion.Remoting.BoundDef[] bounds = new Cerebrum.Ganglion.Remoting.BoundDef[1];
					bounds[0] = new Cerebrum.Ganglion.Remoting.TableNameBoundDef(tdf, table);
					Cerebrum.Ganglion.Remoting.LimitDef[] limits = new Cerebrum.Ganglion.Remoting.LimitDef[1];
					limits[0] = new Cerebrum.Ganglion.Remoting.LimitDef(bounds);
					return new Cerebrum.Ganglion.Remoting.QueryDef(tables, limits, Cerebrum.Ganglion.Remoting.QueryType.Select);
				}
			}
		}

		private static Cerebrum.Ganglion.Remoting.TableDef[] ExecuteQueryInternal(string dbName, Cerebrum.Ganglion.Remoting.QueryDef query)
		{
			System.Collections.Hashtable relatedTables = new System.Collections.Hashtable();
			Cerebrum.Ganglion.Remoting.IEnsembleController ensemble;
			Cerebrum.Ganglion.Ensemble.IDatabaseController database;
			Cerebrum.Integrator.DomainContext domain;
			ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
			database = ensemble.GetDatabaseController(dbName, true) as Cerebrum.Ganglion.Ensemble.IDatabaseController;
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				domain = Cerebrum.Integrator.DomainContext.FromConnector(database.Workspace as Cerebrum.IConnector);

				Cerebrum.Ganglion.Remoting.LimitDef ld;
				System.Collections.Hashtable htTableDefsList;
				Cerebrum.Ganglion.Remoting.TableNameBoundDef tnbd;

				htTableDefsList = new System.Collections.Hashtable();
				QueryContext context = new QueryContext();

				// ���������� BoundDef-� �� TableDef-��.
				if (query.TableDefs != null)
					for (int itd = 0; itd < query.TableDefs.Length; itd++)
					{
						Cerebrum.Ganglion.Remoting.TableDef td = query.TableDefs[itd];
						if (!context.TableDefWrappers.ContainsKey(td))
						{
							context.TableDefWrappers.Add(td, new TableDefContext(td));
						}
					}
				// ������� ������������ ��� ����� ����, 
				// ������ ���� (����������� ������������� ���������� �����) � ����� ��� �� ��������� ����������

				//#warning "������ ������"
				// ��� ����� ��������� �������� ��� ��� �� ���� TableDefContext � ��������� ��� ��� ������ 
				// TableDefContext.QueryFieldDefs �����, ����������� � �������.
				// ���� �� ���� TableDefContext. 
				// ����� hashtable � ��������� ���� ����� �� ��������� TableDef.Fields - ��� ��� ������, 
				// ������ ����������� TableDefContext.
				// (����� ��������� ��� ����� ��� � ���� � ��� ����� fields ������� ��������� � ��������� 
				// � �� ����� � ���������� ���������, ���� ���������.) �������.
				// 
				if (query.Expression != null && query.Expression.Length > 0)
				{
					for (int i1 = 0; i1 < query.Expression.Length; i1++)
					{
						ld = query.Expression[i1];
						LimitDefContext ldc = new LimitDefContext(ld);
						context.LimitDefWrappers.Add(ld, ldc);

						bool isDirectHandleLimitDef;
						isDirectHandleLimitDef = false;

						for (int i2 = 0; i2 < ld.Expression.Length; i2++)
						{
							Cerebrum.Ganglion.Remoting.BoundDef bdf = ld.Expression[i2];

							// ��������� BaseFieldBoundDef � ���� ������: ��������� �� �� TableDefWrappers & FieldDefWrappers
							if (bdf is Cerebrum.Ganglion.Remoting.BaseTableBoundDef)
							{
								Cerebrum.Ganglion.Remoting.BaseTableBoundDef bdd = bdf as Cerebrum.Ganglion.Remoting.BaseTableBoundDef;
								TableDefContext tdc;
								Cerebrum.Ganglion.Remoting.TableDef td2 = bdd.Table;
								if (context.TableDefWrappers.ContainsKey(td2))
								{
									tdc = (TableDefContext)context.TableDefWrappers[td2];
								}
								else
								{
									tdc = new TableDefContext(td2);
									context.TableDefWrappers.Add(td2, tdc);
								}

								if (tdc.OwnedBoundDefs.ContainsKey(ld) == true)
								{
									System.Collections.ArrayList al = (System.Collections.ArrayList)tdc.OwnedBoundDefs[ld];
									al.Add(bdd);
								}
								else
								{
									System.Collections.ArrayList al = new System.Collections.ArrayList();
									al.Add(bdd);
									tdc.OwnedBoundDefs.Add(ld, al);
								}

								tnbd = bdd as Cerebrum.Ganglion.Remoting.TableNameBoundDef;
								if (tnbd != null)
								{
									ldc.TableNameBoundDefMap.Add(tnbd.Table, tnbd);
								}

								// ������ ��� ���� ������������ � ����������.
								if (bdf is Cerebrum.Ganglion.Remoting.BaseTableFieldBoundDef)
								{
									Cerebrum.Ganglion.Remoting.BaseTableFieldBoundDef fbd = (Cerebrum.Ganglion.Remoting.BaseTableFieldBoundDef)bdf;
									TableDefContext tdc1 = (TableDefContext)context.TableDefWrappers[fbd.Field.Table];
									if (tdc1 == null)
									{
										tdc1 = new TableDefContext(fbd.Field.Table);
										context.TableDefWrappers.Add(fbd.Field.Table, tdc1);
									}
									FieldDefContext fdc1 = tdc1.FieldDefWrappers[fbd.Field] as FieldDefContext;
									if (fdc1 == null)
									{
										fdc1 = new FieldDefContext(fbd.Field);
										tdc1.FieldDefWrappers.Add(fbd.Field, fdc1);
									}

									// � ���� ��� ���������� ������� �������� ��� ���������, ������� ��������� �� ������ �������� ����� ����.
									// �����, ����� ��������� �� ����� �����, ��. ������� ���� ����� ��������� ������ - ����� �����
									// ����� ��������� �� ����� �����, �������� �� ��������, �������� �� ��� ����� � ��������� ������. ����� �������� ������.
									if (bdf is Cerebrum.Ganglion.Remoting.FieldJoinBoundDef)
									{
										fdc1.JoinContainer = true;
										if (tdc1.RelatedTableJoins.Contains(bdf) == false)
										{
											tdc1.RelatedTableJoins.Add(bdf);
											if (relatedTables.Contains(td2) == false)
											{
												relatedTables.Add(td2, bdf);
											}
										}
									}

									// ������ ��� ���� ������������ � ����������.
									if (bdf is Cerebrum.Ganglion.Remoting.CompareBoundDef)
									{
										// � ��� ��������� ������� ������ ���� ������������, ��� ��� ������� ����� 
										// ��� �� ��������� � ��������� � ���� ������ ����� ������.

										Cerebrum.Ganglion.Remoting.CompareBoundDef cbd = (Cerebrum.Ganglion.Remoting.CompareBoundDef)bdf;
										Cerebrum.Ganglion.Remoting.FieldDef fd2 = cbd.Value as Cerebrum.Ganglion.Remoting.FieldDef;
										if (fd2 != null)
										{
											TableDefContext tdc2 = (TableDefContext)context.TableDefWrappers[fd2.Table];
											if (tdc2 == null)
											{
												tdc2 = new TableDefContext(fd2.Table);
												context.TableDefWrappers.Add(fd2.Table, tdc2);

												if (tdc2.FieldDefWrappers.ContainsKey(fd2) == false)
												{
													tdc2.FieldDefWrappers.Add(fd2, new FieldDefContext(fd2));
												}
											}
										}

										Cerebrum.Ganglion.Remoting.ValueDef vd2 = cbd.Value as Cerebrum.Ganglion.Remoting.ValueDef;
										if ((vd2 != null)&&(cbd.Field.Name == "ObjectHandle"))
										{
											isDirectHandleLimitDef = true;
											ldc.DirectHandleBoundDef = cbd;
										}
									}
								}
							}
						}
						if (isDirectHandleLimitDef)
						{
							// ��������� LimitDef � ������ DirectHandleLimits
							context.DirectHandleLimitDefs.Add(ld);
						}
						else
						{
							// ��������� LimitDef � ������ CommonLimits
							context.CommonSearchLimitDefs.Add(ld);
						}
					}
				}
				if (query.Expression != null && query.Expression.Length > 1)
				{
					// ��������� LimitDefWrappers � ��������, ���� � ���� ���������� ���� ���������� ������� ���������
					System.Collections.Hashtable TblNameList = new System.Collections.Hashtable();

					System.Collections.Hashtable CloneTblNameList;
					LimitDefContext ldc = (LimitDefContext)context.LimitDefWrappers[(Cerebrum.Ganglion.Remoting.LimitDef)query.Expression[0]];
					foreach(Cerebrum.Ganglion.Remoting.TableNameBoundDef tnbd0 in ldc.TableNameBoundDefMap.Values)
					{
						TblNameList.Add(tnbd0.TableName, tnbd0);
					}
					for (int i1 = 1; i1 < query.Expression.Length; i1++)
					{
						CloneTblNameList = (System.Collections.Hashtable)TblNameList.Clone();
						ldc = (LimitDefContext)context.LimitDefWrappers[(Cerebrum.Ganglion.Remoting.LimitDef)query.Expression[i1]];
						foreach (Cerebrum.Ganglion.Remoting.TableNameBoundDef tnbd0 in ldc.TableNameBoundDefMap.Values)
						{
							if (CloneTblNameList.ContainsKey(tnbd0.TableName) == true)
							{
								CloneTblNameList.Remove(tnbd0.TableName);
							}
							else
							{
								throw (new Exception("�������� ������ �������."));
							}
						}
						if (CloneTblNameList.Count > 0)
						{
							throw (new Exception("������ ������."));
						}
					}
				}
				if (query.Expression != null && query.Expression.Length > 0)
				{
					LimitDefContext ldc1 = (LimitDefContext)context.LimitDefWrappers[(Cerebrum.Ganglion.Remoting.LimitDef)query.Expression[0]];
					foreach (Cerebrum.Ganglion.Remoting.TableNameBoundDef tnbd1 in ldc1.TableNameBoundDefMap.Values)
					{
						if (false == relatedTables.ContainsKey(tnbd1.Table))
						{
							FillRootTableDef(domain, tnbd1, context);
						}
					}

					// ��������� �������� �������.
					foreach (System.Collections.DictionaryEntry deTbl in context.TableDefWrappers)
					{
						TableDefContext tdc = deTbl.Value as TableDefContext;
						Cerebrum.Ganglion.Remoting.TableDef table = tdc.TableDef;
						if (table.Tuples == null && tdc.TupleDefWrappers.Count > 0)
						{
							// ��������� �������� �������.
							table.Tuples = new Cerebrum.Ganglion.Remoting.TupleDef[tdc.TupleDefWrappers.Count];
							// ���� ���� ��������� ����������
							if (tdc.SortMap != null)
							{
								for (int row = 0; row < tdc.SortMap.Count; row++)
								{
									table.Tuples[row] = (tdc.TupleDefWrappers[(Cerebrum.ObjectHandle)tdc.SortMap.GetByIndex(row)] as TupleDefContext).Tuple;
								}
							}
							else
							{
								int row = 0;
								foreach (System.Collections.DictionaryEntry de in tdc.TupleDefWrappers)
								{
									table.Tuples[row] = (de.Value as TupleDefContext).Tuple;
									row++;
								}
							}
						}
					}
				}
			}
			return query.TableDefs;
		}

		private static Cerebrum.Ganglion.Remoting.TableDef FillRootTableDef(Cerebrum.Integrator.DomainContext domain, Cerebrum.Ganglion.Remoting.TableNameBoundDef tnbd, QueryContext context)
        {
            Cerebrum.Data.TableView view;

			Cerebrum.Ganglion.Remoting.TableDef table = tnbd.Table;

            TableDefContext tdc = (TableDefContext)context.TableDefWrappers[table];

			if (tdc.SortMap == null)
			{
				tdc.SortMap = BuildSortMap(tdc);
			}

            using (view = Cerebrum.Management.Utilites.GetView(domain, tnbd.TableName))
            {
				PreparePropertyDescriptors(view, tdc);

                // ������ TupleDef.
				if (context.CommonSearchLimitDefs.Count > 0)
				{
					System.Collections.ArrayList limitDefs = new System.Collections.ArrayList(context.CommonSearchLimitDefs);
					limitDefs.AddRange(context.DirectHandleLimitDefs);

					for (int r = 0; r < view.Count; r++)
					{
						Cerebrum.Data.ComponentItemView item = view[r];
						ProcessItemView(domain, context, null, tdc, item, limitDefs);
					}
				}
				else
				{
					if (context.DirectHandleLimitDefs.Count > 0)
					{
						foreach (Cerebrum.Ganglion.Remoting.LimitDef ld in context.DirectHandleLimitDefs)
						{
							LimitDefContext ldc = (LimitDefContext)context.LimitDefWrappers[ld];
							Cerebrum.Data.ComponentItemView item = view.CreateItemViewByObjectHandle((Cerebrum.ObjectHandle)((Cerebrum.Ganglion.Remoting.ValueDef)ldc.DirectHandleBoundDef.Value).Value);
							ProcessItemView(domain, context, null, tdc, item, new System.Collections.ArrayList(new Cerebrum.Ganglion.Remoting.LimitDef[] {ld}));
						}
					}
				}

				// ����� ������� ������� �� ��� ��������
				foreach (System.Collections.DictionaryEntry fdwde in tdc.FieldDefWrappers)
				{
					FieldDefContext fdc = fdwde.Value as FieldDefContext;
					fdc.PropertyDescriptor = null;
				}
			}

            return table;
        }

		private static System.Collections.SortedList BuildSortMap(TableDefContext tdc)
		{
			Cerebrum.Ganglion.Remoting.TableDef table = tdc.TableDef;
			System.Collections.SortedList sortMap = null;
			Cerebrum.Ganglion.Remoting.OrderDirection[] drords = tdc.SortMapOrderDirections;
			if (drords == null)
			{
				if (table.Orders != null && table.Orders.Length > 0)
				{
					drords = new Cerebrum.Ganglion.Remoting.OrderDirection[table.Orders.Length + 1];
					for (int i = 0; i < table.Orders.Length; i++)
					{
						drords[i] = table.Orders[i].Direction;
					}
					drords[table.Orders.Length] = Cerebrum.Ganglion.Remoting.OrderDirection.Ascending;
					tdc.SortMapOrderDirections = drords;
				}
			}
			if (drords != null)
			{
				sortMap = new System.Collections.SortedList(new ArrayComparer(drords));
			}
			return sortMap;
		}

		private static void PreparePropertyDescriptors(System.ComponentModel.ITypedList view, TableDefContext tdc)
		{
			if (tdc.PropDescsPrepared)
			{
				return;
			}
			else
			{
				tdc.PropDescsPrepared = true;
				System.ComponentModel.PropertyDescriptorCollection pds = view.GetItemProperties(null);
				foreach (System.Collections.DictionaryEntry fdwde in tdc.FieldDefWrappers)
				{
					FieldDefContext fdc = fdwde.Value as FieldDefContext;
					fdc.PropertyDescriptor = pds[fdc.FieldDef.Name];

					//��������� ������������ ������� 
					if (fdc.PropertyDescriptor is Cerebrum.Data.AttributePropertyDescriptor)
					{
						Cerebrum.ObjectHandle plasma = (fdc.PropertyDescriptor as Cerebrum.Data.AttributePropertyDescriptor).AttributePlasmaHandle;
						if (plasma != fdc.FieldDef.ObjectHandle)
						{
							throw new Exception(string.Format("Field '{0}' has incompatible id's: databse = '{1}', request = '{2}'", fdc.FieldDef.Name, plasma.ToString(), fdc.FieldDef.ObjectHandle.ToString()));
						}
					}
				}
			}
		}

		private static TupleDefContext ProcessItemView(Cerebrum.Integrator.DomainContext domain, QueryContext context, System.Collections.SortedList sortMap, TableDefContext tdc, Cerebrum.Data.ComponentItemView item, System.Collections.ArrayList limitDefs)
		{
			Cerebrum.Ganglion.Remoting.TableDef table = tdc.TableDef;

			// ������� ��� ���� ��� ��������� ��� ������ ������� (����� ������� row)
			Cerebrum.Collections.Specialized.HandleObjectDictionary Row = new Cerebrum.Collections.Specialized.HandleObjectDictionary();

			// �������� �� ���� ����� ����������� � TableDefContext.FieldDefWrappers � �������� 
			// �� view �������� � ���� ���.
			foreach (System.Collections.DictionaryEntry fdwde in tdc.FieldDefWrappers)
			{
				FieldDefContext fdc = fdwde.Value as FieldDefContext;
				if (!(fdc.JoinContainer && (fdc.PropertyDescriptor.PropertyType == typeof(System.ComponentModel.IBindingList))))
				{
					if (fdc.PropertyDescriptor.PropertyType == typeof(System.ComponentModel.IBindingList))
					{
						System.ComponentModel.IBindingList cll = (System.ComponentModel.IBindingList)fdc.PropertyDescriptor.GetValue(item);
						System.ComponentModel.PropertyDescriptor pdobjh = ((System.ComponentModel.ITypedList)cll).GetItemProperties(null)["ObjectHandle"];
						Cerebrum.ObjectHandle[] harr = new ObjectHandle[cll.Count];
						for (int i2 = 0; i2 < cll.Count; i2++)
						{
							harr[i2] = (Cerebrum.ObjectHandle)pdobjh.GetValue(cll[i2]);
						}
						Row[fdc.FieldDef.ObjectHandle] = harr;
						if (cll is System.IDisposable)
						{
							(cll as System.IDisposable).Dispose();
						}
					}
					else
					{
						Row[fdc.FieldDef.ObjectHandle] = fdc.PropertyDescriptor.GetValue(item);
					}
				}
			}

			// ��� �� �������� �������������� �������� ������ � �� ���� ������ � �� ������ ���.
			// ������ ��� �� ������ ����� row ���������� �������� ���� ����� �����. 
			// � ����� ���� �������� �������, ��������� TupleDef �� ������ row � TableDef.Fields.

			bool goodRow = false;
			// ���������� LimitDef-�.
			foreach (Cerebrum.Ganglion.Remoting.LimitDef ld in limitDefs)
			{
				// �������� ������ BoundDef-��, ��������� � ������� BoundDef-��.
				System.Collections.ArrayList BoundDefList = null;
				BoundDefList = (System.Collections.ArrayList)tdc.OwnedBoundDefs[ld];

				// ��������� ������� �������.
				if (BoundDefList != null)
				{
					goodRow = CalculateAND(BoundDefList, Row);
				}
				else
				{
					goodRow = true;
				}

				if (goodRow == true)
				{
					break;
				}
			}

			if (goodRow == true)
			{
				Cerebrum.ObjectHandle tupleHandle = item.ObjectHandle;

				TupleDefContext tuc = (TupleDefContext)context.TupleDefWrappers[tupleHandle];
				bool tblTupleEmpty = (tdc.TupleDefWrappers[tupleHandle] == null);

				//����������
				object[] sortValues = null;
				if (sortMap != null || (tdc.SortMap != null && tblTupleEmpty))
				{
					sortValues = new object[table.Orders.Length + 1];
					for (int ii = 0; ii < table.Orders.Length; ii++)
					{
						Cerebrum.Ganglion.Remoting.FieldDef fd = table.Orders[ii].Field;
						sortValues[ii] = Row[fd.ObjectHandle];
					}
					sortValues[table.Orders.Length] = tdc.TupleDefWrappers.Count;

					if (sortMap != null)
					{
						sortMap.Add(sortValues, tupleHandle);
					}
				}

				// ���� � HandleObjectDictionary ���� ������ � ObjectHandle ����������� � ������ ������...
				if (tblTupleEmpty)
				{
					if (tuc == null)
					{
						tuc = new TupleDefContext();
						context.TupleDefWrappers[tupleHandle] = tuc;
					}

					if (tuc.Tuple == null || tuc.Tuple.Values == null || tuc.Tuple.Values.Length < 1)
					{
						Cerebrum.Ganglion.Remoting.EntryDef[] values = new Cerebrum.Ganglion.Remoting.EntryDef[table.Fields.Length];

						// ��������� ������ � �������.
						for (int ii = 0; ii < table.Fields.Length; ii++)
						{
							Cerebrum.Ganglion.Remoting.FieldDef fd = table.Fields[ii];

#warning "� ����� ���� ����������� ����, ������ �� ��� ������� ����� �������������� ���� ����������"
							values[ii] = new Cerebrum.Ganglion.Remoting.ValueDef(fd.ObjectHandle, Row[fd.ObjectHandle]);
						}

						tuc.Tuple = new Cerebrum.Ganglion.Remoting.TupleDef(tupleHandle, values);
					}
					else
					{
						Cerebrum.Collections.Specialized.HandleObjectDictionary hdvals = new Cerebrum.Collections.Specialized.HandleObjectDictionary();
						//�������� �������� �� tuc.Tuple.Values �� ���������� ������� Row, ��������� ���������� ������������
						foreach (Cerebrum.Ganglion.Remoting.EntryDef ed in tuc.Tuple.Values)
						{
							hdvals[ed.ObjectHandle] = ed;
						}

						// ��������� ������ � �������.
						for (int ii = 0; ii < table.Fields.Length; ii++)
						{
							Cerebrum.Ganglion.Remoting.FieldDef fd = table.Fields[ii];
							hdvals[fd.ObjectHandle] = new Cerebrum.Ganglion.Remoting.ValueDef(fd.ObjectHandle, Row[fd.ObjectHandle]);
						}

						Cerebrum.Ganglion.Remoting.EntryDef[] values = new Cerebrum.Ganglion.Remoting.EntryDef[hdvals.Count];
						int rowi = 0;
						foreach (System.Collections.DictionaryEntry de in hdvals)
						{
#warning "� ����� ���� ����������� ����, ������ �� ��� ������� ����� �������������� ���� ����������"
							values[rowi] = de.Value as Cerebrum.Ganglion.Remoting.EntryDef;
							rowi++;
						}
						tuc.Tuple = new Cerebrum.Ganglion.Remoting.TupleDef(tupleHandle, values);
					}


					// ������������ ���������� ��������� �������.
					tdc.TupleDefWrappers[tupleHandle] = tuc;

					if (tdc.SortMap != null)
					{
						tdc.SortMap.Add(sortValues, tupleHandle);
					}
				}

				//��������� JOINs
                            
				// ����� ������� ������� ������, ���� ��� ���� ������ ���������� ��� ��������� � ��� 
				// ������� � ����������� ������ � ��� - ��������
				for(int iii = 0; iii<tdc.RelatedTableJoins.Count; iii++)
				{
					Cerebrum.Ganglion.Remoting.FieldJoinBoundDef fjbd = (Cerebrum.Ganglion.Remoting.FieldJoinBoundDef)tdc.RelatedTableJoins[iii];
					if(fjbd != null)
					{
						if (fjbd.JoinType == Cerebrum.Ganglion.Remoting.FieldJoinType.ValueDef)
						{
							object oh = Row[fjbd.Field.ObjectHandle];
							if (oh is Cerebrum.ObjectHandle)
							{
								TableDefContext tdc2 = (TableDefContext)context.TableDefWrappers[fjbd.Table];
								LimitDefContext ldc0 = (LimitDefContext)context.LimitDefWrappers[limitDefs[0]];
								Cerebrum.Ganglion.Remoting.TableNameBoundDef tnbd = (Cerebrum.Ganglion.Remoting.TableNameBoundDef)ldc0.TableNameBoundDefMap[fjbd.Table];
								string tableName = tnbd.TableName;
								using (Cerebrum.Data.TableView view2 = Cerebrum.Management.Utilites.GetView(domain, tableName))
								{
									PreparePropertyDescriptors(view2 as System.ComponentModel.ITypedList, tdc2);

									Cerebrum.Data.ComponentItemView item0 = view2.CreateItemViewByObjectHandle((Cerebrum.ObjectHandle)oh);
									TupleDefContext tuc0 = ProcessItemView(domain, context, null, tdc2, item0, limitDefs);

									//�������� tuple � �������� ���������� ValueDef
									Cerebrum.Ganglion.Remoting.Utilites.FindValueDef(tuc.Tuple, fjbd.Field.ObjectHandle).Value = tuc0.Tuple;
								}
							}
						}
						if (fjbd.JoinType == Cerebrum.Ganglion.Remoting.FieldJoinType.TableDef)
						{
							FieldDefContext fdc = (FieldDefContext)tdc.FieldDefWrappers[fjbd.Field];
							TableDefContext tdc2 = (TableDefContext)context.TableDefWrappers[fjbd.Table];
							System.ComponentModel.IBindingList view2 = (System.ComponentModel.IBindingList)fdc.PropertyDescriptor.GetValue(item);

							if (view2 is System.ComponentModel.ITypedList)
							{
								PreparePropertyDescriptors(view2 as System.ComponentModel.ITypedList, tdc2);

								//����������
								if (tdc2.SortMap == null)
								{
									tdc2.SortMap = BuildSortMap(tdc2);
								}
								System.Collections.SortedList sortMap2 = BuildSortMap(tdc2);

								Cerebrum.Ganglion.Remoting.TupleDef[] harr = new Cerebrum.Ganglion.Remoting.TupleDef[view2.Count];
								for (int i2 = 0; i2 < view2.Count; i2++)
								{
									Cerebrum.Data.ComponentItemView item0 = (Cerebrum.Data.ComponentItemView)view2[i2];
									TupleDefContext tuc0 = ProcessItemView(domain, context, sortMap2, tdc2, item0, limitDefs);
									harr[i2] = tuc0.Tuple;
								}
								Cerebrum.Ganglion.Remoting.TableDef ntdf = new Cerebrum.Ganglion.Remoting.TableDef(fjbd.Field.ObjectHandle);
								ntdf.Fields = fjbd.Table.Fields;

								if (sortMap2 != null)
								{
									ntdf.Tuples = new Cerebrum.Ganglion.Remoting.TupleDef[sortMap2.Count];
									for (int row = 0; row < sortMap2.Count; row++)
									{
										ntdf.Tuples[row] = (tdc2.TupleDefWrappers[(Cerebrum.ObjectHandle)sortMap2.GetByIndex(row)] as TupleDefContext).Tuple;
									}
								}
								else
								{
									ntdf.Tuples = harr;
								}

								//�������� table � �������� ���������� ValueDef
								for (int vx = 0; vx < tuc.Tuple.Values.Length; vx++)
								{
									if (ntdf.ObjectHandle.Equals(tuc.Tuple.Values[vx].ObjectHandle))
									{
										tuc.Tuple.Values[vx] = ntdf;
										break;
									}
								}
								if (view2 is System.IDisposable)
								{
									(view2 as System.IDisposable).Dispose();
								}
							}
						}
					}
				}
				return tuc;
			}
			return null;
		}


		private static Cerebrum.Ganglion.Remoting.TableDef FromView(System.ComponentModel.IBindingList view, string[] columns, int from, int count, OrderByEntry[] orderBy)
        {
            Cerebrum.Ganglion.Remoting.TableDef tdf = new Cerebrum.Ganglion.Remoting.TableDef(Cerebrum.ObjectHandle.Null);
            System.ComponentModel.PropertyDescriptorCollection pds = (view as System.ComponentModel.ITypedList).GetItemProperties(null);
			System.Collections.ArrayList flst = new System.Collections.ArrayList();
            for (int c = 0; c < columns.Length; c++)
            {
                string column = columns[c];
                System.ComponentModel.PropertyDescriptor pd = pds[column];
				if (pd != null)
				{
					Cerebrum.ObjectHandle ch;
					if (pd is Cerebrum.Data.AttributePropertyDescriptor)
					{
						ch = (pd as Cerebrum.Data.AttributePropertyDescriptor).AttributePlasmaHandle;
					}
					else
					{
						ch = new Cerebrum.ObjectHandle(-c - 1);
					}
					System.Type propType = pd.PropertyType;
					flst.Add(new Cerebrum.Ganglion.Remoting.FieldDef(ch, tdf, column, propType));
				}
            }
            tdf.Fields = (Cerebrum.Ganglion.Remoting.FieldDef[])flst.ToArray(typeof(Cerebrum.Ganglion.Remoting.FieldDef));

			if (count != 0 && from < view.Count)
            {
                int _f, _t;
                if (from >= 0)
                {
                    _f = from;
                }
                else
                {
                    _f = 0;
                }
                if (count >= 0)
                {
                    int to = _f + count - 1;
                    if (to < view.Count)
                    {
                        _t = to;
                    }
                    else
                    {
                        _t = view.Count - 1;
                    }
                }
                else
                {
                    _t = view.Count - 1;
                }
                if (_f <= _t)
                {
                    tdf.Tuples = new Cerebrum.Ganglion.Remoting.TupleDef[_t - _f + 1];
                    System.Collections.SortedList map = null;
					if (orderBy != null && orderBy.Length > 0)
                    {
						System.ComponentModel.PropertyDescriptor[] pdords = new System.ComponentModel.PropertyDescriptor[orderBy.Length];
						Cerebrum.Ganglion.Remoting.OrderDirection[] drords = new Cerebrum.Ganglion.Remoting.OrderDirection[orderBy.Length];
                        for (int i = 0; i < pdords.Length; i++)
                        {
							pdords[i] = pds[orderBy[i].Name];
							drords[i] = orderBy[i].Direction;
                        }
                        map = new System.Collections.SortedList(new ArrayComparer(drords));
                        for (int r = 0; r < view.Count; r++)
                        {
                            object[] values = new object[pdords.Length];
                            object or = view[r];
                            for (int i = 0; i < pdords.Length; i++)
                            {
                                values[i] = pdords[i].GetValue(or);
                            }
                            map.Add(values, r);
                        }
                    }
                    for (int i = _f, r = 0; i <= _t; i++, r++)
                    {
                        object or;
                        if (map == null)
                        {
                            or = view[i];
                        }
                        else
                        {
                            or = view[(int)map.GetByIndex(i)];
                        }
                        Cerebrum.ObjectHandle orh;
                        if (or is Cerebrum.Data.ComponentItemView)
                        {
                            orh = (or as Cerebrum.Data.ComponentItemView).ObjectHandle;
                        }
                        else
                        {
                            orh = Cerebrum.ObjectHandle.Null;
                        }

                        Cerebrum.Ganglion.Remoting.TupleDef tpf = new Cerebrum.Ganglion.Remoting.TupleDef(orh, new Cerebrum.Ganglion.Remoting.ValueDef[columns.Length]);
                        tdf.Tuples[r] = tpf;
						for (int c = 0; c < tdf.Fields.Length; c++)
                        {
							string column = tdf.Fields[c].Name;
                            System.ComponentModel.PropertyDescriptor pd = pds[column];
                            if (pd.PropertyType == typeof(System.ComponentModel.IBindingList))
                            {
                                System.ComponentModel.PropertyDescriptorCollection pds2 = ((System.ComponentModel.ITypedList)pd.GetValue(or)).GetItemProperties(null);
                                System.ComponentModel.PropertyDescriptor pdobjh = pds2["ObjectHandle"];
                                System.Collections.IList cll = (System.Collections.IList)pd.GetValue(or);
                                Cerebrum.ObjectHandle[] harr = new ObjectHandle[cll.Count];
                                for (int i2 = 0; i2 < cll.Count; i2++)
                                {
                                    harr[i2] = (Cerebrum.ObjectHandle)pdobjh.GetValue(cll[i2]);
                                }
                                tpf.Values[c] = new Cerebrum.Ganglion.Remoting.ValueDef(tdf.Fields[c].ObjectHandle, harr);
                            }
                            else
                            {
                                tpf.Values[c] = new Cerebrum.Ganglion.Remoting.ValueDef(tdf.Fields[c].ObjectHandle, pd.GetValue(or));
                            }
                        }
                    }
                }
            }
            return tdf;
        }

        #region IRemoteDatabase Members

        public bool UpdateRecord(string dbName, string tableName, Cerebrum.ObjectHandle handle, string[] columns, object[] values)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(dbName, true);
            return database.UpdateRecord(tableName, handle, columns, values);
        }

        public bool DeleteRecord(string dbName, string tableName, Cerebrum.ObjectHandle handle)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(dbName, true);
            return database.DeleteRecord(tableName, handle);
        }

        public Cerebrum.Ganglion.Remoting.ValueDef[] SelectRecord(string dbName, string tableName, Cerebrum.ObjectHandle handle, string[] columns)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(dbName, true);
            return database.SelectRecord(tableName, handle, columns);
        }

        public Cerebrum.ObjectHandle InsertRecord(string dbName, string tableName, string[] columns, object[] values)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(dbName, true);
            return database.InsertRecord(tableName, columns, values);
        }


		public Cerebrum.Ganglion.Remoting.QueryRes QueryTable(string dbName, string tableName, string[] columns, int from, int count)
        {
			string[] columns2 = new string[columns.Length];
			OrderByEntry[] orderBy = PrepOrderByNames(columns2, columns);

			Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Ensemble.IDatabaseController database = ensemble.GetDatabaseController(dbName, true) as Cerebrum.Ganglion.Ensemble.IDatabaseController;
            using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
            {
                Cerebrum.Integrator.DomainContext domain = Cerebrum.Integrator.DomainContext.FromConnector(database.Workspace as Cerebrum.IConnector);
                using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, tableName))
                {
					return new Cerebrum.Ganglion.Remoting.QueryRes(FromView(view, columns2, from, count, orderBy), view.Count);
                }
            }
        }

		private static OrderByEntry[] PrepOrderByNames(string[] columns2, string[] columns)
		{
			System.Collections.ArrayList olst = new System.Collections.ArrayList();
			for (int c = 0; c < columns.Length; c++)
			{
				string column = columns[c];
				if (column.EndsWith("+"))
				{
					Cerebrum.Ganglion.Remoting.OrderDirection od = Cerebrum.Ganglion.Remoting.OrderDirection.Ascending;
					od = Cerebrum.Ganglion.Remoting.OrderDirection.Ascending;
					column = column.Substring(0, column.Length - 1);
					olst.Add(new OrderByEntry(column, od));
					columns2[c] = column;
				}
				else if (column.EndsWith("-"))
				{
					Cerebrum.Ganglion.Remoting.OrderDirection od = Cerebrum.Ganglion.Remoting.OrderDirection.Ascending;
					od = Cerebrum.Ganglion.Remoting.OrderDirection.Descending;
					column = column.Substring(0, column.Length - 1);
					olst.Add(new OrderByEntry(column, od));
					columns2[c] = column;
				}
				else
				{
					columns2[c] = column;
				}
			}
			OrderByEntry[] orderBy = (OrderByEntry[])olst.ToArray(typeof(OrderByEntry));
			return orderBy;
		}

		public Cerebrum.Ganglion.Remoting.QueryRes QueryRecord(string dbName, string tableName, string fieldName, ObjectHandle handle, string[] columns, int from, int count)
        {
			string[] columns2 = new string[columns.Length];
			OrderByEntry[] orderBy = PrepOrderByNames(columns2, columns);
			
			Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Ensemble.IDatabaseController database = ensemble.GetDatabaseController(dbName, true) as Cerebrum.Ganglion.Ensemble.IDatabaseController;
            try
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    Cerebrum.Integrator.DomainContext domain = Cerebrum.Integrator.DomainContext.FromConnector(database.Workspace as Cerebrum.IConnector);
                    using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, tableName))
                    {
                        System.ComponentModel.PropertyDescriptor pd = (view as System.ComponentModel.ITypedList).GetItemProperties(null)[fieldName];
                        object o = view.CreateItemViewByObjectHandle(handle);
                        object v = pd.GetValue(o);
                        if (v is Cerebrum.Data.SimpleView)
                            using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)v)
                            {
                                return new Cerebrum.Ganglion.Remoting.QueryRes(FromView(view2, columns2, from, count, orderBy), view2.Count);
                            }
                    }
                }
                return null;
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                throw ControllerImplementation.BuildPublicException(ex);
            }
        }

        public object InvokeRecord(string databaseName, string tableName, ObjectHandle handle, string method, object[] parameters)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(databaseName, true);
            return database.InvokeRecord(tableName, handle, method, parameters);
        }

        public object InvokeComponent(string databaseName, /*string tableName, */string componentName, string method, object[] parameters)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(databaseName, true);
            string tableName = "Components";
            Cerebrum.ObjectHandle handle = database.ResolveHandle(tableName, "Name", componentName);
            return database.InvokeRecord(tableName, handle, method, parameters);
        }

        public bool EngageRecord(string databaseName, string tableName, ObjectHandle handle)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(databaseName, true);
            return database.EngageRecord(tableName, handle);
        }

        public bool EngageRecord(string databaseName, string tableName, ObjectHandle recordHandle, string fieldName, ObjectHandle engageHandle)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble;
            Cerebrum.Ganglion.Ensemble.IDatabaseController database;
            Cerebrum.Integrator.DomainContext domain;
            Cerebrum.Data.ComponentItemView iv;
            bool res = false;

            ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            database = ensemble.GetDatabaseController(databaseName, true) as Cerebrum.Ganglion.Ensemble.IDatabaseController;
            try
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    domain = Cerebrum.Integrator.DomainContext.FromConnector(database.Workspace as Cerebrum.IConnector);
                    // �������� �������.
                    using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, tableName))
                    {
                        // �������� ������ ��� ������ �� ���� �������.
                        iv = view.FindItemViewByObjectHandle(recordHandle);
                        // �������� ������.
                        using (Cerebrum.Data.VectorView v = (Cerebrum.Data.VectorView)iv[fieldName])
                        {
                            using (Cerebrum.Runtime.NativeSector sector = domain.GetSector())
                            {
                                using (Cerebrum.IComposite comp = sector.AttachConnector(engageHandle))
                                {
                                    // ��������� engageHandle � ���� ������.
                                    v.EngageConnector(engageHandle, comp);
                                    res = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                throw ControllerImplementation.BuildPublicException(ex);
            }
            return res;
        }

        public ObjectHandle ResolveHandle(string databaseName, string tableName, string fieldName, object value)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(databaseName, true);
            return database.ResolveHandle(tableName, fieldName, value);
        }

        public void Shutdown(string databaseName)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(databaseName, false);
            database.Shutdown();
        }

        public void Impersonate(System.Security.Principal.IPrincipal principal)
        {
            System.Threading.Thread.CurrentPrincipal = principal;
        }

        public void SetExecutionContext(Cerebrum.Ganglion.Remoting.ExecutionContext executionContext)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
            {
                if (executionContext != null)
                {
                    System.LocalDataStoreSlot slot = System.Threading.Thread.GetNamedDataSlot("Cerebrum.Ganglion.ExecutionContext");
                    if (slot == null)
                    {
                        slot = System.Threading.Thread.AllocateNamedDataSlot("Cerebrum.Ganglion.ExecutionContext");
                    }
                    System.Threading.Thread.SetData(slot, executionContext);
                }
            }
        }

        #endregion
    }
    /*
    private static System.Data.DataTable FromView(System.ComponentModel.IBindingList view, string[] columns, int from, int to)
    {
        System.Data.DataTable dt = new System.Data.DataTable();
        System.ComponentModel.PropertyDescriptorCollection pds = (view as System.ComponentModel.ITypedList).GetItemProperties(null);
        foreach (string column in columns)
        {
            System.ComponentModel.PropertyDescriptor pd = pds[column];
            System.Type propType = pd.PropertyType;
            System.Data.DataColumn dc;
            if (propType == typeof(Cerebrum.ObjectHandle))
            {
                propType = typeof(System.Int32);
                dc = new System.Data.DataColumn(column, propType, null, System.Data.MappingType.Attribute);
            }
            else
            {
                dc = new System.Data.DataColumn(column, propType, null, System.Data.MappingType.Element);
            }
            dt.Columns.Add(dc);
        }
        int _f, _t;
        if (from >= 0)
        {
            _f = from;
        }
        else
        {
            _f = 0;
        }
        if (to >= 0)
        {
            if (to < view.Count)
            {
                _t = to;
            }
            else
            {
                _t = view.Count - 1;
            }
        }
        else
        {
            _t = view.Count - 1;
        }
        for (int i = _f; i <= _t; i++)
        {
            object or = view[i];
            System.Data.DataRow dr = dt.NewRow();
            foreach (string column in columns)
            {
                System.ComponentModel.PropertyDescriptor pd = pds[column];
                System.Data.DataColumn dc = dt.Columns[column];

                if (pd.PropertyType == typeof(Cerebrum.ObjectHandle))
                {
                    dr[dc] = ((Cerebrum.ObjectHandle)pd.GetValue(or)).ToInt32();
                }
                else
                {
                    dr[dc] = pd.GetValue(or);
                }

            }
            dt.Rows.Add(dr);
        }
        return dt;
    }
    */
}

