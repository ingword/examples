// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble
{
    internal interface IDatabaseController : Cerebrum.Ganglion.Remoting.IDatabaseController
    {
        Cerebrum.IWorkspace Workspace { get;}
    }
    /// <summary>
    /// Summary description for DatabaseController.
    /// </summary>
    public class DatabaseController : System.MarshalByRefObject, IDatabaseController, System.IDisposable
    {
        private string m_DomainName;
        private Cerebrum.Integrator.IContextFactory m_ContextFactory;
        private Cerebrum.IWorkspace m_Workspace;
		private bool m_IsDirty;
		private bool m_IsClean;
        private bool m_IsReadOnly;

		public bool IsDirty
		{
			get
			{
				return this.m_IsDirty || !this.m_IsClean;
			}
		}

		public void SetDirty()
		{
			this.m_IsDirty = true;
		}

		public void SetClean()
		{
			this.m_IsClean = true;
		}


        internal DatabaseController(string name, string databaseFileName, string activityFileName, string templateFileName, bool readOnly)
        {
            m_DomainName = name;

            m_ContextFactory = new Cerebrum.Management.SimpleContextFactory(ControllerImplementation.Instance.Application, databaseFileName, activityFileName, 8, true, templateFileName, true);

			m_Workspace = null;
            this.m_IsReadOnly = readOnly;
			this.m_IsDirty = false;
			this.m_IsClean = false;
        }

        public void Initialize()
        {
            if (m_Workspace == null)
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    if (m_Workspace == null)
                    {
                        Cerebrum.Ganglion.Ensemble.Application application = ControllerImplementation.Instance.Application;


                        string databaseFileName = m_ContextFactory.DatabaseFileName;

                        bool isNew;
                        //try
                        //{
                            if (System.IO.File.Exists(databaseFileName))
                            {
                                m_Workspace = m_ContextFactory.LoadContext(databaseFileName, this.m_IsReadOnly);
                                isNew = false;
                            }
                            else
                            {
                                m_Workspace = m_ContextFactory.InitContext(databaseFileName, this.m_DomainName);
                                isNew = true;
                            }
#warning "commented BuildPublicException"
                        //}
                        //catch (Cerebrum.Runtime.NativeException ex)
                        //{
                        //    throw ControllerImplementation.BuildPublicException(ex);
                        //}
                        Cerebrum.Integrator.DomainContext c = m_Workspace.Component as Cerebrum.Integrator.DomainContext;

                        m_ContextFactory.ImportDatabase(c, isNew);

                        application.AddDomain(c);

                        c.Initialize();
                    }
                }
            }
        }

        public void Shutdown()
        {
			this.m_IsDirty = false;
			this.m_IsClean = false;

			if (m_Workspace != null)
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    if (m_Workspace != null)
                    {
                        Cerebrum.Integrator.DomainContext domainContext = Cerebrum.Integrator.DomainContext.FromConnector(m_Workspace as Cerebrum.IConnector);
                        domainContext.Workspace.Activator = null;
                        domainContext.Shutdown();
                        m_Workspace = null;
                    }
                }
            }
        }
        public Cerebrum.IWorkspace Workspace
        {
            get
            {
                return new SyncronizedWorkspace(m_Workspace);
            }
        }

        internal Cerebrum.Integrator.DomainContext DomainContext
        {
            get
            {
                this.Initialize();
                return Cerebrum.Integrator.DomainContext.FromConnector(this.m_Workspace);
            }
        }

        public Cerebrum.ObjectHandle InsertRecord(string tableName, string[] columns, object[] values)
        {
            try
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    Cerebrum.Data.ComponentItemView o = null;
                    Cerebrum.Integrator.DomainContext domain = this.DomainContext;
                    using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, tableName))
                    {
                        System.ComponentModel.PropertyDescriptorCollection pds = (view as System.ComponentModel.ITypedList).GetItemProperties(null);
                        o = view.AddNew();
                        for (int i = 0; i < columns.Length; i++)
                        {
                            string column = columns[i];
                            System.ComponentModel.PropertyDescriptor pd = pds[column];

                            pd.SetValue(o, values[i]);
                        }
                    }
                    Cerebrum.ObjectHandle h = o.ObjectHandle;
                    this.Shutdown();
                    return h;
                }
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                throw ControllerImplementation.BuildPublicException(ex);
            }
        }

        public bool UpdateRecord(string tableName, Cerebrum.ObjectHandle handle, string[] columns, object[] values)
        {
            try
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    Cerebrum.Integrator.DomainContext domain = this.DomainContext;
                    using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, tableName))
                    {
                        System.ComponentModel.PropertyDescriptorCollection pds = (view as System.ComponentModel.ITypedList).GetItemProperties(null);
                        object o = view.CreateItemViewByObjectHandle(new Cerebrum.ObjectHandle(handle.ToIntPtr()));
                        for (int i = 0; i < columns.Length; i++)
                        {
                            string column = columns[i];
                            System.ComponentModel.PropertyDescriptor pd = pds[column];

                            pd.SetValue(o, values[i]);
                        }
                    }
                    this.Shutdown();
                    return true;
                }
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                throw ControllerImplementation.BuildPublicException(ex);
            }
        }
        public bool EngageRecord(string tableName, Cerebrum.ObjectHandle handle)
        {
            try
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    Cerebrum.Integrator.DomainContext domain = this.DomainContext;
                    using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, tableName))
                    {
                        using (Cerebrum.IComposite connector = domain.AttachConnector(handle))
                        {
                            view.EngageConnector(handle, connector);
                        }
                    }
                    this.Shutdown();
                    return true;
                }
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                throw ControllerImplementation.BuildPublicException(ex);
            }
        }
        public object InvokeRecord(string tableName, Cerebrum.ObjectHandle handle, string method, object[] parameters)
        {
            try
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
					this.m_IsClean = false;

                    Cerebrum.Integrator.DomainContext domain = this.DomainContext;
                    object v = Tools.InvokeRecord(domain, tableName, handle, method, parameters);

					if (this.IsDirty)
					{
						this.Shutdown();
					}
					else
					{
						this.m_IsClean = false;
					}
                    return v;
                }
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                throw ControllerImplementation.BuildPublicException(ex);
            }
        }
        public bool DeleteRecord(string tableName, Cerebrum.ObjectHandle handle)
        {
            try
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    Cerebrum.Integrator.DomainContext domain = this.DomainContext;
                    using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, tableName))
                    {
                        view.Remove(view.CreateItemViewByObjectHandle(handle));
                        domain.Workspace.RemoveConnector(handle);
                    }
                    this.Shutdown();
                    return true;
                }
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                throw ControllerImplementation.BuildPublicException(ex);
            }
        }

        public Cerebrum.Ganglion.Remoting.ValueDef[] SelectRecord(string tableName, Cerebrum.ObjectHandle handle, string[] columns)
        {
            try
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
					Cerebrum.Ganglion.Remoting.ValueDef[] values = new Cerebrum.Ganglion.Remoting.ValueDef[columns.Length];
                    Cerebrum.Integrator.DomainContext domain = this.DomainContext;
                    using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domain, tableName))
                    {
                        System.ComponentModel.PropertyDescriptorCollection pds = (view as System.ComponentModel.ITypedList).GetItemProperties(null);
                        object o = view.CreateItemViewByObjectHandle(handle);
                        for (int i = 0; i < columns.Length; i++)
                        {
                            string column = columns[i];
                            System.ComponentModel.PropertyDescriptor pd = pds[column];
							Cerebrum.ObjectHandle fieldHable = Cerebrum.ObjectHandle.Null;
							if (pd is Cerebrum.Data.AttributePropertyDescriptor)
							{
								fieldHable = (pd as Cerebrum.Data.AttributePropertyDescriptor).AttributePlasmaHandle;
							}
                            values[i] = new Cerebrum.Ganglion.Remoting.ValueDef(fieldHable, pd.GetValue(o));
                        }
                    }
                    return values;
                }
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                throw ControllerImplementation.BuildPublicException(ex);
            }
        }
        #region IDisposable Members

        public void Dispose()
        {
            this.Shutdown();
            m_ContextFactory = null;
        }

        #endregion

        #region IDatabaseController Members


        public ObjectHandle ResolveHandle(string tableName, string fieldName, object value)
        {
            try
            {
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    Cerebrum.Integrator.DomainContext domain = this.DomainContext;
                    return Cerebrum.Management.Utilites.ResolveHandle(domain, tableName, fieldName, value);
                }
            }
            catch (Cerebrum.Runtime.NativeException ex)
            {
                throw ControllerImplementation.BuildPublicException(ex);
            }
        }

        #endregion
    }
}
