// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble
{
	/// <summary>
	/// Summary description for SyncronizedWorkspace.
	/// </summary>
	public class SyncronizedWorkspace : System.MarshalByRefObject, Cerebrum.IWorkspace, Cerebrum.IConnector
	{
		private Cerebrum.IWorkspace m_Workspace;

		public SyncronizedWorkspace(Cerebrum.IWorkspace workspace)
		{
			m_Workspace = workspace;
		}
		#region IConnector Members

		public object Component
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Workspace.Component;
				}
			}
			set
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					m_Workspace.Component = value;
				}
			}
		}

		public Cerebrum.IWorkspace Workspace
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return new SyncronizedWorkspace(m_Workspace.Workspace);
				}
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			// TODO:  Add SyncronizedWorkspace.Dispose implementation
		}

		#endregion

		#region IContainer Members

		public void EngageConnector(Cerebrum.ObjectHandle plasma, Cerebrum.IConnector connector)
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				m_Workspace.EngageConnector(plasma, connector);
			}
		}

		public Cerebrum.IComposite AttachConnector(Cerebrum.ObjectHandle plasma)
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				return new SyncronizedComposite(m_Workspace.AttachConnector(plasma));
			}
		}

		public void RemoveConnector(Cerebrum.ObjectHandle plasma)
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				m_Workspace.RemoveConnector(plasma);
			}
		}

		public Cerebrum.IComposite CreateConnector(Cerebrum.ObjectHandle plasma, Cerebrum.ObjectHandle typeID)
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				return new SyncronizedComposite(this.m_Workspace.CreateConnector(plasma, typeID));
			}
		}

		#endregion

		#region IDisposable Members

		public event System.EventHandler Disposing
		{
			add
			{
			}
			remove
			{
			}
		}

		#endregion

		#region IWorkspace Members

		public Cerebrum.ObjectHandle NextSequence()
		{
            using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
            {
                return this.m_Workspace.NextSequence();
            }
		}

		public Cerebrum.ISavepoints Savepoints
		{
			get
			{
				// TODO:  Add SyncronizedWorkspace.Savepoints getter implementation
				return null;
			}
		}

		public string Name
		{
			get
			{
                using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
                {
                    return this.m_Workspace.Name;
                }
            }
		}

		public Cerebrum.IActivator Activator
		{
			get
			{
				return null;
			}
			set
			{
				// TODO:  Add SyncronizedWorkspace.Activator setter implementation
			}
		}

		public Cerebrum.IFormatter Formatter
		{
			get
			{
				return null;
			}
		}

		public Cerebrum.ObjectHandle CurrSequence()
		{
            using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
            {
                return this.m_Workspace.CurrSequence();
            }
        }

		public IContainer GetSector()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}
