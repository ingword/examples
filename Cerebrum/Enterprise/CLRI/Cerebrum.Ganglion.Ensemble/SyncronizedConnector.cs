// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble
{
	/// <summary>
	/// Summary description for SyncronizedConnector.
	/// </summary>
	public class SyncronizedConnector : System.MarshalByRefObject, Cerebrum.IConnector
	{
		private Cerebrum.IConnector m_Connector;
		public SyncronizedConnector(Cerebrum.IConnector connector)
		{
			m_Connector = connector;
		}
		#region IConnector Members

		public object Component
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Connector.Component;
				}
			}
			set
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					m_Connector.Component = value;
				}
			}
		}

		public Cerebrum.IWorkspace Workspace
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return new SyncronizedWorkspace(m_Connector.Workspace);
				}
			}
		}

		#endregion

		#region IDisposable Members

		public event System.EventHandler Disposing
		{
			add
			{
			}
			remove
			{
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				m_Connector.Dispose();
			}
		}

		#endregion
	}
}
