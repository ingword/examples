// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Ensemble
{
	/// <summary>
	/// Summary description for SyncronizedContainer.
	/// </summary>
	public class SyncronizedContainer : System.MarshalByRefObject, Cerebrum.IContainer, Cerebrum.IComposite
	{
		private Cerebrum.IContainer m_Container;
		private Cerebrum.IComposite m_Composite;
		public SyncronizedContainer(Cerebrum.IContainer container)
		{
			m_Container = container;
			m_Composite = (Cerebrum.IComposite)container;
		}
		#region IContainer Members

		public void EngageConnector(Cerebrum.ObjectHandle plasma, Cerebrum.IConnector connector)
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				m_Container.EngageConnector(plasma, connector);
			}
		}

		public Cerebrum.IComposite AttachConnector(Cerebrum.ObjectHandle plasma)
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				return new SyncronizedComposite(m_Container.AttachConnector(plasma));
			}
		}

		public void RemoveConnector(Cerebrum.ObjectHandle plasma)
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				m_Container.RemoveConnector(plasma);
			}
		}

		public Cerebrum.IComposite CreateConnector(Cerebrum.ObjectHandle plasma, Cerebrum.ObjectHandle typeID)
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				return new SyncronizedComposite(m_Container.CreateConnector(plasma, typeID));
			}
		}

		#endregion

		#region IComposite Members

		public Cerebrum.ObjectHandle GroundHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.GroundHandle;
				}
			}
		}

		public Cerebrum.ObjectHandle LiquidHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.LiquidHandle;
				}
			}
		}

		public Cerebrum.ObjectHandle TypeIdHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.TypeIdHandle;
				}
			}
            set
            {
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					m_Composite.TypeIdHandle = value;
				}
            }
		}

		public Cerebrum.ObjectHandle KindIdHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.KindIdHandle;
				}
			}
		}

		public Cerebrum.IConnector GetPrecedent()
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				return new SyncronizedConnector(m_Composite.GetPrecedent());
			}
		}

		public Cerebrum.ObjectHandle PlasmaHandle
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.PlasmaHandle;
				}
			}
		}

		#endregion

		#region IConnector Members

		public object Component
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return m_Composite.Component;
				}
			}
			set
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					m_Composite.Component = value;
				}
			}
		}

		public Cerebrum.IWorkspace Workspace
		{
			get
			{
				using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
				{
					return new SyncronizedWorkspace(m_Composite.Workspace);
				}
			}
		}

		#endregion

		#region IDisposable Members

		public event System.EventHandler Disposing
		{
			add
			{
			}
			remove
			{
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			using (System.IDisposable sync = ControllerImplementation.CreateGlobalSyncLock())
			{
				m_Composite.Dispose();
			}
		}

		#endregion
	}
}
