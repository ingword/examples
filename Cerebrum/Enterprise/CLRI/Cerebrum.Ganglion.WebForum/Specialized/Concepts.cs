// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.WebForum.Specialized
{
	/// <summary>
	/// Summary description for Concepts.
	/// </summary>
	public class Concepts
	{
		public Concepts()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static Cerebrum.ObjectHandle ArticleTextAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleText);
		}
		public static Cerebrum.ObjectHandle CreatedDateTimeAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", Cerebrum.WebForum.Remoting.Specialized.KnownNames.CreatedDateTime);
		}
		public static Cerebrum.ObjectHandle UpdatedDateTimeAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "UpdatedDateTime");
		}
		public static Cerebrum.ObjectHandle ArticleSubjectAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleSubject);
		}
		public static Cerebrum.ObjectHandle ArticleAuthorAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleAuthor);
		}
		public static Cerebrum.ObjectHandle UserHostAddressAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", Cerebrum.WebForum.Remoting.Specialized.KnownNames.UserHostAddress);
		}
		public static Cerebrum.ObjectHandle SecurityOwnerAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", Cerebrum.WebForum.Remoting.Specialized.KnownNames.SecurityOwner);
		}
        public static Cerebrum.ObjectHandle DependentArticlesAttribute(Cerebrum.Integrator.DomainContext context)
		{
            return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", Cerebrum.WebForum.Remoting.Specialized.KnownNames.DependentArticles);
		}
        public static Cerebrum.ObjectHandle PrecedentArticlesAttribute(Cerebrum.Integrator.DomainContext context)
		{
            return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", Cerebrum.WebForum.Remoting.Specialized.KnownNames.PrecedentArticles);
		}
	}
}
