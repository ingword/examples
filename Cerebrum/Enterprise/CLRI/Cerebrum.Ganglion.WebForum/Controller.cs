// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Text;

namespace Cerebrum.Ganglion.WebForum
{
	public class Controller : Cerebrum.Integrator.GenericComponent
	{
        private void InternalCollect()
        {
            System.GC.Collect(3);
            System.GC.WaitForPendingFinalizers();
            System.GC.Collect();
            Cerebrum.Runtime.Environment env = Cerebrum.Runtime.Environment.Current;
            if (env != null)
            {
                env.Collect();
            }
        }

        public Cerebrum.ObjectHandle CreateWebMessage(Cerebrum.ObjectHandle threadHandle, string author, string subject, string text, string userhost)
		{
			System.DateTime recordDateTime = System.DateTime.Now;

			Cerebrum.Security.CerebrumPrincipal principal = System.Threading.Thread.CurrentPrincipal as Cerebrum.Security.CerebrumPrincipal;

			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;
			Cerebrum.ObjectHandle hindxId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Components", "Name", "Cerebrum.Ganglion.Indexing.Controller");
			Cerebrum.ObjectHandle hringId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", "DependentInstances");

            Cerebrum.ObjectHandle messageHandle = Cerebrum.ObjectHandle.Null;

			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, "WebArticles"))
			{
				Cerebrum.Data.ComponentItemView item = view.AddNew();
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleAuthor] = author;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleSubject] = subject;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleText] = text;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.UserHostAddress] = userhost;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.CreatedDateTime] = recordDateTime;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime] = recordDateTime;
				if (principal != null)
				{
					item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.SecurityOwner] = (principal.Identity as Cerebrum.Security.CerebrumIdentity).PrincipalHandle;
				}
				System.ComponentModel.PropertyDescriptor pdRep = item.GetProperties()["UniqueIdentity"];
				ReIndexReplicationInternal(domainContext, hindxId, hringId, pdRep, item);

				if (threadHandle != Cerebrum.ObjectHandle.Null)
				{
					Cerebrum.Data.ComponentItemView parent = view.CreateItemViewByObjectHandle(threadHandle);

					object threadDateTime = parent[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime];
					if ((!(threadDateTime is System.DateTime)) || (Convert.ToDateTime(threadDateTime) < recordDateTime))
					{
						parent[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime] = recordDateTime;
					}

					System.ComponentModel.PropertyDescriptor pdView = view.GetItemProperties(null)["DependentArticles"];

					object v = pdView.GetValue(parent);
					if (v is Cerebrum.Data.SimpleView)
						using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)v)
						{
							using (Cerebrum.IComposite composite = item.GetComposite())
							{
								view2.EngageConnector(item.ObjectHandle, composite);
							}
							using (Cerebrum.Data.SimpleView view3 = (Cerebrum.Data.SimpleView)item["PrecedentArticles"])
							{
								using (Cerebrum.IComposite cp = parent.GetComposite())
								{
									view3.EngageConnector(parent.ObjectHandle, cp);
								}
							}
						}
				}
				messageHandle = item.ObjectHandle;
			}

            InternalCollect();
            return messageHandle;
		}

		public Cerebrum.ObjectHandle CreateWebContent(Cerebrum.ObjectHandle threadHandle, string author, string subject, string name, string path, string userhost)
		{
			System.DateTime recordDateTime = System.DateTime.Now;

			Cerebrum.Security.CerebrumPrincipal principal = System.Threading.Thread.CurrentPrincipal as Cerebrum.Security.CerebrumPrincipal;

			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;
			Cerebrum.ObjectHandle hindxId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Components", "Name", "Cerebrum.Ganglion.Indexing.Controller");
			Cerebrum.ObjectHandle hringId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", "DependentInstances");
			Cerebrum.ObjectHandle typeIdHandle = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Types", "Name", "WebContent");

            Cerebrum.ObjectHandle contentHandle = Cerebrum.ObjectHandle.Null;

            using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebArticles))
			{
				Cerebrum.Data.ComponentItemView item = view.AddNew(domainContext.NextSequence(), typeIdHandle);
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleAuthor] = author;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleSubject] = subject;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.ContentName] = name;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticlePath] = path;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.UserHostAddress] = userhost;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.CreatedDateTime] = recordDateTime;
				item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime] = recordDateTime;
				if (principal != null)
				{
					item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.SecurityOwner] = (principal.Identity as Cerebrum.Security.CerebrumIdentity).PrincipalHandle;
				}
				System.ComponentModel.PropertyDescriptor pdRep = item.GetProperties()["UniqueIdentity"];
				ReIndexReplicationInternal(domainContext, hindxId, hringId, pdRep, item);

				if (threadHandle != Cerebrum.ObjectHandle.Null)
				{
					Cerebrum.Data.ComponentItemView parent = view.CreateItemViewByObjectHandle(threadHandle);

					object threadDateTime = parent[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime];
					if ((!(threadDateTime is System.DateTime)) || (Convert.ToDateTime(threadDateTime) < recordDateTime))
					{
						parent[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime] = recordDateTime;
					}
					
					System.ComponentModel.PropertyDescriptor pdView = view.GetItemProperties(null)["DependentArticles"];

					object v = pdView.GetValue(parent);
					if (v is Cerebrum.Data.SimpleView)
						using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)v)
						{
							using (Cerebrum.IComposite composite = item.GetComposite())
							{
								view2.EngageConnector(item.ObjectHandle, composite);
							}
							using (Cerebrum.Data.SimpleView view3 = (Cerebrum.Data.SimpleView)item["PrecedentArticles"])
							{
								using (Cerebrum.IComposite cp = parent.GetComposite())
								{
									view3.EngageConnector(parent.ObjectHandle, cp);
								}
							}
						}
				}
				contentHandle = item.ObjectHandle;
			}
            InternalCollect();
            return contentHandle;
		}


        public void DeleteWebElement(Cerebrum.ObjectHandle recordHandle)
		{
			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;
			Cerebrum.ObjectHandle hindxId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Components", "Name", "Cerebrum.Ganglion.Indexing.Controller");
			Cerebrum.ObjectHandle hringId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", "DependentInstances");

			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, "WebArticles"))
			{
				Cerebrum.Data.ComponentItemView recordItem = view.CreateItemViewByObjectHandle(recordHandle);
				string uniqueIdentity = Convert.ToString(recordItem["UniqueIdentity"]);
				Cerebrum.Ganglion.Ensemble.Tools.InvokeRecord(domainContext, "Components", hindxId, "RemoveIndex", new object[] { hringId, uniqueIdentity, recordHandle });

				object v2;

				v2 = recordItem["PrecedentArticles"];
				if (v2 is Cerebrum.Data.SimpleView)
					using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)v2)
					{
						foreach (Cerebrum.Data.ComponentItemView threadItem in view2)
						{
							object v3 = threadItem["DependentArticles"];
							if (v3 is Cerebrum.Data.SimpleView)
								using (Cerebrum.Data.SimpleView view3 = (Cerebrum.Data.SimpleView)v3)
								{
									view3.Remove(view3.CreateItemViewByObjectHandle(recordHandle));
								}

						}
					}
				v2 = recordItem["DependentArticles"];
				if (v2 is Cerebrum.Data.SimpleView)
					using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)v2)
					{
						foreach (Cerebrum.Data.ComponentItemView threadItem in view2)
						{
							object v3 = threadItem["PrecedentArticles"];
							if (v3 is Cerebrum.Data.SimpleView)
								using (Cerebrum.Data.SimpleView view3 = (Cerebrum.Data.SimpleView)v3)
								{
									view3.Remove(view3.CreateItemViewByObjectHandle(recordHandle));
								}

						}
					}
				view.Remove(recordItem);
			}
			domainContext.Workspace.RemoveConnector(recordHandle);

            InternalCollect();
        }

        public void EngageWebElement(Cerebrum.ObjectHandle threadHandle, Cerebrum.ObjectHandle recordHandle)
		{
			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, "WebArticles"))
			{
				object v;
				Cerebrum.Data.ComponentItemView threadItem = view.CreateItemViewByObjectHandle(threadHandle);
				Cerebrum.Data.ComponentItemView recordItem = view.CreateItemViewByObjectHandle(recordHandle);

				object threadDateTime = threadItem[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime];
				object recordDateTime = recordItem[Cerebrum.WebForum.Remoting.Specialized.KnownNames.CreatedDateTime];

				if ((recordDateTime is System.DateTime) && ((!(threadDateTime is System.DateTime)) || (Convert.ToDateTime(threadDateTime) < Convert.ToDateTime(recordDateTime))))
				{
					threadItem[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime] = recordDateTime;
				}

				v = threadItem["DependentArticles"];
				if (v is Cerebrum.Data.SimpleView)
					using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)v)
					{
						using(Cerebrum.IComposite c3 = recordItem.GetComposite())
						{
							view2.EngageConnector(recordHandle, c3);
						}
					}

				v = recordItem["PrecedentArticles"];
				if (v is Cerebrum.Data.SimpleView)
					using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)v)
					{
						using (Cerebrum.IComposite c3 = threadItem.GetComposite())
						{
							view2.EngageConnector(threadHandle, c3);
						}
					}
			}

            InternalCollect();
		}

        public void RemoveWebElement(Cerebrum.ObjectHandle threadHandle, Cerebrum.ObjectHandle recordHandle)
		{
			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, "WebArticles"))
			{
				object v;
				Cerebrum.Data.ComponentItemView threadItem = view.CreateItemViewByObjectHandle(threadHandle);
				Cerebrum.Data.ComponentItemView recordItem = view.CreateItemViewByObjectHandle(recordHandle);

				v = threadItem["DependentArticles"];
				if (v is Cerebrum.Data.SimpleView)
					using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)v)
					{
						view2.Remove(view2.CreateItemViewByObjectHandle(recordHandle));
					}

				v = recordItem["PrecedentArticles"];
				if (v is Cerebrum.Data.SimpleView)
					using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)v)
					{
						view2.Remove(view2.CreateItemViewByObjectHandle(threadHandle));
					}
			}

            InternalCollect();
        }

        public Cerebrum.Ganglion.Remoting.TableDef[] SelectWebElement(Cerebrum.ObjectHandle recordHandle, string[] fields)
        {
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, true);
            database.SetClean();


            Cerebrum.Ganglion.Remoting.IRemoteDatabase ctl = new Cerebrum.Ganglion.Ensemble.RemoteDatabase();

            //Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

            Cerebrum.Ganglion.Remoting.QueryDef queryDef;

            Cerebrum.Ganglion.Remoting.QueryDef qdf1 = ctl.BuildQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "WebArticles", fields, null);
            Cerebrum.Ganglion.Remoting.QueryDef qdf2 = ctl.BuildQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "Principals", new string[] { "ObjectHandle", "UniqueIdentity", "DisplayName" }, null);
            Cerebrum.Ganglion.Remoting.BoundDef[] bdfsU = new Cerebrum.Ganglion.Remoting.BoundDef[ qdf1.Expression[0].Expression.Length + qdf2.Expression[0].Expression.Length + 2];
            qdf1.Expression[0].Expression.CopyTo(bdfsU, 0);
            qdf2.Expression[0].Expression.CopyTo(bdfsU, qdf1.Expression[0].Expression.Length);
            Cerebrum.Ganglion.Remoting.FieldDef fldOH0 = Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf1.TableDefs[0], "ObjectHandle");
            bdfsU[bdfsU.Length - 2] = new Cerebrum.Ganglion.Remoting.CompareBoundDef(qdf1.TableDefs[0], fldOH0, new Cerebrum.Ganglion.Remoting.ValueDef(fldOH0.ObjectHandle, recordHandle), Cerebrum.Ganglion.Remoting.LogicAction.Equal);
            bdfsU[bdfsU.Length - 1] = new Cerebrum.Ganglion.Remoting.FieldJoinBoundDef(qdf2.TableDefs[0], Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf1.TableDefs[0], "SecurityOwner"), Cerebrum.Ganglion.Remoting.FieldJoinType.ValueDef);

            queryDef = new Cerebrum.Ganglion.Remoting.QueryDef(new Cerebrum.Ganglion.Remoting.TableDef[] { qdf1.TableDefs[0], qdf2.TableDefs[0] }, new Cerebrum.Ganglion.Remoting.LimitDef[] { new Cerebrum.Ganglion.Remoting.LimitDef(bdfsU) }, Cerebrum.Ganglion.Remoting.QueryType.Select);

            Cerebrum.Ganglion.Remoting.TableDef[] queryRes = ctl.ExecuteQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, queryDef);

            return queryRes;
        }


        public Cerebrum.WebForum.Remoting.WebArticlesResult SelectWebElements(Cerebrum.ObjectHandle threadHandle, string[] fields, string sortMode, string pageMode, int pageSize, int currPage, Cerebrum.ObjectHandle recordHandle, Cerebrum.ObjectHandle authorHandle)
		{
			Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
			Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, true);
			database.SetClean();

			if (currPage < 0 && (sortMode==null || sortMode==string.Empty))
			{
				if (pageMode == "f")
				{
					sortMode = "d";
				}
				else
				{
					sortMode = "a";
				}
			}

			Cerebrum.Ganglion.Remoting.IRemoteDatabase ctl = new Cerebrum.Ganglion.Ensemble.RemoteDatabase();

			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;
			Cerebrum.ObjectHandle contentTypeIdHandle = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Types", "Name", "WebContent");
			Cerebrum.ObjectHandle messageTypeIdHandle = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Types", "Name", "WebMessage");

			Cerebrum.Ganglion.Remoting.TableDef textTable;
			Cerebrum.Ganglion.Remoting.TableDef msgsTable;
			Cerebrum.Ganglion.Remoting.TableDef lnksTable;
			string orderDirection = (sortMode != "d") ? "+" : "-";
			string orderAttribute;
			if (threadHandle == Cerebrum.ObjectHandle.Null)
			{
				orderAttribute = Cerebrum.WebForum.Remoting.Specialized.KnownNames.CreatedDateTime;
			}
			else
			{
				orderAttribute = Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime;
			}
			string[] orderDef = new string[] { orderAttribute + orderDirection };
			Cerebrum.Ganglion.Remoting.QueryDef queryDef;
			if (threadHandle == Cerebrum.ObjectHandle.Null)
			{
                int an = 0;
                //if (authorHandle != Cerebrum.ObjectHandle.Null)
                //{
                    an++;
                //}
				Cerebrum.Ganglion.Remoting.QueryDef qdf0 = ctl.BuildQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "WebArticles", fields, orderDef);
				Cerebrum.Ganglion.Remoting.QueryDef qdf2 = ctl.BuildQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "Principals", new string[] { "ObjectHandle", "UniqueIdentity", "DisplayName" }, null);
				Cerebrum.Ganglion.Remoting.BoundDef[] bdfsU = new Cerebrum.Ganglion.Remoting.BoundDef[qdf0.Expression[0].Expression.Length + qdf2.Expression[0].Expression.Length + 1 + an];
                int offset = 0;
                qdf0.Expression[0].Expression.CopyTo(bdfsU, offset);
                offset += qdf0.Expression[0].Expression.Length;
				qdf2.Expression[0].Expression.CopyTo(bdfsU, offset);
                offset += qdf2.Expression[0].Expression.Length;
                bdfsU[offset] = new Cerebrum.Ganglion.Remoting.FieldJoinBoundDef(qdf2.TableDefs[0], Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf0.TableDefs[0], "SecurityOwner"), Cerebrum.Ganglion.Remoting.FieldJoinType.ValueDef);
                offset++;
                if (authorHandle != Cerebrum.ObjectHandle.Null)
                {
                    Cerebrum.Ganglion.Remoting.FieldDef fldSO = Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf0.TableDefs[0], "SecurityOwner");
                    bdfsU[offset] = new Cerebrum.Ganglion.Remoting.CompareBoundDef(qdf0.TableDefs[0], fldSO, new Cerebrum.Ganglion.Remoting.ValueDef(fldSO.ObjectHandle, authorHandle), Cerebrum.Ganglion.Remoting.LogicAction.Equal);
                }
                else
                {
                    Cerebrum.Ganglion.Remoting.FieldDef fldTH = Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf0.TableDefs[0], "TypeIdHandle");
                    bdfsU[offset] = new Cerebrum.Ganglion.Remoting.CompareBoundDef(qdf0.TableDefs[0], fldTH, new Cerebrum.Ganglion.Remoting.ValueDef(fldTH.ObjectHandle, messageTypeIdHandle), Cerebrum.Ganglion.Remoting.LogicAction.Equal);
                }

				queryDef = new Cerebrum.Ganglion.Remoting.QueryDef(new Cerebrum.Ganglion.Remoting.TableDef[] { qdf0.TableDefs[0], qdf2.TableDefs[0] }, new Cerebrum.Ganglion.Remoting.LimitDef[] { new Cerebrum.Ganglion.Remoting.LimitDef(bdfsU) }, Cerebrum.Ganglion.Remoting.QueryType.Select);

				msgsTable = ctl.ExecuteQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, queryDef)[0];
				lnksTable = null;
				textTable = null;
			}
			else
			{
                Cerebrum.Ganglion.Remoting.QueryDef qdf0 = ctl.BuildQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "WebArticles", new string[] { "ObjectHandle", "DependentArticles", "PrecedentArticles", Cerebrum.WebForum.Remoting.Specialized.KnownNames.UniqueIdentity, Cerebrum.WebForum.Remoting.Specialized.KnownNames.SecurityOwner, Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleAuthor, Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleSubject, Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleText, Cerebrum.WebForum.Remoting.Specialized.KnownNames.CreatedDateTime, Cerebrum.WebForum.Remoting.Specialized.KnownNames.UpdatedDateTime }, null);
				Cerebrum.Ganglion.Remoting.QueryDef qdf1 = ctl.BuildQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "WebArticles", fields, orderDef);
				Cerebrum.Ganglion.Remoting.QueryDef qdf2 = ctl.BuildQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "Principals", new string[] { "ObjectHandle", "UniqueIdentity", "DisplayName" }, null);
				Cerebrum.Ganglion.Remoting.QueryDef qdf3 = ctl.BuildQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "WebArticles", new string[] { "ObjectHandle", Cerebrum.WebForum.Remoting.Specialized.KnownNames.UniqueIdentity, Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleSubject }, orderDef);
				Cerebrum.Ganglion.Remoting.BoundDef[] bdfsU = new Cerebrum.Ganglion.Remoting.BoundDef[qdf0.Expression[0].Expression.Length + qdf1.Expression[0].Expression.Length + qdf2.Expression[0].Expression.Length + qdf3.Expression[0].Expression.Length + 5];
				qdf0.Expression[0].Expression.CopyTo(bdfsU, 0);
				qdf1.Expression[0].Expression.CopyTo(bdfsU, qdf0.Expression[0].Expression.Length);
				qdf2.Expression[0].Expression.CopyTo(bdfsU, qdf0.Expression[0].Expression.Length + qdf1.Expression[0].Expression.Length);
				qdf3.Expression[0].Expression.CopyTo(bdfsU, qdf0.Expression[0].Expression.Length + qdf1.Expression[0].Expression.Length + qdf2.Expression[0].Expression.Length);
				Cerebrum.Ganglion.Remoting.FieldDef fldOH0 = Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf0.TableDefs[0], "ObjectHandle");
				bdfsU[bdfsU.Length - 5] = new Cerebrum.Ganglion.Remoting.CompareBoundDef(qdf0.TableDefs[0], fldOH0, new Cerebrum.Ganglion.Remoting.ValueDef(fldOH0.ObjectHandle, threadHandle), Cerebrum.Ganglion.Remoting.LogicAction.Equal);
				bdfsU[bdfsU.Length - 4] = new Cerebrum.Ganglion.Remoting.FieldJoinBoundDef(qdf1.TableDefs[0], Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf0.TableDefs[0], "DependentArticles"), Cerebrum.Ganglion.Remoting.FieldJoinType.TableDef);
				bdfsU[bdfsU.Length - 3] = new Cerebrum.Ganglion.Remoting.FieldJoinBoundDef(qdf2.TableDefs[0], Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf0.TableDefs[0], "SecurityOwner"), Cerebrum.Ganglion.Remoting.FieldJoinType.ValueDef);
				bdfsU[bdfsU.Length - 2] = new Cerebrum.Ganglion.Remoting.FieldJoinBoundDef(qdf2.TableDefs[0], Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf1.TableDefs[0], "SecurityOwner"), Cerebrum.Ganglion.Remoting.FieldJoinType.ValueDef);
				bdfsU[bdfsU.Length - 1] = new Cerebrum.Ganglion.Remoting.FieldJoinBoundDef(qdf3.TableDefs[0], Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qdf0.TableDefs[0], "PrecedentArticles"), Cerebrum.Ganglion.Remoting.FieldJoinType.TableDef);

				queryDef = new Cerebrum.Ganglion.Remoting.QueryDef(new Cerebrum.Ganglion.Remoting.TableDef[] { qdf0.TableDefs[0], qdf1.TableDefs[0], qdf2.TableDefs[0], qdf3.TableDefs[0] }, new Cerebrum.Ganglion.Remoting.LimitDef[] { new Cerebrum.Ganglion.Remoting.LimitDef(bdfsU) }, Cerebrum.Ganglion.Remoting.QueryType.Select);

				Cerebrum.Ganglion.Remoting.TableDef [] queryRes = ctl.ExecuteQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, queryDef);
				textTable = queryRes[0];
				msgsTable = queryRes[1];
				lnksTable = queryRes[3];
			}

			Cerebrum.Ganglion.Remoting.TupleDef[] tuples = msgsTable.Tuples;

			if (pageSize < 0)
			{
				pageSize = 10;
			}

			if (currPage < 0)
			{
				if (pageMode == "f")
				{
					currPage = 0;
				}
				else
				{
					if (tuples != null)
					{
						if (sortMode != "d")
						{
							int pages = (tuples.Length + pageSize - 1) / pageSize;
							currPage = pages - 1;
						}
						else
						{
							currPage = 0;
						}
					}
					else
					{
						currPage = 0;
					}
				}
			}

			if (currPage < 0)
			{
				currPage = 0;
			}
			if (recordHandle != Cerebrum.ObjectHandle.Null)
			{
				Cerebrum.ObjectHandle hObjectHandle = Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(msgsTable, "ObjectHandle").ObjectHandle;
				int ordinal = -1;
				for (int i = 0; i < tuples.Length; i++)
				{
					Cerebrum.Ganglion.Remoting.TupleDef tuple = tuples[i];
					if (recordHandle.Equals(Cerebrum.Ganglion.Remoting.Utilites.FindValueDef(tuple, hObjectHandle).Value))
					{
						ordinal = i;
						break;
					}
				}
				if (ordinal < 0)
				{
					if (currPage < 0)
					{
						currPage = 0;
					}
				}
				else
				{
					currPage = ordinal / pageSize;
				}
			}
			System.Collections.ArrayList list = new System.Collections.ArrayList();
			int TotalCount = 0;
			if (tuples != null)
			{
				TotalCount = tuples.Length;
				for (int i = currPage * pageSize; i < tuples.Length && i < ((currPage + 1) * pageSize); i++)
				{
					list.Add(tuples[i]);
				}
			}
			msgsTable.Tuples = (Cerebrum.Ganglion.Remoting.TupleDef[])list.ToArray(typeof(Cerebrum.Ganglion.Remoting.TupleDef));

			/*Cerebrum.ObjectHandle hSecurityOwner = Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(msgsTable, "SecurityOwner").ObjectHandle;
			Cerebrum.Collections.Specialized.HandleObjectDictionary principals = new Cerebrum.Collections.Specialized.HandleObjectDictionary();
			if (msgsTable.Tuples != null)
				for (int i = 0; i < msgsTable.Tuples.Length; i++)
				{
					Cerebrum.Ganglion.Remoting.TupleDef tuple = msgsTable.Tuples[i];
					object oh = Cerebrum.Ganglion.Remoting.Utilites.FindValueDef(tuple, hSecurityOwner).Value;
					if (oh != null && !Convert.IsDBNull(oh))
					{
						Cerebrum.ObjectHandle ownerHandle = (Cerebrum.ObjectHandle)oh;
						if (principals[ownerHandle] == null)
						{
                            Cerebrum.Ganglion.Remoting.ValueDef[] values = ctl.SelectRecord(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "Principals", ownerHandle, new string[] { "ObjectHandle", "UniqueIdentity", "DisplayName" });
                            principals[ownerHandle] = new Cerebrum.Ganglion.Remoting.TupleDef((Cerebrum.ObjectHandle)values[0].Value, values);
						}
						Cerebrum.Ganglion.Remoting.Utilites.FindValueDef(tuple, hSecurityOwner).Value = principals[ownerHandle];
					}
				}*/
			Cerebrum.WebForum.Remoting.WebArticlesResult wmr = new Cerebrum.WebForum.Remoting.WebArticlesResult();
			wmr.MesgsTable = msgsTable;
			wmr.LinksTable = lnksTable;
			wmr.TitleTable = textTable;
			wmr.TotalCount = TotalCount;
			wmr.PageSize = pageSize;
			wmr.CurrentPage = currPage;
			wmr.ContentTypeIdHandle = contentTypeIdHandle;
			return wmr;
		}

		public Cerebrum.ObjectHandle CreatePrincipal(string loginName, string password, string fullName, string description, string email, string homePageURL)
		{
			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;
			Cerebrum.ObjectHandle hindxId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Components", "Name", "Cerebrum.Ganglion.Indexing.Controller");
			Cerebrum.ObjectHandle hringId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", "DependentInstances");

			Cerebrum.ObjectHandle huserId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Principals", "Name", loginName);
			if (huserId != Cerebrum.ObjectHandle.Null)
			{
				throw new System.Security.SecurityException("User with Name = '" + loginName + "' already exists");
			}
            Cerebrum.ObjectHandle principalHandle = Cerebrum.ObjectHandle.Null;

            using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, "Principals"))
			{
				Cerebrum.Data.ComponentItemView item = view.AddNew();
				using (Cerebrum.IConnector connector = item.GetComposite())
				{
					Cerebrum.Management.Security.Reflection.PrincipalDescriptor prd = connector.Component as Cerebrum.Management.Security.Reflection.PrincipalDescriptor;
					prd.Name = loginName;
					prd.Password = password;
					prd.DisplayName = fullName;
					prd.Description = description;
					prd.EmailAddress = email;
					prd.HomePageURL = homePageURL;
					//prd.Disabled = false;
					//prd.Internal = false;
					//prd.CreatedDateTime = System.DateTime.Now;
				}

				System.ComponentModel.PropertyDescriptor pdUID = item.GetProperties()["UniqueIdentity"];
				ReIndexReplicationInternal(domainContext, hindxId, hringId, pdUID, item);

				Cerebrum.Management.Utilites.RefreshHandle(domainContext, "Principals", "Name", item.ObjectHandle);

				principalHandle = item.ObjectHandle;
			}
            InternalCollect();
            return principalHandle;
		}

		public void DeletePrincipal(Cerebrum.ObjectHandle recordHandle)
		{
			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;
			Cerebrum.ObjectHandle hindxId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Components", "Name", "Cerebrum.Ganglion.Indexing.Controller");
			Cerebrum.ObjectHandle hringId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", "DependentInstances");

			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, "Principals"))
			{
				Cerebrum.Data.ComponentItemView recordItem = view.CreateItemViewByObjectHandle(recordHandle);
				string uniqueIdentity = Convert.ToString(recordItem["UniqueIdentity"]);
				Cerebrum.Ganglion.Ensemble.Tools.InvokeRecord(domainContext, "Components", hindxId, "RemoveIndex", new object[] { hringId, uniqueIdentity, recordHandle });

				view.Remove(recordItem);
			}
			domainContext.Workspace.RemoveConnector(recordHandle);
		}

		public Cerebrum.Security.CerebrumPrincipal Authenticate(string userName, string password)
		{
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, true);
            database.SetClean();

            Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

            Cerebrum.Security.CerebrumPrincipal cp = Cerebrum.Management.Utilites.Authenticate(domainContext, userName, password);
			if (cp == null)
			{
				return null;
			}
			Cerebrum.Ganglion.Remoting.IRemoteDatabase ctl = new Cerebrum.Ganglion.Ensemble.RemoteDatabase();
			string uniqueIdentity = Convert.ToString(ctl.SelectRecord(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "Principals", (cp.Identity as Cerebrum.Security.CerebrumIdentity).PrincipalHandle, new string[] { "UniqueIdentity" })[0].Value);
			return new Cerebrum.WebForum.Remoting.CerebrumPrincipalEx(cp, uniqueIdentity);
		}

		public Cerebrum.Security.CerebrumPrincipal LoadPrincipal(Cerebrum.ObjectHandle principalHandle)
		{
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, true);
            database.SetClean();

            Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

            Cerebrum.Security.CerebrumPrincipal cp = Cerebrum.Management.Utilites.LoadPrincipal(domainContext, principalHandle, true);
			if (cp == null)
			{
				return null;
			}
			Cerebrum.Ganglion.Remoting.IRemoteDatabase ctl = new Cerebrum.Ganglion.Ensemble.RemoteDatabase();
			string uniqueIdentity = Convert.ToString(ctl.SelectRecord(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "Principals", principalHandle, new string[] { "UniqueIdentity" })[0].Value);
			return new Cerebrum.WebForum.Remoting.CerebrumPrincipalEx(cp, uniqueIdentity);
		}

		public string[] RewritePath(string resourceIdentity, Cerebrum.WebForum.Remoting.ISAPIExtensionEnum resourceExtension, string argumentIdentity)
		{
			Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
			Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, true);
			database.SetClean();

			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

			Cerebrum.ObjectHandle hindxId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Components", "Name", "Cerebrum.Ganglion.Indexing.Controller");
			Cerebrum.ObjectHandle hringId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", "DependentInstances");

			Cerebrum.ObjectHandle[] hs1 = (Cerebrum.ObjectHandle[])Cerebrum.Ganglion.Ensemble.Tools.InvokeRecord(domainContext, "Components", hindxId, "ObtainIndex", new object[] { hringId, resourceIdentity });
			Cerebrum.ObjectHandle[] hs2 = null;

			if (hs1 != null && hs1.Length > 0)
			{
				if (argumentIdentity!=null && argumentIdentity!=string.Empty)
				{
					hs2 = (Cerebrum.ObjectHandle[])Cerebrum.Ganglion.Ensemble.Tools.InvokeRecord(domainContext, "Components", hindxId, "ObtainIndex", new object[] { hringId, argumentIdentity });
				}

				string p = string.Empty;
				string f = string.Empty;
#warning " Refactor to use RequestStateBase "
				Cerebrum.ObjectHandle h = hs1[0];
				Cerebrum.ObjectHandle v = Cerebrum.ObjectHandle.Null;
				if (hs2 != null && hs2.Length > 0)
				{
					v = hs2[0];
				}
				using (Cerebrum.IComposite connector = domainContext.AttachConnector(h))
				{
					string typeName = string.Empty;
					using (Cerebrum.IComposite typecon = domainContext.AttachConnector(connector.TypeIdHandle))
					{
						typeName = (typecon.Component as Cerebrum.Reflection.TypeDescriptor).Name;
					}
					object component = connector.Component;
					if (component is Cerebrum.Management.Security.Reflection.PrincipalDescriptor)
					{
						if (v != Cerebrum.ObjectHandle.Null)
						{
							p = "&w=" + v.ToString();
						}
						return new string[] { "~/UserProfile.aspx", "u=u&c=v&h=" + Convert.ToString(h) + p };
					}
					else if (component is Cerebrum.Management.Security.Reflection.WorkgroupDescriptor)
					{
						if (v != Cerebrum.ObjectHandle.Null)
						{
							p = "&v=" + v.ToString();
						}
						return new string[] { "~/RoleProfile.aspx", "u=w&c=v&h=" + Convert.ToString(h) + p };
					}
					else if (typeName == Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebMessage)
					{
                        if (resourceExtension == Cerebrum.WebForum.Remoting.ISAPIExtensionEnum.Content)
                        {
                            return new string[] { "~/ArticleData.ashx", "h=" + Convert.ToString(h) };
                        }
                        else
                        {
                            if (v != Cerebrum.ObjectHandle.Null)
                            {
                                p = "&h=" + v.ToString();
                            }
                            object fmt = null;
                            if (connector is Cerebrum.IContainer)
                            {
                                fmt = Cerebrum.Integrator.Utilites.ObtainComponent(connector as Cerebrum.IContainer, Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", Cerebrum.WebForum.Remoting.Specialized.KnownNames.ArticleFormat));
                            }
                            if (fmt is string)
                            {
                                f = "&" + (string)fmt;
                            }
                            return new string[] { "~/Record.aspx", "u=m&c=v&t=" + Convert.ToString(h) + p + f };
                        }
					}
					else if (typeName == Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebContent)
					{
						if (v != Cerebrum.ObjectHandle.Null)
						{
							p = "&a=" + v.ToString();
						}
						if (resourceExtension == Cerebrum.WebForum.Remoting.ISAPIExtensionEnum.Content)
						{
							return new string[] { "~/ContentData.ashx", "h=" + Convert.ToString(h) + p };
						}
						else
						{
							return new string[] { "~/ContentEdit.aspx", "u=c&c=v&h=" + Convert.ToString(h) + p };
						}
					}
				}
			}
			return null;
		}

		public Cerebrum.Ganglion.Remoting.TableDef SelectWorkgroups(Cerebrum.ObjectHandle principalHandle, Cerebrum.Ganglion.Remoting.QueryDef queryDef)
		{
            Cerebrum.Ganglion.Remoting.IEnsembleController ensemble = new Cerebrum.Ganglion.Ensemble.EnsembleController();
            Cerebrum.Ganglion.Remoting.IDatabaseController database = ensemble.GetDatabaseController(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, true);
            database.SetClean();

            Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

            Cerebrum.Ganglion.Remoting.IRemoteDatabase ctl = new Cerebrum.Ganglion.Ensemble.RemoteDatabase();
			Cerebrum.Ganglion.Remoting.TableDef tdf = ctl.ExecuteQuery(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, queryDef)[0];
			Cerebrum.ObjectHandle hObjectHandle = Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(tdf, "ObjectHandle").ObjectHandle;
			using (Cerebrum.IComposite connector = domainContext.AttachConnector(principalHandle))
			{
				Cerebrum.Management.Security.Reflection.PrincipalDescriptor prd = connector.Component as Cerebrum.Management.Security.Reflection.PrincipalDescriptor;
				using (Cerebrum.Runtime.NativeWarden workgroupsVector = prd.GetSelectedWorkgroupsVector())
				{
					System.Collections.ArrayList fields = new System.Collections.ArrayList(tdf.Fields);
#warning "new OH"
					Cerebrum.Ganglion.Remoting.FieldDef fldChecked = new Cerebrum.Ganglion.Remoting.FieldDef(new Cerebrum.ObjectHandle(fields.Count + 1), tdf, "Checked", typeof(bool));
					fields.Add(fldChecked);
					tdf.Fields = (Cerebrum.Ganglion.Remoting.FieldDef[])fields.ToArray(typeof(Cerebrum.Ganglion.Remoting.FieldDef));
					for (int i = 0; i < tdf.Tuples.Length; i++)
					{
						Cerebrum.Ganglion.Remoting.TupleDef tuple = tdf.Tuples[i];
						System.Collections.ArrayList values = new System.Collections.ArrayList(tuple.Values);
						if (workgroupsVector == null)
						{
							values.Add(new Cerebrum.Ganglion.Remoting.ValueDef(fldChecked.ObjectHandle, false));
						}
						else
						{
							values.Add(new Cerebrum.Ganglion.Remoting.ValueDef(fldChecked.ObjectHandle, workgroupsVector.HasMap((Cerebrum.ObjectHandle)Cerebrum.Ganglion.Remoting.Utilites.FindValueDef(tuple, hObjectHandle).Value)));
						}
                        tuple.Values = (Cerebrum.Ganglion.Remoting.ValueDef[])values.ToArray(typeof(Cerebrum.Ganglion.Remoting.ValueDef));
						tdf.Tuples[i] = tuple;
					}
				}
			}
			return tdf;
		}
		public void UpdateWorkgroups(Cerebrum.ObjectHandle principalHandle, Cerebrum.Ganglion.Remoting.TableDef tableDef)
		{
            Cerebrum.ObjectHandle hObjectHandle = Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(tableDef, "ObjectHandle").ObjectHandle;
			Cerebrum.ObjectHandle hChecked = Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(tableDef, "Checked").ObjectHandle;
			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(this.DomainContext, "Principals"))
			{
				Cerebrum.Data.ComponentItemView item = view.CreateItemViewByObjectHandle(principalHandle);
				System.ComponentModel.PropertyDescriptor pdSW = view.GetItemProperties(null)["SelectedWorkgroups"];
				using (System.IDisposable dspwrk = (System.IDisposable)pdSW.GetValue(item))
				{
					Cerebrum.Data.SimpleView list = (Cerebrum.Data.SimpleView)dspwrk;
					for (int i = 0; i < tableDef.Tuples.Length; i++)
					{
						Cerebrum.Ganglion.Remoting.TupleDef currentTuple = tableDef.Tuples[i];
						Cerebrum.ObjectHandle h = (Cerebrum.ObjectHandle)Cerebrum.Ganglion.Remoting.Utilites.FindValueDef(currentTuple, hObjectHandle).Value;
						bool newChecked = (bool)Cerebrum.Ganglion.Remoting.Utilites.FindValueDef(currentTuple, hChecked).Value;
						bool oldChecked = list.ContainsObject(h);
						if (oldChecked != newChecked)
						{
							if (newChecked)
							{
								using (Cerebrum.IComposite workgroup = this.DomainContext.AttachConnector(h))
								{
									list.EngageConnector(h, workgroup);
								}
							}
							else
							{
								list.Remove(list.CreateItemViewByObjectHandle(h));
							}
						}
					}
				}
			}
		}

		public void ReIndexDatabase()
		{
			Cerebrum.Ganglion.Remoting.IRemoteDatabase database = new Cerebrum.Ganglion.Ensemble.RemoteDatabase();

			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

			Cerebrum.ObjectHandle hindxId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Components", "Name", "Cerebrum.Ganglion.Indexing.Controller");
			Cerebrum.ObjectHandle hringId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", "DependentInstances");

			ReIndexTableView(domainContext, Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebArticles, hindxId, hringId);

			ReIndexTableView(domainContext, "Principals", hindxId, hringId);

			ReIndexTableView(domainContext, "Workgroups", hindxId, hringId);

			ReIndexTableView(domainContext, "Privileges", hindxId, hringId);

			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, "WebArticles"))
			{
				foreach (Cerebrum.Data.ComponentItemView item in view)
				{
					Cerebrum.Ganglion.Remoting.QueryRes qr = database.QueryRecord(Cerebrum.WebForum.Remoting.Specialized.KnownNames.WebForumDatabaseName, "WebArticles", "DependentArticles", item.ObjectHandle, new string[] { "ObjectHandle", "CreatedDateTime-" }, 0, 1);

					object threadDateTime = item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.CreatedDateTime];
					object actualDateTime = item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime];
					object recordDateTime = System.DBNull.Value;
					if (qr.Table != null && qr.Table.Tuples != null && qr.Table.Tuples.Length > 0)
					{
						recordDateTime = Cerebrum.Ganglion.Remoting.Utilites.FindValueDef(qr.Table.Tuples[0], Cerebrum.Ganglion.Remoting.Utilites.FindFieldDef(qr.Table, Cerebrum.WebForum.Remoting.Specialized.KnownNames.CreatedDateTime).ObjectHandle).Value;
					}
					if(!(recordDateTime is System.DateTime))
					{
						recordDateTime = threadDateTime;
					}

					if ((recordDateTime is System.DateTime) && ((!(actualDateTime is System.DateTime)) || (!(threadDateTime is System.DateTime)) || (Convert.ToDateTime(threadDateTime) < Convert.ToDateTime(recordDateTime))))
					{
						item[Cerebrum.WebForum.Remoting.Specialized.KnownNames.OrdinalDateTime] = recordDateTime;
					}
				}
			}
		}

		private void ReIndexTableView(Cerebrum.Integrator.DomainContext domainContext, string tableName, Cerebrum.ObjectHandle hindxId, Cerebrum.ObjectHandle hringId)
		{
			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, tableName))
			{
				System.ComponentModel.PropertyDescriptor pdRep = view.GetItemProperties(null)["UniqueIdentity"];

				foreach (Cerebrum.Data.ComponentItemView item in view)
				{
					ReIndexReplicationInternal(domainContext, hindxId, hringId, pdRep, item);
				}
			}
		}
		public void ReIndexContent(string tableName, Cerebrum.ObjectHandle handle)
		{
			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;
			Cerebrum.ObjectHandle hindxId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Components", "Name", "Cerebrum.Ganglion.Indexing.Controller");
			Cerebrum.ObjectHandle hringId = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", "DependentInstances");

			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, tableName))
			{
				Cerebrum.Data.ComponentItemView item = view.CreateItemViewByObjectHandle(handle);
				System.ComponentModel.PropertyDescriptor pdUID = item.GetProperties()["UniqueIdentity"];

				ReIndexReplicationInternal(domainContext, hindxId, hringId, pdUID, item);
			}
		}
		private void ReIndexReplicationInternal(Cerebrum.Integrator.DomainContext domainContext, Cerebrum.ObjectHandle hIndexer, Cerebrum.ObjectHandle hringId, System.ComponentModel.PropertyDescriptor pdUID, Cerebrum.Data.ComponentItemView item)
		{
			string uniqueIdentity = Convert.ToString(pdUID.GetValue(item));
			if (uniqueIdentity == null || uniqueIdentity.Length < 1)
			{
				uniqueIdentity = Cerebrum.Ganglion.Remoting.Utilites.NewReplicationId(false, false);
				pdUID.SetValue(item, uniqueIdentity);
			}
			else if(uniqueIdentity.Length==32)
			{
				uniqueIdentity = Cerebrum.Ganglion.Remoting.Utilites.Encode032(130, Cerebrum.Ganglion.Remoting.Utilites.Decode16(uniqueIdentity + "00"));
				pdUID.SetValue(item, uniqueIdentity);
			}
			Cerebrum.Ganglion.Ensemble.Tools.InvokeRecord(domainContext, "Components", hIndexer, "CreateIndex", new object[] { hringId, uniqueIdentity, item.ObjectHandle });
		}

        public string ExportDatabase()
        {
            Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

            System.Text.StringBuilder sb = new StringBuilder();
            using (System.IO.StringWriter sw = new System.IO.StringWriter(sb))
            {
                System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(sw);

                xw.Formatting = System.Xml.Formatting.Indented;
                Cerebrum.Management.Utilites.ExportDatabase(domainContext.Workspace, xw, false);

                xw.Close();
                sw.Close();
            }
            return sb.ToString();
        }
		/*
		/// <summary>
		/// Add all WebArticles to DependentArticles of 00000000002gs8nabsur1p7cs4
		/// </summary>
		public void ReIndexDatabase2()
		{
			Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;

			Cerebrum.ObjectHandle hparent = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "WebArticles", "UniqueIdentity", "00000000002gs8nabsur1p7cs4");
			using (Cerebrum.Data.TableView view = Cerebrum.Management.Utilites.GetView(domainContext, "WebArticles"))
			{
				Cerebrum.Data.ComponentItemView parent = view.CreateItemViewByObjectHandle(hparent);
				using (Cerebrum.Data.SimpleView view2 = (Cerebrum.Data.SimpleView)parent["DependentArticles"])
				{
					foreach (Cerebrum.Data.ComponentItemView item in view)
					{
						if (hparent != item.ObjectHandle)
						{
							using (Cerebrum.IComposite composite = item.GetComposite())
							{
								view2.Add(item.ObjectHandle, composite);
							}
							using (Cerebrum.Data.SimpleView view3 = (Cerebrum.Data.SimpleView)item["PrecedentArticles"])
							{
								using (Cerebrum.IComposite cp = parent.GetComposite())
								{
									view3.Add(parent.ObjectHandle, cp);
								}
							}
						}
					}
				}
			}
		}
		*/
	}
}
