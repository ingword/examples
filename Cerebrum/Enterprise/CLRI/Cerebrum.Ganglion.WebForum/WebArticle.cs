// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.WebForum.Reflection
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class WebArticle : Cerebrum.Integrator.GenericComponent
	{
        public WebArticle()
		{
			//
			// TODO: Add constructor logic here
			//
		}

        public string ArticleText
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.ArticleTextAttribute(this.DomainContext)));
			}
			set
			{
                SetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.ArticleTextAttribute(this.DomainContext), value);
			}
		}

        public string ArticleSubject
		{
			get
			{
                return Convert.ToString(GetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.ArticleSubjectAttribute(this.DomainContext)));
			}
			set
			{
                SetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.ArticleSubjectAttribute(this.DomainContext), value);
			}
		}

        public string ArticleAuthor
		{
			get
			{
                return Convert.ToString(GetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.ArticleAuthorAttribute(this.DomainContext)));
			}
			set
			{
                SetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.ArticleAuthorAttribute(this.DomainContext), value);
			}
		}

		public string UserHostAddress
		{
			get
			{
				return Convert.ToString(GetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.UserHostAddressAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.UserHostAddressAttribute(this.DomainContext), value);
			}
		}

		public Cerebrum.ObjectHandle SecurityOwner
		{
			get
			{
				return (Cerebrum.ObjectHandle)GetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.SecurityOwnerAttribute(this.DomainContext));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.SecurityOwnerAttribute(this.DomainContext), value);
			}
		}

		public System.DateTime CreatedDateTime
		{
			get
			{
				return Convert.ToDateTime(GetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.CreatedDateTimeAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.CreatedDateTimeAttribute(this.DomainContext), value);
			}
		}

		public System.DateTime UpdatedDateTime
		{
			get
			{
				return Convert.ToDateTime(GetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.UpdatedDateTimeAttribute(this.DomainContext)));
			}
			set
			{
				SetAttributeComponent(Cerebrum.Ganglion.WebForum.Specialized.Concepts.UpdatedDateTimeAttribute(this.DomainContext), value);
			}
		}

        protected override void SetConnector(Cerebrum.SerializeDirection direction, Cerebrum.IConnector connector)
        {
            base.SetConnector(direction, connector);
            switch (direction)
            {
                case Cerebrum.SerializeDirection.Init:
                    {
                        Cerebrum.ObjectHandle ohDependentArticlesAttribute = Cerebrum.Ganglion.WebForum.Specialized.Concepts.DependentArticlesAttribute(this.DomainContext);
                        Cerebrum.ObjectHandle ohPrecedentArticlesAttribute = Cerebrum.Ganglion.WebForum.Specialized.Concepts.PrecedentArticlesAttribute(this.DomainContext);

                        using (Cerebrum.Runtime.NativeWarden warden = (this.GetChildComponents() as Cerebrum.Runtime.NativeWarden))
                        {
                            warden.Newobj(ohDependentArticlesAttribute, Cerebrum.Runtime.KernelObjectKindId.Warden);
                            warden.Newobj(ohPrecedentArticlesAttribute, Cerebrum.Runtime.KernelObjectKindId.Warden);

                            using (Cerebrum.IComposite composite = warden.AttachConnector(ohDependentArticlesAttribute))
                            {
                                if (composite is Cerebrum.Runtime.ICompositeEx)
                                {
                                    ((Cerebrum.Runtime.ICompositeEx)composite).FastenGround();
                                }
                            }

                            using (Cerebrum.IComposite composite = warden.AttachConnector(ohPrecedentArticlesAttribute))
                            {
                                if (composite is Cerebrum.Runtime.ICompositeEx)
                                {
                                    ((Cerebrum.Runtime.ICompositeEx)composite).FastenGround();
                                }
                            }

                            warden.FastenGround();
                        }
                        break;
                    }
            }
        }
    }
}
