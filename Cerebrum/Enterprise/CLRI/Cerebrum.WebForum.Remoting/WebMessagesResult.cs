// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Text;

namespace Cerebrum.WebForum.Remoting
{
	[Serializable()]
	public class WebArticlesResult
	{
		public Cerebrum.Ganglion.Remoting.TableDef MesgsTable;
		public Cerebrum.Ganglion.Remoting.TableDef LinksTable;
		public Cerebrum.Ganglion.Remoting.TableDef TitleTable;
		public int TotalCount;
		public int PageSize;
		public int CurrentPage;
		public Cerebrum.ObjectHandle ContentTypeIdHandle;
	}
}
