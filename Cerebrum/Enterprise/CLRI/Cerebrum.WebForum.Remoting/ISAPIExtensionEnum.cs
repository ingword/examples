// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Text;

namespace Cerebrum.WebForum.Remoting
{
	public enum ISAPIExtensionEnum
	{
		Unknown,
		Default,
		Content,
	}
}
