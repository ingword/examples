// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.WebForum.Remoting
{
	[Serializable()]
	public class CerebrumPrincipalEx : Cerebrum.Security.CerebrumPrincipal
	{
		protected string m_UniqueIdentity;

		public CerebrumPrincipalEx(Cerebrum.Security.CerebrumPrincipal principal, string uniqueIdentity)
			: base((Cerebrum.Security.CerebrumIdentity)principal.Identity, principal.Workgroups, principal.Privileges)
		{
			this.m_UniqueIdentity = uniqueIdentity;
		}

		public string UniqueIdentity
		{
			get
			{
				return this.m_UniqueIdentity;
			}
		}
	}
}
