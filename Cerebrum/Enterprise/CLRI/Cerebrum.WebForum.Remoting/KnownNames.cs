// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.WebForum.Remoting.Specialized
{
	/// <summary>
	/// Summary description for KnownNames.
	/// </summary>
	public class KnownNames
	{
		public KnownNames()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static readonly string WebForumDatabaseName = "Cerebrum.Ganglion.WebForum";

		public static readonly string ArticleText = "ArticleText";
		public static readonly string ArticleData = "ArticleData";
		public static readonly string ArticlePath = "ArticlePath";
		public static readonly string ContentName = "ContentName";
		public static readonly string ArticleSubject = "ArticleSubject";
		public static readonly string ArticleAuthor = "ArticleAuthor";
		public static readonly string ArticleFormat = "ArticleFormat";
		public static readonly string UserHostAddress = "UserHostAddress";
		public static readonly string CreatedDateTime = "CreatedDateTime";
		public static readonly string UpdatedDateTime = "UpdatedDateTime";
		public static readonly string OrdinalDateTime = "OrdinalDateTime";
		public static readonly string SecurityOwner = "SecurityOwner";
		public static readonly string WebMessage = "WebMessage";
		public static readonly string WebContent = "WebContent";
		public static readonly string WebArticles = "WebArticles";
		public static readonly string UniqueIdentity = "UniqueIdentity";
		public static readonly string ObjectHandle = "ObjectHandle";
		public static readonly string TypeIdHandle = "TypeIdHandle";
		public static readonly string DependentArticles = "DependentArticles";
		public static readonly string PrecedentArticles = "PrecedentArticles";
	}
}
