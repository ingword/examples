// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
namespace Cerebrum.WebForum.Remoting
{
	/// <summary>
	/// Summary description for WebForumPrincipal
	/// </summary>
	[Serializable()]
	public class WebForumPrincipal : System.Security.Principal.IPrincipal
	{
		public WebForumPrincipal(System.Security.Principal.IIdentity identity, string userName, bool isInternal)
		{
			this.m_Identity = identity;
			this.m_UserName = userName;
			this.m_IsInternal = isInternal;
		}

		private System.Security.Principal.IIdentity m_Identity;
		private string m_UserName;
		private bool m_IsInternal;


		#region IPrincipal Members

		public System.Security.Principal.IIdentity Identity
		{
			get
			{
				return m_Identity;
			}
		}

		public bool IsInRole(string role)
		{
			if (role == "Internals")
			{
				return this.m_IsInternal;
			}
			return false;
		}

		#endregion

		public string UserName
		{
			get
			{
				return this.m_UserName;
			}
		}
	}
}