using System;
using System.Collections.Generic;
using System.Text;

namespace Cerebrum.WebForum.Remoting.ClientModel
{
    [Serializable()]
    public class Element
    {
        public string UniqueIdentity;
    }

    [Serializable()]
    public class Message : Element
    {
        public string Author;
        public string AuthorIdentity;
        public string Subject;
        public string Text;
        public string Options;
        public string MsgURL;
        public string UserHost;
        public DateTime? CreatedDate;
        public DateTime? UpdatedDate;
    }
}
