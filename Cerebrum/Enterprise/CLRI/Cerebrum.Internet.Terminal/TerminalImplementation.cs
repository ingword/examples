// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Collections;
using System.Security.Policy;
using System.Reflection;
using System.Runtime.InteropServices;
using mscoree;

namespace Cerebrum.Internet.Terminal
{
	/// <summary>
	/// Summary description for TerminalImplementation.
	/// </summary>
	public class TerminalImplementation
	{
		public static readonly object StaticLock = new object();

		public static TerminalImplementation GetInstance(System.Web.HttpContext httpContext)
		{
			return TerminalImplementation.GetInstance(httpContext, false);
		}
		public static TerminalImplementation GetInstance(System.Web.HttpContext httpContext, bool reCreate)
		{
			TerminalImplementation instance;
			if (reCreate)
			{
				httpContext.Application["TerminalImplementation"] = null;
				httpContext.Application["TerminalImplementation.SyncRoot"] = null;
			}
			instance = httpContext.Application["TerminalImplementation"] as TerminalImplementation;
			if(instance==null)
			{
				lock(GetSyncRoot(httpContext))
				{
					instance = httpContext.Application["TerminalImplementation"] as TerminalImplementation;
					if(instance==null)
					{
						instance = new TerminalImplementation(httpContext);
						httpContext.Application["TerminalImplementation"] = instance;
					}
				}
			}
			return instance;
		}

		private static object GetSyncRoot(System.Web.HttpContext httpContext)
		{
			object syncRoot = httpContext.Application["TerminalImplementation.SyncRoot"];
			if(syncRoot==null)
			{
				lock (TerminalImplementation.StaticLock)
				{
					syncRoot = httpContext.Application["TerminalImplementation.SyncRoot"];
					if(syncRoot==null)
					{
						syncRoot = new object();
						httpContext.Application["TerminalImplementation.SyncRoot"] = syncRoot;
					}
				}
			}
			return syncRoot;
		}

		private object m_SyncRoot;

		public object SyncRoot
		{
			get
			{
				return m_SyncRoot;
			}
		}

		internal TerminalImplementation(System.Web.HttpContext httpContext)
		{
			m_CerebrumDomain = null;
			m_SyncRoot = GetSyncRoot(httpContext);
			System.AppDomain.CurrentDomain.DomainUnload += new EventHandler(CurrentDomain_DomainUnload);
			this.FindDomain(httpContext);
		}


		private void CurrentDomain_DomainUnload(object sender, EventArgs e)
		{
			//Shutdown(null);
		}

		private AppDomain m_CerebrumDomain;

		private void FindDomain(System.Web.HttpContext httpContext)
		{
			if(m_CerebrumDomain==null)
			{
				lock(this.SyncRoot)
				{
					if(m_CerebrumDomain==null)
					{
						string cerebrumDomainName = this.GetCerebrumDomainName(httpContext);
						System.Collections.ArrayList appdomains = new ArrayList();
						CorRuntimeHostClass host = new CorRuntimeHostClass(); 
						IntPtr enumHandle = IntPtr.Zero; 
						try
						{
							host.EnumDomains(out enumHandle); 
							object currentDomain; 
							while(true) 
							{ 
								host.NextDomain(enumHandle, out currentDomain); 
								if (currentDomain != null)
								{
									string domainName = string.Empty;
									try
									{
										domainName = (currentDomain as AppDomain).FriendlyName;
									}
									catch
									{
										domainName = string.Empty;
									}
									if (domainName == cerebrumDomainName)
									{
										appdomains.Add(currentDomain as AppDomain);
									}
								}
								else
									break;
							} 
						}
						finally
						{
							host.CloseEnum(enumHandle); 
							Marshal.ReleaseComObject(host); 
						}
						/*foreach(AppDomain appd in appdomains)
						{
							AppDomain.Unload(appd);
						}*/
						if(appdomains.Count > 0)
						{
							m_CerebrumDomain = appdomains[0] as AppDomain;
						}
					}
				}
			}
		}
		public void Initialize(System.Web.HttpContext httpContext)
		{
			if(m_CerebrumDomain==null)
			{
				this.FindDomain(httpContext);
				if(m_CerebrumDomain==null)
				{
					lock(this.SyncRoot)
					{
						if(m_CerebrumDomain==null)
						{
							string cerebrumDomainName = this.GetCerebrumDomainName(httpContext);

							AppDomainSetup setup = new AppDomainSetup();
							setup.ApplicationBase = httpContext.Server.MapPath("~/cerebrum");
							setup.ConfigurationFile = httpContext.Server.MapPath("~/cerebrum/Cerebrum.Ganglion.WebForum.config");

							// Set up the Evidence
							Evidence baseEvidence = AppDomain.CurrentDomain.Evidence;
							Evidence evidence = new Evidence(baseEvidence);
							evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Typedef.dll"));
                            evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Runtime.dll"));
                            evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Runtime.Semiotics.dll"));
                            //evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Runtime.Diagnosis.dll"));
							//evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Integrator.dll"));
							//evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Management.dll"));
							evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Ganglion.Remoting.dll"));
							evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Ganglion.Ensemble.dll"));
							evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Ganglion.WebForum.dll"));
							//evidence.AddAssembly(httpContext.Server.MapPath("~/cerebrum/Cerebrum.Ganglion.Terminal.dll"));
							//evidence.AddHost("(some host)");

							// Create the AppDomain      
							m_CerebrumDomain = AppDomain.CreateDomain(cerebrumDomainName, evidence, setup);
						}
					}
				}
			}
		}

		public void Shutdown(System.Web.HttpContext httpContext, bool forceUnloadOnError)
		{
			if(m_CerebrumDomain!=null)
			{
				lock(this.SyncRoot)
				{
					if(m_CerebrumDomain!=null)
					{
						try
						{
							System.Runtime.Remoting.ObjectHandle obj = m_CerebrumDomain.CreateInstance("Cerebrum.Ganglion.Ensemble", "Cerebrum.Ganglion.Ensemble.EnsembleController");

							if (obj != null)
							{
								Cerebrum.Ganglion.Remoting.IEnsembleController ctl = obj.Unwrap() as Cerebrum.Ganglion.Remoting.IEnsembleController;
								ctl.Shutdown(forceUnloadOnError);
							}
						}
						catch
						{
							if (forceUnloadOnError)
							{
								System.AppDomain.Unload(m_CerebrumDomain);
								m_CerebrumDomain = null;
							}
							throw;
						}

						System.AppDomain.Unload(m_CerebrumDomain);
						m_CerebrumDomain = null;
					}
				}
			}
		}

		public string GetParameter(System.Web.HttpContext httpContext, string sectionName, string valueName)
		{
			lock(this.SyncRoot)
			{
				try
				{
					string svalue = null;
					string path = httpContext.Server.MapPath("~/terminal.config");
					if (System.IO.File.Exists(path))
					{
						System.Xml.XmlDocument xc = new System.Xml.XmlDocument();
						using (System.IO.Stream fs = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite))
						{
							using (System.IO.StreamReader sr = new System.IO.StreamReader(fs, System.Text.Encoding.GetEncoding("windows-1251")))
							{
								System.Xml.XmlReader xr = new System.Xml.XmlTextReader(sr);
								xc.Load(xr);
								xr.Close();
							}
						}
						System.Xml.XmlNode xcnt = xc.SelectSingleNode("database/" + sectionName + "/item[@name='" + valueName + "']");
						if (xcnt != null)
						{
							svalue = xcnt.Attributes["value"].Value;
						}
						else
						{
							svalue = string.Empty;
						}
					}
					return svalue;
				}
				catch (System.Threading.ThreadAbortException)
				{
					throw;
				}
				catch (System.Exception ex)
				{
#warning "Proccess Exception"
					return string.Empty;
				}
			}
		}

		public void SetParameter(System.Web.HttpContext httpContext, string sectionName, string valueName, string value)
		{
			lock(this.SyncRoot)
			{
				string path = httpContext.Server.MapPath("~/terminal.config");
				System.Xml.XmlDocument xc = new System.Xml.XmlDocument();
				if (System.IO.File.Exists(path))
				{
					using (System.IO.Stream fs = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite))
					{
						using (System.IO.StreamReader sr = new System.IO.StreamReader(fs, System.Text.Encoding.GetEncoding("windows-1251")))
						{
							System.Xml.XmlReader xr = new System.Xml.XmlTextReader(sr);
							xc.Load(xr);
							xr.Close();
						}
					}
				}
				else
				{
					xc.LoadXml("<?xml version=\"1.0\" encoding=\"windows-1251\"?><database></database>");
				}
				System.Xml.XmlNode xcntsect = xc.DocumentElement.SelectSingleNode(sectionName);
				if (xcntsect == null)
				{
					xcntsect = xc.CreateElement(sectionName);
					xc.DocumentElement.AppendChild(xcntsect);
				}
				System.Xml.XmlNode xcnt = xcntsect.SelectSingleNode("item[@name='" + valueName + "']");
				if(xcnt==null)
				{
					xcnt = xc.CreateElement("item");
					System.Xml.XmlAttribute attr =  xc.CreateAttribute("name");
					attr.Value = valueName;
					xcnt.Attributes.Append(attr);
					attr =  xc.CreateAttribute("value");
					attr.Value = "0";
					xcnt.Attributes.Append(attr);
					xcntsect.AppendChild(xcnt);
				}
				if(xcnt!=null)
				{
					xcnt.Attributes["value"].Value = value;
				}
				using (System.IO.Stream fs = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.ReadWrite))
				{
					using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fs, System.Text.Encoding.GetEncoding("windows-1251")))
					{
						System.Xml.XmlWriter xw = new System.Xml.XmlTextWriter(sw);
						xc.Save(xw);
						xw.Close();
					}
				}
			}
		}

		public bool IsLoaded
		{
			get
			{
				return this.m_CerebrumDomain != null;
			}
		}
		public bool GetStartOnDemand(System.Web.HttpContext httpContext)
		{
			string v = GetParameter(httpContext, "ganglion", "StartOnDemand");
			return (v==null || v.Length < 1)?false:Convert.ToBoolean(v);
		}
		public void SetStartOnDemand(System.Web.HttpContext httpContext, bool value)
		{
			SetParameter(httpContext, "ganglion", "StartOnDemand", value.ToString());
		}
		public string GetCerebrumDomainName(System.Web.HttpContext httpContext)
		{
			return GetParameter(httpContext, "ganglion", "CerebrumDomainName");
		}

		public GanglionStatus GetGanglionStatus(System.Web.HttpContext httpContext)
		{
			if(m_CerebrumDomain!=null)
			{
				return GanglionStatus.Online;
			}
			else
			{
				if(GetStartOnDemand(httpContext))
				{
					return GanglionStatus.Steady;
				}
				else
				{
					return GanglionStatus.Offline;
				}
			}
		}
		public System.Runtime.Remoting.ObjectHandle CreateInstance(System.Web.HttpContext httpContext, string assemblyName, string typeName)
		{
			if(m_CerebrumDomain==null && GetStartOnDemand(httpContext))
			{
				this.Initialize(httpContext);
			}
			if(m_CerebrumDomain!=null)
			{
				return m_CerebrumDomain.CreateInstance(assemblyName, typeName);
			}
			return null;
		}
	}

	public enum GanglionStatus
	{
		Offline,
		Steady,
		Online
	}
}
