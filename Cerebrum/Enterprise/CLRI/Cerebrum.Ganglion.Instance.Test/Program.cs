// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Windows.Forms;

namespace Cerebrum.Ganglion.Server.Test
{
	public class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.Run(new Form1());
		}
	}
}