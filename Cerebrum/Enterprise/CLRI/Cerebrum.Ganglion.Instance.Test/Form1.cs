// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Cerebrum.Ganglion.Server.Test
{
	public class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnTest_Click(object sender, EventArgs e)
		{
			string srv = "localhost";// System.Configuration.ConfigurationSettings.AppSettings["CerebrumServer"];

			Cerebrum.Ganglion.Remoting.IRemoteDatabase rs = (Cerebrum.Ganglion.Remoting.IRemoteDatabase)Activator.GetObject(
				typeof(Cerebrum.Ganglion.Remoting.IRemoteDatabase),
				"tcp://" + srv + ":8011/Cerebrum/Ganglion/Ensemble/RemoteDatabase");
			Cerebrum.ObjectHandle h = rs.ResolveHandle("Cerebrum.Ganglion.WebForum", "Tables", "Name", "Types");
			//rs.Shutdown("Cerebrum.Ganglion.WebForum");
			MessageBox.Show(h.ToString());
			/*Cerebrum.Ganglion.Remoting.IEnsembleController ec = (Cerebrum.Ganglion.Remoting.IEnsembleController)Activator.GetObject(
				typeof(Cerebrum.Ganglion.Remoting.IEnsembleController),
				"tcp://" + srv + ":8011/Cerebrum/Ganglion/Ensemble/EnsembleController");
			ec.Shutdown();*/
		}

		private System.Windows.Forms.Button btnTest2;


		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnTest = new System.Windows.Forms.Button();
			this.btnTest2 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnTest
			// 
			this.btnTest.Location = new System.Drawing.Point(32, 26);
			this.btnTest.Name = "btnTest";
			this.btnTest.TabIndex = 2;
			this.btnTest.Text = "Test";
			this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
			// 
			// btnTest2
			// 
			this.btnTest2.Location = new System.Drawing.Point(32, 72);
			this.btnTest2.Name = "btnTest2";
			this.btnTest2.TabIndex = 3;
			this.btnTest2.Text = "Test 2";
			this.btnTest2.Click += new System.EventHandler(this.btnTest2_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.btnTest2);
			this.Controls.Add(this.btnTest);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnTest;

		private void btnTest2_Click(object sender, System.EventArgs e)
		{
			try
			{
				string srv = "localhost";// System.Configuration.ConfigurationSettings.AppSettings["CerebrumServer"];

				Cerebrum.Ganglion.Remoting.IRemoteDatabase rs = (Cerebrum.Ganglion.Remoting.IRemoteDatabase)Activator.GetObject(
					typeof(Cerebrum.Ganglion.Remoting.IRemoteDatabase),
					"tcp://" + srv + ":8011/Cerebrum/Ganglion/Ensemble/RemoteDatabase");

				Cerebrum.ObjectHandle h = new ObjectHandle(11111);
			
				//rs.DeleteRecord("Cerebrum.Ganglion.WebForum", "Tables", h);
			}
			catch(System.Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(ex.ToString());
			}
		}
	}
}