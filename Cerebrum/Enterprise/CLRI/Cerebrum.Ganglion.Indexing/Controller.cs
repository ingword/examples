// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Text;

namespace Cerebrum.Ganglion.Indexing
{
	public class Controller : Cerebrum.Integrator.GenericComponent
	{
        static Controller()
		{
			Cerebrum.Runtime.Semiotics.KernelObjectKindId.Register();
		}

        private Cerebrum.Collections.Specialized.HandleObjectDictionary ReadDictionary(System.IO.Stream stream)
        {
			stream.Seek(0, System.IO.SeekOrigin.Begin);
			Cerebrum.Collections.Specialized.HandleObjectDictionary dict = new Cerebrum.Collections.Specialized.HandleObjectDictionary();
            Cerebrum.ObjectHandle key = Cerebrum.ObjectHandle.ReadFrom(stream);
            while (key != Cerebrum.ObjectHandle.Null)
            {
                Cerebrum.Collections.Specialized.HandleCollection collection = new Cerebrum.Collections.Specialized.HandleCollection();
                collection.Serialize(SerializeDirection.Load, null, stream);
                dict[key] = collection;
                key = Cerebrum.ObjectHandle.ReadFrom(stream);
            }
            return dict;
        }

        private void WriteDictionary(System.IO.Stream stream, Cerebrum.Collections.Specialized.HandleObjectDictionary dictionary, bool truncateStream)
        {
			stream.Seek(0, System.IO.SeekOrigin.Begin);
            foreach (System.Collections.DictionaryEntry de in dictionary)
            {
                Cerebrum.ObjectHandle key = (Cerebrum.ObjectHandle)de.Key;
                Cerebrum.ObjectHandle.WriteTo(stream, key);
                Cerebrum.Collections.Specialized.HandleCollection collection = (Cerebrum.Collections.Specialized.HandleCollection)de.Value;
                collection.Serialize(SerializeDirection.Save, null, stream);
            }
            Cerebrum.ObjectHandle.WriteTo(stream, Cerebrum.ObjectHandle.Null);
			if (truncateStream)
			{
				stream.SetLength(stream.Position);
			}
        }

		private Cerebrum.Runtime.ISemiotics m_Semiotics = null;
		private Cerebrum.Runtime.ISemiotics Semiotics
        {
            get
            {
				if (this.m_Semiotics == null)
                {
                    Cerebrum.Integrator.DomainContext domainContext = this.DomainContext;
                    Cerebrum.ObjectHandle rootAttribute = Cerebrum.Management.Utilites.ResolveHandle(domainContext, "Attributes", "Name", "IndexRootHandle");
                    object rh = this.GetAttributeComponent(rootAttribute);
					this.m_Semiotics = Cerebrum.Runtime.LFacade.CreateInstance(this.m_Connector.Workspace);

                    if (!(rh is Cerebrum.ObjectHandle))
                    {
						rh = this.m_Semiotics.CreateSector();
                        this.SetAttributeComponent(rootAttribute, rh);
                    }
                    else
                    {
						this.m_Semiotics.ReviveSector((Cerebrum.ObjectHandle)rh);
                    }
                }
				return this.m_Semiotics;
            }
        }

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
				if (this.m_Semiotics != null)
                {
					this.m_Semiotics.Dispose();
					this.m_Semiotics = null;
                }
            }
            base.Dispose(disposing);
        }

		private Cerebrum.ObjectHandle[] CreateText(string text, bool isreadonly)
		{
			return this.Semiotics.ParseToken(text, isreadonly);
		}

		private string ObtainText(Cerebrum.ObjectHandle nodeHandle)
		{
			using (Cerebrum.IComposite composite = this.Semiotics.AttachConnector(nodeHandle))
			{
				return Convert.ToString(composite.Component);
			}
		}

		public void CreateIndex(Cerebrum.ObjectHandle ringId, string text, Cerebrum.ObjectHandle target)
		{
			Cerebrum.ObjectHandle[] leafs = this.CreateText(text, false);

			using (System.IO.Stream stm = (System.IO.Stream)this.Semiotics.AttachConnector(leafs[0]))
            {
                Cerebrum.Collections.Specialized.HandleObjectDictionary dict;
                if (stm.Length > 0)
                {
                    dict = this.ReadDictionary(stm);
                }
                else
                {
                    dict = new Cerebrum.Collections.Specialized.HandleObjectDictionary();
                }
                Cerebrum.Collections.Specialized.HandleCollection coll = dict[ringId] as Cerebrum.Collections.Specialized.HandleCollection;
                if (coll == null)
                {
                    coll = new Cerebrum.Collections.Specialized.HandleCollection();
                    dict[ringId] = coll;
                }
                coll.Add(target);
                this.WriteDictionary(stm, dict, true);
            }
		}

		public bool RemoveIndex(Cerebrum.ObjectHandle ringId, string text, Cerebrum.ObjectHandle target)
		{
            Cerebrum.ObjectHandle[] leafs = this.CreateText(text, true);

            if (leafs != null && leafs.Length > 0)
            {
				using (System.IO.Stream stm = (System.IO.Stream)this.Semiotics.AttachConnector(leafs[0]))
                {
                    Cerebrum.Collections.Specialized.HandleObjectDictionary dict;
                    if (stm.Length > 0)
                    {
                        dict = this.ReadDictionary(stm);
                    }
                    else
                    {
                        dict = null;
                    }
                    if (dict != null)
                    {
                        Cerebrum.Collections.Specialized.HandleCollection coll = dict[ringId] as Cerebrum.Collections.Specialized.HandleCollection;
                        if (coll != null)
                        {
                            coll.Remove(target);
                            this.WriteDictionary(stm, dict, true);
                        }
                    }
                }
            }
			return false;
		}

		public Cerebrum.ObjectHandle[] ObtainIndex(Cerebrum.ObjectHandle ringId, string text)
		{
            Cerebrum.ObjectHandle[] leafs = this.CreateText(text, true);
            if (leafs != null && leafs.Length > 0)
            {
				using (System.IO.Stream stm = (System.IO.Stream)this.Semiotics.AttachConnector(leafs[0]))
                {
                    Cerebrum.Collections.Specialized.HandleObjectDictionary dict;
                    if (stm.Length > 0)
                    {
                        dict = this.ReadDictionary(stm);
                    }
                    else
                    {
                        dict = null;
                    }
                    if (dict != null)
                    {
                        Cerebrum.Collections.Specialized.HandleCollection coll = dict[ringId] as Cerebrum.Collections.Specialized.HandleCollection;
                        if (coll != null)
                        {
                            return coll.ToArray();
                        }
                    }
                }
            }
            return null;
        }
	}
}
