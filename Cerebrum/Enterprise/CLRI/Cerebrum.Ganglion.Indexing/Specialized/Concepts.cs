// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Indexing.Specialized
{
	/// <summary>
	/// Summary description for Concepts.
	/// </summary>
	public class Concepts
	{
		public Concepts()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/*public static Cerebrum.ObjectHandle DependentObjectsListAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "DependentObjectsList");
		}
		public static Cerebrum.ObjectHandle PrecedentObjectsListAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "PrecedentObjectsList");
		}
		public static Cerebrum.ObjectHandle SymbolAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "Symbol");
		}*/
		public static Cerebrum.ObjectHandle SymbolNeuronType(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Types", "Name", "FastSymbolNeuron");
		}
		/*public static Cerebrum.ObjectHandle ObjectCollectionType(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Types", "Name", "ObjectCollection");
		}*/
		/*public static Cerebrum.ObjectHandle DependentComponentsListAttribute(Cerebrum.Integrator.DomainContext context)
		{
			return Cerebrum.Management.Utilites.ResolveHandle(context, "Attributes", "Name", "DependentComponentsList");
		}*/
	}
}
