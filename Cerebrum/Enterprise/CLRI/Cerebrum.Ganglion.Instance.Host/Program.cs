// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Instance.Host
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Program
	{
		public Cerebrum.Ganglion.Instance.Server m_Server;
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			Program app = new Program();
			app.Run(args);
		}

		public void Run(string[] args)
		{
			m_Server = new Cerebrum.Ganglion.Instance.Server();

			m_Server.ServerPort = 8011;
			m_Server.StartServer();
			Console.WriteLine("Cerebrum Database is started");
			Console.ReadLine();
			m_Server.ExitServer();
		}
	}
}
