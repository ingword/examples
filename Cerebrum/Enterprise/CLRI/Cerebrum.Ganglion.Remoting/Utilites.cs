// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Text;

namespace Cerebrum.Ganglion.Remoting
{
	public class Utilites
	{
		private static byte[] Reverse(byte[] bytes)
		{
			byte[] buff = new byte[bytes.Length];
			for (int i = 0; i < bytes.Length; i++)
			{
				buff[i] = bytes[bytes.Length - 1 - i];
			}
			return buff;
		}

		static System.Random random = new System.Random();
		public static string NewReplicationId(bool b1, bool b0)
		{
			System.DateTime time = System.DateTime.Now;
			System.Int32 hash1 = 0;
			System.Int16 hash2 = 0;
			System.Int64 ticks = time.Ticks;
			System.Int32 temp3 = random.Next();
			System.Int16 hash3 = (System.Int16)(temp3 ^ (temp3 >> 16));

			System.Collections.ArrayList list = new System.Collections.ArrayList();
			list.AddRange(Reverse(System.BitConverter.GetBytes(hash1)));
			list.AddRange(Reverse(System.BitConverter.GetBytes(hash2)));
			list.AddRange(Reverse(System.BitConverter.GetBytes(ticks)));
			list.AddRange(Reverse(System.BitConverter.GetBytes(hash3)));
			list.Add((byte)((b0 ? 1 : 0) + (b1 ? 2 : 0)));
			return Encode032(130, (byte[])list.ToArray(typeof(byte)));
			//return string.Format("{0:x}", hash1).PadLeft(8, '0') + string.Format("{0:x}", hash2).PadLeft(4, '0') + string.Format("{0:x}", ticks).PadLeft(16, '0') + string.Format("{0:x}", hash3).PadLeft(4, '0');
			//return System.Guid.NewGuid().ToString().Replace("{", "").Replace("}", "").Replace("-", "");
		}

        public static string Encode032(int bits, byte[] bytes)
        {
            System.Text.StringBuilder sb = new StringBuilder();
            while (bits > 0)
            {
                byte tail = 0;
                char c;
                byte v = (byte)(bytes[0] & 0x1f);
                if (v < 10)
                {
                    c = (char)('0' + v);
                }
                else
                {
                    c = (char)('a' + v - 10);
                }
                sb.Append(c);

                for (int i = bytes.Length - 1; i >= 0; i--)
                {
                    byte temp = bytes[i];
                    bytes[i] = (byte)(tail | (temp >> 5));
                    tail = (byte)(temp << 3);
                }
                bits -= 5;
            }
            return sb.ToString();
        }

        /*
        public static byte[] Decode032(int bits, string text)
        {
            byte[] buff = new byte[(bits + 7) / 8];
            for (int i = text.Length - 1; i >= 0; i--)
            {
                byte v = 0;
                char c = text[i];
                if ('0' <= c && c <= '9')
                {
                    v = (byte)(c - '0');
                }
                if ('A' <= c && c <= 'Z')
                {
                    v = (byte)(10 + c - 'A');
                }
                if ('a' <= c && c <= 'z')
                {
                    v = (byte)(10 + c - 'a');
                }
                byte tail = (byte)(v);
                for (int j = 0; j < buff.Length; j++)
                {
                    byte temp = buff[j];
                    buff[j] = (byte)(tail | (temp << 5));
                    tail = (byte)(temp >> 3);
                }
            }
            return buff;
        }
        */

		public static string Encode32Z(int bits, byte[] bytes)
		{
			System.Text.StringBuilder sb = new StringBuilder();
			while (bits > 0)
			{
				byte tail = 0;
				char c;
				byte v = (byte)(bytes[0] & 0x1f);
                if (v < 10)
                {
                    if (v == 0)
                    {
                        c = 'z';
                    }
                    else
                    {
                        c = (char)('0' + v);
                    }
                }
                else
                {
                    c = (char)('a' + v - 10);
                }
				sb.Append(c);

				for (int i = bytes.Length - 1; i >= 0; i--)
				{
					byte temp = bytes[i];
					bytes[i] = (byte)(tail | (temp >> 5));
					tail = (byte)(temp << 3);
				}
				bits -= 5;
			}
			return sb.ToString();
		}

		public static byte[] Decode32Z(int bits, string text)
		{
			byte[] buff = new byte[(bits + 7) / 8];
			for (int i = text.Length - 1; i >= 0; i--)
			{
				byte v = 0;
				char c = text[i];
                if ('0' <= c && c <= '9')
                {
                    v = (byte)(c - '0');
                }
                else
                {
                    if ('A' <= c && c < 'Z')
                    {
                        v = (byte)(10 + c - 'A');
                    }
                    if ('a' <= c && c < 'z')
                    {
                        v = (byte)(10 + c - 'a');
                    }
                    // if ('z' == c || 'Z' == c) already v = 0
                }
				byte tail = (byte)(v);
				for (int j = 0; j < buff.Length; j++)
				{
					byte temp = buff[j];
					buff[j] =(byte)(tail | (temp << 5));
					tail = (byte)(temp >> 3);
				}
			}
			return buff;
		}

		public static string Encode16(byte[] value)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder(value.Length * 2);
			for (int i = 0; i < value.Length; i++)
			{
				sb.Append(string.Format("{0:x}", value[i]).PadLeft(2, '0'));
			}
			return sb.ToString();
		}

		public static byte[] Decode16(string value)
		{
			int l = (value.Length + 1) / 2;
			byte[] b = new byte[l];
			for (int i = 0; i < l; i++)
			{
				string s = value.Substring(i * 2, 2);
				b[i] = byte.Parse(s, System.Globalization.NumberStyles.HexNumber);
			}
			return b;
		}


		public static FieldDef FindFieldDef(TableDef table, string name)
		{
			for (int i = 0; i < table.Fields.Length; i++)
			{
				FieldDef fd = table.Fields[i];
				if (name.Equals(fd.Name))
				{
					return fd;
				}
			}
			return null;
		}

		public static EntryDef FindEntryDef(TupleDef tuple, Cerebrum.ObjectHandle fieldHandle)
		{
#warning "�������� �� ����� ������� ����������� �������"
			foreach (EntryDef vd in tuple.Values)
			{
				if (fieldHandle.Equals(vd.ObjectHandle))
				{
					return vd;
				}
			}
			return null;
		}

		public static ValueDef FindValueDef(TupleDef tuple, Cerebrum.ObjectHandle fieldHandle)
		{
			return FindEntryDef(tuple, fieldHandle) as ValueDef;
		}
	}
}
