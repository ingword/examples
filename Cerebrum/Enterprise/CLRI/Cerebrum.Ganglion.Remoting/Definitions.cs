// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;

namespace Cerebrum.Ganglion.Remoting
{
	[Serializable]
	public class ExecutionContext
	{
		public string Description;
	}

	/// <summary>
	/// Summary description for IApplication.
	/// </summary>
	public interface IEnsembleController
	{
		void Initialize();
		void Shutdown(bool forceShutdown);
		IDatabaseController GetDatabaseController(string name, bool autostart);
	}

	public interface IDatabaseController
	{
		void Initialize();
		void Shutdown();
		void SetClean();
		void SetDirty();
		Cerebrum.ObjectHandle InsertRecord(string tableName, string[] columns, object[] values);
		object InvokeRecord(string tableName, Cerebrum.ObjectHandle handle, string method, object[] parameters);
		bool UpdateRecord(string tableName, Cerebrum.ObjectHandle handle, string[] columns, object[] values);
		bool EngageRecord(string tableName, Cerebrum.ObjectHandle handle);
		bool DeleteRecord(string tableName, Cerebrum.ObjectHandle handle);
		ValueDef[] SelectRecord(string tableName, Cerebrum.ObjectHandle handle, string[] columns);
		ObjectHandle ResolveHandle(string tableName, string fieldName, object value);
	}
	public interface IRemoteDatabase
	{
		void Shutdown(string databaseName);
		Cerebrum.ObjectHandle InsertRecord(string databaseName, string tableName, string[] columns, object[] values);
		bool UpdateRecord(string databaseName, string tableName, Cerebrum.ObjectHandle handle, string[] columns, object[] values);
		object InvokeRecord(string databaseName, string tableName, Cerebrum.ObjectHandle handle, string method, object[] parameters);
		bool EngageRecord(string databaseName, string tableName, Cerebrum.ObjectHandle handle);
		bool EngageRecord(string databaseName, string tableName, ObjectHandle recordHandle, string fieldName, ObjectHandle engageHandle);
		bool DeleteRecord(string databaseName, string tableName, Cerebrum.ObjectHandle handle);
		ValueDef[] SelectRecord(string databaseName, string tableName, Cerebrum.ObjectHandle handle, string[] columns);
		ObjectHandle ResolveHandle(string databaseName, string tableName, string fieldName, object value);
		QueryRes QueryTable(string databaseName, string tableName, string[] columns, int from, int count);
		QueryRes QueryRecord(string dbName, string tableName, string fieldName, ObjectHandle handle, string[] columns, int from, int count);
		object InvokeComponent(string databaseName, /*string tableName, */string componentName, string method, object[] parameters);
		void Impersonate(System.Security.Principal.IPrincipal principal);
		void SetExecutionContext(Cerebrum.Ganglion.Remoting.ExecutionContext executionContext);

		Cerebrum.Ganglion.Remoting.TableDef[] ExecuteQuery(string dbName, QueryDef query);
		QueryDef BuildQuery(string databaseName, string tableName, string[] fields, string[] orderby/*, int from, int count, object query*/);
	}

	[Serializable]
	public class EntryDef
	{
		public Cerebrum.ObjectHandle ObjectHandle;

		public EntryDef(Cerebrum.ObjectHandle objectHandle)
		{
			this.ObjectHandle = objectHandle;
		}
	}

	[Serializable]
	public class ValueDef : EntryDef
	{
		public object Value;

		public ValueDef(Cerebrum.ObjectHandle objectHandle, object value)
			: base(objectHandle)
		{
			this.Value = value;
		}
	}

	[Serializable]
	public class TupleDef : EntryDef
	{
		public EntryDef[] Values;

		public TupleDef(Cerebrum.ObjectHandle objectHandle, EntryDef[] valueDefs)
			: base(objectHandle)
		{
			this.Values = valueDefs;
		}
	}

	[Serializable]
	public class FieldDef : EntryDef
	{
		public string Name;
		public System.Type Type;
		public TableDef Table;

		public FieldDef(Cerebrum.ObjectHandle objectHandle, TableDef table, string name, System.Type type)
			: base(objectHandle)
		{
			this.Name = name;
			this.Type = type;
			this.Table = table;
		}
	}

	[Serializable]
	public class TableDef : EntryDef
	{
		public FieldDef[] Fields;
		public TupleDef[] Tuples;
		public OrderDef[] Orders;

		public TableDef(Cerebrum.ObjectHandle objectHandle)
			: base(objectHandle)
		{
			this.Fields = null;
			this.Tuples = null;
			this.Orders = null;
		}
	}

	public enum OrderDirection
	{
		Ascending,
		Descending
	}

	[Serializable]
	public class OrderDef
	{
		public OrderDef(FieldDef field, OrderDirection direction)
		{
			this.Field = field;
			this.Direction = direction;
		}
		public FieldDef Field;
		public OrderDirection Direction;
	}

	/// <summary>
	/// ������������ ���������� ��������.
	/// </summary>
	public enum LogicAction
	{
		Equal,
		Above,
		Below,
		NotEqual,
		NotAbove,
		NotBelow
	}

	[Serializable]
	public class BoundDef// : EntryDef
	{
		public BoundDef()// : base(Cerebrum.ObjectHandle.Null)
		{
		}
	}

	/*[Serializable]
	public class BaseFieldBoundDef : BoundDef
	{
		public FieldDef Field;

		public BaseFieldBoundDef(FieldDef field)
		{
			this.Field = field;
		}
	}*/

	[Serializable]
	public class BaseTableBoundDef : BoundDef
	{
		public TableDef Table;

		public BaseTableBoundDef(TableDef table)
		{
			this.Table = table;
		}
	}

	[Serializable]
	public class BaseTableFieldBoundDef : BaseTableBoundDef
	{
		public FieldDef Field;

		public BaseTableFieldBoundDef(TableDef table, FieldDef field)
			: base(table)
		{
			this.Field = field;
		}
	}

	/// <summary>
	/// ������������ ������� �������� �� ������� Tables.
	/// </summary>
	[Serializable]
	public class TableNameBoundDef : BaseTableBoundDef
	{
		public string TableName;

		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="table">��������� �� TableDef, ���� ����� ������� ������ �� ������� tableName</param>
		/// <param name="tableName">��� �������</param>
		public TableNameBoundDef(TableDef table, string tableName)
			: base(table)
		{
			this.TableName = tableName;
		}
	}

	public enum FieldJoinType
	{
		ValueDef,
		TupleDef,
		TableDef
	}

	/// <summary>
	/// TupleDef Join
	/// [07.05.2007 11:33:12] >> Tbl1 - ������ �������.
	/// Tbl1.Vector1 - ������ �� ��������� ������� �� Tbl2
	/// ��� ������ �����.
	/// � ������ ����� - ��� ����� ������ �� Tbl2 ��������� � Tbl1 ��� ������������� Vector1. ��� �����?
	/// 
	/// TableDef Join
	/// ������
	/// 20.04.2007 15:21:39 bsvh wrote: � WardenJoinBoundDef �������� Field - ��� � ���� ������. 
	/// Table - ��� ������� � ���������, ������� ���� ������� �� ���� �� ��������� ������ � �������� Field.
	/// 20.04.2007 15:22:22 ������� ������ (Cerebrum) wrote: ���
	/// 20.04.2007 15:23:04 bsvh wrote: � ����� �� ��������� ��������� ����� �� ��������� ����� ������?
	/// 20.04.2007 15:23:37 ������� ������ (Cerebrum) wrote: ���� ������ ������ � ������ - ���
	/// 
	/// [07.05.2007 11:33:12] >> Tbl1 - ������ �������.
	/// Tbl1.Vector1 - ������ �� ��������� ������� �� Tbl2
	/// ��� ������ �����.
	/// 
	/// ValueDef Join
	/// ������ LEFT JOIN ��� RIGHT JOIN
	/// �������� ����� ������: 
	/// SELECT A.Name, A.DefaultTypeId, T.ObjectHandle, T.Name 
	/// FROM Attributes AS A LEFT JOIN Types AS T ON A.DefaultTypeId = T.ObjectHandle
	/// ��������� � ����������� ������ ��� �:
	/// SELECT A.Name, A.DefaultTypeId, T.ObjectHandle, T.Name 
	/// WHERE (Name = 'Attributes') OR (Name = 'Types')
	/// 
	/// 07.05.2007 11:26:11 ������� ������ (Cerebrum) wrote:
	/// ��. ������ � ���� ���� ������ - �� �������� ������������ � ������� ������� � �����������
	/// 07.05.2007 11:26:35 ������� ������ (Cerebrum) wrote:
	/// � ������� ���� ��������� - �������, ��������� ����� �����, � � ��������� ����� ������ ������ ��������
	/// 07.05.2007 11:26:51 ������� ������ (Cerebrum) wrote:
	/// ��� ������� ������ �����
	/// 
	/// 07.05.2007 11:48:45 ������� ������ (Cerebrum) wrote:
	/// Tbl1 - ������ �������.
	/// Tbl1.Field1 - ������ Cerebrum.ObjectHandle ���������� sector-plasma ���������� �������
	/// HandleJoin.Table Def - ������� � ������� ���� �������� ��������� ������. 
	/// HandleJoin.Field (Field.Table - ������� Tbl1, Field.Name - Tbl1.Field1)
	/// (��� � ����� ������ ����������, HandleJoin ��������� true ��� ����������, ���� � ���� ObjectHandle == �������� � ������� ������ Tbl1, Field.Name  ��� ���������� ����������� ������������)
	/// 07.05.2007 11:50:25 ������� ������ (Cerebrum) wrote:
	/// Tbl1  ��������� � ����� �� ������� TableNameBoundDef
	/// 07.05.2007 11:51:31 ������� ������ (Cerebrum) wrote:
	/// �.�. ���� ������ �� ���� ������� �� ������� ������� � ��������� � ������ TableDef (HandleJoin.Table) ��� ������� �� ������� ��������� HandleJoin.Field
	/// </summary>
	[Serializable]
	public class FieldJoinBoundDef : BaseTableFieldBoundDef
	{
		public FieldJoinType JoinType;

		public FieldJoinBoundDef(TableDef table, FieldDef field, FieldJoinType joinType)
			: base(table, field)
		{
			this.JoinType = joinType;
		}
	}


	/// <summary>
	/// ������������ ������� �������� �� ����������� ��������� ����� ����� ����������
	/// </summary>
	[Serializable]
	public class CompareBoundDef : BaseTableFieldBoundDef
	{
		public EntryDef Value;
		public LogicAction Action;

		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="val">��������</param>
		/// <param name="action">���������� �������� ��������� �������� ���� � ����������� ��������</param>
		public CompareBoundDef(TableDef table, FieldDef field, EntryDef val, LogicAction action)
			: base(table, field)
		{
			this.Value = val;
			this.Action = action;
		}
	}

	/// <summary>
	/// ������ ������� ��� ������� (������� ���������� ���������) 
	/// ����� �������� ����� ����������� ���������� �������� AND.
	/// </summary>
	[Serializable]
	public class LimitDef
	{
		public BoundDef[] Expression;

		public LimitDef(BoundDef[] expression)
		{
			this.Expression = expression;
		}
	}

	public enum QueryType
	{
		Select,
		Insert,
		Update,
		Delete
	}

	[Serializable]
	public class QueryRes
	{
		public TableDef Table;
		public int TotalCount;

		public QueryRes(TableDef table, int totalCount)
		{
			this.Table = table;
			this.TotalCount = totalCount;
		}
	}

	[Serializable]
	public class QueryDef
	{
		public TableDef[] TableDefs;
		public LimitDef[] Expression;
		public QueryType QueryType;

		public QueryDef(TableDef[] td, LimitDef[] expression, QueryType queryType)
		{
			this.TableDefs = td;
			this.Expression = expression;
			this.QueryType = queryType;
		}
	}
}