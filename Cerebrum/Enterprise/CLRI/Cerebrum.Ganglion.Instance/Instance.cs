// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Text;

namespace Cerebrum.Ganglion.Instance
{
	public class Server : System.MarshalByRefObject 
	{

		protected System.Windows.Forms.ApplicationContext ServerAppContext = null;
		protected System.Threading.Thread ServerThread = null;
		protected System.Windows.Forms.Control ServerControl = null;
		protected int m_ServerPort = 8011;
		protected string m_EngineName = "Cerebrum/Ganglion/Ensemble";
		protected string m_PrimaryDirectory = null;

		protected void ServerThreadRoutine()
		{
			Cerebrum.Ganglion.Ensemble.EnsembleController ec = new Cerebrum.Ganglion.Ensemble.EnsembleController();
			ec.PrimaryDirectory = this.m_PrimaryDirectory;

			ServerAppContext = new System.Windows.Forms.ApplicationContext();
			ServerControl = new System.Windows.Forms.Control();
			ServerControl.CreateControl();


			System.Runtime.Remoting.Channels.BinaryServerFormatterSinkProvider provider = new System.Runtime.Remoting.Channels.BinaryServerFormatterSinkProvider();
			provider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;

			System.Collections.IDictionary props;

			props = new System.Collections.Hashtable();

			props.Add("port", this.m_ServerPort);

			props.Add("typeFilterLevel", System.Runtime.Serialization.Formatters.TypeFilterLevel.Full);

			string channelName = System.Guid.NewGuid().ToString();
			props.Add("name", channelName);

			System.Runtime.Remoting.Channels.Tcp.TcpChannel channel = new System.Runtime.Remoting.Channels.Tcp.TcpChannel(props, null, provider);
			System.Runtime.Remoting.Channels.ChannelServices.RegisterChannel(channel);

			System.Runtime.Remoting.RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(Cerebrum.Ganglion.Ensemble.RemoteDatabase),
				this.m_EngineName + "/RemoteDatabase",
				System.Runtime.Remoting.WellKnownObjectMode.Singleton);

			System.Runtime.Remoting.RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(Cerebrum.Ganglion.Ensemble.EnsembleController),
				this.m_EngineName + "/EnsembleController",
				System.Runtime.Remoting.WellKnownObjectMode.Singleton);

			System.Windows.Forms.Application.Run(ServerAppContext);

			foreach (System.Runtime.Remoting.Channels.IChannel achannel in System.Runtime.Remoting.Channels.ChannelServices.RegisteredChannels)
			{
				if( achannel.ChannelName == channelName)
				{
					System.Runtime.Remoting.Channels.ChannelServices.UnregisterChannel(achannel);
				}
			}
			//System.Runtime.Remoting.Channels.ChannelServices.UnregisterChannel(channel);

			ec.Shutdown(false);

			ServerControl.Dispose();
			ServerControl = null;

			ServerAppContext.Dispose();
			ServerAppContext = null;
		}

		public int ServerPort
		{
			get
			{
				return this.m_ServerPort;
			}
			set
			{
				if(ServerThread == null)
				{
					this.m_ServerPort = value;
				}
				else
				{
					throw new System.Exception("Can't change port while server is running");
				}
			}
		}

		public string EngineName
		{
			get
			{
				return this.m_EngineName;
			}
			set
			{
				this.m_EngineName = value;
			}
		}

		public string PrimaryDirectory
		{
			get
			{
				return this.m_PrimaryDirectory;
			}
			set
			{
				this.m_PrimaryDirectory = value;
			}
		}

		public void StartServer()
		{
			if (ServerThread == null)
			{
				ServerThread = new System.Threading.Thread(new System.Threading.ThreadStart(this.ServerThreadRoutine));
				ServerThread.Start();
			}
		}

		public void ExitServer()
		{
			if (ServerThread != null)
			{
				ServerAppContext.ExitThread();
				ServerThread.Join();
				ServerThread = null;
			}
		}
	}
}
