﻿// Copyright (C) Dmitry Shuklin 2006-2009. All rights reserved.

using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Cerebrum.Ganglion.Instance")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SHDsoftware")]
[assembly: AssemblyProduct("Cerebrum.Ganglion.Instance")]
[assembly: AssemblyCopyright("Copyright © 2006-2009 SHDsoftware")]
[assembly: AssemblyTrademark("© SHDsoftware")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3065596d-13bd-4083-a929-694eabfb1a56")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.300.6")]
[assembly: AssemblyFileVersion("1.0.300.6")]

[assembly: CLSCompliant(true)]

[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("..\\..\\..\\..\\CERT\\integrator.snk")]
[assembly: AssemblyKeyName("")]
