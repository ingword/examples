Cerebrum V1.0.300.19 - beta 2 (2009.04.17) Object-oriented network knowledge base management system


Please look at 

Cerebrum\Samples\                                                                  Console sample

Cerebrum\Enterprise\                                                               Client-Server sample

Cerebrum\Integrator\Binary\Application\Debug\Cerebrum.DesktopClient.exe            IDE and demo application



File Menu in the default configuration database contains:

  Data Manager - allows open database and then view or edit database content.


and Streams sample menu items:

  New Stream ... - creates new DB file for Streams-01 example
  Open Stream ... - opens existing DB file with Streams-01 example


Cerebrum : Object-oriented network knowledge base. 

The Object-oriented network knowledge base (OONKB) Cerebrum(tm) has the following features: 

- It is core for database management system with a network data model. 

- It allows to to create single direction links between the nodes. 

- Nodes in the Cerebrum are instances of Microsoft .NET Managed Objects. 

- Cerebrum saves the current state of the graph of objects or the neural network into the storage between application runs. 

- On the next run all saved instances can be instantiated by demand.

- Cerebrum caches in the RAM only that instances which are used and fits into allowed memory size. The other 
  objects are frozen in the file storage. 


The primary goal of this research is to create a virtual machine supporting free topology object-oriented network with 
up 2 billons of object instances within one physical storage area. 



Copyright (C) 2000-2009 Dmitry Shuklin   http://www.shuklin.com/ai/ht/ru/cerebrum
