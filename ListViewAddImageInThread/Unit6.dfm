object Form6: TForm6
  Left = 0
  Top = 0
  Caption = 'Form6'
  ClientHeight = 281
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object lvImages: TListView
    Left = 0
    Top = 40
    Width = 418
    Height = 241
    Align = alBottom
    Columns = <>
    LargeImages = ilImages
    TabOrder = 0
  end
  object btnMain: TButton
    Left = 152
    Top = 9
    Width = 75
    Height = 25
    Caption = 'btnMain'
    TabOrder = 1
    OnClick = btnMainClick
  end
  object ilImages: TImageList
    Height = 128
    Width = 128
    Left = 120
    Top = 96
  end
end
