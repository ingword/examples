unit Unit6;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  ShlObj, ActiveX, System.Win.ComObj, System.ImageList, Vcl.ImgList, Jpeg,
  system.SyncObjs;

const
  IMAGE_PATH = 'E:\Temp\12';
  IMAGE_NAME = 'Panorama 4.JPG';
  THUMBNAIL_SIZE = 128;

type
  TLoadThread = class (TThread)
  private
    FDirectoryPath : string;
    FFileName : string;
    FPicture : TPicture;

  protected
    procedure Execute; override;
    procedure InsertItem;
  public
    constructor Create(aDirectoryPath : string; aFileName: string);
    destructor Destroy; override;
  end;

type
  TForm6 = class(TForm)
    lvImages: TListView;
    btnMain: TButton;
    ilImages: TImageList;
    procedure btnMainClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    CS : TCriticalSection;
  end;

var
  Form6: TForm6;

implementation

{$R *.dfm}

procedure TForm6.btnMainClick(Sender: TObject);
var
  LoadThread : TLoadThread;
begin
  LoadThread := TLoadThread.Create(IMAGE_PATH, IMAGE_NAME)
end;

{ TLoadThread }
constructor TLoadThread.Create(aDirectoryPath : string; aFileName: string);
begin
  FDirectoryPath := aDirectoryPath;
  FFileName := aFileName;

  FPicture := TPicture.Create;
  FreeOnTerminate := True;
  inherited Create(False)
end;

destructor TLoadThread.Destroy;
begin
  FreeAndNil(FPicture);
  inherited;
end;

procedure TLoadThread.Execute;
{const
  IEIFLAG_ASPECT = 4;
  IEIFLAG_OFFLINE = 8;
  IEIFLAG_SCREEN = $20;
  IEIFLAG_ORIGSIZE = $40;

var
  FolderISF, DesktopISF: IShellFolder;
  IExtractImg: IExtractImage;
  Attrib, Eaten: DWORD;
  pItemIDL: PItemIDList;
  MemAlloc: IMalloc;
  CharBuf: array [0 .. 2047] of WideChar;
  Bmp: TBitmap;
  hBmp: HBITMAP;
  Size: TSize;
  Priority, Flags: Cardinal;
  GLResult: HResult;
begin
  inherited;
  if (SHGetMalloc(MemAlloc) <> NOERROR) or (MemAlloc = nil) then
    Exit;;
  // we need IMAlloc interface to free allocated memory of Shell
  if NOERROR = SHGetDesktopFolder(DesktopISF) then
  begin
    OleCheck(DesktopISF.ParseDisplayName(0, nil, PChar(FDirectoryPath), Eaten,
      // place your Folder name here
      pItemIDL, Attrib));
    DesktopISF.BindToObject(pItemIDL, nil, IShellFolder, FolderISF);
    // you must free all of the allocated shell memory of pItemIDL
    MemAlloc.Free(pItemIDL);
    OleCheck(FolderISF.ParseDisplayName(0, nil, PChar(FFileName), Eaten,
      // place your file name here
      pItemIDL, Attrib));
    FolderISF.GetUIObjectOf(0, 1, pItemIDL, IExtractImage, nil, IExtractImg);
    MemAlloc.Free(pItemIDL);
    // set the size of the thumbnail with the Size1
    Size.cx := THUMBNAIL_SIZE; // thumbnail bitmap width
    Size.cy := THUMBNAIL_SIZE; // thumnbail bitmap height, before crop

    Flags := // IEIFLAG_SCREEN // does not crop off bitmap
      IEIFLAG_ORIGSIZE // crops extra area off. may not work in win 2000
      or IEIFLAG_OFFLINE; // no online search for unknown video codex

    Priority := 0; // not sure is this has any effect

    GLResult := IExtractImg.GetLocation(CharBuf, sizeof(CharBuf), Priority,
      Size, 32 { ColorDepth } {, Flags);
    if (GLResult = NOERROR) or (GLResult = E_PENDING) then
    begin
      Bmp := TBitmap.Create;
      try
        OleCheck(IExtractImg.Extract(hBmp));
        Bmp.Handle := hBmp;

        Bmp.Width := THUMBNAIL_SIZE;
        Bmp.Height := THUMBNAIL_SIZE;
        FPicture.Assign(Bmp);

        Synchronize(InsertItem);
      finally
        Bmp.Free;
      end;
    end;
    //Path := CharBuf;
  end;     }
var
  BmImg: TBitmap;
  Bmp: TBitmap;
begin
  Form6.CS.Enter;
  try
    FPicture.LoadFromFile(FDirectoryPath + '\' + FFileName);

    BmImg := TBitmap.Create;
    BmImg.Assign(FPicture.Graphic);

    Bmp := TBitmap.Create;
    Bmp.SetSize(THUMBNAIL_SIZE, THUMBNAIL_SIZE);
    SetStretchBltMode(Bmp.Canvas.Handle, HALFTONE);
    StretchBlt(Bmp.Canvas.Handle, 0, 0, Bmp.Width, Bmp.Height,
               BmImg.Canvas.Handle, 0, 0, BmImg.Width, BmImg.Height, SRCCOPY);
    BmImg.Free;

    FPicture.Assign(Bmp);
    Synchronize(InsertItem);

    Bmp.Free;
  except
     on E : Exception do
          ShowMessage(E.ClassName + ' : ' + E.Message);
  end;
  Form6.CS.Leave;
end;

procedure TLoadThread.InsertItem;
var
  I : Integer;
begin
  Form6.lvImages.Items.BeginUpdate;
  I := Form6.ilImages.Add(FPicture.Bitmap, nil);
  with Form6.lvImages.Items.Add do
  begin
    Caption := FFileName;
    ImageIndex := I;
  end;
  Form6.lvImages.Items.EndUpdate;
end;


procedure TForm6.FormCreate(Sender: TObject);
begin
  CS := TCriticalSection.Create;
end;

procedure TForm6.FormDestroy(Sender: TObject);
begin
  FreeAndNil(CS);
end;

end.
