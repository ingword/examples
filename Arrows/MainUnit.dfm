object MainForm: TMainForm
  Left = 192
  Top = 122
  Caption = 'MainForm'
  ClientHeight = 606
  ClientWidth = 862
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object PaintBox: TPaintBox
    Left = 56
    Top = 168
    Width = 105
    Height = 105
    OnMouseDown = PaintBoxMouseDown
    OnMouseMove = PaintBoxMouseMove
    OnMouseUp = PaintBoxMouseUp
    OnPaint = PaintBoxPaint
  end
  object ControlPanel: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 161
    Align = alTop
    Caption = 'ControlPanel'
    TabOrder = 0
    object BegGroupBox: TGroupBox
      Left = 8
      Top = 8
      Width = 209
      Height = 145
      Caption = #1053#1072#1095#1072#1083#1086
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 20
        Width = 42
        Height = 13
        Caption = #1052#1072#1088#1082#1077#1088':'
      end
      object Label2: TLabel
        Left = 8
        Top = 52
        Width = 36
        Height = 13
        Caption = #1044#1083#1080#1085#1072':'
      end
      object Label3: TLabel
        Left = 8
        Top = 83
        Width = 42
        Height = 13
        Caption = #1064#1080#1088#1080#1085#1072':'
      end
      object Label4: TLabel
        Left = 8
        Top = 116
        Width = 38
        Height = 13
        Caption = #1054#1090#1089#1090#1091#1087':'
      end
      object BegMarkerComboBox: TComboBox
        Left = 56
        Top = 16
        Width = 145
        Height = 21
        TabOrder = 0
        Text = 'BegMarkerComboBox'
        Items.Strings = (
          #1053#1077#1090
          #1058#1088#1077#1059#1075#1086#1083#1100#1085#1072#1103' '#1057#1090#1088#1077#1083#1082#1072
          #1054#1090#1082#1088#1099#1090#1072#1103' '#1057#1090#1088#1077#1083#1082#1072
          #1042#1086#1075#1085#1091#1090#1072#1103' '#1057#1090#1088#1077#1083#1082#1072
          #1050#1074#1072#1076#1088#1072#1090#1085#1099#1081' '#1052#1072#1088#1082#1077#1088
          #1056#1086#1084#1073#1086#1042#1080#1076#1085#1099#1081' '#1052#1072#1088#1082#1077#1088
          #1064#1072#1088#1086#1054#1073#1088#1072#1079#1085#1099#1081' '#1052#1072#1088#1082#1077#1088)
      end
      object BegLengthComboBox: TComboBox
        Left = 56
        Top = 48
        Width = 145
        Height = 21
        TabOrder = 1
        Items.Strings = (
          '0'
          '8'
          '16'
          '32'
          '64')
      end
      object BegWidthComboBox: TComboBox
        Left = 56
        Top = 80
        Width = 145
        Height = 21
        TabOrder = 2
        Items.Strings = (
          '0'
          '8'
          '16'
          '32'
          '64')
      end
      object BegIndentComboBox: TComboBox
        Left = 56
        Top = 112
        Width = 145
        Height = 21
        TabOrder = 3
        Items.Strings = (
          '0'
          '8'
          '16'
          '32'
          '64')
      end
    end
    object EndGroupBox: TGroupBox
      Left = 448
      Top = 8
      Width = 209
      Height = 145
      Caption = #1050#1086#1085#1077#1094
      TabOrder = 1
      object Label5: TLabel
        Left = 8
        Top = 20
        Width = 42
        Height = 13
        Caption = #1052#1072#1088#1082#1077#1088':'
      end
      object Label6: TLabel
        Left = 8
        Top = 52
        Width = 36
        Height = 13
        Caption = #1044#1083#1080#1085#1072':'
      end
      object Label7: TLabel
        Left = 8
        Top = 83
        Width = 42
        Height = 13
        Caption = #1064#1080#1088#1080#1085#1072':'
      end
      object Label8: TLabel
        Left = 8
        Top = 116
        Width = 38
        Height = 13
        Caption = #1054#1090#1089#1090#1091#1087':'
      end
      object EndMarkerComboBox: TComboBox
        Left = 56
        Top = 16
        Width = 145
        Height = 21
        TabOrder = 0
        Text = 'EndMarkerComboBox'
        Items.Strings = (
          #1053#1077#1090
          #1058#1088#1077#1059#1075#1086#1083#1100#1085#1072#1103' '#1057#1090#1088#1077#1083#1082#1072
          #1054#1090#1082#1088#1099#1090#1072#1103' '#1057#1090#1088#1077#1083#1082#1072
          #1042#1086#1075#1085#1091#1090#1072#1103' '#1057#1090#1088#1077#1083#1082#1072
          #1050#1074#1072#1076#1088#1072#1090#1085#1099#1081' '#1052#1072#1088#1082#1077#1088
          #1056#1086#1084#1073#1086#1042#1080#1076#1085#1099#1081' '#1052#1072#1088#1082#1077#1088
          #1064#1072#1088#1086#1054#1073#1088#1072#1079#1085#1099#1081' '#1052#1072#1088#1082#1077#1088)
      end
      object EndLengthComboBox: TComboBox
        Left = 56
        Top = 48
        Width = 145
        Height = 21
        TabOrder = 1
        Items.Strings = (
          '0'
          '8'
          '16'
          '32'
          '64')
      end
      object EndWidthComboBox: TComboBox
        Left = 56
        Top = 80
        Width = 145
        Height = 21
        TabOrder = 2
        Items.Strings = (
          '0'
          '8'
          '16'
          '32'
          '64')
      end
      object EndIndentComboBox: TComboBox
        Left = 56
        Top = 112
        Width = 145
        Height = 21
        TabOrder = 3
        Items.Strings = (
          '0'
          '8'
          '16'
          '32'
          '64')
      end
    end
    object LineGroupBox: TGroupBox
      Left = 224
      Top = 8
      Width = 217
      Height = 145
      Caption = #1051#1080#1085#1080#1103
      TabOrder = 2
      object Label9: TLabel
        Left = 8
        Top = 20
        Width = 28
        Height = 13
        Caption = #1062#1074#1077#1090':'
      end
      object Label10: TLabel
        Left = 8
        Top = 52
        Width = 49
        Height = 13
        Caption = #1058#1086#1083#1097#1080#1085#1072':'
      end
      object Label11: TLabel
        Left = 8
        Top = 84
        Width = 33
        Height = 13
        Caption = #1057#1090#1080#1083#1100':'
      end
      object ColorBox: TColorBox
        Left = 64
        Top = 16
        Width = 145
        Height = 22
        TabOrder = 0
      end
      object WidthComboBox: TComboBox
        Left = 64
        Top = 48
        Width = 145
        Height = 21
        TabOrder = 1
        Text = 'WidthComboBox'
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          '10'
          '11'
          '12'
          '13'
          '14'
          '15'
          '16')
      end
      object StyleMarkerComboBox: TComboBox
        Left = 64
        Top = 80
        Width = 145
        Height = 21
        Style = csDropDownList
        TabOrder = 2
        Items.Strings = (
          #1057#1087#1083#1086#1096#1085#1072#1103
          #1055#1091#1085#1082#1090#1080#1088#1085#1072#1103
          #1058#1086#1095#1077#1095#1085#1072#1103
          #1058#1086#1095#1077#1095#1085#1086'-'#1055#1091#1085#1082#1090#1080#1088#1085#1072#1103
          #1058#1086#1095#1077#1095#1085#1086'-'#1058#1086#1095#1077#1095#1085#1086'-'#1055#1091#1085#1082#1090#1080#1088#1085#1072#1103
          #1054#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
          #1042#1087#1080#1089#1072#1085#1085#1072#1103)
      end
    end
    object btn1: TButton
      Left = 680
      Top = 80
      Width = 75
      Height = 25
      Caption = 'btn1'
      TabOrder = 3
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 587
    Width = 862
    Height = 19
    Panels = <>
  end
  object MainActionList: TActionList
    Left = 8
    Top = 168
    object CloseAction: TAction
      Category = #1054#1089#1085#1086#1074#1085#1099#1077
      Caption = '&'#1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1075#1099#1090#1100' '#1082' '#1063#1105#1075#1090#1086#1074#1086#1081' '#1052#1072#1090#1077#1075#1080'!'
      ShortCut = 27
      OnExecute = CloseActionExecute
    end
  end
end
