program TestInsight;

uses
  System.StartUpCopy,
  FMX.Forms,
  uMain in 'uMain.pas' {Form6},
  TestuMain in 'TestuMain.pas',
  TestInsight.DUnit;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm6, Form6);
  TestInsight.DUnit.RunRegisteredTests;
  Application.Run;
end.

