unit TestuMain;
{

  Delphi DUnit Test Case
  ----------------------
  This unit contains a skeleton test case class generated by the Test Case Wizard.
  Modify the generated code to correctly setup and call the methods from the unit 
  being tested.

}

interface

uses
  TestFramework, System.SysUtils, FMX.Controls.Presentation, FMX.Edit, FMX.Graphics,
  FMX.Dialogs, System.Variants, System.UITypes, FMX.Types, FMX.Controls, FMX.StdCtrls,
  FMX.Forms, System.Types, System.Classes, uMain;

type
  // Test methods for class TForm6

  TestTForm6 = class(TTestCase)
  strict private
    FForm6: TForm6;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestbtnMainClick;
  end;

implementation

procedure TestTForm6.SetUp;
begin
  FForm6 := TForm6.Create(nil);
end;

procedure TestTForm6.TearDown;
begin
  FForm6.Free;
  FForm6 := nil;
end;

procedure TestTForm6.TestbtnMainClick;
var
  Sender: TObject;
begin
  // TODO: Setup method call parameters
  FForm6.btnMainClick(Sender);
  // TODO: Validate method results
end;

initialization
  // Register any test cases with the test runner
  RegisterTest(TestTForm6.Suite);
end.

