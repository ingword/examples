unit UnitFormMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants, FMX.Forms, FMX.Types, FMX.Graphics, FMX.Ani, FMX.Controls,
  FMX.Objects, FMX.StdCtrls, FMX.Layouts, FMX.TabControl, UnitFrameWin,
  UnitFrameSettings, System.IniFiles, System.Math, System.StrUtils,
  System.UIConsts, System.DateUtils, System.IOUtils, FMX.Styles, FMX.Memo;

type
  TArrayCell = array of Integer;
  TArrayImage = array of String;

type
  TFormMain = class(TForm)
    TabControl1: TTabControl;
    tiMenu: TTabItem;
    tiPlayGame: TTabItem;
    sbPlay: TSpeedButton;
    sbSettings: TSpeedButton;
    Layout1: TLayout;
    rPlayingField: TRectangle;
    Timer1: TTimer;
    Image1: TImage;
    Layout2: TLayout;
    procedure FormCreate(Sender: TObject);
    procedure sbPlayClick(Sender: TObject);
    procedure sbNewGameClick(Sender: TObject);
    procedure sbMenuClick(Sender: TObject);
    procedure sbSettingsClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private
    { Private declarations }
    FSettingsIniFile: TIniFile;
    { SETTINGS }
    FComplexity: String;
    FDurationAni: Integer;
    FTypeBitmap: String;
    FNumberImages: Integer;
    FGenerateCells: String;
    FCountColumns: Integer;
    FCountLines: Integer;
    FColorPlayingField: String;
    FStroke: Boolean;

    FCountCells: Integer;
    FArrayImage: TArrayImage;
    FArrayCell: TArrayCell;
    FCell: TRectangle;
    FSolveCells: Integer;
    FTime: String;

    FFrameWin: TFrameWin;
    FFrameSettings: TFrameSettings;
  public
    { Public declarations }
    property pComplexity: String read FComplexity write FComplexity;
    property pDurationAni: Integer read FDurationAni write FDurationAni;
    property pTypeBitmap: String read FTypeBitmap write FTypeBitmap;
    property pNumberImages: Integer read FNumberImages write FNumberImages;
    property pGenerateCells: String read FGenerateCells write FGenerateCells;
    property pCountLines: Integer read FCountLines write FCountLines;
    property pCountColumns: Integer read FCountColumns write FCountColumns;
    property pColorPlayingField: String read FColorPlayingField write FColorPlayingField;
    property pStroke: Boolean read FStroke write FStroke;

    procedure CreateArrayImage;
    procedure CreateArrayCell(const KeyArrayImage: Integer);
    procedure AnimateBitmap(const AParent: TFmxObject;
      const APropertyName: string; const AniName: string; ADuration: Single = 5;
      const AStartValue: String = ''; AStopValue: String = ''; AFinish: TNotifyEvent = nil);
    procedure RectangleClick(Sender: TObject);
    procedure LoadSettingsIniFile;
    procedure CalculateSizeAndAddCell(GenerateCell: boolean);
    procedure ClearAllField;
    procedure ProtectiveLayerClose(Sender: TObject);
  end;

var
  FormMain: TFormMain;

implementation

{$R *.fmx}

uses
 FMX.Platform, FMX.VirtualKeyboard;

const
  RectangleName = 'Rect';
  AnimationName = 'BitmapAni';
  ProtectiveLayerName = 'ProtectiveLayerGame';

procedure TFormMain.RectangleClick(Sender: TObject);
var
  CellOne: TRectangle;
  AniBitmapOne, AniBitmapTwo: TBitmapAnimation;
begin
  if (Sender is TRectangle) then
  begin
    CellOne := TRectangle(Sender);
    AniBitmapOne := TBitmapAnimation(FindComponent(CellOne.TagString));
    AniBitmapOne.Duration := 0.5;
    AniBitmapOne.Inverse := True;
    AniBitmapOne.Start;
    if (FCell = nil) then
    begin
      FCell := CellOne;
    end
    else
    begin
      if (FCell.Tag = CellOne.Tag) AND (FCell.Name <> CellOne.Name) then
      begin
        CellOne.OnClick := nil;
        FCell.OnClick := nil;
        FSolveCells := FSolveCells + 2;
      end
      else
      begin
        AniBitmapOne.Inverse := False;
        AniBitmapOne.Start;

        AniBitmapTwo := TBitmapAnimation(FindComponent(FCell.TagString));
        AniBitmapTwo.Duration := 0.5;
        AniBitmapTwo.Inverse := False;
        AniBitmapTwo.Start;
      end;
      FCell := nil;
    end;
  end;

  if FSolveCells = FCountCells then
  begin
    Timer1.Enabled := False;
    FFrameWin := TFrameWin.Create(Self);
    FFrameWin.Parent := tiPlayGame;
    FFrameWin.Name := 'TFrameWin1';
    FFrameWin.sbNewGameTemp.OnClick := sbNewGameClick;
    FFrameWin.sbMenuTemp.OnClick := sbMenuClick;
    FFrameWin.LabelTime.Text := '�����: ' + FTime;
  end;

end;

// ��������� ������ ��� ��������� ����� �� ������� ����
procedure TFormMain.CreateArrayCell(const KeyArrayImage: Integer);
var
  RndNumOne, RndNumTwo: Integer;
begin

  RndNumOne := RandomRange(0, FCountCells);
  RndNumTwo := RandomRange(0, FCountCells);

  if (RndNumOne <> RndNumTwo) AND (FArrayCell[RndNumOne] = 0) AND
    (FArrayCell[RndNumTwo] = 0) then
  begin
    FArrayCell[RndNumOne] := KeyArrayImage;
    FArrayCell[RndNumTwo] := KeyArrayImage;
  end
  else
  begin
    CreateArrayCell(KeyArrayImage);
    exit;
  end;

end;

// ������� ���������� �������� � �������� � ��� � ������
procedure TFormMain.CreateArrayImage;
const
  ArrayTypeBitmap: Array[0..7] of String = ('All 163', 'FruitsVegetables 27', 'Dogs 24',
   'Cats 60', 'Christmas 12', 'Houses 15', 'StPatrick 12', 'Halloween 13');
var
  i, CellArrayImage, RndImage, j: Integer;
  CheckOne, CheckTwo: boolean;

  TempArray: TStringDynArray;
  TypeBitmaps: String;
  KeyType: Integer;
  ValueType: String;
  QuantityTypeImage: Integer;
  BitmapOne: String;
begin

  CheckOne := False;
  CellArrayImage := 0;

  for i := 0 to Length(FArrayImage) - 1 do
  begin
    if FArrayImage[i] = '' then
    begin
      CheckOne := True;
      CellArrayImage := i;
    end;
  end;

  if CheckOne then
  begin

    //��� ��������
    TempArray := SplitString(FTypeBitmap, ' ');
    TypeBitmaps := TempArray[0];

    // �������� �������� �������� �� ���� ��������� �����
    if TypeBitmaps = 'All' then
    begin

      // ���������� ����� ��� ��������
      KeyType := RandomRange(1, Length(ArrayTypeBitmap));

      // �������� ������
      SetLength(TempArray, 0);

      // ��������� ��������� ������
      TempArray := SplitString(ArrayTypeBitmap[KeyType], ' ');

      // ��������� �� ���������� �������: �������� ����
      ValueType := TempArray[0];

      // ��������� �� ���������� �������: ���������� ��������
      QuantityTypeImage := StrToInt(TempArray[1]);

      RndImage := RandomRange(1, QuantityTypeImage + 1);

      BitmapOne := ValueType + '_' + IntToStr(RndImage);

    end
    else
    begin

      RndImage := RandomRange(1, (FNumberImages div 2) + 1);

      BitmapOne := TypeBitmaps + '_' + IntToStr(RndImage);

    end;

    CheckTwo := False;

    for j := 0 to Length(FArrayImage) - 1 do
    begin
      if BitmapOne = FArrayImage[j] then
      begin
        CheckTwo := True;
        CreateArrayImage;
      end;
    end;

    if not CheckTwo then
    begin
      FArrayImage[CellArrayImage] := BitmapOne;
      CreateArrayCell(CellArrayImage);
    end;

  end;

end;

procedure TFormMain.sbPlayClick(Sender: TObject);
begin
  TabControl1.ActiveTab := tiPlayGame;
  sbNewGameClick(Self);
end;

procedure TFormMain.sbSettingsClick(Sender: TObject);
begin
  FFrameSettings := TFrameSettings.Create(Self);
  FFrameSettings.Parent := tiMenu;
  FFrameSettings.Name := 'TFrameSettings1';
  FFrameSettings.LoadSettings(FSettingsIniFile);
  FFrameSettings.sbMenu2.OnClick := sbMenuClick;
end;

procedure TFormMain.Timer1Timer(Sender: TObject);
begin
  FTime := TimeToStr(IncSecond(StrToTime(FTime), 1));
end;

procedure TFormMain.sbMenuClick(Sender: TObject);
begin
  TabControl1.ActiveTab := tiMenu;
  ClearAllField;
end;

procedure TFormMain.sbNewGameClick(Sender: TObject);
var
  ProtectiveLayer: TRectangle;
  i, j: Integer;
  CellOne: TRectangle;
  AniName: String;
begin

  ClearAllField;

  // ��������� ���������� ����� � ������ ��
  CalculateSizeAndAddCell(True);

  rPlayingField.Fill.Color := StringToAlphaColor(FColorPlayingField);

  // ����� ������� ��������
  SetLength(FArrayCell, FCountCells);
  SetLength(FArrayImage, FCountCells div 2);

  // ������� �������
  for i := 0 to Length(FArrayCell) - 1 do
  begin
    CreateArrayImage;
  end;

  // ��������� ������ � ������������ � ��������
  for j := 0 to Length(FArrayCell) - 1 do
  begin
    CellOne := TRectangle(FindComponent(RectangleName + IntToStr(j + 1)));
    CellOne.Tag := FArrayCell[j];
    CellOne.Fill.Kind := TBrushKind.Bitmap;
    CellOne.Fill.Bitmap.WrapMode := TWrapMode.TileStretch;

    AniName := AnimationName + IntToStr(j + 1);

    CellOne.TagString := AniName;

    CellOne.OnClick := RectangleClick;

    if j = (Length(FArrayCell) - 1) then
    begin
      AnimateBitmap(CellOne, 'Fill.Bitmap.Bitmap', AniName, FDurationAni.ToSingle,
        FArrayImage[FArrayCell[j]], 'Question', ProtectiveLayerClose);
    end
    else
    begin
      AnimateBitmap(CellOne, 'Fill.Bitmap.Bitmap', AniName, FDurationAni.ToSingle,
        FArrayImage[FArrayCell[j]], 'Question');
    end;

  end;

  if FDurationAni <> 0 then
  begin
    ProtectiveLayer := TRectangle.Create(Self);
    ProtectiveLayer.Parent := rPlayingField;
    ProtectiveLayer.Name := ProtectiveLayerName;
    ProtectiveLayer.Align := TAlignLayout.Contents;
    ProtectiveLayer.Opacity := 0.3;
    ProtectiveLayer.Stroke.Kind := TBrushKind.None;
  end;

end;

procedure TFormMain.AnimateBitmap(const AParent: TFmxObject;
  const APropertyName: string; const AniName: string; ADuration: Single = 5;
  const AStartValue: String = ''; AStopValue: String = ''; AFinish: TNotifyEvent = nil);
var
  A: TBitmapAnimation;
  Stream, StreamTwo: TResourceStream;
begin
  Stream := TResourceStream.Create(hInstance, AStartValue, RT_RCDATA);
  StreamTwo := TResourceStream.Create(hInstance, AStopValue, RT_RCDATA);
  try
    A := TBitmapAnimation.Create(Self);
    A.Parent := AParent;
    A.AnimationType := TAnimationType.In;
    A.Interpolation := TInterpolationType.Exponential;
    A.Duration := ADuration;
    A.PropertyName := APropertyName;
    A.StartValue.LoadFromStream(Stream);
    A.StopValue.LoadFromStream(StreamTwo);
    A.Name := AniName;
    A.OnFinish := AFinish;
    A.Start;
  finally
    Stream.Free;
    StreamTwo.Free;
  end;
end;

procedure TFormMain.CalculateSizeAndAddCell(GenerateCell: boolean);
const
  PlayingFieldIndent = 18;
  MaxSizeBitmap = 128;
  MinSizeBitmap = 70;
var
  PlayingFieldWidth, PlayingFieldHeight, CountColumns, CountLines: Integer;
  CellSize, CellIndentX, CellIndentY, x, i, j: Integer;
  A: TRectangle;
  StopCycle: boolean;
  n, k, TempCountCell: Integer;
begin

  PlayingFieldWidth := Trunc(Self.ClientWidth - PlayingFieldIndent);
  PlayingFieldHeight := Trunc(Self.ClientHeight - PlayingFieldIndent);

  if (FGenerateCells = 'automax') OR (FGenerateCells = 'automin') then
  begin

    if FGenerateCells = 'automax' then
    begin
      CellSize := MinSizeBitmap;
    end;

    if FGenerateCells = 'automin' then
    begin
      CellSize := MaxSizeBitmap;
    end;

    CountColumns := PlayingFieldWidth div CellSize;

    CountLines := PlayingFieldHeight div CellSize;

  end
  else
  begin
    if FGenerateCells = 'manual' then
    begin

      CountColumns := FCountColumns;

      CountLines := FCountLines;

      CellSize := (PlayingFieldWidth div CountColumns);
      if (CellSize * CountLines) > PlayingFieldHeight then
      begin
        CellSize := (PlayingFieldHeight div CountLines);
      end;

      if CellSize > MaxSizeBitmap then
      begin
        CellSize := MaxSizeBitmap;
        CountColumns := PlayingFieldWidth div CellSize;
        CountLines := PlayingFieldHeight div CellSize;
      end
      else
      begin
        if CellSize < MinSizeBitmap then
        begin
          CellSize := MinSizeBitmap;
          CountColumns := PlayingFieldWidth div CellSize;
          CountLines := PlayingFieldHeight div CellSize;
        end;
      end;
    end;
  end;

  if (CountColumns * CountLines) > FNumberImages then
  begin

    StopCycle := False;
    for n := CountColumns downto 3 do
    begin

      for k := CountLines downto 3 do
      begin

        TempCountCell := n * k;

        if (TempCountCell <= FNumberImages) AND
          (TempCountCell in [FNumberImages - 3 .. FNumberImages]) then
        begin
          CountColumns := n;
          CountLines := k;
          StopCycle := True;
          break;
        end;

      end;

      if StopCycle then
      begin
        break;
      end;

    end;

    CellSize := (PlayingFieldWidth div CountColumns);
    if (CellSize * CountLines) > PlayingFieldHeight then
    begin
      CellSize := (PlayingFieldHeight div CountLines);
    end;

  end;

  if (CountColumns mod 2 <> 0) AND (CountLines mod 2 <> 0) then
  begin
    if CountColumns >= CountLines then
    begin
      if PlayingFieldWidth > PlayingFieldHeight then
      begin
        Dec(CountLines);
      end
      else
      begin
        Dec(CountColumns);
      end;
    end
    else
    begin
      Dec(CountLines);
    end;

    CellSize := (PlayingFieldWidth div CountColumns);
    if (CellSize * CountLines) > PlayingFieldHeight then
    begin
      CellSize := (PlayingFieldHeight div CountLines);
    end;
  end;

  FCountColumns := CountColumns;
  FCountLines := CountLines;

  // ����� �����
  FCountCells := CountColumns * CountLines;

  if GenerateCell then
  begin
    rPlayingField.DeleteChildren;

    CellIndentX := ((PlayingFieldWidth - (CellSize * CountColumns)) div 2) +
      (PlayingFieldIndent div 2);
    CellIndentY := ((PlayingFieldHeight - (CellSize * CountLines)) div 2) +
      (PlayingFieldIndent div 2);

    x := 1;

    for i := 1 to CountLines do
    begin
      for j := 1 to CountColumns do
      begin
        A := TRectangle.Create(Self);
        A.Parent := rPlayingField;
        A.Name := RectangleName + x.ToString;
        A.Height := CellSize - 2;
        A.Width := CellSize - 2;

        if not FStroke then
        begin
          A.Stroke.Kind := TBrushKind.None;
        end;

        A.Position.x := (CellSize * j) - CellSize + CellIndentX + 1;
        A.Position.Y := (CellSize * i) - CellSize + CellIndentY + 1;

        Inc(x);
      end;
    end;
  end;

end;

procedure TFormMain.ClearAllField;
begin

  rPlayingField.DeleteChildren;

  SetLength(FArrayCell, 0);
  SetLength(FArrayImage, 0);

  if FFrameWin <> nil then
  begin
    FFrameWin.DisposeOf;
    FFrameWin := nil;
  end;

  if FFrameSettings <> nil then
  begin
    FFrameSettings.DisposeOf;
    FFrameSettings := nil;
  end;

  FSolveCells := 0;
  FCell := nil;

  FTime := '0:00:00';
  Timer1.Enabled := False;

end;

procedure TFormMain.FormCreate(Sender: TObject);
begin

  Randomize;

  // ���������� ���������
{$IFDEF ANDROID}
  FSettingsIniFile := TIniFile.Create(TPath.Combine(TPath.GetDocumentsPath, 'SettingsIniFile.ini'));
{$ELSE}
  FSettingsIniFile := TIniFile.Create('.\SettingsIniFile.ini');
{$ENDIF}
  LoadSettingsIniFile;

  TabControl1.ActiveTab := tiMenu;

  FCell := nil;

  // ����������� �����
  FSolveCells := 0;

  FFrameWin := nil;
  FFrameSettings := nil;

  FTime := '0:00:00';
  Timer1.Enabled := False;

end;

procedure TFormMain.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
var
  Keyboard: IFMXVirtualKeyboardService;
begin
  if (Key = vkHardwareBack)then
  begin
    if (TabControl1.ActiveTab <> tiMenu) OR (FFrameWin <> nil) OR (FFrameSettings <> nil) then
    begin
      if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(Keyboard)) then
      begin
        if TVirtualKeyBoardState.Visible in Keyboard.GetVirtualKeyBoardState then
        begin
          Keyboard.HideVirtualKeyboard;
        end
        else
        begin
          TabControl1.ActiveTab := tiMenu;
          ClearAllField;
          Key := 0;
        end;
      end
    end;
  end;
end;

// �������� ���������
procedure TFormMain.LoadSettingsIniFile;
begin
  FComplexity := FSettingsIniFile.ReadString('GAME', 'Complexity', 'easy');
  FDurationAni := FSettingsIniFile.ReadInteger('GAME', 'DurationAni', 5);
  FTypeBitmap := FSettingsIniFile.ReadString('GAME', 'TypeBitmap', 'All 0');
  FNumberImages := FSettingsIniFile.ReadInteger('GAME', 'NumberImages', 326);
  FGenerateCells := FSettingsIniFile.ReadString('PlayingField', 'GenerateCells',
    'automax');
  FCountColumns := FSettingsIniFile.ReadInteger('PlayingField',
    'CountColumns', 4);
  FCountLines := FSettingsIniFile.ReadInteger('PlayingField', 'CountLines', 3);
  FColorPlayingField := FSettingsIniFile.ReadString('PlayingField',
    'ColorPlayingField', '#FFE0E0E0');
  FStroke := FSettingsIniFile.ReadBool('PlayingField', 'Stroke', False);
end;

procedure TFormMain.ProtectiveLayerClose(Sender: TObject);
begin
  if FDurationAni <> 0 then
  begin
    TRectangle(FindComponent(ProtectiveLayerName)).DisposeOf;
  end;
  TBitmapAnimation(Sender).OnFinish := nil;
  Timer1.Enabled := True;
end;

end.
