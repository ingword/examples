unit UnitFrameWin;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Layouts;

type
  TFrameWin = class(TFrame)
    LayoutTemp: TLayout;
    LabelTextWin: TLabel;
    LayoutTemp2: TLayout;
    sbNewGameTemp: TSpeedButton;
    sbMenuTemp: TSpeedButton;
    LabelTime: TLabel;
    rBackground: TRectangle;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

end.
