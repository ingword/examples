unit UnitFrameSettings;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.ListBox, FMX.Layouts, System.IniFiles, System.StrUtils,
  FMX.Objects, FMX.Colors, System.UIConsts;

type
  TFrameSettings = class(TFrame)
    Rectangle111: TRectangle;
    ListBox1: TListBox;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    ListBoxItem1: TListBoxItem;
    cbComplexity: TComboBox;
    cbcEasy: TListBoxItem;
    cbcHard: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    cbTypeBitmap: TComboBox;
    cbtbAll: TListBoxItem;
    cbtbFruitsVegetables: TListBoxItem;
    cbtbDogs: TListBoxItem;
    cbtbCats: TListBoxItem;
    cbtbChristmas: TListBoxItem;
    cbtbHouses: TListBoxItem;
    cbtbStPatrick: TListBoxItem;
    cbtbHalloween: TListBoxItem;
    LabelCountBitmap: TLabel;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    ListBoxItem4: TListBoxItem;
    cbGenerateCells: TComboBox;
    cbncAutoMax: TListBoxItem;
    cbncAutoMin: TListBoxItem;
    cbncManual: TListBoxItem;
    ListBoxItem5: TListBoxItem;
    ListBoxItem6: TListBoxItem;
    ListBoxItem7: TListBoxItem;
    ccbColorPlayingField: TComboColorBox;
    ToolBar1: TToolBar;
    sbMenu2: TSpeedButton;
    ListBoxItem8: TListBoxItem;
    cbStroke: TCheckBox;
    sbDurationAni: TEdit;
    sbCountColumns: TEdit;
    sbCountLines: TEdit;
    procedure cbComplexityChange(Sender: TObject);
    procedure sbDurationAniChange(Sender: TObject);
    procedure cbTypeBitmapChange(Sender: TObject);
    procedure cbGenerateCellsChange(Sender: TObject);
    procedure sbCountColumnsChange(Sender: TObject);
    procedure sbCountLinesChange(Sender: TObject);
    procedure ccbColorPlayingFieldChange(Sender: TObject);
    procedure cbStrokeChange(Sender: TObject);
  private
    { Private declarations }
    { SETTINGS }
    FSettingsIniFile: TIniFile;
    FComplexity: String;
    FDurationAni: Integer;
    FTypeBitmap: String;
    FNumberImages: Integer;
    FGenerateCells: String;
    FCountColumns: Integer;
    FCountLines: Integer;
    FColorPlayingField: String;
    FStroke: Boolean;
  public
    { Public declarations }
    procedure LoadSettings(SettingsIniFile: TIniFile);
  end;

implementation

{$R *.fmx}
{ TFrame2 }

uses
  UnitFormMain;

// ��������� ����
procedure TFrameSettings.cbComplexityChange(Sender: TObject);
begin
  case cbComplexity.ItemIndex of
    0:
      FComplexity := 'easy';
    1:
      FComplexity := 'hard';
  else
    FComplexity := 'easy';
  end;

  if FComplexity = 'easy' then
  begin
    //sbDurationAni.Min := 1;
    sbDurationAni.Text := '5';
    sbDurationAni.Enabled := True;
  end
  else
  begin
    //sbDurationAni.Min := 0;
    sbDurationAni.Text := '0';
    sbDurationAni.Enabled := False;
  end;

  FormMain.pComplexity := FComplexity;
  FormMain.pDurationAni := StrToInt(sbDurationAni.Text);

  FSettingsIniFile.WriteString('GAME', 'Complexity', FormMain.pComplexity);
  FSettingsIniFile.WriteInteger('GAME', 'DurationAni', FormMain.pDurationAni);

end;

procedure TFrameSettings.cbGenerateCellsChange(Sender: TObject);
begin
  case cbGenerateCells.ItemIndex of
    0:
      FGenerateCells := 'automax';
    1:
      FGenerateCells := 'automin';
    2:
      FGenerateCells := 'manual';
  else
    FGenerateCells := 'automax';
  end;

  // ��������� ��������� ���������� �����
  FormMain.pGenerateCells := FGenerateCells;
  FormMain.CalculateSizeAndAddCell(False);

  if (FGenerateCells = 'automax') OR (FGenerateCells = 'automin') then
  begin
    sbCountColumns.Enabled := False;
    sbCountLines.Enabled := False;
  end
  else
  begin
    if FGenerateCells = 'manual' then
    begin
      sbCountColumns.Enabled := True;
      sbCountLines.Enabled := True;
    end;
  end;

  sbCountColumns.Text := FormMain.pCountColumns.ToString;
  sbCountLines.Text := FormMain.pCountLines.ToString;

  FSettingsIniFile.WriteString('PlayingField', 'GenerateCells',
    FormMain.pGenerateCells);
  FSettingsIniFile.WriteInteger('PlayingField', 'CountColumns',
    FormMain.pCountColumns);
  FSettingsIniFile.WriteInteger('PlayingField', 'CountLines',
    FormMain.pCountLines);

end;

procedure TFrameSettings.cbStrokeChange(Sender: TObject);
begin
  FormMain.pStroke := cbStroke.IsChecked;
  FSettingsIniFile.WriteBool('PlayingField', 'Stroke', FormMain.pStroke);
end;

procedure TFrameSettings.cbTypeBitmapChange(Sender: TObject);
begin
  case cbTypeBitmap.ItemIndex of
    0:
      FTypeBitmap := 'All';
    1:
      FTypeBitmap := 'FruitsVegetables';
    2:
      FTypeBitmap := 'Dogs';
    3:
      FTypeBitmap := 'Cats';
    4:
      FTypeBitmap := 'Christmas';
    5:
      FTypeBitmap := 'Houses';
    6:
      FTypeBitmap := 'StPatrick';
    7:
      FTypeBitmap := 'Halloween';
  else
    FTypeBitmap := 'All';
  end;

  FormMain.pNumberImages := cbTypeBitmap.Selected.Tag * 2;
  FSettingsIniFile.WriteInteger('GAME', 'NumberImages', FormMain.pNumberImages);

  LabelCountBitmap.Text := IntToStr(cbTypeBitmap.Selected.Tag);

  FormMain.pTypeBitmap := FTypeBitmap + ' ' + cbTypeBitmap.ItemIndex.ToString;

  FSettingsIniFile.WriteString('GAME', 'TypeBitmap', FormMain.pTypeBitmap);
end;

procedure TFrameSettings.ccbColorPlayingFieldChange(Sender: TObject);
begin
  FormMain.pColorPlayingField := AlphaColorToString(ccbColorPlayingField.Color);
  FSettingsIniFile.WriteString('PlayingField', 'ColorPlayingField',
    FormMain.pColorPlayingField);
end;

// ���������� ��� ��������� (������ FrameCreate)
procedure TFrameSettings.LoadSettings(SettingsIniFile: TIniFile);
var
  TypeBitmapTemp: TStringDynArray;
begin

  FSettingsIniFile := SettingsIniFile;

  // ��������� ��������� ���������� �����
  FormMain.CalculateSizeAndAddCell(False);

  FComplexity := FormMain.pComplexity;
  FDurationAni := FormMain.pDurationAni;
  FTypeBitmap := FormMain.pTypeBitmap;
  FGenerateCells := FormMain.pGenerateCells;
  FCountColumns := FormMain.pCountColumns;
  FCountLines := FormMain.pCountLines;
  FColorPlayingField := FormMain.pColorPlayingField;
  FStroke := FormMain.pStroke;

  // ���������: ��� ��������
  TypeBitmapTemp := SplitString(FTypeBitmap, ' ');
  FTypeBitmap := TypeBitmapTemp[0];
  cbTypeBitmap.ItemIndex := StrToInt(TypeBitmapTemp[1]);

  FNumberImages := FormMain.pNumberImages;
  LabelCountBitmap.Text := IntToStr(FNumberImages div 2);

  FSettingsIniFile.WriteString('GAME', 'Complexity', FComplexity);
  FSettingsIniFile.WriteInteger('GAME', 'DurationAni', FDurationAni);
  FSettingsIniFile.WriteString('GAME', 'TypeBitmap',
    FTypeBitmap + ' ' + cbTypeBitmap.ItemIndex.ToString);
  FSettingsIniFile.WriteInteger('GAME', 'NumberImages', FNumberImages);
  FSettingsIniFile.WriteString('PlayingField', 'GenerateCells', FGenerateCells);
  FSettingsIniFile.WriteInteger('PlayingField', 'CountColumns', FCountColumns);
  FSettingsIniFile.WriteInteger('PlayingField', 'CountLines', FCountLines);
  FSettingsIniFile.WriteString('PlayingField', 'ColorPlayingField',
    FColorPlayingField);
  FStroke := FSettingsIniFile.ReadBool('PlayingField', 'Stroke', True);

  // ���������: ��������� ���� � ����� �����������
  if FComplexity = 'easy' then
  begin
    cbComplexity.ItemIndex := 0;
    sbDurationAni.Text := FDurationAni.ToString;
    //sbDurationAni.Min := 1;
    sbDurationAni.Enabled := True;
  end
  else
  begin
    if FComplexity = 'hard' then
    begin
      cbComplexity.ItemIndex := 1;
      if FDurationAni > 0 then
      begin
        FDurationAni := 0;
      end;
      sbDurationAni.Text := FDurationAni.ToString;
      //sbDurationAni.Min := 0;
      sbDurationAni.Enabled := False;
    end;
  end;

  // ���������: ���������� �����(�������� � �����)
  if FGenerateCells = 'automax' then
  begin
    cbGenerateCells.ItemIndex := 0;
    sbCountColumns.Enabled := False;
    sbCountLines.Enabled := False;
  end
  else
  begin
    if FGenerateCells = 'automin' then
    begin
      cbGenerateCells.ItemIndex := 1;
      sbCountColumns.Enabled := False;
      sbCountLines.Enabled := False;
    end
    else
    begin
      if FGenerateCells = 'manual' then
      begin
        cbGenerateCells.ItemIndex := 2;
        sbCountColumns.Enabled := True;
        sbCountLines.Enabled := True;
      end;
    end;
  end;
  sbCountColumns.Text := FCountColumns.ToString;
  sbCountLines.Text := FCountLines.ToString;

  ccbColorPlayingField.Color := StringToAlphaColor(FColorPlayingField);

  cbStroke.IsChecked := FStroke;

end;

procedure TFrameSettings.sbCountColumnsChange(Sender: TObject);
begin
  FormMain.pCountColumns := StrToInt(sbCountColumns.Text);
  //FSettingsIniFile.WriteInteger('PlayingField', 'CountColumns',
  //  FormMain.pCountColumns);
end;

procedure TFrameSettings.sbCountLinesChange(Sender: TObject);
begin
  FormMain.pCountLines := StrToInt(sbCountLines.Text);
  //FSettingsIniFile.WriteInteger('PlayingField', 'CountLines',
  //  FormMain.pCountLines);
end;

procedure TFrameSettings.sbDurationAniChange(Sender: TObject);
begin
  FormMain.pDurationAni := StrToInt(sbDurationAni.Text);
  FSettingsIniFile.WriteInteger('GAME', 'DurationAni', FormMain.pDurationAni);
end;

end.
