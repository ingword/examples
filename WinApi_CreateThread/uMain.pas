unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TfmMain = class(TForm)
    btnStart: TButton;
    lblExecCount: TLabel;
    Memo1: TMemo;
    procedure btnStartClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMain: TfmMain;
  g_ThreadId: DWORD;
  g_ThreadHandle: THandle;
  g_ExecuteCount: Integer;

implementation

{$R *.dfm}


procedure Execute;
const
  c_MaxElements = 20;
var
  l_Text : string;
  l_I, l_Length : byte;
  l_ResultArr : array[0..c_MaxElements] of char;
begin
  while true do
  begin
    // �������� ������ ������
    l_Length := random(c_MaxElements);

    // ��������� ������ ��������� ����������� ��������
    for l_I := 0 to l_Length - 1 do
      l_ResultArr[l_I] := 'a';

    // ��� � ��� ������� :)
    l_Text := l_ResultArr;

    // ������ ������, ��� ��� �� �� �������������.
    for l_I := 0 to c_MaxElements - 1 do
      l_ResultArr[l_I] := #0;

    fmMain.Memo1.Lines.Add(l_Text);
    inc(g_ExecuteCount);
    fmMain.lblExecCount.Caption := IntToStr(g_ExecuteCount);
    sleep(10);
  end;
end;

procedure TfmMain.btnStartClick(Sender: TObject);
begin
  g_ThreadHandle := CreateThread(nil,
                                 0,
                                 @Execute,
                                 nil,
                                 0,
                                 g_ThreadId);
end;

end.
