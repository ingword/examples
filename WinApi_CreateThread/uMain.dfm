object fmMain: TfmMain
  Left = 0
  Top = 0
  Caption = 'fmMain'
  ClientHeight = 365
  ClientWidth = 295
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblExecCount: TLabel
    Left = 62
    Top = 24
    Width = 6
    Height = 13
    Caption = '0'
  end
  object btnStart: TButton
    Left = 30
    Top = 56
    Width = 75
    Height = 25
    Caption = 'btnStart'
    TabOrder = 0
    OnClick = btnStartClick
  end
  object Memo1: TMemo
    Left = 0
    Top = 104
    Width = 295
    Height = 261
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 1
  end
end
