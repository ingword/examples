unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Gestures,
  FMX.StdCtrls;

type
  TForm5 = class(TForm)
    pnlMain: TPanel;
    gstrmngrMain: TGestureManager;
    lblDirection: TLabel;
    lblLocation: TLabel;
    lblDistance: TLabel;
    lblGestureToIdent: TLabel;
    procedure pnlMainGesture(Sender: TObject;
      const EventInfo: TGestureEventInfo; var Handled: Boolean);
    procedure pnlMainMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation

{$R *.fmx}
{$R *.LgXhdpiTb.fmx ANDROID}

procedure TForm5.pnlMainGesture(Sender: TObject;
  const EventInfo: TGestureEventInfo; var Handled: Boolean);
var
 l_GestureName : string;
begin
 if EventInfo.GestureID = sgiLeft then lblDirection.Text := 'Left'
 else if EventInfo.GestureID = sgiRight then lblDirection.Text := 'Right'
 else if EventInfo.GestureID = sgiUp then lblDirection.Text := 'Up'
 else if EventInfo.GestureID = sgiDown then lblDirection.Text := 'Down'

 else if EventInfo.GestureID = sgiLeftRight then lblDirection.Text := 'LeftRight'
 else if EventInfo.GestureID = sgiRightLeft then lblDirection.Text := 'RightLeft'
 else if EventInfo.GestureID = sgiUpDown then lblDirection.Text := 'UpDown'
 else if EventInfo.GestureID = sgiDownUp then lblDirection.Text := 'DownUp'

 // ��� ������ ��� ����
 // http://docwiki.embarcadero.com/Libraries/XE7/en/FMX.Types.TInteractiveGesture
 else if EventInfo.GestureID = System.UITypes.igiPan then lblDirection.Text := lblDirection.Text + ' igiPan'
 else if EventInfo.GestureID = System.UITypes.igiLongTap then lblDirection.Text := 'LongTap';

 lblLocation.Text := 'X =' + FloatToStr(EventInfo.Location.X) + ';' +
                     'Y =' + FloatToStr(EventInfo.Location.Y) + ';';

 lblDistance.Text := 'Distance = ' + IntToStr(EventInfo.Distance);

 if GestureToIdent(EventInfo.GestureID, l_GestureName) then lblGestureToIdent.Text := l_GestureName;
 
end;

procedure TForm5.pnlMainMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Single);
begin
 Self.Caption := 'X =' + FloatToStr(X) + ';' +
                 'Y =' + FloatToStr(Y) + ';';
end;

end.
