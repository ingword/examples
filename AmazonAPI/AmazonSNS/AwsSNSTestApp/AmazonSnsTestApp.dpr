program AmazonSnsTestApp;

uses
  System.StartUpCopy,
  FMX.Forms,
  uFormMainAwsTest in 'uFormMainAwsTest.pas' {FormMainTest},
  AWS.CredentialsMgr in '..\common\AWS.CredentialsMgr.pas' {DMCredentialsMgr: TDataModule},
  AWS.Regions in '..\common\AWS.Regions.pas',
  AWS.SNS in '..\common\AWS.SNS.pas',
  AWS.Types in '..\common\AWS.Types.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormMainTest, FormMainTest);
  Application.CreateForm(TDMCredentialsMgr, DMCredentialsMgr);
  Application.Run;
end.
