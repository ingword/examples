unit uFormMainAwsTest;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.ScrollBox,
  FMX.Memo, FMX.Controls.Presentation, FMX.StdCtrls, FMX.TabControl, FMX.Layouts,
  FMX.ListBox, AWS.SNS, System.Actions, FMX.ActnList, FMX.Edit;

type
  TFormMainTest = class(TForm)
    Panel1: TPanel;
    MemoLog: TMemo;
    PanelClientTop: TPanel;
    Splitter1: TSplitter;
    LayoutClient: TLayout;
    cmbbxRegion: TComboBox;
    lblTopRegionSNS: TLabel;
    TabControlMenu: TTabControl;
    tbiMenuSNS: TTabItem;
    btnSNSListTopics: TButton;
    btnSNSListSubscriptions: TButton;
    btnSNSTopicCreate: TButton;
    btnSNSTopicDelete: TButton;
    btnSNSSubscribe: TButton;
    TabControlClient: TTabControl;
    tbiMemoLog: TTabItem;
    tbiSNSSubscription: TTabItem;
    btnDoSubscribe: TButton;
    edtAddSubsEndpoint: TEdit;
    lblEndpoint: TLabel;
    cmbbxAddSubsProtocol: TComboBox;
    lblProtocol: TLabel;
    cmbbxAddSubsTopic: TComboBox;
    lblTopic: TLabel;
    LabelTitle: TLabel;
    btnUnsubscribe: TButton;
    tbiSNSPublish: TTabItem;
    cmbbxTopicPublish: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    btnDoPublish: TButton;
    edtPublishMsg: TEdit;
    Label3: TLabel;
    edtPublishSubject: TEdit;
    Label4: TLabel;
    btnSNSPublish: TButton;
    btnSNSListSubsByTopic: TButton;
    TabItem1: TTabItem;
    procedure FormCreate(Sender: TObject);
    procedure btnSNSListTopicsClick(Sender: TObject);
    procedure btnSNSListSubscriptionsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSNSTopicCreateClick(Sender: TObject);
    procedure btnSNSTopicDeleteClick(Sender: TObject);
    procedure btnSNSSubscribeClick(Sender: TObject);
    procedure cmbbxAddSubsProtocolChange(Sender: TObject);
    procedure btnDoSubscribeClick(Sender: TObject);
    procedure btnUnsubscribeClick(Sender: TObject);
    procedure btnSNSPublishClick(Sender: TObject);
    procedure btnDoPublishClick(Sender: TObject);
    procedure btnSNSListSubsByTopicClick(Sender: TObject);
  private
    FAmazonSNS: TAmazonSNS;
    procedure DoLoadRegions;
    function GetCurrRegion: string;
    function GetSNS: TAmazonSNS;
    procedure LoadSNSTopics(cmb: TComboBox);
    procedure LoadSNSSubscriptions(cmb: TComboBox; aTopicARN: string = '');
  public
    { Public declarations }
  end;

var
  FormMainTest: TFormMainTest;

implementation

{$R *.fmx}

uses AWS.Regions, AWS.CredentialsMgr;

procedure TFormMainTest.FormCreate(Sender: TObject);
begin
  TabControlClient.ActiveTab := tbiMemoLog;

  DoLoadRegions;
end;

procedure TFormMainTest.DoLoadRegions;
var i: integer; reg: TAWSRegionRec;
begin
  cmbbxRegion.BeginUpdate;
  try
    cmbbxRegion.Items.Clear;
    for i := 0 to TAWSRegions.GetCount-1 do
    begin
      reg := TAWSRegions.GetRec(i);
      cmbbxRegion.Items.Add(reg.Description);
    end;

    cmbbxRegion.ItemIndex := 3; // set to "eu-west-1"

  finally
    cmbbxRegion.EndUpdate;
  end;
end;

procedure TFormMainTest.FormDestroy(Sender: TObject);
begin
  FAmazonSNS.Free;
end;

function TFormMainTest.GetCurrRegion: string;
var i: integer;
begin
  i := cmbbxRegion.ItemIndex;
  Result := TAWSRegions.GetRec(i).Name;
end;

function TFormMainTest.GetSNS: TAmazonSNS;
begin
  if FAmazonSNS = nil then
  begin
    FAmazonSNS := TAmazonSNS.Create(DMCredentialsMgr.DefaultAmazonConnectionInfo);
    FAmazonSNS.Protocol := 'https';
  end;
  FAmazonSNS.Region := GetCurrRegion;
  Result := FAmazonSNS;
end;

procedure TFormMainTest.LoadSNSSubscriptions(cmb: TComboBox; aTopicARN: string);
var i, aCount: integer; aList: TSNSSubscriptions;
begin
  cmb.BeginUpdate;
  try
    cmb.Items.Clear;
    aList := TSNSSubscriptions.Create;
    try
      if aTopicARN <> '' then
        GetSNS.ListSubscriptionsByTopic(aTopicARN, aList)
      else
        GetSNS.ListSubscriptions(aList);

      aCount := aList.Count;
      if aCount > 0 then
      begin
        for i := 0 to aList.Count-1 do
        begin
          cmb.Items.Add(aList[i].SubscriptionArn);
        end;
      end;
    finally
      aList.Free;
    end;

    if cmb.Count > 0 then
      cmb.ItemIndex := 0;

  finally
    cmb.EndUpdate;
  end;

end;

procedure TFormMainTest.LoadSNSTopics(cmb: TComboBox);
var i, aCount: integer; aTopics: TSNSTopics;
begin
  cmb.BeginUpdate;
  try
    cmb.Items.Clear;
    aTopics := TSNSTopics.Create;
    try
      GetSNS.ListTopics(aTopics);
      aCount := aTopics.Count;
      if aCount > 0 then
      begin
        for i := 0 to aTopics.Count-1 do
        begin
          cmb.Items.Add(aTopics[i].ARN);
        end;
      end;
    finally
      aTopics.Free;
    end;

    if cmb.Count > 0 then
      cmb.ItemIndex := 0;

  finally
    cmb.EndUpdate;
  end;
end;

procedure TFormMainTest.btnSNSListSubsByTopicClick(Sender: TObject);
var topicArn, s: string;
begin
  TabControlClient.ActiveTab := tbiMemoLog;

  topicArn := InputBox('List Subscriptions by Topic','Topic ARN','');
  if topicArn = '' then
    ShowMessage('Topic ARN cannot be empty')
  else
  begin
    MemoLog.BeginUpdate;
    try
      MemoLog.Lines.Clear;
      s := GetSNS.ListSubscriptionsByTopicXML(topicArn);
      MemoLog.Text := s;
    finally
      MemoLog.EndUpdate;
    end;

//    LoadSNSSubscriptions(cmbbxSubscriptions, topicARN);

  end;

end;

procedure TFormMainTest.btnSNSListSubscriptionsClick(Sender: TObject);
var s: string;
begin
  MemoLog.BeginUpdate;
  try
    MemoLog.Lines.Clear;
    s := GetSNS.ListSubscriptionsXML;
    MemoLog.Text := s;
  finally
    MemoLog.EndUpdate;
  end;

//  LoadSNSSubscriptions(cmbbxSubscriptions);
end;

procedure TFormMainTest.btnSNSListTopicsClick(Sender: TObject);
var s: string;
begin
  TabControlClient.ActiveTab := tbiMemoLog;
  MemoLog.BeginUpdate;
  try
    MemoLog.Lines.Clear;
    s := GetSNS.ListTopicsXML;
    MemoLog.Text := s;
  finally
    MemoLog.EndUpdate;
  end;
end;

procedure TFormMainTest.btnSNSPublishClick(Sender: TObject);
begin
  TabControlClient.ActiveTab := tbiSNSPublish;

  LoadSNSTopics(cmbbxTopicPublish);
end;

procedure TFormMainTest.btnSNSSubscribeClick(Sender: TObject);
begin
  TabControlClient.ActiveTab := tbiSNSSubscription;

  LoadSNSTopics(cmbbxAddSubsTopic);
end;

procedure TFormMainTest.btnSNSTopicCreateClick(Sender: TObject);
var topicName, s: string;
begin
  TabControlClient.ActiveTab := tbiMemoLog;

  topicName := InputBox('Create SNS Topic','Topic name','my-topic');
  if topicName = '' then
    ShowMessage('Topic name cannot be empty')
  else if Length(topicName) > 256 then
    ShowMessage('Topic name cannot exceed 256 characters')
  else
  begin
    MemoLog.BeginUpdate;
    try
      MemoLog.Lines.Clear;
      s := GetSNS.CreateTopic(topicName);
      MemoLog.Text := s;
    finally
      MemoLog.EndUpdate;
    end;

  end;
end;

procedure TFormMainTest.btnSNSTopicDeleteClick(Sender: TObject);
var topicArn, s: string;
begin
  TabControlClient.ActiveTab := tbiMemoLog;

  topicArn := InputBox('Delete SNS Topic','Topic ARN','');
  if topicArn = '' then
    ShowMessage('Topic ARN cannot be empty')
  else
  begin
    MemoLog.BeginUpdate;
    try
      MemoLog.Lines.Clear;
      s := GetSNS.DeleteTopic(topicArn);
      MemoLog.Text := s;
    finally
      MemoLog.EndUpdate;
    end;

  end;
end;

procedure TFormMainTest.btnUnsubscribeClick(Sender: TObject);
var s, arn: string;
begin
  TabControlClient.ActiveTab := tbiMemoLog;

  arn := InputBox('Unsubscribe','Subscription ARN','');
  if arn = '' then
    ShowMessage('Subscription ARN cannot be empty')
  else
  begin
    MemoLog.BeginUpdate;
    try
      MemoLog.Lines.Clear;
      s := GetSNS.Unsubscribe(arn);
      MemoLog.Text := s;
    finally
      MemoLog.EndUpdate;
    end;
  end;
end;

procedure TFormMainTest.btnDoPublishClick(Sender: TObject);
var s, aTopicArn, aSubject, aMsg: string;
begin
  TabControlClient.ActiveTab := tbiMemoLog;

  aTopicArn := cmbbxTopicPublish.Selected.Text;
  aSubject := edtPublishSubject.Text;
  aMsg := edtPublishMsg.Text;

  MemoLog.BeginUpdate;
  try
    MemoLog.Lines.Clear;
    s := GetSNS.PublishXML(aTopicArn, aSubject, aMsg);
    MemoLog.Text := s;
  finally
    MemoLog.EndUpdate;
  end;

end;

procedure TFormMainTest.btnDoSubscribeClick(Sender: TObject);
var s, aTopicArn, aProtocol, aEndpoint: string;
begin
  TabControlClient.ActiveTab := tbiMemoLog;

  aTopicArn := cmbbxAddSubsTopic.Selected.Text;
  aProtocol := cmbbxAddSubsProtocol.Selected.Text;
  aEndpoint := edtAddSubsEndpoint.Text;

  MemoLog.BeginUpdate;
  try
    MemoLog.Lines.Clear;
    s := GetSNS.SubscribeXML(aEndpoint, aProtocol, aTopicArn);
    MemoLog.Text := s;
  finally
    MemoLog.EndUpdate;
  end;

end;

procedure TFormMainTest.cmbbxAddSubsProtocolChange(Sender: TObject);
begin
  if cmbbxAddSubsProtocol.Selected.Text = 'http' then
    edtAddSubsEndpoint.Hint := 'URL beginning with "http://"'

  else if cmbbxAddSubsProtocol.Selected.Text = 'https' then
    edtAddSubsEndpoint.Hint := 'URL beginning with "https://"'

  else if cmbbxAddSubsProtocol.Selected.Text = 'email' then
    edtAddSubsEndpoint.Hint := 'email address'

  else if cmbbxAddSubsProtocol.Selected.Text = 'email-json' then
    edtAddSubsEndpoint.Hint := 'email address'

  else if cmbbxAddSubsProtocol.Selected.Text = 'sms' then
    edtAddSubsEndpoint.Hint := 'a phone number of an SMS-enabled device'

  else if cmbbxAddSubsProtocol.Selected.Text = 'sqs' then
    edtAddSubsEndpoint.Hint := 'ARN of an Amazon SQS queue'

  else if cmbbxAddSubsProtocol.Selected.Text = 'application' then
    edtAddSubsEndpoint.Hint := 'EndpointArn of a mobile app and device'

  else if cmbbxAddSubsProtocol.Selected.Text = 'lambda' then
    edtAddSubsEndpoint.Hint := 'ARN of an AWS Lambda function'

  else
    edtAddSubsEndpoint.Hint := '';


end;

end.
