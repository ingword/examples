unit AWS.SNS;

interface

uses
  System.Classes, System.SysUtils, Data.Cloud.CloudAPI, Data.Cloud.AmazonAPI,
  System.Generics.Collections, AWS.Types;

type
  EAWSSNS = class(EAWS);

  TSNSTopic = class
  private
    FARN: string;
    FName: string;
    procedure SetARN(const Value: string);
  public
    constructor Create(aArn: string);
    property ARN: string read FARN write SetARN;
    property Name: string read FName;
  end;

  TSNSTopics = TObjectList<TSNSTopic>;

  TSNSSubscription = class
  private
    FProtocol: string;
    FTopicArn: string;
    FOwner: string;
    FSubscriptionArn: string;
    FEndpoint: string;
  public
    property Protocol: string read FProtocol write FProtocol;
    property Owner: string read FOwner write FOwner;
    property TopicArn: string read FTopicArn write FTopicArn;
    property SubscriptionArn: string read FSubscriptionArn write FSubscriptionArn;
    property Endpoint: string read FEndpoint write FEndpoint;
  end;

  TSNSSubscriptions = TObjectList<TSNSSubscription>;

  TAmazonSNS = class(TAmazonBasicService)
  private
    FProtocol: string;
    FRegion: string;
    procedure SubscriptionsXMLToList(xml: string; aSubscriptions: TSNSSubscriptions);
  protected
    function GetServiceVersion: string; override;
    function GetServiceHost: string; override;
    function GetServiceURL: string;
  public
    constructor Create(const ConnectionInfo: TAmazonConnectionInfo);

    property Protocol: string read FProtocol write FProtocol;
    property Region: string read FRegion write FRegion;

    // http://docs.aws.amazon.com/sns/latest/api/API_ListTopics.html
    function ListTopicsXML(OptionalParams: TStrings = nil; ResponseInfo: TCloudResponseInfo = nil): string;
    procedure ListTopics(aTopics: TSNSTopics; OptionalParams: TStrings = nil; ResponseInfo: TCloudResponseInfo = nil);

    // http://docs.aws.amazon.com/sns/latest/api/API_CreateTopic.html
    function CreateTopic(Name: string; OptionalParams: TStrings = nil; ResponseInfo: TCloudResponseInfo = nil): string;

    // http://docs.aws.amazon.com/sns/latest/api/API_DeleteTopic.html
    function DeleteTopic(TopicArn: string; OptionalParams: TStrings = nil; ResponseInfo: TCloudResponseInfo = nil): string;

    // http://docs.aws.amazon.com/sns/latest/api/API_ListSubscriptions.html
    function ListSubscriptionsXML(OptionalParams: TStrings = nil;
      ResponseInfo: TCloudResponseInfo = nil): string;
    procedure ListSubscriptions(aSubscriptions: TSNSSubscriptions; OptionalParams: TStrings = nil;
      ResponseInfo: TCloudResponseInfo = nil);

    // http://docs.aws.amazon.com/sns/latest/api/API_ListSubscriptionsByTopic.html
    function ListSubscriptionsByTopicXML(TopicArn: string; OptionalParams: TStrings = nil;
      ResponseInfo: TCloudResponseInfo = nil): string;
    procedure ListSubscriptionsByTopic(TopicArn: string; aSubscriptions: TSNSSubscriptions; OptionalParams: TStrings = nil;
      ResponseInfo: TCloudResponseInfo = nil);

    // http://docs.aws.amazon.com/sns/latest/api/API_Subscribe.html
    function SubscribeXML(Endpoint: string; Protocol: string; TopicArn: string;
      OptionalParams: TStrings = nil; ResponseInfo: TCloudResponseInfo = nil): string;

    // http://docs.aws.amazon.com/sns/latest/api/API_Unsubscribe.html
    function Unsubscribe(SubscriptionArn: string;
      OptionalParams: TStrings = nil; ResponseInfo: TCloudResponseInfo = nil): string;

    // http://docs.aws.amazon.com/sns/latest/api/API_Publish.html
    function PublishXML(TopicArn: string; Subject: string; Msg: string;
      OptionalParams: TStrings = nil; ResponseInfo: TCloudResponseInfo = nil): string;

  end;


implementation

uses
  Xml.XMLIntf, Xml.XMLDoc;

{ TAmazonSNS }

constructor TAmazonSNS.Create(const ConnectionInfo: TAmazonConnectionInfo);
begin
  inherited Create(ConnectionInfo);

  // set defaults
  FProtocol := 'https';
  FRegion := 'eu-west-1';
end;

function TAmazonSNS.GetServiceVersion: string;
begin
  Result := '2010-03-31';
end;

function TAmazonSNS.GetServiceHost: string;
begin
  Result := Format('sns.%s.amazonaws.com', [Region]);
end;

function TAmazonSNS.GetServiceURL: string;
begin
  Result := Format('%s://%s', [Protocol, GetServiceHost]);
end;

function TAmazonSNS.CreateTopic(Name: string; OptionalParams: TStrings;
  ResponseInfo: TCloudResponseInfo): string;
var
  Response: TCloudHTTP;
  QueryParams: TStringList;
begin
  if Name.Length = 0 then
    raise EAWSSNS.Create('SNS topic cannot be empty');

  if Name.Length > 256 then
    raise EAWSSNS.Create('SNS topic cannot exceed 256 characters');

  QueryParams := BuildQueryParameters('CreateTopic');

  QueryParams.Insert(0,'Name=' + Name);

  if OptionalParams <> nil then
    QueryParams.AddStrings(OptionalParams);

  Response := nil;
  try
    Response := IssueRequest(GetServiceURL, QueryParams, ResponseInfo, Result);
  finally
    Response.Free;
    QueryParams.Free;
  end;
end;

function TAmazonSNS.DeleteTopic(TopicArn: string; OptionalParams: TStrings;
  ResponseInfo: TCloudResponseInfo): string;
var
  Response: TCloudHTTP;
  QueryParams: TStringList;
begin
  QueryParams := BuildQueryParameters('DeleteTopic');

  QueryParams.Insert(0,'TopicArn=' + TopicArn);

  if OptionalParams <> nil then
    QueryParams.AddStrings(OptionalParams);

  Response := nil;
  try
    Response := IssueRequest(GetServiceURL, QueryParams, ResponseInfo, Result);
  finally
    Response.Free;
    QueryParams.Free;
  end;
end;

function TAmazonSNS.ListTopicsXML(OptionalParams: TStrings;
  ResponseInfo: TCloudResponseInfo): string;
var
  aURL: string;
  Response: TCloudHTTP;
  QueryParams: TStringList;
begin
  QueryParams := BuildQueryParameters('ListTopics');

  if OptionalParams <> nil then
    QueryParams.AddStrings(OptionalParams);

  Response := nil;
  try
    aURL := GetServiceURL;
    Response := IssueRequest(aURL, QueryParams, ResponseInfo, Result);
  finally
    Response.Free;
    QueryParams.Free;
  end;
end;

function TAmazonSNS.PublishXML(TopicArn, Subject, Msg: string;
  OptionalParams: TStrings; ResponseInfo: TCloudResponseInfo): string;
var
  Response: TCloudHTTP;
  QueryParams: TStringList;
begin
  if Subject.Length > 100 then
    raise EAWSSNS.Create('SNS subject cannot exceed 100 characters');

  QueryParams := BuildQueryParameters('Publish');

  QueryParams.Insert(0,'Message=' + msg);
  QueryParams.Insert(0,'TopicArn=' + TopicArn);
  QueryParams.Insert(0,'Subject=' + Subject);

  if OptionalParams <> nil then
    QueryParams.AddStrings(OptionalParams);

  Response := nil;
  try
    Response := IssueRequest(GetServiceURL, QueryParams, ResponseInfo, Result);
  finally
    Response.Free;
    QueryParams.Free;
  end;
end;

function TAmazonSNS.SubscribeXML(Endpoint, Protocol, TopicArn: string;
  OptionalParams: TStrings; ResponseInfo: TCloudResponseInfo): string;
var
  Response: TCloudHTTP;
  QueryParams: TStringList;
begin
  QueryParams := BuildQueryParameters('Subscribe');

  QueryParams.Insert(0,'TopicArn=' + TopicArn);
  QueryParams.Insert(0,'Protocol=' + Protocol);
  QueryParams.Insert(0,'Endpoint=' + Endpoint);

  if OptionalParams <> nil then
    QueryParams.AddStrings(OptionalParams);

  Response := nil;
  try
    Response := IssueRequest(GetServiceURL, QueryParams, ResponseInfo, Result);
  finally
    Response.Free;
    QueryParams.Free;
  end;
end;

function TAmazonSNS.Unsubscribe(SubscriptionArn: string;
  OptionalParams: TStrings; ResponseInfo: TCloudResponseInfo): string;
var
  Response: TCloudHTTP;
  QueryParams: TStringList;
begin
  QueryParams := BuildQueryParameters('Unsubscribe');

  QueryParams.Insert(0,'SubscriptionArn=' + SubscriptionArn);

  if OptionalParams <> nil then
    QueryParams.AddStrings(OptionalParams);

  Response := nil;
  try
    Response := IssueRequest(GetServiceURL, QueryParams, ResponseInfo, Result);
  finally
    Response.Free;
    QueryParams.Free;
  end;
end;

procedure TAmazonSNS.ListTopics(aTopics: TSNSTopics;
  OptionalParams: TStrings; ResponseInfo: TCloudResponseInfo);
var xml: string;
  xmlDoc: IXMLDocument;
  nodeListTopicsResult: IXMLNode;
  nodeTopics: IXMLNode;
  memberList: IXMLNodeList;
  i: integer;
  nodeMember: IXMLNode;
  nodeTopicArn: IXMLNode;
begin
  if aTopics <> nil then
  begin
    aTopics.Clear;

    xml := ListTopicsXML(OptionalParams, ResponseInfo);

    if xml = EmptyStr then
      Exit;

    xmlDoc := TXMLDocument.Create(nil);
    xmlDoc.LoadFromXML(XML);

    nodeListTopicsResult := xmlDoc.DocumentElement.ChildNodes.FindNode('ListTopicsResult');

    if (nodeListTopicsResult <> nil) and (nodeListTopicsResult.HasChildNodes) then
    begin
      nodeTopics := nodeListTopicsResult.ChildNodes.FindNode('Topics');

      if (nodeTopics <> nil) and (nodeTopics.HasChildNodes) then
      begin
        memberList := nodeTopics.ChildNodes;
        for i := 0 to memberList.Count-1 do
        begin
          nodeMember := memberList.Get(i);
          if nodeMember.HasChildNodes then
          begin
            nodeTopicArn := nodeMember.ChildNodes.FindNode('TopicArn');
            if nodeTopicArn <> nil then
              aTopics.Add(TSNSTopic.Create(nodeTopicArn.Text));
          end;
        end;
      end;
    end;
  end;
end;

function TAmazonSNS.ListSubscriptionsXML(OptionalParams: TStrings;
  ResponseInfo: TCloudResponseInfo): string;
var
  Response: TCloudHTTP;
  QueryParams: TStringList;
begin
  // TODO: handle returning more than 100 subscriptions via a received token

  QueryParams := BuildQueryParameters('ListSubscriptions');

  if OptionalParams <> nil then
    QueryParams.AddStrings(OptionalParams);

  Response := nil;
  try
    Response := IssueRequest(GetServiceURL, QueryParams, ResponseInfo, Result);
  finally
    Response.Free;
    QueryParams.Free;
  end;

end;

procedure TAmazonSNS.SubscriptionsXMLToList(xml: string;
  aSubscriptions: TSNSSubscriptions);
var
  xmlDoc: IXMLDocument;
  nodeListSubscriptionsResult: IXMLNode;
  nodeSubscriptions: IXMLNode;
  i: integer;
  subscriptionList: IXMLNodeList;
  nodeSubscription: IXMLNode;
  nodeProtocol,
  nodeOwner,
  nodeTopicArn,
  nodeSubscriptionArn,
  nodeEndpoint: IXMLNode;
  subscription: TSNSSubscription;
begin
  if aSubscriptions <> nil then
  begin
    aSubscriptions.Clear;

    xmlDoc := TXMLDocument.Create(nil);
    xmlDoc.LoadFromXML(XML);

    nodeListSubscriptionsResult := xmlDoc.DocumentElement.ChildNodes.FindNode('ListSubscriptionsResult');

    if (nodeListSubscriptionsResult <> nil) and (nodeListSubscriptionsResult.HasChildNodes) then
    begin
      nodeSubscriptions := nodeListSubscriptionsResult.ChildNodes.FindNode('Subscriptions');

      if (nodeSubscriptions <> nil) and (nodeSubscriptions.HasChildNodes) then
      begin
        subscriptionList := nodeSubscriptions.ChildNodes;
        for i := 0 to subscriptionList.Count-1 do
        begin
          subscription := TSNSSubscription.Create;

          nodeSubscription := subscriptionList.Get(i);
          if nodeSubscription.HasChildNodes then
          begin
            nodeProtocol := nodeSubscription.ChildNodes.FindNode('Protocol');
            if nodeProtocol <> nil then
              subscription.Protocol := nodeProtocol.Text;

            nodeOwner := nodeSubscription.ChildNodes.FindNode('Owner');
            if nodeOwner <> nil then
              subscription.Owner := nodeOwner.Text;

            nodeTopicArn := nodeSubscription.ChildNodes.FindNode('TopicArn');
            if nodeTopicArn <> nil then
              subscription.TopicArn := nodeTopicArn.Text;

            nodeSubscriptionArn := nodeSubscription.ChildNodes.FindNode('SubscriptionArn');
            if nodeSubscriptionArn <> nil then
              subscription.SubscriptionArn := nodeSubscriptionArn.Text;

            nodeEndpoint := nodeSubscription.ChildNodes.FindNode('Endpoint');
            if nodeEndpoint <> nil then
              subscription.Endpoint := nodeEndpoint.Text;

            aSubscriptions.Add(subscription);
          end;
        end;
      end;
    end;
  end;
end;

procedure TAmazonSNS.ListSubscriptions(aSubscriptions: TSNSSubscriptions;
  OptionalParams: TStrings; ResponseInfo: TCloudResponseInfo);
var xml: string;
begin
  xml := ListSubscriptionsXML(OptionalParams, ResponseInfo);

  if xml = EmptyStr then
    Exit;

  SubscriptionsXMLToList(xml, aSubscriptions);
end;

procedure TAmazonSNS.ListSubscriptionsByTopic(TopicArn: string;
  aSubscriptions: TSNSSubscriptions; OptionalParams: TStrings;
  ResponseInfo: TCloudResponseInfo);
var xml: string;
begin
  xml := ListSubscriptionsByTopicXML(TopicArn, OptionalParams, ResponseInfo);

  if xml = EmptyStr then
    Exit;

  SubscriptionsXMLToList(xml, aSubscriptions);

end;

function TAmazonSNS.ListSubscriptionsByTopicXML(TopicArn: string;
  OptionalParams: TStrings; ResponseInfo: TCloudResponseInfo): string;
var
  Response: TCloudHTTP;
  QueryParams: TStringList;
begin
  QueryParams := BuildQueryParameters('ListSubscriptionsByTopic');

  QueryParams.Insert(0,'TopicArn=' + TopicArn);

  if OptionalParams <> nil then
    QueryParams.AddStrings(OptionalParams);

  Response := nil;
  try
    Response := IssueRequest(GetServiceURL, QueryParams, ResponseInfo, Result);
  finally
    Response.Free;
    QueryParams.Free;
  end;
end;

{ TSNSTopic }

constructor TSNSTopic.Create(aARN: string);
begin
  ARN := aARN;
end;

procedure TSNSTopic.SetARN(const Value: string);
var i: integer;
begin
  FARN := Value;
  i := LastDelimiter(':', FARN);
  FName := copy(FARN, i+1, Length(FARN));
end;

end.
