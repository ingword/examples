unit AWS.CredentialsMgr;

interface

uses
  System.SysUtils, System.Classes, IPPeerClient, Data.Cloud.CloudAPI,
  Data.Cloud.AmazonAPI;

type
  TDMCredentialsMgr = class(TDataModule)
    AmazonConnectionInfo1: TAmazonConnectionInfo;
  private
    { Private declarations }
  public
    function DefaultAmazonConnectionInfo: TAmazonConnectionInfo;
  end;

var
  DMCredentialsMgr: TDMCredentialsMgr;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TDMCredentialsMgr }

function TDMCredentialsMgr.DefaultAmazonConnectionInfo: TAmazonConnectionInfo;
begin
  Result := AmazonConnectionInfo1;
end;

end.
