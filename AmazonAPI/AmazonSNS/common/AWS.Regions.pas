unit AWS.Regions;

interface

type
  TAWSRegionRec = record
    Name: string;
    Description: string;
  end;

  TAWSRegion = class
  private
    FName: string;
    FDescription: string;
    procedure SetDescription(const Value: string);
    procedure SetName(const Value: string);
  public
    constructor Create(AName, ADescription: string);
    property Name: string read FName write SetName;
    property Description: string read FDescription write SetDescription;
  end;

  TAWSRegions = class
  public
    class function GetCount: integer;
    class function GetRec(i: integer): TAWSRegionRec;
    class function GetItem(i: integer): TAWSRegion;
  end;

implementation

{ TAWSRegion }

class function TAWSRegions.GetCount: integer;
begin
  Result := 9;
end;

class function TAWSRegions.GetItem(i: integer): TAWSRegion;
var rec: TAWSRegionRec;
begin
  rec := GetRec(i);
  Result := TAWSRegion.Create(rec.Name, rec.Description);
end;

class function TAWSRegions.GetRec(i: integer): TAWSRegionRec;
begin
  if i = 0 then
  begin
    Result.Name := 'us-east-1';
    Result.Description := 'US East (N.Virginia)';
  end

  else if i = 1 then
  begin
    Result.Name := 'us-west-2';
    Result.Description := 'US West (Oregon)';
  end

  else if i = 2 then
  begin
    Result.Name := 'us-west-1';
    Result.Description := 'US West (N. California)';
  end

  else if i = 3 then
  begin
    Result.Name := 'eu-west-1';
    Result.Description := 'EU (Ireland)';
  end

  else if i = 4 then
  begin
    Result.Name := 'eu-central-1';
    Result.Description := 'EU (Frankfurt)';
  end

  else if i = 5 then
  begin
    Result.Name := 'ap-southeast-1';
    Result.Description := 'Asia Pacific (Singapore)';
  end

  else if i = 6 then
  begin
    Result.Name := 'ap-southeast-2';
    Result.Description := 'Asia Pacific (Sydney)';
  end

  else if i = 7 then
  begin
    Result.Name := 'ap-northeast-1';
    Result.Description := 'Asia Pacific (Tokyo)';
  end

  else if i = 8 then
  begin
    Result.Name := 'sa-east-1';
    Result.Description := 'South America (Sao Paulo)';
  end

  // unknown - empty
  else
  begin
    Result.Name := '';
    Result.Description := '';
  end

end;

{ TAWSRegion }

constructor TAWSRegion.Create(AName, ADescription: string);
begin
  FName := aName;
  FDescription := aDescription;
end;

procedure TAWSRegion.SetDescription(const Value: string);
begin
  FDescription := Value;
end;

procedure TAWSRegion.SetName(const Value: string);
begin
  FName := Value;
end;

end.
