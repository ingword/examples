unit uHelpers;

interface

// http://stackoverflow.com/questions/8666411/enumerate-twebrequest-http-header-fields

uses
  IdHTTPWebBrokerBridge, IdCustomHTTPServer, IdHTTPHeaderInfo;

type
  TIdHTTPAppRequestHelper = class helper for TIdHTTPAppRequest
  public
    function GetRequestInfo: TIdEntityHeaderInfo;
    function GetContent: string;
  end;

function UnQuote(s: string): string;

implementation

function UnQuote(s: string): string;
begin
  Result := Copy(s,2,Length(s)-2);
end;

{ TIdHTTPAppRequestHelper }

function TIdHTTPAppRequestHelper.GetContent: string;
begin
  Result := ReadString(FContentStream.Size);
end;

function TIdHTTPAppRequestHelper.GetRequestInfo: TIdEntityHeaderInfo;
begin
  Result := FRequestInfo;
end;

end.
