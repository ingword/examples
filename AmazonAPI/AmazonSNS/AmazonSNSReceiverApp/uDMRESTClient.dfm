object DMRESTClient: TDMRESTClient
  OldCreateOrder = False
  Height = 101
  Width = 314
  object RESTResponse1: TRESTResponse
    Left = 211
    Top = 30
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Params = <
      item
        name = 'Action'
        Value = 'ConfirmSubscription'
      end
      item
        name = 'TopicArn'
        Value = 'arn:aws:sns:eu-west-1:057711458419:TestTopic2'
      end
      item
        name = 'Token'
        Value = 
          '2336412f37fb687f5d51e6e241d638b0577eef7173cfdfc95e6b9fef404d8d43' +
          'dfb440bc9b7eb38e77c1dd132da574965cd93734a4a6c17a84058e268840d214' +
          'c39a721f736a98c54bbd6c4aa88738d5654b7d0f2f02b08fa67f4372bd6f0214' +
          '928a2137be6d5e5a72b4a433c00c7dc3'
      end>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 35
    Top = 23
  end
  object RESTClient1: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    AcceptEncoding = 'identity'
    BaseURL = 'https://sns.eu-west-1.amazonaws.com'
    Params = <>
    HandleRedirects = True
    Left = 114
    Top = 19
  end
end
