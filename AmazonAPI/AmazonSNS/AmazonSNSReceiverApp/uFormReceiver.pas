unit uFormReceiver;

interface

uses
  Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.AppEvnts, Vcl.StdCtrls, IdHTTPWebBrokerBridge, Web.HTTPApp, Vcl.ExtCtrls,
  Vcl.ComCtrls, IPPeerClient, REST.Client, Data.Bind.Components,
  Data.Bind.ObjectScope;

type
  TFormReceiver = class(TForm)
    ButtonStart: TButton;
    ButtonStop: TButton;
    EditPort: TEdit;
    LabelPort: TLabel;
    ApplicationEvents1: TApplicationEvents;
    PanelTop: TPanel;
    StatusBar1: TStatusBar;
    PanelClient: TPanel;
    ButtonClear: TButton;
    MemoLog: TMemo;
    RESTRequest1: TRESTRequest;
    RESTClient1: TRESTClient;
    RESTResponse1: TRESTResponse;
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure ButtonStartClick(Sender: TObject);
    procedure ButtonStopClick(Sender: TObject);
    procedure ButtonClearClick(Sender: TObject);
  private
    FServer: TIdHTTPWebBrokerBridge;
    procedure StartServer;
    function GetTime: string;
  public
    procedure Log(s: string);
    function DoSNSConfirmRequest(aURL: string): integer;
  end;

var
  FormReceiver: TFormReceiver;

implementation

{$R *.dfm}

uses
  WinApi.Windows, Winapi.ShellApi;

procedure TFormReceiver.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
  ButtonStart.Enabled := not FServer.Active;
  ButtonStop.Enabled := FServer.Active;
  EditPort.Enabled := not FServer.Active;
end;

procedure TFormReceiver.ButtonClearClick(Sender: TObject);
begin
  MemoLog.Clear;
end;

procedure TFormReceiver.ButtonStartClick(Sender: TObject);
begin
  StartServer;
end;

procedure TFormReceiver.ButtonStopClick(Sender: TObject);
begin
  FServer.Active := False;
  FServer.Bindings.Clear;
end;

procedure TFormReceiver.FormCreate(Sender: TObject);
begin
  FServer := TIdHTTPWebBrokerBridge.Create(Self);
end;

function TFormReceiver.GetTime: string;
var h,m,s,ms: word;
begin
  DecodeTime(Now, h, m, s, ms);
  Result := h.ToString + ':' + m.ToString + ':' + s.ToString + ',' + ms.ToString;
end;

procedure TFormReceiver.Log(s: string);
begin
  MemoLog.Lines.Add(GetTime + ': ' + s);
end;

procedure TFormReceiver.StartServer;
begin
  if not FServer.Active then
  begin
    FServer.Bindings.Clear;
    FServer.DefaultPort := StrToInt(EditPort.Text);
    FServer.Active := True;
  end;
end;

function TFormReceiver.DoSNSConfirmRequest(aURL: string): integer;
var baseURL: string; par1, par2, par3: string; p: integer;
begin
  p := Pos('/?',aURL)-1;
  baseURL := copy(aURL,0,p);
  aURL := copy(aURL, p+3, aURL.Length);
  p := Pos('&', aURL);
  par1 := copy(aURL, 0, p-1);
  aURL := copy(aURL, p+1, aURL.Length);
  p := Pos('&', aURL);
  par2 := copy(aURL, 0, p-1);
  par3 := copy(aURL, p+1, aURL.Length);

  p := Pos('=', par1);
  par1 := copy(par1, p+1, par1.Length);

  p := Pos('=', par2);
  par2 := copy(par2, p+1, par2.Length);

  p := Pos('=', par3);
  par3 := copy(par3, p+1, par3.Length);

  RESTClient1.BaseURL := baseURL;
  RESTRequest1.Params[0].Value := par1;
  RESTRequest1.Params[1].Value := par2;
  RESTRequest1.Params[2].Value := par3;

  Log('BaseURL: ' + baseURL);
  Log('par1: ' + par1);
  Log('par2: ' + par2);
  Log('par3: ' + par3);

  try
    RESTRequest1.Execute;
  except
    on e: Exception
      do Log('[' + e.ClassName + '] ' + e.Message);
  end;

  Result := RESTResponse1.StatusCode;
  Log('status code: ' + Result.ToString);
end;

end.
