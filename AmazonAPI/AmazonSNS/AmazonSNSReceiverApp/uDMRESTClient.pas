unit uDMRESTClient;

interface

uses
  System.SysUtils, System.Classes, IPPeerClient, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope;

type
  TDMRESTClient = class(TDataModule)
    RESTResponse1: TRESTResponse;
    RESTRequest1: TRESTRequest;
    RESTClient1: TRESTClient;
  private
    { Private declarations }
  public
    function DoSNSConfirmRequest(aURL: string): integer;
  end;

var
  DMRESTClient: TDMRESTClient;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDMRESTClient }

function TDMRESTClient.DoSNSConfirmRequest(aURL: string): integer;
var baseURL: string; par1, par2, par3: string; p: integer;

begin
  p := Pos('/?',aURL)-1;
  baseURL := copy(aURL,0,p);
  aURL := copy(aURL, p+3, aURL.Length);
  p := Pos('&', aURL);
  par1 := copy(aURL, 0, p-1);
  aURL := copy(aURL, p+1, aURL.Length);
  p := Pos('&', aURL);
  par2 := copy(aURL, 0, p-1);
  par3 := copy(aURL, p+1, aURL.Length);

  p := Pos('=', par1);
  par1 := copy(par1, p+1, par1.Length);

  p := Pos('=', par2);
  par2 := copy(par2, p+1, par2.Length);

  p := Pos('=', par3);
  par3 := copy(par3, p+1, par3.Length);

  RESTClient1.BaseURL := baseURL;
  RESTRequest1.Params[0].Value := par1;
  RESTRequest1.Params[1].Value := par2;
  RESTRequest1.Params[2].Value := par3;

  RESTRequest1.Execute;

  Result := RESTResponse1.StatusCode;
end;

end.
