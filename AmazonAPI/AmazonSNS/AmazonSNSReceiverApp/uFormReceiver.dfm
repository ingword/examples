object FormReceiver: TFormReceiver
  Left = 271
  Top = 114
  Caption = 'Amazon SNS - Test Receiver App'
  ClientHeight = 417
  ClientWidth = 713
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 713
    Height = 41
    Align = alTop
    TabOrder = 0
    object LabelPort: TLabel
      Left = 184
      Top = 13
      Width = 20
      Height = 13
      Caption = 'Port'
    end
    object ButtonStart: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = ButtonStartClick
    end
    object ButtonStop: TButton
      Left = 89
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 1
      OnClick = ButtonStopClick
    end
    object EditPort: TEdit
      Left = 210
      Top = 10
      Width = 49
      Height = 21
      TabOrder = 2
      Text = '8999'
    end
    object ButtonClear: TButton
      Left = 276
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Clear Log'
      TabOrder = 3
      OnClick = ButtonClearClick
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 398
    Width = 713
    Height = 19
    Panels = <>
  end
  object PanelClient: TPanel
    Left = 0
    Top = 41
    Width = 713
    Height = 357
    Align = alClient
    TabOrder = 2
    object MemoLog: TMemo
      Left = 1
      Top = 1
      Width = 711
      Height = 355
      Align = alClient
      TabOrder = 0
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 388
    Top = 65
  end
  object RESTRequest1: TRESTRequest
    Client = RESTClient1
    Params = <
      item
        name = 'Action'
        Value = 'ConfirmSubscription'
      end
      item
        name = 'TopicArn'
        Value = 'arn:aws:sns:eu-west-1:057711458419:TestTopic2'
      end
      item
        name = 'Token'
        Value = 
          '2336412f37fb687f5d51e6e241d638b0577eef7173cfdfc95e6b9fef404d8d43' +
          'dfb440bc9b7eb38e77c1dd132da574965cd93734a4a6c17a84058e268840d214' +
          'c39a721f736a98c54bbd6c4aa88738d5654b7d0f2f02b08fa67f4372bd6f0214' +
          '928a2137be6d5e5a72b4a433c00c7dc3'
      end>
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 128
    Top = 191
  end
  object RESTClient1: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    AcceptEncoding = 'identity'
    BaseURL = 'https://sns.eu-west-1.amazonaws.com'
    Params = <>
    HandleRedirects = True
    Left = 207
    Top = 187
  end
  object RESTResponse1: TRESTResponse
    Left = 304
    Top = 198
  end
end
