unit WebModuleUnit1;

interface

uses System.SysUtils, System.Classes, Web.HTTPApp, uHelpers;

type
  TWebModule1 = class(TWebModule)
    procedure WebModule1DefaultHandlerAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  private
    procedure Log(s: string);
    procedure LogRequest(Request: TWebRequest);
    procedure ProcessAWSSNSMsg(Request: TWebRequest);
  public
    { Public declarations }
  end;

var
  WebModuleClass: TComponentClass = TWebModule1;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uFormReceiver, IdHTTPHeaderInfo, IdCustomHTTPServer, IdHTTPWebBrokerBridge,
  IdHeaderList, System.JSON;

{$R *.dfm}

procedure TWebModule1.Log(s: string);
begin
  FormReceiver.Log(s);
end;

procedure TWebModule1.LogRequest(Request: TWebRequest);
var hl: TIdHeaderList; var req: TIdHTTPAppRequest; i: integer;
begin
  if Request <> nil then
  begin
    if Request is TIdHTTPAppRequest then
    begin
      req := TIdHTTPAppRequest(Request);

      hl := req.GetRequestInfo.RawHeaders;
      if hl <> nil then
      begin
        for i := 0 to hl.Count-1 do
        begin
          Log(hl[i]);
        end
      end
      else
        Log('No headers found');

      Log('');
      Log('==============================');
      Log(req.GetContent);
      Log('==============================');

    end;
  end;
end;

procedure TWebModule1.ProcessAWSSNSMsg(Request: TWebRequest);
var hl: TIdHeaderList; var req: TIdHTTPAppRequest;
  s: string; body: TJSONObject; aSubscribeURL: string;
  snsSubject, snsMessage: string; jsonStr: string;
  jsonV: TJSONValue;
begin
  if Request <> nil then
  begin
    Log('Request received');
    if Request is TIdHTTPAppRequest then
    begin
      req := TIdHTTPAppRequest(Request);
      hl := req.GetRequestInfo.RawHeaders;
      s := hl.Values['x-amz-sns-message-type'];

      if s = '' then
      begin
        Log('Failed to find "x-amz-sns-message-type" message header.');
        exit;
      end;

      jsonStr := req.GetContent.Trim;

      jsonV := TJSONObject.ParseJSONValue(jsonStr);
      if jsonV <> nil then
      begin
        if jsonV is TJSONObject then
        begin
          body := TJSONObject(jsonV);

          if SameText(s, 'Notification') then
          begin
            Log('Received SNS Notification');
            snsSubject := body.Values['Subject'].ToString;
            Log('Subject: ' + snsSubject);
            snsMessage := body.Values['Message'].ToString;
            Log('Message: ' + snsMessage);
          end

          else if SameText(s, 'SubscriptionConfirmation') then
          begin
            Log('SNS Subscription Confirmation Received');
            aSubscribeURL := body.Values['SubscribeURL'].ToString;
            aSubscribeURL := UnQuote(aSubscribeURL);

            Log('SubscribeURL is: ' + aSubscribeURL);
            FormReceiver.DoSNSConfirmRequest(aSubscribeURL);
          end

          else if SameText(s, 'UnsubscribeConfirmation') then
          begin
            Log('Received SNS Unsubscribe Confirmation');
          end

          else
            Log('Unknown SNS message type: ' + s);

        end
        else
        begin
          Log('request body is not JSON object');
        end;

      end
      else
        Log('failed to parse request body as JSON');

    end;
  end;
end;

procedure TWebModule1.WebModule1DefaultHandlerAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  Handled := True;
//  LogRequest(Request);
  ProcessAWSSNSMsg(Request);
  Log('========================================');
end;

end.
