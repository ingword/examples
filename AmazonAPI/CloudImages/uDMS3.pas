unit uDMS3;

interface

uses
  System.SysUtils, System.Classes, FMX.Graphics,
  Data.Cloud.CloudAPI, Data.Cloud.AmazonAPI;

type
  TDMS3 = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
  private
    FS3: TAmazonStorageService;
    function GetS3: TAmazonStorageService;
    property S3: TAmazonStorageService read GetS3;
    function GetTargetBucket: string;
  public
    function UploadBitmap(aBitmap: TBitmap): string;
    function DownloadBitmap(imgname: string): TBitmap;
  end;

var
  DMS3: TDMS3;

implementation

uses AWS.CredentialsMgr, uConfig, uIDGen;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TDMS3.DataModuleDestroy(Sender: TObject);
begin
  FS3.Free;
end;

function TDMS3.GetS3: TAmazonStorageService;
begin
  if FS3 = nil then
    FS3 := TAmazonStorageService.Create(DMCredentialsMgr.DefaultAmazonConnectionInfo);

  Result := FS3;
end;

function TDMS3.GetTargetBucket: string;
begin
  Result := TConfig.GetDefaultS3Bucket;
end;

function TDMS3.UploadBitmap(aBitmap: TBitmap): string;
var bstr: TBytesStream; success: boolean; objname: string;
begin
  Result := '';

  if aBitmap <> nil then
  begin
    bstr := TBytesStream.Create;
    try
      aBitmap.SaveToStream(bstr);
      objname := TIDGen.NewID;
      success := S3.UploadObject(GetTargetBucket, objname, bstr.Bytes);

      if success then
        Result := objname
      else
        Result := '';
    finally
      bstr.Free;
    end;
  end;
end;

function TDMS3.DownloadBitmap(imgname: string): TBitmap;
var success: boolean; objstr: TMemoryStream;
begin
  Result := nil;

  objstr := TMemoryStream.Create;
  try
    success := s3.GetObject(GetTargetBucket, imgname, objstr);
    if success then
    begin
      Result := TBitmap.Create;
      Result.LoadFromStream(objstr);
    end;
  finally
    objstr.Free;
  end;
end;

end.
