unit uDMSimpleDB;

interface

uses
  System.SysUtils, System.Classes, System.Generics.Collections,
  Data.Cloud.CloudAPI, Data.Cloud.AmazonAPI;

type
  TDMSimpleDB = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
  private
    FSimpleDB: TAmazonTableService;
    function SimpleDB: TAmazonTableService;
  public
    procedure InsertImageInfo(s3objname: string);
    procedure GetImageList(s3objnames: TStringList);
  end;

var
  DMSimpleDB: TDMSimpleDB;

implementation

uses AWS.CredentialsMgr, uConfig, uIDGen;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TDMSimpleDB.DataModuleDestroy(Sender: TObject);
begin
  FSimpleDB.Free;
end;

procedure TDMSimpleDB.GetImageList(s3objnames: TStringList);
var
  aTableName: string;
  aColName: string;
  ResponseList: TList<TCloudTableRow>;
  Row: TCloudTableRow;
  Col: TCloudTableColumn;
  Msg, ALine: String;
begin
  s3objnames.Clear;

  aTableName := TConfig.GetCloudImagesTableName;
  aColName := TConfig.GetCloudImagesAttrName;

  ResponseList := nil;

  try
    ResponseList := SimpleDB.GetRows(aTableName);

    if responseList <> nil then
    begin
      for Row in ResponseList do
      begin
        Col := Row.GetColumn(aColName);
        s3objnames.Add(Col.Value);

        Row.Free;
      end;
    end;

  finally
    ResponseList.Free;
  end;
end;

procedure TDMSimpleDB.InsertImageInfo(s3objname: string);
var
  aTableRow: TCloudTableRow;
  aRowId: String;
  aTableName: string;
  aColName: string;
  success: boolean;
begin
  aTableName := TConfig.GetCloudImagesTableName;
  aColName := TConfig.GetCloudImagesAttrName;
  aRowId := TIDGen.NewID;
  aTableRow := TCloudTableRow.Create;
  try
    aTableRow.SetColumn(aColName, s3objname);
    success := SimpleDB.InsertRow(aTableName, aRowId, aTableRow, nil);
  finally
    aTableRow.Free;
  end;
end;

function TDMSimpleDB.SimpleDB: TAmazonTableService;
begin
  if FSimpleDB = nil then
    FSimpleDB := TAmazonTableService.Create(DMCredentialsMgr.DefaultAmazonConnectionInfo);

  Result := FSimpleDB;
end;

end.
