unit uConfig;

interface

// http://blogs.aws.amazon.com/security/post/Tx3VRSWZ6B3SHAV/Writing-IAM-Policies-How-to-grant-access-to-an-Amazon-S3-bucket

// s3pawelstorageaccess - inline IAM policy

//{
//  "Version": "2012-10-17",
//  "Statement": [
//    {
//      "Effect": "Allow",
//      "Action": ["s3:ListBucket"],
//      "Resource": ["arn:aws:s3:::pawelstorage"]
//    },
//    {
//      "Effect": "Allow",
//      "Action": [
//        "s3:PutObject",
//        "s3:GetObject",
//        "s3:DeleteObject"
//      ],
//      "Resource": ["arn:aws:s3:::pawelstorage/*"]
//    }
//  ]
//}

type
  TConfig = class
  public
    class function GetDefaultS3Bucket: string;
    class function GetCloudImagesTableName: string;
    class function GetCloudImagesAttrName: string;
  end;

implementation

{ TConfig }

class function TConfig.GetCloudImagesAttrName: string;
begin
  Result := 's3objname';
end;

class function TConfig.GetCloudImagesTableName: string;
begin
  Result := 'cloudimages';
end;

class function TConfig.GetDefaultS3Bucket: string;
begin
  Result := 'pawelstorage';
end;

end.
