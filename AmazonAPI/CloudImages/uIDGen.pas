unit uIDGen;

interface

type
  TIDGen = class
    class function NewID: string;
  end;

implementation

uses
  SysUtils;

{ TIDGen }

const
  PREFIX = 'A';


function Pad2(s: string): string;
begin
  if s.Length = 1 then
    Result := '0' + s
  else
    Result := s;
end;

function Pad3(s: string): string;
begin
  if s.Length = 3 then
    Result := s
  else if s.Length = 2 then
    Result := '0' + s
  else if s.Length = 1 then
    Result := '00' + s;
end;


class function TIDGen.NewID: string;
var dt: TDateTime; y,m,d,h,min,s,ms : word; x: string;
begin
  dt := Now;
  DecodeDate(dt,y,m,d);
  DecodeTime(dt,h,min,s,ms);

  x := IntToStr(random(900)+100);

  Result := PREFIX + y.ToString + Pad2(m.ToString) + Pad2(d.ToString) +
    Pad2(h.ToString) + Pad2(min.ToString) + Pad2(s.ToString) + Pad3(s.ToString) + x;

end;

initialization
  Randomize;

end.
