unit uFormMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Actions,
  FMX.ActnList, FMX.StdCtrls, FMX.Controls.Presentation, FMX.TabControl,
  FMX.Objects, FMX.Edit, FMX.StdActns, FMX.MediaLibrary.Actions,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView;

type
  TFormMain = class(TForm)
    TabControl1: TTabControl;
    tbiPhoto: TTabItem;
    tbiList: TTabItem;
    tlbrPhotos: TToolBar;
    spdbtnTakePhoto: TSpeedButton;
    spdbtnUploadPhoto: TSpeedButton;
    ActionList1: TActionList;
    edtStatus: TEdit;
    imgPhoto: TImage;
    TakePhotoFromCameraAction1: TTakePhotoFromCameraAction;
    ListViewImages: TListView;
    ToolBar1: TToolBar;
    spdbtnRefreshList: TSpeedButton;
    procedure imgPhotoClick(Sender: TObject);
    procedure TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
    procedure spdbtnUploadPhotoClick(Sender: TObject);
    procedure spdbtnRefreshListClick(Sender: TObject);
    procedure ListViewImagesItemClick(const Sender: TObject;
      const AItem: TListViewItem);
  private
    procedure RefreshImageList;
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.fmx}

uses uDMS3, uDMSimpleDB;

procedure TFormMain.imgPhotoClick(Sender: TObject);
var
  lOpenDialog: TOpenDialog;
begin
  //Do nothing on Android and iOS, they use camera
  if TOSVersion.Platform in [TOSVersion.TPlatform.pfWindows, TOSVersion.TPlatform.pfMacOS] then
  begin
    lOpenDialog := TOpenDialog.Create(Self);
    lOpenDialog.Filter := 'JPG|*.jpg|PNG|*.png';
    if lOpenDialog.Execute then
      imgPhoto.Bitmap.LoadFromFile(lOpenDialog.FileName);
    lOpenDialog.Free;
  end;
end;

procedure TFormMain.ListViewImagesItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var bmp: TBitmap;
begin
  bmp := DMS3.DownloadBitmap(AItem.Text);

  imgPhoto.Bitmap.Assign(bmp);

  TabControl1.ActiveTab := tbiPhoto;
end;

procedure TFormMain.RefreshImageList;
var i: integer; aList: TStringList; aItem: TListViewItem;
begin
  aList := TStringList.Create;
  try
    ListViewImages.BeginUpdate;
    try
      ListViewImages.Items.Clear;

      DMSimpleDB.GetImageList(aList);
      for i := 0 to aList.Count-1 do
      begin
        aItem := ListViewImages.Items.Add;
        aItem.Text := aList[i];
      end;

    finally
      ListViewImages.EndUpdate;
    end;

  finally
    aList.Free;
  end;
end;

procedure TFormMain.spdbtnRefreshListClick(Sender: TObject);
begin
  RefreshImageList;
end;

procedure TFormMain.spdbtnUploadPhotoClick(Sender: TObject);
var s: string;
begin
  if imgPhoto.Bitmap <> nil then
  begin
    edtStatus.Text := 'Uploading photo. Please wait...';
    Application.ProcessMessages;

    s := DMS3.UploadBitmap(imgPhoto.Bitmap);

    if s = '' then
      edtStatus.Text := 'Failed to upload photo'
    else
    begin
      DMSimpleDB.InsertImageInfo(s);
      edtStatus.Text := 'OK - ' + s;
    end;
  end
  else
    edtStatus.Text := 'Take photo before uploading';
end;

procedure TFormMain.TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
begin
  imgPhoto.Bitmap.Assign(Image);
end;

end.
