program CloudImages;

uses
  System.StartUpCopy,
  FMX.Forms,
  uFormMain in 'uFormMain.pas' {FormMain},
  uIDGen in 'uIDGen.pas',
  uConfig in 'uConfig.pas',
  uDMS3 in 'uDMS3.pas' {DMS3: TDataModule},
  AWS.CredentialsMgr in 'AWS.CredentialsMgr.pas' {DMCredentialsMgr: TDataModule},
  uDMSimpleDB in 'uDMSimpleDB.pas' {DMSimpleDB: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TDMS3, DMS3);
  Application.CreateForm(TDMCredentialsMgr, DMCredentialsMgr);
  Application.CreateForm(TDMSimpleDB, DMSimpleDB);
  Application.Run;
end.
