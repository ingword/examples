object DMCredentialsMgr: TDMCredentialsMgr
  OldCreateOrder = False
  Height = 150
  Width = 215
  object AmazonConnectionInfo1: TAmazonConnectionInfo
    AccountName = 'Your_Account_Name_here'
    AccountKey = 'Your_Account_Key_here'
    TableEndpoint = 'sdb.amazonaws.com'
    QueueEndpoint = 'queue.amazonaws.com'
    StorageEndpoint = 's3-eu-west-1.amazonaws.com'
    UseDefaultEndpoints = False
    Left = 79
    Top = 45
  end
end
