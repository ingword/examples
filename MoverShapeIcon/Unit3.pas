﻿unit Unit3;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects;

type
  TForm3 = class(TForm)
    imgMain: TImage;
    btnPaint: TButton;
    btnLineBuild: TButton;
    btnLineRotate: TButton;
    imgLineRotate: TImage;
    procedure btnPaintClick(Sender: TObject);
    procedure btnLineBuildClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

Uses
 System.Math.Vectors
 ;
{$R *.fmx}

const
 c_ArrowLength = 20;
 c_ArrowWidth = 15;
 c_TriangleWidth = 10;
 c_TriangleHeight = 15;

type
 TmsDirection = (dTop, dBottom, dRight, dLeft);
 TTmsDirection = set of TmsDirection;

function BuildArrow(const aStartPoint: TPointF; const aDirection: TTmsDirection): TPolygon;
var
 l_Polygon : TPolygon;
 l_Point: TPointF;
 l_X, l_Y : Single;
 l_Invert : SmallInt;

begin
 l_Invert := 1;
 SetLength(l_Polygon, 7);

 l_Point := aStartPoint;
 l_Polygon[0] := l_Point;

 if (dLeft in aDirection) or (dBottom in aDirection) then
  l_Invert := -1;

 if (dTop in aDirection) or
    (dBottom in aDirection) then
 begin
  l_Y := l_Point.Y - (c_ArrowLength * l_Invert);
  l_Point := TPointF.Create(l_Point.X, l_Y);
  l_Polygon[1] := l_Point;

  l_X := l_Point.X - (c_TriangleWidth * l_Invert);
  l_Point := TPointF.Create(l_X, l_Point.Y);
  l_Polygon[2] := l_Point;

  l_X := l_Point.X + (c_TriangleWidth + c_ArrowWidth / 2) * l_Invert;
  l_Y := l_Point.Y - (c_TriangleHeight * l_Invert);
  l_Point := TPointF.Create(l_X, l_Y);
  l_Polygon[3] := l_Point;

  l_X := l_Point.X + (c_TriangleWidth + c_ArrowWidth / 2) * l_Invert;
  l_Y := l_Point.Y + (c_TriangleHeight * l_Invert);
  l_Point := TPointF.Create(l_X, l_Y);
  l_Polygon[4] := l_Point;

  l_X := l_Point.X - (c_TriangleWidth * l_Invert);
  l_Y := l_Point.Y;
  l_Point := TPointF.Create(l_X, l_Y);
  l_Polygon[5] := l_Point;

  l_Y := l_Point.Y + (c_ArrowLength * l_invert);
  l_Point := TPointF.Create(l_Point.X, l_Y);
  l_Polygon[6] := l_Point;
  Result := l_Polygon;
 end;

 if (dRight in aDirection) or
    (dLeft in aDirection) then
 begin
  l_X := l_Point.X + (c_ArrowLength * l_Invert);
  l_Point := TPointF.Create(l_X, l_Point.Y);
  l_Polygon[1] := l_Point;

  l_Y :=  l_Point.Y - (c_TriangleWidth * l_Invert);
  l_Point := TPointF.Create(l_Point.X, l_Y);
  l_Polygon[2] := l_Point;

  l_X := l_Point.X + (c_TriangleHeight * l_Invert);
  l_Y := l_Point.Y + (c_TriangleWidth + c_ArrowWidth / 2) * l_Invert;
  l_Point := TPointF.Create(l_X, l_Y);
  l_Polygon[3] := l_Point;

  l_X := l_Point.X - (c_TriangleHeight * l_Invert);
  l_Y := l_Point.Y + (c_TriangleWidth + c_ArrowWidth / 2) * l_Invert;
  l_Point := TPointF.Create(l_X, l_Y);
  l_Polygon[4] := l_Point;

  l_Y := l_Point.Y - (c_TriangleWidth * l_Invert);
  l_Point := TPointF.Create(l_Point.X, l_Y);;
  l_Polygon[5] := l_Point;

  l_X := l_Point.X - (c_ArrowLength * l_Invert);
  l_Point := TPointF.Create(l_X, l_Point.Y);
  l_Polygon[6] := l_Point;
  Result := l_Polygon;
 end;
end;

procedure TForm3.btnLineBuildClick(Sender: TObject);
var
 l_Polygon : TPolygon;
 l_StartPoint, l_Point: TPointF;
 l_RectF: TRectF;
begin
 //
 l_StartPoint := TPointF.Create(imgLineRotate.Position.X,
                                imgLineRotate.Position.Y);

 l_StartPoint.Offset(TPointF.Create(30, 30));

 l_Point := TPointF.Create(l_StartPoint.X, l_StartPoint.Y);

 l_Polygon := l_Polygon + BuildArrow(l_Point, [TmsDirection.dTop]);
 // Нарисовали стрелку вверх

 l_Point := (l_Polygon[High(l_Polygon)]);
 l_Polygon := l_Polygon + BuildArrow(l_Point, [TmsDirection.dRight]);
 // Нарисовали стрелку вправо

 l_Point := (l_Polygon[High(l_Polygon)]);
 l_Polygon := l_Polygon + BuildArrow(l_Point, [TmsDirection.dBottom]);
 // Нарисовали стрелку вниз

 l_Point := (l_Polygon[High(l_Polygon)]);
 l_Polygon := l_Polygon + BuildArrow(l_Point, [TmsDirection.dLeft]);
 // Нарисовали стрелку влево

 imgLineRotate.Canvas.BeginScene;
 imgLineRotate.Canvas.DrawPolygon(l_Polygon, 1);
 imgLineRotate.Canvas.FillPolygon(l_Polygon, 0.5);
 imgLineRotate.Canvas.EndScene;
end;

procedure TForm3.btnPaintClick(Sender: TObject);
var
 l_Polygon : TPolygon;
 l_StartPoint, l_Point: TPointF;
 l_RectF: TRectF;
begin
 //
 l_StartPoint := TPointF.Create(20, 20);

 l_Point := TPointF.Create(l_StartPoint.X, l_StartPoint.Y);

 l_Polygon := l_Polygon + BuildArrow(l_Point, [TmsDirection.dTop]);
 // Нарисовали стрелку вверх

 l_Point := (l_Polygon[High(l_Polygon)]);
 l_Polygon := l_Polygon + BuildArrow(l_Point, [TmsDirection.dRight]);
 // Нарисовали стрелку вправо

 l_Point := (l_Polygon[High(l_Polygon)]);
 l_Polygon := l_Polygon + BuildArrow(l_Point, [TmsDirection.dBottom]);
 // Нарисовали стрелку вниз

 l_Point := (l_Polygon[High(l_Polygon)]);
 l_Polygon := l_Polygon + BuildArrow(l_Point, [TmsDirection.dLeft]);
 // Нарисовали стрелку влево

 imgMain.Canvas.BeginScene;
 imgMain.Canvas.DrawPolygon(l_Polygon, 1);
 imgMain.Canvas.FillPolygon(l_Polygon, 0.5);
 imgMain.Canvas.EndScene;
end;

end.
