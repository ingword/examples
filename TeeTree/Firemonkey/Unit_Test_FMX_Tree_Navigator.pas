unit Unit_Test_FMX_Tree_Navigator;
{$I TeeDefs.inc}

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMXTee.Procs, FMXTee.Tree, FMX.Layouts,
  TeeTreeNavigator, FMX.ListBox;

type
  TFormTreeNavigator = class(TForm)
    Layout1: TLayout;
    Tree1: TTree;
    PanelNavigator: TLayout;
    Splitter1: TSplitter;
    PanelTop: TLayout;
    CBScrollOutside: TCheckBox;
    CBClickOnMap: TCheckBox;
    CBTreeZoom: TCheckBox;
    Label1: TLabel;
    CBQuality: TComboBox;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    PanelZoom: TLayout;
    TBZoom: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure Tree1Scroll(Sender: TObject);
    procedure CBScrollOutsideChange(Sender: TObject);
    procedure CBClickOnMapChange(Sender: TObject);
    procedure CBTreeZoomChange(Sender: TObject);
    procedure CBQualityChange(Sender: TObject);
    procedure Tree1Resize(Sender: TObject);
    procedure Tree1Zoom(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Tree1AfterDraw(Sender: TObject);
  private
    { Private declarations }
    Navigator : TTreeNavigator;

    procedure AdjustNavigatorRatio;
  public
    { Public declarations }
  end;

var
  FormTreeNavigator: TFormTreeNavigator;

implementation

{$R *.fmx}

uses Unit_Tree_Utils;

procedure TFormTreeNavigator.CBScrollOutsideChange(Sender: TObject);
begin
  Navigator.ScrollOutside:=CBScrollOutside.IsChecked;
end;

procedure TFormTreeNavigator.CBTreeZoomChange(Sender: TObject);
begin
  Tree1.Zoom.Allow:=CBTreeZoom.IsChecked;
  PanelZoom.Visible:=Tree1.Zoom.Allow;
end;

procedure TFormTreeNavigator.CBClickOnMapChange(Sender: TObject);
begin
  Navigator.ClickToNavigate:=CBClickOnMap.IsChecked;
end;

procedure TFormTreeNavigator.CBQualityChange(Sender: TObject);
begin
  case CBQuality.ItemIndex of
    0: Navigator.Quality:=nqLow;
    1: Navigator.Quality:=nqNormal;
    2: Navigator.Quality:=nqHigh;
  else
    Navigator.Quality:=nqHighest;
  end;
end;

procedure TFormTreeNavigator.FormCreate(Sender: TObject);
begin
  // Cosmetic settings:
  Tree1.Page.Border.Hide;

  Tree1.BevelInner:=bvNone;
  Tree1.BevelOuter:=bvNone;

  // Pending to support Zoom
  Tree1.Zoom.Allow:=False;

  PanelNavigator.Width:=Width div 2;

  // Add lots of Tree nodes, at random positions and colors:
  AddRandomNodes(Tree1);

  // Create the Navigator map:
  Navigator:=TTreeNavigator.Create(Self);
  Navigator.Align:=TAlignLayout.Client;
  Navigator.Parent:=PanelNavigator;

  // Set the Tree control to navigate:
  Navigator.Tree:=Tree1;

  // Disabled zoom by default:
  Tree1.Zoom.Allow:=False;
  TBZoom.Value:=Tree1.View3DOptions.Zoom;
end;

procedure TFormTreeNavigator.FormShow(Sender: TObject);
begin
end;

// When the Tree is scrolled (by dragging with the right-mouse button, or
// by using the Tree scrollbars), change the position in the Navigator map:
procedure TFormTreeNavigator.Tree1AfterDraw(Sender: TObject);
begin
  if Navigator.BackImage.IsEmpty then
     Navigator.RefreshTreeMap;
end;

procedure TFormTreeNavigator.Tree1Resize(Sender: TObject);
begin
  AdjustNavigatorRatio;
end;

procedure TFormTreeNavigator.Tree1Scroll(Sender: TObject);
begin
  Navigator.Reposition;
end;

procedure TFormTreeNavigator.Tree1Zoom(Sender: TObject);
begin
  Navigator.Reposition;
end;

// Change the Navigator control Height, to best match the aspect ratio of the
// Tree Total Bounds:
procedure TFormTreeNavigator.AdjustNavigatorRatio;
var Ratio : Single;
begin
  if {Showing and} Assigned(Navigator) then
  begin
    Ratio:=Navigator.TotalBoundsRatio;

    PanelTop.Height:=PanelNavigator.Height-Round(Navigator.Width/Ratio);
  end;
end;

end.
