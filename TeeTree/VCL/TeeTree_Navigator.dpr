program TeeTree_Navigator;

uses
  Forms,
  Unit_Test_Tree_Navigator in 'Unit_Test_Tree_Navigator.pas' {FormTreeNavigator},
  TeeTreeNavigator in '..\TeeTreeNavigator.pas',
  Unit_Tree_Utils in '..\Unit_Tree_Utils.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormTreeNavigator, FormTreeNavigator);
  Application.Run;
end.
