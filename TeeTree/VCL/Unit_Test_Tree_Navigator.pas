unit Unit_Test_Tree_Navigator;
{$I TeeDefs.inc}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeeGDIPlus, ExtCtrls, TeeTree, TeeTreeNavigator, TeeProcs,
  Math, StdCtrls, ComCtrls, TeCanvas;

type
  TFormTreeNavigator = class(TForm)
    Tree1: TTree;
    Splitter1: TSplitter;
    PanelNavigator: TPanel;
    PanelTop: TPanel;
    Splitter2: TSplitter;
    Panel1: TPanel;
    CBScrollOutside: TCheckBox;
    CBClickOnMap: TCheckBox;
    CBTreeZoom: TCheckBox;
    PanelZoom: TPanel;
    Label1: TLabel;
    LZoom: TLabel;
    TBZoom: TTrackBar;
    Label2: TLabel;
    CBQuality: TComboFlat;
    procedure FormCreate(Sender: TObject);
    procedure Tree1Scroll(Sender: TObject);
    procedure CBScrollOutsideClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBClickOnMapClick(Sender: TObject);
    procedure CBTreeZoomClick(Sender: TObject);
    procedure Tree1Resize(Sender: TObject);
    procedure TBZoomChange(Sender: TObject);
    procedure CBQualityChange(Sender: TObject);
    procedure Tree1Zoom(Sender: TObject);
  private
    { Private declarations }

    Navigator : TTreeNavigator;

    procedure AdjustNavigatorRatio;
  public
    { Public declarations }
  end;

var
  FormTreeNavigator: TFormTreeNavigator;

implementation

uses
  Unit_Tree_Utils;

{$R *.dfm}

procedure TFormTreeNavigator.FormCreate(Sender: TObject);
begin
  // Cosmetic settings:
  Tree1.Page.Border.Hide;

  Tree1.BevelInner:=bvNone;
  Tree1.BevelOuter:=bvNone;

  // Pending to support Zoom
  Tree1.Zoom.Allow:=False;
  
  PanelNavigator.Width:=Width div 2;

  // Add lots of Tree nodes, at random positions and colors:
  AddRandomNodes(Tree1);

  // Create the Navigator map:
  Navigator:=TTreeNavigator.Create(Self);
  Navigator.Align:=alClient;
  Navigator.Parent:=PanelNavigator;

  // Example, changing the small rectangle format:
  // Navigator.Selector.Shape.Pen.Color:=clLime;
  
  // Set the Tree control to navigate:
  Navigator.Tree:=Tree1;

  // Disabled zoom by default:
  Tree1.Zoom.Allow:=False;
  TBZoom.Position:=Tree1.View3DOptions.Zoom;
end;

// Change the Navigator control Height, to best match the aspect ratio of the
// Tree Total Bounds:
procedure TFormTreeNavigator.AdjustNavigatorRatio;
var Ratio : Single;
begin
  if Showing and Assigned(Navigator) then
  begin
    Ratio:=Navigator.TotalBoundsRatio;

    PanelTop.Height:=PanelNavigator.Height-Round(Navigator.Width/Ratio);
  end;
end;

// When the Tree is scrolled (by dragging with the right-mouse button, or
// by using the Tree scrollbars), change the position in the Navigator map:
procedure TFormTreeNavigator.Tree1Scroll(Sender: TObject);
begin
  Navigator.Reposition;
end;

procedure TFormTreeNavigator.CBScrollOutsideClick(Sender: TObject);
begin
  Navigator.ScrollOutside:=CBScrollOutside.Checked;
end;

procedure TFormTreeNavigator.FormShow(Sender: TObject);
begin
  AdjustNavigatorRatio;
end;

procedure TFormTreeNavigator.CBClickOnMapClick(Sender: TObject);
begin
  Navigator.ClickToNavigate:=CBClickOnMap.Checked;
end;

procedure TFormTreeNavigator.CBTreeZoomClick(Sender: TObject);
begin
  Tree1.Zoom.Allow:=CBTreeZoom.Checked;
  PanelZoom.Visible:=Tree1.Zoom.Allow;
end;

procedure TFormTreeNavigator.Tree1Resize(Sender: TObject);
begin
  AdjustNavigatorRatio;
end;

procedure TFormTreeNavigator.TBZoomChange(Sender: TObject);
begin
  Tree1.View3DOptions.Zoom:=TBZoom.Position;
  LZoom.Caption:=IntToStr(TBZoom.Position)+'%';
end;

procedure TFormTreeNavigator.CBQualityChange(Sender: TObject);
begin
  case CBQuality.ItemIndex of
    0: Navigator.Quality:=nqLow;
    1: Navigator.Quality:=nqNormal;
    2: Navigator.Quality:=nqHigh;
  else
    Navigator.Quality:=nqHighest;
  end;
end;

procedure TFormTreeNavigator.Tree1Zoom(Sender: TObject);
begin
  Navigator.Reposition;
end;

end.
