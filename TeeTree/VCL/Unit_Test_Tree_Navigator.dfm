�
 TFORMTREENAVIGATOR 0�  TPF0TFormTreeNavigatorFormTreeNavigatorLeft� Top}Width�Height�CaptionTeeTree Navigator ExampleColor	clBtnFace
ParentFont	OldCreateOrderWindowStatewsMaximizedOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight 	TSplitter	Splitter1LeftETop)HeightiAlignalRight  TTreeTree1Left Top)WidthEHeightiCrossBox.SignPen.Width OnScrollTree1ScrollOnZoom	Tree1ZoomAlignalClientTabOrder OnResizeTree1ResizeDefaultCanvasTGDIPlusCanvas  TPanelPanelNavigatorLeftHTop)Width}HeightiAlignalRight
BevelOuterbvNoneTabOrder 	TSplitter	Splitter2Left Top� Width}HeightCursorcrVSplitAlignalTop  TPanelPanelTopLeft Top Width}Height� AlignalTopColorclWhiteTabOrder    TPanelPanel1Left Top Width�Height)AlignalTop
BevelOuterbvNoneColorclWhiteCtl3DParentCtl3DTabOrder TLabelLabel2Left�TopWidth#HeightCaption	&Quality:FocusControl	CBQuality  	TCheckBoxCBScrollOutsideLeftTopWidth� HeightCaptionEnable scroll &outside boundsTabOrder OnClickCBScrollOutsideClick  	TCheckBoxCBClickOnMapLeft� TopWidth|HeightCaptionEnable &Click on MapChecked	State	cbCheckedTabOrderOnClickCBClickOnMapClick  	TCheckBox
CBTreeZoomLeft?TopWidthrHeightCaptionEnable Tree &ZoomTabOrderOnClickCBTreeZoomClick  TPanel	PanelZoomLeft�Top Width� Height)AlignalRight
BevelOuterbvNoneColorclWhiteTabOrderVisible TLabelLabel1LeftTopWidthHeightCaption&Zoom:FocusControlTBZoom  TLabelLZoomLeft� TopWidthHeightCaption100%  	TTrackBarTBZoomLeft0TopWidth� Height Ctl3DMax� MinParentCtl3D	Frequency
PositionTabOrder OnChangeTBZoomChange   
TComboFlat	CBQualityLeft�TopWidthY	ItemIndexTabOrderTextNormalOnChangeCBQualityChangeItems.StringsLowNormalHighHighest     