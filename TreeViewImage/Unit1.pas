unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Rtti, System.Classes,
  System.Variants, FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.Layouts,
  FMX.TreeView;

type
  TForm1 = class(TForm)
    TreeView1: TTreeView;
    Button1: TButton;
    StyleBook1: TStyleBook;
    procedure Button1Click(Sender: TObject);
  private
    Index: Integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
uses TreeViewImage;

{$R *.fmx}

procedure TForm1.Button1Click(Sender: TObject);
var Item: TTreeViewImageItem;
begin
  Item := TTreeViewImageItem.Create(Self);
  inc(Index);
  Item.Text := 'Item'+IntToStr(Index);
  if (Index mod 2) = 1 then
    Item.ImageStyleLookup := 'Image1'
  else
    Item.ImageStyleLookup := 'Image2';

  if TreeView1.Selected <> nil then
  begin
    Item.Parent := TreeView1.Selected;
    TreeView1.Selected.IsExpanded := True;
  end
  else
    Item.Parent := TreeView1;
end;

end.
