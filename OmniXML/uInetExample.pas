Unit uInetExample;

interface

uses
 FMX.ListView,
 Xml.XMLIntf,
 Xml.xmldom,
 Xml.Internal.OmniXML,
 FMX.Dialogs,
 Xml.XMLDoc,
 System.Variants
 ;


procedure amOpenFileExecute(Sender: TObject);

implementation

procedure amOpenFileExecute(Sender: TObject);
var
lv1: TListView;
DocXML: IXMLDocument; // OmniIXMLDocument;
Node, NodeB: IXMLNode; // OmniIXMLNode;
NodeList: IXMLNodeList;

 errXML: IDOMParseError;
 v: TDOMVendor;
 i, j, k: Integer;
 head: Variant;
 ListI: TListViewItems;
 N: IXMLNode;
 dt: string;
 dlgOpen : TOpenDialog;
 lOriginal, lSourceLang, lTargetLang, lTs :string;
begin
 DocXML:= CreateXMLDoc;
  try
//   mmo1.Lines.Clear;
   DocXML.Active := False;
   DocXML.Options := [doNodeAutoCreate, doAutoPrefix, doNamespaceDecl];
   (DocXML as TXMLDocument).DOMVendor := GetDOMVendor(' Omni XML ');
   DocXML.LoadFromFile(dlgOpen.FileName);
   errXML := (DocXML.DOMDocument as IDOMParseError);

   if errXML.errorCode = 0 then
   begin
    Node := DocXML.DocumentElement;
    lOriginal := VarToStr(Node.ChildNodes[' file '].Attributes[' original ']);
    lSourceLang := VarToStr(Node.ChildNodes[' file '].Attributes[' source-language ']);
    lTargetLang := VarToStr(Node.ChildNodes[' file '].Attributes[' target-language ']);
    lTs := VarToStr(Node.ChildNodes[' file '].Attributes[' ts']);
    {
      <trans-unit id = "uDataConst_sDeptNotRemove" resname = "uDataConst_sDeptNotRemove">
      <source> "subdividing removal \" %s \Is forbidden "." </source>
      <prop-group name = "ITEFieldProp">
      <prop prop-type = "Created"> 41978.4877617361 </prop>
      <prop prop-type = "Status"> 1 </prop>
      </prop-group>
      <target> "Zaboroneno   \" %s \"." </target>
      <alt-trans>
      <source> "subdividing removal \" % \Is forbidden "." </source>
      <target> "Zaboroneno   \" %s \"." </target>
      <prop-group name = "ITEFieldProp">
      <prop prop-type = "Modified"> 41981.5661134259 </prop>
      </prop-group>
      </alt-trans>
      </trans-unit>
    }
    lv1.Items.BeginUpdate;
    NodeB := DocXML.DocumentElement;
    Node := NodeB.ChildNodes[' file '].ChildNodes[' body '];
    if Node.HasChildNodes then
     begin
     for i := 0 to Node.ChildNodes.Count - 1 do
     begin
      N := Node.ChildNodes.Nodes[i];
      ListI:= lv1.Items.Add;
      ListI.Caption: = IntToStr(i);
      for k: = 0 to lv1.Columns.Count - 1 do
       ListI.SubItems.Add(");
      ListI.SubItems.Strings[0]: = VarToStr(N.Attributes[' id ']);
      ListI.SubItems.Strings[1]: = VarToStrDef(N.ChildNodes[' source '].NodeValue, ");
      dt := StringReplace(VarToStr(N.ChildNodes[' prop-group '].ChildNodes[' prop '].NodeValue), '. ', ', ', [rfReplaceAll]);
      ListI.SubItems.Strings[4]: = FormatDateTime(' dd.mm.yyyy hh:nn:ss', (StrToFloat(dt)));
      ListI.SubItems.Strings[2]: = VarToStr(N.ChildNodes[' prop-group '].ChildNodes[' prop '].NodeValue);
      // mmo1.Lines. Append (VarToStr (N.ChildNodes [' prop-group '].ChildNodes [' prop '].AttributeNodes [0].NodeValue));
      // mmo1.Lines. Append (VarToStr (N.ChildNodes [' prop-group '].ChildNodes [' prop '].AttributeNodes [1].NodeValue));
      ListI.SubItems.Strings[3]: = VarToStr(N.ChildNodes[' target '].NodeValue);
      ListI.SubItems.Strings[6]: = VarToStr(N.ChildNodes[' alt-trans'].ChildNodes[' source '].NodeValue);
      ListI.SubItems.Strings[7]: = VarToStr(N.ChildNodes[' alt-trans'].ChildNodes[' target '].NodeValue);
     end;
    mmo1.Lines.Append(' ===================== ');
    mmo1.Lines.Append(VarToStr(N.ChildNodes[' prop-group '].ChildNodes[' prop '].NextSibling.NodeValue));
    mmo1.Lines.Append(VarToStr(N.ChildNodes[' prop-group '].ChildNodes[' prop '].AttributeNodes.Nodes[' prop-type '].LocalName));
    mmo1.Lines.Append(VarToStr(N.ChildNodes[' prop-group '].ChildNodes[' prop '].AttributeNodes.Nodes[' prop-type '].NamespaceURI));
    mmo1.Lines.Append(VarToStr(N.ChildNodes[' prop-group '].ChildNodes[' prop '].Text));
    mmo1.Lines.Append(' XML: ' + VarToStr((N.ChildNodes[' prop-group '].ChildNodes[' prop '].Attributes[' prop-type '])));
   end;
  finally
   lv1.Items.EndUpdate;
  end;
end;

end.
