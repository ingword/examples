unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Xml.Internal.OmniXML, FMX.StdCtrls, FMX.Layouts, FMX.Memo, Xml.XMLDoc;

type
  TForm6 = class(TForm)
    mmoMain: TMemo;
    btnCreateXml: TButton;
    procedure btnCreateXmlClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation

{$R *.fmx}

procedure TForm6.btnCreateXmlClick(Sender: TObject);
var
 l_XMLDoc : OmniIXMLDocument;
begin
 l_XMLDoc := CreateXMLDoc;
 l_XMLDoc.AppendChild( l_XMLDoc.CreateElementNS('NameSpace', 'qualifed name'));
 l_XMLDoc.AppendChild( l_XMLDoc.CreateAttribute('1st Attribut'));
 l_XMLDoc.Save('omni.xml');
 mmoMain.Lines.LoadFromFile('omni.xml');
// FreeAndNil(l_XMLDoc);
end;

end.
