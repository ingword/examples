unit uMainForm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.TreeView, FMX.Objects, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit;

type
  TMainForm = class(TForm)
    MainTree: TTreeView;
    MainImage: TImage;
    AddNode: TButton;
    OpenImage: TOpenDialog;
    StyleBookMain: TStyleBook;
    AddImageList: TButton;
    procedure AddNodeClick(Sender: TObject);
    procedure MainTreeChange(Sender: TObject);
    procedure AddImageListClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type
  TNode = class(TTreeViewItem)
  strict private
    FImage: TImage;
    FButton: TButton;
    FEdit: TEdit;
  private
    procedure SetImage(const aValue: TImage);
    procedure TreeButtonClick(Sender: TObject);
    procedure SetButton(const Value: TButton);
    procedure SetEdit(const Value: TEdit);
  public
    constructor Create(Owner: TComponent; const aText: String;
                                          const aImageFileName: String); reintroduce;
    destructor Destroy; override;
  published
    property Image: TImage Read FImage Write SetImage;
    property Button: TButton Read FButton Write SetButton;
    property Edit: TEdit Read FEdit Write SetEdit;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.fmx}
{$R *.SmXhdpiPh.fmx ANDROID}

{ TTestNode }

constructor TNode.Create(Owner: TComponent; const aText: String;
                                            const aImageFileName: String);
begin
  inherited Create(Owner);
  Self.Text := aText;

  FButton := TButton.Create(Owner);
  FButton.Text := 'Send';
  Self.AddObject(FButton);
  FButton.Align := TAlignLayout.Center;
  FButton.SendToBack;
  FButton.OnClick := TreeButtonClick;

  FEdit:= TEdit.Create(Owner);
  Self.AddObject(FEdit);
  FEdit.Position.X := 150;
  FEdit.Position.Y := 25;
  FEdit.SendToBack;

  FImage := TImage.Create(Owner);
  Self.AddObject(FImage);
  FImage.Align := TAlignLayout.Right;
  FImage.Bitmap.LoadFromFile(aImageFileName);
  FImage.SendToBack;
end;

destructor TNode.Destroy;
begin
  Image.FreeOnRelease;
  FreeAndNil(FButton);
  inherited;
end;

procedure TNode.SetButton(const Value: TButton);
begin
  FButton := Value;
end;

procedure TNode.SetEdit(const Value: TEdit);
begin
  FEdit := Value;
end;

procedure TNode.SetImage(const aValue: TImage);
begin
  FImage := aValue;
end;

procedure TMainForm.AddImageListClick(Sender: TObject);
var
  ImageFileName: string;
  Node : TNode;
begin
  OpenImage.Options := OpenImage.Options + [TOpenOption.ofAllowMultiSelect];
  if OpenImage.Execute then
  begin
    for ImageFileName in OpenImage.Files do
    begin
      Node := TNode.Create(MainTree, ExtractFileName(ImageFileName), ImageFileName);
      MainTree.AddObject(Node);
    end;

    MainImage.Bitmap.LoadFromFile(OpenImage.Files[0]);
  end;
end;

procedure TMainForm.AddNodeClick(Sender: TObject);
var
  Node : TNode;
begin
  OpenImage.Options := OpenImage.Options - [TOpenOption.ofAllowMultiSelect];

  if OpenImage.Execute then
  begin
    MainImage.Bitmap.LoadFromFile(OpenImage.Files[0]);

    Node := TNode.Create(MainTree, ExtractFileName(OpenImage.Files[0]), OpenImage.Files[0]);

    if MainTree.Selected = nil then
      MainTree.AddObject(Node)
    else
      MainTree.Selected.AddObject(Node);
  end;
end;

procedure TMainForm.MainTreeChange(Sender: TObject);
begin
  if MainTree.Selected is TNode then
    MainImage.Bitmap := (MainTree.Selected as TNode).Image.Bitmap;
end;

procedure TNode.TreeButtonClick(Sender: TObject);
begin
  ShowMessage('Hello World');
end;

end.
