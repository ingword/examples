object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 138
  ClientWidth = 164
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnListCreateAndFill: TButton
    Left = 32
    Top = 24
    Width = 105
    Height = 25
    Caption = 'List create and fill'
    TabOrder = 0
    OnClick = btnListCreateAndFillClick
  end
  object btnListClear: TButton
    Left = 32
    Top = 55
    Width = 105
    Height = 25
    Caption = 'List clear'
    TabOrder = 1
    OnClick = btnListClearClick
  end
  object btnLastItemFree: TButton
    Left = 32
    Top = 86
    Width = 105
    Height = 25
    Caption = 'Last item free'
    TabOrder = 2
    OnClick = btnLastItemFreeClick
  end
end
