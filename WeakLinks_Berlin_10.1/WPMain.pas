unit WPMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  IXListItem = interface
    ['{02E680D6-9F86-4303-85B5-256ACD89AD46}']
    function GetName: string;
    procedure SetName(const Name: string);
    property Name: string read GetName write SetName;
  end;

  IXList = interface
    ['{922BDB26-4728-46DA-8632-C4F331C5A013}']
    function Add: IXListItem;
    function Count: Integer;
    function GetItem(Index: Integer): IXListItem;
    property Items[Index: Integer]: IXListItem read GetItem;
    procedure Clear;
  end;

  TXListItem = class(TInterfacedObject, IXListItem)
  private
    FOwner: IXList;
    FName: string;
  public
    constructor Create(const Owner: IXList);
    destructor Destroy; override;
    procedure SetName(const Name: string);
    function GetName: string;
  end;

  TXList = class(TInterfacedObject, IXList)
  private
    FItems: array of IXListItem;
  public
    function Add: IXListItem;
    function Count: Integer;
    function GetItem(Index: Integer): IXListItem;
    procedure Clear;
    destructor Destroy; override;
  end;

type
  TForm1 = class(TForm)
    btnListCreateAndFill: TButton;
    btnListClear: TButton;
    btnLastItemFree: TButton;
    procedure btnListCreateAndFillClick(Sender: TObject);
    procedure btnListClearClick(Sender: TObject);
    procedure btnLastItemFreeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  List: IXList;
  Item: IXListItem;

implementation

{$R *.dfm}

{ TXListItem }

constructor TXListItem.Create(const Owner: IXList);
begin
  FOwner := Owner;
end;

destructor TXListItem.Destroy;
begin
  ShowMessage('Destroy ' + GetName);
  inherited;
end;

function TXListItem.GetName: string;
var
  List: IXList;
begin
  List := FOwner;
  if Assigned(List) then
    Exit(FName + '; owner assined!')
  else
    Exit(FName + '; owner NOT assined!');
end;

procedure TXListItem.SetName(const Name: string);
begin
  FName := Name;
end;

{ TXList }

function TXList.Add: IXListItem;
var
  c: Integer;
begin
  c := Length(FItems);
  SetLength(FItems, c + 1);
  Result := TXListItem.Create(Self);
  FItems[c] := Result;
end;

procedure TXList.Clear;
var
  i: Integer;
begin
  for i := 0 to Length(FItems) - 1 do
    FItems[i] := nil;
end;

function TXList.Count: Integer;
begin
  Result := Length(FItems);
end;

destructor TXList.Destroy;
begin
  Clear;
  ShowMessage('Destroy list');
  inherited;
end;

function TXList.GetItem(Index: Integer): IXListItem;
begin
  Result := FItems[Index];
end;

procedure TForm1.btnListCreateAndFillClick(Sender: TObject);
begin
  List := TXList.Create;
  List.Add.Name := 'item1';
  List.Add.Name := 'item2';
  Item := List.Add;
  Item.Name := 'item3';
end;

procedure TForm1.btnListClearClick(Sender: TObject);
begin
  List := nil;
end;

procedure TForm1.btnLastItemFreeClick(Sender: TObject);
begin
  Item := nil;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown := true;
end;

end.
