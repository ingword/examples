unit NotificationServiceUnit;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Android.Service,
  AndroidApi.JNI.GraphicsContentViewText,
  Androidapi.JNI.Os, System.Notification, System.Net.Socket;

type
  TNotificationServiceDM = class(TAndroidService)
    NotificationCenter1: TNotificationCenter;
    function AndroidServiceStartCommand(const Sender: TObject; const Intent: JIntent; Flags, StartId: Integer): Integer;
    procedure AndroidServiceDestroy(Sender: TObject);
  private
    { Private declarations }
    FThread: TThread;
    FClientSocket: TSocket;
    FMessage: string;

    procedure WaitForNotification;
    procedure LaunchNotification;
   procedure SetMessage(const Value: string);
  public
    { Public declarations }
    property TheMessage: string read FMessage write SetMessage;
  end;

var
  NotificationServiceDM: TNotificationServiceDM;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses
  Androidapi.JNI.App, System.DateUtils;

{$R *.dfm}

procedure TNotificationServiceDM.WaitForNotification;
var
  Buf: TBytes;
begin
  try
    //Connect to VCL server
    FClientSocket := TSocket.Create(TSocketType.TCP);
    FClientSocket.Connect(string.Empty, '192.168.1.207'+#0, string.Empty, 4851);

    while True do
    begin
      // Receive the message from the VCL server
      SetLength(Buf, 160);
      FClientSocket.Receive(Buf, 160);
      TheMessage := TEncoding.UTF8.GetString(Buf,0, 160);
      LaunchNotification;
    end;
  except
    on E:Exception do
      TheMessage := 'Exception: ' + E.Message;
  end;
end;

procedure TNotificationServiceDM.AndroidServiceDestroy(Sender: TObject);
begin
  FClientSocket.Free;
  FThread.Free;
end;

function TNotificationServiceDM.AndroidServiceStartCommand(const Sender: TObject; const Intent: JIntent; Flags,
  StartId: Integer): Integer;
begin
  if FThread = nil then
  begin
    FThread := TThread.CreateAnonymousThread(procedure
      begin
        WaitForNotification;
        JavaService.stopSelf;
      end);
    FThread.FreeOnTerminate := False;
    FThread.Start;
  end;

  Result := TJService.JavaClass.START_STICKY;
end;

procedure TNotificationServiceDM.LaunchNotification;
var
  MyNotification: TNotification;
begin
  MyNotification := NotificationCenter1.CreateNotification;
  try
    MyNotification.Name := 'ServiceNotification';
    MyNotification.Title := 'Android Service Notification';
    MyNotification.AlertBody := 'RAD Studio 10 Seattle';
    NotificationCenter1.PresentNotification(MyNotification);
  finally
    MyNotification.Free;
  end;
end;

procedure TNotificationServiceDM.SetMessage(const Value: string);
begin
  FMessage := Value;
end;

end.
