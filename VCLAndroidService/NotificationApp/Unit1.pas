unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation, FMX.StdCtrls,
  System.Notification, FMX.ScrollBox, FMX.Memo, System.Android.Service, Androidapi.JNI.Os,
  NotificationServiceUnit;

type
  TForm1 = class(TForm)
    Button1: TButton;
    NotificationCenter1: TNotificationCenter;
    Memo1: TMemo;
    Label1: TLabel;
    procedure NotificationCenter1ReceiveLocalNotification(Sender: TObject; ANotification: TNotification);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure StartConnectService;
  private
    { Private declarations }
    [Unsafe] FServiceDM: TNotificationServiceDM;
    FServiceConnection: TLocalServiceConnection;
    procedure OnServiceConnected(const ALocalService: TAndroidBaseService);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  AndroidApi.Helpers,
  Androidapi.JNI.JavaTypes,
  Androidapi.JNI.Widget;

procedure TForm1.Button1Click(Sender: TObject);
begin
  StartConnectService;
end;

procedure TForm1.StartConnectService;
begin
  if FServiceConnection = nil then
  begin
    TLocalServiceConnection.StartService('NotificationService');

    FServiceConnection := TLocalServiceConnection.Create;
    FServiceConnection.OnConnected := OnServiceConnected;

    if FServiceConnection.LocalService = nil then
      FServiceConnection.BindService('NotificationService');
  end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FServiceConnection.Free;
end;

procedure TForm1.OnServiceConnected(const ALocalService: TAndroidBaseService);
begin
  FServiceDM := TNotificationServiceDM(ALocalService);
end;

procedure TForm1.NotificationCenter1ReceiveLocalNotification(Sender: TObject; ANotification: TNotification);
begin
  StartConnectService;
  while FServiceConnection.LocalService = nil do Application.ProcessMessages; // wait for connection to service

  TThread.Queue(nil, procedure
  begin
    try
      Memo1.Lines.Add('Notification received:');
      Memo1.Lines.Add(FServiceDM.TheMessage);
    except
      Memo1.Lines.Add('-- SERVICE NOT AVAILABLE --');
    end;
  end);
end;

end.
