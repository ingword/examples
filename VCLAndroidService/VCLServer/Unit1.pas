unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Net.Socket;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    Edit1: TEdit;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FServerSocket: TSocket;
    FClientSocket: TSocket;
    FThread: TThread;

    procedure StartServer;
    procedure LaunchNotification;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.StartServer;
begin
  FServerSocket := TSocket.Create(TSocketType.TCP);

  FServerSocket.Listen(string.Empty, string.Empty, 4851);

  Memo1.Lines.Add('Server up and running');
  Application.ProcessMessages;

  while not FThread.Finished do
  begin
    FClientSocket := FServerSocket.Accept(MAXDWORD);
    Memo1.Lines.Add('Client connected');
    Application.ProcessMessages;
    Sleep(5);
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  FThread := TThread.CreateAnonymousThread(procedure
    begin
      StartServer;
    end);
  FThread.Start;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  LaunchNotification;
end;

procedure TForm1.LaunchNotification;
var
  Buf: TBytes;
  MyMessage: String;
begin
  MyMessage := edit1.Text + StringOfChar(' ',160-Length(edit1.Text));
  if FClientSocket <> nil then
  begin
    SetLength(Buf, 160);
    Buf := TEncoding.UTF8.GetBytes(MyMessage);
    FClientSocket.Send(Buf, 0, 160);
  end;
end;

end.
