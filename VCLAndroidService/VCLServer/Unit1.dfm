object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 337
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 32
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Init Server'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 32
    Top = 72
    Width = 585
    Height = 257
    TabOrder = 1
  end
  object Button2: TButton
    Left = 210
    Top = 24
    Width = 119
    Height = 25
    Caption = 'Launch Notification'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 335
    Top = 26
    Width = 282
    Height = 21
    MaxLength = 160
    TabOrder = 3
    Text = 'Message from Windows app'
  end
end
