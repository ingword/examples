{    
    �����: ������ �.�.
    ����: http://yaroslavbrovin.ru
}
unit MainFrm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Rtti, System.Classes,
  System.Variants, FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs,
  FMX.StdCtrls, FMX.Layouts, FMX.Objects, System.Actions, FMX.ActnList, FMX.Edit, FMX.Graphics;

type
  TForm51 = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    ActionList: TActionList;
    ActionMakeScreenshot: TAction;
    Line1: TLine;
    imgDest: TImage;
    Label1: TLabel;
    layoutSource: TLayout;
    Label2: TLabel;
    Switch1: TSwitch;
    TrackBar1: TTrackBar;
    CalloutPanel1: TCalloutPanel;
    Edit1: TEdit;
    Layout1: TLayout;
    recBackground: TRectangle;
    ProgressBar1: TProgressBar;
    procedure ActionMakeScreenshotExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form51: TForm51;

implementation

{$R *.fmx}

procedure TForm51.ActionMakeScreenshotExecute(Sender: TObject);
var
  BitmapBuffer: TBitmap;
  SourceRect: TRectF;
begin
  // ��������� ������ ��������� �������
  SourceRect := TRectF.Create(0, 0, layoutSource.Width, layoutSource.Height);
  // ������� ��������� ����� ��� ��������� ���������
  BitmapBuffer := TBitmap.Create(Round(SourceRect.Width), Round(SourceRect.Height));
  try
    // ��������� ����� � ����� ��������� - �������� ������� ��������� �����
    if BitmapBuffer.Canvas.BeginScene then
      try
        // ������� �������� ���������� ���� � ����� ������ ������ � ��������� �������
        layoutSource.PaintTo(BitmapBuffer.Canvas, SourceRect);
      finally
        // ��������� ������� ���������, ���������� ����������� �����
        BitmapBuffer.Canvas.EndScene;
      end;
    imgDest.Bitmap.Assign(BitmapBuffer);
    BitmapBuffer.SaveToFile('./screenshot.png');
  finally
    FreeAndNil(BitmapBuffer);
  end;
end;

end.
