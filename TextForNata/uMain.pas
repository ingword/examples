unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    mmoText: TMemo;
    pnlBottom: TPanel;
    btnResult: TButton;
    btnClear: TButton;
    procedure btnResultClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  System.StrUtils
  , System.Types
  ;

{$R *.dfm}

procedure TForm1.btnClearClick(Sender: TObject);
begin
  mmoText.Lines.Clear;
end;

procedure TForm1.btnResultClick(Sender: TObject);
var
  TextFromMemo, TempStr, StrForAdd: string;
  sb: TStringBuilder;
  i, IndexFrom: Int32;
const
  Delim = 'http://';
  DelimLength = 7;
begin
  TextFromMemo := mmoText.Lines.Text;

  TextFromMemo := TextFromMemo.Replace(#13#10,'');
  sb := TStringBuilder.Create;

  TempStr := '';
  IndexFrom := 1;

  for I :=1 to Length(TextFromMemo) do
  begin
    TempStr := TempStr + TextFromMemo[i];
    if TempStr = Delim then
    begin
      if i = DelimLength then
      begin
        TempStr := '';
        Continue;
      end;

      StrForAdd := Copy(TextFromMemo, IndexFrom, I - IndexFrom - DelimLength + 1);
      sb.AppendLine(StrForAdd);
      IndexFrom := I - DelimLength + 1;
    end;

    if Length(TempStr) = DelimLength then
      Delete(TempStr, 1, 1);
  end;

  mmoText.Text := sb.ToString;
  FreeAndNil(sb);
end;

end.
