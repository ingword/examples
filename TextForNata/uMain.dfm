object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Do strngs'
  ClientHeight = 390
  ClientWidth = 1084
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object mmoText: TMemo
    Left = 0
    Top = 0
    Width = 1084
    Height = 349
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitWidth = 529
    ExplicitHeight = 233
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 349
    Width = 1084
    Height = 41
    Align = alBottom
    TabOrder = 1
    ExplicitLeft = 288
    ExplicitTop = 280
    ExplicitWidth = 185
    object btnResult: TButton
      Left = 1008
      Top = 1
      Width = 75
      Height = 39
      Align = alRight
      Caption = 'Result'
      TabOrder = 0
      OnClick = btnResultClick
      ExplicitLeft = 496
      ExplicitTop = 8
      ExplicitHeight = 25
    end
    object btnClear: TButton
      Left = 933
      Top = 1
      Width = 75
      Height = 39
      Align = alRight
      Caption = 'Clear'
      TabOrder = 1
      OnClick = btnClearClick
      ExplicitLeft = 496
      ExplicitTop = 8
      ExplicitHeight = 25
    end
  end
end
