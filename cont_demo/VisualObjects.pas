unit VisualObjects;

interface

uses SysUtils, Classes, CustomTypes;

const
  // ������� ��������
  HT_OUT          = $00000000;         // ��� �������
  HT_IN           = $80000000;         // ���������� �������
  HT_VERTEX       = $40000000;         // �������
  HT_SIDE         = $20000000;         // �������
  // ���� �������
  CR_DEFAULT      = 0;           
  CR_SIZEALL      = 1;
  CR_HORIZONTAL   = 2;
  CR_VERTICAL     = 3;
  CR_DIAG1        = 4;
  CR_DIAG2        = 5;
  // ������� ��������
  VOC_BEGINDRAG         = 1;
  VOC_ENDDRAG           = 2;
  VOC_DRAG              = 3;
  VOC_VERTEXMOVE        = 4;
  VOC_SIDEMOVE          = 5;
  VOC_MOVE              = 6;
  VOC_HITTEST           = 7;
  VOC_GETCURSOR         = 8;
  VOC_CONSTRUCTPOINT    = 9;
  VOC_PROCESSCONSTRUCT  = 10;
  VOC_STOPCONSTRUCT     = 11;
  VOC_VCONTROL	       	= 12;

type
  // ��������� ��� ����������� ������� �������
  PHitTestParams = ^THitTestParams;
  THitTestParams = record
    XPos, YPos: Integer;  // ������� � �������� ��������   
    Tolerance: Integer;   // ����������������
  end;

  // ������ �������������� ��� ������������ �����
  TVOCBeginDrag = packed record
    CmdID: Cardinal;
    HitTest: Cardinal;      // ������� �������
    StartPos: PFloatPoint;  // �������, � ������� �������� ��������������
    Result: Longint;
  end;

  // ���������� �������������� ��� ������������ �����
  TVOCEndDrag = packed record
    CmdID: Cardinal;
    Unused1: Longint;       // �� ������������
    Unused2: Longint;       // �� ������������
    Result: Longint;
  end;

  // �������������� ��� ������������ �����
  TVOCDrag = packed record
    CmdID: Cardinal;
    Unused: Longint;      // �� ������������
    NewPos: PFloatPoint;  // �������, � ������� ������������� ����
    Result: Longint;
  end;

  // ����������� �������
  TVOCVertexMove = packed record
    CmdID: Cardinal;
    Index: Integer;       // ������ �������
    NewPos: PFloatPoint;  // ����� ������� �������
    Result: Longint;
  end;

  // ����������� �������
  TVOCSideMove = packed record
    CmdID: Cardinal;
    Index: Integer;       // ������ �������
    NewPos: PFloatPoint;  // ����� ������� �������
    Result: Longint;
  end;

  // ����������� �������
  TVOCMove = packed record
    CmdID: Cardinal;
    DeltaX: PExtended;    // �������� �� ��� X
    DeltaY: PExtended;    // �������� �� ��� Y
    Result: Longint;
  end;

  // ����������� ������� �������
  TVOCHitTest = packed record
    CmdID: Cardinal;
    ConvertIntf: ICoordConvert; // ��������� �������������� ���������
    Params: PHitTestParams;     // ���������
    Result: Cardinal;
  end;

  // ����������� ���� �������
  TVOCGetCursor = packed record
    CmdID: Cardinal;
    Unused: Longint;      // �� ������������
    HitTest: Cardinal;    // ������� �������
    Result: Cardinal;
  end;

  // ���������� ����� ��� ���������������
  TVOCConstructPoint = packed record
    CmdID: Cardinal;
    Unused: Longint;        // �� ������������
    Pos: PFloatPoint;       // ������� ����� �����
    Result: Longint;
  end;

  // ���������������
  TVOCProcessConstruct = packed record
    CmdID: Cardinal;
    Unused: Longint;        // �� ������������
    Pos: PFloatPoint;       // �������
    Result: Longint;
  end;

  // ���������� ���������������
  TVOCStopConstruct = packed record
    CmdID: Cardinal;
    Unused1: Longint;        // �� ������������
    Unused2: Longint;        // �� ������������
    Result: Longint;
  end;

  // ���������� ���������
  TVOCVControl = packed record
    CmdID: Cardinal;
    HitTest: Cardinal;      // ������� ������
    Pos: PFloatPoint;       // �������, � ������� ����� ��������� ����� �������
    Result: Longint;
  end;

  // ������� ����� ���������� ��������
  TBaseVisualObject = class(TObject)
  private
    FBasePoints: TList;
    FOnChange: TNotifyEvent;
    FLockCount: Integer;
    FDragging: Boolean;
    FDragHitTest: Cardinal;
    FDragStartPos: TFloatPoint;
    function GetBasePointsCount: Integer;
    function GetBasePoint(Index: Integer): TFloatPoint;
    procedure SetBasePoint(Index: Integer; const Value: TFloatPoint);
    procedure VOCBeginDrag(var Command: TVOCBeginDrag); message VOC_BEGINDRAG;
    procedure VOCEndDrag(var Command: TVOCEndDrag); message VOC_ENDDRAG;
    procedure VOCDrag(var Command: TVOCDrag); message VOC_DRAG;
    procedure VOCVertexMove(var Command: TVOCVertexMove); message VOC_VERTEXMOVE;
    procedure VOCMove(var Command: TVOCMove); message VOC_MOVE;
    procedure VOCHitTest(var Command: TVOCHitTest); message VOC_HITTEST;
    procedure VOCGetCursor(var Command: TVOCGetCursor); message VOC_GETCURSOR;
  protected
    procedure Change;
    // ������ ���������� �������� �������. ������ ��� ������������� � ��������,
    // ����������� ���� ��� �� ��������
    procedure AddBasePoint(X, Y: Extended);
    procedure InsertBasePoint(Index: Integer; X, Y: Extended);
    procedure DeleteBasePoint(Index: Integer);
    procedure ClearBasePoints;
    property BasePointsCount: Integer read GetBasePointsCount;
    property BasePoints[Index: Integer]: TFloatPoint read GetBasePoint
      write SetBasePoint;
    // ������ ���������� ���������. ������������ ����� ��������� � �������� �������
    // ����� ������ � ��������
    function GetVertexesCount: Integer; virtual; abstract;
    function GetVertex(Index: Integer): TFloatPoint; virtual; abstract;
    procedure SetVertex(Index: Integer; const Value: TFloatPoint); virtual; abstract;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    // ������ ����������/�������������
    procedure BeginUpdate;
    procedure EndUpdate;
    // �������� �������
    function SendCommand(ID: Cardinal; wParam, lParam: Longint): Longint;
    // ���������
    procedure Draw(Canvas: TLogicalCanvas); virtual;
    // ��������/�������
    property VertexesCount: Integer read GetVertexesCount;
    property Vertex[Index: Integer]: TFloatPoint read GetVertex write SetVertex;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

  // ��������� ���������� ��������
  TVisualObjectClass = class of TBaseVisualObject;

  // ������� ����� ��� "�������������" ��������
  TRectVisualObject = class(TBaseVisualObject)
  private
    FConstructing: Boolean;
    FCurrentPoint: Integer;
    FText: String;
    procedure SetText(const Value: String);
    procedure VOCSideMove(var Command: TVOCSideMove); message VOC_SIDEMOVE;
    procedure VOCHitTest(var Command: TVOCHitTest); message VOC_HITTEST;
    procedure VOCGetCursor(var Command: TVOCGetCursor); message VOC_GETCURSOR;
    procedure VOCConstructPoint(var Command: TVOCConstructPoint);
      message VOC_CONSTRUCTPOINT;
    procedure VOCProcessConstruct(var Command: TVOCProcessConstruct);
      message VOC_PROCESSCONSTRUCT;
    procedure VOCStopConstruct(var Command: TVOCStopConstruct);
      message VOC_STOPCONSTRUCT;
  protected
    function GetVertexesCount: Integer; override;
    function GetVertex(Index: Integer): TFloatPoint; override;
    procedure SetVertex(Index: Integer; const Value: TFloatPoint); override;
  public
    constructor Create; override;
    // ����� ��������
    property Text: String read FText write SetText;
  end;

  // ������� ����� ��� ��������-�����
  TLineVisualObject = class(TBaseVisualObject)
  private
    FConstructing: Boolean;
    FCurrentPoint: Integer;
    procedure VOCHitTest(var Command: TVOCHitTest); message VOC_HITTEST;
    procedure VOCGetCursor(var Command: TVOCGetCursor); message VOC_GETCURSOR;
    procedure VOCConstructPoint(var Command: TVOCConstructPoint);
      message VOC_CONSTRUCTPOINT;
    procedure VOCProcessConstruct(var Command: TVOCProcessConstruct);
      message VOC_PROCESSCONSTRUCT;
    procedure VOCStopConstruct(var Command: TVOCStopConstruct);
      message VOC_STOPCONSTRUCT;
  protected
    function GetVertexesCount: Integer; override;
    function GetVertex(Index: Integer): TFloatPoint; override;
    procedure SetVertex(Index: Integer; const Value: TFloatPoint); override;
    // ����������� ������� ���������� ���������������
    function NeedToStopConstruct(Count: Integer): Longint; virtual; abstract;
  public
    procedure Draw(Canvas: TLogicalCanvas); override;
  end;

  // ������ - �������������� �����
  TSimpleLineBlock = class(TLineVisualObject)
  protected
    function NeedToStopConstruct(Count: Integer): Longint; override;
  end;

  // ������ - ������� �����
  TPolyLineBlock = class(TLineVisualObject)
  private
    procedure VOCVControl(var Command: TVOCVControl); message VOC_VCONTROL;
  protected
    function NeedToStopConstruct(Count: Integer): Longint; override;
  end;

  // ������ - ������/�����
  TBeginEndBlock = class(TRectVisualObject)
  public
    procedure Draw(Canvas: TLogicalCanvas); override;
  end;

  // ������ - ����/�����
  TInputOutputBlock = class(TRectVisualObject)
  public
    procedure Draw(Canvas: TLogicalCanvas); override;
  end;

  // ������ - ��������
  TActionBlock = class(TRectVisualObject)
  public
    procedure Draw(Canvas: TLogicalCanvas); override;
  end;

  // ������ - �������
  TConditionBlock = class(TRectVisualObject)
  public
    procedure Draw(Canvas: TLogicalCanvas); override;
  end;

  // ������ - ������������
  TSubBlock = class(TRectVisualObject)
  public
    procedure Draw(Canvas: TLogicalCanvas); override;
  end;

  // ������ - �����
  TTextBlock = class(TRectVisualObject)
  public
    procedure Draw(Canvas: TLogicalCanvas); override;
  end;

implementation

uses Messages, Math, RTLConsts;

// ���������� ���������� �� ����� (X; Y) �� �������, ��������� �������
// (X1; Y1) � (X2; Y2)
function LineDistance(X, Y, X1, Y1, X2, Y2: Integer): Extended;
var
  A1, B1, C1, A2, B2, C2, DD: Integer;
  D, Px, Py: Extended;

procedure Xchg(var A, B: Integer);
var
  C: Integer;
begin
  C := A;
  A := B;
  B := C;
end;

begin
  if (X2 = X1) and (Y2 = Y1) then
    Result := Sqrt(Sqr(X - X1) + Sqr(Y - Y1))
  else begin
    A1 := Y2 - Y1;
    B1 := X1 - X2;
    C1 := Y1*(X2 - X1) - X1*(Y2 - Y1);
    D := Abs(A1*X + B1*Y + C1)/Sqrt(Sqr(A1) + Sqr(B1));
    if X1 > X2 then Xchg(X1, X2);
    if Y2 > Y1 then Xchg(Y1, Y2);
    A2 := -B1;
    B2 := A1;
    C2 := B1*X - A1*Y;
    DD := A1*B2 - B1*A2;
    Px := (-C1*B2 + B1*C2)/DD;
    Py := (-A1*C2 + C1*A2)/DD;
    if (Px >= X1) and (Px <= X2) and (Py >= Y2) and (Py <= Y1) then
      Result := D
    else
      Result := Min(Sqrt(Sqr(X - X1) + Sqr(Y - Y1)),
                    Sqrt(Sqr(X - X2) + Sqr(Y - Y2)));
  end;
end;

{ TBaseVisualObject }

procedure TBaseVisualObject.AddBasePoint(X, Y: Extended);
var
  NewBasePoint: PFloatPoint;
begin
  // �������� ������ ��� ����� ����� � ��������� ��������� �� ��� � ������
  New(NewBasePoint);
  NewBasePoint^.X := X;
  NewBasePoint^.Y := Y;
  FBasePoints.Add(NewBasePoint);
  Change;
end;

procedure TBaseVisualObject.BeginUpdate;
begin
  Inc(FLockCount);
end;

procedure TBaseVisualObject.Change;
begin
  if Assigned(FOnChange) and (FLockCount = 0) then
    FOnChange(Self);
end;

procedure TBaseVisualObject.ClearBasePoints;
var
  i: Integer;
begin
  // ����������� ������ ��� ������� ����� � ������� ������ �� ���������� �� ���
  for i := 0 to FBasePoints.Count - 1 do
    Dispose(PFloatPoint(FBasePoints[i]));
  FBasePoints.Clear;
  Change;
end;

constructor TBaseVisualObject.Create;
begin
  inherited Create;
  FBasePoints := TList.Create;
end;

procedure TBaseVisualObject.DeleteBasePoint(Index: Integer);
begin
  // ����������� ������, ���������� ��� �������� ��������� ������� �����, � �������
  // ��������� �� ������
  Dispose(PFloatPoint(FBasePoints[Index]));
  FBasePoints.Delete(Index);
  Change;
end;

destructor TBaseVisualObject.Destroy;
var
  i: Integer;
begin
  // ����� ������������ ������, ����������� ������ ��� �������
  for i := 0 to FBasePoints.Count - 1 do
    Dispose(PFloatPoint(FBasePoints[i]));
  FBasePoints.Free;
  inherited Destroy;
end;

procedure TBaseVisualObject.Draw(Canvas: TLogicalCanvas);
var
  i: Integer;
begin
  // ������ �������
  for i := 0 to VertexesCount - 1 do
    Canvas.DrawVertex(Vertex[i].X, Vertex[i].Y);
end;

procedure TBaseVisualObject.EndUpdate;
begin
  FLockCount := Max(0, FLockCount - 1);
  if FLockCount = 0 then
    Change;
end;

function TBaseVisualObject.GetBasePoint(Index: Integer): TFloatPoint;
begin
  Result := PFloatPoint(FBasePoints[Index])^;
end;

function TBaseVisualObject.GetBasePointsCount: Integer;
begin
  Result := FBasePoints.Count;
end;

procedure TBaseVisualObject.InsertBasePoint(Index: Integer; X, Y: Extended);
var
  NewBasePoint: PFloatPoint;
begin
  // �������� ������ ��� ����� ����� � ��������� ��������� �� ��� � ������
  New(NewBasePoint);
  NewBasePoint^.X := X;
  NewBasePoint^.Y := Y;
  FBasePoints.Insert(Index, NewBasePoint);
  Change;
end;

function TBaseVisualObject.SendCommand(ID: Cardinal; wParam,
  lParam: Longint): Longint;
var
  Message: TMessage;
begin
  Message.Msg := ID;
  Message.WParam := wParam;
  Message.LParam := lParam;
  Dispatch(Message);
  Result := Message.Result;
end;

procedure TBaseVisualObject.SetBasePoint(Index: Integer;
  const Value: TFloatPoint);
begin
  PFloatPoint(FBasePoints[Index])^ := Value;
  Change;
end;

procedure TBaseVisualObject.VOCBeginDrag(var Command: TVOCBeginDrag);
begin
  FDragging := True;
  FDragHitTest := Command.HitTest;
  FDragStartPos := Command.StartPos^;
end;

procedure TBaseVisualObject.VOCDrag(var Command: TVOCDrag);
var
  HitTest: Cardinal;
  Index: Integer;
  DeltaX, DeltaY: Extended;
begin
  if FDragging then begin
    // ������������ FDragHitTest �� ����� ��� ������� � ������
    HitTest := FDragHitTest and $FFFF0000;
    Index := FDragHitTest and $0000FFFF;
    // � ����������� �� ����, ��� ����� �������� ����, �������� ���������
    // �������
    case HitTest of
      HT_IN:
        begin
          // ���������� �������� ��������
          DeltaX := Command.NewPos^.X - FDragStartPos.X;
          DeltaY := Command.NewPos^.Y - FDragStartPos.Y;
          // � ��������� ��� �������� ����� ������� �� ������� �������
          FDragStartPos := Command.NewPos^;
          SendCommand(VOC_MOVE, Longint(@DeltaX), Longint(@DeltaY));
        end;
      HT_VERTEX: SendCommand(VOC_VERTEXMOVE, Index, Longint(Command.NewPos));
      HT_SIDE: SendCommand(VOC_SIDEMOVE, Index, Longint(Command.NewPos));
    end;
  end;
end;

procedure TBaseVisualObject.VOCEndDrag(var Command: TVOCEndDrag);
begin
  FDragging := False;
end;

procedure TBaseVisualObject.VOCGetCursor(var Command: TVOCGetCursor);
begin
  Command.Result := CR_DEFAULT;
end;

procedure TBaseVisualObject.VOCHitTest(var Command: TVOCHitTest);
begin
  Command.Result := HT_OUT;
end;

procedure TBaseVisualObject.VOCMove(var Command: TVOCMove);
var
  i: Integer;
  Pos: TFloatPoint;
begin
  BeginUpdate;
  try
    // ���������� ��� ������� �� �������� ��������
    for i := 0 to BasePointsCount - 1 do begin
      Pos := BasePoints[i];
      Pos.X := Pos.X + Command.DeltaX^;
      Pos.Y := Pos.Y + Command.DeltaY^;
      BasePoints[i] := Pos;
    end;
  finally
    EndUpdate;
  end;
end;

procedure TBaseVisualObject.VOCVertexMove(var Command: TVOCVertexMove);
begin
  Vertex[Command.Index] := Command.NewPos^;
end;

{ TRectVisualObject }

constructor TRectVisualObject.Create;
begin
  inherited Create;
  AddBasePoint(0, 0);
  AddBasePoint(0, 0);
end;

function TRectVisualObject.GetVertex(Index: Integer): TFloatPoint;
begin
  // 0 - ����� ������� ����
  // 1 - ������ ������� ����
  // 2 - ������ ������ ����
  // 3 - ����� ������ ����
  case Index of
    0: Result := BasePoints[0];
    1:
      begin
        Result.X := BasePoints[1].X;
        Result.Y := BasePoints[0].Y;
      end;
    2: Result := BasePoints[1];
    3:
      begin
        Result.X := BasePoints[0].X;
        Result.Y := BasePoints[1].Y;
      end;
    else
      TList.Error(@SListIndexError, Index);
  end;
end;

function TRectVisualObject.GetVertexesCount: Integer;
begin
  Result := 4;
end;

procedure TRectVisualObject.SetText(const Value: String);
begin
  if FText <> Value then begin
    FText := Value;
    Change;
  end;
end;

procedure TRectVisualObject.SetVertex(Index: Integer;
  const Value: TFloatPoint);
var
  Point: TFloatPoint;
begin
  // ������������� ����� �������� ������� ������ � ������ ����, ��� 0-�� �����
  // ������ ������ ���� ����� � ���� 1-��
  case Index of
    0:
      begin
        Point := BasePoints[0];
        Point.X := Min(Value.X, BasePoints[1].X);
        Point.Y := Min(Value.Y, BasePoints[1].Y);
        BasePoints[0] := Point;
      end;
    1:
      begin
        Point := BasePoints[1];
        Point.X := Max(Value.X, BasePoints[0].X);
        BasePoints[1] := Point;
        Point := BasePoints[0];
        Point.Y := Min(Value.Y, BasePoints[1].Y);
        BasePoints[0] := Point;
      end;
    2:
      begin
        Point := BasePoints[1];
        Point.X := Max(Value.X, BasePoints[0].X);
        Point.Y := Max(Value.Y, BasePoints[0].Y);
        BasePoints[1] := Point;
      end;
    3:
      begin
        Point := BasePoints[0];
        Point.X := Min(Value.X, BasePoints[1].X);
        BasePoints[0] := Point;
        Point := BasePoints[1];
        Point.Y := Max(Value.Y, BasePoints[0].Y);
        BasePoints[1] := Point;
      end;
    else
      TList.Error(@SListIndexError, Index);
  end;
end;

procedure TRectVisualObject.VOCConstructPoint(
  var Command: TVOCConstructPoint);
begin
  // ���� ������ �� ��������� � ������ ��������������� - ��������� ��� � ����
  // ����� � ������������� ��������� ����� ������� ������������� �����
  if not FConstructing then begin
    FConstructing := True;
    FCurrentPoint := 0;
  end;
  // � ����������� �� ������ ������������� �����, ��������� ������ ��������
  // ����������������
  case FCurrentPoint of
    0:
      begin
        // ���������� ��� ����� ������� � ���������
        BasePoints[0] := Command.Pos^;
        BasePoints[1] := Command.Pos^;
        // ��������������� �� ��������
        Command.Result := 1;
      end;
    1:
      begin
        // ���������� ����� � �������� 1
        BasePoints[1] := Command.Pos^;
        // ��������������� ��������
        FConstructing := False;
        Command.Result := 0;
      end;
  else
    TList.Error(@SListIndexError, FCurrentPoint);
  end;
  // ��������� ������� ������� �����
  Inc(FCurrentPoint);
end;

procedure TRectVisualObject.VOCGetCursor(var Command: TVOCGetCursor);
begin
  case Command.HitTest of
    HT_IN: Command.Result := CR_SIZEALL;
    HT_VERTEX + 0, HT_VERTEX + 2: Command.Result := CR_DIAG1;
    HT_VERTEX + 1, HT_VERTEX + 3: Command.Result := CR_DIAG2;
    HT_SIDE + 0, HT_SIDE + 2: Command.Result := CR_HORIZONTAL;
    HT_SIDE + 1, HT_SIDE + 3: Command.Result := CR_VERTICAL;
  else
    Command.Result := CR_DEFAULT;
  end;
end;

procedure TRectVisualObject.VOCHitTest(var Command: TVOCHitTest);
var
  sX1, sY1, sX2, sY2: Integer;
begin
  // ��������� � �������� ����������
  Command.ConvertIntf.LogToScreen(BasePoints[0].X, BasePoints[0].Y, sX1, sY1);
  Command.ConvertIntf.LogToScreen(BasePoints[1].X, BasePoints[1].Y, sX2, sY2);
  // �������� ������� � �����
  Command.Result := HT_OUT;
  if (Abs(Command.Params.XPos - sX1) <= Command.Params.Tolerance) and
     (Abs(Command.Params.YPos - sY1) <= Command.Params.Tolerance)
  then begin
    // ������� 0
    Command.Result := HT_VERTEX + 0;
    Exit;
  end;
  if (Abs(Command.Params.XPos - sX2) <= Command.Params.Tolerance) and
     (Abs(Command.Params.YPos - sY1) <= Command.Params.Tolerance)
  then begin
    // ������� 1
    Command.Result := HT_VERTEX + 1;
    Exit;
  end;
  if (Abs(Command.Params.XPos - sX2) <= Command.Params.Tolerance) and
     (Abs(Command.Params.YPos - sY2) <= Command.Params.Tolerance)
  then begin
    // ������� 2
    Command.Result := HT_VERTEX + 2;
    Exit;
  end;
  if (Abs(Command.Params.XPos - sX1) <= Command.Params.Tolerance) and
     (Abs(Command.Params.YPos - sY2) <= Command.Params.Tolerance)
  then begin
    // ������� 3
    Command.Result := HT_VERTEX + 3;
    Exit;
  end;
  if (Abs(Command.Params.XPos - sX1) <= Command.Params.Tolerance) and
     (Command.Params.YPos > sY1) and (Command.Params.YPos < sY2)
  then begin
    // ������� 0
    Command.Result := HT_SIDE + 0;
    Exit;
  end;
  if (Abs(Command.Params.YPos - sY1) <= Command.Params.Tolerance) and
     (Command.Params.XPos > sX1) and (Command.Params.XPos < sX2)
  then begin
    // ������� 1
    Command.Result := HT_SIDE + 1;
    Exit;
  end;
  if (Abs(Command.Params.XPos - sX2) <= Command.Params.Tolerance) and
     (Command.Params.YPos > sY1) and (Command.Params.YPos < sY2)
  then begin
    // ������� 2
    Command.Result := HT_SIDE + 2;
    Exit;
  end;
  if (Abs(Command.Params.YPos - sY2) <= Command.Params.Tolerance) and
     (Command.Params.XPos > sX1) and (Command.Params.XPos < sX2)
  then begin
    // ������� 1
    Command.Result := HT_SIDE + 3;
    Exit;
  end;
  if (Command.Params.XPos > sX1) and (Command.Params.XPos < sX2) and
     (Command.Params.YPos > sY1) and (Command.Params.YPos < sY2)
  then begin
    // ������
    Command.Result := HT_IN;
    Exit;
  end;
end;

procedure TRectVisualObject.VOCProcessConstruct(
  var Command: TVOCProcessConstruct);
begin
  // ���������� �������, ��������������� ������� �����.
  if FConstructing then
    case FCurrentPoint of
      0: Vertex[0] := Command.Pos^;
      1: Vertex[2] := Command.Pos^;
    end;
end;

procedure TRectVisualObject.VOCSideMove(var Command: TVOCSideMove);
var
  Point: TFloatPoint;
begin
  // 0 - ����� �������
  // 1 - ������� �������
  // 2 - ������ �������
  // 3 - ������ �������
  case Command.Index of
    0:
      begin
        Point := Vertex[0];
        Point.X := Command.NewPos^.X;
        Vertex[0] := Point;
      end;
    1:
      begin
        Point := Vertex[0];
        Point.Y := Command.NewPos^.Y;
        Vertex[0] := Point;
      end;
    2:
      begin
        Point := Vertex[2];
        Point.X := Command.NewPos^.X;
        Vertex[2] := Point;
      end;
    3:
      begin
        Point := Vertex[2];
        Point.Y := Command.NewPos^.Y;
        Vertex[2] := Point;
      end;
  else
    TList.Error(@SListIndexError, Command.Index);
  end;
end;

procedure TRectVisualObject.VOCStopConstruct(
  var Command: TVOCStopConstruct);
begin
  Command.Result := 1;
  if FConstructing then begin
    // ������� �� ������ ��������������� ������������� ���������� ��� � ���,
    // ��� ������ �� �������� �� �����
    FConstructing := False;
    Command.Result := 0;
  end;
end;

{ TLineVisualObject }

procedure TLineVisualObject.Draw(Canvas: TLogicalCanvas);
var
  i: Integer;
begin
  inherited;
  // ��������� ������� �������
  for i := 1 to VertexesCount - 1 do
    Canvas.DrawLine(Vertex[i - 1].X, Vertex[i - 1].Y, Vertex[i].X, Vertex[i].Y, 0.5);
end;

function TLineVisualObject.GetVertex(Index: Integer): TFloatPoint;
begin
  Result := BasePoints[Index];
end;

function TLineVisualObject.GetVertexesCount: Integer;
begin
  Result := BasePointsCount;
end;

procedure TLineVisualObject.SetVertex(Index: Integer;
  const Value: TFloatPoint);
begin
  BasePoints[Index] := Value;
end;

procedure TLineVisualObject.VOCConstructPoint(
  var Command: TVOCConstructPoint);
begin
  // ���� ��������������� ������ ������ - ��������� ������ � �����
  // ���������������, ������������� ��������� ��������� � ��������� ������ �����
  if not FConstructing then begin
    FConstructing := True;
    BeginUpdate;
    try
      ClearBasePoints;
      FCurrentPoint := 0;
      AddBasePoint(Command.Pos^.X, Command.Pos^.Y);
    finally
      EndUpdate;
    end;
  end;
  // ����� �� ������, ���������� �� ��������� ���������������, �������������
  // �� ����������� ����� NeedToStopConstruct
  Command.Result := NeedToStopConstruct(FCurrentPoint + 1);
  if Command.Result = 0 then begin;
    FConstructing := False;
    Exit;
  end;
  // ��������� ����� ����� � �������� ������ �������������
  AddBasePoint(Command.Pos^.X, Command.Pos^.Y);
  Inc(FCurrentPoint);
end;

procedure TLineVisualObject.VOCGetCursor(var Command: TVOCGetCursor);
begin
  if Command.HitTest <> HT_OUT then
    Command.Result := CR_SIZEALL
  else
    Command.Result := CR_DEFAULT;
end;

procedure TLineVisualObject.VOCHitTest(var Command: TVOCHitTest);
var
  i, sX1, sY1, sX2, sY2: Integer;
  D: Extended;
begin
  Command.Result := HT_OUT;
  for i := VertexesCount - 1 downto 0 do begin
    // ��������� � �������� ����������
    Command.ConvertIntf.LogToScreen(Vertex[i].X, Vertex[i].Y, sX1, sY1);
    if (Abs(Command.Params.XPos - sX1) <= Command.Params.Tolerance) and
       (Abs(Command.Params.YPos - sY1) <= Command.Params.Tolerance)
    then begin
      // ������� i
      Command.Result := HT_VERTEX + i;
      Exit;
    end;
  end;
  // �� �� ����� ��?
  for i := VertexesCount - 1 downto 1 do begin
    Command.ConvertIntf.LogToScreen(Vertex[i].X, Vertex[i].Y, sX1, sY1);
    Command.ConvertIntf.LogToScreen(Vertex[i - 1].X, Vertex[i - 1].Y, sX2, sY2);
    D := LineDistance(Command.Params.XPos, Command.Params.YPos,
      sX1, sY1, sX2, sY2);
    if D <= Command.Params.Tolerance then begin
      // �� �����
      Command.Result := HT_IN + i - 1;
      Exit;
    end;
  end;
end;

procedure TLineVisualObject.VOCProcessConstruct(
  var Command: TVOCProcessConstruct);
begin
  // ���������� ������� �����
  if FConstructing then
    BasePoints[FCurrentPoint] := Command.Pos^;
end;

procedure TLineVisualObject.VOCStopConstruct(
  var Command: TVOCStopConstruct);
begin
  Command.Result := 1;
  if FConstructing then begin
    // ������� �� ������ ���������������, ������� ������� ����� � ����
    // ����������� ������ ���� ����� - ���������� 0
    FConstructing := False;
    DeleteBasePoint(FCurrentPoint);
    if VertexesCount < 2 then
      Command.Result := 0;
  end;
end;

{ TSimpleLineBlock }

function TSimpleLineBlock.NeedToStopConstruct(Count: Integer): Longint;
begin
  Result := IfThen(Count < 2, 1, 0);
end;

{ TPolyLineBlock }

function TPolyLineBlock.NeedToStopConstruct(Count: Integer): Longint;
begin
  // ��������� ��������������� ����� ������ � ������� ������� VOC_STOPCONSTRUCT
  Result := 1;
end;

procedure TPolyLineBlock.VOCVControl(var Command: TVOCVControl);
var
  HitTest: Cardinal;
  Index: Integer;
begin
  // ������������ Command.HitTest �� ����� ��� ������� � ������
  HitTest := Command.HitTest and $FFFF0000;
  Index := Command.HitTest and $0000FFFF;
  // � ����������� �� HitTest ��������� ��� ������� �������. ������� �� ���������
  // ���� �� ���������� ������ ������ ����.
  case HitTest of
    HT_IN: InsertBasePoint(Index + 1, Command.Pos^.X, Command.Pos^.Y);
    HT_VERTEX: if VertexesCount > 2 then
      DeleteBasePoint(Index);
  end;
end;

{ TBeginEndBlock }

procedure TBeginEndBlock.Draw(Canvas: TLogicalCanvas);
begin
  Canvas.DrawRoundRect(BasePoints[0].X, BasePoints[0].Y, BasePoints[1].X,
    BasePoints[1].Y, 0.5);
  Canvas.DrawText(BasePoints[0].X, BasePoints[0].Y, BasePoints[1].X,
    BasePoints[1].Y, 5, Text);
  inherited Draw(Canvas);
end;

{ TInputOutputBlock }

procedure TInputOutputBlock.Draw(Canvas: TLogicalCanvas);
var
  Pts: TPointsArray;
begin
  SetLength(Pts, 4);
  Pts[0].X := Vertex[0].X + (Vertex[1].X - Vertex[0].X) / 6;
  Pts[0].Y := Vertex[0].Y;
  Pts[1].X := Vertex[1].X;
  Pts[1].Y := Vertex[1].Y;
  Pts[2].X := Vertex[2].X - (Vertex[2].X - Vertex[3].X) / 6;
  Pts[2].Y := Vertex[2].Y;
  Pts[3].X := Vertex[3].X;
  Pts[3].Y := Vertex[3].Y;
  Canvas.DrawPolygon(Pts, 0.5);
  Canvas.DrawText(BasePoints[0].X, BasePoints[0].Y, BasePoints[1].X,
    BasePoints[1].Y, 5, Text);
  inherited Draw(Canvas);
end;

{ TActionBlock }

procedure TActionBlock.Draw(Canvas: TLogicalCanvas);
begin
  Canvas.DrawRect(BasePoints[0].X, BasePoints[0].Y, BasePoints[1].X,
    BasePoints[1].Y, 0.5);
  Canvas.DrawText(BasePoints[0].X, BasePoints[0].Y, BasePoints[1].X,
    BasePoints[1].Y, 5, Text);
  inherited Draw(Canvas);
end;

{ TConditionBlock }

procedure TConditionBlock.Draw(Canvas: TLogicalCanvas);
var
  Pts: TPointsArray;
begin
  SetLength(Pts, 4);
  Pts[0].X := Vertex[0].X + (Vertex[1].X - Vertex[0].X) / 2;
  Pts[0].Y := Vertex[0].Y;
  Pts[1].X := Vertex[1].X;
  Pts[1].Y := Vertex[1].Y + (Vertex[2].Y - Vertex[1].Y) / 2;
  Pts[2].X := Vertex[0].X + (Vertex[1].X - Vertex[0].X) / 2;
  Pts[2].Y := Vertex[2].Y;
  Pts[3].X := Vertex[3].X;
  Pts[3].Y := Vertex[1].Y + (Vertex[2].Y - Vertex[1].Y) / 2;
  Canvas.DrawPolygon(Pts, 0.5);
  Canvas.DrawText(BasePoints[0].X, BasePoints[0].Y, BasePoints[1].X,
    BasePoints[1].Y, 5, Text);
  inherited Draw(Canvas);
end;

{ TSubBlock }

procedure TSubBlock.Draw(Canvas: TLogicalCanvas);
begin
  Canvas.DrawRect(BasePoints[0].X, BasePoints[0].Y, BasePoints[1].X,
    BasePoints[1].Y, 0.5);
  Canvas.DrawLine(BasePoints[0].X + (BasePoints[1].X - BasePoints[0].X) / 10,
    BasePoints[0].Y, BasePoints[0].X + (BasePoints[1].X - BasePoints[0].X) / 10,
    BasePoints[1].Y, 0.5);
  Canvas.DrawLine(BasePoints[1].X - (BasePoints[1].X - BasePoints[0].X) / 10,
    BasePoints[0].Y, BasePoints[1].X - (BasePoints[1].X - BasePoints[0].X) / 10,
    BasePoints[1].Y, 0.5);
  Canvas.DrawText(BasePoints[0].X, BasePoints[0].Y, BasePoints[1].X,
    BasePoints[1].Y, 5, Text);
  inherited Draw(Canvas);
end;

{ TTextBlock }

procedure TTextBlock.Draw(Canvas: TLogicalCanvas);
begin
  Canvas.DrawText(BasePoints[0].X, BasePoints[0].Y, BasePoints[1].X,
    BasePoints[1].Y, 5, Text);
  inherited Draw(Canvas);
end;

end.
