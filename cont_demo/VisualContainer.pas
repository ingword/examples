unit VisualContainer;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Graphics, Contnrs, Forms,
  CustomTypes, VisualObjects;

type
  TObjectEvent = procedure(Sender: TObject; Index: Integer) of object;

  // ���������-���������
  TVisualContainer = class(TCustomControl, ICoordConvert)
  private
    FScale: Extended;
    FTopRow: Integer;
    FLeftCol: Integer;
    FOriginX: Integer;
    FOriginY: Integer;
    FLogicalWidth: Integer;
    FLogicalHeight: Integer;
    FBorderStyle: TBorderStyle;
    FObjects: TObjectList;
    FObjectType: TVisualObjectClass;
    FConstructing: Boolean;
    FDragging: Boolean;
    FCurrentObject: TBaseVisualObject;
    FSelected: Integer;
    FOnObjectSelect: TObjectEvent;
    FOnBeginConstruct: TObjectEvent;
    FOnEndConstruct: TObjectEvent;
    procedure UpdateScrollBars;
    procedure StartConstruct;
    procedure FinishConstruct;
    procedure StartDrag(Index: Integer);
    procedure FinishDrag;
    procedure ObjectChanged(Sender: TObject);
    function FindObject(X, Y: Integer; var HitTest: Cardinal): Integer;
    procedure SetTopRow(ATopRow: Integer);
    procedure SetLeftCol(ALeftCol: Integer);
    procedure SetLogicalHeight(const Value: Integer);
    procedure SetLogicalWidth(const Value: Integer);
    procedure SetScale(const Value: Extended);
    procedure SetBorderStyle(const Value: TBorderStyle);
    procedure SetSelected(const Value: Integer);
    function Get(Index: Integer): TBaseVisualObject;
    function GetSelectedObject: TBaseVisualObject;
    // ��������� ���������
    procedure WMVScroll(var Msg: TWMVScroll); message WM_VSCROLL;
    procedure WMHScroll(var Msg: TWMHScroll); message WM_HSCROLL;
    procedure WMLButtonDown(var Msg: TWMMouse); message WM_LBUTTONDOWN;
    procedure WMLButtonUp(var Msg: TWMMouse); message WM_LBUTTONUP;
    procedure WMMouseMove(var Msg: TWMMouse); message WM_MOUSEMOVE;
    procedure WMRButtonDown(var Msg: TWMMouse); message WM_RBUTTONDOWN;
    procedure WMLButtonDblClk(var Msg: TWMMouse); message WM_LBUTTONDBLCLK;
    procedure WMMouseWheel(var Msg: TWMMouseWheel); message WM_MOUSEWHEEL;
    procedure WMKeyDown(var Msg: TWMKeyDown); message WM_KEYDOWN;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure Resize; override;
    procedure Paint; override;
    function DecodeCursor(Code: Cardinal): TCursor; virtual;
    // ������ �������� ��������� � ������ ��������
    procedure LogToScreen(lX, lY: Extended; var sX, sY: Integer); overload;
    procedure ScreenToLog(sX, sY: Integer; var lX, lY: Extended); overload;
    function LogToScreen(Value: Extended): Integer; overload; virtual;
    function ScreenToLog(Value: Integer): Extended; overload; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    // ���������������� ��������
    property Objects[Index: Integer]: TBaseVisualObject read Get;
    property ObjectType: TVisualObjectClass read FObjectType write FObjectType;
    property Selected: Integer read FSelected write SetSelected;
    property SelectedObject: TBaseVisualObject read GetSelectedObject;
  published
    // ����������� ��������
    property LogicalWidth: Integer read FLogicalWidth write SetLogicalWidth;
    property LogicalHeight: Integer read FLogicalHeight write SetLogicalHeight;
    property Scale: Extended read FScale write SetScale;
    // ����� ��������
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle default bsSingle;
    property Align;
    property Anchors;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BevelKind;
    property Constraints;
    property Enabled;
    property TabStop;
    property TabOrder;
    property Visible;
    // �������
    property OnObjectSelect: TObjectEvent read FOnObjectSelect write FOnObjectSelect;
    property OnBeginConstruct: TObjectEvent read FOnBeginConstruct write FOnBeginConstruct;
    property OnEndConstruct: TObjectEvent read FOnEndConstruct write FOnEndConstruct;
  end;

procedure Register;

implementation

uses Math;

procedure Register;
begin
  RegisterComponents('Samples', [TVisualContainer]);
end;

{ TVisualContainer }

constructor TVisualContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  // ����� �������������
  ControlStyle := [csClickEvents, csDoubleClicks];
  FBorderStyle := bsSingle;
  Width := 200;
  Height := 200;
  DoubleBuffered := True;
  // ����������� �������������
  FLogicalWidth := 297;
  FLogicalHeight := 210;
  FScale := 100;
  FSelected := -1;
  // ������� ������ ��������
  FObjects := TObjectList.Create(True);
end;

procedure TVisualContainer.CreateParams(var Params: TCreateParams);
const
  BorderStyles: array[TBorderStyle] of DWORD = (0, WS_BORDER);
begin
  inherited CreateParams(Params);
  with Params do begin
    Style := Style or BorderStyles[FBorderStyle];
    if NewStyleControls and Ctl3D and (FBorderStyle = bsSingle) then begin
      Style   := Style and not WS_BORDER;
      ExStyle := ExStyle or WS_EX_CLIENTEDGE;
    end;
  end;
end;

procedure TVisualContainer.CreateWnd;
begin
  inherited CreateWnd;
  UpdateScrollBars;
end;

function TVisualContainer.DecodeCursor(Code: Cardinal): TCursor;
begin
  case Code of
    CR_SIZEALL: Result := crSizeAll;
    CR_HORIZONTAL: Result := crSizeWE;
    CR_VERTICAL: Result := crSizeNS;
    CR_DIAG1: Result := crSizeNWSE;
    CR_DIAG2: Result := crSizeNESW;
  else
    Result := crDefault;
  end;
end;

destructor TVisualContainer.Destroy;
begin
  FObjects.Free;
  inherited Destroy;
end;

function TVisualContainer.FindObject(X, Y: Integer; var HitTest: Cardinal): Integer;
var
  i: Integer;
  Params: THitTestParams;
begin
  Result := -1;
  Params.XPos := X;
  Params.YPos := Y;
  Params.Tolerance := 3;
  // ���������� �� ������ �������� �� ����� � ������ � ������� �������, ���
  // ����������� �� ������� VOC_HITTEST �� ����� HT_OUT
  for i := FObjects.Count - 1 downto 0 do begin
    HitTest := TBaseVisualObject(FObjects[i]).SendCommand(VOC_HITTEST,
      Longint(Self as ICoordConvert), Longint(@Params));
    if HitTest <> HT_OUT
    then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TVisualContainer.FinishConstruct;
var
  Index: Integer;
begin
  FConstructing := False;
  Index := FObjects.IndexOf(FCurrentObject);
  if Assigned(FOnEndConstruct) then FOnEndConstruct(Self, Index);
  ReleaseCapture;
end;

procedure TVisualContainer.FinishDrag;
begin
  FDragging := False;
  ReleaseCapture;
end;

function TVisualContainer.Get(Index: Integer): TBaseVisualObject;
begin
  Result := TBaseVisualObject(FObjects[Index]);
end;

function TVisualContainer.GetSelectedObject: TBaseVisualObject;
begin
  Result := nil;
  if Selected > -1 then
    Result := Objects[Selected];
end;

function TVisualContainer.LogToScreen(Value: Extended): Integer;
var
  dpi: Integer;
begin
  dpi := Screen.PixelsPerInch;  // ����� �������� �� ����
  Result := Round(dpi * (Value / 25.4) * (Scale / 100));
end;

procedure TVisualContainer.LogToScreen(lX, lY: Extended; var sX,
  sY: Integer);
begin
  sX := LogToScreen(lX) + FOriginX;
  sY := LogToScreen(lY) + FOriginY;
end;

procedure TVisualContainer.ObjectChanged(Sender: TObject);
begin
  Invalidate;
end;

procedure TVisualContainer.Paint;
var
  LogicalCanvas: TLogicalCanvas;
  i: Integer;
begin
  // ����������� ��� �����
  Canvas.Brush.Color := clWhite;
  Canvas.FillRect(ClientRect);
  // �������� ���������� �����
  LogicalCanvas := TLogicalCanvas.Create(Canvas, Self);
  try
    Canvas.Font.Name := 'Arial';
    // ������ �������������� ���� A4 �����
    Canvas.Pen.Color := clBlack;
    LogicalCanvas.DrawRect(0, 0, LogicalWidth, LogicalHeight, 1);
    // ������� ��� �������. ���������� - ������� ������
    for i := 0 to FObjects.Count - 1 do begin
      if i = Selected then begin
        Canvas.Pen.Color := clLime;
        Canvas.Font.Color := clLime;
      end
      else begin
        Canvas.Pen.Color := clBlack;
        Canvas.Font.Color := clBlack;
      end;
      TBaseVisualObject(FObjects[i]).Draw(LogicalCanvas);
    end;
  finally
    LogicalCanvas.Free;
  end;
end;

procedure TVisualContainer.Resize;
begin
  inherited Resize;
  UpdateScrollBars;
end;

function TVisualContainer.ScreenToLog(Value: Integer): Extended;
var
  dpi: Integer;
begin
  dpi := Screen.PixelsPerInch;  // ����� �������� �� ����
  Result := Round(((Value / dpi) * 25.4) / (Scale / 100));
end;

procedure TVisualContainer.ScreenToLog(sX, sY: Integer; var lX,
  lY: Extended);
begin
  lX := ScreenToLog(sX - FOriginX);
  lY := ScreenToLog(sY - FOriginY);
end;

procedure TVisualContainer.SetBorderStyle(const Value: TBorderStyle);
begin
  if Value <> FBorderStyle then begin
    FBorderStyle := Value;
    RecreateWnd;
  end;
end;

procedure TVisualContainer.SetLeftCol(ALeftCol: Integer);
var
  WMax: Integer;
begin
  WMax := FLogicalWidth - Trunc(ScreenToLog(ClientWidth));
  FLeftCol := Max(0, Min(ALeftCol, WMax));
  UpdateScrollBars;
  Invalidate;
end;

procedure TVisualContainer.SetLogicalHeight(const Value: Integer);
begin
  FLogicalHeight := Value;
  UpdateScrollBars;
  Invalidate;
end;

procedure TVisualContainer.SetLogicalWidth(const Value: Integer);
begin
  FLogicalWidth := Value;
  UpdateScrollBars;
  Invalidate;
end;

procedure TVisualContainer.SetScale(const Value: Extended);
begin
  FScale := Value;
  UpdateScrollBars;
  Invalidate;
end;

procedure TVisualContainer.SetSelected(const Value: Integer);
begin
  if FSelected <> Value then begin
    FSelected := Value;
    if Assigned(FOnObjectSelect) then FOnObjectSelect(Self, Value);
    Invalidate;
  end;
end;

procedure TVisualContainer.SetTopRow(ATopRow: Integer);
var
  HMax: Integer;
begin
  HMax := FLogicalHeight - Trunc(ScreenToLog(ClientHeight));
  FTopRow := Max(0, Min(ATopRow, HMax));
  UpdateScrollBars;
  Invalidate;
end;

procedure TVisualContainer.StartConstruct;
begin
  FConstructing := True;
  FCurrentObject := ObjectType.Create;
  FCurrentObject.OnChange := ObjectChanged;
  Selected := FObjects.Add(FCurrentObject);
  if Assigned(FOnBeginConstruct) then FOnBeginConstruct(Self, Selected);
  SetCapture(Handle);
end;

procedure TVisualContainer.StartDrag(Index: Integer);
begin
  FDragging := True;
  FCurrentObject := Objects[Index];
  SetCapture(Handle);
end;

procedure TVisualContainer.UpdateScrollBars;
var
  h, w: Integer;
  si: TScrollInfo;
begin
  if not HandleAllocated then
    Exit;
  h := Round(ScreenToLog(ClientHeight));
  w := Round(ScreenToLog(ClientWidth));
  si.cbSize := SizeOf(si);
  si.fMask := SIF_PAGE or SIF_POS or SIF_RANGE;
  si.nMin := 0;
  // ����������� ��������� ������������ ������ ���������
  si.nMax := IfThen(FLogicalHeight > h, FLogicalHeight, 0);
  si.nPage := IfThen(si.nMax <> 0, h, 0);
  if (si.nMax <> 0) and (FTopRow > 0) then begin
    si.nPos := FTopRow;
    si.nTrackPos := FTopRow;
  end
  else begin
    FTopRow := 0;
    si.nPos := 0;
    si.nTrackPos := 0;
  end;
  FOriginY := IfThen(si.nMax <> 0, -LogToScreen(FTopRow),
    (ClientHeight - LogToScreen(FLogicalHeight)) div 2);
  SetScrollInfo(Handle, SB_VERT, si, True);
  // ����������� ��������� �������������� ������ ���������
  si.nMax := IfThen(FLogicalWidth > w, FLogicalWidth, 0);
  si.nPage := IfThen(si.nMax <> 0, w, 0);
  if (si.nMax <> 0) and (FLeftCol > 0) then begin
    si.nPos := FLeftCol;
    si.nTrackPos := FLeftCol;
  end
  else begin
    FLeftCol := 0;
    si.nPos := 0;
    si.nTrackPos := 0;
  end;
  FOriginX := IfThen(si.nMax <> 0, -LogToScreen(FLeftCol),
    (ClientWidth - LogToScreen(FLogicalWidth)) div 2);
  SetScrollInfo(Handle, SB_HORZ, si, True);
end;

procedure TVisualContainer.WMHScroll(var Msg: TWMHScroll);
var
  w: Integer;
  si: TScrollInfo;
begin
  inherited;
  w := Round(ScreenToLog(ClientWidth));
  case Msg.ScrollCode of
    SB_LINELEFT: SetLeftCol(FLeftCol - 1);
    SB_LINERIGHT: SetLeftCol(FLeftCol + 1);
    SB_PAGELEFT: SetLeftCol(FLeftCol - w);
    SB_PAGERIGHT: SetLeftCol(FLeftCol + w);
    SB_THUMBPOSITION, SB_THUMBTRACK:
      begin
        si.cbSize := SizeOf(TScrollInfo);
        si.fMask  := SIF_TRACKPOS;
        if GetScrollInfo(Handle, SB_HORZ, si) then
          SetLeftCol(si.nTrackPos);
      end;
  end;
end;

procedure TVisualContainer.WMKeyDown(var Msg: TWMKeyDown);
var
  Index: Integer;
begin
  inherited;
  if (Msg.CharCode = VK_DELETE) and (Selected > -1) then begin
    // ������� ���������� ������
    Selected := -1;
    Index := FObjects.IndexOf(FCurrentObject);
    FObjects.Delete(Index);
    Invalidate;
  end;
end;

procedure TVisualContainer.WMLButtonDblClk(var Msg: TWMMouse);
var
  Pos: TFloatPoint;
  Index: Integer;
  HitTest: Cardinal;
begin
  inherited;
  ScreenToLog(Msg.XPos, Msg.YPos, Pos.X, Pos.Y);
  if ObjectType = nil then begin
  // ���� ������ ��� �����
    Index := FindObject(Msg.XPos, Msg.YPos, HitTest);
    if Index > -1 then begin
      // ���������� ������� VOC_VCONTROL
      Objects[Index].SendCommand(VOC_VCONTROL, HitTest, Longint(@Pos));
      Invalidate;
    end;
  end;
end;

procedure TVisualContainer.WMLButtonDown(var Msg: TWMMouse);
var
  Pos: TFloatPoint;
  Index: Integer;
  HitTest: Cardinal;
begin
  inherited;
  SetFocus;
  ScreenToLog(Msg.XPos, Msg.YPos, Pos.X, Pos.Y);
  if ObjectType = nil then begin
    // ���� ������ ��� �����
    Index := FindObject(Msg.XPos, Msg.YPos, HitTest);
    Selected := Index;
    if Index > -1 then begin
      // ������ ������, ������ � ����� �������������� � ���������� �������
      // ��� � ������
      StartDrag(Index);
      Objects[Index].SendCommand(VOC_BEGINDRAG, HitTest, Longint(@Pos));
    end;
  end
  else begin
    if not FConstructing then
      // ������ � ����� ���������������
      StartConstruct;
    // ���������� ������� ��������� �����
    if FCurrentObject.SendCommand(VOC_CONSTRUCTPOINT, 0, Longint(@Pos)) = 0 then
      // ��������������� ���������
      FinishConstruct;
  end;
end;

procedure TVisualContainer.WMLButtonUp(var Msg: TWMMouse);
begin
  inherited;
  if FDragging then begin
    // ��������� ��������������
    FCurrentObject.SendCommand(VOC_ENDDRAG, 0, 0);
    FinishDrag;
  end;
end;

procedure TVisualContainer.WMMouseMove(var Msg: TWMMouse);
var
  Pos: TFloatPoint;
  Index: Integer;
  cr: TCursor;
  HitTest, Code: Cardinal;
begin
  inherited;
  // ���������� ��� �������
  cr := crDefault;
  if ObjectType = nil then begin
    Index := FindObject(Msg.XPos, Msg.YPos, HitTest);
    if Index > -1 then begin
      Code := Objects[Index].SendCommand(VOC_GETCURSOR, 0, HitTest);
      cr := DecodeCursor(Code);
    end;
  end;
  Cursor := cr;
  ScreenToLog(Msg.XPos, Msg.YPos, Pos.X, Pos.Y);
  if FConstructing then begin
    // ��������� ������� �������
    FCurrentObject.SendCommand(VOC_PROCESSCONSTRUCT, 0, Longint(@Pos));
    Invalidate;
  end;
  if FDragging then begin
    // ���������� ������
    FCurrentObject.SendCommand(VOC_DRAG, 0, Longint(@Pos));
    Invalidate;
  end;
end;

procedure TVisualContainer.WMMouseWheel(var Msg: TWMMouseWheel);
begin
  inherited;
  SetTopRow(FTopRow - Round(ScreenToLog(Msg.WheelDelta)));
end;

procedure TVisualContainer.WMRButtonDown(var Msg: TWMMouse);
var
  Index: Integer;
begin
  inherited;
  if FConstructing then begin
    if FCurrentObject.SendCommand(VOC_STOPCONSTRUCT, 0, 0) = 0 then begin
      // ������ �� ����� ���� ������
      Selected := -1;
      Index := FObjects.IndexOf(FCurrentObject);
      FObjects.Delete(Index);
    end;
    FinishConstruct;
    Invalidate;
  end;
end;

procedure TVisualContainer.WMVScroll(var Msg: TWMVScroll);
var
  h: Integer;
  si: TScrollInfo;
begin
  inherited;
  h := Round(ScreenToLog(ClientHeight));
  case Msg.ScrollCode of
    SB_LINEUP: SetTopRow(FTopRow - 1);
    SB_LINEDOWN: SetTopRow(FTopRow + 1);
    SB_PAGEUP: SetTopRow(FTopRow - h);
    SB_PAGEDOWN: SetTopRow(FTopRow + h);
    SB_THUMBPOSITION, SB_THUMBTRACK:
      begin
        si.cbSize := SizeOf(TScrollInfo);
        si.fMask  := SIF_TRACKPOS;
        if GetScrollInfo(Handle, SB_VERT, si) then
          SetTopRow(si.nTrackPos);
      end;
  end;
end;

end.
