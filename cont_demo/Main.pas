unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VisualContainer, ExtCtrls, ToolWin, ComCtrls, StdCtrls, ImgList;

type
  TForm1 = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    Edit1: TEdit;
    Panel2: TPanel;
    Panel3: TPanel;
    ComboBox1: TComboBox;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ImageList1: TImageList;
    s2: TToolButton;
    s1: TToolButton;
    VisualContainer1: TVisualContainer;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure VisualContainer1ObjectSelect(Sender: TObject;
      Index: Integer);
    procedure Edit1Change(Sender: TObject);
    procedure VisualContainer1EndConstruct(Sender: TObject;
      Index: Integer);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses VisualObjects, Types;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  ToolButton1.Tag := Integer(nil);
  ToolButton2.Tag := Integer(TSimpleLineBlock);
  ToolButton3.Tag := Integer(TPolyLineBlock);
  ToolButton4.Tag := Integer(TBeginEndBlock);
  ToolButton5.Tag := Integer(TInputOutputBlock);
  ToolButton6.Tag := Integer(TActionBlock);
  ToolButton7.Tag := Integer(TConditionBlock);
  ToolButton8.Tag := Integer(TSubBlock);
  ToolButton9.Tag := Integer(TTextBlock);
end;

procedure TForm1.ToolButton1Click(Sender: TObject);
begin
  VisualContainer1.ObjectType := TVisualObjectClass((Sender as TComponent).Tag);
end;

procedure TForm1.VisualContainer1ObjectSelect(Sender: TObject;
  Index: Integer);
begin
  if (Index > -1) and (VisualContainer1.Objects[Index] is TRectVisualObject)
  then begin
    Edit1.Enabled := True;
    Edit1.Text := (VisualContainer1.Objects[Index] as TRectVisualObject).Text;
  end
  else begin
    Edit1.Enabled := False;
    Edit1.Text := '';
  end;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  if VisualContainer1.SelectedObject is TRectVisualObject then
    (VisualContainer1.SelectedObject as TRectVisualObject).Text := Edit1.Text;
end;

procedure TForm1.VisualContainer1EndConstruct(Sender: TObject;
  Index: Integer);
begin
  ToolButton1.Down := True;
  ToolButton1.Click;
end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  case ComboBox1.ItemIndex of
    0: VisualContainer1.Scale := 10;
    1: VisualContainer1.Scale := 25;
    2: VisualContainer1.Scale := 50;
    3: VisualContainer1.Scale := 100;
    4: VisualContainer1.Scale := 200;
    5: VisualContainer1.Scale := 400;
  end;
end;

end.
