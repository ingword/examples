program TreeViewIterator;

uses
  System.StartUpCopy,
  FMX.Forms,
  uMain in 'uMain.pas' {fmMain},
  FMX.DUnit.Interfaces in '..\..\mindstream\fmx.dunit\FMX.DUnit.Interfaces.pas',
  FMX.DUnit.msAppLog in '..\..\mindstream\fmx.dunit\FMX.DUnit.msAppLog.pas',
  FMX.DUnit.msLog in '..\..\mindstream\fmx.dunit\FMX.DUnit.msLog.pas',
  FMX.DUnit.Settings in '..\..\mindstream\fmx.dunit\FMX.DUnit.Settings.pas',
  Script.Interfaces in '..\..\mindstream\fmx.dunit\Script.Interfaces.pas',
  u_FirstTest in '..\..\mindstream\fmx.dunit\u_FirstTest.pas',
  u_fmGUITestRunner in '..\..\mindstream\fmx.dunit\u_fmGUITestRunner.pas' {fmGUITestRunner},
  u_SecondTest in '..\..\mindstream\fmx.dunit\u_SecondTest.pas',
  u_TCounter in '..\..\mindstream\fmx.dunit\u_TCounter.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMain, fmMain);
  u_fmGUITestRunner.RunRegisteredTestsModeless;
  Application.Run;
end.
