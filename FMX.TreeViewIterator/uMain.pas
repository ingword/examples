unit uMain;

interface

uses
  System.SysUtils
  , System.Types
  , System.UITypes
  , System.Classes
  , System.Variants
  , FMX.Types
  , FMX.Controls
  , FMX.Forms
  , FMX.Graphics
  , FMX.Dialogs
  , FMX.TreeView
  , FMX.StdCtrls
  , FMX.Controls.Presentation
  , FMX.Edit
  , FMX.Layouts
  , FMX.Memo
  , System.Generics.Collections
  ;

type
  IIterator = interface
   function HasNext: Boolean;
   function Next: TTreeViewItem;
  end;

  IAggregate = interface
   function CreateIterator: IIterator;
  end;

 TTreeAggregate = class(TTreeView, IAggregate)
  function CreateIterator: IIterator;
  constructor Create(AOwner: TComponent); override;
 end;

  TTreeIterator = class(TInterfacedObject, IIterator)
  strict private
   f_Tree: TTreeAggregate;
   f_Stack: TStack<TTreeViewItem>;
  public
   constructor Create(aTree: TTreeAggregate);
   destructor Destroy; reintroduce;
   function HasNext: Boolean;
   function Next: TTreeViewItem;
  end;

type
  TfmMain = class(TForm)
    btnTreeToMemo: TButton;
    edtFind: TEdit;
    mmoFindResult: TMemo;
    btnCreateTree: TButton;
    btnFindNode: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure btnTreeToMemoClick(Sender: TObject);
    procedure btnCreateTreeClick(Sender: TObject);
    procedure btnFindNodeClick(Sender: TObject);
  private
    f_Tree : TTreeAggregate;
  public
   function CreateTree: TTreeAggregate;
  end;

var
  fmMain: TfmMain;

implementation

{$R *.fmx}

procedure TfmMain.btnCreateTreeClick(Sender: TObject);
begin
 f_Tree := CreateTree;
end;

procedure TfmMain.btnFindNodeClick(Sender: TObject);
var
 l_Iterator: IIterator;
 l_Node: TTreeViewItem;
begin
 assert(Assigned(f_Tree));

 l_Iterator := f_Tree.CreateIterator;

 while l_Iterator.HasNext do
 begin
  l_Node :=  l_Iterator.Next;
  if Pos(edtFind.Text, l_Node.Text) > 0 then
   l_Node.IsChecked := True;
 end;

 l_Iterator := nil;
end;

procedure TfmMain.btnTreeToMemoClick(Sender: TObject);
var
 l_Iterator: IIterator;
begin
 assert(Assigned(f_Tree));

 l_Iterator := f_Tree.CreateIterator;

 while l_Iterator.HasNext do
  mmoFindResult.Text := mmoFindResult.Text + #10#13 + l_Iterator.Next.Text;

 l_Iterator := nil;
end;

function TfmMain.CreateTree: TTreeAggregate;
var
 l_NewRootNode,
 l_AnotherLevelNode : TTreeViewItem;

 l_FirstLevel,
 l_SecondLevel : Integer;
begin
 Result := TTreeAggregate.Create(self);
 Result.Height := Self.ClientHeight;
 Result.Width := Self.ClientWidth div 2;
 Result.ShowCheckboxes := True;
 self.AddObject(Result);

 for l_FirstLevel := 0 to 5 do
 begin
  l_NewRootNode := TTreeViewItem.Create(self);
  l_NewRootNode.Text := 'FirstLevel ' + IntToStr(l_FirstLevel);
  Result.AddObject(l_NewRootNode);

  if (l_FirstLevel div 2 = 0) then
  begin
   l_AnotherLevelNode := TTreeViewItem.Create(self);
   l_AnotherLevelNode.Text := 'SecondLevel ' + IntToStr(l_FirstLevel + 1);
   l_NewRootNode.AddObject(l_AnotherLevelNode);

   l_NewRootNode := TTreeViewItem.Create(self);
   l_NewRootNode.Text := 'Test 2 ' + IntToStr(l_FirstLevel);
   Result.AddObject(l_NewRootNode);

   for l_SecondLevel := 0 to 4 do
    if (l_SecondLevel div 3 = 0) then
     begin
      l_NewRootNode := TTreeViewItem.Create(self);
      l_NewRootNode.Text := 'ThirdLevel ' + IntToStr(l_SecondLevel);
      l_AnotherLevelNode.AddObject(l_NewRootNode);

      l_NewRootNode := TTreeViewItem.Create(self);
      l_NewRootNode.Text := 'Test 3 ' + IntToStr(l_FirstLevel);
      Result.AddObject(l_NewRootNode);
     end;
  end;
 end;

 Result.ExpandAll;
end;

procedure TfmMain.FormDestroy(Sender: TObject);
begin
 FreeAndNil(f_Tree);
end;

function TTreeAggregate.CreateIterator: IIterator;
begin
 Result := TTreeIterator.Create(self);
end;

constructor TTreeIterator.Create(aTree: TTreeAggregate);
 procedure TraverseNode(aTTreeViewItem: TTreeViewItem);
 var
  l_I : Integer;
 begin
  for l_I := 0 to Pred(aTTreeViewItem.Count) do
  begin
   f_Stack.Push(aTTreeViewItem.Items[l_I]);

   if aTTreeViewItem.Items[l_I].Count > 0 then
    TraverseNode(aTTreeViewItem.Items[l_I])
  end;
 end;

var
 l_Index : integer;
begin
 inherited Create;
 self.f_Tree := aTree;

 f_Stack := TStack<TTreeViewItem>.Create;
 f_Stack.Push(nil);

 if f_Tree.Count=0 then Exit;

 for l_Index := 0 to Pred(f_Tree.Count) do
 begin
  f_Stack.Push(f_Tree.Items[l_Index]);

  if f_Tree.Items[l_Index].Count > 0 then
   TraverseNode(f_Tree.Items[l_Index]);
 end;
end;

function TTreeIterator.Next: TTreeViewItem;
begin
 Result := f_Stack.Pop;
End;

function TTreeIterator.HasNext: Boolean;
begin
 // ��� ��� �� �������� ���������� ���� ����� f_Stack.Push(nil); � Create,
 // �� ���� ��� �������� ����� ��������, ����� ��������� 1 �������
 if f_Stack.Count > 1 then
  Result := True
 else
  Result := False;
end;

destructor TTreeIterator.Destroy;
begin
 FreeAndNil(f_Stack);
 inherited;
end;

{ TTreeAggregate }

constructor TTreeAggregate.Create(aOwner: TComponent);
begin
 inherited;
end;

end.
