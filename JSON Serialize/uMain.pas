﻿unit uMain;

interface

uses
 System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
 FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
 FMX.Layouts, FMX.Memo, FMX.TreeView;

type
 TForm2 = class(TForm)
  SaveDialog1: TSaveDialog;
  Memo1: TMemo;
  btSaveJson: TButton;
  btSaveEMB_Example: TButton;
  procedure btCreateShapeClick(Sender: TObject);
  procedure btSaveJsonClick(Sender: TObject);
  procedure btSaveEMB_ExampleClick(Sender: TObject);
 private
  { Private declarations }
 public
  { Public declarations }
 end;

type
 TmsShape = class
 private
  fInt: integer;
  [JSONMarshalled(False)]
  fStr: String;
  fList: TStringList;
 public
  constructor Create(const aInt: integer; const aStr: String);
 end;

var
 Form2: TForm2;

implementation

uses
 json,
 dbxjsonreflect,
 Generics.Collections,
 uFromEmbarcadero;

const
 с_FileNameSave = 'e:\TestingJson.Json';
{$R *.fmx}

procedure TForm2.btCreateShapeClick(Sender: TObject);
var
 l_Shape: TmsShape;
begin
 l_Shape := TmsShape.Create(7, 'Hello World');
 Memo1.Text := l_Shape.fStr;
end;

{ TmsShape }

constructor TmsShape.Create(const aInt: integer; const aStr: String);
begin
 fInt := aInt;
 fStr := aStr;
 fList := TStringList.Create;
end;

procedure TForm2.btSaveEMB_ExampleClick(Sender: TObject);
begin
 Memo1.Lines.Assign(mainproc);
end;

procedure TForm2.btSaveJsonClick(Sender: TObject);
var
 l_Marshal: TJSONMarshal;
 unm: TJSONUnMarshal;

 l_Json: TJSONObject;

 l_Shape1: TmsShape;
 l_Shape2: TmsShape;

 l_StringList: TStringList;

 l_Converter: TObjectsConverter;
begin
 l_Shape1 := TmsShape.Create(1, 'First');
 l_Shape1.fList.Add('qwe');
 l_Shape1.fList.Add('asd');
 l_Shape1.fList.Add('zxc');
 try
  try

   l_Marshal := TJSONMarshal.Create;
   unm := TJSONUnMarshal.Create;

   l_StringList := TStringList.Create;
   try

    l_Converter := function(Data: TObject; Field: String): TListOfObjects
     var
      l_StringList: TStringList;
      i: integer;
     begin
      l_StringList := TStringList.Create();
      SetLength(Result, l_StringList.Count);
      if l_StringList.Count <> 0
       then for i := 0 to l_StringList.Count - 1
             do Result[i] := TObject(l_StringList[i]);
     end;

    // Register a specific converter for field FList
    l_Marshal.RegisterConverter(TStringList, 'fList', l_Converter);

    l_Json := l_Marshal.Marshal(l_Shape1) as TJSONObject;
    try
     Memo1.Lines.Text := l_Json.tostring;

     l_StringList.Add(l_Json.tostring);
     l_StringList.SaveToFile(с_FileNameSave);
    finally
     FreeAndNil(l_Json);
     FreeAndNil(l_StringList);
    end;
   finally
    FreeAndNil(l_Marshal);
   end;
  finally
  end;
 finally
  FreeAndNil(l_Shape1);
  FreeAndNil(l_Shape2);
 end;
end;

end.
