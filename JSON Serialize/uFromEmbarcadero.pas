unit uFromEmbarcadero;

interface

uses
 SysUtils,
 Classes,
 JSON,
 DBXJSON,
 DBXJSONReflect;

type
 TAddress = record
  FStreet: String;
  FCity: String;
  FCode: String;
  FCountry: String;
  FDescription: TStringList;
 end;

 TPerson = class
 private
  FName: string;
  FHeight: integer;
  FAddress: TAddress;
  FSex: char;
  FRetired: boolean;
  FChildren: array of TPerson;
  FNumbers: set of 1 .. 10;
 public
  constructor Create;
  destructor Destroy; override;

  procedure AddChild(kid: TPerson);
 end;

function MainProc: TStringList;

var
 m: TJSONMarshal;
 unm: TJSONUnMarshal;

implementation

constructor TPerson.Create;
begin
 FAddress.FDescription := TStringList.Create;
end;

destructor TPerson.Destroy;
begin
 FAddress.FDescription.Free;
 inherited;
end;

procedure TPerson.AddChild(kid: TPerson);
begin
 SetLength(FChildren, Length(FChildren) + 1);
 FChildren[Length(FChildren) - 1] := kid;
end;

function MainProc: TStringList;
var
 person, newperson: TPerson;
 kid: TPerson;
 JSONString: String;

begin

 Result := TStringList.Create;
 m := TJSONMarshal.Create(TJSONConverter.Create);
 unm := TJSONUnMarshal.Create;

 { For each complex field type, we will define a converter/reverter pair. We will individually deal
   with the "FChildren" array, the "TStringList" type, and the "FAddress" record. We will transform
   the array type into an actual array of "TPerson", as illustrated below. }

 m.RegisterConverter(TPerson, 'FChildren',
  function(Data: TObject; Field: String): TListOfObjects
  var
   obj: TPerson;
   I: integer;

  begin
   SetLength(Result, Length(TPerson(Data).FChildren));
   I := Low(Result);
   for obj in TPerson(Data).FChildren do
   begin
    Result[I] := obj;
    Inc(I);
   end;
  end);

 { The implementation is quite straightforward: each child "TPerson" is appended to a predefined
   type instance "TListOfObjects". Later on, each of these objects will be serialized by the same
   marshaller and added to a "TJSONArray" instance. The reverter will receive as argument a
   "TListOfObjects" being oblivious of the "TJSONArray" used for that. }

 { For "TStringList", we will have a generic converter that can be reused for other marshal instances.
   The converter simply returns the array of strings of the list. }

 { Note that this converter is not really needed here because the TStringList converter is already
   implemented in the Marshaller. }

 m.RegisterConverter(TStringList,
  function(Data: TObject): TListOfStrings
  var
   I, count: integer;

  begin
   count := TStringList(Data).count;
   SetLength(Result, count);
   for I := 0 to count - 1 do
    Result[I] := TStringList(Data)[I];
  end);

 { Finally, the address record will be transformed into an array of strings, one for each record
   field with the description content at the end of it. }

 m.RegisterConverter(TPerson, 'FAddress',
  function(Data: TObject; Field: String): TListOfStrings
  var
   person: TPerson;
   I: integer;
   count: integer;

  begin
   person := TPerson(Data);
   if person.FAddress.FDescription <> nil then
    count := person.FAddress.FDescription.count
   else
    count := 0;
   SetLength(Result, count + 4);
   Result[0] := person.FAddress.FStreet;
   Result[1] := person.FAddress.FCity;
   Result[2] := person.FAddress.FCode;
   Result[3] := person.FAddress.FCountry;
   for I := 0 to count - 1 do
    Result[4 + I] := person.FAddress.FDescription[I];
  end);

 { It is easy to imagine the reverter's implementation, present below in bulk. }

 unm.RegisterReverter(TPerson, 'FChildren',
  procedure(Data: TObject; Field: String; Args: TListOfObjects)
  var
   obj: TObject;
   I: integer;

  begin
   SetLength(TPerson(Data).FChildren, Length(Args));
   I := Low(TPerson(Data).FChildren);
   for obj in Args do
   begin
    TPerson(Data).FChildren[I] := TPerson(obj);
    Inc(I);
   end
  end);

 { Note that this reverter is not really needed here because the TStringList reverter is already
   implemented in the Unmarshaller. }

 unm.RegisterReverter(TStringList,
  function(Data: TListOfStrings): TObject
  var
   StrList: TStringList;
   Str: string;

  begin
   StrList := TStringList.Create;
   for Str in Data do
   begin
    StrList.Add(Str);
   end;
   Result := StrList;
  end);

 unm.RegisterReverter(TPerson, 'FAddress',
  procedure(Data: TObject; Field: String; Args: TListOfStrings)
  var
   person: TPerson;
   I: integer;

  begin
   person := TPerson(Data);
   if person.FAddress.FDescription <> nil then
    person.FAddress.FDescription.Clear
   else if Length(Args) > 4 then
    person.FAddress.FDescription := TStringList.Create;

   person.FAddress.FStreet := Args[0];
   person.FAddress.FCity := Args[1];
   person.FAddress.FCode := Args[2];
   person.FAddress.FCountry := Args[3];
   for I := 4 to Length(Args) - 1 do
    person.FAddress.FDescription.Add(Args[I]);
  end);

 { The test code is as follows. }

 person := TPerson.Create;
 person.FName := 'John Doe';
 person.FHeight := 167;
 person.FSex := 'M';
 person.FRetired := false;
 person.FAddress.FStreet := '62 Peter St';
 person.FAddress.FCity := 'TO';
 person.FAddress.FCode := '1334566';
 person.FAddress.FDescription.Add('Driving directions: exit 84 on highway 66');
 person.FAddress.FDescription.Add('Entry code: 31415');

 kid := TPerson.Create;
 kid.FName := 'Jane Doe';
 person.AddChild(kid);

 { Marshal the "person" as a JSONValue and display its contents. }

 JSONString := m.Marshal(person).ToString;
 Result.Clear;
 Result.Add(JSONString);
 Result.Add('-----------------------');

 { Unmarshal the JSONString to a TPerson class }

 newperson := unm.Unmarshal(TJSONObject.ParseJSONValue(JSONString)) as TPerson;
 Result.Add(newperson.FName);
 Result.Add(IntToStr(newperson.FHeight));

 { and so on for the other fields }
end;

end.
