unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Styles;

type
  TForm1 = class(TForm)
    btn1: TButton;
    chk1: TCheckBox;
    grp1: TGroupBox;
    rb1: TRadioButton;
    stylbk1: TStyleBook;
    tmr1: TTimer;
    btnDark: TButton;
    btnLight: TButton;
    procedure tmr1Timer(Sender: TObject);
    procedure btnLightClick(Sender: TObject);
    procedure btnDarkClick(Sender: TObject);
  private
    FIsDark: Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.btnDarkClick(Sender: TObject);
begin
  TStyleManager.TrySetStyleFromResource('Dark');
end;

procedure TForm1.btnLightClick(Sender: TObject);
begin
  TStyleManager.TrySetStyleFromResource('Light');
end;

procedure TForm1.tmr1Timer(Sender: TObject);
begin
  FIsDark := not FIsDark;
  if FIsDark then
    TStyleManager.TrySetStyleFromResource('Dark')
  else
    TStyleManager.TrySetStyleFromResource('Light');
end;

end.
