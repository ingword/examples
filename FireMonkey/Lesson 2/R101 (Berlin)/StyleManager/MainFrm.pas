unit MainFrm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation, FMX.StdCtrls, FMX.ScrollBox,
  FMX.Memo, FMX.Layouts, FMX.ListBox, FMX.TabControl, FMX.TreeView;

type
  TFormMain = class(TForm)
    MemoLog: TMemo;
    Panel1: TPanel;
    Button2: TButton;
    ComboBoxResources: TComboBox;
    GroupBox1: TGroupBox;
    Button1: TButton;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TreeViewStyle: TTreeView;
    Button3: TButton;
    OpenDialog1: TOpenDialog;
    Button4: TButton;
    Button5: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    procedure Log(const AMessage: string);
  public
    procedure PrintResourceStyles;
    procedure PrintStyleTree;
    procedure PrintStyleDescriptor;
    constructor Create(AOwner: TComponent); override;
  end;

var
  FormMain: TFormMain;

implementation

uses
  FMX.Styles, ChildFrm, FMX.Objects;

{$R *.fmx}

function OSPaltformToString(const APlatform: TOSPlatform) : string;
begin
  case APlatform of
    TOSPlatform.Windows:
      Result := 'Windows';
    TOSPlatform.OSX:
      Result := 'OSX';
    TOSPlatform.iOS:
      Result := 'iOS';
    TOSPlatform.Android:
      Result := 'Android';
  else
    Result := 'Unknown';
  end;
end;

procedure TFormMain.Button1Click(Sender: TObject);
begin
  FormChild.Show;
end;

procedure TFormMain.Button2Click(Sender: TObject);
var
  StyleResourceName: string;
begin
  if ComboBoxResources.Selected <> nil then
  begin
    StyleResourceName := ComboBoxResources.Selected.Text;
    TStyleManager.TrySetStyleFromResource(StyleResourceName);
    PrintStyleTree;
    PrintStyleDescriptor;
  end;
end;

procedure TFormMain.Button3Click(Sender: TObject);
begin
  TStyleManager.TrySetStyleFromResource('airstyle');
  PrintStyleTree;
  PrintStyleDescriptor;
end;

procedure TFormMain.Button4Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    TStyleManager.SetStyleFromFile(OpenDialog1.FileName);
    PrintStyleTree;
    PrintStyleDescriptor;
  end;
end;

procedure TFormMain.Button5Click(Sender: TObject);
var
  Style: TFmxObject;
  ButtonStyle: TFmxObject;
  Rectangle: TRectangle;
begin
  Style := TStyleManager.ActiveStyle(nil);
  if Style <> nil then
  begin
    ButtonStyle := Style.FindStyleResource('buttonstyle');
    Rectangle := TRectangle.Create(nil);
    Rectangle.Margins.Rect := TRectF.Create(3,3,3,3);
    Rectangle.Opacity := 0.1;
    Rectangle.HitTest := False;
    Rectangle.Align := TAlignLayout.Contents;
    Rectangle.StyleName := 'MyBackground';
    ButtonStyle.AddObject(Rectangle);

    PrintStyleTree;
    // ��������� ����� �� ���� ����������
    TStyleManager.UpdateScenes;
  end;
end;

constructor TFormMain.Create(AOwner: TComponent);
begin
  inherited;
  PrintResourceStyles;
  PrintStyleTree;
  PrintStyleDescriptor;
end;

procedure TFormMain.Log(const AMessage: string);
begin
  MemoLog.Lines.Add(Format('%s: %s', [DateTimeToStr(Now), AMessage]));
end;

procedure TFormMain.PrintResourceStyles;
begin
  Log('---------------------------------------------------------------------');
  Log('������������������ �����:');
  ComboBoxResources.BeginUpdate;
  try
    ComboBoxResources.Clear;
    TStyleManager.EnumStyleResources(procedure (const AResourceName: string; const APlatform: TOSPlatform) begin
      Log(Format('    %s: %s', [OSPaltformToString(APlatform), AResourceName]));
      ComboBoxResources.Items.Add(AResourceName);
    end);
  finally
    ComboBoxResources.EndUpdate;
  end;
end;

procedure TFormMain.PrintStyleDescriptor;
var
  StyleDescriptor: TStyleDescription;
begin
  Log('---------------------------------------------------------------------');
  Log('���������� �����:');
  StyleDescriptor := TStyleManager.FindStyleDescriptor(TStyleManager.ActiveStyle(nil));
  if StyleDescriptor <> nil then
  begin
    Log('    Author: ' + StyleDescriptor.Author);
    Log('    Author Email: ' + StyleDescriptor.AuthorEMail);
    Log('    URL: ' + StyleDescriptor.AuthorURL);
    Log('    PlatformTarget: ' + StyleDescriptor.PlatformTarget);
    Log('    MobilePlatform: ' + BoolToStr(StyleDescriptor.MobilePlatform, True));
    Log('    Title: ' + StyleDescriptor.Title);
    Log('    Version: ' + StyleDescriptor.Version);
  end
  else
    Log('    ����� �� �������� ����������');
end;

procedure TFormMain.PrintStyleTree;

  function AddNode(AParentNode: TTreeViewItem; const ATitle: string): TTreeViewItem;
  begin
    Result := TTreeViewItem.Create(Self);
    if AParentNode = nil then
      Result.Parent := TreeViewStyle
    else
      Result.Parent := AParentNode;
    Result.Text := ATitle;
  end;

  procedure ReadStyleObject(AParentNode: TTreeViewItem; const AStyleObject: TFmxObject);
  var
    ChildStyle: TFmxObject;
    ChildNode: TTreeViewItem;
  begin
    for ChildStyle in AStyleObject.Children do
    begin
      // ������� ���� ��������� ������� �����
      ChildNode := AddNode(AParentNode, Format('%s: %s',
        [ChildStyle.StyleName, ChildStyle.ClassName]));
      // ���� � �������� ������� ����� ���� ��������, �� ������ ���� ������
      if ChildStyle.ChildrenCount > 0 then
        ReadStyleObject(ChildNode, ChildStyle);
    end;
  end;

var
  Style: TFmxObject;
begin
  TreeViewStyle.BeginUpdate;
  TreeViewStyle.Clear;
  Style := TStyleManager.ActiveStyle(nil);
  Style.Sort(function(Left, Right: TFmxObject): Integer
    begin
      Result := string.Compare(Left.StyleName, Right.StyleName);
    end);
  try
    ReadStyleObject(nil, Style);
  finally
    TreeViewStyle.EndUpdate;
  end;
end;

end.
