unit Unit_Main;

// Example shows how to use different TeeChart Series as "layers"
// to visualize different kinds of data.

// TeeChart World and Map series (polygons, shapes)

// Google Maps KML http://en.wikipedia.org/wiki/Keyhole_Markup_Language

// ESRI(tm) ShapeFiles

// Simple *.txt files containing placemarks


// As each "layer" is a normal Series, it can be shown or hidden individually,
// and fully customized as usually (visual formatting, series data, etc)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeeGDIPlus, ExtCtrls, StdCtrls,
  TeeProcs, TeEngine, Chart,
  TeeWorldSeries,
  TeeTools,
  TeeMapSeries,
  Series, TeCanvas, TeeComma,
  TeeShp,
  TeeSurfa,
  TeeAbout;

type
  TMainForm = class(TForm)
    Chart1: TChart;
    TeeCommander1: TTeeCommander;
    CBRoute66: TCheckBox;
    CBNuclear: TCheckBox;
    CBCities: TCheckBox;
    CBPopulated: TCheckBox;
    CBUSAColors: TCheckBox;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure CBRoute66Click(Sender: TObject);
    procedure CBNuclearClick(Sender: TObject);
    procedure CBCitiesClick(Sender: TObject);
    procedure CBPopulatedClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure CBUSAColorsClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }

    Route66 : TLineSeries;

    NuclearPlants,
    Cities : TPointSeries;

    Populated : TMapSeries;

    USA : TWorldSeries;

    procedure HidePointsOutsideUSA(const ASeries:TChartSeries); overload;
    procedure HidePointsOutsideUSA(const ASeries:TMapSeries); overload;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses Types;

{$R *.dfm}

procedure TMainForm.FormCreate(Sender: TObject);
var s : TStringList;
begin
  // NOTE:
  // Most of the following code its not necessary at all.
  // Same features can also be done at design-time using the
  // TeeChart editor dialog or Object Inspector.


  // Do not show Legend, Axes and Walls:

  Chart1.Legend.Hide;
  Chart1.Axes.Hide;
  Chart1.Walls.Hide;

  // Cosmetics:

  Chart1.Color:=clWhite;
  Chart1.View3D:=False;
  Chart1.Title.Caption:='USA';

  // Match USA map aspect when zooming:
  Chart1.Zoom.KeepAspectRatio:=True;
  
  // Create USA map:

  USA:=TWorldSeries.Create(Self);
  USA.Map:=wmUSA;

  USA.Color:=clWhite;
  
  USA.ParentChart:=Chart1;

  USA.FillSampleValues;

  // Add tools:

  Chart1.Tools.Add(TMarksTipTool);

  // Import Route 66 KML

  Route66:=TLineSeries.Create(Self);
  TTeeKMLSource.Load(Route66,'Data\Route66.kml');

  Chart1.AddSeries(Route66);

  // Cosmetics:

  Route66.Pointer.Visible:=True;
  Route66.Pointer.Size:=3;
  Route66.Pointer.Style:=psCircle;
  Route66.Pointer.Pen.Color:=TTeeCanvas.ColorFrom(clRed,128);
  Route66.Pointer.Color:=clMoneyGreen;

  // Nuclear Plants

  NuclearPlants:=TPointSeries.Create(Self);
  NuclearPlants.Pointer.Size:=2;

  TTeeKMLSource.Load(NuclearPlants,'Data\WorldPowerPlants.kml');

  Chart1.AddSeries(NuclearPlants);

  HidePointsOutsideUSA(NuclearPlants);

  // USA Cities

  Cities:=TPointSeries.Create(Self);

  // Load txt file and add to Cities series:
  s:=TStringList.Create;
  try
    s.LoadFromFile('Data\USACities.txt');
    USA.AddPlacemarks(s,Cities);
  finally
    s.Free;
  end;

  Cities.Pointer.Size:=2;
  Cities.Pointer.Pen.Hide;
  
  Cities.ParentChart:=Chart1; // <-- same as: Chart1.AddSeries(Cities)

  // Hide cities in Alaska, etc:
  HidePointsOutsideUSA(Cities);

  // Populated areas ESRI ShapeFile:

  Populated:=TMapSeries.Create(Self);
  Populated.ParentChart:=Chart1;

  LoadMap(Populated,'Data\ne_50m_urban_areas.shp');

  // Change automatic colors of Population area polygons:
  Populated.PaletteStyle:=psRainbow;
  Populated.UseColorRange:=False;
  Populated.UsePalette:=True;

  HidePointsOutsideUSA(Populated);
end;


// Simple function to remove points in ASeries that
// are outside the USA map. (XY coordinates outside USA bounding rectangle).

procedure TMainForm.HidePointsOutsideUSA(const ASeries:TChartSeries);
var X0,X1,
    Y0,Y1 : Single;

  function InsideBounds(Index:Integer):Boolean;
  var x,y : TChartValue;
  begin
    x:=ASeries.XValue[Index];
    y:=ASeries.YValue[Index];

    result:=(x>=X0) and (x<=X1) and
            (y>=Y0) and (y<=Y1);
  end;

var t : Integer;
begin
  X0:=USA.MinXValue;
  X1:=USA.MaxXValue;

  Y0:=USA.MinYValue;
  Y1:=USA.MaxYValue;

  t:=0;

  while t<ASeries.Count do
      if InsideBounds(t) then
         Inc(t)
      else
         ASeries.Delete(t);
end;

// Simple function to remove points in MapSeries ASeries that
// are outside the USA map. (Polygon Shape bound coordinates outside USA bounding rectangle).

procedure TMainForm.HidePointsOutsideUSA(const ASeries:TMapSeries);
var X0,X1,
    Y0,Y1 : Single;

  function InsideBounds(Index:Integer):Boolean;
  var pX0,pX1,
      pY0,pY1 : Single;
  begin
    pX0:=ASeries.Polygon[Index].Points.MinXValue;
    pX1:=ASeries.Polygon[Index].Points.MaxXValue;

    pY0:=ASeries.Polygon[Index].Points.MinYValue;
    pY1:=ASeries.Polygon[Index].Points.MaxYValue;

    result:=(pX0>=X0) and (pX1<=X1) and
            (pY0>=Y0) and (pY1<=Y1);
  end;

var t : Integer;
begin
  X0:=USA.MinXValue;
  X1:=USA.MaxXValue;

  Y0:=USA.MinYValue;
  Y1:=USA.MaxYValue;

  t:=0;

  while t<ASeries.Count do
      if InsideBounds(t) then
         Inc(t)
      else
         ASeries.Delete(t);
end;

procedure TMainForm.CBRoute66Click(Sender: TObject);
begin
  Route66.Visible:=CBRoute66.Checked;
end;

procedure TMainForm.CBNuclearClick(Sender: TObject);
begin
  NuclearPlants.Visible:=CBNuclear.Checked;
end;

procedure TMainForm.CBCitiesClick(Sender: TObject);
begin
  Cities.Visible:=CBCities.Checked;
end;

procedure TMainForm.CBPopulatedClick(Sender: TObject);
begin
  Populated.Visible:=CBPopulated.Checked;
end;

procedure TMainForm.Button1Click(Sender: TObject);
begin
  Chart1.Axes.Reset;
end;

procedure TMainForm.CBUSAColorsClick(Sender: TObject);
begin
  USA.UseColorRange:=CBUSAColors.Checked;
end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  TeeShowAboutBox;
end;

end.
