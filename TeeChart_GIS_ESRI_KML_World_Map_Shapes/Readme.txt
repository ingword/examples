www.steema.com
===============

Source code example, for Delphi 7 and up to RAD Studio XE6.

VCL for Windows (32bit and 64bit), and Firemonkey (FMX) projects for
Windows (32bit and 64bit), Mac OSX, Android and iOS.

This example shows how to use different TeeChart Series as "layers"
to visualize different kinds of data.

-TeeChart World and Map series (polygons, shapes)

-Google Maps KML http://en.wikipedia.org/wiki/Keyhole_Markup_Language

-ESRI(tm) ShapeFiles

-Simple *.txt files containing placemarks

As each "layer" is a normal Series, it can be shown or hidden individually,
and fully customized as usually (visual formatting, series data, etc)

Copyright notices:
==================

Dataset file: ne_50m_urban_areas.shp
"Made with Natural Earth. Free vector and raster map data @ naturalearthdata.com"

Route 66 KML path file:
http://route66map.publishpath.com/google-map

Nuclear Power Plants KML:
https://maps.google.com/maps/ms?ie=UTF8&msa=0&output=kml&msid=211748341523244283833.00045902e10d2ec3bf089


