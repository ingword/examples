unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Rtti, Vcl.ComCtrls, Vcl.StdCtrls;

type
  TfmMain = class(TForm)
    tvMain: TTreeView;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMain: TfmMain;

implementation

{$R *.dfm}

procedure TfmMain.FormCreate(Sender: TObject);
var
  LContext: TRttiContext;
  LType: TRttiType;
  LMethod: TRttiMethod;
  LProperty: TRttiProperty;
  LField: TRttiField;
  LTreeNode1, LTreeNode2: TTreeNode;
begin
  LContext := TRttiContext.Create;
  try
    LType := LContext.GetType(TButton);
    LTreeNode1 := tvMain.Items.AddChild(nil, LType.ToString);

    LTreeNode2 := tvMain.Items.AddChild(LTreeNode1, 'Methods');
    for LMethod in LType.GetMethods do
    begin
      tvMain.Items.AddChild(LTreeNode2, LMethod.ToString);
    end;

    LTreeNode2 := tvMain.Items.AddChild(LTreeNode1, 'Properties');
    for LProperty in LType.GetProperties do
    begin
      tvMain.Items.AddChild(LTreeNode2, LProperty.ToString);
    end;

    LTreeNode2 := tvMain.Items.AddChild(LTreeNode1, 'Fields');
    for LField in LType.GetFields do
    begin
      tvMain.Items.AddChild(LTreeNode2, LField.ToString);
    end;

    tvMain.FullExpand;
  finally
    LContext.Free;
  end;
end;

end.
