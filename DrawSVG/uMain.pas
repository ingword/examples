﻿unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls;

type
  TForm4 = class(TForm)
    btnPaintSvg: TButton;
    btnPaintX: TButton;
    procedure btnPaintSvgClick(Sender: TObject);
    procedure btnPaintXClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

uses
 System.Math.Vectors
 ;
{$R *.fmx}
{$R *.LgXhdpiTb.fmx ANDROID}

procedure TForm4.btnPaintSvgClick(Sender: TObject);
const
 c_BigRectangleH  = 40;
 c_BigRectangleW = 60;
 c_SmallRectangleH = 10;
 c_SmallRectangleW = 15;
var
 l_PolygonSVG : TPolygon;
 l_SVG_String: string;
 l_PD: TPathData;
 l_Point: TPointF;
begin
 l_PD := TPathData.Create;
 l_SVG_String := 'M 40,40, L 100,40, L 100,80, L 40,80, L 40,40' +
 // begin UHO
 'L 40,20' +
 'L 60, 20' +
 'L 60, 40';

 l_PD.Data := l_SVG_String;
 l_Point:= l_PD.FlattenToPolygon(l_PolygonSVG);

 self.Canvas.BeginScene;
 self.Canvas.DrawPolygon(l_PolygonSVG, 1);
 self.Canvas.FillPolygon(l_PolygonSVG, 0.5);
 self.Canvas.EndScene;

 FreeAndNil(l_PD);


end;

procedure TForm4.btnPaintXClick(Sender: TObject);
var
 l_PolygonSVG : TPolygon;
 l_SVG_String: string;
 l_PD: TPathData;
 l_Point: TPointF;
begin
 l_PD := TPathData.Create;
 l_SVG_String := 'M 10,30 L 30,10 L 50,30 L 70,10 L 90,30 L 70,50 L 90,70' +
                 'L 70,90 L 50,70 L 30,90 L 10,70 L 30,50 L 30,50 L 10,30' ;

 l_PD.Data := l_SVG_String;
 l_Point:= l_PD.FlattenToPolygon(l_PolygonSVG);

 self.Canvas.BeginScene;
 self.Canvas.DrawPolygon(l_PolygonSVG, 1);
 self.Canvas.FillPolygon(l_PolygonSVG, 0.5);
 self.Canvas.EndScene;

 FreeAndNil(l_PD);
end;

end.
